using UnityEngine;
using Zenject;

namespace BeatDaBeat
{
    [CreateAssetMenu(fileName = "RootInstaller", menuName = "Installers/RootInstaller")]
    public class RootInstaller : ScriptableObjectInstaller<RootInstaller>
    {
        [SerializeField] private NelliFader _nelliFader;
        [SerializeField] private MyCursor _myCursor;

        public override void InstallBindings()
        {
            Container.Bind<LevelLoader>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();

            Container.Bind<NelliFader>().FromComponentInNewPrefab(_nelliFader).AsSingle().NonLazy();
            Container.Bind<MyCursor>().FromComponentInNewPrefab(_myCursor).AsSingle().NonLazy();
        }
    }
}