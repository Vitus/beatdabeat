﻿using MusicHell;
using MusicHell.Core;
using Zenject;

namespace BeatDaBeat
{
    public class PreloaderSceneInstaller : MonoInstaller
    {
        public float WaitDuration;
        public string NextScene;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<UnityTime>()
                .AsSingle();
            Container.BindInterfacesTo<SceneLoader>()
                .AsSingle();
            Container.BindInterfacesTo<TimeScaleAutoReset>()
                .AsSingle()
                .NonLazy();
            
            Container.BindInterfacesTo<SkipSceneByMouseInput>()
                .AsSingle();
            Container.BindInterfacesTo<UnityTimer>()
                .AsTransient()
                .WithArguments(WaitDuration)
                .WhenInjectedInto<SceneSkipper>();
            Container.BindInterfacesTo<SceneSkipper>()
                .AsSingle()
                .WithArguments(NextScene)
                .NonLazy();
        }
    }
}