﻿using Cysharp.Threading.Tasks;
using MusicHell.Core;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace MusicHell
{
    public class SceneSkipper : IInitializable
    {
        private readonly string _nextSceneName;
        private readonly ISceneLoader _sceneLoader;
        private readonly ISkipSceneInput _skipSceneInput;
        private readonly ITimer _skipTimer;

        public SceneSkipper(string nextSceneName, ISceneLoader sceneLoader, ISkipSceneInput skipSceneInput, ITimer skipTimer)
        {
            Assert.IsTrue(!string.IsNullOrEmpty(nextSceneName));
            Assert.IsNotNull(sceneLoader);
            Assert.IsNotNull(skipSceneInput);
            Assert.IsNotNull(skipTimer);

            _nextSceneName = nextSceneName;
            _sceneLoader = sceneLoader;
            _skipSceneInput = skipSceneInput;
            _skipTimer = skipTimer;
        }

        public void Initialize()
        {
            LoadNextSceneWhenRequired();
        }

        private async void LoadNextSceneWhenRequired()
        {
            Debug.Log("LoadNextScene coroutine");
            _skipTimer.Start();
            await UniTask.WaitUntil(() => _skipSceneInput.Performed || _skipTimer.Completed);
            _sceneLoader.LoadScene(_nextSceneName);
        }
    }
}