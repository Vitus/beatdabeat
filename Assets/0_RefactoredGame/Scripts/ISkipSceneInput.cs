﻿namespace MusicHell
{
    public interface ISkipSceneInput
    {
        bool Performed { get; }
    }
}