﻿using UnityEngine;

namespace MusicHell
{
    public class SkipSceneByMouseInput : ISkipSceneInput
    {
        public bool Performed
            => Input.GetMouseButtonDown(0);
    }
}