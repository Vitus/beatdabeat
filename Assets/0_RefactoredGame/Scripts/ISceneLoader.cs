﻿namespace MusicHell
{
    public interface ISceneLoader
    {
        void LoadScene(string levelName);
    }
}