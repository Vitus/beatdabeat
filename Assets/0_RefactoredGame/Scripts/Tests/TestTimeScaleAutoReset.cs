﻿using MusicHell.Core;
using NSubstitute;
using NUnit.Framework;

namespace MusicHell.Tests
{
    public class TestTimeScaleAutoReset
    {
        [Test]
        public void Should_reset_time_scale_at_initialization()
        {
            var time = Substitute.For<ITime>();
            var autoReset = new TimeScaleAutoReset(time);
        
            autoReset.Initialize();
        
            time.Received().TimeScale = TimeScaleAutoReset.DefaultTimeScale;
        }
    }
}