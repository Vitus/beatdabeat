﻿using System.Collections;
using MusicHell.Core;
using NSubstitute;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace MusicHell.Tests
{
    [TestFixture]
    public class TestSceneSkipper
    {
        private const string NextSceneName = "NextScene";

        private ISceneLoader _sceneLoader;
        private ISkipSceneInput _skipInput;
        private ITimer _skipTimer;
        private SceneSkipper _sceneSkipper;

        [SetUp]
        public void SetUp()
        {
            _sceneLoader = Substitute.For<ISceneLoader>();
            _skipInput = Substitute.For<ISkipSceneInput>();
            _skipTimer = Substitute.For<ITimer>();

            _sceneSkipper = new SceneSkipper(NextSceneName, _sceneLoader, _skipInput, _skipTimer);
        }

        [UnityTest]
        public IEnumerator Should_load_next_scene_when_skip_action_performed()
        {
            _sceneSkipper.Initialize();
        
            yield return null;
            _sceneLoader.DidNotReceive().LoadScene(Arg.Any<string>());
        
            _skipInput.Performed.Returns(true);
        
            yield return null;
            _sceneLoader.Received().LoadScene(NextSceneName);
        }

        [UnityTest]
        public IEnumerator Should_not_start_waiting_until_initialized()
        {
            _skipTimer.Completed.Returns(true);
            _skipInput.Performed.Returns(true);
        
            yield return null;
            yield return null;
        
            _sceneLoader.DidNotReceive().LoadScene(Arg.Any<string>());
        }

        [UnityTest]
        public IEnumerator Should_load_next_scene_when_time_passed()
        {
            _sceneSkipper.Initialize();
            _skipTimer.Received().Start();
        
            yield return null;
            _sceneLoader.DidNotReceive().LoadScene(Arg.Any<string>());
        
            _skipTimer.Completed.Returns(true);
        
            yield return null;
            _sceneLoader.Received().LoadScene(NextSceneName);
        }
    }
}