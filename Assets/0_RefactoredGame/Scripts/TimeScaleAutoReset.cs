﻿using MusicHell.Core;
using Zenject;

namespace MusicHell
{
    public class TimeScaleAutoReset : IInitializable
    {
        public const float DefaultTimeScale = 1f;
        private readonly ITime _time;

        public TimeScaleAutoReset(ITime time)
        {
            _time = time;
        }
    
        public void Initialize()
        {
            _time.TimeScale = DefaultTimeScale;
        }
    }
}