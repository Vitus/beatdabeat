﻿using UnityEngine;

namespace MusicHell.Core
{
    public class UnityTime : ITime
    {
        public float TimeScale
        {
            get => Time.timeScale;
            set => Time.timeScale = value;
        }
    }
}