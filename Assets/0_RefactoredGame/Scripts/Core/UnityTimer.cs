﻿using UnityEngine;

namespace MusicHell.Core
{
    public class UnityTimer : ITimer
    {
        private readonly float _duration;
        private float _startTime;

        public UnityTimer(float duration)
        {
            _duration = duration;
        }

        public bool Completed
            => Time.realtimeSinceStartup - _startTime > _duration;

        public void Start()
        {
            _startTime = Time.realtimeSinceStartup;
        }
    }
}