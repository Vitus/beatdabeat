﻿namespace MusicHell.Core
{
    public interface ITimer
    {
        bool Completed { get; }
        void Start();
    }
}