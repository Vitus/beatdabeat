﻿namespace MusicHell.Core
{
    public interface ITime
    {
        float TimeScale { get; set; }
    }
}