// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// This work is licensed under a Creative Commons Attribution 3.0 Unported License.
// http://creativecommons.org/licenses/by/3.0/deed.en_GB
//
// You are free:
//
// to copy, distribute, display, and perform the work
// to make derivative works
// to make commercial use of the work


Shader "My/GlitchShader" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_DispTex ("Base (RGB)", 2D) = "white" {}
	_Intensity ("Glitch Intensity", Range(0.1, 1.0)) = 1.0
	_Displacement ("Displacement Distance", Range(0, 5.0)) = 1.0
	_DisplacementRandom ("Displacement Ripple", Range(0, 5.0)) = 1.0
	_ColorShift ("Color shift", Range(0, 5.0)) = 0.0
}

SubShader {
	Pass {
	    Tags { 
			"Queue"="Transparent" 
			"RenderType"="Transparent" 
			}
		Cull Off
        Lighting Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		
		#include "UnityCG.cginc"
		
		uniform sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		uniform sampler2D _DispTex;
		float _Intensity;
		float _Displacement;
		float _DisplacementRandom;
		float _ColorShift;

		float scale;
		
		struct v2f {
			float4 pos : POSITION;
			float2 uv : TEXCOORD0;
		};
		
		v2f vert( appdata_img v )
		{
			v2f o;
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv = v.texcoord.xy;
			return o;
		}
		
		half4 frag (v2f i) : COLOR
		{
	
			half4 normal = tex2D (_DispTex, float2(i.uv.x * _DisplacementRandom, 0));
			half4 normalColor = tex2D (_DispTex, float2(i.uv.x * _DisplacementRandom, 1));
			
			i.uv.y += (normal.y) * _MainTex_TexelSize.y * _Displacement;
			
			half4 color = tex2D(_MainTex,  i.uv.xy);
			half4 redcolor = tex2D(_MainTex,  float2(i.uv.x, i.uv.y + (normal.y) * _MainTex_TexelSize.y * _ColorShift));	
			half4 greencolor = tex2D(_MainTex,  float2(i.uv.x, i.uv.y - (normal.y) * _MainTex_TexelSize.y * _ColorShift));
			float alpha = color.a / 3 + redcolor.a / 3 + greencolor.a / 3;
			
			color.r = redcolor.r * redcolor.a;
			color.g = greencolor.g * greencolor.a;
			color.b = color.b * color.a;
			color.a = alpha;

			return color;
		}
		ENDCG
	}
}

Fallback off

}