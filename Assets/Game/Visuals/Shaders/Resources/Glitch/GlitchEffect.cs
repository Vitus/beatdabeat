/**
This work is licensed under a Creative Commons Attribution 3.0 Unported License.
http://creativecommons.org/licenses/by/3.0/deed.en_GB

You are free:

to copy, distribute, display, and perform the work
to make derivative works
to make commercial use of the work
*/

using UnityEngine;

public class GlitchEffect : MonoBehaviour {
    public float Intensity;
    public float Dispacement;
    public float ColorShift;

    private float _ColorTimer;
    private float _nextColorTime;
    private float _DisplaceTimer;
    private float _nextDisplaceTime;
    private Material _material;

    void Start() {
        _material = GetComponent<Renderer>().material;
        _material.shader = Shader.Find("My/GlitchShader");
    }

	void Update () {
		_material.SetFloat("_Intensity", Intensity);
		
		_ColorTimer += Time.deltaTime * Intensity;
		_DisplaceTimer += Time.deltaTime * Intensity / 2f;
		
		if(_ColorTimer > _nextColorTime) {
		    float randomShift = Mathf.Clamp(Random.Range(-5f, 3f), 0f, 1f);
            _material.SetFloat("_ColorShift", randomShift * ColorShift);
			_ColorTimer = 0;
			_nextColorTime = Random.value;
		}

	    if (_DisplaceTimer > _nextDisplaceTime)
	    {
            float randomShift = Mathf.Clamp(Random.Range(-5f, 3f), 0f, 1f);
            if (randomShift > 0f)
            {
	            _material.SetFloat("_Displacement", Random.value*Dispacement);
	            _material.SetFloat("_DisplacementRandom", Random.Range(0f, 1.5f));
	        }
	        else
	            _material.SetFloat("_Displacement", 0);
            _DisplaceTimer = 0;
            _nextDisplaceTime = Random.value;
        }
	}
}