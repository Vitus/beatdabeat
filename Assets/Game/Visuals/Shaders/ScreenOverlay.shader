﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "NelliShader/ScreenOverlay" {
   Properties {
      _MainTex ("Main texture", 2D) = "white" {} 
      _OverlayTex ("Overlay texture", 2D) = "white" {}
	  _Power ("Overlay power", Range (0.0, 3.0)) = 1.0
   }
   SubShader {
      Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
      Cull Off
      ZWrite Off 
      Fog { Mode Off }
      Blend One OneMinusSrcAlpha

      Pass {

         CGPROGRAM
 
         #pragma vertex vert  
         #pragma fragment frag 
		 #include "UnityCG.cginc"

		 sampler2D _MainTex;
	     fixed4 _MainTex_ST;
		 sampler2D _OverlayTex;
		 float _Power;
 
         struct vertexInput {
            float4 vertex : POSITION;
			fixed2 uv : TEXCOORD0;
			fixed4 color : COLOR;
         };

         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 position_in_screen_space : TEXCOORD1;
			fixed2 uv : TEXCOORD0;
			fixed4 color : COLOR;
         };
 
         vertexOutput vert(vertexInput i) 
         {
            vertexOutput o; 
			o.uv = TRANSFORM_TEX(i.uv,_MainTex);
            o.pos =  UnityObjectToClipPos(i.vertex);
            o.position_in_screen_space = ComputeScreenPos(o.pos);
			o.color = i.color;
            return o;
         }
 
         float4 frag(vertexOutput i) : COLOR 
         {
			float2 screenUV = i.position_in_screen_space.xy / i.position_in_screen_space.w;

			fixed4 mainColor = tex2D (_MainTex, i.uv) * i.color;
			fixed4 texColor = tex2D (_OverlayTex, screenUV);

			fixed4 resultCol = lerp((mainColor*texColor*2), (1.0-(2.0*(1.0-mainColor)*(1.0-texColor))), round(mainColor));
			resultCol = lerp(mainColor, resultCol, _Power);
			resultCol.a = mainColor.a;
			resultCol.rgb *= resultCol.a;

			return resultCol;
         }
 
         ENDCG  
      }
   }
}