#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_StreetWise : LevelScript
    {
        protected override string ClipName {
            get { return "StreetWise.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object rightatack = blackboard.GetValue((c, i, t, l, castc, casti) => -1f);
            blackboard.SetValue("node_19.output_value", rightatack);

            object leftatack = blackboard.GetValue((c, i, t, l, castc, casti) => 1f);
            blackboard.SetValue("node_20.output_value", leftatack);

            object rotation = blackboard.GetValue((c, i, t, l, castc, casti) => 0f);
            blackboard.SetValue("node_37.output_value", rotation);

            object enemies1_3;
            blackboard.SubscribeForChanges("node_38.output_value", (b, o) => enemies1_3 = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_33.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_38.output_value");
            object enemies2_4;
            blackboard.SubscribeForChanges("node_45.output_value", (b, o) => enemies2_4 = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_40.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_45.output_value");
            object path = blackboard.GetValue((c, i, t, l, castc, casti) => new MyPath(-360f,50f,-320f,-50f,-280f,50f,-240f,-50f,-200f,50f,-160f,-50f,-120f,50f,-80f,-50f,-40f,50f,0f,-50f,40f,50f,80f,-50f,120f,50f,160f,-50f,200f,50f,240f,-50f,280f,50f,320f,-50f,360f,50f));
            blackboard.SetValue("node_50.output_path", path);

            object enemies1_3_2;
            blackboard.SubscribeForChanges("node_79.output_value", (b, o) => enemies1_3_2 = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_75.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_79.output_value");
            object enemies2_4_2;
            blackboard.SubscribeForChanges("node_86.output_value", (b, o) => enemies2_4_2 = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_81.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_86.output_value");

            if (launchTime + 0.1012573 > Game.Music.Time) yield return Wait(launchTime + 0.1012573f);
            Player.StartCoroutine(Pattern_8/*Effects*/(launchTime + 0.1012573f, blackboard/*out*/));
            if (launchTime + 0.2697908 > Game.Music.Time) yield return Wait(launchTime + 0.2697908f);
            Player.StartCoroutine(Pattern_24/*DanceMan*/(launchTime + 0.2697908f, blackboard/*out*/));
            if (launchTime + 0.9397901 > Game.Music.Time) yield return Wait(launchTime + 0.9397901f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.9397901f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,336f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 1.356968 > Game.Music.Time) yield return Wait(launchTime + 1.356968f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.356968f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(1f,100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 1.359802f));
            if (launchTime + 2.928044 > Game.Music.Time) yield return Wait(launchTime + 2.928044f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.928044f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 16f*(float)d, (c,i,t,l, castc, casti) => (360f/(float)toint((float)c,(float)d))*(float)toint((float)i,(float)d), (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (360f/(float)toint((float)c,(float)d))*(float)toint((float)i,(float)d), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 3.63242 > Game.Music.Time) yield return Wait(launchTime + 3.63242f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 3.63242f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => (float)i%(float)d*50f+200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 6.00456 > Game.Music.Time) yield return Wait(launchTime + 6.00456f);
            Player.StartCoroutine(Pattern_1/*FirstPartOfLevel*/(launchTime + 6.00456f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 6.00456 > Game.Music.Time) yield return Wait(launchTime + 6.00456f);
            Player.StartCoroutine(Pattern_1/*FirstPartOfLevel*/(launchTime + 6.00456f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 6.258766 > Game.Music.Time) yield return Wait(launchTime + 6.258766f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.258766f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_8.output_enemy(ies)", 0.3489594f));
            if (launchTime + 6.68075 > Game.Music.Time) yield return Wait(launchTime + 6.68075f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 6.68075f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 21.06052 > Game.Music.Time) yield return Wait(launchTime + 21.06052f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 21.06052f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 21.9636 > Game.Music.Time) yield return Wait(launchTime + 21.9636f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 21.9636f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 22.83769 > Game.Music.Time) yield return Wait(launchTime + 22.83769f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 22.83769f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 23.7381 > Game.Music.Time) yield return Wait(launchTime + 23.7381f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 23.7381f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 24.63254 > Game.Music.Time) yield return Wait(launchTime + 24.63254f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 24.63254f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 25.51241 > Game.Music.Time) yield return Wait(launchTime + 25.51241f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 25.51241f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 26.41581 > Game.Music.Time) yield return Wait(launchTime + 26.41581f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 26.41581f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 27.28673 > Game.Music.Time) yield return Wait(launchTime + 27.28673f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 27.28673f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 28.18669 > Game.Music.Time) yield return Wait(launchTime + 28.18669f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 28.18669f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 29.06443 > Game.Music.Time) yield return Wait(launchTime + 29.06443f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 29.06443f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 29.9651 > Game.Music.Time) yield return Wait(launchTime + 29.9651f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 29.9651f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 30.85571 > Game.Music.Time) yield return Wait(launchTime + 30.85571f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 30.85571f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 31.74691 > Game.Music.Time) yield return Wait(launchTime + 31.74691f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 31.74691f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 32.61307 > Game.Music.Time) yield return Wait(launchTime + 32.61307f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 32.61307f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 33.50878 > Game.Music.Time) yield return Wait(launchTime + 33.50878f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 33.50878f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 33.72584 > Game.Music.Time) yield return Wait(launchTime + 33.72584f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 33.72584f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 33.95685 > Game.Music.Time) yield return Wait(launchTime + 33.95685f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 33.95685f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 34.17704 > Game.Music.Time) yield return Wait(launchTime + 34.17704f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 34.17704f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 34.38509 > Game.Music.Time) yield return Wait(launchTime + 34.38509f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 34.38509f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 34.62105 > Game.Music.Time) yield return Wait(launchTime + 34.62105f);
            Player.StartCoroutine(Pattern_3/*RightBeatAtacker*/(launchTime + 34.62105f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 34.83433 > Game.Music.Time) yield return Wait(launchTime + 34.83433f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 34.83433f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 35.3314 > Game.Music.Time) yield return Wait(launchTime + 35.3314f);
            Player.StartCoroutine(Pattern_4/*FirstLine*/(launchTime + 35.3314f, blackboard, (c,i,t,l, castc, casti) => "node_50.output_path", (c,i,t,l, castc, casti) => new Vector2(0f,180f), (c,i,t,l, castc, casti) => new Vector2((float)ar,0.8f), (c,i,t,l, castc, casti) => 0f/*out*/, "node_33.output_enemy"));
            if (launchTime + 35.58519 > Game.Music.Time) yield return Wait(launchTime + 35.58519f);
            Player.StartCoroutine(Pattern_10/*MelodyShoot*/(launchTime + 35.58519f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_value", (c,i,t,l, castc, casti) => "node_37.output_value"/*out*/));
            if (launchTime + 38.14696 > Game.Music.Time) yield return Wait(launchTime + 38.14696f);
            Player.StartCoroutine(Pattern_28/*Infiltration*/(launchTime + 38.14696f, blackboard/*out*/));
            if (launchTime + 39.09135 > Game.Music.Time) yield return Wait(launchTime + 39.09135f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 39.09135f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 39.09135 > Game.Music.Time) yield return Wait(launchTime + 39.09135f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 39.09135f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 45.83427 > Game.Music.Time) yield return Wait(launchTime + 45.83427f);
            Player.StartCoroutine(Pattern_31/*Final*/(launchTime + 45.83427f, blackboard/*out*/));
            if (launchTime + 49.55185 > Game.Music.Time) yield return Wait(launchTime + 49.55185f);
            Player.StartCoroutine(Pattern_7/*ThirdLine*/(launchTime + 49.55185f, blackboard, (c,i,t,l, castc, casti) => "node_50.output_path", (c,i,t,l, castc, casti) => new Vector2(0f,120f), (c,i,t,l, castc, casti) => new Vector2(-(float)ar,0.8f), (c,i,t,l, castc, casti) => 0f/*out*/, "node_40.output_enemy"));
            if (launchTime + 49.81768 > Game.Music.Time) yield return Wait(launchTime + 49.81768f);
            Player.StartCoroutine(Pattern_10/*MelodyShoot*/(launchTime + 49.81768f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_value", (c,i,t,l, castc, casti) => "node_37.output_value"/*out*/));
            if (launchTime + 49.84996 > Game.Music.Time) yield return Wait(launchTime + 49.84996f);
            Player.StartCoroutine(Pattern_12/*BeatShoot*/(launchTime + 49.84996f, blackboard, (c,i,t,l, castc, casti) => "node_45.output_value", (c,i,t,l, castc, casti) => "node_37.output_value"/*out*/));
            if (launchTime + 52.38052 > Game.Music.Time) yield return Wait(launchTime + 52.38052f);
            Player.StartCoroutine(Pattern_28/*Infiltration*/(launchTime + 52.38052f, blackboard/*out*/));
            if (launchTime + 53.32563 > Game.Music.Time) yield return Wait(launchTime + 53.32563f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 53.32563f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 53.32563 > Game.Music.Time) yield return Wait(launchTime + 53.32563f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 53.32563f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 56.50823 > Game.Music.Time) yield return Wait(launchTime + 56.50823f);
            Player.StartCoroutine(Pattern_33/*Circle*/(launchTime + 56.50823f, blackboard/*out*/));
            if (launchTime + 56.94571 > Game.Music.Time) yield return Wait(launchTime + 56.94571f);
            Player.StartCoroutine(Pattern_12/*BeatShoot*/(launchTime + 56.94571f, blackboard, (c,i,t,l, castc, casti) => "node_45.output_value", (c,i,t,l, castc, casti) => "node_37.output_value"/*out*/));
            if (launchTime + 59.97026 > Game.Music.Time) yield return Wait(launchTime + 59.97026f);
            Player.StartCoroutine(Pattern_33/*Circle*/(launchTime + 59.97026f, blackboard/*out*/));
            if (launchTime + 60.01689 > Game.Music.Time) yield return Wait(launchTime + 60.01689f);
            Player.StartCoroutine(Pattern_31/*Final*/(launchTime + 60.01689f, blackboard/*out*/));
            if (launchTime + 64.05103 > Game.Music.Time) yield return Wait(launchTime + 64.05103f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 64.05103f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2182617f));
            if (launchTime + 64.05328 > Game.Music.Time) yield return Wait(launchTime + 64.05328f);
            Player.StartCoroutine(Pattern_34/*Three*/(launchTime + 64.05328f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-171f*(float)ar,152f)/*out*/));
            if (launchTime + 64.37694 > Game.Music.Time) yield return Wait(launchTime + 64.37694f);
            Player.StartCoroutine(Pattern_34/*Three*/(launchTime + 64.37694f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,152f)/*out*/));
            if (launchTime + 64.3913 > Game.Music.Time) yield return Wait(launchTime + 64.3913f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 64.3913f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2293396f));
            if (launchTime + 64.71529 > Game.Music.Time) yield return Wait(launchTime + 64.71529f);
            Player.StartCoroutine(Pattern_34/*Three*/(launchTime + 64.71529f, blackboard, (c,i,t,l, castc, casti) => new Vector2(171f*(float)ar,152f)/*out*/));
            if (launchTime + 64.71612 > Game.Music.Time) yield return Wait(launchTime + 64.71612f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 64.71612f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2210464f));
            if (launchTime + 65.81428 > Game.Music.Time) yield return Wait(launchTime + 65.81428f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 65.81428f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2320786f));
            if (launchTime + 66.15972 > Game.Music.Time) yield return Wait(launchTime + 66.15972f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 66.15972f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2320862f));
            if (launchTime + 66.5 > Game.Music.Time) yield return Wait(launchTime + 66.5f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 66.5f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2293243f));
            if (launchTime + 67.17916 > Game.Music.Time) yield return Wait(launchTime + 67.17916f);
            Player.StartCoroutine(Pattern_14/*BossEncount*/(launchTime + 67.17916f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 67.60414 > Game.Music.Time) yield return Wait(launchTime + 67.60414f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 67.60414f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.23629f));
            if (launchTime + 67.60764 > Game.Music.Time) yield return Wait(launchTime + 67.60764f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 67.60764f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x58/255f,0x24/255f,0x00/255f)/*out*/, 1.312149f));
            if (launchTime + 68.94216 > Game.Music.Time) yield return Wait(launchTime + 68.94216f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 68.94216f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x05/255f,0x45/255f,0x1b/255f)/*out*/, 2.200829f));
            if (launchTime + 71.1654 > Game.Music.Time) yield return Wait(launchTime + 71.1654f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 71.1654f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x01/255f,0x22/255f,0x3e/255f)/*out*/, 3.288231f));
            if (launchTime + 74.48412 > Game.Music.Time) yield return Wait(launchTime + 74.48412f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 74.48412f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x23/255f,0x00/255f)/*out*/, 3.331238f));
            if (launchTime + 77.83727 > Game.Music.Time) yield return Wait(launchTime + 77.83727f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 77.83727f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x38/255f,0x00/255f,0x2f/255f)/*out*/, 2.213486f));
            if (launchTime + 80.0899 > Game.Music.Time) yield return Wait(launchTime + 80.0899f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 80.0899f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x2f/255f,0x28/255f)/*out*/, 3.165794f));
            if (launchTime + 81.3926 > Game.Music.Time) yield return Wait(launchTime + 81.3926f);
            Player.StartCoroutine(Pattern_14/*BossEncount*/(launchTime + 81.3926f, blackboard, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 83.28198 > Game.Music.Time) yield return Wait(launchTime + 83.28198f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 83.28198f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x42/255f,0x26/255f,0x05/255f)/*out*/, 4.310036f));
            if (launchTime + 87.60854 > Game.Music.Time) yield return Wait(launchTime + 87.60854f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 87.60854f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 3.957405f));
            if (launchTime + 91.63285 > Game.Music.Time) yield return Wait(launchTime + 91.63285f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 91.63285f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x26/255f,0x00/255f)/*out*/, 3.269119f));
            if (launchTime + 94.9073 > Game.Music.Time) yield return Wait(launchTime + 94.9073f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 94.9073f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 1.108727f));
            if (launchTime + 94.92511 > Game.Music.Time) yield return Wait(launchTime + 94.92511f);
            Player.StartCoroutine(Pattern_37/*AnotherPart*/(launchTime + 94.92511f, blackboard/*out*/));
            if (launchTime + 96.04893 > Game.Music.Time) yield return Wait(launchTime + 96.04893f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 96.04893f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4161606f));
            if (launchTime + 111.7653 > Game.Music.Time) yield return Wait(launchTime + 111.7653f);
            Player.StartCoroutine(Pattern_4/*FirstLine*/(launchTime + 111.7653f, blackboard, (c,i,t,l, castc, casti) => "node_50.output_path", (c,i,t,l, castc, casti) => new Vector2(240f*(float)ar,0f), (c,i,t,l, castc, casti) => new Vector2((float)ar,0.8f), (c,i,t,l, castc, casti) => -90f/*out*/, "node_75.output_enemy"));
            if (launchTime + 112.029 > Game.Music.Time) yield return Wait(launchTime + 112.029f);
            Player.StartCoroutine(Pattern_10/*MelodyShoot*/(launchTime + 112.029f, blackboard, (c,i,t,l, castc, casti) => "node_79.output_value", (c,i,t,l, castc, casti) => 90f/*out*/));
            if (launchTime + 114.5902 > Game.Music.Time) yield return Wait(launchTime + 114.5902f);
            Player.StartCoroutine(Pattern_28/*Infiltration*/(launchTime + 114.5902f, blackboard/*out*/));
            if (launchTime + 115.543 > Game.Music.Time) yield return Wait(launchTime + 115.543f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 115.543f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 115.543 > Game.Music.Time) yield return Wait(launchTime + 115.543f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 115.543f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 122.2657 > Game.Music.Time) yield return Wait(launchTime + 122.2657f);
            Player.StartCoroutine(Pattern_31/*Final*/(launchTime + 122.2657f, blackboard/*out*/));
            if (launchTime + 125.9997 > Game.Music.Time) yield return Wait(launchTime + 125.9997f);
            Player.StartCoroutine(Pattern_7/*ThirdLine*/(launchTime + 125.9997f, blackboard, (c,i,t,l, castc, casti) => "node_50.output_path", (c,i,t,l, castc, casti) => new Vector2(-240f*(float)ar,0f), (c,i,t,l, castc, casti) => new Vector2((float)ar,0.8f), (c,i,t,l, castc, casti) => 90f/*out*/, "node_81.output_enemy"));
            if (launchTime + 126.2584 > Game.Music.Time) yield return Wait(launchTime + 126.2584f);
            Player.StartCoroutine(Pattern_10/*MelodyShoot*/(launchTime + 126.2584f, blackboard, (c,i,t,l, castc, casti) => "node_79.output_value", (c,i,t,l, castc, casti) => 90f/*out*/));
            if (launchTime + 126.2775 > Game.Music.Time) yield return Wait(launchTime + 126.2775f);
            Player.StartCoroutine(Pattern_12/*BeatShoot*/(launchTime + 126.2775f, blackboard, (c,i,t,l, castc, casti) => "node_86.output_value", (c,i,t,l, castc, casti) => 90f/*out*/));
            if (launchTime + 128.8335 > Game.Music.Time) yield return Wait(launchTime + 128.8335f);
            Player.StartCoroutine(Pattern_28/*Infiltration*/(launchTime + 128.8335f, blackboard/*out*/));
            if (launchTime + 129.7656 > Game.Music.Time) yield return Wait(launchTime + 129.7656f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 129.7656f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 129.7656 > Game.Music.Time) yield return Wait(launchTime + 129.7656f);
            Player.StartCoroutine(Pattern_29/*SmallShots*/(launchTime + 129.7656f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 132.9253 > Game.Music.Time) yield return Wait(launchTime + 132.9253f);
            Player.StartCoroutine(Pattern_33/*Circle*/(launchTime + 132.9253f, blackboard/*out*/));
            if (launchTime + 133.4048 > Game.Music.Time) yield return Wait(launchTime + 133.4048f);
            Player.StartCoroutine(Pattern_12/*BeatShoot*/(launchTime + 133.4048f, blackboard, (c,i,t,l, castc, casti) => "node_86.output_value", (c,i,t,l, castc, casti) => 90f/*out*/));
            if (launchTime + 136.3484 > Game.Music.Time) yield return Wait(launchTime + 136.3484f);
            Player.StartCoroutine(Pattern_33/*Circle*/(launchTime + 136.3484f, blackboard/*out*/));
            if (launchTime + 136.5119 > Game.Music.Time) yield return Wait(launchTime + 136.5119f);
            Player.StartCoroutine(Pattern_31/*Final*/(launchTime + 136.5119f, blackboard/*out*/));
            if (launchTime + 139.6659 > Game.Music.Time) yield return Wait(launchTime + 139.6659f);
            Player.StartCoroutine(Pattern_15/*Boss2*/(launchTime + 139.6659f, blackboard/*out*/));
            if (launchTime + 140.4851 > Game.Music.Time) yield return Wait(launchTime + 140.4851f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 140.4851f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => null/*out*/, 0.2058716f));
            if (launchTime + 168.9354 > Game.Music.Time) yield return Wait(launchTime + 168.9354f);
            Player.StartCoroutine(Pattern_25/*PartN*/(launchTime + 168.9354f, blackboard/*out*/));
            if (launchTime + 168.9426 > Game.Music.Time) yield return Wait(launchTime + 168.9426f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 168.9426f, blackboard, (c,i,t,l, castc, casti) => new Color(0x68/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4756622f));
            if (launchTime + 169.6068 > Game.Music.Time) yield return Wait(launchTime + 169.6068f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 169.6068f, blackboard, (c,i,t,l, castc, casti) => new Color(0x68/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.422821f));
            if (launchTime + 170.2638 > Game.Music.Time) yield return Wait(launchTime + 170.2638f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 170.2638f, blackboard, (c,i,t,l, castc, casti) => new Color(0x68/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4346771f));
            if (launchTime + 170.7254 > Game.Music.Time) yield return Wait(launchTime + 170.7254f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 170.7254f, blackboard, (c,i,t,l, castc, casti) => new Color(0x68/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3438568f));
            if (launchTime + 171.3836 > Game.Music.Time) yield return Wait(launchTime + 171.3836f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 171.3836f, blackboard, (c,i,t,l, castc, casti) => new Color(0x68/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3438568f));
            if (launchTime + 172.0418 > Game.Music.Time) yield return Wait(launchTime + 172.0418f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 172.0418f, blackboard, (c,i,t,l, castc, casti) => new Color(0x68/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4289093f));
            if (launchTime + 172.4596 > Game.Music.Time) yield return Wait(launchTime + 172.4596f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 172.4596f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2657013f));
            if (launchTime + 172.8266 > Game.Music.Time) yield return Wait(launchTime + 172.8266f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 172.8266f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.243988f));
            if (launchTime + 173.154 > Game.Music.Time) yield return Wait(launchTime + 173.154f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 173.154f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.243988f));
            if (launchTime + 174.2706 > Game.Music.Time) yield return Wait(launchTime + 174.2706f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 174.2706f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.243988f));
            if (launchTime + 174.6034 > Game.Music.Time) yield return Wait(launchTime + 174.6034f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 174.6034f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.243988f));
            if (launchTime + 174.9363 > Game.Music.Time) yield return Wait(launchTime + 174.9363f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 174.9363f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.243988f));
            if (launchTime + 175.3785 > Game.Music.Time) yield return Wait(launchTime + 175.3785f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 175.3785f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07154846f));
            if (launchTime + 175.4905 > Game.Music.Time) yield return Wait(launchTime + 175.4905f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 175.4905f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.05841064f));
            if (launchTime + 175.5972 > Game.Music.Time) yield return Wait(launchTime + 175.5972f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 175.5972f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.05841064f));
            if (launchTime + 175.6033 > Game.Music.Time) yield return Wait(launchTime + 175.6033f);
            Player.StartCoroutine(Pattern_14/*BossEncount*/(launchTime + 175.6033f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 175.7231 > Game.Music.Time) yield return Wait(launchTime + 175.7231f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 175.7231f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.05841064f));
            if (launchTime + 175.8182 > Game.Music.Time) yield return Wait(launchTime + 175.8182f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 175.8182f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.05841064f));
            if (launchTime + 175.9364 > Game.Music.Time) yield return Wait(launchTime + 175.9364f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 175.9364f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.05841064f));
            if (launchTime + 176.0405 > Game.Music.Time) yield return Wait(launchTime + 176.0405f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 176.0405f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.3468475f));
            if (launchTime + 176.0638 > Game.Music.Time) yield return Wait(launchTime + 176.0638f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 176.0638f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5d/255f,0x2d/255f,0x00/255f)/*out*/, 3.716415f));
            if (launchTime + 179.8082 > Game.Music.Time) yield return Wait(launchTime + 179.8082f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 179.8082f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x35/255f,0x00/255f)/*out*/, 5.471146f));
            if (launchTime + 185.2944 > Game.Music.Time) yield return Wait(launchTime + 185.2944f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 185.2944f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x00/255f,0x00/255f)/*out*/, 3.14975f));
            if (launchTime + 188.4743 > Game.Music.Time) yield return Wait(launchTime + 188.4743f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 188.4743f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x33/255f,0x54/255f)/*out*/, 5.471085f));
            if (launchTime + 189.8463 > Game.Music.Time) yield return Wait(launchTime + 189.8463f);
            Player.StartCoroutine(Pattern_14/*BossEncount*/(launchTime + 189.8463f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 193.9605 > Game.Music.Time) yield return Wait(launchTime + 193.9605f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 193.9605f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x08/255f,0x26/255f)/*out*/, 4.014099f));
            if (launchTime + 198.014 > Game.Music.Time) yield return Wait(launchTime + 198.014f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 198.014f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x12/255f,0x45/255f,0x00/255f)/*out*/, 4.258118f));
            if (launchTime + 202.3121 > Game.Music.Time) yield return Wait(launchTime + 202.3121f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 202.3121f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 2.166885f));
            if (launchTime + 203.0686 > Game.Music.Time) yield return Wait(launchTime + 203.0686f);
            Player.StartCoroutine(Pattern_20/*Last*/(launchTime + 203.0686f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 203.0686 > Game.Music.Time) yield return Wait(launchTime + 203.0686f);
            Player.StartCoroutine(Pattern_20/*Last*/(launchTime + 203.0686f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 204.5128 > Game.Music.Time) yield return Wait(launchTime + 204.5128f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 204.5128f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2632599f));
            if (launchTime + 216.6243 > Game.Music.Time) yield return Wait(launchTime + 216.6243f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 216.6243f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 216.8383 > Game.Music.Time) yield return Wait(launchTime + 216.8383f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 216.8383f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 217.0491 > Game.Music.Time) yield return Wait(launchTime + 217.0491f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 217.0491f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 217.2745 > Game.Music.Time) yield return Wait(launchTime + 217.2745f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 217.2745f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 217.5175 > Game.Music.Time) yield return Wait(launchTime + 217.5175f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 217.5175f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 217.7312 > Game.Music.Time) yield return Wait(launchTime + 217.7312f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 217.7312f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 217.9508 > Game.Music.Time) yield return Wait(launchTime + 217.9508f);
            Player.StartCoroutine(Pattern_3/*BeatAtacker*/(launchTime + 217.9508f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard, InputFunc flipy_input, InputFunc flipx_input, InputFunc flipvariants_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipy_input, "node_0.output_patterninput");
            object flipy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);

            object portal;
            blackboard.SubscribeForChanges("node_8.output_value", (b, o) => portal = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => EnemyImage.EnemyTeleport, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(0f,-300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.2573647 > Game.Music.Time) yield return Wait(launchTime + 0.2573647f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.2573647f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-154f*(float)flipx*(float)ar,-170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.3527231f));
            if (launchTime + 0.7117672 > Game.Music.Time) yield return Wait(launchTime + 0.7117672f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.7117672f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-225f*(float)flipx*(float)ar,-170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.4026303f));
            if (launchTime + 1.145839 > Game.Music.Time) yield return Wait(launchTime + 1.145839f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 1.145839f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy9/*out*/, "node_7.output_enemy"));
            if (launchTime + 1.951292 > Game.Music.Time) yield return Wait(launchTime + 1.951292f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.951292f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+((float)toint((float)i,3f)*50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)"));
            if (launchTime + 2.298725 > Game.Music.Time) yield return Wait(launchTime + 2.298725f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.298725f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+((float)toint((float)i,3f)*50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_bullet(s)"));
            if (launchTime + 2.713689 > Game.Music.Time) yield return Wait(launchTime + 2.713689f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.713689f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy", (c,i,t,l, castc, casti) => 6f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+((float)toint((float)i,3f)*50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_bullet(s)"));
            if (launchTime + 2.933724 > Game.Music.Time) yield return Wait(launchTime + 2.933724f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 2.933724f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_12.output_enemy"));
            if (launchTime + 3.829528 > Game.Music.Time) yield return Wait(launchTime + 3.829528f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 3.829528f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(250f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_bullet(s)"));
            if (launchTime + 4.265031 > Game.Music.Time) yield return Wait(launchTime + 4.265031f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 4.265031f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_bullet(s)", (c,i,t,l, castc, casti) => 0.07f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_14.output_bullet(s)", 0.4498119f));
            if (launchTime + 4.714321 > Game.Music.Time) yield return Wait(launchTime + 4.714321f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 4.714321f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 4.720036 > Game.Music.Time) yield return Wait(launchTime + 4.720036f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 4.720036f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => EnemyImage.Enemy3, (c,i,t,l, castc, casti) => 600f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_16.output_enemy(ies)"));
            if (launchTime + 4.727417 > Game.Music.Time) yield return Wait(launchTime + 4.727417f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.727417f, blackboard, (c,i,t,l, castc, casti) => new Color(0x58/255f,0x54/255f,0x52/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1433525f));
            if (launchTime + 4.929028 > Game.Music.Time) yield return Wait(launchTime + 4.929028f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 4.929028f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((float)rndf(-20f,20f)*(float)flipx,38f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_18.output_enemy(ies)", 0.05919838f));
            if (launchTime + 5.033252 > Game.Music.Time) yield return Wait(launchTime + 5.033252f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.033252f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((float)rndf(-20f,20f)*(float)flipx,38f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_19.output_enemy(ies)", 0.06291771f));
            if (launchTime + 5.159081 > Game.Music.Time) yield return Wait(launchTime + 5.159081f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.159081f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((float)rndf(-20f,20f)*(float)flipx,38f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_20.output_enemy(ies)", 0.06291771f));
            if (launchTime + 5.26003 > Game.Music.Time) yield return Wait(launchTime + 5.26003f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.26003f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((float)rndf(-20f,20f)*(float)flipx,38f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_21.output_enemy(ies)", 0.06291771f));
            if (launchTime + 5.374802 > Game.Music.Time) yield return Wait(launchTime + 5.374802f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.374802f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((float)rndf(-20f,20f)*(float)flipx,38f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_22.output_enemy(ies)", 0.06291771f));
            if (launchTime + 5.474366 > Game.Music.Time) yield return Wait(launchTime + 5.474366f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.474366f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((float)rndf(-20f,20f)*(float)flipx,38f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_23.output_enemy(ies)", 0.06291771f));
            if (launchTime + 5.596051 > Game.Music.Time) yield return Wait(launchTime + 5.596051f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.596051f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-156f*(float)flipx,161f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_24.output_enemy(ies)", 0.09198952f));
            if (launchTime + 5.697741 > Game.Music.Time) yield return Wait(launchTime + 5.697741f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.697741f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-132f*(float)flipx,136f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_25.output_enemy(ies)", 0.09273338f));
            if (launchTime + 5.813158 > Game.Music.Time) yield return Wait(launchTime + 5.813158f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.813158f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-44f*(float)flipx,123f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_26.output_enemy(ies)", 0.09198761f));
            if (launchTime + 5.930004 > Game.Music.Time) yield return Wait(launchTime + 5.930004f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.930004f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-271f*(float)flipx,217f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_27.output_enemy(ies)", 0.107481f));
            if (launchTime + 6.115844 > Game.Music.Time) yield return Wait(launchTime + 6.115844f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.115844f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 180f*((float)flipx/2f-0.5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*20f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_28.output_bullet(s)"));
            if (launchTime + 6.143463 > Game.Music.Time) yield return Wait(launchTime + 6.143463f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.143463f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_29.output_enemy(ies)", 0.0509758f));
            if (launchTime + 6.19532 > Game.Music.Time) yield return Wait(launchTime + 6.19532f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.19532f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 180f*((float)flipx/2f-0.5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*20f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_30.output_bullet(s)"));
            if (launchTime + 6.219649 > Game.Music.Time) yield return Wait(launchTime + 6.219649f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.219649f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_31.output_enemy(ies)", 0.05097389f));
            if (launchTime + 6.273831 > Game.Music.Time) yield return Wait(launchTime + 6.273831f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.273831f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 180f*((float)flipx/2f-0.5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*20f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_32.output_bullet(s)"));
            if (launchTime + 6.292467 > Game.Music.Time) yield return Wait(launchTime + 6.292467f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.292467f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_33.output_enemy(ies)", 0.0509758f));
            if (launchTime + 6.344056 > Game.Music.Time) yield return Wait(launchTime + 6.344056f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.344056f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 180f*((float)flipx/2f-0.5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*20f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_34.output_bullet(s)"));
            if (launchTime + 6.363047 > Game.Music.Time) yield return Wait(launchTime + 6.363047f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.363047f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_35.output_enemy(ies)", 0.05097389f));
            if (launchTime + 6.415863 > Game.Music.Time) yield return Wait(launchTime + 6.415863f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.415863f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 180f*((float)flipx/2f-0.5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*20f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_36.output_bullet(s)"));
            if (launchTime + 6.439228 > Game.Music.Time) yield return Wait(launchTime + 6.439228f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.439228f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_37.output_enemy(ies)", 0.0509758f));
            if (launchTime + 6.489169 > Game.Music.Time) yield return Wait(launchTime + 6.489169f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 6.489169f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_38.output_enemy"));
            if (launchTime + 6.49301 > Game.Music.Time) yield return Wait(launchTime + 6.49301f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.49301f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 180f*((float)flipx/2f-0.5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*20f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_39.output_bullet(s)"));
            if (launchTime + 6.501967 > Game.Music.Time) yield return Wait(launchTime + 6.501967f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.501967f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-182f*(float)flipx,-335f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_40.output_enemy(ies)", 0.0509758f));
            if (launchTime + 6.619603 > Game.Music.Time) yield return Wait(launchTime + 6.619603f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 6.619603f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 7.301772 > Game.Music.Time) yield return Wait(launchTime + 7.301772f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 7.301772f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+((float)toint((float)i,3f)*30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_42.output_bullet(s)"));
            if (launchTime + 7.623762 > Game.Music.Time) yield return Wait(launchTime + 7.623762f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 7.623762f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+((float)toint((float)i,3f)*30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_43.output_bullet(s)"));
            if (launchTime + 7.828163 > Game.Music.Time) yield return Wait(launchTime + 7.828163f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 7.828163f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_enemy", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)sint((float)i/(float)c*5f)*50f+350f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x38/255f,0xcf/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_44.output_bullet(s)"));
            if (launchTime + 8.052938 > Game.Music.Time) yield return Wait(launchTime + 8.052938f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 8.052938f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+((float)toint((float)i,3f)*30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_45.output_bullet(s)"));
            if (launchTime + 8.272243 > Game.Music.Time) yield return Wait(launchTime + 8.272243f);
            Player.StartCoroutine(Pattern_9/*ShakeMonster*/(launchTime + 8.272243f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value"/*out*/));
            if (launchTime + 10.05259 > Game.Music.Time) yield return Wait(launchTime + 10.05259f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 10.05259f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_47.output_enemy"));
            if (launchTime + 10.93726 > Game.Music.Time) yield return Wait(launchTime + 10.93726f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 10.93726f, blackboard, (c,i,t,l, castc, casti) => "node_47.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(250f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_48.output_bullet(s)"));
            if (launchTime + 11.39366 > Game.Music.Time) yield return Wait(launchTime + 11.39366f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 11.39366f, blackboard, (c,i,t,l, castc, casti) => "node_48.output_bullet(s)", (c,i,t,l, castc, casti) => 0.07f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_49.output_bullet(s)", 0.1423111f));
            if (launchTime + 11.60817 > Game.Music.Time) yield return Wait(launchTime + 11.60817f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 11.60817f, blackboard, (c,i,t,l, castc, casti) => "node_48.output_bullet(s)", (c,i,t,l, castc, casti) => 0.07f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_50.output_bullet(s)", 0.2168961f));
            if (launchTime + 11.83307 > Game.Music.Time) yield return Wait(launchTime + 11.83307f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 11.83307f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_51.output_enemy"));
            if (launchTime + 11.83508 > Game.Music.Time) yield return Wait(launchTime + 11.83508f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 11.83508f, blackboard, (c,i,t,l, castc, casti) => "node_48.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 13.15647 > Game.Music.Time) yield return Wait(launchTime + 13.15647f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 13.15647f, blackboard, (c,i,t,l, castc, casti) => "node_51.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(237f*(float)flipx,421f), (c,i,t,l, castc, casti) => new Vector2(0f,-3004f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_53.output_bullet(s)"));
            if (launchTime + 13.60326 > Game.Music.Time) yield return Wait(launchTime + 13.60326f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 13.60326f, blackboard, (c,i,t,l, castc, casti) => "node_53.output_bullet(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => 0.314552f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_54.output_laser(s)"));
            if (launchTime + 13.61592 > Game.Music.Time) yield return Wait(launchTime + 13.61592f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 13.61592f, blackboard, (c,i,t,l, castc, casti) => "node_53.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 13.93106 > Game.Music.Time) yield return Wait(launchTime + 13.93106f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 13.93106f, blackboard, (c,i,t,l, castc, casti) => "node_54.output_laser(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f*(float)rndi(-1f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => (float)rndf(25f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => 0.1105634f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_56.output_laser(s)"));
            if (launchTime + 14.05528 > Game.Music.Time) yield return Wait(launchTime + 14.05528f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 14.05528f, blackboard, (c,i,t,l, castc, casti) => "node_56.output_laser(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f*(float)rndi(-1f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => (float)rndf(25f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => 0.1958531f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_57.output_laser(s)"));
            if (launchTime + 14.27216 > Game.Music.Time) yield return Wait(launchTime + 14.27216f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 14.27216f, blackboard, (c,i,t,l, castc, casti) => "node_57.output_laser(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f*(float)rndi(-1f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => (float)rndf(25f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => 0.1027826f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_58.output_laser(s)"));
            if (launchTime + 14.39007 > Game.Music.Time) yield return Wait(launchTime + 14.39007f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 14.39007f, blackboard, (c,i,t,l, castc, casti) => "node_58.output_laser(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f*(float)rndi(-1f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => (float)rndf(25f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => 0.1981931f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_59.output_laser(s)"));
            if (launchTime + 14.59767 > Game.Music.Time) yield return Wait(launchTime + 14.59767f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 14.59767f, blackboard, (c,i,t,l, castc, casti) => "node_59.output_laser(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f*(float)rndi(-1f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => (float)rndf(25f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => 0.1105921f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_60.output_laser(s)"));
            if (launchTime + 14.72276 > Game.Music.Time) yield return Wait(launchTime + 14.72276f);
            Player.StartCoroutine(CreateLaser.Act(launchTime + 14.72276f, blackboard, (c,i,t,l, castc, casti) => "node_60.output_laser(s)", (c,i,t,l, castc, casti) => false, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f*(float)rndi(-1f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => (float)rndf(25f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => 0.1931229f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null/*out*/, "node_61.output_laser(s)"));
            if (launchTime + 14.92731 > Game.Music.Time) yield return Wait(launchTime + 14.92731f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 14.92731f, blackboard, (c,i,t,l, castc, casti) => "node_61.output_laser(s)", (c,i,t,l, castc, casti) => (float)d+3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_62.output_bullet(s)"));
            if (launchTime + 15.38386 > Game.Music.Time) yield return Wait(launchTime + 15.38386f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 15.38386f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_63.output_enemy"));
            if (launchTime + 16.93757 > Game.Music.Time) yield return Wait(launchTime + 16.93757f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 16.93757f, blackboard, (c,i,t,l, castc, casti) => "node_63.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+((float)toint((float)i,3f)*50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_64.output_bullet(s)"));
            if (launchTime + 17.15756 > Game.Music.Time) yield return Wait(launchTime + 17.15756f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 17.15756f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_65.output_enemy"));
            if (launchTime + 18.04114 > Game.Music.Time) yield return Wait(launchTime + 18.04114f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 18.04114f, blackboard, (c,i,t,l, castc, casti) => "node_65.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(250f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_66.output_bullet(s)"));
            if (launchTime + 18.48508 > Game.Music.Time) yield return Wait(launchTime + 18.48508f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 18.48508f, blackboard, (c,i,t,l, castc, casti) => "node_66.output_bullet(s)", (c,i,t,l, castc, casti) => 0.07f, (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_67.output_bullet(s)", 0.2783432f));
            if (launchTime + 18.9388 > Game.Music.Time) yield return Wait(launchTime + 18.9388f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 18.9388f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_68.output_enemy"));
            if (launchTime + 19.8227 > Game.Music.Time) yield return Wait(launchTime + 19.8227f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 19.8227f, blackboard, (c,i,t,l, castc, casti) => "node_68.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_69.output_bullet(s)"));
            if (launchTime + 19.9267 > Game.Music.Time) yield return Wait(launchTime + 19.9267f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 19.9267f, blackboard, (c,i,t,l, castc, casti) => "node_68.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_70.output_bullet(s)"));
            if (launchTime + 20.04338 > Game.Music.Time) yield return Wait(launchTime + 20.04338f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 20.04338f, blackboard, (c,i,t,l, castc, casti) => "node_68.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_71.output_bullet(s)"));
            if (launchTime + 20.16259 > Game.Music.Time) yield return Wait(launchTime + 20.16259f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 20.16259f, blackboard, (c,i,t,l, castc, casti) => "node_68.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_72.output_bullet(s)"));
            if (launchTime + 20.70478 > Game.Music.Time) yield return Wait(launchTime + 20.70478f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 20.70478f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_73.output_enemy"));
            if (launchTime + 22.04426 > Game.Music.Time) yield return Wait(launchTime + 22.04426f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 22.04426f, blackboard, (c,i,t,l, castc, casti) => "node_73.output_enemy", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)sint((float)i/(float)c*5f)*50f+350f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x38/255f,0xcf/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_74.output_bullet(s)"));
            if (launchTime + 22.2803 > Game.Music.Time) yield return Wait(launchTime + 22.2803f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 22.2803f, blackboard, (c,i,t,l, castc, casti) => "node_73.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+((float)toint((float)i,3f)*30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_75.output_bullet(s)"));
            if (launchTime + 22.48784 > Game.Music.Time) yield return Wait(launchTime + 22.48784f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 22.48784f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_76.output_enemy"));
            if (launchTime + 23.8065 > Game.Music.Time) yield return Wait(launchTime + 23.8065f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 23.8065f, blackboard, (c,i,t,l, castc, casti) => "node_76.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f/(float)c*(float)i+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xd9/255f,0x6c/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_77.output_bullet(s)"));
            if (launchTime + 24.04272 > Game.Music.Time) yield return Wait(launchTime + 24.04272f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 24.04272f, blackboard, (c,i,t,l, castc, casti) => "node_76.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx-45f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f/(float)c*(float)i+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_78.output_bullet(s)"));
            if (launchTime + 24.11128 > Game.Music.Time) yield return Wait(launchTime + 24.11128f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 24.11128f, blackboard, (c,i,t,l, castc, casti) => "node_76.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx-15f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f/(float)c*(float)i+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_79.output_bullet(s)"));
            if (launchTime + 24.19598 > Game.Music.Time) yield return Wait(launchTime + 24.19598f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 24.19598f, blackboard, (c,i,t,l, castc, casti) => "node_76.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx+15f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f/(float)c*(float)i+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_80.output_bullet(s)"));
            if (launchTime + 24.25953 > Game.Music.Time) yield return Wait(launchTime + 24.25953f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 24.25953f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_81.output_enemy"));
            if (launchTime + 24.26454 > Game.Music.Time) yield return Wait(launchTime + 24.26454f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 24.26454f, blackboard, (c,i,t,l, castc, casti) => "node_76.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx+45f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f/(float)c*(float)i+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_82.output_bullet(s)"));
            if (launchTime + 25.15395 > Game.Music.Time) yield return Wait(launchTime + 25.15395f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 25.15395f, blackboard, (c,i,t,l, castc, casti) => "node_81.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(250f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_83.output_bullet(s)"));
            if (launchTime + 25.59505 > Game.Music.Time) yield return Wait(launchTime + 25.59505f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 25.59505f, blackboard, (c,i,t,l, castc, casti) => "node_83.output_bullet(s)", (c,i,t,l, castc, casti) => 0.05f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_84.output_bullet(s)", 0.1760788f));
            if (launchTime + 25.82881 > Game.Music.Time) yield return Wait(launchTime + 25.82881f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 25.82881f, blackboard, (c,i,t,l, castc, casti) => "node_83.output_bullet(s)", (c,i,t,l, castc, casti) => 0.05f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_85.output_bullet(s)", 0.2068348f));
            if (launchTime + 26.05445 > Game.Music.Time) yield return Wait(launchTime + 26.05445f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 26.05445f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_86.output_enemy"));
            if (launchTime + 26.06152 > Game.Music.Time) yield return Wait(launchTime + 26.06152f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 26.06152f, blackboard, (c,i,t,l, castc, casti) => "node_83.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 27.38127 > Game.Music.Time) yield return Wait(launchTime + 27.38127f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 27.38127f, blackboard, (c,i,t,l, castc, casti) => "node_86.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(237f*(float)flipx,421f), (c,i,t,l, castc, casti) => new Vector2(0f,-3004f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_88.output_bullet(s)"));
            if (launchTime + 29.39719 > Game.Music.Time) yield return Wait(launchTime + 29.39719f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 29.39719f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc portal_input, InputFunc enemyimage_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, portal_input, "node_1.output_patterninput");
            object portal = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => portal = o);

            SubscribeInput(outboard, blackboard, enemyimage_input, "node_2.output_patterninput");
            object enemyimage = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemyimage = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_3.output_enemy(ies)");
            object path = blackboard.GetValue((c, i, t, l, castc, casti) => new MyPath(0f,0f,-30f*(float)ar,-30f,0f,-60f));
            blackboard.SetValue("node_9.output_path", path);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.005340576 > Game.Music.Time) yield return Wait(launchTime + 0.005340576f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.005340576f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.1679382f));
            if (launchTime + 0.005355835 > Game.Music.Time) yield return Wait(launchTime + 0.005355835f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.005355835f, blackboard, (c,i,t,l, castc, casti) => new Color(0x58/255f,0x54/255f,0x52/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1636047f));
            if (launchTime + 0.2296753 > Game.Music.Time) yield return Wait(launchTime + 0.2296753f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.2296753f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_7.output_enemy(ies)", 0.05586243f));
            if (launchTime + 0.3390655 > Game.Music.Time) yield return Wait(launchTime + 0.3390655f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.3390655f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_7.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_8.output_enemy(ies)", 0.05108643f));
            if (launchTime + 0.4557495 > Game.Music.Time) yield return Wait(launchTime + 0.4557495f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.4557495f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_10.output_enemy(ies)", 0.06085205f));
            if (launchTime + 0.5653991 > Game.Music.Time) yield return Wait(launchTime + 0.5653991f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.5653991f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_10.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_11.output_enemy(ies)", 0.05024719f));
            if (launchTime + 0.6694488 > Game.Music.Time) yield return Wait(launchTime + 0.6694488f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.6694488f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_11.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_12.output_enemy(ies)", 0.05044556f));
            if (launchTime + 0.7798919 > Game.Music.Time) yield return Wait(launchTime + 0.7798919f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.7798919f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_9.output_path", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,-0.8f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_13.output_enemy(ies)", 0.04289246f));
            if (launchTime + 1.808625 > Game.Music.Time) yield return Wait(launchTime + 1.808625f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.808625f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-376f*(float)flipx*(float)ar,283f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_15.output_enemy(ies)", 0.3267517f));
            if (launchTime + 2.312653 > Game.Music.Time) yield return Wait(launchTime + 2.312653f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.312653f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy9, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-107f*(float)flipx*(float)ar,220f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.214798 > Game.Music.Time) yield return Wait(launchTime + 0.214798f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.214798f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-107f*(float)flipx*(float)ar,160f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutElastic/*out*/, "node_2.output_enemy(ies)", 0.5236664f));
            if (launchTime + 0.3195801 > Game.Music.Time) yield return Wait(launchTime + 0.3195801f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3195801f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5f/255f,0x61/255f,0x06/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1877899f));
            if (launchTime + 0.3207245 > Game.Music.Time) yield return Wait(launchTime + 0.3207245f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.3207245f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*(float)d+1f, (c,i,t,l, castc, casti) => ((float)i-(float)c/2f+0.5f)*45f/(float)c, (c,i,t,l, castc, casti) => 40f, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => ((float)i-(float)c/2f+0.5f)*90f/(float)c, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => -100f, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.751175 > Game.Music.Time) yield return Wait(launchTime + 0.751175f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.751175f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-355f*(float)flipx*(float)ar,284f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.4242249f));
            if (launchTime + 1.19722 > Game.Music.Time) yield return Wait(launchTime + 1.19722f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.19722f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard, InputFunc path_input, InputFunc pathposition_input, InputFunc pathscale_input, InputFunc pathrotation_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, path_input, "node_0.output_patterninput");
            object path = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => path = o);

            SubscribeInput(outboard, blackboard, pathposition_input, "node_1.output_patterninput");
            object pathposition = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => pathposition = o);

            SubscribeInput(outboard, blackboard, pathscale_input, "node_2.output_patterninput");
            object pathscale = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => pathscale = o);

            SubscribeInput(outboard, blackboard, pathrotation_input, "node_3.output_patterninput");
            object pathrotation = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => pathrotation = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_9.output_value");
            object enemyvar;
            blackboard.SubscribeForChanges("node_9.output_value", (b, o) => enemyvar = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_enem", (c,i,t,l, castc, casti) => "node_6.output_enem", (c,i,t,l, castc, casti) => "node_7.output_enem", (c,i,t,l, castc, casti) => "node_8.output_enem", (c,i,t,l, castc, casti) => "node_10.output_enem", (c,i,t,l, castc, casti) => "node_11.output_enem", (c,i,t,l, castc, casti) => "node_13.output_enem", (c,i,t,l, castc, casti) => "node_14.output_enem"/*out*/, "node_9.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_4.output_enem"));
            if (launchTime + 3.55494 > Game.Music.Time) yield return Wait(launchTime + 3.55494f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 3.55494f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_6.output_enem"));
            if (launchTime + 7.111977 > Game.Music.Time) yield return Wait(launchTime + 7.111977f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 7.111977f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_7.output_enem"));
            if (launchTime + 10.66713 > Game.Music.Time) yield return Wait(launchTime + 10.66713f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 10.66713f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_8.output_enem"));
            if (launchTime + 14.21085 > Game.Music.Time) yield return Wait(launchTime + 14.21085f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 14.21085f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_10.output_enem"));
            if (launchTime + 17.77191 > Game.Music.Time) yield return Wait(launchTime + 17.77191f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 17.77191f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_11.output_enem"));
            if (launchTime + 21.33105 > Game.Music.Time) yield return Wait(launchTime + 21.33105f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 21.33105f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_13.output_enem"));
            if (launchTime + 24.89575 > Game.Music.Time) yield return Wait(launchTime + 24.89575f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 24.89575f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_14.output_enem"));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard, InputFunc path_input, InputFunc pathposition_input, InputFunc pathscale_input, InputFunc pathrotation_input, string enem_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, path_input, "node_0.output_patterninput");
            object path = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => path = o);

            SubscribeInput(outboard, blackboard, pathposition_input, "node_1.output_patterninput");
            object pathposition = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => pathposition = o);

            SubscribeInput(outboard, blackboard, pathscale_input, "node_2.output_patterninput");
            object pathscale = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => pathscale = o);

            SubscribeInput(outboard, blackboard, pathrotation_input, "node_4.output_patterninput");
            object pathrotation = blackboard.GetValue("node_4.output_patterninput");
            blackboard.SubscribeForChanges("node_4.output_patterninput", (b, o) => pathrotation = o);

            SubscribeOutput(outboard, blackboard, enem_output, "node_7.output_value");
            object variable;
            blackboard.SubscribeForChanges("node_7.output_value", (b, o) => variable = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy", (c,i,t,l, castc, casti) => "node_6.output_enemy", (c,i,t,l, castc, casti) => "node_8.output_enemy", (c,i,t,l, castc, casti) => "node_9.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_6/*Enemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/, "node_3.output_enemy"));
            if (launchTime + 0.886322 > Game.Music.Time) yield return Wait(launchTime + 0.886322f);
            Player.StartCoroutine(Pattern_6/*Enemy*/(launchTime + 0.886322f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/, "node_6.output_enemy"));
            if (launchTime + 1.778687 > Game.Music.Time) yield return Wait(launchTime + 1.778687f);
            Player.StartCoroutine(Pattern_6/*Enemy*/(launchTime + 1.778687f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/, "node_8.output_enemy"));
            if (launchTime + 2.671051 > Game.Music.Time) yield return Wait(launchTime + 2.671051f);
            Player.StartCoroutine(Pattern_6/*Enemy*/(launchTime + 2.671051f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/, "node_9.output_enemy"));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard, InputFunc path_input, InputFunc pathposition_input, InputFunc pathscale_input, InputFunc pathrotation_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, path_input, "node_0.output_patterninput");
            object path = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => path = o);

            SubscribeInput(outboard, blackboard, pathposition_input, "node_1.output_patterninput");
            object pathposition = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => pathposition = o);

            SubscribeInput(outboard, blackboard, pathscale_input, "node_2.output_patterninput");
            object pathscale = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => pathscale = o);

            SubscribeInput(outboard, blackboard, pathrotation_input, "node_3.output_patterninput");
            object pathrotation = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => pathrotation = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_4.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(5f,407f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_enemy(ies)"));
            if (launchTime + 0.01249701 > Game.Music.Time) yield return Wait(launchTime + 0.01249701f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.01249701f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_5.output_enemy(ies)", 0.01139832f));
            if (launchTime + 0.1501618 > Game.Music.Time) yield return Wait(launchTime + 0.1501618f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.1501618f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_6.output_enemy(ies)", 0.2415466f));
            if (launchTime + 0.5931549 > Game.Music.Time) yield return Wait(launchTime + 0.5931549f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.5931549f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_8.output_enemy(ies)", 0.2523804f));
            if (launchTime + 1.037872 > Game.Music.Time) yield return Wait(launchTime + 1.037872f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.037872f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_10.output_enemy(ies)", 0.233017f));
            if (launchTime + 1.494461 > Game.Music.Time) yield return Wait(launchTime + 1.494461f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.494461f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => 8f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_11.output_enemy(ies)", 0.2312927f));
            if (launchTime + 1.926438 > Game.Music.Time) yield return Wait(launchTime + 1.926438f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.926438f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 8f, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_12.output_enemy(ies)", 0.2792511f));
            if (launchTime + 2.388748 > Game.Music.Time) yield return Wait(launchTime + 2.388748f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 2.388748f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_13.output_enemy(ies)", 0.27771f));
            if (launchTime + 2.83432 > Game.Music.Time) yield return Wait(launchTime + 2.83432f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 2.83432f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => 14f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_14.output_enemy(ies)", 0.27771f));
            if (launchTime + 3.279862 > Game.Music.Time) yield return Wait(launchTime + 3.279862f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 3.279862f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 14f, (c,i,t,l, castc, casti) => 16f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_15.output_enemy(ies)", 0.2777252f));
            if (launchTime + 3.731552 > Game.Music.Time) yield return Wait(launchTime + 3.731552f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 3.731552f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 16f, (c,i,t,l, castc, casti) => 18f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutSine/*out*/, "node_16.output_enemy(ies)", 0.27771f));
            if (launchTime + 4.03537 > Game.Music.Time) yield return Wait(launchTime + 4.03537f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 4.03537f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard, InputFunc path_input, InputFunc pathposition_input, InputFunc pathscale_input, InputFunc pathrotation_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, path_input, "node_0.output_patterninput");
            object path = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => path = o);

            SubscribeInput(outboard, blackboard, pathposition_input, "node_1.output_patterninput");
            object pathposition = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => pathposition = o);

            SubscribeInput(outboard, blackboard, pathscale_input, "node_2.output_patterninput");
            object pathscale = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => pathscale = o);

            SubscribeInput(outboard, blackboard, pathrotation_input, "node_3.output_patterninput");
            object pathrotation = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => pathrotation = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_6.output_value");
            object enemy;
            blackboard.SubscribeForChanges("node_6.output_value", (b, o) => enemy = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_enem", (c,i,t,l, castc, casti) => "node_8.output_enem", (c,i,t,l, castc, casti) => "node_9.output_enem", (c,i,t,l, castc, casti) => "node_10.output_enem", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_4.output_enem"));
            if (launchTime + 3.566124 > Game.Music.Time) yield return Wait(launchTime + 3.566124f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 3.566124f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_8.output_enem"));
            if (launchTime + 7.114235 > Game.Music.Time) yield return Wait(launchTime + 7.114235f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 7.114235f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_9.output_enem"));
            if (launchTime + 10.66951 > Game.Music.Time) yield return Wait(launchTime + 10.66951f);
            Player.StartCoroutine(Pattern_5/*4Enemies*/(launchTime + 10.66951f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_10.output_enem"));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 0.1847758 > Game.Music.Time) yield return Wait(launchTime + 0.1847758f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.1847758f, blackboard, (c,i,t,l, castc, casti) => new Color(0x61/255f,0x63/255f,0x65/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.6718969f));
            if (launchTime + 1.063073 > Game.Music.Time) yield return Wait(launchTime + 1.063073f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.063073f, blackboard, (c,i,t,l, castc, casti) => new Color(0x61/255f,0x63/255f,0x65/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.6665759f));
            if (launchTime + 1.949355 > Game.Music.Time) yield return Wait(launchTime + 1.949355f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.949355f, blackboard, (c,i,t,l, castc, casti) => new Color(0x61/255f,0x63/255f,0x65/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.6745584f));
            if (launchTime + 2.843621 > Game.Music.Time) yield return Wait(launchTime + 2.843621f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.843621f, blackboard, (c,i,t,l, castc, casti) => new Color(0x61/255f,0x63/255f,0x65/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4430084f));
            if (launchTime + 3.513399 > Game.Music.Time) yield return Wait(launchTime + 3.513399f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 3.513399f, blackboard, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4736969f));
            if (launchTime + 6.163913 > Game.Music.Time) yield return Wait(launchTime + 6.163913f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 6.163913f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1958365f));
            if (launchTime + 6.170129 > Game.Music.Time) yield return Wait(launchTime + 6.170129f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 6.170129f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 6.624807 > Game.Music.Time) yield return Wait(launchTime + 6.624807f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 6.624807f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2195697f));
            if (launchTime + 7.833618 > Game.Music.Time) yield return Wait(launchTime + 7.833618f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.833618f, blackboard, (c,i,t,l, castc, casti) => new Color(0x65/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1351628f));
            if (launchTime + 8.186588 > Game.Music.Time) yield return Wait(launchTime + 8.186588f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.186588f, blackboard, (c,i,t,l, castc, casti) => new Color(0x65/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1275787f));
            if (launchTime + 8.610236 > Game.Music.Time) yield return Wait(launchTime + 8.610236f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.610236f, blackboard, (c,i,t,l, castc, casti) => new Color(0x65/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2440004f));
            if (launchTime + 10.1857 > Game.Music.Time) yield return Wait(launchTime + 10.1857f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.1857f, blackboard, (c,i,t,l, castc, casti) => new Color(0x63/255f,0x31/255f,0x1d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4409065f));
            if (launchTime + 11.95159 > Game.Music.Time) yield return Wait(launchTime + 11.95159f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.95159f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3a/255f,0x70/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.0668993f));
            if (launchTime + 12.04419 > Game.Music.Time) yield return Wait(launchTime + 12.04419f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.04419f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3a/255f,0x70/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05899429f));
            if (launchTime + 12.11817 > Game.Music.Time) yield return Wait(launchTime + 12.11817f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.11817f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3a/255f,0x70/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05899334f));
            if (launchTime + 12.19216 > Game.Music.Time) yield return Wait(launchTime + 12.19216f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.19216f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3a/255f,0x70/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05899334f));
            if (launchTime + 12.26095 > Game.Music.Time) yield return Wait(launchTime + 12.26095f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.26095f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3a/255f,0x70/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05899334f));
            if (launchTime + 12.34662 > Game.Music.Time) yield return Wait(launchTime + 12.34662f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.34662f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3a/255f,0x70/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05899334f));
            if (launchTime + 13.16479 > Game.Music.Time) yield return Wait(launchTime + 13.16479f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.16479f, blackboard, (c,i,t,l, castc, casti) => new Color(0x58/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1338406f));
            if (launchTime + 13.51024 > Game.Music.Time) yield return Wait(launchTime + 13.51024f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.51024f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5d/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1495743f));
            if (launchTime + 13.72626 > Game.Music.Time) yield return Wait(launchTime + 13.72626f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.72626f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x49/255f,0x81/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2003698f));
            if (launchTime + 13.94049 > Game.Music.Time) yield return Wait(launchTime + 13.94049f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.94049f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4b/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2279367f));
            if (launchTime + 14.39023 > Game.Music.Time) yield return Wait(launchTime + 14.39023f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.39023f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.08538914f));
            if (launchTime + 14.49485 > Game.Music.Time) yield return Wait(launchTime + 14.49485f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.49485f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09018898f));
            if (launchTime + 14.60193 > Game.Music.Time) yield return Wait(launchTime + 14.60193f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.60193f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09531879f));
            if (launchTime + 14.73304 > Game.Music.Time) yield return Wait(launchTime + 14.73304f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.73304f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09601688f));
            if (launchTime + 14.83233 > Game.Music.Time) yield return Wait(launchTime + 14.83233f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.83233f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09601688f));
            if (launchTime + 14.94161 > Game.Music.Time) yield return Wait(launchTime + 14.94161f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.94161f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07535458f));
            if (launchTime + 15.06919 > Game.Music.Time) yield return Wait(launchTime + 15.06919f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.06919f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07342052f));
            if (launchTime + 15.17257 > Game.Music.Time) yield return Wait(launchTime + 15.17257f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.17257f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07342052f));
            if (launchTime + 15.39227 > Game.Music.Time) yield return Wait(launchTime + 15.39227f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.39227f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07342052f));
            if (launchTime + 15.5005 > Game.Music.Time) yield return Wait(launchTime + 15.5005f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.5005f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07342052f));
            if (launchTime + 15.60712 > Game.Music.Time) yield return Wait(launchTime + 15.60712f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.60712f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07342052f));
            if (launchTime + 15.7194 > Game.Music.Time) yield return Wait(launchTime + 15.7194f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.7194f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.03514194f));
            if (launchTime + 15.79523 > Game.Music.Time) yield return Wait(launchTime + 15.79523f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.79523f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.03549767f));
            if (launchTime + 15.87021 > Game.Music.Time) yield return Wait(launchTime + 15.87021f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.87021f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.03549671f));
            if (launchTime + 15.94831 > Game.Music.Time) yield return Wait(launchTime + 15.94831f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.94831f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x37/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.03549767f));
            if (launchTime + 17.26992 > Game.Music.Time) yield return Wait(launchTime + 17.26992f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.26992f, blackboard, (c,i,t,l, castc, casti) => new Color(0x56/255f,0x31/255f,0x17/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1942024f));
            if (launchTime + 17.515 > Game.Music.Time) yield return Wait(launchTime + 17.515f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.515f, blackboard, (c,i,t,l, castc, casti) => new Color(0x56/255f,0x31/255f,0x17/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2421532f));
            if (launchTime + 19.0441 > Game.Music.Time) yield return Wait(launchTime + 19.0441f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 19.0441f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2421532f));
            if (launchTime + 20.84371 > Game.Music.Time) yield return Wait(launchTime + 20.84371f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.84371f, blackboard, (c,i,t,l, castc, casti) => new Color(0x65/255f,0x65/255f,0x68/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4209652f));
            if (launchTime + 22.82146 > Game.Music.Time) yield return Wait(launchTime + 22.82146f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 22.82146f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5f/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2388973f));
            if (launchTime + 24.38885 > Game.Music.Time) yield return Wait(launchTime + 24.38885f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 24.38885f, blackboard, (c,i,t,l, castc, casti) => new Color(0x58/255f,0x34/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2152214f));
            if (launchTime + 25.7201 > Game.Music.Time) yield return Wait(launchTime + 25.7201f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.7201f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x45/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.08614731f));
            if (launchTime + 25.83674 > Game.Music.Time) yield return Wait(launchTime + 25.83674f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.83674f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x45/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07448006f));
            if (launchTime + 25.9361 > Game.Music.Time) yield return Wait(launchTime + 25.9361f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.9361f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x45/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06756973f));
            if (launchTime + 26.04584 > Game.Music.Time) yield return Wait(launchTime + 26.04584f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 26.04584f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x45/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.0628624f));
            if (launchTime + 26.15786 > Game.Music.Time) yield return Wait(launchTime + 26.15786f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 26.15786f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x45/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.0628624f));
            if (launchTime + 27.92043 > Game.Music.Time) yield return Wait(launchTime + 27.92043f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 27.92043f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x3b/255f,0x4d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2315598f));
            if (launchTime + 28.17206 > Game.Music.Time) yield return Wait(launchTime + 28.17206f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 28.17206f, blackboard, (c,i,t,l, castc, casti) => new Color(0x47/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2566376f));
            if (launchTime + 29.7178 > Game.Music.Time) yield return Wait(launchTime + 29.7178f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 29.7178f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x00/255f,0x54/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1480045f));
            if (launchTime + 29.9426 > Game.Music.Time) yield return Wait(launchTime + 29.9426f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 29.9426f, blackboard, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x00/255f,0x2b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05996895f));
            if (launchTime + 30.01911 > Game.Music.Time) yield return Wait(launchTime + 30.01911f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 30.01911f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x00/255f,0x54/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.04083252f));
            if (launchTime + 30.09547 > Game.Music.Time) yield return Wait(launchTime + 30.09547f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 30.09547f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x00/255f,0x54/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.04083252f));
            if (launchTime + 30.16645 > Game.Music.Time) yield return Wait(launchTime + 30.16645f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 30.16645f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x00/255f,0x54/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.04083252f));
            if (launchTime + 31.48907 > Game.Music.Time) yield return Wait(launchTime + 31.48907f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 31.48907f, blackboard, (c,i,t,l, castc, casti) => new Color(0x52/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1769543f));
            if (launchTime + 31.72253 > Game.Music.Time) yield return Wait(launchTime + 31.72253f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 31.72253f, blackboard, (c,i,t,l, castc, casti) => new Color(0x52/255f,0x28/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2277374f));
            if (launchTime + 33.72448 > Game.Music.Time) yield return Wait(launchTime + 33.72448f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 33.72448f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            if (launchTime + 33.94735 > Game.Music.Time) yield return Wait(launchTime + 33.94735f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 33.94735f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            if (launchTime + 34.168 > Game.Music.Time) yield return Wait(launchTime + 34.168f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 34.168f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            if (launchTime + 34.39754 > Game.Music.Time) yield return Wait(launchTime + 34.39754f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 34.39754f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            if (launchTime + 34.60482 > Game.Music.Time) yield return Wait(launchTime + 34.60482f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 34.60482f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            if (launchTime + 34.83661 > Game.Music.Time) yield return Wait(launchTime + 34.83661f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 34.83661f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            if (launchTime + 35.05057 > Game.Music.Time) yield return Wait(launchTime + 35.05057f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 35.05057f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1012955f));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc portal_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, portal_input, "node_1.output_patterninput");
            object portal = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => portal = o);

            object path = blackboard.GetValue((c, i, t, l, castc, casti) => new MyPath(-220f,-160f,-270f,-120f,-170f,-80f,-270f,-40f,-170f,0f,-270f,40f,-170f,80f,-270f,120f,-170f,160f,-270f,200f,-124f,187f,-107f,28f,-205f,125f,-142f,-53f,-83f,6f,-370f,298f));
            blackboard.SetValue("node_5.output_path", path);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.003612519 > Game.Music.Time) yield return Wait(launchTime + 0.003612519f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.003612519f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_3.output_enemy(ies)", 0.04724026f));
            if (launchTime + 0.2138796 > Game.Music.Time) yield return Wait(launchTime + 0.2138796f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.2138796f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.04340267f));
            if (launchTime + 0.2618255 > Game.Music.Time) yield return Wait(launchTime + 0.2618255f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.2618255f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            if (launchTime + 0.3243217 > Game.Music.Time) yield return Wait(launchTime + 0.3243217f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.3243217f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_8.output_enemy(ies)", 0.04340744f));
            if (launchTime + 0.3727894 > Game.Music.Time) yield return Wait(launchTime + 0.3727894f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.3727894f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)"));
            if (launchTime + 0.444211 > Game.Music.Time) yield return Wait(launchTime + 0.444211f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.444211f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_10.output_enemy(ies)", 0.04623795f));
            if (launchTime + 0.4925031 > Game.Music.Time) yield return Wait(launchTime + 0.4925031f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.4925031f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_bullet(s)"));
            if (launchTime + 0.5523996 > Game.Music.Time) yield return Wait(launchTime + 0.5523996f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.5523996f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_12.output_enemy(ies)", 0.0441494f));
            if (launchTime + 0.5975342 > Game.Music.Time) yield return Wait(launchTime + 0.5975342f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.5975342f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_bullet(s)"));
            if (launchTime + 0.7656841 > Game.Music.Time) yield return Wait(launchTime + 0.7656841f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.7656841f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_14.output_enemy(ies)", 0.04070568f));
            if (launchTime + 0.8093166 > Game.Music.Time) yield return Wait(launchTime + 0.8093166f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.8093166f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_15.output_bullet(s)"));
            if (launchTime + 0.8839025 > Game.Music.Time) yield return Wait(launchTime + 0.8839025f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.8839025f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_16.output_enemy(ies)", 0.0484581f));
            if (launchTime + 0.9350071 > Game.Music.Time) yield return Wait(launchTime + 0.9350071f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.9350071f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_17.output_bullet(s)"));
            if (launchTime + 0.987647 > Game.Music.Time) yield return Wait(launchTime + 0.987647f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.987647f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => 8f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_18.output_enemy(ies)", 0.04759216f));
            if (launchTime + 1.038325 > Game.Music.Time) yield return Wait(launchTime + 1.038325f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.038325f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_19.output_bullet(s)"));
            if (launchTime + 1.214466 > Game.Music.Time) yield return Wait(launchTime + 1.214466f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.214466f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 8f, (c,i,t,l, castc, casti) => 9f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_20.output_enemy(ies)", 0.03812027f));
            if (launchTime + 1.255268 > Game.Music.Time) yield return Wait(launchTime + 1.255268f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.255268f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_21.output_bullet(s)"));
            if (launchTime + 1.319627 > Game.Music.Time) yield return Wait(launchTime + 1.319627f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.319627f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 9f, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_22.output_enemy(ies)", 0.04328442f));
            if (launchTime + 1.365472 > Game.Music.Time) yield return Wait(launchTime + 1.365472f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.365472f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_23.output_bullet(s)"));
            if (launchTime + 1.442561 > Game.Music.Time) yield return Wait(launchTime + 1.442561f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.442561f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => 11f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_24.output_enemy(ies)", 0.03811646f));
            if (launchTime + 1.482554 > Game.Music.Time) yield return Wait(launchTime + 1.482554f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.482554f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_25.output_bullet(s)"));
            if (launchTime + 1.54121 > Game.Music.Time) yield return Wait(launchTime + 1.54121f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.54121f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 11f, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_26.output_enemy(ies)", 0.04845238f));
            if (launchTime + 1.593613 > Game.Music.Time) yield return Wait(launchTime + 1.593613f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.593613f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_27.output_bullet(s)"));
            if (launchTime + 1.632146 > Game.Music.Time) yield return Wait(launchTime + 1.632146f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.632146f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => 13f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_28.output_enemy(ies)", 0.04845428f));
            if (launchTime + 1.684005 > Game.Music.Time) yield return Wait(launchTime + 1.684005f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.684005f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_29.output_bullet(s)"));
            if (launchTime + 1.694356 > Game.Music.Time) yield return Wait(launchTime + 1.694356f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.694356f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 13f, (c,i,t,l, castc, casti) => 14f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_30.output_enemy(ies)", 0.04845333f));
            if (launchTime + 1.745989 > Game.Music.Time) yield return Wait(launchTime + 1.745989f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.745989f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i+(float)rndf(-180f,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x59/255f,0xcf/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_31.output_bullet(s)"));
            if (launchTime + 1.779896 > Game.Music.Time) yield return Wait(launchTime + 1.779896f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 1.779896f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_path", (c,i,t,l, castc, casti) => 14f, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_32.output_enemy(ies)", 0.2125511f));
            if (launchTime + 2.111956 > Game.Music.Time) yield return Wait(launchTime + 2.111956f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.111956f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard, InputFunc enemies_input, InputFunc rotation_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            SubscribeInput(outboard, blackboard, rotation_input, "node_1.output_patterninput");
            object rotation = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => rotation = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4312592 > Game.Music.Time) yield return Wait(launchTime + 0.4312592f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 0.4312592f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.111122 > Game.Music.Time) yield return Wait(launchTime + 1.111122f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 1.111122f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.319409 > Game.Music.Time) yield return Wait(launchTime + 1.319409f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 1.319409f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.546627 > Game.Music.Time) yield return Wait(launchTime + 1.546627f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 1.546627f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.765937 > Game.Music.Time) yield return Wait(launchTime + 1.765937f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 1.765937f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.212395 > Game.Music.Time) yield return Wait(launchTime + 2.212395f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 2.212395f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.548225 > Game.Music.Time) yield return Wait(launchTime + 2.548225f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 2.548225f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.533203 > Game.Music.Time) yield return Wait(launchTime + 3.533203f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 3.533203f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.873893 > Game.Music.Time) yield return Wait(launchTime + 3.873893f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 3.873893f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.324914 > Game.Music.Time) yield return Wait(launchTime + 5.324914f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 5.324914f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.648719 > Game.Music.Time) yield return Wait(launchTime + 5.648719f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 5.648719f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.994621 > Game.Music.Time) yield return Wait(launchTime + 5.994621f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 5.994621f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.09166 > Game.Music.Time) yield return Wait(launchTime + 7.09166f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 7.09166f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.541306 > Game.Music.Time) yield return Wait(launchTime + 7.541306f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 7.541306f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.209126 > Game.Music.Time) yield return Wait(launchTime + 8.209126f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 8.209126f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.435509 > Game.Music.Time) yield return Wait(launchTime + 8.435509f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 8.435509f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.655228 > Game.Music.Time) yield return Wait(launchTime + 8.655228f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 8.655228f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.877548 > Game.Music.Time) yield return Wait(launchTime + 8.877548f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 8.877548f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.324013 > Game.Music.Time) yield return Wait(launchTime + 9.324013f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 9.324013f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.655678 > Game.Music.Time) yield return Wait(launchTime + 9.655678f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 9.655678f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.66021 > Game.Music.Time) yield return Wait(launchTime + 10.66021f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 10.66021f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.97913 > Game.Music.Time) yield return Wait(launchTime + 10.97913f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 10.97913f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.32034 > Game.Music.Time) yield return Wait(launchTime + 11.32034f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 11.32034f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.66159 > Game.Music.Time) yield return Wait(launchTime + 11.66159f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 11.66159f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.97878 > Game.Music.Time) yield return Wait(launchTime + 11.97878f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 11.97878f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.20691 > Game.Music.Time) yield return Wait(launchTime + 12.20691f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 12.20691f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.44513 > Game.Music.Time) yield return Wait(launchTime + 12.44513f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 12.44513f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.9978 > Game.Music.Time) yield return Wait(launchTime + 12.9978f);
            Player.StartCoroutine(Pattern_11/*LongShot*/(launchTime + 12.9978f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard, InputFunc rotation_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, rotation_input, "node_0.output_patterninput");
            object rotation = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => rotation = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_1.output_patterninput");
            object enemies = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x33/255f,0x30/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3077774f));
            if (launchTime + 0.02520752 > Game.Music.Time) yield return Wait(launchTime + 0.02520752f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0.02520752f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => (float)d+1f/*out*/, "node_4.output_elements"));
            if (launchTime + 0.02671051 > Game.Music.Time) yield return Wait(launchTime + 0.02671051f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.02671051f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_elements", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 180f*(float)i+(float)rotation, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f+50f/(float)c/2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard, InputFunc enemies_input, InputFunc rotation_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            SubscribeInput(outboard, blackboard, rotation_input, "node_1.output_patterninput");
            object rotation = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => rotation = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4238205 > Game.Music.Time) yield return Wait(launchTime + 0.4238205f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0.4238205f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8596725 > Game.Music.Time) yield return Wait(launchTime + 0.8596725f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0.8596725f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.321778 > Game.Music.Time) yield return Wait(launchTime + 1.321778f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 1.321778f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.747139 > Game.Music.Time) yield return Wait(launchTime + 1.747139f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 1.747139f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.204003 > Game.Music.Time) yield return Wait(launchTime + 2.204003f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 2.204003f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.650368 > Game.Music.Time) yield return Wait(launchTime + 2.650368f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 2.650368f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.080971 > Game.Music.Time) yield return Wait(launchTime + 3.080971f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 3.080971f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.53257 > Game.Music.Time) yield return Wait(launchTime + 3.53257f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 3.53257f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.96846 > Game.Music.Time) yield return Wait(launchTime + 3.96846f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 3.96846f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.430573 > Game.Music.Time) yield return Wait(launchTime + 4.430573f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 4.430573f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.866425 > Game.Music.Time) yield return Wait(launchTime + 4.866425f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 4.866425f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.312798 > Game.Music.Time) yield return Wait(launchTime + 5.312798f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 5.312798f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.746331 > Game.Music.Time) yield return Wait(launchTime + 5.746331f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 5.746331f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.190201 > Game.Music.Time) yield return Wait(launchTime + 6.190201f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 6.190201f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.634064 > Game.Music.Time) yield return Wait(launchTime + 6.634064f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 6.634064f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard, InputFunc rotation_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, rotation_input, "node_0.output_patterninput");
            object rotation = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => rotation = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_1.output_patterninput");
            object enemies = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => (float)d+1f/*out*/, "node_2.output_elements"));
            if (launchTime + 0.003772736 > Game.Music.Time) yield return Wait(launchTime + 0.003772736f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.003772736f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => ((float)d+1f)/2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => ((float)i-(float)c/2f+0.5f)*10f+(float)rotation, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.005218506 > Game.Music.Time) yield return Wait(launchTime + 0.005218506f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.005218506f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => ((float)d+1f)/2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => ((float)i-(float)c/2f+0.5f)*10f+180f+(float)rotation, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(-365f*(float)flipx*(float)ar,-18f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 60f, (c,i,t,l, castc, casti) => -5f/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.1176147 > Game.Music.Time) yield return Wait(launchTime + 0.1176147f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.1176147f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-359f,168f,-11f,206f,259f,180f,32f,137f,-241f,140f,-230f,79f,-31f,86f,170f,75f,255f,125f,383f,143f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx*(float)ar,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 15.2515f));
            if (launchTime + 0.4325714 > Game.Music.Time) yield return Wait(launchTime + 0.4325714f);
            Player.StartCoroutine(Pattern_35/*NewPattern*/(launchTime + 0.4325714f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 7.538285 > Game.Music.Time) yield return Wait(launchTime + 7.538285f);
            Player.StartCoroutine(Pattern_35/*NewPattern*/(launchTime + 7.538285f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 15.43218 > Game.Music.Time) yield return Wait(launchTime + 15.43218f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.43218f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object bullets;
            blackboard.SubscribeForChanges("node_7.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullets", (c,i,t,l, castc, casti) => "node_5.output_bullets", (c,i,t,l, castc, casti) => "node_6.output_bullets", (c,i,t,l, castc, casti) => "node_8.output_bullets", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(-392f,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 80f, (c,i,t,l, castc, casti) => -5f/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.3003388 > Game.Music.Time) yield return Wait(launchTime + 0.3003388f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.3003388f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-372f,202f,33f,-38f,34f,-82f,-239f,-162f,-108f,174f,193f,146f,276f,-180f,-193f,-130f,24f,152f,284f,138f,127f,-73f,-127f,-205f,-296f,-168f,-278f,167f,-149f,201f,-81f,-41f,-247f,31f,79f,118f,245f,-76f,211f,-221f,-46f,-52f,33f,101f,170f,108f,184f,-40f,59f,-21f,86f,51f,-222f,176f,-274f,-34f,-89f,-152f,-122f,28f,-196f,-51f,-30f,-148f,145f,-136f,240f,-12f,285f,182f,160f,208f,68f,172f,194f,-124f,185f,-182f,-10f,-203f,45f,304f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(1.2f*(float)ar,0.85f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 30.31232f));
            if (launchTime + 0.8183899 > Game.Music.Time) yield return Wait(launchTime + 0.8183899f);
            Player.StartCoroutine(Pattern_17/*Long*/(launchTime + 0.8183899f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/, "node_2.output_bullets"));
            if (launchTime + 0.8335266 > Game.Music.Time) yield return Wait(launchTime + 0.8335266f);
            Player.StartCoroutine(Pattern_21/*Beat1*/(launchTime + 0.8335266f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 7.940689 > Game.Music.Time) yield return Wait(launchTime + 7.940689f);
            Player.StartCoroutine(Pattern_17/*Long*/(launchTime + 7.940689f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/, "node_5.output_bullets"));
            if (launchTime + 15.03909 > Game.Music.Time) yield return Wait(launchTime + 15.03909f);
            Player.StartCoroutine(Pattern_17/*Long*/(launchTime + 15.03909f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/, "node_6.output_bullets"));
            if (launchTime + 22.176 > Game.Music.Time) yield return Wait(launchTime + 22.176f);
            Player.StartCoroutine(Pattern_17/*Long*/(launchTime + 22.176f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/, "node_8.output_bullets"));
            if (launchTime + 30.84496 > Game.Music.Time) yield return Wait(launchTime + 30.84496f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 30.84496f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 36.37818 > Game.Music.Time) yield return Wait(launchTime + 36.37818f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 36.37818f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_23/*Beat2*/(launchTime + 3.402823E+38f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 0.1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.01402282 > Game.Music.Time) yield return Wait(launchTime + 0.01402282f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.01402282f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => 0.40f/(float)d, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x77/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)", 2.520309f));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc enemy_input, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            SubscribeOutput(outboard, blackboard, bullets_output, "node_18.output_value");
            object bullets;
            blackboard.SubscribeForChanges("node_15.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullets", (c,i,t,l, castc, casti) => "node_11.output_bullets", (c,i,t,l, castc, casti) => "node_22.output_bullets", (c,i,t,l, castc, casti) => "node_31.output_bullets", (c,i,t,l, castc, casti) => "node_17.output_bullets", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_15.output_value");
            object allbullets;
            blackboard.SubscribeForChanges("node_18.output_value", (b, o) => allbullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_15.output_value", (c,i,t,l, castc, casti) => "node_24.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_18.output_value");
            object bigbullets;
            blackboard.SubscribeForChanges("node_24.output_value", (b, o) => bigbullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => "node_8.output_bullet(s)", (c,i,t,l, castc, casti) => "node_13.output_bullet(s)", (c,i,t,l, castc, casti) => "node_20.output_bullet(s)", (c,i,t,l, castc, casti) => "node_25.output_bullet(s)", (c,i,t,l, castc, casti) => "node_28.output_bullet(s)", (c,i,t,l, castc, casti) => "node_33.output_bullet(s)", (c,i,t,l, castc, casti) => "node_38.output_bullet(s)"/*out*/, "node_24.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x2f/255f,0x56/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4640198f));
            if (launchTime + 0.001647949 > Game.Music.Time) yield return Wait(launchTime + 0.001647949f);
            Player.StartCoroutine(Pattern_41/*Long*/(launchTime + 0.001647949f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_2.output_bullets"));
            if (launchTime + 0.6645356 > Game.Music.Time) yield return Wait(launchTime + 0.6645356f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.6645356f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.6689911 > Game.Music.Time) yield return Wait(launchTime + 0.6689911f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.6689911f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1366577f));
            if (launchTime + 0.6712798 > Game.Music.Time) yield return Wait(launchTime + 0.6712798f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.6712798f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 1.548477 > Game.Music.Time) yield return Wait(launchTime + 1.548477f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.548477f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1509705f));
            if (launchTime + 1.563324 > Game.Music.Time) yield return Wait(launchTime + 1.563324f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.563324f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_bullet(s)"));
            if (launchTime + 1.569839 > Game.Music.Time) yield return Wait(launchTime + 1.569839f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.569839f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)"));
            if (launchTime + 1.770599 > Game.Music.Time) yield return Wait(launchTime + 1.770599f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.770599f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x2f/255f,0x56/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4582062f));
            if (launchTime + 1.796997 > Game.Music.Time) yield return Wait(launchTime + 1.796997f);
            Player.StartCoroutine(Pattern_41/*Long*/(launchTime + 1.796997f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_11.output_bullets"));
            if (launchTime + 2.451645 > Game.Music.Time) yield return Wait(launchTime + 2.451645f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.451645f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1509705f));
            if (launchTime + 2.454117 > Game.Music.Time) yield return Wait(launchTime + 2.454117f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.454117f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_bullet(s)"));
            if (launchTime + 2.458816 > Game.Music.Time) yield return Wait(launchTime + 2.458816f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.458816f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_14.output_bullet(s)"));
            if (launchTime + 2.681869 > Game.Music.Time) yield return Wait(launchTime + 2.681869f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.681869f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x2f/255f,0x56/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.446701f));
            if (launchTime + 2.683609 > Game.Music.Time) yield return Wait(launchTime + 2.683609f);
            Player.StartCoroutine(Pattern_41/*Long*/(launchTime + 2.683609f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_17.output_bullets"));
            if (launchTime + 3.33548 > Game.Music.Time) yield return Wait(launchTime + 3.33548f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.33548f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1372528f));
            if (launchTime + 3.362472 > Game.Music.Time) yield return Wait(launchTime + 3.362472f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 3.362472f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_20.output_bullet(s)"));
            if (launchTime + 3.367279 > Game.Music.Time) yield return Wait(launchTime + 3.367279f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 3.367279f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_21.output_bullet(s)"));
            if (launchTime + 3.556472 > Game.Music.Time) yield return Wait(launchTime + 3.556472f);
            Player.StartCoroutine(Pattern_41/*Long*/(launchTime + 3.556472f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_22.output_bullets"));
            if (launchTime + 3.564285 > Game.Music.Time) yield return Wait(launchTime + 3.564285f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.564285f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x2f/255f,0x56/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.440918f));
            if (launchTime + 4.231857 > Game.Music.Time) yield return Wait(launchTime + 4.231857f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 4.231857f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_25.output_bullet(s)"));
            if (launchTime + 4.23233 > Game.Music.Time) yield return Wait(launchTime + 4.23233f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.23233f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1372528f));
            if (launchTime + 4.232391 > Game.Music.Time) yield return Wait(launchTime + 4.232391f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 4.232391f, blackboard, (c,i,t,l, castc, casti) => "node_25.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_27.output_bullet(s)"));
            if (launchTime + 5.107086 > Game.Music.Time) yield return Wait(launchTime + 5.107086f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 5.107086f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_28.output_bullet(s)"));
            if (launchTime + 5.111053 > Game.Music.Time) yield return Wait(launchTime + 5.111053f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.111053f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1372528f));
            if (launchTime + 5.116836 > Game.Music.Time) yield return Wait(launchTime + 5.116836f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 5.116836f, blackboard, (c,i,t,l, castc, casti) => "node_28.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_30.output_bullet(s)"));
            if (launchTime + 5.33789 > Game.Music.Time) yield return Wait(launchTime + 5.33789f);
            Player.StartCoroutine(Pattern_41/*Long*/(launchTime + 5.33789f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_31.output_bullets"));
            if (launchTime + 5.340652 > Game.Music.Time) yield return Wait(launchTime + 5.340652f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.340652f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x2f/255f,0x56/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4351349f));
            if (launchTime + 6.005646 > Game.Music.Time) yield return Wait(launchTime + 6.005646f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.005646f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_33.output_bullet(s)"));
            if (launchTime + 6.007904 > Game.Music.Time) yield return Wait(launchTime + 6.007904f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.007904f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1372528f));
            if (launchTime + 6.009842 > Game.Music.Time) yield return Wait(launchTime + 6.009842f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.009842f, blackboard, (c,i,t,l, castc, casti) => "node_33.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_35.output_bullet(s)"));
            if (launchTime + 6.886627 > Game.Music.Time) yield return Wait(launchTime + 6.886627f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.886627f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4e/255f,0x2b/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1372528f));
            if (launchTime + 6.894531 > Game.Music.Time) yield return Wait(launchTime + 6.894531f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.894531f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_38.output_bullet(s)"));
            if (launchTime + 6.898956 > Game.Music.Time) yield return Wait(launchTime + 6.898956f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.898956f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_39.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+(float)i*15f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x86/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => EnemyImage.EnemyTeleport, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(0f,-300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.191757 > Game.Music.Time) yield return Wait(launchTime + 0.191757f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.191757f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-154f*(float)flipx*(float)ar,-170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_2.output_enemy(ies)", 0.2648773f));
            if (launchTime + 0.7767481 > Game.Music.Time) yield return Wait(launchTime + 0.7767481f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.7767481f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-225f*(float)flipx*(float)ar,-170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_3.output_enemy(ies)", 0.5852966f));
            if (launchTime + 1.436111 > Game.Music.Time) yield return Wait(launchTime + 1.436111f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 1.436111f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_4.output_enemy"));
            if (launchTime + 2.968902 > Game.Music.Time) yield return Wait(launchTime + 2.968902f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.968902f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+((float)toint((float)i,3f)*50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 2.975463 > Game.Music.Time) yield return Wait(launchTime + 2.975463f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.975463f, blackboard, (c,i,t,l, castc, casti) => new Color(0x63/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.118042f));
            if (launchTime + 3.210205 > Game.Music.Time) yield return Wait(launchTime + 3.210205f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 3.210205f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_8.output_enemy"));
            if (launchTime + 4.101928 > Game.Music.Time) yield return Wait(launchTime + 4.101928f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.101928f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5f/255f,0x5f/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2505951f));
            if (launchTime + 4.108398 > Game.Music.Time) yield return Wait(launchTime + 4.108398f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 4.108398f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(250f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_bullet(s)"));
            if (launchTime + 4.493347 > Game.Music.Time) yield return Wait(launchTime + 4.493347f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.493347f, blackboard, (c,i,t,l, castc, casti) => new Color(0x6c/255f,0x00/255f,0x47/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.278183f));
            if (launchTime + 4.525299 > Game.Music.Time) yield return Wait(launchTime + 4.525299f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 4.525299f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet(s)", (c,i,t,l, castc, casti) => 0.07f, (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_12.output_bullet(s)", 0.2526398f));
            if (launchTime + 5.006454 > Game.Music.Time) yield return Wait(launchTime + 5.006454f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 5.006454f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_13.output_enemy"));
            if (launchTime + 5.864624 > Game.Music.Time) yield return Wait(launchTime + 5.864624f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.864624f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x6a/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06584167f));
            if (launchTime + 5.889465 > Game.Music.Time) yield return Wait(launchTime + 5.889465f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 5.889465f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_15.output_bullet(s)"));
            if (launchTime + 5.981338 > Game.Music.Time) yield return Wait(launchTime + 5.981338f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.981338f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x6a/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05934143f));
            if (launchTime + 5.992523 > Game.Music.Time) yield return Wait(launchTime + 5.992523f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 5.992523f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_17.output_bullet(s)"));
            if (launchTime + 6.078613 > Game.Music.Time) yield return Wait(launchTime + 6.078613f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.078613f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x6a/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06585693f));
            if (launchTime + 6.105545 > Game.Music.Time) yield return Wait(launchTime + 6.105545f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.105545f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_19.output_bullet(s)"));
            if (launchTime + 6.195312 > Game.Music.Time) yield return Wait(launchTime + 6.195312f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.195312f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x6a/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07229614f));
            if (launchTime + 6.219451 > Game.Music.Time) yield return Wait(launchTime + 6.219451f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.219451f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_21.output_bullet(s)"));
            if (launchTime + 6.32286 > Game.Music.Time) yield return Wait(launchTime + 6.32286f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.32286f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x6a/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05281067f));
            if (launchTime + 6.32695 > Game.Music.Time) yield return Wait(launchTime + 6.32695f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.32695f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_enemy", (c,i,t,l, castc, casti) => 5f+2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+360f/(float)c/2f*0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+50f*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x77/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_23.output_bullet(s)"));
            if (launchTime + 6.771591 > Game.Music.Time) yield return Wait(launchTime + 6.771591f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 6.771591f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_24.output_enemy"));
            if (launchTime + 8.082138 > Game.Music.Time) yield return Wait(launchTime + 8.082138f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.082138f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x49/255f,0x77/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09823608f));
            if (launchTime + 8.094788 > Game.Music.Time) yield return Wait(launchTime + 8.094788f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 8.094788f, blackboard, (c,i,t,l, castc, casti) => "node_24.output_enemy", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (360f/(float)c)*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)sint((float)i/(float)c*5f)*50f+350f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x38/255f,0xcf/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_26.output_bullet(s)"));
            if (launchTime + 8.32637 > Game.Music.Time) yield return Wait(launchTime + 8.32637f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.32637f, blackboard, (c,i,t,l, castc, casti) => new Color(0x6e/255f,0x6a/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1782227f));
            if (launchTime + 8.337769 > Game.Music.Time) yield return Wait(launchTime + 8.337769f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 8.337769f, blackboard, (c,i,t,l, castc, casti) => "node_24.output_enemy", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => ((float)i%3f-3f/2f+0.5f)*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f+((float)toint((float)i,3f)*30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_28.output_bullet(s)"));
            if (launchTime + 8.558975 > Game.Music.Time) yield return Wait(launchTime + 8.558975f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 8.558975f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_29.output_enemy"));
            if (launchTime + 9.860413 > Game.Music.Time) yield return Wait(launchTime + 9.860413f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 9.860413f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f/(float)c*(float)i+100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xd9/255f,0x6c/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_30.output_bullet(s)"));
            if (launchTime + 9.863007 > Game.Music.Time) yield return Wait(launchTime + 9.863007f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.863007f, blackboard, (c,i,t,l, castc, casti) => new Color(0x49/255f,0x00/255f,0x52/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09823608f));
            if (launchTime + 10.08562 > Game.Music.Time) yield return Wait(launchTime + 10.08562f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.08562f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x00/255f,0x5d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.0463562f));
            if (launchTime + 10.08917 > Game.Music.Time) yield return Wait(launchTime + 10.08917f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 10.08917f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx-45f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f/(float)c*(float)i+100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_33.output_bullet(s)"));
            if (launchTime + 10.16484 > Game.Music.Time) yield return Wait(launchTime + 10.16484f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.16484f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x00/255f,0x5d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06150818f));
            if (launchTime + 10.16669 > Game.Music.Time) yield return Wait(launchTime + 10.16669f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 10.16669f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx-15f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f/(float)c*(float)i+100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_35.output_bullet(s)"));
            if (launchTime + 10.24197 > Game.Music.Time) yield return Wait(launchTime + 10.24197f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.24197f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x00/255f,0x5d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06150818f));
            if (launchTime + 10.2455 > Game.Music.Time) yield return Wait(launchTime + 10.2455f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 10.2455f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx+15f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f/(float)c*(float)i+100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_37.output_bullet(s)"));
            if (launchTime + 10.31326 > Game.Music.Time) yield return Wait(launchTime + 10.31326f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 10.31326f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_38.output_enemy"));
            if (launchTime + 10.31874 > Game.Music.Time) yield return Wait(launchTime + 10.31874f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 10.31874f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx+45f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f/(float)c*(float)i+100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f/(float)c*(float)i+0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_39.output_bullet(s)"));
            if (launchTime + 10.32047 > Game.Music.Time) yield return Wait(launchTime + 10.32047f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.32047f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x00/255f,0x5d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06150818f));
            if (launchTime + 11.20676 > Game.Music.Time) yield return Wait(launchTime + 11.20676f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.20676f, blackboard, (c,i,t,l, castc, casti) => new Color(0x61/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2752838f));
            if (launchTime + 11.21909 > Game.Music.Time) yield return Wait(launchTime + 11.21909f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 11.21909f, blackboard, (c,i,t,l, castc, casti) => "node_38.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(250f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_42.output_bullet(s)"));
            if (launchTime + 11.63528 > Game.Music.Time) yield return Wait(launchTime + 11.63528f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.63528f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5f/255f,0x1d/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1627502f));
            if (launchTime + 11.63855 > Game.Music.Time) yield return Wait(launchTime + 11.63855f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 11.63855f, blackboard, (c,i,t,l, castc, casti) => "node_42.output_bullet(s)", (c,i,t,l, castc, casti) => 0.05f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_44.output_bullet(s)", 0.1638031f));
            if (launchTime + 11.85916 > Game.Music.Time) yield return Wait(launchTime + 11.85916f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.85916f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5f/255f,0x1d/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2378845f));
            if (launchTime + 11.86427 > Game.Music.Time) yield return Wait(launchTime + 11.86427f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 11.86427f, blackboard, (c,i,t,l, castc, casti) => "node_42.output_bullet(s)", (c,i,t,l, castc, casti) => 0.05f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,70f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x70/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_46.output_bullet(s)", 0.2449188f));
            if (launchTime + 12.11131 > Game.Music.Time) yield return Wait(launchTime + 12.11131f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 12.11131f, blackboard, (c,i,t,l, castc, casti) => "node_42.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 12.12016 > Game.Music.Time) yield return Wait(launchTime + 12.12016f);
            Player.StartCoroutine(Pattern_2/*DefaultMonster*/(launchTime + 12.12016f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_48.output_enemy"));
            if (launchTime + 13.43016 > Game.Music.Time) yield return Wait(launchTime + 13.43016f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.43016f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5f/255f,0x5d/255f,0x61/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3063354f));
            if (launchTime + 13.4323 > Game.Music.Time) yield return Wait(launchTime + 13.4323f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 13.4323f, blackboard, (c,i,t,l, castc, casti) => "node_48.output_enemy", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(237f*(float)flipx,421f), (c,i,t,l, castc, casti) => new Vector2(0f,-3004f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_50.output_bullet(s)"));
            if (launchTime + 17.43523 > Game.Music.Time) yield return Wait(launchTime + 17.43523f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 17.43523f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_19/*LastPart*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4485779 > Game.Music.Time) yield return Wait(launchTime + 0.4485779f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.4485779f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9021455 > Game.Music.Time) yield return Wait(launchTime + 0.9021455f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.9021455f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.338454 > Game.Music.Time) yield return Wait(launchTime + 1.338454f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 1.338454f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.792006 > Game.Music.Time) yield return Wait(launchTime + 1.792006f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 1.792006f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.232635 > Game.Music.Time) yield return Wait(launchTime + 2.232635f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 2.232635f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.660295 > Game.Music.Time) yield return Wait(launchTime + 2.660295f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 2.660295f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.109544 > Game.Music.Time) yield return Wait(launchTime + 3.109544f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 3.109544f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.550157 > Game.Music.Time) yield return Wait(launchTime + 3.550157f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 3.550157f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.003737 > Game.Music.Time) yield return Wait(launchTime + 4.003737f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 4.003737f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.448625 > Game.Music.Time) yield return Wait(launchTime + 4.448625f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 4.448625f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.902223 > Game.Music.Time) yield return Wait(launchTime + 4.902223f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 4.902223f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.342834 > Game.Music.Time) yield return Wait(launchTime + 5.342834f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 5.342834f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.786574 > Game.Music.Time) yield return Wait(launchTime + 5.786574f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 5.786574f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.238191 > Game.Music.Time) yield return Wait(launchTime + 6.238191f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 6.238191f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.678023 > Game.Music.Time) yield return Wait(launchTime + 6.678023f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 6.678023f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.106063 > Game.Music.Time) yield return Wait(launchTime + 7.106063f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 7.106063f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.557679 > Game.Music.Time) yield return Wait(launchTime + 7.557679f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 7.557679f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.005356 > Game.Music.Time) yield return Wait(launchTime + 8.005356f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 8.005356f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.445175 > Game.Music.Time) yield return Wait(launchTime + 8.445175f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 8.445175f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.904648 > Game.Music.Time) yield return Wait(launchTime + 8.904648f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 8.904648f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.348404 > Game.Music.Time) yield return Wait(launchTime + 9.348404f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 9.348404f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.788239 > Game.Music.Time) yield return Wait(launchTime + 9.788239f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 9.788239f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.23198 > Game.Music.Time) yield return Wait(launchTime + 10.23198f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 10.23198f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.67575 > Game.Music.Time) yield return Wait(launchTime + 10.67575f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 10.67575f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.11163 > Game.Music.Time) yield return Wait(launchTime + 11.11163f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 11.11163f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.55931 > Game.Music.Time) yield return Wait(launchTime + 11.55931f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 11.55931f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.007 > Game.Music.Time) yield return Wait(launchTime + 12.007f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 12.007f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.44682 > Game.Music.Time) yield return Wait(launchTime + 12.44682f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 12.44682f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.89449 > Game.Music.Time) yield return Wait(launchTime + 12.89449f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 12.89449f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.3461 > Game.Music.Time) yield return Wait(launchTime + 13.3461f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 13.3461f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.77017 > Game.Music.Time) yield return Wait(launchTime + 13.77017f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 13.77017f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.9082201 > Game.Music.Time) yield return Wait(launchTime + 0.9082201f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0.9082201f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.77121 > Game.Music.Time) yield return Wait(launchTime + 1.77121f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 1.77121f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.670923 > Game.Music.Time) yield return Wait(launchTime + 2.670923f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 2.670923f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.339282 > Game.Music.Time) yield return Wait(launchTime + 3.339282f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 3.339282f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/2f/*out*/));
            if (launchTime + 6.006077 > Game.Music.Time) yield return Wait(launchTime + 6.006077f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 6.006077f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 6.890863 > Game.Music.Time) yield return Wait(launchTime + 6.890863f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 6.890863f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 8.690862 > Game.Music.Time) yield return Wait(launchTime + 8.690862f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 8.690862f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 10.44449 > Game.Music.Time) yield return Wait(launchTime + 10.44449f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 10.44449f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 12.23453 > Game.Music.Time) yield return Wait(launchTime + 12.23453f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 12.23453f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 14.00352 > Game.Music.Time) yield return Wait(launchTime + 14.00352f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 14.00352f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 15.80199 > Game.Music.Time) yield return Wait(launchTime + 15.80199f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 15.80199f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 17.57519 > Game.Music.Time) yield return Wait(launchTime + 17.57519f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 17.57519f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 19.33455 > Game.Music.Time) yield return Wait(launchTime + 19.33455f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 19.33455f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 21.12159 > Game.Music.Time) yield return Wait(launchTime + 21.12159f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 21.12159f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 22.89899 > Game.Music.Time) yield return Wait(launchTime + 22.89899f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 22.89899f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 24.68061 > Game.Music.Time) yield return Wait(launchTime + 24.68061f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 24.68061f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 26.44961 > Game.Music.Time) yield return Wait(launchTime + 26.44961f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 26.44961f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 28.24385 > Game.Music.Time) yield return Wait(launchTime + 28.24385f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 28.24385f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 30.00442 > Game.Music.Time) yield return Wait(launchTime + 30.00442f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 30.00442f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 31.79868 > Game.Music.Time) yield return Wait(launchTime + 31.79868f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 31.79868f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 35.32835 > Game.Music.Time) yield return Wait(launchTime + 35.32835f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 35.32835f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 38.18911 > Game.Music.Time) yield return Wait(launchTime + 38.18911f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 38.18911f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 38.89696 > Game.Music.Time) yield return Wait(launchTime + 38.89696f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 38.89696f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 39.56838 > Game.Music.Time) yield return Wait(launchTime + 39.56838f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 39.56838f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 40.64841 > Game.Music.Time) yield return Wait(launchTime + 40.64841f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 40.64841f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 45.64736 > Game.Music.Time) yield return Wait(launchTime + 45.64736f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 45.64736f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 45.99762 > Game.Music.Time) yield return Wait(launchTime + 45.99762f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 45.99762f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 48.57373 > Game.Music.Time) yield return Wait(launchTime + 48.57373f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 48.57373f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 49.5443 > Game.Music.Time) yield return Wait(launchTime + 49.5443f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 49.5443f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 52.43421 > Game.Music.Time) yield return Wait(launchTime + 52.43421f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 52.43421f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 53.12018 > Game.Music.Time) yield return Wait(launchTime + 53.12018f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 53.12018f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 53.79888 > Game.Music.Time) yield return Wait(launchTime + 53.79888f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 53.79888f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 54.88621 > Game.Music.Time) yield return Wait(launchTime + 54.88621f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 54.88621f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 59.83408 > Game.Music.Time) yield return Wait(launchTime + 59.83408f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 59.83408f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 60.22812 > Game.Music.Time) yield return Wait(launchTime + 60.22812f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 60.22812f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 62.77505 > Game.Music.Time) yield return Wait(launchTime + 62.77505f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 62.77505f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 63.7748 > Game.Music.Time) yield return Wait(launchTime + 63.7748f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 63.7748f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 67.32005 > Game.Music.Time) yield return Wait(launchTime + 67.32005f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 67.32005f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 72.66215 > Game.Music.Time) yield return Wait(launchTime + 72.66215f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 72.66215f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 74.44843 > Game.Music.Time) yield return Wait(launchTime + 74.44843f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 74.44843f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 79.78099 > Game.Music.Time) yield return Wait(launchTime + 79.78099f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 79.78099f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 81.57383 > Game.Music.Time) yield return Wait(launchTime + 81.57383f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 81.57383f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 86.89747 > Game.Music.Time) yield return Wait(launchTime + 86.89747f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 86.89747f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 88.67245 > Game.Music.Time) yield return Wait(launchTime + 88.67245f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 88.67245f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 94.0028 > Game.Music.Time) yield return Wait(launchTime + 94.0028f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 94.0028f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 95.81675 > Game.Music.Time) yield return Wait(launchTime + 95.81675f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 95.81675f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 109.8923 > Game.Music.Time) yield return Wait(launchTime + 109.8923f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 109.8923f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 110.4401 > Game.Music.Time) yield return Wait(launchTime + 110.4401f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 110.4401f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 111.7868 > Game.Music.Time) yield return Wait(launchTime + 111.7868f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 111.7868f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 114.6601 > Game.Music.Time) yield return Wait(launchTime + 114.6601f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 114.6601f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 115.3385 > Game.Music.Time) yield return Wait(launchTime + 115.3385f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 115.3385f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 115.9899 > Game.Music.Time) yield return Wait(launchTime + 115.9899f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 115.9899f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 117.1097 > Game.Music.Time) yield return Wait(launchTime + 117.1097f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 117.1097f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 122.1085 > Game.Music.Time) yield return Wait(launchTime + 122.1085f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 122.1085f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 122.4637 > Game.Music.Time) yield return Wait(launchTime + 122.4637f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 122.4637f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 124.9898 > Game.Music.Time) yield return Wait(launchTime + 124.9898f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 124.9898f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 125.997 > Game.Music.Time) yield return Wait(launchTime + 125.997f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 125.997f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 129.2167 > Game.Music.Time) yield return Wait(launchTime + 129.2167f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 129.2167f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 129.5484 > Game.Music.Time) yield return Wait(launchTime + 129.5484f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 129.5484f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 130.2298 > Game.Music.Time) yield return Wait(launchTime + 130.2298f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 130.2298f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 131.3286 > Game.Music.Time) yield return Wait(launchTime + 131.3286f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 131.3286f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 136.3327 > Game.Music.Time) yield return Wait(launchTime + 136.3327f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 136.3327f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 136.6691 > Game.Music.Time) yield return Wait(launchTime + 136.6691f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 136.6691f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 139.1901 > Game.Music.Time) yield return Wait(launchTime + 139.1901f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 139.1901f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 140.2204 > Game.Music.Time) yield return Wait(launchTime + 140.2204f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 140.2204f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 147.3354 > Game.Music.Time) yield return Wait(launchTime + 147.3354f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 147.3354f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 154.4314 > Game.Music.Time) yield return Wait(launchTime + 154.4314f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 154.4314f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 161.5553 > Game.Music.Time) yield return Wait(launchTime + 161.5553f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 161.5553f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 168.6654 > Game.Music.Time) yield return Wait(launchTime + 168.6654f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 168.6654f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 172.219 > Game.Music.Time) yield return Wait(launchTime + 172.219f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 172.219f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 173.1336 > Game.Music.Time) yield return Wait(launchTime + 173.1336f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 173.1336f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 174.0144 > Game.Music.Time) yield return Wait(launchTime + 174.0144f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 174.0144f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 175.1112 > Game.Music.Time) yield return Wait(launchTime + 175.1112f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 175.1112f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 270f/*out*/));
            if (launchTime + 175.7866 > Game.Music.Time) yield return Wait(launchTime + 175.7866f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 175.7866f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 181.1131 > Game.Music.Time) yield return Wait(launchTime + 181.1131f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 181.1131f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 182.8823 > Game.Music.Time) yield return Wait(launchTime + 182.8823f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 182.8823f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 188.2279 > Game.Music.Time) yield return Wait(launchTime + 188.2279f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 188.2279f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 190.0289 > Game.Music.Time) yield return Wait(launchTime + 190.0289f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 190.0289f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 195.3491 > Game.Music.Time) yield return Wait(launchTime + 195.3491f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 195.3491f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 197.0992 > Game.Music.Time) yield return Wait(launchTime + 197.0992f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 197.0992f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 202.4321 > Game.Music.Time) yield return Wait(launchTime + 202.4321f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 202.4321f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 204.233 > Game.Music.Time) yield return Wait(launchTime + 204.233f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 204.233f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 206.0387 > Game.Music.Time) yield return Wait(launchTime + 206.0387f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 206.0387f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 207.7725 > Game.Music.Time) yield return Wait(launchTime + 207.7725f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 207.7725f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 209.5732 > Game.Music.Time) yield return Wait(launchTime + 209.5732f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 209.5732f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 211.3678 > Game.Music.Time) yield return Wait(launchTime + 211.3678f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 211.3678f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 213.132 > Game.Music.Time) yield return Wait(launchTime + 213.132f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 213.132f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 214.9084 > Game.Music.Time) yield return Wait(launchTime + 214.9084f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 214.9084f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 216.6544 > Game.Music.Time) yield return Wait(launchTime + 216.6544f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 216.6544f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 135f/*out*/));
            if (launchTime + 219.5666 > Game.Music.Time) yield return Wait(launchTime + 219.5666f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 219.5666f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 220.2239 > Game.Music.Time) yield return Wait(launchTime + 220.2239f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 220.2239f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 222.1805 > Game.Music.Time) yield return Wait(launchTime + 222.1805f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 222.1805f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_26/*Appear*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bullet"));
            if (launchTime + 0.675705 > Game.Music.Time) yield return Wait(launchTime + 0.675705f);
            Player.StartCoroutine(Pattern_26/*Appear*/(launchTime + 0.675705f, blackboard/*out*/, "node_2.output_bullet"));
            if (launchTime + 1.342926 > Game.Music.Time) yield return Wait(launchTime + 1.342926f);
            Player.StartCoroutine(Pattern_26/*Appear*/(launchTime + 1.342926f, blackboard/*out*/, "node_3.output_bullet"));
            if (launchTime + 1.789582 > Game.Music.Time) yield return Wait(launchTime + 1.789582f);
            Player.StartCoroutine(Pattern_26/*Appear*/(launchTime + 1.789582f, blackboard/*out*/, "node_4.output_bullet"));
            if (launchTime + 2.44574 > Game.Music.Time) yield return Wait(launchTime + 2.44574f);
            Player.StartCoroutine(Pattern_26/*Appear*/(launchTime + 2.44574f, blackboard/*out*/, "node_5.output_bullet"));
            if (launchTime + 3.135056 > Game.Music.Time) yield return Wait(launchTime + 3.135056f);
            Player.StartCoroutine(Pattern_26/*Appear*/(launchTime + 3.135056f, blackboard/*out*/, "node_6.output_bullet"));
            if (launchTime + 3.561081 > Game.Music.Time) yield return Wait(launchTime + 3.561081f);
            Player.StartCoroutine(Pattern_27/*Explode*/(launchTime + 3.561081f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet"/*out*/));
            if (launchTime + 3.888733 > Game.Music.Time) yield return Wait(launchTime + 3.888733f);
            Player.StartCoroutine(Pattern_27/*Explode*/(launchTime + 3.888733f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet"/*out*/));
            if (launchTime + 4.225113 > Game.Music.Time) yield return Wait(launchTime + 4.225113f);
            Player.StartCoroutine(Pattern_27/*Explode*/(launchTime + 4.225113f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet"/*out*/));
            if (launchTime + 5.327911 > Game.Music.Time) yield return Wait(launchTime + 5.327911f);
            Player.StartCoroutine(Pattern_27/*Explode*/(launchTime + 5.327911f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet"/*out*/));
            if (launchTime + 5.658783 > Game.Music.Time) yield return Wait(launchTime + 5.658783f);
            Player.StartCoroutine(Pattern_27/*Explode*/(launchTime + 5.658783f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet"/*out*/));
            if (launchTime + 6.006164 > Game.Music.Time) yield return Wait(launchTime + 6.006164f);
            Player.StartCoroutine(Pattern_27/*Explode*/(launchTime + 6.006164f, blackboard, (c,i,t,l, castc, casti) => "node_6.output_bullet"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard, string bullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullet_output, "node_1.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-250f,250f),(float)rndf(0f,200f)), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x65/255f), (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard, InputFunc bullet_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullet_input, "node_0.output_patterninput");
            object bullet = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullet = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 10f*(float)d-5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.01481628 > Game.Music.Time) yield return Wait(launchTime + 0.01481628f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.01481628f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy3, (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(1f,272f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.01528549 > Game.Music.Time) yield return Wait(launchTime + 0.01528549f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.01528549f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(2f,160f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_2.output_enemy(ies)", 0.2912331f));
            if (launchTime + 0.3380509 > Game.Music.Time) yield return Wait(launchTime + 0.3380509f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.3380509f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.111f, (c,i,t,l, castc, casti) => 5f+(5f*(float)d+1f)*(float)t, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)t*(40f*(float)d), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f*(float)t+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x88/255f,0x10/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)", 0.6165085f));
            if (launchTime + 1.027584 > Game.Music.Time) yield return Wait(launchTime + 1.027584f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.027584f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(1f,298f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_4.output_enemy(ies)", 0.2964706f));
            if (launchTime + 1.33965 > Game.Music.Time) yield return Wait(launchTime + 1.33965f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.33965f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc flipy_input, InputFunc flipvariants_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_3.output_patterninput");
            object flipy = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_4.output_patterninput");
            object flipvariants = blackboard.GetValue("node_4.output_patterninput");
            blackboard.SubscribeForChanges("node_4.output_patterninput", (b, o) => flipvariants = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy9, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(348f*(float)ar*(float)flipx,139f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.06314468 > Game.Music.Time) yield return Wait(launchTime + 0.06314468f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.06314468f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(238f*(float)ar*(float)flipx,155f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_2.output_enemy(ies)", 0.2970543f));
            if (launchTime + 0.3716278 > Game.Music.Time) yield return Wait(launchTime + 0.3716278f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.3716278f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(175f*(float)ar*(float)flipx,114f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_5.output_enemy(ies)", 0.3192978f));
            if (launchTime + 0.7314911 > Game.Music.Time) yield return Wait(launchTime + 0.7314911f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 0.7314911f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 0.8483467 > Game.Music.Time) yield return Wait(launchTime + 0.8483467f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 0.8483467f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 0.9470329 > Game.Music.Time) yield return Wait(launchTime + 0.9470329f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 0.9470329f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.172958 > Game.Music.Time) yield return Wait(launchTime + 1.172958f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 1.172958f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.279427 > Game.Music.Time) yield return Wait(launchTime + 1.279427f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 1.279427f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.396286 > Game.Music.Time) yield return Wait(launchTime + 1.396286f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 1.396286f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.510544 > Game.Music.Time) yield return Wait(launchTime + 1.510544f);
            Player.StartCoroutine(Pattern_30/*Shot*/(launchTime + 1.510544f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.840767 > Game.Music.Time) yield return Wait(launchTime + 1.840767f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.840767f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(248f*(float)ar*(float)flipx,81f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_14.output_enemy(ies)", 0.3191681f));
            if (launchTime + 2.182281 > Game.Music.Time) yield return Wait(launchTime + 2.182281f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 2.182281f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(349f*(float)ar*(float)flipx,85f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_15.output_enemy(ies)", 0.0906105f));
            if (launchTime + 2.286678 > Game.Music.Time) yield return Wait(launchTime + 2.286678f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.286678f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => new Vector2(50f/((float)c-1f)*(float)i-25f,20f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x2d/255f,0x42/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy10, (c,i,t,l, castc, casti) => 800f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(335f*(float)ar,119f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 25f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.09920879 > Game.Music.Time) yield return Wait(launchTime + 0.09920879f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.09920879f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-189f*(float)ar,119f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 2.871048f));
            if (launchTime + 0.4314422 > Game.Music.Time) yield return Wait(launchTime + 0.4314422f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 0.4314422f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 0.765686 > Game.Music.Time) yield return Wait(launchTime + 0.765686f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 0.765686f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.11023 > Game.Music.Time) yield return Wait(launchTime + 1.11023f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 1.11023f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.459885 > Game.Music.Time) yield return Wait(launchTime + 1.459885f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 1.459885f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.77356 > Game.Music.Time) yield return Wait(launchTime + 1.77356f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 1.77356f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.999817 > Game.Music.Time) yield return Wait(launchTime + 1.999817f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 1.999817f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 2.21579 > Game.Music.Time) yield return Wait(launchTime + 2.21579f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 2.21579f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 2.745423 > Game.Music.Time) yield return Wait(launchTime + 2.745423f);
            Player.StartCoroutine(Pattern_32/*Laser*/(launchTime + 2.745423f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 2.981762 > Game.Music.Time) yield return Wait(launchTime + 2.981762f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 2.981762f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.111f, (c,i,t,l, castc, casti) => 5f+(5f*(float)d+1f)*(float)t, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)t*(40f*(float)d), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f*(float)t+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x88/255f,0x10/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_bullet(s)", 0.9976807f));
            if (launchTime + 4.00201 > Game.Music.Time) yield return Wait(launchTime + 4.00201f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 4.00201f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-362f*(float)ar,118f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_12.output_enemy(ies)", 0.4211731f));
            if (launchTime + 4.44125 > Game.Music.Time) yield return Wait(launchTime + 4.44125f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 4.44125f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -20f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 20f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d+6f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Triangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f-(float)i*5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xc5/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-1f,207f), (c,i,t,l, castc, casti) => (float)d*7f+10f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard, InputFunc position_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, position_input, "node_0.output_patterninput");
            object position = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => position = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 1.788383 > Game.Music.Time) yield return Wait(launchTime + 1.788383f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.788383f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (360f/(float)c*(float)i)+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,250f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 1.803062 > Game.Music.Time) yield return Wait(launchTime + 1.803062f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.803062f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_16/*SingleShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4458619 > Game.Music.Time) yield return Wait(launchTime + 0.4458619f);
            Player.StartCoroutine(Pattern_16/*SingleShot*/(launchTime + 0.4458619f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.550544 > Game.Music.Time) yield return Wait(launchTime + 1.550544f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.550544f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.663475 > Game.Music.Time) yield return Wait(launchTime + 1.663475f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.663475f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.775886 > Game.Music.Time) yield return Wait(launchTime + 1.775886f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.775886f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.996071 > Game.Music.Time) yield return Wait(launchTime + 1.996071f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.996071f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.214698 > Game.Music.Time) yield return Wait(launchTime + 2.214698f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.214698f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.448753 > Game.Music.Time) yield return Wait(launchTime + 2.448753f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.448753f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.650474 > Game.Music.Time) yield return Wait(launchTime + 2.650474f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.650474f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.331664 > Game.Music.Time) yield return Wait(launchTime + 3.331664f);
            Player.StartCoroutine(Pattern_16/*SingleShot*/(launchTime + 3.331664f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.573928 > Game.Music.Time) yield return Wait(launchTime + 3.573928f);
            Player.StartCoroutine(Pattern_16/*SingleShot*/(launchTime + 3.573928f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.996764 > Game.Music.Time) yield return Wait(launchTime + 3.996764f);
            Player.StartCoroutine(Pattern_16/*SingleShot*/(launchTime + 3.996764f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.332336 > Game.Music.Time) yield return Wait(launchTime + 5.332336f);
            Player.StartCoroutine(Pattern_36/*Spread*/(launchTime + 5.332336f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.665145 > Game.Music.Time) yield return Wait(launchTime + 5.665145f);
            Player.StartCoroutine(Pattern_36/*Spread*/(launchTime + 5.665145f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.995269 > Game.Music.Time) yield return Wait(launchTime + 5.995269f);
            Player.StartCoroutine(Pattern_36/*Spread*/(launchTime + 5.995269f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.217468 > Game.Music.Time) yield return Wait(launchTime + 6.217468f);
            Player.StartCoroutine(Pattern_36/*Spread*/(launchTime + 6.217468f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.557113 > Game.Music.Time) yield return Wait(launchTime + 6.557113f);
            Player.StartCoroutine(Pattern_36/*Spread*/(launchTime + 6.557113f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.89038 > Game.Music.Time) yield return Wait(launchTime + 6.89038f);
            Player.StartCoroutine(Pattern_36/*Spread*/(launchTime + 6.89038f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xe2/255f,0x1b/255f,0x82/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_40/*TwoCollide*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.134087 > Game.Music.Time) yield return Wait(launchTime + 1.134087f);
            Player.StartCoroutine(Pattern_38/*Melody*/(launchTime + 1.134087f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object count1 = blackboard.GetValue((c, i, t, l, castc, casti) => 1f);
            blackboard.SetValue("node_0.output_value", count1);

            object count2 = blackboard.GetValue((c, i, t, l, castc, casti) => 2f);
            blackboard.SetValue("node_1.output_value", count2);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 0.3257294 > Game.Music.Time) yield return Wait(launchTime + 0.3257294f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 0.3257294f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 0.6567764 > Game.Music.Time) yield return Wait(launchTime + 0.6567764f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 0.6567764f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 0.879013 > Game.Music.Time) yield return Wait(launchTime + 0.879013f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 0.879013f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 1.326599 > Game.Music.Time) yield return Wait(launchTime + 1.326599f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.326599f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 1.43383 > Game.Music.Time) yield return Wait(launchTime + 1.43383f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.43383f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 1.550392 > Game.Music.Time) yield return Wait(launchTime + 1.550392f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.550392f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 1.654526 > Game.Music.Time) yield return Wait(launchTime + 1.654526f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.654526f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 1.778854 > Game.Music.Time) yield return Wait(launchTime + 1.778854f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.778854f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 2.097458 > Game.Music.Time) yield return Wait(launchTime + 2.097458f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 2.097458f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 2.450249 > Game.Music.Time) yield return Wait(launchTime + 2.450249f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 2.450249f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 2.656952 > Game.Music.Time) yield return Wait(launchTime + 2.656952f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 2.656952f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 2.98642 > Game.Music.Time) yield return Wait(launchTime + 2.98642f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 2.98642f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 3.219543 > Game.Music.Time) yield return Wait(launchTime + 3.219543f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.219543f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 3.32988 > Game.Music.Time) yield return Wait(launchTime + 3.32988f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.32988f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 3.440231 > Game.Music.Time) yield return Wait(launchTime + 3.440231f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.440231f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 3.554954 > Game.Music.Time) yield return Wait(launchTime + 3.554954f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.554954f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 3.880752 > Game.Music.Time) yield return Wait(launchTime + 3.880752f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.880752f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 4.199448 > Game.Music.Time) yield return Wait(launchTime + 4.199448f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 4.199448f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 4.429855 > Game.Music.Time) yield return Wait(launchTime + 4.429855f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 4.429855f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 4.7658 > Game.Music.Time) yield return Wait(launchTime + 4.7658f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 4.7658f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 4.994064 > Game.Music.Time) yield return Wait(launchTime + 4.994064f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 4.994064f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 5.108169 > Game.Music.Time) yield return Wait(launchTime + 5.108169f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 5.108169f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 5.323517 > Game.Music.Time) yield return Wait(launchTime + 5.323517f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 5.323517f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 5.661606 > Game.Music.Time) yield return Wait(launchTime + 5.661606f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 5.661606f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 5.995384 > Game.Music.Time) yield return Wait(launchTime + 5.995384f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 5.995384f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 6.219345 > Game.Music.Time) yield return Wait(launchTime + 6.219345f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.219345f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 6.552711 > Game.Music.Time) yield return Wait(launchTime + 6.552711f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.552711f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 6.765213 > Game.Music.Time) yield return Wait(launchTime + 6.765213f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.765213f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 6.893661 > Game.Music.Time) yield return Wait(launchTime + 6.893661f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.893661f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 6.991722 > Game.Music.Time) yield return Wait(launchTime + 6.991722f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.991722f, blackboard, (c,i,t,l, castc, casti) => (float)count1/*out*/));
            if (launchTime + 7.105308 > Game.Music.Time) yield return Wait(launchTime + 7.105308f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 7.105308f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 7.430939 > Game.Music.Time) yield return Wait(launchTime + 7.430939f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 7.430939f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 7.768593 > Game.Music.Time) yield return Wait(launchTime + 7.768593f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 7.768593f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 8.005417 > Game.Music.Time) yield return Wait(launchTime + 8.005417f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 8.005417f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 8.335335 > Game.Music.Time) yield return Wait(launchTime + 8.335335f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 8.335335f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 8.55188 > Game.Music.Time) yield return Wait(launchTime + 8.55188f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 8.55188f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 8.661865 > Game.Music.Time) yield return Wait(launchTime + 8.661865f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 8.661865f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 8.88858 > Game.Music.Time) yield return Wait(launchTime + 8.88858f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 8.88858f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 9.213425 > Game.Music.Time) yield return Wait(launchTime + 9.213425f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 9.213425f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 9.543335 > Game.Music.Time) yield return Wait(launchTime + 9.543335f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 9.543335f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 9.763298 > Game.Music.Time) yield return Wait(launchTime + 9.763298f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 9.763298f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 10.09149 > Game.Music.Time) yield return Wait(launchTime + 10.09149f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 10.09149f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 10.32157 > Game.Music.Time) yield return Wait(launchTime + 10.32157f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 10.32157f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 10.42139 > Game.Music.Time) yield return Wait(launchTime + 10.42139f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 10.42139f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 10.5483 > Game.Music.Time) yield return Wait(launchTime + 10.5483f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 10.5483f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 10.65993 > Game.Music.Time) yield return Wait(launchTime + 10.65993f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 10.65993f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 10.98646 > Game.Music.Time) yield return Wait(launchTime + 10.98646f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 10.98646f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 11.31975 > Game.Music.Time) yield return Wait(launchTime + 11.31975f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 11.31975f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 11.54477 > Game.Music.Time) yield return Wait(launchTime + 11.54477f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 11.54477f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 11.87808 > Game.Music.Time) yield return Wait(launchTime + 11.87808f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 11.87808f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 12.1014 > Game.Music.Time) yield return Wait(launchTime + 12.1014f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 12.1014f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 12.21984 > Game.Music.Time) yield return Wait(launchTime + 12.21984f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 12.21984f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 12.43978 > Game.Music.Time) yield return Wait(launchTime + 12.43978f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 12.43978f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 12.76293 > Game.Music.Time) yield return Wait(launchTime + 12.76293f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 12.76293f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 13.0979 > Game.Music.Time) yield return Wait(launchTime + 13.0979f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 13.0979f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 13.31446 > Game.Music.Time) yield return Wait(launchTime + 13.31446f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 13.31446f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 13.65285 > Game.Music.Time) yield return Wait(launchTime + 13.65285f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 13.65285f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 13.89141 > Game.Music.Time) yield return Wait(launchTime + 13.89141f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 13.89141f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 13.98612 > Game.Music.Time) yield return Wait(launchTime + 13.98612f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 13.98612f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            if (launchTime + 14.09952 > Game.Music.Time) yield return Wait(launchTime + 14.09952f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 14.09952f, blackboard, (c,i,t,l, castc, casti) => (float)count2/*out*/));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard, InputFunc count_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, count_input, "node_0.output_patterninput");
            object count = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => count = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => (float)d*(float)count, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-320f,320f),(float)rndf(0f,240f)), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x95/255f,0xd2/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.000595092 > Game.Music.Time) yield return Wait(launchTime + 0.000595092f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.000595092f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object right;
            blackboard.SubscribeForChanges("node_0.output_value", (b, o) => right = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_value");
            object left;
            blackboard.SubscribeForChanges("node_1.output_value", (b, o) => left = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(406f,36f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 40f, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.01879895 > Game.Music.Time) yield return Wait(launchTime + 0.01879895f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.01879895f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(-387f,109f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 40f, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.02612996 > Game.Music.Time) yield return Wait(launchTime + 0.02612996f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.02612996f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(-379f,155f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 40f, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_enemy(ies)"));
            if (launchTime + 0.03095996 > Game.Music.Time) yield return Wait(launchTime + 0.03095996f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.03095996f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(396f,133f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 40f, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_enemy(ies)"));
            if (launchTime + 0.330528 > Game.Music.Time) yield return Wait(launchTime + 0.330528f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.330528f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-260f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.7798004f));
            if (launchTime + 0.3441389 > Game.Music.Time) yield return Wait(launchTime + 0.3441389f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.3441389f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(260f,120f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_7.output_enemy(ies)", 0.7837677f));
            if (launchTime + 0.3458173 > Game.Music.Time) yield return Wait(launchTime + 0.3458173f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.3458173f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-260f,120f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_8.output_enemy(ies)", 0.7713242f));
            if (launchTime + 0.3485334 > Game.Music.Time) yield return Wait(launchTime + 0.3485334f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.3485334f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(260f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_9.output_enemy(ies)", 0.7707901f));
            if (launchTime + 1.126632 > Game.Music.Time) yield return Wait(launchTime + 1.126632f);
            Player.StartCoroutine(Pattern_42/*FirstBeat*/(launchTime + 1.126632f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_value"/*out*/));
            if (launchTime + 1.596458 > Game.Music.Time) yield return Wait(launchTime + 1.596458f);
            Player.StartCoroutine(Pattern_42/*FirstBeat*/(launchTime + 1.596458f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_value"/*out*/));
            if (launchTime + 12.97424 > Game.Music.Time) yield return Wait(launchTime + 12.97424f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 12.97424f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(260f,200f,241f,69f,222f,147f,92f,197f,32f,119f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_13.output_enemy(ies)", 2.764275f));
            if (launchTime + 12.98351 > Game.Music.Time) yield return Wait(launchTime + 12.98351f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 12.98351f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-260f,120f,-247f,48f,-190f,118f,-130f,36f,-77f,67f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_14.output_enemy(ies)", 3.201378f));
            if (launchTime + 12.98351 > Game.Music.Time) yield return Wait(launchTime + 12.98351f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 12.98351f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-260f,200f,-224f,177f,-207f,87f,-73f,142f,32f,119f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_15.output_enemy(ies)", 2.768929f));
            if (launchTime + 13.00828 > Game.Music.Time) yield return Wait(launchTime + 13.00828f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 13.00828f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(260f,120f,235f,45f,102f,87f,21f,32f,98f,5f,0f,92f,-77f,67f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_16.output_enemy(ies)", 3.210716f));
            if (launchTime + 15.76193 > Game.Music.Time) yield return Wait(launchTime + 15.76193f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.76193f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 15.7655 > Game.Music.Time) yield return Wait(launchTime + 15.7655f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.7655f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 16.19457 > Game.Music.Time) yield return Wait(launchTime + 16.19457f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 16.19457f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 16.20395 > Game.Music.Time) yield return Wait(launchTime + 16.20395f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 16.20395f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_41(float launchTime, Blackboard outboard, InputFunc enemy_input, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            SubscribeOutput(outboard, blackboard, bullets_output, "node_1.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0.03f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x6c/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)", 0.4285583f));
            yield break;
        }

        private IEnumerator Pattern_42(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8949357 > Game.Music.Time) yield return Wait(launchTime + 0.8949357f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 0.8949357f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.778931 > Game.Music.Time) yield return Wait(launchTime + 1.778931f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 1.778931f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.662949 > Game.Music.Time) yield return Wait(launchTime + 2.662949f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 2.662949f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.550827 > Game.Music.Time) yield return Wait(launchTime + 3.550827f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 3.550827f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.454384 > Game.Music.Time) yield return Wait(launchTime + 4.454384f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 4.454384f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.334458 > Game.Music.Time) yield return Wait(launchTime + 5.334458f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 5.334458f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.222359 > Game.Music.Time) yield return Wait(launchTime + 6.222359f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 6.222359f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.118103 > Game.Music.Time) yield return Wait(launchTime + 7.118103f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 7.118103f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.00209 > Game.Music.Time) yield return Wait(launchTime + 8.00209f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 8.00209f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.893914 > Game.Music.Time) yield return Wait(launchTime + 8.893914f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 8.893914f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.766174 > Game.Music.Time) yield return Wait(launchTime + 9.766174f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 9.766174f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.66971 > Game.Music.Time) yield return Wait(launchTime + 10.66971f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 10.66971f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.58109 > Game.Music.Time) yield return Wait(launchTime + 11.58109f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 11.58109f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_43(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x61/255f,0x13/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

    }
}