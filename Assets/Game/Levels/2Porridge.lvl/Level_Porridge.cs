#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_Porridge : LevelScript
    {
        protected override string ClipName {
            get { return "Porridge.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0.02544777 > Game.Music.Time) yield return Wait(launchTime + 0.02544777f);
            Player.StartCoroutine(Pattern_1/*Part1*/(launchTime + 0.02544777f, blackboard/*out*/));
            if (launchTime + 0.03371882 > Game.Music.Time) yield return Wait(launchTime + 0.03371882f);
            Player.StartCoroutine(BossHealth.Act(launchTime + 0.03371882f, blackboard, (c,i,t,l, castc, casti) => "Praying Mantis", (c,i,t,l, castc, casti) => 200000f, (c,i,t,l, castc, casti) => true/*out*/));
            if (launchTime + 0.03377743 > Game.Music.Time) yield return Wait(launchTime + 0.03377743f);
            Player.StartCoroutine(Pattern_38/*DanceMan*/(launchTime + 0.03377743f, blackboard/*out*/));
            if (launchTime + 0.05138224 > Game.Music.Time) yield return Wait(launchTime + 0.05138224f);
            Player.StartCoroutine(Pattern_24/*TwoBeats*/(launchTime + 0.05138224f, blackboard/*out*/));
            if (launchTime + 6.455097 > Game.Music.Time) yield return Wait(launchTime + 6.455097f);
            Player.StartCoroutine(Pattern_4/*Part2*/(launchTime + 6.455097f, blackboard/*out*/));
            if (launchTime + 13.72847 > Game.Music.Time) yield return Wait(launchTime + 13.72847f);
            Player.StartCoroutine(Pattern_11/*Part3*/(launchTime + 13.72847f, blackboard/*out*/));
            if (launchTime + 20.57457 > Game.Music.Time) yield return Wait(launchTime + 20.57457f);
            Player.StartCoroutine(Pattern_14/*Part4*/(launchTime + 20.57457f, blackboard/*out*/));
            if (launchTime + 25.72558 > Game.Music.Time) yield return Wait(launchTime + 25.72558f);
            Player.StartCoroutine(Pattern_15/*Transition1*/(launchTime + 25.72558f, blackboard/*out*/));
            if (launchTime + 27.86423 > Game.Music.Time) yield return Wait(launchTime + 27.86423f);
            Player.StartCoroutine(Pattern_16/*Part5*/(launchTime + 27.86423f, blackboard/*out*/, "node_9.output_bullets"));
            if (launchTime + 34.66409 > Game.Music.Time) yield return Wait(launchTime + 34.66409f);
            Player.StartCoroutine(Pattern_19/*Part6*/(launchTime + 34.66409f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_bullets"/*out*/));
            if (launchTime + 37.92871 > Game.Music.Time) yield return Wait(launchTime + 37.92871f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 37.92871f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x33/255f,0x35/255f)/*out*/, 3.208931f));
            if (launchTime + 41.14252 > Game.Music.Time) yield return Wait(launchTime + 41.14252f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 41.14252f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.05469894f));
            if (launchTime + 41.14918 > Game.Music.Time) yield return Wait(launchTime + 41.14918f);
            Player.StartCoroutine(Pattern_21/*Part7*/(launchTime + 41.14918f, blackboard/*out*/));
            if (launchTime + 41.15874 > Game.Music.Time) yield return Wait(launchTime + 41.15874f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 41.15874f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.5669823f));
            if (launchTime + 51.78399 > Game.Music.Time) yield return Wait(launchTime + 51.78399f);
            Player.StartCoroutine(Pattern_29/*MoarBreak*/(launchTime + 51.78399f, blackboard/*out*/));
            if (launchTime + 54.87091 > Game.Music.Time) yield return Wait(launchTime + 54.87091f);
            Player.StartCoroutine(Pattern_30/*Part8*/(launchTime + 54.87091f, blackboard/*out*/));
            if (launchTime + 66.87409 > Game.Music.Time) yield return Wait(launchTime + 66.87409f);
            Player.StartCoroutine(Pattern_37/*Insertion*/(launchTime + 66.87409f, blackboard/*out*/));
            if (launchTime + 69.01514 > Game.Music.Time) yield return Wait(launchTime + 69.01514f);
            Player.StartCoroutine(Pattern_16/*Part5*/(launchTime + 69.01514f, blackboard/*out*/, "node_18.output_bullets"));
            if (launchTime + 75.82429 > Game.Music.Time) yield return Wait(launchTime + 75.82429f);
            Player.StartCoroutine(Pattern_19/*Part6*/(launchTime + 75.82429f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_bullets"/*out*/));
            if (launchTime + 82.29666 > Game.Music.Time) yield return Wait(launchTime + 82.29666f);
            Player.StartCoroutine(Pattern_21/*Part7*/(launchTime + 82.29666f, blackboard/*out*/));
            if (launchTime + 94.27439 > Game.Music.Time) yield return Wait(launchTime + 94.27439f);
            Player.StartCoroutine(Pattern_26/*4Beats*/(launchTime + 94.27439f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4143831 > Game.Music.Time) yield return Wait(launchTime + 0.4143831f);
            Player.StartCoroutine(Pattern_3/*Fireworks*/(launchTime + 0.4143831f, blackboard/*out*/));
            if (launchTime + 1.696221 > Game.Music.Time) yield return Wait(launchTime + 1.696221f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 1.696221f, blackboard/*out*/));
            if (launchTime + 2.553802 > Game.Music.Time) yield return Wait(launchTime + 2.553802f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 2.553802f, blackboard/*out*/));
            if (launchTime + 3.415147 > Game.Music.Time) yield return Wait(launchTime + 3.415147f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 3.415147f, blackboard/*out*/));
            if (launchTime + 5.130309 > Game.Music.Time) yield return Wait(launchTime + 5.130309f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 5.130309f, blackboard/*out*/));
            if (launchTime + 5.976607 > Game.Music.Time) yield return Wait(launchTime + 5.976607f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 5.976607f, blackboard/*out*/));
            if (launchTime + 6.837951 > Game.Music.Time) yield return Wait(launchTime + 6.837951f);
            Player.StartCoroutine(Pattern_10/*Explosion*/(launchTime + 6.837951f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Triangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-10f,10f)+(float)rndf(-3f,3f)*((float)d-1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 170f+30f/(float)c*((float)i+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x7b/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4191892 > Game.Music.Time) yield return Wait(launchTime + 0.4191892f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 0.4191892f, blackboard/*out*/));
            if (launchTime + 0.8507825 > Game.Music.Time) yield return Wait(launchTime + 0.8507825f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 0.8507825f, blackboard/*out*/));
            if (launchTime + 1.275903 > Game.Music.Time) yield return Wait(launchTime + 1.275903f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 1.275903f, blackboard/*out*/));
            if (launchTime + 1.698015 > Game.Music.Time) yield return Wait(launchTime + 1.698015f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 1.698015f, blackboard/*out*/));
            if (launchTime + 2.134934 > Game.Music.Time) yield return Wait(launchTime + 2.134934f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 2.134934f, blackboard/*out*/));
            if (launchTime + 2.565795 > Game.Music.Time) yield return Wait(launchTime + 2.565795f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 2.565795f, blackboard/*out*/));
            if (launchTime + 2.983013 > Game.Music.Time) yield return Wait(launchTime + 2.983013f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 2.983013f, blackboard/*out*/));
            if (launchTime + 3.418234 > Game.Music.Time) yield return Wait(launchTime + 3.418234f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 3.418234f, blackboard/*out*/));
            if (launchTime + 3.840827 > Game.Music.Time) yield return Wait(launchTime + 3.840827f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 3.840827f, blackboard/*out*/));
            if (launchTime + 4.272422 > Game.Music.Time) yield return Wait(launchTime + 4.272422f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 4.272422f, blackboard/*out*/));
            if (launchTime + 4.706584 > Game.Music.Time) yield return Wait(launchTime + 4.706584f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 4.706584f, blackboard/*out*/));
            if (launchTime + 4.949183 > Game.Music.Time) yield return Wait(launchTime + 4.949183f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 4.949183f, blackboard/*out*/));
            if (launchTime + 5.128487 > Game.Music.Time) yield return Wait(launchTime + 5.128487f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 5.128487f, blackboard/*out*/));
            if (launchTime + 5.38758 > Game.Music.Time) yield return Wait(launchTime + 5.38758f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 5.38758f, blackboard/*out*/));
            if (launchTime + 5.81497 > Game.Music.Time) yield return Wait(launchTime + 5.81497f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 5.81497f, blackboard/*out*/));
            if (launchTime + 6.195265 > Game.Music.Time) yield return Wait(launchTime + 6.195265f);
            Player.StartCoroutine(Pattern_2/*Firework*/(launchTime + 6.195265f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object enemies;
            blackboard.SubscribeForChanges("node_7.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy", (c,i,t,l, castc, casti) => "node_4.output_enemy", (c,i,t,l, castc, casti) => "node_6.output_enemy", (c,i,t,l, castc, casti) => "node_9.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*CreateEnemies*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 50f/*out*/, "node_1.output_enemy"));
            if (launchTime + 0.4250017 > Game.Music.Time) yield return Wait(launchTime + 0.4250017f);
            Player.StartCoroutine(Pattern_8/*Beat*/(launchTime + 0.4250017f, blackboard/*out*/));
            if (launchTime + 0.4264922 > Game.Music.Time) yield return Wait(launchTime + 0.4264922f);
            Player.StartCoroutine(Pattern_6/*Shoot*/(launchTime + 0.4264922f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 1.716767 > Game.Music.Time) yield return Wait(launchTime + 1.716767f);
            Player.StartCoroutine(Pattern_5/*CreateEnemies*/(launchTime + 1.716767f, blackboard, (c,i,t,l, castc, casti) => 100f/*out*/, "node_4.output_enemy"));
            if (launchTime + 2.133141 > Game.Music.Time) yield return Wait(launchTime + 2.133141f);
            Player.StartCoroutine(Pattern_6/*Shoot*/(launchTime + 2.133141f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 3.431602 > Game.Music.Time) yield return Wait(launchTime + 3.431602f);
            Player.StartCoroutine(Pattern_5/*CreateEnemies*/(launchTime + 3.431602f, blackboard, (c,i,t,l, castc, casti) => 150f/*out*/, "node_6.output_enemy"));
            if (launchTime + 3.850086 > Game.Music.Time) yield return Wait(launchTime + 3.850086f);
            Player.StartCoroutine(Pattern_6/*Shoot*/(launchTime + 3.850086f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 5.154905 > Game.Music.Time) yield return Wait(launchTime + 5.154905f);
            Player.StartCoroutine(Pattern_5/*CreateEnemies*/(launchTime + 5.154905f, blackboard, (c,i,t,l, castc, casti) => 200f/*out*/, "node_9.output_enemy"));
            if (launchTime + 5.559773 > Game.Music.Time) yield return Wait(launchTime + 5.559773f);
            Player.StartCoroutine(Pattern_6/*Shoot*/(launchTime + 5.559773f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 6.854837 > Game.Music.Time) yield return Wait(launchTime + 6.854837f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 6.854837f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 6.964841 > Game.Music.Time) yield return Wait(launchTime + 6.964841f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 6.964841f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 7.064514 > Game.Music.Time) yield return Wait(launchTime + 7.064514f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 7.064514f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 7.173255 > Game.Music.Time) yield return Wait(launchTime + 7.173255f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 7.173255f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value"/*out*/));
            if (launchTime + 7.279022 > Game.Music.Time) yield return Wait(launchTime + 7.279022f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 7.279022f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_15.output_enemy(ies)", 0.5745449f));
            if (launchTime + 7.873324 > Game.Music.Time) yield return Wait(launchTime + 7.873324f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 7.873324f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard, InputFunc ypos_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, ypos_input, "node_0.output_patterninput");
            object ypos = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => ypos = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_4.output_value");
            object enemy;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => enemy = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.000648022 > Game.Music.Time) yield return Wait(launchTime + 0.000648022f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.000648022f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(227f*(float)ar,(float)ypos), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_3.output_enemy(ies)", 0.1646547f));
            if (launchTime + 0.2083717 > Game.Music.Time) yield return Wait(launchTime + 0.2083717f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.2083717f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_enemy(ies)"));
            if (launchTime + 0.210928 > Game.Music.Time) yield return Wait(launchTime + 0.210928f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.210928f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-227f*(float)ar,(float)ypos), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.1779213f));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.206542 > Game.Music.Time) yield return Wait(launchTime + 0.206542f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0.206542f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4184351 > Game.Music.Time) yield return Wait(launchTime + 0.4184351f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0.4184351f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5189943 > Game.Music.Time) yield return Wait(launchTime + 0.5189943f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0.5189943f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6357136 > Game.Music.Time) yield return Wait(launchTime + 0.6357136f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0.6357136f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7434568 > Game.Music.Time) yield return Wait(launchTime + 0.7434568f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0.7434568f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8601751 > Game.Music.Time) yield return Wait(launchTime + 0.8601751f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 0.8601751f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.059499 > Game.Music.Time) yield return Wait(launchTime + 1.059499f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 1.059499f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.1772 > Game.Music.Time) yield return Wait(launchTime + 1.1772f);
            Player.StartCoroutine(Pattern_7/*SingleShot*/(launchTime + 1.1772f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*2f - 1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.007678986 > Game.Music.Time) yield return Wait(launchTime + 0.007678986f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.007678986f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.007772446 > Game.Music.Time) yield return Wait(launchTime + 0.007772446f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.007772446f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 45f/(float)c*((float)i+0.5f)-22.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.008906364 > Game.Music.Time) yield return Wait(launchTime + 0.008906364f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.008906364f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.4246474 > Game.Music.Time) yield return Wait(launchTime + 0.4246474f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 0.4246474f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.4246474 > Game.Music.Time) yield return Wait(launchTime + 0.4246474f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 0.4246474f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.8654715 > Game.Music.Time) yield return Wait(launchTime + 0.8654715f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 0.8654715f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.8654715 > Game.Music.Time) yield return Wait(launchTime + 0.8654715f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 0.8654715f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.273942 > Game.Music.Time) yield return Wait(launchTime + 1.273942f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 1.273942f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.273942 > Game.Music.Time) yield return Wait(launchTime + 1.273942f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 1.273942f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.718807 > Game.Music.Time) yield return Wait(launchTime + 1.718807f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 1.718807f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.718807 > Game.Music.Time) yield return Wait(launchTime + 1.718807f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 1.718807f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.143457 > Game.Music.Time) yield return Wait(launchTime + 2.143457f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 2.143457f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.143457 > Game.Music.Time) yield return Wait(launchTime + 2.143457f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 2.143457f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.568105 > Game.Music.Time) yield return Wait(launchTime + 2.568105f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 2.568105f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.568105 > Game.Music.Time) yield return Wait(launchTime + 2.568105f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 2.568105f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.004885 > Game.Music.Time) yield return Wait(launchTime + 3.004885f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 3.004885f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.004885 > Game.Music.Time) yield return Wait(launchTime + 3.004885f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 3.004885f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.425487 > Game.Music.Time) yield return Wait(launchTime + 3.425487f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 3.425487f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.425487 > Game.Music.Time) yield return Wait(launchTime + 3.425487f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 3.425487f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.858223 > Game.Music.Time) yield return Wait(launchTime + 3.858223f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 3.858223f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.858223 > Game.Music.Time) yield return Wait(launchTime + 3.858223f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 3.858223f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.286915 > Game.Music.Time) yield return Wait(launchTime + 4.286915f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 4.286915f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.286915 > Game.Music.Time) yield return Wait(launchTime + 4.286915f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 4.286915f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.707518 > Game.Music.Time) yield return Wait(launchTime + 4.707518f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 4.707518f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.707518 > Game.Music.Time) yield return Wait(launchTime + 4.707518f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 4.707518f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.152387 > Game.Music.Time) yield return Wait(launchTime + 5.152387f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 5.152387f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.152387 > Game.Music.Time) yield return Wait(launchTime + 5.152387f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 5.152387f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.572989 > Game.Music.Time) yield return Wait(launchTime + 5.572989f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 5.572989f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.572989 > Game.Music.Time) yield return Wait(launchTime + 5.572989f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 5.572989f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.001681 > Game.Music.Time) yield return Wait(launchTime + 6.001681f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 6.001681f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.001681 > Game.Music.Time) yield return Wait(launchTime + 6.001681f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 6.001681f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.43846 > Game.Music.Time) yield return Wait(launchTime + 6.43846f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 6.43846f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.43846 > Game.Music.Time) yield return Wait(launchTime + 6.43846f);
            Player.StartCoroutine(Pattern_9/*Shot*/(launchTime + 6.43846f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3f/255f,0x2c/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2722101f));
            if (launchTime + 0.01105833 > Game.Music.Time) yield return Wait(launchTime + 0.01105833f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.01105833f, blackboard, (c,i,t,l, castc, casti) => new Vector2(123f*(float)flipx,138f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-100f,100f),(float)rndf(-250f,-200f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x63/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x2a/255f,0x2a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8009448f));
            if (launchTime + 0.006897092 > Game.Music.Time) yield return Wait(launchTime + 0.006897092f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.006897092f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),(float)rndf(0f,200f)), (c,i,t,l, castc, casti) => 4f*(float)d-2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 300f-50f*(float)i%2f, (c,i,t,l, castc, casti) => -200f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3b/255f,0x14/255f,0x2e/255f)/*out*/, 1.065057f));
            if (launchTime + 0.001591682 > Game.Music.Time) yield return Wait(launchTime + 0.001591682f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.001591682f, blackboard, (c,i,t,l, castc, casti) => new Vector2(1f,127f), (c,i,t,l, castc, casti) => 0.1f, (c,i,t,l, castc, casti) => (float)d*4f-2f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)t*360f, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => ParticleBulletType.SmallRectangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)rndf(-10f,10f)+(float)t*360f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x82/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 1.067172f));
            if (launchTime + 1.073865 > Game.Music.Time) yield return Wait(launchTime + 1.073865f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.073865f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.04797745f));
            if (launchTime + 1.078132 > Game.Music.Time) yield return Wait(launchTime + 1.078132f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 1.078132f, blackboard/*out*/));
            if (launchTime + 1.289897 > Game.Music.Time) yield return Wait(launchTime + 1.289897f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 1.289897f, blackboard/*out*/));
            if (launchTime + 1.409593 > Game.Music.Time) yield return Wait(launchTime + 1.409593f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 1.409593f, blackboard/*out*/));
            if (launchTime + 1.711662 > Game.Music.Time) yield return Wait(launchTime + 1.711662f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.711662f, blackboard, (c,i,t,l, castc, casti) => new Vector2(1f,127f), (c,i,t,l, castc, casti) => 0.1f, (c,i,t,l, castc, casti) => (float)d*5f-3f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)t*360f, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => ParticleBulletType.SmallRectangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)rndf(-10f,10f)+(float)t*360f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xb4/255f,0x82/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)", 1.064345f));
            if (launchTime + 1.712369 > Game.Music.Time) yield return Wait(launchTime + 1.712369f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.712369f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3b/255f,0x14/255f,0x2e/255f)/*out*/, 1.063782f));
            if (launchTime + 2.778962 > Game.Music.Time) yield return Wait(launchTime + 2.778962f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.778962f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.04979324f));
            if (launchTime + 2.790675 > Game.Music.Time) yield return Wait(launchTime + 2.790675f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 2.790675f, blackboard/*out*/));
            if (launchTime + 3.007043 > Game.Music.Time) yield return Wait(launchTime + 3.007043f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 3.007043f, blackboard/*out*/));
            if (launchTime + 3.216994 > Game.Music.Time) yield return Wait(launchTime + 3.216994f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 3.216994f, blackboard/*out*/));
            if (launchTime + 3.322747 > Game.Music.Time) yield return Wait(launchTime + 3.322747f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 3.322747f, blackboard/*out*/));
            if (launchTime + 3.648147 > Game.Music.Time) yield return Wait(launchTime + 3.648147f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 3.648147f, blackboard/*out*/));
            if (launchTime + 3.87592 > Game.Music.Time) yield return Wait(launchTime + 3.87592f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 3.87592f, blackboard/*out*/));
            if (launchTime + 4.077666 > Game.Music.Time) yield return Wait(launchTime + 4.077666f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 4.077666f, blackboard/*out*/));
            if (launchTime + 4.196433 > Game.Music.Time) yield return Wait(launchTime + 4.196433f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 4.196433f, blackboard/*out*/));
            if (launchTime + 4.50068 > Game.Music.Time) yield return Wait(launchTime + 4.50068f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 4.50068f, blackboard/*out*/));
            if (launchTime + 4.721948 > Game.Music.Time) yield return Wait(launchTime + 4.721948f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 4.721948f, blackboard/*out*/));
            if (launchTime + 4.939967 > Game.Music.Time) yield return Wait(launchTime + 4.939967f);
            Player.StartCoroutine(Pattern_12/*Shot*/(launchTime + 4.939967f, blackboard/*out*/));
            if (launchTime + 5.148103 > Game.Music.Time) yield return Wait(launchTime + 5.148103f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 5.148103f, blackboard/*out*/));
            if (launchTime + 5.366893 > Game.Music.Time) yield return Wait(launchTime + 5.366893f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 5.366893f, blackboard/*out*/));
            if (launchTime + 5.56818 > Game.Music.Time) yield return Wait(launchTime + 5.56818f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 5.56818f, blackboard/*out*/));
            if (launchTime + 5.786651 > Game.Music.Time) yield return Wait(launchTime + 5.786651f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 5.786651f, blackboard/*out*/));
            if (launchTime + 6.005119 > Game.Music.Time) yield return Wait(launchTime + 6.005119f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 6.005119f, blackboard/*out*/));
            if (launchTime + 6.221134 > Game.Music.Time) yield return Wait(launchTime + 6.221134f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 6.221134f, blackboard/*out*/));
            if (launchTime + 6.432238 > Game.Music.Time) yield return Wait(launchTime + 6.432238f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 6.432238f, blackboard/*out*/));
            if (launchTime + 6.650711 > Game.Music.Time) yield return Wait(launchTime + 6.650711f);
            Player.StartCoroutine(Pattern_13/*Shot2*/(launchTime + 6.650711f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-1f,129f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-20f,20f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.0004310608 > Game.Music.Time) yield return Wait(launchTime + 0.0004310608f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0004310608f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-1f,129f), (c,i,t,l, castc, casti) => 3f*(float)d - 3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x19/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-1f,116f), (c,i,t,l, castc, casti) => 5f*2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)angle+360f/(float)toint((float)c,2f*(float)d)*((float)i%(2f*(float)d)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x54/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.003068924 > Game.Music.Time) yield return Wait(launchTime + 0.003068924f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.003068924f, blackboard, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x14/255f,0x35/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1671886f));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object bullets;
            blackboard.SubscribeForChanges("node_12.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_12.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 0.02157021 > Game.Music.Time) yield return Wait(launchTime + 0.02157021f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.02157021f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.107f*4f/(2f-(float)toint((float)d,3f)), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)shw-21f,(float)rndf(-239f,239f)), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(-150f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 6.846247f));
            if (launchTime + 0.1208858 > Game.Music.Time) yield return Wait(launchTime + 0.1208858f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.1208858f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.107f*4f/(2f-(float)toint((float)d,3f)), (c,i,t,l, castc, casti) => (float)toint((float)d+1f,3f), (c,i,t,l, castc, casti) => new Vector2(-(float)shw+20f,(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(150f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)", 6.740089f));
            if (launchTime + 0.2293205 > Game.Music.Time) yield return Wait(launchTime + 0.2293205f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.2293205f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.107f*4f, (c,i,t,l, castc, casti) => (float)toint((float)d,3f), (c,i,t,l, castc, casti) => new Vector2((float)rndf(-(float)shw+20f,(float)shw-20f),240f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)", 6.635151f));
            if (launchTime + 0.3290882 > Game.Music.Time) yield return Wait(launchTime + 0.3290882f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.3290882f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.107f*4f, (c,i,t,l, castc, casti) => (float)toint((float)d,3f), (c,i,t,l, castc, casti) => new Vector2((float)rndf(-(float)shw+20f,(float)shw-20f),-240f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)", 6.533382f));
            if (launchTime + 0.4241124 > Game.Music.Time) yield return Wait(launchTime + 0.4241124f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 0.4241124f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 0.846819 > Game.Music.Time) yield return Wait(launchTime + 0.846819f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 0.846819f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 1.28142 > Game.Music.Time) yield return Wait(launchTime + 1.28142f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 1.28142f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 1.716498 > Game.Music.Time) yield return Wait(launchTime + 1.716498f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 1.716498f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 2.134279 > Game.Music.Time) yield return Wait(launchTime + 2.134279f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 2.134279f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 2.465916 > Game.Music.Time) yield return Wait(launchTime + 2.465916f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 2.465916f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 2.884188 > Game.Music.Time) yield return Wait(launchTime + 2.884188f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 2.884188f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 3.318897 > Game.Music.Time) yield return Wait(launchTime + 3.318897f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 3.318897f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 3.738665 > Game.Music.Time) yield return Wait(launchTime + 3.738665f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 3.738665f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 4.173375 > Game.Music.Time) yield return Wait(launchTime + 4.173375f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 4.173375f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 4.603598 > Game.Music.Time) yield return Wait(launchTime + 4.603598f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 4.603598f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 5.024858 > Game.Music.Time) yield return Wait(launchTime + 5.024858f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 5.024858f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 5.453593 > Game.Music.Time) yield return Wait(launchTime + 5.453593f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 5.453593f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 5.885314 > Game.Music.Time) yield return Wait(launchTime + 5.885314f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 5.885314f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 6.314045 > Game.Music.Time) yield return Wait(launchTime + 6.314045f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 6.314045f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 6.754731 > Game.Music.Time) yield return Wait(launchTime + 6.754731f);
            Player.StartCoroutine(Pattern_40/*4Flash*/(launchTime + 6.754731f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,140f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x7b/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.004747391 > Game.Music.Time) yield return Wait(launchTime + 0.004747391f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.004747391f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3102646f));
            if (launchTime + 0.4212341 > Game.Music.Time) yield return Wait(launchTime + 0.4212341f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.4212341f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2657394f));
            if (launchTime + 0.4257469 > Game.Music.Time) yield return Wait(launchTime + 0.4257469f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.4257469f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*8f-4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x74/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.427103 > Game.Music.Time) yield return Wait(launchTime + 0.427103f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.427103f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 0.8508186 > Game.Music.Time) yield return Wait(launchTime + 0.8508186f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.8508186f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1793003f));
            if (launchTime + 0.8588066 > Game.Music.Time) yield return Wait(launchTime + 0.8588066f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.8588066f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            if (launchTime + 1.065609 > Game.Music.Time) yield return Wait(launchTime + 1.065609f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.065609f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1845379f));
            if (launchTime + 1.079199 > Game.Music.Time) yield return Wait(launchTime + 1.079199f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.079199f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*8f-4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)"));
            if (launchTime + 1.280401 > Game.Music.Time) yield return Wait(launchTime + 1.280401f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.280401f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.08460999f));
            if (launchTime + 1.286487 > Game.Music.Time) yield return Wait(launchTime + 1.286487f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.286487f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*8f-4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_bullet(s)"));
            if (launchTime + 1.386743 > Game.Music.Time) yield return Wait(launchTime + 1.386743f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.386743f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*8f-4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_12.output_bullet(s)"));
            if (launchTime + 1.392721 > Game.Music.Time) yield return Wait(launchTime + 1.392721f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.392721f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07453918f));
            if (launchTime + 1.49768 > Game.Music.Time) yield return Wait(launchTime + 1.49768f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.49768f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07453918f));
            if (launchTime + 1.506319 > Game.Music.Time) yield return Wait(launchTime + 1.506319f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.506319f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*8f-4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_15.output_bullet(s)"));
            if (launchTime + 1.604282 > Game.Music.Time) yield return Wait(launchTime + 1.604282f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.604282f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07453918f));
            if (launchTime + 1.609747 > Game.Music.Time) yield return Wait(launchTime + 1.609747f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.609747f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*8f-4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_17.output_bullet(s)"));
            if (launchTime + 1.623481 > Game.Music.Time) yield return Wait(launchTime + 1.623481f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.623481f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullets_output, "node_54.output_value");
            object bullet1;
            blackboard.SubscribeForChanges("node_11.output_value", (b, o) => bullet1 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_value");
            object bullet2;
            blackboard.SubscribeForChanges("node_13.output_value", (b, o) => bullet2 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_value");
            object bullet3;
            blackboard.SubscribeForChanges("node_16.output_value", (b, o) => bullet3 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_16.output_value");
            object bullet4;
            blackboard.SubscribeForChanges("node_20.output_value", (b, o) => bullet4 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_20.output_value");
            object bullet5;
            blackboard.SubscribeForChanges("node_23.output_value", (b, o) => bullet5 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_23.output_value");
            object bullet6;
            blackboard.SubscribeForChanges("node_26.output_value", (b, o) => bullet6 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_6.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_26.output_value");
            object bullet7;
            blackboard.SubscribeForChanges("node_29.output_value", (b, o) => bullet7 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_29.output_value");
            object bullet8;
            blackboard.SubscribeForChanges("node_32.output_value", (b, o) => bullet8 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_8.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_32.output_value");
            object bullet9;
            blackboard.SubscribeForChanges("node_35.output_value", (b, o) => bullet9 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_9.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_35.output_value");
            object bullet10;
            blackboard.SubscribeForChanges("node_39.output_value", (b, o) => bullet10 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_39.output_value");
            object temp;
            blackboard.SubscribeForChanges("node_49.output_value", (b, o) => temp = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_11.output_value", (c,i,t,l, castc, casti) => "node_13.output_value", (c,i,t,l, castc, casti) => "node_16.output_value", (c,i,t,l, castc, casti) => "node_20.output_value", (c,i,t,l, castc, casti) => "node_23.output_value", (c,i,t,l, castc, casti) => "node_26.output_value", (c,i,t,l, castc, casti) => "node_29.output_value", (c,i,t,l, castc, casti) => "node_32.output_value"/*out*/, "node_49.output_value");
            object allbullets;
            blackboard.SubscribeForChanges("node_54.output_value", (b, o) => allbullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_49.output_value", (c,i,t,l, castc, casti) => "node_35.output_value", (c,i,t,l, castc, casti) => "node_39.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_54.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 0f/*out*/, "node_1.output_bullet"));
            if (launchTime + 0.1163635 > Game.Music.Time) yield return Wait(launchTime + 0.1163635f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 0.1163635f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_2.output_bullet"));
            if (launchTime + 0.3381805 > Game.Music.Time) yield return Wait(launchTime + 0.3381805f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 0.3381805f, blackboard, (c,i,t,l, castc, casti) => 2f/*out*/, "node_3.output_bullet"));
            if (launchTime + 0.6327436 > Game.Music.Time) yield return Wait(launchTime + 0.6327436f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 0.6327436f, blackboard, (c,i,t,l, castc, casti) => 3f/*out*/, "node_4.output_bullet"));
            if (launchTime + 0.8581996 > Game.Music.Time) yield return Wait(launchTime + 0.8581996f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 0.8581996f, blackboard, (c,i,t,l, castc, casti) => 4f/*out*/, "node_5.output_bullet"));
            if (launchTime + 1.080017 > Game.Music.Time) yield return Wait(launchTime + 1.080017f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 1.080017f, blackboard, (c,i,t,l, castc, casti) => 5f/*out*/, "node_6.output_bullet"));
            if (launchTime + 1.295945 > Game.Music.Time) yield return Wait(launchTime + 1.295945f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 1.295945f, blackboard, (c,i,t,l, castc, casti) => 6f/*out*/, "node_7.output_bullet"));
            if (launchTime + 1.720024 > Game.Music.Time) yield return Wait(launchTime + 1.720024f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 1.720024f, blackboard, (c,i,t,l, castc, casti) => 7f/*out*/, "node_8.output_bullet"));
            if (launchTime + 2.149125 > Game.Music.Time) yield return Wait(launchTime + 2.149125f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 2.149125f, blackboard, (c,i,t,l, castc, casti) => 8f/*out*/, "node_9.output_bullet"));
            if (launchTime + 2.581864 > Game.Music.Time) yield return Wait(launchTime + 2.581864f);
            Player.StartCoroutine(Pattern_17/*ShowBullet*/(launchTime + 2.581864f, blackboard, (c,i,t,l, castc, casti) => 9f/*out*/, "node_10.output_bullet"));
            if (launchTime + 2.962762 > Game.Music.Time) yield return Wait(launchTime + 2.962762f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 2.962762f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_value"/*out*/));
            if (launchTime + 3.068497 > Game.Music.Time) yield return Wait(launchTime + 3.068497f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.068497f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_value"/*out*/));
            if (launchTime + 3.176392 > Game.Music.Time) yield return Wait(launchTime + 3.176392f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.176392f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_value"/*out*/));
            if (launchTime + 3.295625 > Game.Music.Time) yield return Wait(launchTime + 3.295625f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.295625f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 3.39499 > Game.Music.Time) yield return Wait(launchTime + 3.39499f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.39499f, blackboard, (c,i,t,l, castc, casti) => "node_23.output_value"/*out*/));
            if (launchTime + 3.500031 > Game.Music.Time) yield return Wait(launchTime + 3.500031f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.500031f, blackboard, (c,i,t,l, castc, casti) => "node_26.output_value"/*out*/));
            if (launchTime + 3.622116 > Game.Music.Time) yield return Wait(launchTime + 3.622116f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.622116f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_value"/*out*/));
            if (launchTime + 3.71013 > Game.Music.Time) yield return Wait(launchTime + 3.71013f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.71013f, blackboard, (c,i,t,l, castc, casti) => "node_32.output_value"/*out*/));
            if (launchTime + 3.815178 > Game.Music.Time) yield return Wait(launchTime + 3.815178f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.815178f, blackboard, (c,i,t,l, castc, casti) => "node_35.output_value"/*out*/));
            if (launchTime + 3.919678 > Game.Music.Time) yield return Wait(launchTime + 3.919678f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.919678f, blackboard, (c,i,t,l, castc, casti) => "node_39.output_value"/*out*/));
            if (launchTime + 4.040512 > Game.Music.Time) yield return Wait(launchTime + 4.040512f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.040512f, blackboard, (c,i,t,l, castc, casti) => "node_35.output_value"/*out*/));
            if (launchTime + 4.137933 > Game.Music.Time) yield return Wait(launchTime + 4.137933f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.137933f, blackboard, (c,i,t,l, castc, casti) => "node_32.output_value"/*out*/));
            if (launchTime + 4.242059 > Game.Music.Time) yield return Wait(launchTime + 4.242059f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.242059f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_value"/*out*/));
            if (launchTime + 4.365921 > Game.Music.Time) yield return Wait(launchTime + 4.365921f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.365921f, blackboard, (c,i,t,l, castc, casti) => "node_26.output_value"/*out*/));
            if (launchTime + 4.465744 > Game.Music.Time) yield return Wait(launchTime + 4.465744f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.465744f, blackboard, (c,i,t,l, castc, casti) => "node_23.output_value"/*out*/));
            if (launchTime + 4.587791 > Game.Music.Time) yield return Wait(launchTime + 4.587791f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.587791f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 4.68213 > Game.Music.Time) yield return Wait(launchTime + 4.68213f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.68213f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_value"/*out*/));
            if (launchTime + 4.783334 > Game.Music.Time) yield return Wait(launchTime + 4.783334f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.783334f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_value"/*out*/));
            if (launchTime + 4.892479 > Game.Music.Time) yield return Wait(launchTime + 4.892479f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.892479f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_value"/*out*/));
            if (launchTime + 4.991096 > Game.Music.Time) yield return Wait(launchTime + 4.991096f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.991096f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_value"/*out*/));
            if (launchTime + 5.103974 > Game.Music.Time) yield return Wait(launchTime + 5.103974f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.103974f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_value"/*out*/));
            if (launchTime + 5.21505 > Game.Music.Time) yield return Wait(launchTime + 5.21505f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.21505f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_value"/*out*/));
            if (launchTime + 5.324509 > Game.Music.Time) yield return Wait(launchTime + 5.324509f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.324509f, blackboard, (c,i,t,l, castc, casti) => "node_23.output_value"/*out*/));
            if (launchTime + 5.422631 > Game.Music.Time) yield return Wait(launchTime + 5.422631f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.422631f, blackboard, (c,i,t,l, castc, casti) => "node_26.output_value"/*out*/));
            if (launchTime + 5.538552 > Game.Music.Time) yield return Wait(launchTime + 5.538552f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.538552f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_value"/*out*/));
            if (launchTime + 5.639436 > Game.Music.Time) yield return Wait(launchTime + 5.639436f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.639436f, blackboard, (c,i,t,l, castc, casti) => "node_32.output_value"/*out*/));
            if (launchTime + 5.756181 > Game.Music.Time) yield return Wait(launchTime + 5.756181f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.756181f, blackboard, (c,i,t,l, castc, casti) => "node_35.output_value"/*out*/));
            if (launchTime + 5.852753 > Game.Music.Time) yield return Wait(launchTime + 5.852753f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.852753f, blackboard, (c,i,t,l, castc, casti) => "node_39.output_value"/*out*/));
            if (launchTime + 5.954026 > Game.Music.Time) yield return Wait(launchTime + 5.954026f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 5.954026f, blackboard, (c,i,t,l, castc, casti) => "node_35.output_value"/*out*/));
            if (launchTime + 6.09169 > Game.Music.Time) yield return Wait(launchTime + 6.09169f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 6.09169f, blackboard, (c,i,t,l, castc, casti) => "node_32.output_value"/*out*/));
            if (launchTime + 6.172005 > Game.Music.Time) yield return Wait(launchTime + 6.172005f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 6.172005f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_value"/*out*/));
            if (launchTime + 6.286721 > Game.Music.Time) yield return Wait(launchTime + 6.286721f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 6.286721f, blackboard, (c,i,t,l, castc, casti) => "node_26.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc positionnum_input, string bullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, positionnum_input, "node_0.output_patterninput");
            object positionnum = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => positionnum = o);

            SubscribeOutput(outboard, blackboard, bullet_output, "node_2.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,300f+(150f*((float)ar-1f))), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 140f/9f*(float)positionnum-70f, (c,i,t,l, castc, casti) => 300f*(float)ar, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.004051209 > Game.Music.Time) yield return Wait(launchTime + 0.004051209f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.004051209f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x65/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard, InputFunc caster_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, caster_input, "node_0.output_patterninput");
            object caster = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => caster = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 1.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.03787613 > Game.Music.Time) yield return Wait(launchTime + 0.03787613f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.03787613f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => -90f+(float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x84/255f,0xad/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.03865624 > Game.Music.Time) yield return Wait(launchTime + 0.03865624f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.03865624f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 90f+(float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x84/255f,0xad/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.09017754 > Game.Music.Time) yield return Wait(launchTime + 0.09017754f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.09017754f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.1174925 > Game.Music.Time) yield return Wait(launchTime + 0.1174925f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 0.1174925f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3256529 > Game.Music.Time) yield return Wait(launchTime + 0.3256529f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 0.3256529f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6557156 > Game.Music.Time) yield return Wait(launchTime + 0.6557156f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 0.6557156f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8623806 > Game.Music.Time) yield return Wait(launchTime + 0.8623806f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 0.8623806f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.066071 > Game.Music.Time) yield return Wait(launchTime + 1.066071f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 1.066071f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.293549 > Game.Music.Time) yield return Wait(launchTime + 1.293549f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 1.293549f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.733643 > Game.Music.Time) yield return Wait(launchTime + 1.733643f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 1.733643f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.149948 > Game.Music.Time) yield return Wait(launchTime + 2.149948f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 2.149948f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.567726 > Game.Music.Time) yield return Wait(launchTime + 2.567726f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 2.567726f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.000389 > Game.Music.Time) yield return Wait(launchTime + 3.000389f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 3.000389f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.107437 > Game.Music.Time) yield return Wait(launchTime + 3.107437f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 3.107437f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.212998 > Game.Music.Time) yield return Wait(launchTime + 3.212998f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 3.212998f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.543068 > Game.Music.Time) yield return Wait(launchTime + 3.543068f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 3.543068f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.295388 > Game.Music.Time) yield return Wait(launchTime + 4.295388f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 4.295388f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.507996 > Game.Music.Time) yield return Wait(launchTime + 4.507996f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 4.507996f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.575508 > Game.Music.Time) yield return Wait(launchTime + 5.575508f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 5.575508f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.689987 > Game.Music.Time) yield return Wait(launchTime + 5.689987f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 5.689987f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.895172 > Game.Music.Time) yield return Wait(launchTime + 5.895172f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 5.895172f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.999245 > Game.Music.Time) yield return Wait(launchTime + 5.999245f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 5.999245f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.232666 > Game.Music.Time) yield return Wait(launchTime + 6.232666f);
            Player.StartCoroutine(Pattern_20/*Lazer*/(launchTime + 6.232666f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.48666 > Game.Music.Time) yield return Wait(launchTime + 6.48666f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 6.48666f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_23.output_bullet(s)"));
            if (launchTime + 6.49384 > Game.Music.Time) yield return Wait(launchTime + 6.49384f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 6.49384f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)toint((float)d,3f)+1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.005615242 > Game.Music.Time) yield return Wait(launchTime + 0.005615242f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.005615242f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 1.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.04677201 > Game.Music.Time) yield return Wait(launchTime + 0.04677201f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.04677201f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+(float)rndf(-30f,30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+10f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xd4/255f,0x00/255f,0x7b/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.0467949 > Game.Music.Time) yield return Wait(launchTime + 0.0467949f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0467949f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 90f+(float)rndf(-30f,30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+10f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xd4/255f,0x00/255f,0x7b/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.1453018 > Game.Music.Time) yield return Wait(launchTime + 0.1453018f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.1453018f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_9.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*Collect*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.720703 > Game.Music.Time) yield return Wait(launchTime + 1.720703f);
            Player.StartCoroutine(Pattern_24/*TwoBeats*/(launchTime + 1.720703f, blackboard/*out*/));
            if (launchTime + 3.422242 > Game.Music.Time) yield return Wait(launchTime + 3.422242f);
            Player.StartCoroutine(Pattern_22/*Collect*/(launchTime + 3.422242f, blackboard/*out*/));
            if (launchTime + 5.15107 > Game.Music.Time) yield return Wait(launchTime + 5.15107f);
            Player.StartCoroutine(Pattern_26/*4Beats*/(launchTime + 5.15107f, blackboard/*out*/));
            if (launchTime + 6.865746 > Game.Music.Time) yield return Wait(launchTime + 6.865746f);
            Player.StartCoroutine(Pattern_22/*Collect*/(launchTime + 6.865746f, blackboard/*out*/));
            if (launchTime + 8.592772 > Game.Music.Time) yield return Wait(launchTime + 8.592772f);
            Player.StartCoroutine(Pattern_24/*TwoBeats*/(launchTime + 8.592772f, blackboard/*out*/));
            if (launchTime + 9.029999 > Game.Music.Time) yield return Wait(launchTime + 9.029999f);
            Player.StartCoroutine(Pattern_27/*Break*/(launchTime + 9.029999f, blackboard/*out*/));
            if (launchTime + 10.29768 > Game.Music.Time) yield return Wait(launchTime + 10.29768f);
            Player.StartCoroutine(Pattern_22/*Collect*/(launchTime + 10.29768f, blackboard/*out*/));
            if (launchTime + 12.01121 > Game.Music.Time) yield return Wait(launchTime + 12.01121f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 12.01121f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_10.output_enemy(ies)", 0.09040833f));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.1086349 > Game.Music.Time) yield return Wait(launchTime + 0.1086349f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.1086349f, blackboard/*out*/));
            if (launchTime + 0.2139969 > Game.Music.Time) yield return Wait(launchTime + 0.2139969f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.2139969f, blackboard/*out*/));
            if (launchTime + 0.3133469 > Game.Music.Time) yield return Wait(launchTime + 0.3133469f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.3133469f, blackboard/*out*/));
            if (launchTime + 0.4187088 > Game.Music.Time) yield return Wait(launchTime + 0.4187088f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.4187088f, blackboard/*out*/));
            if (launchTime + 0.5541686 > Game.Music.Time) yield return Wait(launchTime + 0.5541686f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.5541686f, blackboard/*out*/));
            if (launchTime + 0.641472 > Game.Music.Time) yield return Wait(launchTime + 0.641472f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.641472f, blackboard/*out*/));
            if (launchTime + 0.7468337 > Game.Music.Time) yield return Wait(launchTime + 0.7468337f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 0.7468337f, blackboard/*out*/));
            if (launchTime + 1.070061 > Game.Music.Time) yield return Wait(launchTime + 1.070061f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 1.070061f, blackboard/*out*/));
            if (launchTime + 1.40007 > Game.Music.Time) yield return Wait(launchTime + 1.40007f);
            Player.StartCoroutine(Pattern_23/*Bullet*/(launchTime + 1.40007f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);

            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_1.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,168f), (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => (float)angle+360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)angle-180f+360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x40/255f), (c,i,t,l, castc, casti) => 0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => 0.5f/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.002986908 > Game.Music.Time) yield return Wait(launchTime + 0.002986908f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.002986908f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0.025f, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.02139282f));
            if (launchTime + 0.4756165 > Game.Music.Time) yield return Wait(launchTime + 0.4756165f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.4756165f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_0.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1.125f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_2.output_enemy(ies)", 0.02139664f));
            if (launchTime + 0.0002822876 > Game.Music.Time) yield return Wait(launchTime + 0.0002822876f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.0002822876f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07953262f));
            if (launchTime + 0.005523678 > Game.Music.Time) yield return Wait(launchTime + 0.005523678f);
            Player.StartCoroutine(Pattern_25/*Beat*/(launchTime + 0.005523678f, blackboard/*out*/));
            if (launchTime + 0.218628 > Game.Music.Time) yield return Wait(launchTime + 0.218628f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.218628f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.09986496f));
            if (launchTime + 0.2206192 > Game.Music.Time) yield return Wait(launchTime + 0.2206192f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.2206192f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.0369911f));
            if (launchTime + 0.2255555 > Game.Music.Time) yield return Wait(launchTime + 0.2255555f);
            Player.StartCoroutine(Pattern_25/*Beat*/(launchTime + 0.2255555f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(1f,135f), (c,i,t,l, castc, casti) => (float)d*10f-6f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xdb/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.0002555843 > Game.Music.Time) yield return Wait(launchTime + 0.0002555843f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0002555843f, blackboard, (c,i,t,l, castc, casti) => new Vector2(1f,135f), (c,i,t,l, castc, casti) => (float)d*10f-6f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x25/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_0.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1.1875f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 0.09474945f));
            if (launchTime + 0.002731324 > Game.Music.Time) yield return Wait(launchTime + 0.002731324f);
            Player.StartCoroutine(Pattern_25/*Beat*/(launchTime + 0.002731324f, blackboard/*out*/));
            if (launchTime + 0.4313202 > Game.Music.Time) yield return Wait(launchTime + 0.4313202f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.4313202f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1.125f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.09474948f));
            if (launchTime + 0.4428177 > Game.Music.Time) yield return Wait(launchTime + 0.4428177f);
            Player.StartCoroutine(Pattern_25/*Beat*/(launchTime + 0.4428177f, blackboard/*out*/));
            if (launchTime + 0.8546485 > Game.Music.Time) yield return Wait(launchTime + 0.8546485f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.8546485f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1.0625f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.09474945f));
            if (launchTime + 0.8575097 > Game.Music.Time) yield return Wait(launchTime + 0.8575097f);
            Player.StartCoroutine(Pattern_25/*Beat*/(launchTime + 0.8575097f, blackboard/*out*/));
            if (launchTime + 1.29995 > Game.Music.Time) yield return Wait(launchTime + 1.29995f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.29995f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1.125f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_8.output_enemy(ies)", 0.09474945f));
            if (launchTime + 1.306049 > Game.Music.Time) yield return Wait(launchTime + 1.306049f);
            Player.StartCoroutine(Pattern_25/*Beat*/(launchTime + 1.306049f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.1243325 > Game.Music.Time) yield return Wait(launchTime + 0.1243325f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.1243325f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.1243325 > Game.Music.Time) yield return Wait(launchTime + 0.1243325f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.1243325f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.2160188 > Game.Music.Time) yield return Wait(launchTime + 0.2160188f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.2160188f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.2160188 > Game.Music.Time) yield return Wait(launchTime + 0.2160188f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.2160188f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.3164254 > Game.Music.Time) yield return Wait(launchTime + 0.3164254f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.3164254f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.3164254 > Game.Music.Time) yield return Wait(launchTime + 0.3164254f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.3164254f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.4473915 > Game.Music.Time) yield return Wait(launchTime + 0.4473915f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.4473915f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.4473915 > Game.Music.Time) yield return Wait(launchTime + 0.4473915f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.4473915f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.5216217 > Game.Music.Time) yield return Wait(launchTime + 0.5216217f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.5216217f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.5216217 > Game.Music.Time) yield return Wait(launchTime + 0.5216217f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.5216217f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.6351355 > Game.Music.Time) yield return Wait(launchTime + 0.6351355f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.6351355f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.6351355 > Game.Music.Time) yield return Wait(launchTime + 0.6351355f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.6351355f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.7530056 > Game.Music.Time) yield return Wait(launchTime + 0.7530056f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.7530056f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.7530056 > Game.Music.Time) yield return Wait(launchTime + 0.7530056f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.7530056f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.8708875 > Game.Music.Time) yield return Wait(launchTime + 0.8708875f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.8708875f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.8708875 > Game.Music.Time) yield return Wait(launchTime + 0.8708875f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.8708875f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.067349 > Game.Music.Time) yield return Wait(launchTime + 1.067349f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.067349f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.067349 > Game.Music.Time) yield return Wait(launchTime + 1.067349f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.067349f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.189591 > Game.Music.Time) yield return Wait(launchTime + 1.189591f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.189591f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.189591 > Game.Music.Time) yield return Wait(launchTime + 1.189591f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.189591f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.290009 > Game.Music.Time) yield return Wait(launchTime + 1.290009f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.290009f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.290009 > Game.Music.Time) yield return Wait(launchTime + 1.290009f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.290009f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-87f*(float)flipx,167f), (c,i,t,l, castc, casti) => (float)toint((float)d,3f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-100f,100f),(float)rndf(0f,50f)), (c,i,t,l, castc, casti) => new Vector2(0f,-100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xf3/255f,0xd5/255f,0x6e/255f), (c,i,t,l, castc, casti) => 0.8f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.5425795 > Game.Music.Time) yield return Wait(launchTime + 0.5425795f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.5425795f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.5425795 > Game.Music.Time) yield return Wait(launchTime + 0.5425795f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.5425795f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.9576412 > Game.Music.Time) yield return Wait(launchTime + 0.9576412f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.9576412f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.9576412 > Game.Music.Time) yield return Wait(launchTime + 0.9576412f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.9576412f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.076999 > Game.Music.Time) yield return Wait(launchTime + 1.076999f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.076999f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.076999 > Game.Music.Time) yield return Wait(launchTime + 1.076999f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.076999f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.182815 > Game.Music.Time) yield return Wait(launchTime + 1.182815f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.182815f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.182815 > Game.Music.Time) yield return Wait(launchTime + 1.182815f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.182815f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.291313 > Game.Music.Time) yield return Wait(launchTime + 1.291313f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.291313f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.291313 > Game.Music.Time) yield return Wait(launchTime + 1.291313f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.291313f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.399818 > Game.Music.Time) yield return Wait(launchTime + 1.399818f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.399818f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.399818 > Game.Music.Time) yield return Wait(launchTime + 1.399818f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.399818f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.500179 > Game.Music.Time) yield return Wait(launchTime + 1.500179f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.500179f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.500179 > Game.Music.Time) yield return Wait(launchTime + 1.500179f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.500179f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.608707 > Game.Music.Time) yield return Wait(launchTime + 1.608707f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.608707f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.608707 > Game.Music.Time) yield return Wait(launchTime + 1.608707f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.608707f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.828464 > Game.Music.Time) yield return Wait(launchTime + 1.828464f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.828464f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.828464 > Game.Music.Time) yield return Wait(launchTime + 1.828464f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.828464f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.040054 > Game.Music.Time) yield return Wait(launchTime + 2.040054f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.040054f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.040054 > Game.Music.Time) yield return Wait(launchTime + 2.040054f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.040054f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.24894 > Game.Music.Time) yield return Wait(launchTime + 2.24894f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.24894f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.24894 > Game.Music.Time) yield return Wait(launchTime + 2.24894f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.24894f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.365585 > Game.Music.Time) yield return Wait(launchTime + 2.365585f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.365585f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.365585 > Game.Music.Time) yield return Wait(launchTime + 2.365585f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.365585f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.471382 > Game.Music.Time) yield return Wait(launchTime + 2.471382f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.471382f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.471382 > Game.Music.Time) yield return Wait(launchTime + 2.471382f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.471382f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.579883 > Game.Music.Time) yield return Wait(launchTime + 2.579883f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.579883f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.579883 > Game.Music.Time) yield return Wait(launchTime + 2.579883f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.579883f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.685684 > Game.Music.Time) yield return Wait(launchTime + 2.685684f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.685684f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.685684 > Game.Music.Time) yield return Wait(launchTime + 2.685684f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.685684f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.796924 > Game.Music.Time) yield return Wait(launchTime + 2.796924f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.796924f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.796924 > Game.Music.Time) yield return Wait(launchTime + 2.796924f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.796924f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.005798 > Game.Music.Time) yield return Wait(launchTime + 3.005798f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 3.005798f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.005798 > Game.Music.Time) yield return Wait(launchTime + 3.005798f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 3.005798f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_31/*Swarms*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4294281 > Game.Music.Time) yield return Wait(launchTime + 0.4294281f);
            Player.StartCoroutine(Pattern_33/*Drums*/(launchTime + 0.4294281f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4302406 > Game.Music.Time) yield return Wait(launchTime + 0.4302406f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 0.4302406f, blackboard/*out*/));
            if (launchTime + 0.5901336 > Game.Music.Time) yield return Wait(launchTime + 0.5901336f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 0.5901336f, blackboard/*out*/));
            if (launchTime + 0.8510933 > Game.Music.Time) yield return Wait(launchTime + 0.8510933f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 0.8510933f, blackboard/*out*/));
            if (launchTime + 1.072101 > Game.Music.Time) yield return Wait(launchTime + 1.072101f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 1.072101f, blackboard/*out*/));
            if (launchTime + 1.276645 > Game.Music.Time) yield return Wait(launchTime + 1.276645f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 1.276645f, blackboard/*out*/));
            if (launchTime + 1.514106 > Game.Music.Time) yield return Wait(launchTime + 1.514106f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 1.514106f, blackboard/*out*/));
            if (launchTime + 2.583877 > Game.Music.Time) yield return Wait(launchTime + 2.583877f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 2.583877f, blackboard/*out*/));
            if (launchTime + 2.779018 > Game.Music.Time) yield return Wait(launchTime + 2.779018f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 2.779018f, blackboard/*out*/));
            if (launchTime + 2.957702 > Game.Music.Time) yield return Wait(launchTime + 2.957702f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 2.957702f, blackboard/*out*/));
            if (launchTime + 3.190456 > Game.Music.Time) yield return Wait(launchTime + 3.190456f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 3.190456f, blackboard/*out*/));
            if (launchTime + 3.434971 > Game.Music.Time) yield return Wait(launchTime + 3.434971f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 3.434971f, blackboard/*out*/));
            if (launchTime + 3.86993 > Game.Music.Time) yield return Wait(launchTime + 3.86993f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 3.86993f, blackboard/*out*/));
            if (launchTime + 4.072124 > Game.Music.Time) yield return Wait(launchTime + 4.072124f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 4.072124f, blackboard/*out*/));
            if (launchTime + 4.189693 > Game.Music.Time) yield return Wait(launchTime + 4.189693f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 4.189693f, blackboard/*out*/));
            if (launchTime + 4.502391 > Game.Music.Time) yield return Wait(launchTime + 4.502391f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 4.502391f, blackboard/*out*/));
            if (launchTime + 4.718689 > Game.Music.Time) yield return Wait(launchTime + 4.718689f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 4.718689f, blackboard/*out*/));
            if (launchTime + 4.937356 > Game.Music.Time) yield return Wait(launchTime + 4.937356f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 4.937356f, blackboard/*out*/));
            if (launchTime + 5.36996 > Game.Music.Time) yield return Wait(launchTime + 5.36996f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 5.36996f, blackboard/*out*/));
            if (launchTime + 5.579208 > Game.Music.Time) yield return Wait(launchTime + 5.579208f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 5.579208f, blackboard/*out*/));
            if (launchTime + 5.783752 > Game.Music.Time) yield return Wait(launchTime + 5.783752f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 5.783752f, blackboard/*out*/));
            if (launchTime + 6.225757 > Game.Music.Time) yield return Wait(launchTime + 6.225757f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 6.225757f, blackboard/*out*/));
            if (launchTime + 6.430301 > Game.Music.Time) yield return Wait(launchTime + 6.430301f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 6.430301f, blackboard/*out*/));
            if (launchTime + 6.648956 > Game.Music.Time) yield return Wait(launchTime + 6.648956f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 6.648956f, blackboard/*out*/));
            if (launchTime + 6.87701 > Game.Music.Time) yield return Wait(launchTime + 6.87701f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 6.87701f, blackboard/*out*/));
            if (launchTime + 7.497722 > Game.Music.Time) yield return Wait(launchTime + 7.497722f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 7.497722f, blackboard/*out*/));
            if (launchTime + 8.141922 > Game.Music.Time) yield return Wait(launchTime + 8.141922f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 8.141922f, blackboard/*out*/));
            if (launchTime + 8.58628 > Game.Music.Time) yield return Wait(launchTime + 8.58628f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 8.58628f, blackboard/*out*/));
            if (launchTime + 9.223426 > Game.Music.Time) yield return Wait(launchTime + 9.223426f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 9.223426f, blackboard/*out*/));
            if (launchTime + 9.855873 > Game.Music.Time) yield return Wait(launchTime + 9.855873f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 9.855873f, blackboard/*out*/));
            if (launchTime + 10.29319 > Game.Music.Time) yield return Wait(launchTime + 10.29319f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 10.29319f, blackboard/*out*/));
            if (launchTime + 10.93033 > Game.Music.Time) yield return Wait(launchTime + 10.93033f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 10.93033f, blackboard/*out*/));
            if (launchTime + 11.58159 > Game.Music.Time) yield return Wait(launchTime + 11.58159f);
            Player.StartCoroutine(Pattern_32/*Swarm*/(launchTime + 11.58159f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x1d/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09807205f));
            if (launchTime + 0.004402161 > Game.Music.Time) yield return Wait(launchTime + 0.004402161f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.004402161f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,140f), (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(1f,100f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-5f,5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.8566024 > Game.Music.Time) yield return Wait(launchTime + 0.8566024f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 0.8566024f, blackboard/*out*/));
            if (launchTime + 1.056385 > Game.Music.Time) yield return Wait(launchTime + 1.056385f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 1.056385f, blackboard/*out*/));
            if (launchTime + 1.701301 > Game.Music.Time) yield return Wait(launchTime + 1.701301f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 1.701301f, blackboard/*out*/));
            if (launchTime + 2.567027 > Game.Music.Time) yield return Wait(launchTime + 2.567027f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 2.567027f, blackboard/*out*/));
            if (launchTime + 2.770316 > Game.Music.Time) yield return Wait(launchTime + 2.770316f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 2.770316f, blackboard/*out*/));
            if (launchTime + 3.425742 > Game.Music.Time) yield return Wait(launchTime + 3.425742f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 3.425742f, blackboard/*out*/));
            if (launchTime + 4.270457 > Game.Music.Time) yield return Wait(launchTime + 4.270457f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 4.270457f, blackboard/*out*/));
            if (launchTime + 4.393128 > Game.Music.Time) yield return Wait(launchTime + 4.393128f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 4.393128f, blackboard/*out*/));
            if (launchTime + 4.5053 > Game.Music.Time) yield return Wait(launchTime + 4.5053f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 4.5053f, blackboard/*out*/));
            if (launchTime + 4.620959 > Game.Music.Time) yield return Wait(launchTime + 4.620959f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 4.620959f, blackboard/*out*/));
            if (launchTime + 5.13271 > Game.Music.Time) yield return Wait(launchTime + 5.13271f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 5.13271f, blackboard/*out*/));
            if (launchTime + 5.784635 > Game.Music.Time) yield return Wait(launchTime + 5.784635f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 5.784635f, blackboard/*out*/));
            if (launchTime + 5.994929 > Game.Music.Time) yield return Wait(launchTime + 5.994929f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 5.994929f, blackboard/*out*/));
            if (launchTime + 6.096573 > Game.Music.Time) yield return Wait(launchTime + 6.096573f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 6.096573f, blackboard/*out*/));
            if (launchTime + 6.208728 > Game.Music.Time) yield return Wait(launchTime + 6.208728f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 6.208728f, blackboard/*out*/));
            if (launchTime + 6.324374 > Game.Music.Time) yield return Wait(launchTime + 6.324374f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 6.324374f, blackboard/*out*/));
            if (launchTime + 6.850158 > Game.Music.Time) yield return Wait(launchTime + 6.850158f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 6.850158f, blackboard/*out*/));
            if (launchTime + 7.705371 > Game.Music.Time) yield return Wait(launchTime + 7.705371f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 7.705371f, blackboard/*out*/));
            if (launchTime + 7.912167 > Game.Music.Time) yield return Wait(launchTime + 7.912167f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 7.912167f, blackboard/*out*/));
            if (launchTime + 8.564087 > Game.Music.Time) yield return Wait(launchTime + 8.564087f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 8.564087f, blackboard/*out*/));
            if (launchTime + 9.412304 > Game.Music.Time) yield return Wait(launchTime + 9.412304f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 9.412304f, blackboard/*out*/));
            if (launchTime + 9.636608 > Game.Music.Time) yield return Wait(launchTime + 9.636608f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 9.636608f, blackboard/*out*/));
            if (launchTime + 10.28853 > Game.Music.Time) yield return Wait(launchTime + 10.28853f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 10.28853f, blackboard/*out*/));
            if (launchTime + 11.12268 > Game.Music.Time) yield return Wait(launchTime + 11.12268f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 11.12268f, blackboard/*out*/));
            if (launchTime + 11.2524 > Game.Music.Time) yield return Wait(launchTime + 11.2524f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 11.2524f, blackboard/*out*/));
            if (launchTime + 11.34702 > Game.Music.Time) yield return Wait(launchTime + 11.34702f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 11.34702f, blackboard/*out*/));
            if (launchTime + 11.45917 > Game.Music.Time) yield return Wait(launchTime + 11.45917f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 11.45917f, blackboard/*out*/));
            if (launchTime + 11.56784 > Game.Music.Time) yield return Wait(launchTime + 11.56784f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 11.56784f, blackboard/*out*/));
            if (launchTime + 11.98844 > Game.Music.Time) yield return Wait(launchTime + 11.98844f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 11.98844f, blackboard/*out*/));
            if (launchTime + 12.42305 > Game.Music.Time) yield return Wait(launchTime + 12.42305f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 12.42305f, blackboard/*out*/));
            if (launchTime + 12.85417 > Game.Music.Time) yield return Wait(launchTime + 12.85417f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 12.85417f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,165f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-50f,50f),(float)rndf(-150f,-200f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x61/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullets_output, "node_4.output_value");
            object bullets;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => bullets = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullets", (c,i,t,l, castc, casti) => "node_2.output_bullets", (c,i,t,l, castc, casti) => "node_3.output_bullets", (c,i,t,l, castc, casti) => "node_5.output_bullets", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/, "node_0.output_bullets"));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/, "node_0.output_bullets"));
            if (launchTime + 0.1148757 > Game.Music.Time) yield return Wait(launchTime + 0.1148757f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0.1148757f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/, "node_2.output_bullets"));
            if (launchTime + 0.1148757 > Game.Music.Time) yield return Wait(launchTime + 0.1148757f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0.1148757f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/, "node_2.output_bullets"));
            if (launchTime + 0.2210845 > Game.Music.Time) yield return Wait(launchTime + 0.2210845f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0.2210845f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/, "node_3.output_bullets"));
            if (launchTime + 0.2210845 > Game.Music.Time) yield return Wait(launchTime + 0.2210845f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0.2210845f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/, "node_3.output_bullets"));
            if (launchTime + 0.3214645 > Game.Music.Time) yield return Wait(launchTime + 0.3214645f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0.3214645f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/, "node_5.output_bullets"));
            if (launchTime + 0.3214645 > Game.Music.Time) yield return Wait(launchTime + 0.3214645f);
            Player.StartCoroutine(Pattern_36/*Appear*/(launchTime + 0.3214645f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/, "node_5.output_bullets"));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, bullets_output, "node_4.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-(float)shw+20f,-(float)shw+100f)*(float)flipx,(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object lastbullet;
            blackboard.SubscribeForChanges("node_3.output_value", (b, o) => lastbullet = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullets", (c,i,t,l, castc, casti) => "node_2.output_bullets", (c,i,t,l, castc, casti) => "node_4.output_bullets", (c,i,t,l, castc, casti) => "node_6.output_bullets", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_value");
            object bullets;
            blackboard.SubscribeForChanges("node_5.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_3.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_35/*Appear4*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bullets"));
            if (launchTime + 0.4223404 > Game.Music.Time) yield return Wait(launchTime + 0.4223404f);
            Player.StartCoroutine(Pattern_35/*Appear4*/(launchTime + 0.4223404f, blackboard/*out*/, "node_2.output_bullets"));
            if (launchTime + 0.8492966 > Game.Music.Time) yield return Wait(launchTime + 0.8492966f);
            Player.StartCoroutine(Pattern_35/*Appear4*/(launchTime + 0.8492966f, blackboard/*out*/, "node_4.output_bullets"));
            if (launchTime + 1.278221 > Game.Music.Time) yield return Wait(launchTime + 1.278221f);
            Player.StartCoroutine(Pattern_35/*Appear4*/(launchTime + 1.278221f, blackboard/*out*/, "node_6.output_bullets"));
            if (launchTime + 1.703858 > Game.Music.Time) yield return Wait(launchTime + 1.703858f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.703858f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.8691939f));
            if (launchTime + 1.714317 > Game.Music.Time) yield return Wait(launchTime + 1.714317f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 1.714317f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 140f/*out*/));
            if (launchTime + 6.833365 > Game.Music.Time) yield return Wait(launchTime + 6.833365f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 6.833365f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 13.70101 > Game.Music.Time) yield return Wait(launchTime + 13.70101f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 13.70101f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 20.54376 > Game.Music.Time) yield return Wait(launchTime + 20.54376f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 20.54376f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 25.70005 > Game.Music.Time) yield return Wait(launchTime + 25.70005f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 25.70005f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 27.40888 > Game.Music.Time) yield return Wait(launchTime + 27.40888f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 27.40888f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 30.8489 > Game.Music.Time) yield return Wait(launchTime + 30.8489f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 30.8489f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 34.26652 > Game.Music.Time) yield return Wait(launchTime + 34.26652f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 34.26652f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 41.11671 > Game.Music.Time) yield return Wait(launchTime + 41.11671f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 41.11671f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 42.82553 > Game.Music.Time) yield return Wait(launchTime + 42.82553f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 42.82553f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 140f*2f/*out*/));
            if (launchTime + 43.25086 > Game.Music.Time) yield return Wait(launchTime + 43.25086f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 43.25086f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 44.55672 > Game.Music.Time) yield return Wait(launchTime + 44.55672f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 44.55672f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 140f/*out*/));
            if (launchTime + 46.26554 > Game.Music.Time) yield return Wait(launchTime + 46.26554f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 46.26554f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 47.97437 > Game.Music.Time) yield return Wait(launchTime + 47.97437f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 47.97437f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 49.70557 > Game.Music.Time) yield return Wait(launchTime + 49.70557f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 49.70557f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 280f/*out*/));
            if (launchTime + 50.11599 > Game.Music.Time) yield return Wait(launchTime + 50.11599f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 50.11599f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 51.41438 > Game.Music.Time) yield return Wait(launchTime + 51.41438f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 51.41438f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 140f/*out*/));
            if (launchTime + 54.84695 > Game.Music.Time) yield return Wait(launchTime + 54.84695f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 54.84695f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 61.69716 > Game.Music.Time) yield return Wait(launchTime + 61.69716f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 61.69716f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 66.83855 > Game.Music.Time) yield return Wait(launchTime + 66.83855f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 66.83855f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 68.53992 > Game.Music.Time) yield return Wait(launchTime + 68.53992f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 68.53992f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 70f/*out*/));
            if (launchTime + 71.95756 > Game.Music.Time) yield return Wait(launchTime + 71.95756f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 71.95756f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 140f/*out*/));
            if (launchTime + 75.3976 > Game.Music.Time) yield return Wait(launchTime + 75.3976f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 75.3976f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 82.27019 > Game.Music.Time) yield return Wait(launchTime + 82.27019f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 82.27019f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 83.97903 > Game.Music.Time) yield return Wait(launchTime + 83.97903f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 83.97903f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 280f/*out*/));
            if (launchTime + 84.3969 > Game.Music.Time) yield return Wait(launchTime + 84.3969f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 84.3969f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 85.67292 > Game.Music.Time) yield return Wait(launchTime + 85.67292f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 85.67292f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 140f/*out*/));
            if (launchTime + 87.4116 > Game.Music.Time) yield return Wait(launchTime + 87.4116f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 87.4116f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 89.12786 > Game.Music.Time) yield return Wait(launchTime + 89.12786f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 89.12786f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 90.83668 > Game.Music.Time) yield return Wait(launchTime + 90.83668f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 90.83668f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 280f/*out*/));
            if (launchTime + 91.23965 > Game.Music.Time) yield return Wait(launchTime + 91.23965f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 91.23965f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 92.53805 > Game.Music.Time) yield return Wait(launchTime + 92.53805f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 92.53805f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 140f/*out*/));
            if (launchTime + 94.28419 > Game.Music.Time) yield return Wait(launchTime + 94.28419f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 94.28419f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d/*out*/, "node_1.output_elements"));
            if (launchTime + 0.001527786 > Game.Music.Time) yield return Wait(launchTime + 0.001527786f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.001527786f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.09857559 > Game.Music.Time) yield return Wait(launchTime + 0.09857559f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.09857559f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*Flash*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.102787 > Game.Music.Time) yield return Wait(launchTime + 0.102787f);
            Player.StartCoroutine(Pattern_39/*Flash*/(launchTime + 0.102787f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2131786 > Game.Music.Time) yield return Wait(launchTime + 0.2131786f);
            Player.StartCoroutine(Pattern_39/*Flash*/(launchTime + 0.2131786f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3177051 > Game.Music.Time) yield return Wait(launchTime + 0.3177051f);
            Player.StartCoroutine(Pattern_39/*Flash*/(launchTime + 0.3177051f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

    }
}