#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_Dash : LevelScript
    {
        protected override string ClipName {
            get { return "Dash.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0.01514354 > Game.Music.Time) yield return Wait(launchTime + 0.01514354f);
            Player.StartCoroutine(BossHealth.Act(launchTime + 0.01514354f, blackboard, (c,i,t,l, castc, casti) => "Blarbrr The Invincible", (c,i,t,l, castc, casti) => 500000f, (c,i,t,l, castc, casti) => true/*out*/));
            if (launchTime + 0.05028102 > Game.Music.Time) yield return Wait(launchTime + 0.05028102f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.05028102f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.3142007f));
            if (launchTime + 0.05262298 > Game.Music.Time) yield return Wait(launchTime + 0.05262298f);
            Player.StartCoroutine(Pattern_75/*DanceMan*/(launchTime + 0.05262298f, blackboard/*out*/));
            if (launchTime + 0.06262197 > Game.Music.Time) yield return Wait(launchTime + 0.06262197f);
            Player.StartCoroutine(Pattern_66/*Part0*/(launchTime + 0.06262197f, blackboard/*out*/));
            if (launchTime + 11.85454 > Game.Music.Time) yield return Wait(launchTime + 11.85454f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.85454f, blackboard, (c,i,t,l, castc, casti) => new Color(0x86/255f,0x88/255f,0x84/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2946491f));
            if (launchTime + 12.23961 > Game.Music.Time) yield return Wait(launchTime + 12.23961f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.23961f, blackboard, (c,i,t,l, castc, casti) => new Color(0x88/255f,0x88/255f,0x88/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2800255f));
            if (launchTime + 12.62015 > Game.Music.Time) yield return Wait(launchTime + 12.62015f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 12.62015f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.425889f));
            if (launchTime + 12.62468 > Game.Music.Time) yield return Wait(launchTime + 12.62468f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 12.62468f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2215357f));
            if (launchTime + 12.63135 > Game.Music.Time) yield return Wait(launchTime + 12.63135f);
            Player.StartCoroutine(Pattern_1/*Part1*/(launchTime + 12.63135f, blackboard/*out*/));
            if (launchTime + 35.48687 > Game.Music.Time) yield return Wait(launchTime + 35.48687f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 35.48687f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Fbss1, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(120f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_10.output_enemy(ies)"));
            if (launchTime + 37.72602 > Game.Music.Time) yield return Wait(launchTime + 37.72602f);
            Player.StartCoroutine(Pattern_11/*Part2*/(launchTime + 37.72602f, blackboard/*out*/));
            if (launchTime + 37.7351 > Game.Music.Time) yield return Wait(launchTime + 37.7351f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 37.7351f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.3031654f));
            if (launchTime + 62.12828 > Game.Music.Time) yield return Wait(launchTime + 62.12828f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 62.12828f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Fbss2, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(120f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_13.output_enemy(ies)"));
            if (launchTime + 64.36555 > Game.Music.Time) yield return Wait(launchTime + 64.36555f);
            Player.StartCoroutine(Pattern_14/*Part3*/(launchTime + 64.36555f, blackboard/*out*/));
            if (launchTime + 64.38735 > Game.Music.Time) yield return Wait(launchTime + 64.38735f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 64.38735f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.5740433f));
            if (launchTime + 75.47213 > Game.Music.Time) yield return Wait(launchTime + 75.47213f);
            Player.StartCoroutine(Pattern_19/*Transition3*/(launchTime + 75.47213f, blackboard/*out*/));
            if (launchTime + 76.53568 > Game.Music.Time) yield return Wait(launchTime + 76.53568f);
            Player.StartCoroutine(Pattern_20/*Part4*/(launchTime + 76.53568f, blackboard/*out*/));
            if (launchTime + 87.51964 > Game.Music.Time) yield return Wait(launchTime + 87.51964f);
            Player.StartCoroutine(Pattern_23/*Transition4*/(launchTime + 87.51964f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 87.51964 > Game.Music.Time) yield return Wait(launchTime + 87.51964f);
            Player.StartCoroutine(Pattern_23/*Transition4*/(launchTime + 87.51964f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 87.9041 > Game.Music.Time) yield return Wait(launchTime + 87.9041f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 87.9041f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x17/255f,0x47/255f,0x13/255f)/*out*/, 1.509842f));
            if (launchTime + 89.43681 > Game.Music.Time) yield return Wait(launchTime + 89.43681f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 89.43681f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.09304047f));
            if (launchTime + 89.4407 > Game.Music.Time) yield return Wait(launchTime + 89.4407f);
            Player.StartCoroutine(Pattern_24/*Part5*/(launchTime + 89.4407f, blackboard/*out*/));
            if (launchTime + 89.44327 > Game.Music.Time) yield return Wait(launchTime + 89.44327f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 89.44327f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2926941f));
            if (launchTime + 101.3001 > Game.Music.Time) yield return Wait(launchTime + 101.3001f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 101.3001f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Fbss3, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(120f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_23.output_enemy(ies)"));
            if (launchTime + 103.6013 > Game.Music.Time) yield return Wait(launchTime + 103.6013f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 103.6013f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2855759f));
            if (launchTime + 103.6073 > Game.Music.Time) yield return Wait(launchTime + 103.6073f);
            Player.StartCoroutine(Pattern_37/*Part6*/(launchTime + 103.6073f, blackboard/*out*/));
            if (launchTime + 127.9686 > Game.Music.Time) yield return Wait(launchTime + 127.9686f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 127.9686f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Fbss4, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(120f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_26.output_enemy(ies)"));
            if (launchTime + 129.1028 > Game.Music.Time) yield return Wait(launchTime + 129.1028f);
            Player.StartCoroutine(Pattern_78/*Lazers*/(launchTime + 129.1028f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 6f/*out*/));
            if (launchTime + 129.1028 > Game.Music.Time) yield return Wait(launchTime + 129.1028f);
            Player.StartCoroutine(Pattern_78/*Lazers*/(launchTime + 129.1028f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 6f/*out*/));
            if (launchTime + 129.4771 > Game.Music.Time) yield return Wait(launchTime + 129.4771f);
            Player.StartCoroutine(Pattern_31/*Part7*/(launchTime + 129.4771f, blackboard/*out*/));
            if (launchTime + 130.282 > Game.Music.Time) yield return Wait(launchTime + 130.282f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 130.282f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4282684f));
            if (launchTime + 143.0265 > Game.Music.Time) yield return Wait(launchTime + 143.0265f);
            Player.StartCoroutine(Pattern_42/*Part7_1*/(launchTime + 143.0265f, blackboard/*out*/));
            if (launchTime + 155.3711 > Game.Music.Time) yield return Wait(launchTime + 155.3711f);
            Player.StartCoroutine(Pattern_49/*Part8*/(launchTime + 155.3711f, blackboard/*out*/));
            if (launchTime + 167.9207 > Game.Music.Time) yield return Wait(launchTime + 167.9207f);
            Player.StartCoroutine(Pattern_56/*Part9*/(launchTime + 167.9207f, blackboard/*out*/));
            if (launchTime + 178.5842 > Game.Music.Time) yield return Wait(launchTime + 178.5842f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 178.5842f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 1.869705f));
            if (launchTime + 178.9425 > Game.Music.Time) yield return Wait(launchTime + 178.9425f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 178.9425f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0.067f, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_34.output_bullet(s)", 1.193924f));
            if (launchTime + 180.459 > Game.Music.Time) yield return Wait(launchTime + 180.459f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 180.459f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.02766418f));
            if (launchTime + 193.0033 > Game.Music.Time) yield return Wait(launchTime + 193.0033f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 193.0033f, blackboard, (c,i,t,l, castc, casti) => "node_34.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 193.0043 > Game.Music.Time) yield return Wait(launchTime + 193.0043f);
            Player.StartCoroutine(Pattern_24/*Part5*/(launchTime + 193.0043f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_2/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.004669189 > Game.Music.Time) yield return Wait(launchTime + 0.004669189f);
            Player.StartCoroutine(Pattern_4/*Guitar*/(launchTime + 0.004669189f, blackboard/*out*/));
            if (launchTime + 12.55838 > Game.Music.Time) yield return Wait(launchTime + 12.55838f);
            Player.StartCoroutine(Pattern_7/*Voice*/(launchTime + 12.55838f, blackboard/*out*/));
            if (launchTime + 12.56711 > Game.Music.Time) yield return Wait(launchTime + 12.56711f);
            Player.StartCoroutine(Pattern_2/*Beat*/(launchTime + 12.56711f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.3936958 > Game.Music.Time) yield return Wait(launchTime + 0.3936958f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 0.3936958f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.3936958 > Game.Music.Time) yield return Wait(launchTime + 0.3936958f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 0.3936958f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.9845181 > Game.Music.Time) yield return Wait(launchTime + 0.9845181f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 0.9845181f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.9845181 > Game.Music.Time) yield return Wait(launchTime + 0.9845181f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 0.9845181f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.170914 > Game.Music.Time) yield return Wait(launchTime + 1.170914f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 1.170914f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.170914 > Game.Music.Time) yield return Wait(launchTime + 1.170914f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 1.170914f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.572099 > Game.Music.Time) yield return Wait(launchTime + 1.572099f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 1.572099f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.572099 > Game.Music.Time) yield return Wait(launchTime + 1.572099f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 1.572099f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.947137 > Game.Music.Time) yield return Wait(launchTime + 1.947137f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 1.947137f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.947137 > Game.Music.Time) yield return Wait(launchTime + 1.947137f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 1.947137f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.556615 > Game.Music.Time) yield return Wait(launchTime + 2.556615f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 2.556615f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.556615 > Game.Music.Time) yield return Wait(launchTime + 2.556615f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 2.556615f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.72989 > Game.Music.Time) yield return Wait(launchTime + 2.72989f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 2.72989f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.72989 > Game.Music.Time) yield return Wait(launchTime + 2.72989f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 2.72989f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.152024 > Game.Music.Time) yield return Wait(launchTime + 3.152024f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 3.152024f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.152024 > Game.Music.Time) yield return Wait(launchTime + 3.152024f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 3.152024f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.520688 > Game.Music.Time) yield return Wait(launchTime + 3.520688f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 3.520688f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.520688 > Game.Music.Time) yield return Wait(launchTime + 3.520688f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 3.520688f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.123484 > Game.Music.Time) yield return Wait(launchTime + 4.123484f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 4.123484f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.123484 > Game.Music.Time) yield return Wait(launchTime + 4.123484f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 4.123484f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.302856 > Game.Music.Time) yield return Wait(launchTime + 4.302856f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 4.302856f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.302856 > Game.Music.Time) yield return Wait(launchTime + 4.302856f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 4.302856f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.713676 > Game.Music.Time) yield return Wait(launchTime + 4.713676f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 4.713676f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.713676 > Game.Music.Time) yield return Wait(launchTime + 4.713676f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 4.713676f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.08941 > Game.Music.Time) yield return Wait(launchTime + 5.08941f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 5.08941f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.08941 > Game.Music.Time) yield return Wait(launchTime + 5.08941f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 5.08941f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.692971 > Game.Music.Time) yield return Wait(launchTime + 5.692971f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 5.692971f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.692971 > Game.Music.Time) yield return Wait(launchTime + 5.692971f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 5.692971f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.887225 > Game.Music.Time) yield return Wait(launchTime + 5.887225f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 5.887225f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.887225 > Game.Music.Time) yield return Wait(launchTime + 5.887225f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 5.887225f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.280545 > Game.Music.Time) yield return Wait(launchTime + 6.280545f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 6.280545f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.280545 > Game.Music.Time) yield return Wait(launchTime + 6.280545f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 6.280545f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.666446 > Game.Music.Time) yield return Wait(launchTime + 6.666446f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 6.666446f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.666446 > Game.Music.Time) yield return Wait(launchTime + 6.666446f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 6.666446f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.244163 > Game.Music.Time) yield return Wait(launchTime + 7.244163f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 7.244163f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.244163 > Game.Music.Time) yield return Wait(launchTime + 7.244163f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 7.244163f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.450386 > Game.Music.Time) yield return Wait(launchTime + 7.450386f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 7.450386f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.450386 > Game.Music.Time) yield return Wait(launchTime + 7.450386f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 7.450386f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.850026 > Game.Music.Time) yield return Wait(launchTime + 7.850026f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 7.850026f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.850026 > Game.Music.Time) yield return Wait(launchTime + 7.850026f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 7.850026f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 8.234543 > Game.Music.Time) yield return Wait(launchTime + 8.234543f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 8.234543f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 8.234543 > Game.Music.Time) yield return Wait(launchTime + 8.234543f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 8.234543f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 8.815857 > Game.Music.Time) yield return Wait(launchTime + 8.815857f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 8.815857f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 8.815857 > Game.Music.Time) yield return Wait(launchTime + 8.815857f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 8.815857f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.015358 > Game.Music.Time) yield return Wait(launchTime + 9.015358f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 9.015358f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.015358 > Game.Music.Time) yield return Wait(launchTime + 9.015358f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 9.015358f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.416897 > Game.Music.Time) yield return Wait(launchTime + 9.416897f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 9.416897f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.416897 > Game.Music.Time) yield return Wait(launchTime + 9.416897f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 9.416897f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.797836 > Game.Music.Time) yield return Wait(launchTime + 9.797836f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 9.797836f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.797836 > Game.Music.Time) yield return Wait(launchTime + 9.797836f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 9.797836f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.39881 > Game.Music.Time) yield return Wait(launchTime + 10.39881f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 10.39881f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.39881 > Game.Music.Time) yield return Wait(launchTime + 10.39881f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 10.39881f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.58815 > Game.Music.Time) yield return Wait(launchTime + 10.58815f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 10.58815f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.58815 > Game.Music.Time) yield return Wait(launchTime + 10.58815f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 10.58815f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.98524 > Game.Music.Time) yield return Wait(launchTime + 10.98524f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 10.98524f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.98524 > Game.Music.Time) yield return Wait(launchTime + 10.98524f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 10.98524f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.35166 > Game.Music.Time) yield return Wait(launchTime + 11.35166f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 11.35166f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.35166 > Game.Music.Time) yield return Wait(launchTime + 11.35166f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 11.35166f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.96274 > Game.Music.Time) yield return Wait(launchTime + 11.96274f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 11.96274f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.96274 > Game.Music.Time) yield return Wait(launchTime + 11.96274f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 11.96274f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 12.15329 > Game.Music.Time) yield return Wait(launchTime + 12.15329f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 12.15329f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 12.15329 > Game.Music.Time) yield return Wait(launchTime + 12.15329f);
            Player.StartCoroutine(Pattern_3/*Shot*/(launchTime + 12.15329f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-169f*(float)flipx,104f), (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Arc, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -120f+60f/((float)c-1f)*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x63/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.3968153 > Game.Music.Time) yield return Wait(launchTime + 0.3968153f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0.3968153f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.3968153 > Game.Music.Time) yield return Wait(launchTime + 0.3968153f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0.3968153f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.575877 > Game.Music.Time) yield return Wait(launchTime + 1.575877f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.575877f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.575877 > Game.Music.Time) yield return Wait(launchTime + 1.575877f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.575877f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.967681 > Game.Music.Time) yield return Wait(launchTime + 1.967681f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.967681f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.967681 > Game.Music.Time) yield return Wait(launchTime + 1.967681f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.967681f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.356902 > Game.Music.Time) yield return Wait(launchTime + 2.356902f);
            Player.StartCoroutine(Pattern_6/*CenterShot*/(launchTime + 2.356902f, blackboard/*out*/));
            if (launchTime + 2.942823 > Game.Music.Time) yield return Wait(launchTime + 2.942823f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.942823f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.942823 > Game.Music.Time) yield return Wait(launchTime + 2.942823f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.942823f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.145974 > Game.Music.Time) yield return Wait(launchTime + 3.145974f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.145974f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.145974 > Game.Music.Time) yield return Wait(launchTime + 3.145974f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.145974f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.540679 > Game.Music.Time) yield return Wait(launchTime + 3.540679f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.540679f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.540679 > Game.Music.Time) yield return Wait(launchTime + 3.540679f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.540679f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.704457 > Game.Music.Time) yield return Wait(launchTime + 4.704457f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 4.704457f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.704457 > Game.Music.Time) yield return Wait(launchTime + 4.704457f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 4.704457f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.107865 > Game.Music.Time) yield return Wait(launchTime + 5.107865f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 5.107865f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.107865 > Game.Music.Time) yield return Wait(launchTime + 5.107865f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 5.107865f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.483305 > Game.Music.Time) yield return Wait(launchTime + 5.483305f);
            Player.StartCoroutine(Pattern_6/*CenterShot*/(launchTime + 5.483305f, blackboard/*out*/));
            if (launchTime + 6.070339 > Game.Music.Time) yield return Wait(launchTime + 6.070339f);
            Player.StartCoroutine(Pattern_76/*Side*/(launchTime + 6.070339f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 6.070339 > Game.Music.Time) yield return Wait(launchTime + 6.070339f);
            Player.StartCoroutine(Pattern_76/*Side*/(launchTime + 6.070339f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 6.088819 > Game.Music.Time) yield return Wait(launchTime + 6.088819f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.088819f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.088819 > Game.Music.Time) yield return Wait(launchTime + 6.088819f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.088819f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.277461 > Game.Music.Time) yield return Wait(launchTime + 6.277461f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.277461f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.277461 > Game.Music.Time) yield return Wait(launchTime + 6.277461f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.277461f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.672161 > Game.Music.Time) yield return Wait(launchTime + 6.672161f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.672161f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.672161 > Game.Music.Time) yield return Wait(launchTime + 6.672161f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.672161f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.847548 > Game.Music.Time) yield return Wait(launchTime + 7.847548f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 7.847548f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.847548 > Game.Music.Time) yield return Wait(launchTime + 7.847548f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 7.847548f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 8.250747 > Game.Music.Time) yield return Wait(launchTime + 8.250747f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 8.250747f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 8.250747 > Game.Music.Time) yield return Wait(launchTime + 8.250747f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 8.250747f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.405616 > Game.Music.Time) yield return Wait(launchTime + 9.405616f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 9.405616f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.405616 > Game.Music.Time) yield return Wait(launchTime + 9.405616f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 9.405616f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.812732 > Game.Music.Time) yield return Wait(launchTime + 9.812732f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 9.812732f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.812732 > Game.Music.Time) yield return Wait(launchTime + 9.812732f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 9.812732f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.39017 > Game.Music.Time) yield return Wait(launchTime + 10.39017f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 10.39017f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.39017 > Game.Music.Time) yield return Wait(launchTime + 10.39017f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 10.39017f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.00084 > Game.Music.Time) yield return Wait(launchTime + 11.00084f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 11.00084f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.00084 > Game.Music.Time) yield return Wait(launchTime + 11.00084f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 11.00084f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x0c/255f,0x2d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2222672f));
            if (launchTime + 0.001804352 > Game.Music.Time) yield return Wait(launchTime + 0.001804352f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.001804352f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-82f*(float)flipx,70f), (c,i,t,l, castc, casti) => 0.07f, (c,i,t,l, castc, casti) => (float)toint((float)d,3f)+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-100f,100f),(float)rndf(-100f,100f)), (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x01/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)", 0.2152386f));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x38/255f,0x17/255f,0x17/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3876705f));
            if (launchTime + 0.001973152 > Game.Music.Time) yield return Wait(launchTime + 0.001973152f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.001973152f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,110f), (c,i,t,l, castc, casti) => 0.05f, (c,i,t,l, castc, casti) => (float)toint((float)d,3f)+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-100f,100f),(float)rndf(-100f,100f)), (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x3b/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 0.3880501f));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 0.002664566 > Game.Music.Time) yield return Wait(launchTime + 0.002664566f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 0.002664566f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 0.391571 > Game.Music.Time) yield return Wait(launchTime + 0.391571f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.391571f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1677647f));
            if (launchTime + 0.3941249 > Game.Music.Time) yield return Wait(launchTime + 0.3941249f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 0.3941249f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 0.5871754 > Game.Music.Time) yield return Wait(launchTime + 0.5871754f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 0.5871754f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 0.5921306 > Game.Music.Time) yield return Wait(launchTime + 0.5921306f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5921306f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 0.9872703 > Game.Music.Time) yield return Wait(launchTime + 0.9872703f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 0.9872703f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 0.9887466 > Game.Music.Time) yield return Wait(launchTime + 0.9887466f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.9887466f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 1.360962 > Game.Music.Time) yield return Wait(launchTime + 1.360962f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.360962f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 1.370746 > Game.Music.Time) yield return Wait(launchTime + 1.370746f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 1.370746f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 1.757816 > Game.Music.Time) yield return Wait(launchTime + 1.757816f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 1.757816f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 1.765011 > Game.Music.Time) yield return Wait(launchTime + 1.765011f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.765011f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 2.155659 > Game.Music.Time) yield return Wait(launchTime + 2.155659f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 2.155659f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 2.156391 > Game.Music.Time) yield return Wait(launchTime + 2.156391f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.156391f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 2.341833 > Game.Music.Time) yield return Wait(launchTime + 2.341833f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 2.341833f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 2.342304 > Game.Music.Time) yield return Wait(launchTime + 2.342304f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.342304f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 2.543012 > Game.Music.Time) yield return Wait(launchTime + 2.543012f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 2.543012f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 2.547783 > Game.Music.Time) yield return Wait(launchTime + 2.547783f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.547783f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 2.740219 > Game.Music.Time) yield return Wait(launchTime + 2.740219f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.740219f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 2.741196 > Game.Music.Time) yield return Wait(launchTime + 2.741196f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 2.741196f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 3.137342 > Game.Music.Time) yield return Wait(launchTime + 3.137342f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 3.137342f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 3.137363 > Game.Music.Time) yield return Wait(launchTime + 3.137363f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.137363f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 3.529529 > Game.Music.Time) yield return Wait(launchTime + 3.529529f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.529529f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 3.531286 > Game.Music.Time) yield return Wait(launchTime + 3.531286f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 3.531286f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 3.71829 > Game.Music.Time) yield return Wait(launchTime + 3.71829f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 3.71829f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 3.719955 > Game.Music.Time) yield return Wait(launchTime + 3.719955f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.719955f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 4.11009 > Game.Music.Time) yield return Wait(launchTime + 4.11009f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.11009f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 4.11103 > Game.Music.Time) yield return Wait(launchTime + 4.11103f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 4.11103f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 4.896129 > Game.Music.Time) yield return Wait(launchTime + 4.896129f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.896129f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 4.90016 > Game.Music.Time) yield return Wait(launchTime + 4.90016f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 4.90016f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 5.383598 > Game.Music.Time) yield return Wait(launchTime + 5.383598f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 5.383598f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 5.385368 > Game.Music.Time) yield return Wait(launchTime + 5.385368f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.385368f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 5.476694 > Game.Music.Time) yield return Wait(launchTime + 5.476694f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.476694f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 5.485691 > Game.Music.Time) yield return Wait(launchTime + 5.485691f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 5.485691f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 5.68087 > Game.Music.Time) yield return Wait(launchTime + 5.68087f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 5.68087f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 5.682178 > Game.Music.Time) yield return Wait(launchTime + 5.682178f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.682178f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 5.870037 > Game.Music.Time) yield return Wait(launchTime + 5.870037f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 5.870037f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 5.871347 > Game.Music.Time) yield return Wait(launchTime + 5.871347f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.871347f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 6.268051 > Game.Music.Time) yield return Wait(launchTime + 6.268051f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 6.268051f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 6.269962 > Game.Music.Time) yield return Wait(launchTime + 6.269962f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.269962f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 6.664844 > Game.Music.Time) yield return Wait(launchTime + 6.664844f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 6.664844f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 6.667185 > Game.Music.Time) yield return Wait(launchTime + 6.667185f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.667185f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482353f));
            if (launchTime + 6.852541 > Game.Music.Time) yield return Wait(launchTime + 6.852541f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.852541f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 6.858902 > Game.Music.Time) yield return Wait(launchTime + 6.858902f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 6.858902f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 7.251926 > Game.Music.Time) yield return Wait(launchTime + 7.251926f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 7.251926f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 7.254263 > Game.Music.Time) yield return Wait(launchTime + 7.254263f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.254263f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 7.645235 > Game.Music.Time) yield return Wait(launchTime + 7.645235f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.645235f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 7.646046 > Game.Music.Time) yield return Wait(launchTime + 7.646046f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 7.646046f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 8.027355 > Game.Music.Time) yield return Wait(launchTime + 8.027355f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 8.027355f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 8.030523 > Game.Music.Time) yield return Wait(launchTime + 8.030523f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.030523f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 8.428371 > Game.Music.Time) yield return Wait(launchTime + 8.428371f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 8.428371f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 8.4317 > Game.Music.Time) yield return Wait(launchTime + 8.4317f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.4317f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 8.614351 > Game.Music.Time) yield return Wait(launchTime + 8.614351f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.614351f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 8.61454 > Game.Music.Time) yield return Wait(launchTime + 8.61454f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 8.61454f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 8.826353 > Game.Music.Time) yield return Wait(launchTime + 8.826353f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.826353f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 8.827736 > Game.Music.Time) yield return Wait(launchTime + 8.827736f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 8.827736f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 9.010906 > Game.Music.Time) yield return Wait(launchTime + 9.010906f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 9.010906f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 9.015516 > Game.Music.Time) yield return Wait(launchTime + 9.015516f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.015516f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 9.416887 > Game.Music.Time) yield return Wait(launchTime + 9.416887f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.416887f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 9.420567 > Game.Music.Time) yield return Wait(launchTime + 9.420567f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 9.420567f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 9.801561 > Game.Music.Time) yield return Wait(launchTime + 9.801561f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.801561f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 9.803291 > Game.Music.Time) yield return Wait(launchTime + 9.803291f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 9.803291f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 9.999468 > Game.Music.Time) yield return Wait(launchTime + 9.999468f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.999468f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2170372f));
            if (launchTime + 9.999531 > Game.Music.Time) yield return Wait(launchTime + 9.999531f);
            Player.StartCoroutine(Pattern_8/*Ou*/(launchTime + 9.999531f, blackboard, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 10.38865 > Game.Music.Time) yield return Wait(launchTime + 10.38865f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.38865f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x35/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 10.39036 > Game.Music.Time) yield return Wait(launchTime + 10.39036f);
            Player.StartCoroutine(Pattern_8/*Get*/(launchTime + 10.39036f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 11.18489 > Game.Music.Time) yield return Wait(launchTime + 11.18489f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 11.18489f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 11.19101 > Game.Music.Time) yield return Wait(launchTime + 11.19101f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.19101f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 11.57223 > Game.Music.Time) yield return Wait(launchTime + 11.57223f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 11.57223f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 11.57261 > Game.Music.Time) yield return Wait(launchTime + 11.57261f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.57261f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 11.77642 > Game.Music.Time) yield return Wait(launchTime + 11.77642f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 11.77642f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 11.77809 > Game.Music.Time) yield return Wait(launchTime + 11.77809f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.77809f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 11.95358 > Game.Music.Time) yield return Wait(launchTime + 11.95358f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 11.95358f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 11.96074 > Game.Music.Time) yield return Wait(launchTime + 11.96074f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.96074f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 12.14666 > Game.Music.Time) yield return Wait(launchTime + 12.14666f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.14666f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x19/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1482391f));
            if (launchTime + 12.14877 > Game.Music.Time) yield return Wait(launchTime + 12.14877f);
            Player.StartCoroutine(Pattern_8/*Blah*/(launchTime + 12.14877f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard, InputFunc color_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-200f,200f),120f), (c,i,t,l, castc, casti) => (float)d*10f, (c,i,t,l, castc, casti) => 180f/((float)c-1f)*(float)i-90f  , (c,i,t,l, castc, casti) => (float)d*5f+10f, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.569599 > Game.Music.Time) yield return Wait(launchTime + 1.569599f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 1.569599f, blackboard/*out*/));
            if (launchTime + 3.141236 > Game.Music.Time) yield return Wait(launchTime + 3.141236f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 3.141236f, blackboard/*out*/));
            if (launchTime + 4.701771 > Game.Music.Time) yield return Wait(launchTime + 4.701771f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 4.701771f, blackboard/*out*/));
            if (launchTime + 6.273411 > Game.Music.Time) yield return Wait(launchTime + 6.273411f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 6.273411f, blackboard/*out*/));
            if (launchTime + 7.84029 > Game.Music.Time) yield return Wait(launchTime + 7.84029f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 7.84029f, blackboard/*out*/));
            if (launchTime + 9.410347 > Game.Music.Time) yield return Wait(launchTime + 9.410347f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 9.410347f, blackboard/*out*/));
            if (launchTime + 10.97563 > Game.Music.Time) yield return Wait(launchTime + 10.97563f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 10.97563f, blackboard/*out*/));
            if (launchTime + 12.53934 > Game.Music.Time) yield return Wait(launchTime + 12.53934f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 12.53934f, blackboard/*out*/));
            if (launchTime + 14.12282 > Game.Music.Time) yield return Wait(launchTime + 14.12282f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 14.12282f, blackboard/*out*/));
            if (launchTime + 15.68676 > Game.Music.Time) yield return Wait(launchTime + 15.68676f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 15.68676f, blackboard/*out*/));
            if (launchTime + 17.2426 > Game.Music.Time) yield return Wait(launchTime + 17.2426f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 17.2426f, blackboard/*out*/));
            if (launchTime + 18.83082 > Game.Music.Time) yield return Wait(launchTime + 18.83082f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 18.83082f, blackboard/*out*/));
            if (launchTime + 20.42719 > Game.Music.Time) yield return Wait(launchTime + 20.42719f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 20.42719f, blackboard/*out*/));
            if (launchTime + 21.9668 > Game.Music.Time) yield return Wait(launchTime + 21.9668f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 21.9668f, blackboard/*out*/));
            if (launchTime + 23.53073 > Game.Music.Time) yield return Wait(launchTime + 23.53073f);
            Player.StartCoroutine(Pattern_10/*Bass*/(launchTime + 23.53073f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,102f), (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => -400f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0x5a/255f,0xc3/255f,0x26/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.2961045 > Game.Music.Time) yield return Wait(launchTime + 0.2961045f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.2961045f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 180f*((float)i%2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+100f*(float)toint((float)i,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.5918694 > Game.Music.Time) yield return Wait(launchTime + 0.5918694f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.5918694f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 180f*((float)i%2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+100f*(float)toint((float)i,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 1.184807 > Game.Music.Time) yield return Wait(launchTime + 1.184807f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.184807f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(50f,100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x10/255f,0x82/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 1.19664 > Game.Music.Time) yield return Wait(launchTime + 1.19664f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.19664f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4062119 > Game.Music.Time) yield return Wait(launchTime + 0.4062119f);
            Player.StartCoroutine(Pattern_12/*Melody*/(launchTime + 0.4062119f, blackboard/*out*/));
            if (launchTime + 12.9548 > Game.Music.Time) yield return Wait(launchTime + 12.9548f);
            Player.StartCoroutine(Pattern_12/*Melody*/(launchTime + 12.9548f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object shortvar = blackboard.GetValue((c, i, t, l, castc, casti) => -500f);
            blackboard.SetValue("node_9.output_value", shortvar);

            object mediumvar = blackboard.GetValue((c, i, t, l, castc, casti) => -400f);
            blackboard.SetValue("node_18.output_value", mediumvar);

            object largevar = blackboard.GetValue((c, i, t, l, castc, casti) => -300f);
            blackboard.SetValue("node_33.output_value", largevar);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 0.0001107156 > Game.Music.Time) yield return Wait(launchTime + 0.0001107156f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.0001107156f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2152901f));
            if (launchTime + 0.3790017 > Game.Music.Time) yield return Wait(launchTime + 0.3790017f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0.3790017f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 0.382782 > Game.Music.Time) yield return Wait(launchTime + 0.382782f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.382782f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1479073f));
            if (launchTime + 0.5818596 > Game.Music.Time) yield return Wait(launchTime + 0.5818596f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0.5818596f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 0.5825463 > Game.Music.Time) yield return Wait(launchTime + 0.5825463f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5825463f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1671486f));
            if (launchTime + 0.9760893 > Game.Music.Time) yield return Wait(launchTime + 0.9760893f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 0.9760893f, blackboard, (c,i,t,l, castc, casti) => -200f/*out*/));
            if (launchTime + 0.9772493 > Game.Music.Time) yield return Wait(launchTime + 0.9772493f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.9772493f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8362197f));
            if (launchTime + 3.12714 > Game.Music.Time) yield return Wait(launchTime + 3.12714f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 3.12714f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 3.128864 > Game.Music.Time) yield return Wait(launchTime + 3.128864f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.128864f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2947083f));
            if (launchTime + 3.509884 > Game.Music.Time) yield return Wait(launchTime + 3.509884f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 3.509884f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 3.511539 > Game.Music.Time) yield return Wait(launchTime + 3.511539f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.511539f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2080419f));
            if (launchTime + 3.72422 > Game.Music.Time) yield return Wait(launchTime + 3.72422f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 3.72422f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 3.73777 > Game.Music.Time) yield return Wait(launchTime + 3.73777f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.73777f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.171978f));
            if (launchTime + 4.114632 > Game.Music.Time) yield return Wait(launchTime + 4.114632f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 4.114632f, blackboard, (c,i,t,l, castc, casti) => "node_33.output_value"/*out*/));
            if (launchTime + 4.120437 > Game.Music.Time) yield return Wait(launchTime + 4.120437f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.120437f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.5498047f));
            if (launchTime + 4.700454 > Game.Music.Time) yield return Wait(launchTime + 4.700454f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.700454f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3885651f));
            if (launchTime + 4.707233 > Game.Music.Time) yield return Wait(launchTime + 4.707233f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 4.707233f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 5.132744 > Game.Music.Time) yield return Wait(launchTime + 5.132744f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 5.132744f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 5.133674 > Game.Music.Time) yield return Wait(launchTime + 5.133674f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.133674f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.356823f));
            if (launchTime + 5.500156 > Game.Music.Time) yield return Wait(launchTime + 5.500156f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.500156f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3641815f));
            if (launchTime + 5.50816 > Game.Music.Time) yield return Wait(launchTime + 5.50816f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 5.50816f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 5.885238 > Game.Music.Time) yield return Wait(launchTime + 5.885238f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.885238f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1738358f));
            if (launchTime + 5.886757 > Game.Music.Time) yield return Wait(launchTime + 5.886757f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 5.886757f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 6.09309 > Game.Music.Time) yield return Wait(launchTime + 6.09309f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.09309f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1541328f));
            if (launchTime + 6.097179 > Game.Music.Time) yield return Wait(launchTime + 6.097179f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 6.097179f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 6.462856 > Game.Music.Time) yield return Wait(launchTime + 6.462856f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.462856f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2132149f));
            if (launchTime + 6.46471 > Game.Music.Time) yield return Wait(launchTime + 6.46471f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 6.46471f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 6.832618 > Game.Music.Time) yield return Wait(launchTime + 6.832618f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.832618f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3751183f));
            if (launchTime + 6.843628 > Game.Music.Time) yield return Wait(launchTime + 6.843628f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 6.843628f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 7.237385 > Game.Music.Time) yield return Wait(launchTime + 7.237385f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.237385f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.5720553f));
            if (launchTime + 7.241692 > Game.Music.Time) yield return Wait(launchTime + 7.241692f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 7.241692f, blackboard, (c,i,t,l, castc, casti) => "node_33.output_value"/*out*/));
            if (launchTime + 7.823463 > Game.Music.Time) yield return Wait(launchTime + 7.823463f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 7.823463f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 7.823753 > Game.Music.Time) yield return Wait(launchTime + 7.823753f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.823753f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3970184f));
            if (launchTime + 8.226345 > Game.Music.Time) yield return Wait(launchTime + 8.226345f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.226345f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3926163f));
            if (launchTime + 8.229179 > Game.Music.Time) yield return Wait(launchTime + 8.229179f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 8.229179f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 8.623432 > Game.Music.Time) yield return Wait(launchTime + 8.623432f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 8.623432f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_value"/*out*/));
            if (launchTime + 8.628914 > Game.Music.Time) yield return Wait(launchTime + 8.628914f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.628914f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3773079f));
            if (launchTime + 9.017654 > Game.Music.Time) yield return Wait(launchTime + 9.017654f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 9.017654f, blackboard, (c,i,t,l, castc, casti) => -200f/*out*/));
            if (launchTime + 9.018372 > Game.Music.Time) yield return Wait(launchTime + 9.018372f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.018372f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 1.167164f));
            if (launchTime + 10.20862 > Game.Music.Time) yield return Wait(launchTime + 10.20862f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.20862f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3247948f));
            if (launchTime + 10.21184 > Game.Music.Time) yield return Wait(launchTime + 10.21184f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 10.21184f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 10.62198 > Game.Music.Time) yield return Wait(launchTime + 10.62198f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.62198f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1408005f));
            if (launchTime + 10.62519 > Game.Music.Time) yield return Wait(launchTime + 10.62519f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 10.62519f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 10.80134 > Game.Music.Time) yield return Wait(launchTime + 10.80134f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.80134f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1501465f));
            if (launchTime + 10.80509 > Game.Music.Time) yield return Wait(launchTime + 10.80509f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 10.80509f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 11.14377 > Game.Music.Time) yield return Wait(launchTime + 11.14377f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.14377f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2340012f));
            if (launchTime + 11.14574 > Game.Music.Time) yield return Wait(launchTime + 11.14574f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 11.14574f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            if (launchTime + 11.54212 > Game.Music.Time) yield return Wait(launchTime + 11.54212f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.54212f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.5903854f));
            if (launchTime + 11.55529 > Game.Music.Time) yield return Wait(launchTime + 11.55529f);
            Player.StartCoroutine(Pattern_13/*Shot*/(launchTime + 11.55529f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard, InputFunc acceleration_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, acceleration_input, "node_0.output_patterninput");
            object acceleration = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => acceleration = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,120f), (c,i,t,l, castc, casti) => (float)d*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object bullets;
            blackboard.SubscribeForChanges("node_3.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_lastbullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*CreateBullets*/(launchTime + 0f, blackboard/*out*/, "node_1.output_lastbullet"));
            if (launchTime + 1.558891 > Game.Music.Time) yield return Wait(launchTime + 1.558891f);
            Player.StartCoroutine(Pattern_17/*Guitar*/(launchTime + 1.558891f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard, string lastbullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, lastbullet_output, "node_19.output_value");
            object lastbullet1;
            blackboard.SubscribeForChanges("node_8.output_value", (b, o) => lastbullet1 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet", (c,i,t,l, castc, casti) => "node_2.output_bullet", (c,i,t,l, castc, casti) => "node_3.output_bullet", (c,i,t,l, castc, casti) => "node_4.output_bullet", (c,i,t,l, castc, casti) => "node_5.output_bullet", (c,i,t,l, castc, casti) => "node_6.output_bullet", (c,i,t,l, castc, casti) => "node_7.output_bullet", (c,i,t,l, castc, casti) => "node_9.output_bullet"/*out*/, "node_8.output_value");
            object lastbullet2;
            blackboard.SubscribeForChanges("node_14.output_value", (b, o) => lastbullet2 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet", (c,i,t,l, castc, casti) => "node_11.output_bullet", (c,i,t,l, castc, casti) => "node_12.output_bullet", (c,i,t,l, castc, casti) => "node_13.output_bullet", (c,i,t,l, castc, casti) => "node_15.output_bullet", (c,i,t,l, castc, casti) => "node_16.output_bullet", (c,i,t,l, castc, casti) => "node_17.output_bullet", (c,i,t,l, castc, casti) => "node_18.output_bullet"/*out*/, "node_14.output_value");
            object lastbulletall;
            blackboard.SubscribeForChanges("node_19.output_value", (b, o) => lastbulletall = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_8.output_value", (c,i,t,l, castc, casti) => "node_14.output_value", (c,i,t,l, castc, casti) => "node_24.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_32.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_19.output_value");
            object lastbullet3;
            blackboard.SubscribeForChanges("node_24.output_value", (b, o) => lastbullet3 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_20.output_bullet", (c,i,t,l, castc, casti) => "node_21.output_bullet", (c,i,t,l, castc, casti) => "node_22.output_bullet", (c,i,t,l, castc, casti) => "node_23.output_bullet", (c,i,t,l, castc, casti) => "node_25.output_bullet", (c,i,t,l, castc, casti) => "node_26.output_bullet", (c,i,t,l, castc, casti) => "node_27.output_bullet", (c,i,t,l, castc, casti) => "node_28.output_bullet"/*out*/, "node_24.output_value");
            object lastbullet4;
            blackboard.SubscribeForChanges("node_32.output_value", (b, o) => lastbullet4 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_29.output_bullet", (c,i,t,l, castc, casti) => "node_31.output_bullet", (c,i,t,l, castc, casti) => "node_33.output_bullet", (c,i,t,l, castc, casti) => "node_34.output_bullet", (c,i,t,l, castc, casti) => "node_35.output_bullet", (c,i,t,l, castc, casti) => "node_36.output_bullet", (c,i,t,l, castc, casti) => "node_37.output_bullet", (c,i,t,l, castc, casti) => null/*out*/, "node_32.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bullet"));
            if (launchTime + 0.3835984 > Game.Music.Time) yield return Wait(launchTime + 0.3835984f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 0.3835984f, blackboard/*out*/, "node_2.output_bullet"));
            if (launchTime + 0.7687759 > Game.Music.Time) yield return Wait(launchTime + 0.7687759f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 0.7687759f, blackboard/*out*/, "node_3.output_bullet"));
            if (launchTime + 0.9711457 > Game.Music.Time) yield return Wait(launchTime + 0.9711457f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 0.9711457f, blackboard/*out*/, "node_4.output_bullet"));
            if (launchTime + 1.160903 > Game.Music.Time) yield return Wait(launchTime + 1.160903f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 1.160903f, blackboard/*out*/, "node_5.output_bullet"));
            if (launchTime + 1.35485 > Game.Music.Time) yield return Wait(launchTime + 1.35485f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 1.35485f, blackboard/*out*/, "node_6.output_bullet"));
            if (launchTime + 1.569892 > Game.Music.Time) yield return Wait(launchTime + 1.569892f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 1.569892f, blackboard/*out*/, "node_7.output_bullet"));
            if (launchTime + 2.341491 > Game.Music.Time) yield return Wait(launchTime + 2.341491f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 2.341491f, blackboard/*out*/, "node_9.output_bullet"));
            if (launchTime + 2.539665 > Game.Music.Time) yield return Wait(launchTime + 2.539665f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 2.539665f, blackboard/*out*/, "node_10.output_bullet"));
            if (launchTime + 2.733611 > Game.Music.Time) yield return Wait(launchTime + 2.733611f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 2.733611f, blackboard/*out*/, "node_11.output_bullet"));
            if (launchTime + 3.129966 > Game.Music.Time) yield return Wait(launchTime + 3.129966f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 3.129966f, blackboard/*out*/, "node_12.output_bullet"));
            if (launchTime + 3.526305 > Game.Music.Time) yield return Wait(launchTime + 3.526305f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 3.526305f, blackboard/*out*/, "node_13.output_bullet"));
            if (launchTime + 3.909988 > Game.Music.Time) yield return Wait(launchTime + 3.909988f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 3.909988f, blackboard/*out*/, "node_15.output_bullet"));
            if (launchTime + 4.112389 > Game.Music.Time) yield return Wait(launchTime + 4.112389f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 4.112389f, blackboard/*out*/, "node_16.output_bullet"));
            if (launchTime + 4.310562 > Game.Music.Time) yield return Wait(launchTime + 4.310562f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 4.310562f, blackboard/*out*/, "node_17.output_bullet"));
            if (launchTime + 4.496079 > Game.Music.Time) yield return Wait(launchTime + 4.496079f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 4.496079f, blackboard/*out*/, "node_18.output_bullet"));
            if (launchTime + 5.870636 > Game.Music.Time) yield return Wait(launchTime + 5.870636f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 5.870636f, blackboard/*out*/, "node_20.output_bullet"));
            if (launchTime + 6.06881 > Game.Music.Time) yield return Wait(launchTime + 6.06881f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 6.06881f, blackboard/*out*/, "node_21.output_bullet"));
            if (launchTime + 6.254325 > Game.Music.Time) yield return Wait(launchTime + 6.254325f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 6.254325f, blackboard/*out*/, "node_22.output_bullet"));
            if (launchTime + 6.654884 > Game.Music.Time) yield return Wait(launchTime + 6.654884f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 6.654884f, blackboard/*out*/, "node_23.output_bullet"));
            if (launchTime + 7.051223 > Game.Music.Time) yield return Wait(launchTime + 7.051223f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 7.051223f, blackboard/*out*/, "node_25.output_bullet"));
            if (launchTime + 7.240966 > Game.Music.Time) yield return Wait(launchTime + 7.240966f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 7.240966f, blackboard/*out*/, "node_26.output_bullet"));
            if (launchTime + 7.439109 > Game.Music.Time) yield return Wait(launchTime + 7.439109f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 7.439109f, blackboard/*out*/, "node_27.output_bullet"));
            if (launchTime + 7.633102 > Game.Music.Time) yield return Wait(launchTime + 7.633102f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 7.633102f, blackboard/*out*/, "node_28.output_bullet"));
            if (launchTime + 7.848114 > Game.Music.Time) yield return Wait(launchTime + 7.848114f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 7.848114f, blackboard/*out*/, "node_29.output_bullet"));
            if (launchTime + 9.412422 > Game.Music.Time) yield return Wait(launchTime + 9.412422f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 9.412422f, blackboard/*out*/, "node_31.output_bullet"));
            if (launchTime + 9.791901 > Game.Music.Time) yield return Wait(launchTime + 9.791901f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 9.791901f, blackboard/*out*/, "node_33.output_bullet"));
            if (launchTime + 10.18826 > Game.Music.Time) yield return Wait(launchTime + 10.18826f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 10.18826f, blackboard/*out*/, "node_34.output_bullet"));
            if (launchTime + 10.37796 > Game.Music.Time) yield return Wait(launchTime + 10.37796f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 10.37796f, blackboard/*out*/, "node_35.output_bullet"));
            if (launchTime + 10.57195 > Game.Music.Time) yield return Wait(launchTime + 10.57195f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 10.57195f, blackboard/*out*/, "node_36.output_bullet"));
            if (launchTime + 10.78277 > Game.Music.Time) yield return Wait(launchTime + 10.78277f);
            Player.StartCoroutine(Pattern_16/*Bullet*/(launchTime + 10.78277f, blackboard/*out*/, "node_37.output_bullet"));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, string bullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullet_output, "node_0.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => (float)rndf(200f,340f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.001701355 > Game.Music.Time) yield return Wait(launchTime + 0.001701355f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.001701355f, blackboard, (c,i,t,l, castc, casti) => new Color(0x36/255f,0x17/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1028976f));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_1.output_patterninput");
            object bullets = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 0.393608 > Game.Music.Time) yield return Wait(launchTime + 0.393608f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 0.393608f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 0.604714 > Game.Music.Time) yield return Wait(launchTime + 0.604714f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 0.604714f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 0.9906999 > Game.Music.Time) yield return Wait(launchTime + 0.9906999f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 0.9906999f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 1.563713 > Game.Music.Time) yield return Wait(launchTime + 1.563713f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 1.563713f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 1.955757 > Game.Music.Time) yield return Wait(launchTime + 1.955757f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 1.955757f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 3.156006 > Game.Music.Time) yield return Wait(launchTime + 3.156006f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.156006f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 3.523926 > Game.Music.Time) yield return Wait(launchTime + 3.523926f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.523926f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 3.91597 > Game.Music.Time) yield return Wait(launchTime + 3.91597f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 3.91597f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 4.131416 > Game.Music.Time) yield return Wait(launchTime + 4.131416f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.131416f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 4.516121 > Game.Music.Time) yield return Wait(launchTime + 4.516121f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.516121f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 4.70678 > Game.Music.Time) yield return Wait(launchTime + 4.70678f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 4.70678f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 6.262444 > Game.Music.Time) yield return Wait(launchTime + 6.262444f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 6.262444f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 6.650559 > Game.Music.Time) yield return Wait(launchTime + 6.650559f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 6.650559f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 6.877648 > Game.Music.Time) yield return Wait(launchTime + 6.877648f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 6.877648f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 7.261635 > Game.Music.Time) yield return Wait(launchTime + 7.261635f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 7.261635f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 7.463959 > Game.Music.Time) yield return Wait(launchTime + 7.463959f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 7.463959f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 7.827294 > Game.Music.Time) yield return Wait(launchTime + 7.827294f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 7.827294f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 8.227798 > Game.Music.Time) yield return Wait(launchTime + 8.227798f);
            Player.StartCoroutine(Pattern_18/*Shoot*/(launchTime + 8.227798f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d/*out*/, "node_2.output_elements"));
            if (launchTime + 0.00151825 > Game.Music.Time) yield return Wait(launchTime + 0.00151825f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.00151825f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => 30f, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.01551056 > Game.Music.Time) yield return Wait(launchTime + 0.01551056f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.01551056f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x33/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.14711f));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,153f), (c,i,t,l, castc, casti) => 0.08f, (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)rndf(-10f,10f), (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+180f+(float)rndf(-10f,10f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x79/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)", 1.136131f));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.57238 > Game.Music.Time) yield return Wait(launchTime + 1.57238f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 1.57238f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.57238 > Game.Music.Time) yield return Wait(launchTime + 1.57238f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 1.57238f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.136131 > Game.Music.Time) yield return Wait(launchTime + 3.136131f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 3.136131f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.136131 > Game.Music.Time) yield return Wait(launchTime + 3.136131f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 3.136131f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.704704 > Game.Music.Time) yield return Wait(launchTime + 4.704704f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 4.704704f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.704704 > Game.Music.Time) yield return Wait(launchTime + 4.704704f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 4.704704f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.292709 > Game.Music.Time) yield return Wait(launchTime + 6.292709f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 6.292709f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.292709 > Game.Music.Time) yield return Wait(launchTime + 6.292709f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 6.292709f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.84343 > Game.Music.Time) yield return Wait(launchTime + 7.84343f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 7.84343f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.84343 > Game.Music.Time) yield return Wait(launchTime + 7.84343f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 7.84343f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.404762 > Game.Music.Time) yield return Wait(launchTime + 9.404762f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 9.404762f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.404762 > Game.Music.Time) yield return Wait(launchTime + 9.404762f);
            Player.StartCoroutine(Pattern_21/*Shot*/(launchTime + 9.404762f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);

            object startbullets;
            blackboard.SubscribeForChanges("node_10.output_value", (b, o) => startbullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => "node_6.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_value");
            object allbullets;
            blackboard.SubscribeForChanges("node_13.output_value", (b, o) => allbullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_11.output_bullets", (c,i,t,l, castc, casti) => "node_8.output_bullets", (c,i,t,l, castc, casti) => "node_9.output_bullets", (c,i,t,l, castc, casti) => "node_12.output_bullets", (c,i,t,l, castc, casti) => "node_14.output_bullets", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-72f*(float)flipx,137f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-201f), (c,i,t,l, castc, casti) => new Vector2(0f,200f), (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x8f/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.0002365117 > Game.Music.Time) yield return Wait(launchTime + 0.0002365117f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.0002365117f, blackboard, (c,i,t,l, castc, casti) => new Color(0x11/255f,0x3b/255f,0x24/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1067276f));
            if (launchTime + 0.2013321 > Game.Music.Time) yield return Wait(launchTime + 0.2013321f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.2013321f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-92f*(float)flipx,157f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-67f), (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x8f/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.2045288 > Game.Music.Time) yield return Wait(launchTime + 0.2045288f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.2045288f, blackboard, (c,i,t,l, castc, casti) => new Color(0x11/255f,0x3b/255f,0x24/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1067276f));
            if (launchTime + 0.3835983 > Game.Music.Time) yield return Wait(launchTime + 0.3835983f);
            Player.StartCoroutine(Pattern_22/*Multiply*/(launchTime + 0.3835983f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/, "node_8.output_bullets"));
            if (launchTime + 0.5890274 > Game.Music.Time) yield return Wait(launchTime + 0.5890274f);
            Player.StartCoroutine(Pattern_22/*Multiply*/(launchTime + 0.5890274f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/, "node_9.output_bullets"));
            if (launchTime + 0.7793579 > Game.Music.Time) yield return Wait(launchTime + 0.7793579f);
            Player.StartCoroutine(Pattern_22/*Multiply*/(launchTime + 0.7793579f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/, "node_11.output_bullets"));
            if (launchTime + 0.9660187 > Game.Music.Time) yield return Wait(launchTime + 0.9660187f);
            Player.StartCoroutine(Pattern_22/*Multiply*/(launchTime + 0.9660187f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/, "node_12.output_bullets"));
            if (launchTime + 1.176521 > Game.Music.Time) yield return Wait(launchTime + 1.176521f);
            Player.StartCoroutine(Pattern_22/*Multiply*/(launchTime + 1.176521f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/, "node_14.output_bullets"));
            if (launchTime + 1.56279 > Game.Music.Time) yield return Wait(launchTime + 1.56279f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.56279f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 1.564552 > Game.Music.Time) yield return Wait(launchTime + 1.564552f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 1.564552f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => -0.005f, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard, InputFunc bullets_input, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);

            SubscribeOutput(outboard, blackboard, bullets_output, "node_2.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d+(float)toint((float)d,3f)+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f)*(float)ar,(float)rndf(-30f,30f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x33/255f,0xc1/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.004104614 > Game.Music.Time) yield return Wait(launchTime + 0.004104614f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.004104614f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x13/255f,0x2f/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1067276f));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);

            object bullets;
            blackboard.SubscribeForChanges("node_5.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-72f*(float)flipx,137f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-201f), (c,i,t,l, castc, casti) => new Vector2(0f,200f), (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x8f/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.1893005 > Game.Music.Time) yield return Wait(launchTime + 0.1893005f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.1893005f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-92f*(float)flipx,157f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-67f), (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x8f/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.3931808 > Game.Music.Time) yield return Wait(launchTime + 0.3931808f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.3931808f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_value", (c,i,t,l, castc, casti) => 0.12f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x88/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)", 1.558807f));
            if (launchTime + 1.959351 > Game.Music.Time) yield return Wait(launchTime + 1.959351f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.959351f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_value", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_29/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4089814 > Game.Music.Time) yield return Wait(launchTime + 0.4089814f);
            Player.StartCoroutine(Pattern_25/*Melody*/(launchTime + 0.4089814f, blackboard/*out*/));
            if (launchTime + 4.323044 > Game.Music.Time) yield return Wait(launchTime + 4.323044f);
            Player.StartCoroutine(Pattern_27/*8bit*/(launchTime + 4.323044f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.194748 > Game.Music.Time) yield return Wait(launchTime + 0.194748f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0.194748f, blackboard/*out*/));
            if (launchTime + 0.2883149 > Game.Music.Time) yield return Wait(launchTime + 0.2883149f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0.2883149f, blackboard/*out*/));
            if (launchTime + 0.3921357 > Game.Music.Time) yield return Wait(launchTime + 0.3921357f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0.3921357f, blackboard/*out*/));
            if (launchTime + 0.6758271 > Game.Music.Time) yield return Wait(launchTime + 0.6758271f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0.6758271f, blackboard/*out*/));
            if (launchTime + 0.8764802 > Game.Music.Time) yield return Wait(launchTime + 0.8764802f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0.8764802f, blackboard/*out*/));
            if (launchTime + 0.9803002 > Game.Music.Time) yield return Wait(launchTime + 0.9803002f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 0.9803002f, blackboard/*out*/));
            if (launchTime + 1.074874 > Game.Music.Time) yield return Wait(launchTime + 1.074874f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.074874f, blackboard/*out*/));
            if (launchTime + 1.174041 > Game.Music.Time) yield return Wait(launchTime + 1.174041f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.174041f, blackboard/*out*/));
            if (launchTime + 1.263992 > Game.Music.Time) yield return Wait(launchTime + 1.263992f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.263992f, blackboard/*out*/));
            if (launchTime + 1.367782 > Game.Music.Time) yield return Wait(launchTime + 1.367782f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.367782f, blackboard/*out*/));
            if (launchTime + 1.460006 > Game.Music.Time) yield return Wait(launchTime + 1.460006f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.460006f, blackboard/*out*/));
            if (launchTime + 1.549987 > Game.Music.Time) yield return Wait(launchTime + 1.549987f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.549987f, blackboard/*out*/));
            if (launchTime + 1.656082 > Game.Music.Time) yield return Wait(launchTime + 1.656082f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.656082f, blackboard/*out*/));
            if (launchTime + 1.755264 > Game.Music.Time) yield return Wait(launchTime + 1.755264f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.755264f, blackboard/*out*/));
            if (launchTime + 1.84291 > Game.Music.Time) yield return Wait(launchTime + 1.84291f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.84291f, blackboard/*out*/));
            if (launchTime + 1.949035 > Game.Music.Time) yield return Wait(launchTime + 1.949035f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 1.949035f, blackboard/*out*/));
            if (launchTime + 2.329559 > Game.Music.Time) yield return Wait(launchTime + 2.329559f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 2.329559f, blackboard/*out*/));
            if (launchTime + 2.553344 > Game.Music.Time) yield return Wait(launchTime + 2.553344f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 2.553344f, blackboard/*out*/));
            if (launchTime + 2.640991 > Game.Music.Time) yield return Wait(launchTime + 2.640991f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 2.640991f, blackboard/*out*/));
            if (launchTime + 3.330657 > Game.Music.Time) yield return Wait(launchTime + 3.330657f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 3.330657f, blackboard/*out*/));
            if (launchTime + 3.429824 > Game.Music.Time) yield return Wait(launchTime + 3.429824f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 3.429824f, blackboard/*out*/));
            if (launchTime + 3.522094 > Game.Music.Time) yield return Wait(launchTime + 3.522094f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 3.522094f, blackboard/*out*/));
            if (launchTime + 3.630477 > Game.Music.Time) yield return Wait(launchTime + 3.630477f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 3.630477f, blackboard/*out*/));
            if (launchTime + 4.103317 > Game.Music.Time) yield return Wait(launchTime + 4.103317f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 4.103317f, blackboard/*out*/));
            if (launchTime + 4.209426 > Game.Music.Time) yield return Wait(launchTime + 4.209426f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 4.209426f, blackboard/*out*/));
            if (launchTime + 4.899063 > Game.Music.Time) yield return Wait(launchTime + 4.899063f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 4.899063f, blackboard/*out*/));
            if (launchTime + 5.002883 > Game.Music.Time) yield return Wait(launchTime + 5.002883f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 5.002883f, blackboard/*out*/));
            if (launchTime + 5.473403 > Game.Music.Time) yield return Wait(launchTime + 5.473403f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 5.473403f, blackboard/*out*/));
            if (launchTime + 5.687896 > Game.Music.Time) yield return Wait(launchTime + 5.687896f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 5.687896f, blackboard/*out*/));
            if (launchTime + 5.766311 > Game.Music.Time) yield return Wait(launchTime + 5.766311f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 5.766311f, blackboard/*out*/));
            if (launchTime + 6.361434 > Game.Music.Time) yield return Wait(launchTime + 6.361434f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 6.361434f, blackboard/*out*/));
            if (launchTime + 6.569 > Game.Music.Time) yield return Wait(launchTime + 6.569f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 6.569f, blackboard/*out*/));
            if (launchTime + 6.652023 > Game.Music.Time) yield return Wait(launchTime + 6.652023f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 6.652023f, blackboard/*out*/));
            if (launchTime + 7.04544 > Game.Music.Time) yield return Wait(launchTime + 7.04544f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 7.04544f, blackboard/*out*/));
            if (launchTime + 7.236267 > Game.Music.Time) yield return Wait(launchTime + 7.236267f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 7.236267f, blackboard/*out*/));
            if (launchTime + 7.343201 > Game.Music.Time) yield return Wait(launchTime + 7.343201f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 7.343201f, blackboard/*out*/));
            if (launchTime + 7.640915 > Game.Music.Time) yield return Wait(launchTime + 7.640915f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 7.640915f, blackboard/*out*/));
            if (launchTime + 8.028885 > Game.Music.Time) yield return Wait(launchTime + 8.028885f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 8.028885f, blackboard/*out*/));
            if (launchTime + 8.129532 > Game.Music.Time) yield return Wait(launchTime + 8.129532f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 8.129532f, blackboard/*out*/));
            if (launchTime + 8.230179 > Game.Music.Time) yield return Wait(launchTime + 8.230179f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 8.230179f, blackboard/*out*/));
            if (launchTime + 8.626587 > Game.Music.Time) yield return Wait(launchTime + 8.626587f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 8.626587f, blackboard/*out*/));
            if (launchTime + 8.815674 > Game.Music.Time) yield return Wait(launchTime + 8.815674f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 8.815674f, blackboard/*out*/));
            if (launchTime + 8.918823 > Game.Music.Time) yield return Wait(launchTime + 8.918823f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 8.918823f, blackboard/*out*/));
            if (launchTime + 9.687473 > Game.Music.Time) yield return Wait(launchTime + 9.687473f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 9.687473f, blackboard/*out*/));
            if (launchTime + 9.797943 > Game.Music.Time) yield return Wait(launchTime + 9.797943f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 9.797943f, blackboard/*out*/));
            if (launchTime + 9.898653 > Game.Music.Time) yield return Wait(launchTime + 9.898653f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 9.898653f, blackboard/*out*/));
            if (launchTime + 9.994483 > Game.Music.Time) yield return Wait(launchTime + 9.994483f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 9.994483f, blackboard/*out*/));
            if (launchTime + 10.17863 > Game.Music.Time) yield return Wait(launchTime + 10.17863f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 10.17863f, blackboard/*out*/));
            if (launchTime + 10.39716 > Game.Music.Time) yield return Wait(launchTime + 10.39716f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 10.39716f, blackboard/*out*/));
            if (launchTime + 10.58379 > Game.Music.Time) yield return Wait(launchTime + 10.58379f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 10.58379f, blackboard/*out*/));
            if (launchTime + 11.08237 > Game.Music.Time) yield return Wait(launchTime + 11.08237f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 11.08237f, blackboard/*out*/));
            if (launchTime + 11.36719 > Game.Music.Time) yield return Wait(launchTime + 11.36719f);
            Player.StartCoroutine(Pattern_26/*Shot*/(launchTime + 11.36719f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -5f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 5f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-200f,200f),100f), (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 120f+20f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.289001 > Game.Music.Time) yield return Wait(launchTime + 0.289001f);
            Player.StartCoroutine(Pattern_28/*shot*/(launchTime + 0.289001f, blackboard/*out*/));
            if (launchTime + 0.6030421 > Game.Music.Time) yield return Wait(launchTime + 0.6030421f);
            Player.StartCoroutine(Pattern_28/*shot*/(launchTime + 0.6030421f, blackboard/*out*/));
            if (launchTime + 5.13755 > Game.Music.Time) yield return Wait(launchTime + 5.13755f);
            Player.StartCoroutine(Pattern_28/*shot*/(launchTime + 5.13755f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,107f), (c,i,t,l, castc, casti) => 3f+(float)d, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => 320f/((float)c-1f)*(float)i-160f, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.565307 > Game.Music.Time) yield return Wait(launchTime + 1.565307f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 1.565307f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.565307 > Game.Music.Time) yield return Wait(launchTime + 1.565307f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 1.565307f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.144211 > Game.Music.Time) yield return Wait(launchTime + 3.144211f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 3.144211f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.144211 > Game.Music.Time) yield return Wait(launchTime + 3.144211f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 3.144211f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.71112 > Game.Music.Time) yield return Wait(launchTime + 4.71112f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 4.71112f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.71112 > Game.Music.Time) yield return Wait(launchTime + 4.71112f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 4.71112f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.284012 > Game.Music.Time) yield return Wait(launchTime + 6.284012f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 6.284012f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.284012 > Game.Music.Time) yield return Wait(launchTime + 6.284012f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 6.284012f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.850937 > Game.Music.Time) yield return Wait(launchTime + 7.850937f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 7.850937f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.850937 > Game.Music.Time) yield return Wait(launchTime + 7.850937f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 7.850937f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.405762 > Game.Music.Time) yield return Wait(launchTime + 9.405762f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 9.405762f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.405762 > Game.Music.Time) yield return Wait(launchTime + 9.405762f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 9.405762f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.97262 > Game.Music.Time) yield return Wait(launchTime + 10.97262f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 10.97262f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.97262 > Game.Music.Time) yield return Wait(launchTime + 10.97262f);
            Player.StartCoroutine(Pattern_30/*OnePattern*/(launchTime + 10.97262f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(160f*(float)flipx,160f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.3926697 > Game.Music.Time) yield return Wait(launchTime + 0.3926697f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.3926697f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.3957977 > Game.Music.Time) yield return Wait(launchTime + 0.3957977f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3957977f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3c/255f,0x17/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2003479f));
            if (launchTime + 0.3968964 > Game.Music.Time) yield return Wait(launchTime + 0.3968964f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.3968964f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 0.5919647 > Game.Music.Time) yield return Wait(launchTime + 0.5919647f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.5919647f, blackboard, (c,i,t,l, castc, casti) => new Vector2(160f*(float)flipx,160f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+45f*(float)flipx, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_bullet(s)"));
            if (launchTime + 0.9833832 > Game.Music.Time) yield return Wait(launchTime + 0.9833832f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.9833832f, blackboard, (c,i,t,l, castc, casti) => new Vector2(160f*(float)flipx,160f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+40f*(float)flipx, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)"));
            if (launchTime + 1.174896 > Game.Music.Time) yield return Wait(launchTime + 1.174896f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.174896f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_bullet(s)"));
            if (launchTime + 1.174911 > Game.Music.Time) yield return Wait(launchTime + 1.174911f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.174911f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_bullet(s)"));
            if (launchTime + 1.184921 > Game.Music.Time) yield return Wait(launchTime + 1.184921f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.184921f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3c/255f,0x13/255f,0x15/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2003479f));
            if (launchTime + 1.186859 > Game.Music.Time) yield return Wait(launchTime + 1.186859f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.186859f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.187607 > Game.Music.Time) yield return Wait(launchTime + 1.187607f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.187607f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_32/*Waku*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.002197206 > Game.Music.Time) yield return Wait(launchTime + 0.002197206f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.002197206f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x0e/255f,0x28/255f,0x3e/255f)/*out*/, 0.8767395f));
            if (launchTime + 0.7825469 > Game.Music.Time) yield return Wait(launchTime + 0.7825469f);
            Player.StartCoroutine(Pattern_34/*Melody*/(launchTime + 0.7825469f, blackboard/*out*/));
            if (launchTime + 0.8901367 > Game.Music.Time) yield return Wait(launchTime + 0.8901367f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.8901367f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.05474854f));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.067f, (c,i,t,l, castc, casti) => (float)d+2f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-330f,330f)*(float)ar,(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)", 0.8708954f));
            if (launchTime + 0.9747314 > Game.Music.Time) yield return Wait(launchTime + 0.9747314f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 0.9747314f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 1.194931 > Game.Music.Time) yield return Wait(launchTime + 1.194931f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 1.194931f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 1.366913 > Game.Music.Time) yield return Wait(launchTime + 1.366913f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 1.366913f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 1.484512 > Game.Music.Time) yield return Wait(launchTime + 1.484512f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 1.484512f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 1.674591 > Game.Music.Time) yield return Wait(launchTime + 1.674591f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 1.674591f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 1.756027 > Game.Music.Time) yield return Wait(launchTime + 1.756027f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 1.756027f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 1.951874 > Game.Music.Time) yield return Wait(launchTime + 1.951874f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 1.951874f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.056763 > Game.Music.Time) yield return Wait(launchTime + 2.056763f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.056763f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.152359 > Game.Music.Time) yield return Wait(launchTime + 2.152359f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.152359f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.262787 > Game.Music.Time) yield return Wait(launchTime + 2.262787f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.262787f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.347107 > Game.Music.Time) yield return Wait(launchTime + 2.347107f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.347107f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.456635 > Game.Music.Time) yield return Wait(launchTime + 2.456635f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.456635f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.538315 > Game.Music.Time) yield return Wait(launchTime + 2.538315f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.538315f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.642197 > Game.Music.Time) yield return Wait(launchTime + 2.642197f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.642197f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.746048 > Game.Music.Time) yield return Wait(launchTime + 2.746048f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.746048f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 2.940231 > Game.Music.Time) yield return Wait(launchTime + 2.940231f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 2.940231f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.045807 > Game.Music.Time) yield return Wait(launchTime + 3.045807f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.045807f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.243332 > Game.Music.Time) yield return Wait(launchTime + 3.243332f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.243332f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.331848 > Game.Music.Time) yield return Wait(launchTime + 3.331848f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.331848f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.522675 > Game.Music.Time) yield return Wait(launchTime + 3.522675f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.522675f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.726944 > Game.Music.Time) yield return Wait(launchTime + 3.726944f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.726944f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.832458 > Game.Music.Time) yield return Wait(launchTime + 3.832458f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.832458f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 3.919357 > Game.Music.Time) yield return Wait(launchTime + 3.919357f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 3.919357f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.018173 > Game.Music.Time) yield return Wait(launchTime + 4.018173f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.018173f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.220917 > Game.Music.Time) yield return Wait(launchTime + 4.220917f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.220917f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.317825 > Game.Music.Time) yield return Wait(launchTime + 4.317825f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.317825f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.506851 > Game.Music.Time) yield return Wait(launchTime + 4.506851f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.506851f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.617554 > Game.Music.Time) yield return Wait(launchTime + 4.617554f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.617554f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.808258 > Game.Music.Time) yield return Wait(launchTime + 4.808258f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.808258f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.89505 > Game.Music.Time) yield return Wait(launchTime + 4.89505f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.89505f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 4.997284 > Game.Music.Time) yield return Wait(launchTime + 4.997284f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 4.997284f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.096085 > Game.Music.Time) yield return Wait(launchTime + 5.096085f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.096085f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.193161 > Game.Music.Time) yield return Wait(launchTime + 5.193161f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.193161f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.293564 > Game.Music.Time) yield return Wait(launchTime + 5.293564f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.293564f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.393967 > Game.Music.Time) yield return Wait(launchTime + 5.393967f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.393967f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.479156 > Game.Music.Time) yield return Wait(launchTime + 5.479156f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.479156f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.593307 > Game.Music.Time) yield return Wait(launchTime + 5.593307f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.593307f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.673294 > Game.Music.Time) yield return Wait(launchTime + 5.673294f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.673294f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.780502 > Game.Music.Time) yield return Wait(launchTime + 5.780502f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.780502f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 5.891174 > Game.Music.Time) yield return Wait(launchTime + 5.891174f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 5.891174f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 6.076874 > Game.Music.Time) yield return Wait(launchTime + 6.076874f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 6.076874f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 6.265854 > Game.Music.Time) yield return Wait(launchTime + 6.265854f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 6.265854f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 6.4599 > Game.Music.Time) yield return Wait(launchTime + 6.4599f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 6.4599f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 6.669968 > Game.Music.Time) yield return Wait(launchTime + 6.669968f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 6.669968f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 6.767975 > Game.Music.Time) yield return Wait(launchTime + 6.767975f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 6.767975f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.064423 > Game.Music.Time) yield return Wait(launchTime + 7.064423f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.064423f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.157776 > Game.Music.Time) yield return Wait(launchTime + 7.157776f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.157776f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.241272 > Game.Music.Time) yield return Wait(launchTime + 7.241272f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.241272f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.64946 > Game.Music.Time) yield return Wait(launchTime + 7.64946f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.64946f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.747482 > Game.Music.Time) yield return Wait(launchTime + 7.747482f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.747482f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.837723 > Game.Music.Time) yield return Wait(launchTime + 7.837723f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.837723f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 7.951294 > Game.Music.Time) yield return Wait(launchTime + 7.951294f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 7.951294f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.034607 > Game.Music.Time) yield return Wait(launchTime + 8.034607f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.034607f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.237625 > Game.Music.Time) yield return Wait(launchTime + 8.237625f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.237625f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.326202 > Game.Music.Time) yield return Wait(launchTime + 8.326202f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.326202f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.430466 > Game.Music.Time) yield return Wait(launchTime + 8.430466f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.430466f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.536331 > Game.Music.Time) yield return Wait(launchTime + 8.536331f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.536331f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.624939 > Game.Music.Time) yield return Wait(launchTime + 8.624939f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.624939f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.719818 > Game.Music.Time) yield return Wait(launchTime + 8.719818f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.719818f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.819412 > Game.Music.Time) yield return Wait(launchTime + 8.819412f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.819412f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 8.898743 > Game.Music.Time) yield return Wait(launchTime + 8.898743f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 8.898743f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.010742 > Game.Music.Time) yield return Wait(launchTime + 9.010742f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.010742f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.107208 > Game.Music.Time) yield return Wait(launchTime + 9.107208f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.107208f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.208328 > Game.Music.Time) yield return Wait(launchTime + 9.208328f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.208328f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.312531 > Game.Music.Time) yield return Wait(launchTime + 9.312531f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.312531f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.516342 > Game.Music.Time) yield return Wait(launchTime + 9.516342f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.516342f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.600327 > Game.Music.Time) yield return Wait(launchTime + 9.600327f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.600327f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 9.80722 > Game.Music.Time) yield return Wait(launchTime + 9.80722f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 9.80722f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.09972 > Game.Music.Time) yield return Wait(launchTime + 10.09972f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.09972f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.18993 > Game.Music.Time) yield return Wait(launchTime + 10.18993f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.18993f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.27701 > Game.Music.Time) yield return Wait(launchTime + 10.27701f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.27701f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.3766 > Game.Music.Time) yield return Wait(launchTime + 10.3766f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.3766f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.58659 > Game.Music.Time) yield return Wait(launchTime + 10.58659f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.58659f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.68617 > Game.Music.Time) yield return Wait(launchTime + 10.68617f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.68617f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.78262 > Game.Music.Time) yield return Wait(launchTime + 10.78262f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.78262f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.88524 > Game.Music.Time) yield return Wait(launchTime + 10.88524f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.88524f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 10.98642 > Game.Music.Time) yield return Wait(launchTime + 10.98642f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 10.98642f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 11.16688 > Game.Music.Time) yield return Wait(launchTime + 11.16688f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 11.16688f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 11.3722 > Game.Music.Time) yield return Wait(launchTime + 11.3722f);
            Player.StartCoroutine(Pattern_33/*Move*/(launchTime + 11.3722f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)"/*out*/));
            if (launchTime + 13.35699 > Game.Music.Time) yield return Wait(launchTime + 13.35699f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 13.35699f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => -300f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.7692567 > Game.Music.Time) yield return Wait(launchTime + 0.7692567f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 0.7692567f, blackboard/*out*/));
            if (launchTime + 1.581452 > Game.Music.Time) yield return Wait(launchTime + 1.581452f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 1.581452f, blackboard/*out*/));
            if (launchTime + 2.347809 > Game.Music.Time) yield return Wait(launchTime + 2.347809f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 2.347809f, blackboard/*out*/));
            if (launchTime + 3.13176 > Game.Music.Time) yield return Wait(launchTime + 3.13176f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 3.13176f, blackboard/*out*/));
            if (launchTime + 3.915711 > Game.Music.Time) yield return Wait(launchTime + 3.915711f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 3.915711f, blackboard/*out*/));
            if (launchTime + 4.703217 > Game.Music.Time) yield return Wait(launchTime + 4.703217f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 4.703217f, blackboard/*out*/));
            if (launchTime + 5.490708 > Game.Music.Time) yield return Wait(launchTime + 5.490708f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 5.490708f, blackboard/*out*/));
            if (launchTime + 6.281723 > Game.Music.Time) yield return Wait(launchTime + 6.281723f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 6.281723f, blackboard/*out*/));
            if (launchTime + 7.168107 > Game.Music.Time) yield return Wait(launchTime + 7.168107f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 7.168107f, blackboard/*out*/));
            if (launchTime + 7.955613 > Game.Music.Time) yield return Wait(launchTime + 7.955613f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 7.955613f, blackboard/*out*/));
            if (launchTime + 8.736037 > Game.Music.Time) yield return Wait(launchTime + 8.736037f);
            Player.StartCoroutine(Pattern_35/*3Shots*/(launchTime + 8.736037f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.339386 > Game.Music.Time) yield return Wait(launchTime + 0.339386f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.339386f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.339386 > Game.Music.Time) yield return Wait(launchTime + 0.339386f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.339386f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.4902802 > Game.Music.Time) yield return Wait(launchTime + 0.4902802f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.4902802f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.4902802 > Game.Music.Time) yield return Wait(launchTime + 0.4902802f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.4902802f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x0c/255f,0x2d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2571716f));
            if (launchTime + 0.002655029 > Game.Music.Time) yield return Wait(launchTime + 0.002655029f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.002655029f, blackboard, (c,i,t,l, castc, casti) => new Vector2(150f*(float)flipx,147f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-90f,90f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_38/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.3977967 > Game.Music.Time) yield return Wait(launchTime + 0.3977967f);
            Player.StartCoroutine(Pattern_40/*Melody*/(launchTime + 0.3977967f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.581063 > Game.Music.Time) yield return Wait(launchTime + 1.581063f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.581063f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.581063 > Game.Music.Time) yield return Wait(launchTime + 1.581063f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 1.581063f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.132103 > Game.Music.Time) yield return Wait(launchTime + 3.132103f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.132103f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.132103 > Game.Music.Time) yield return Wait(launchTime + 3.132103f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 3.132103f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.732758 > Game.Music.Time) yield return Wait(launchTime + 4.732758f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 4.732758f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.732758 > Game.Music.Time) yield return Wait(launchTime + 4.732758f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 4.732758f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.28901 > Game.Music.Time) yield return Wait(launchTime + 6.28901f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.28901f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.28901 > Game.Music.Time) yield return Wait(launchTime + 6.28901f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 6.28901f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.858277 > Game.Music.Time) yield return Wait(launchTime + 7.858277f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 7.858277f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.858277 > Game.Music.Time) yield return Wait(launchTime + 7.858277f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 7.858277f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.427551 > Game.Music.Time) yield return Wait(launchTime + 9.427551f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 9.427551f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.427551 > Game.Music.Time) yield return Wait(launchTime + 9.427551f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 9.427551f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.00341 > Game.Music.Time) yield return Wait(launchTime + 11.00341f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 11.00341f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.00341 > Game.Music.Time) yield return Wait(launchTime + 11.00341f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 11.00341f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 12.57273 > Game.Music.Time) yield return Wait(launchTime + 12.57273f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 12.57273f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 12.57273 > Game.Music.Time) yield return Wait(launchTime + 12.57273f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 12.57273f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 14.12234 > Game.Music.Time) yield return Wait(launchTime + 14.12234f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 14.12234f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 14.12234 > Game.Music.Time) yield return Wait(launchTime + 14.12234f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 14.12234f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 15.68507 > Game.Music.Time) yield return Wait(launchTime + 15.68507f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 15.68507f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 15.68507 > Game.Music.Time) yield return Wait(launchTime + 15.68507f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 15.68507f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 17.28062 > Game.Music.Time) yield return Wait(launchTime + 17.28062f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 17.28062f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 17.28062 > Game.Music.Time) yield return Wait(launchTime + 17.28062f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 17.28062f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 18.83024 > Game.Music.Time) yield return Wait(launchTime + 18.83024f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 18.83024f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 18.83024 > Game.Music.Time) yield return Wait(launchTime + 18.83024f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 18.83024f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 20.37982 > Game.Music.Time) yield return Wait(launchTime + 20.37982f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 20.37982f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 20.37982 > Game.Music.Time) yield return Wait(launchTime + 20.37982f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 20.37982f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 21.94253 > Game.Music.Time) yield return Wait(launchTime + 21.94253f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 21.94253f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 21.94253 > Game.Music.Time) yield return Wait(launchTime + 21.94253f);
            Player.StartCoroutine(Pattern_39/*Shot*/(launchTime + 21.94253f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x26/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2470474f));
            if (launchTime + 0.0009765625 > Game.Music.Time) yield return Wait(launchTime + 0.0009765625f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0009765625f, blackboard, (c,i,t,l, castc, casti) => new Vector2(179f*(float)flipx,167f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 90f-90f*(float)flipx, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => -1000f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x35/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.3884277 > Game.Music.Time) yield return Wait(launchTime + 0.3884277f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.3884277f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*7f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x84/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.3890991 > Game.Music.Time) yield return Wait(launchTime + 0.3890991f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3890991f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x26/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2002869f));
            if (launchTime + 0.6524429 > Game.Music.Time) yield return Wait(launchTime + 0.6524429f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.6524429f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x26/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2587891f));
            if (launchTime + 0.6539688 > Game.Music.Time) yield return Wait(launchTime + 0.6539688f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.6539688f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 1.044471 > Game.Music.Time) yield return Wait(launchTime + 1.044471f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.044471f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x26/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07740784f));
            if (launchTime + 1.045746 > Game.Music.Time) yield return Wait(launchTime + 1.045746f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 1.045746f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 1.17321 > Game.Music.Time) yield return Wait(launchTime + 1.17321f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.17321f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x26/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2821959f));
            if (launchTime + 1.177833 > Game.Music.Time) yield return Wait(launchTime + 1.177833f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.177833f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d*7f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x84/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_bullet(s)"));
            if (launchTime + 1.568519 > Game.Music.Time) yield return Wait(launchTime + 1.568519f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.568519f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 0.3825379 > Game.Music.Time) yield return Wait(launchTime + 0.3825379f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 0.3825379f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 0.6759565 > Game.Music.Time) yield return Wait(launchTime + 0.6759565f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 0.6759565f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 1.059075 > Game.Music.Time) yield return Wait(launchTime + 1.059075f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 1.059075f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 3.145751 > Game.Music.Time) yield return Wait(launchTime + 3.145751f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 3.145751f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 3.5411 > Game.Music.Time) yield return Wait(launchTime + 3.5411f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 3.5411f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 3.781548 > Game.Music.Time) yield return Wait(launchTime + 3.781548f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 3.781548f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 4.193206 > Game.Music.Time) yield return Wait(launchTime + 4.193206f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 4.193206f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 4.70263 > Game.Music.Time) yield return Wait(launchTime + 4.70263f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 4.70263f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 5.142822 > Game.Music.Time) yield return Wait(launchTime + 5.142822f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 5.142822f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 5.517776 > Game.Music.Time) yield return Wait(launchTime + 5.517776f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 5.517776f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 5.892715 > Game.Music.Time) yield return Wait(launchTime + 5.892715f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 5.892715f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 6.149482 > Game.Music.Time) yield return Wait(launchTime + 6.149482f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 6.149482f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 6.536652 > Game.Music.Time) yield return Wait(launchTime + 6.536652f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 6.536652f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 6.919762 > Game.Music.Time) yield return Wait(launchTime + 6.919762f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 6.919762f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 7.315094 > Game.Music.Time) yield return Wait(launchTime + 7.315094f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 7.315094f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 7.857147 > Game.Music.Time) yield return Wait(launchTime + 7.857147f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 7.857147f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 8.244331 > Game.Music.Time) yield return Wait(launchTime + 8.244331f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 8.244331f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 8.619278 > Game.Music.Time) yield return Wait(launchTime + 8.619278f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 8.619278f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 8.896454 > Game.Music.Time) yield return Wait(launchTime + 8.896454f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 8.896454f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 10.20875 > Game.Music.Time) yield return Wait(launchTime + 10.20875f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 10.20875f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 10.59185 > Game.Music.Time) yield return Wait(launchTime + 10.59185f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 10.59185f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 10.85675 > Game.Music.Time) yield return Wait(launchTime + 10.85675f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 10.85675f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 11.248 > Game.Music.Time) yield return Wait(launchTime + 11.248f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 11.248f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 11.63925 > Game.Music.Time) yield return Wait(launchTime + 11.63925f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 11.63925f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 12.55219 > Game.Music.Time) yield return Wait(launchTime + 12.55219f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 12.55219f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 12.94343 > Game.Music.Time) yield return Wait(launchTime + 12.94343f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 12.94343f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 13.20023 > Game.Music.Time) yield return Wait(launchTime + 13.20023f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 13.20023f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 13.59146 > Game.Music.Time) yield return Wait(launchTime + 13.59146f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 13.59146f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 15.68631 > Game.Music.Time) yield return Wait(launchTime + 15.68631f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 15.68631f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 16.06941 > Game.Music.Time) yield return Wait(launchTime + 16.06941f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 16.06941f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 16.34246 > Game.Music.Time) yield return Wait(launchTime + 16.34246f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 16.34246f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 16.75005 > Game.Music.Time) yield return Wait(launchTime + 16.75005f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 16.75005f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 17.27986 > Game.Music.Time) yield return Wait(launchTime + 17.27986f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 17.27986f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 17.67926 > Game.Music.Time) yield return Wait(launchTime + 17.67926f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 17.67926f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 18.04607 > Game.Music.Time) yield return Wait(launchTime + 18.04607f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 18.04607f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 18.4251 > Game.Music.Time) yield return Wait(launchTime + 18.4251f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 18.4251f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 18.69817 > Game.Music.Time) yield return Wait(launchTime + 18.69817f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 18.69817f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 19.07312 > Game.Music.Time) yield return Wait(launchTime + 19.07312f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 19.07312f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 19.47662 > Game.Music.Time) yield return Wait(launchTime + 19.47662f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 19.47662f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 19.88011 > Game.Music.Time) yield return Wait(launchTime + 19.88011f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 19.88011f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 20.38549 > Game.Music.Time) yield return Wait(launchTime + 20.38549f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 20.38549f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 20.76049 > Game.Music.Time) yield return Wait(launchTime + 20.76049f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 20.76049f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 21.168 > Game.Music.Time) yield return Wait(launchTime + 21.168f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 21.168f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 21.44105 > Game.Music.Time) yield return Wait(launchTime + 21.44105f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 21.44105f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 22.74518 > Game.Music.Time) yield return Wait(launchTime + 22.74518f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 22.74518f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 23.1487 > Game.Music.Time) yield return Wait(launchTime + 23.1487f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 23.1487f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 23.45845 > Game.Music.Time) yield return Wait(launchTime + 23.45845f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 23.45845f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 23.79669 > Game.Music.Time) yield return Wait(launchTime + 23.79669f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 23.79669f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            if (launchTime + 24.1839 > Game.Music.Time) yield return Wait(launchTime + 24.1839f);
            Player.StartCoroutine(Pattern_41/*Shot*/(launchTime + 24.1839f, blackboard, (c,i,t,l, castc, casti) => 10f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_41(float launchTime, Blackboard outboard, InputFunc length_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, length_input, "node_0.output_patterninput");
            object length = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => length = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-320f,320f)*(float)ar,240f), (c,i,t,l, castc, casti) => (float)length*2f*(float)toint((float)d,3f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+10f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.003410339 > Game.Music.Time) yield return Wait(launchTime + 0.003410339f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.003410339f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-320f,320f)*(float)ar,240f), (c,i,t,l, castc, casti) => (float)length*2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+10f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_42(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_43/*MachineGun*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.1946258 > Game.Music.Time) yield return Wait(launchTime + 0.1946258f);
            Player.StartCoroutine(Pattern_46/*Explosions*/(launchTime + 0.1946258f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_43(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.56279 > Game.Music.Time) yield return Wait(launchTime + 1.56279f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 1.56279f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.56279 > Game.Music.Time) yield return Wait(launchTime + 1.56279f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 1.56279f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.124893 > Game.Music.Time) yield return Wait(launchTime + 3.124893f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 3.124893f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.124893 > Game.Music.Time) yield return Wait(launchTime + 3.124893f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 3.124893f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.689239 > Game.Music.Time) yield return Wait(launchTime + 4.689239f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 4.689239f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.689239 > Game.Music.Time) yield return Wait(launchTime + 4.689239f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 4.689239f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.266571 > Game.Music.Time) yield return Wait(launchTime + 6.266571f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 6.266571f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.266571 > Game.Music.Time) yield return Wait(launchTime + 6.266571f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 6.266571f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.824493 > Game.Music.Time) yield return Wait(launchTime + 7.824493f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 7.824493f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.824493 > Game.Music.Time) yield return Wait(launchTime + 7.824493f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 7.824493f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.40184 > Game.Music.Time) yield return Wait(launchTime + 9.40184f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 9.40184f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.40184 > Game.Music.Time) yield return Wait(launchTime + 9.40184f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 9.40184f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.97592 > Game.Music.Time) yield return Wait(launchTime + 10.97592f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 10.97592f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.97592 > Game.Music.Time) yield return Wait(launchTime + 10.97592f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 10.97592f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_44(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.08894348 > Game.Music.Time) yield return Wait(launchTime + 0.08894348f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0.08894348f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.1927338 > Game.Music.Time) yield return Wait(launchTime + 0.1927338f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0.1927338f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.303421 > Game.Music.Time) yield return Wait(launchTime + 0.303421f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0.303421f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.3934174 > Game.Music.Time) yield return Wait(launchTime + 0.3934174f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0.3934174f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.4832764 > Game.Music.Time) yield return Wait(launchTime + 0.4832764f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0.4832764f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.5870667 > Game.Music.Time) yield return Wait(launchTime + 0.5870667f);
            Player.StartCoroutine(Pattern_45/*Bullet*/(launchTime + 0.5870667f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.613037 > Game.Music.Time) yield return Wait(launchTime + 0.613037f);
            Player.StartCoroutine(Pattern_48/*BigBullet*/(launchTime + 0.613037f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.9737549 > Game.Music.Time) yield return Wait(launchTime + 0.9737549f);
            Player.StartCoroutine(Pattern_48/*BigBullet*/(launchTime + 0.9737549f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_45(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-91f*(float)flipx,85f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => (float)rndf(-20f,20f), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+50f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.002075196 > Game.Music.Time) yield return Wait(launchTime + 0.002075196f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.002075196f, blackboard, (c,i,t,l, castc, casti) => new Color(0x35/255f,0x33/255f,0x11/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.06965637f));
            yield break;
        }

        private IEnumerator Pattern_46(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.7765499 > Game.Music.Time) yield return Wait(launchTime + 0.7765499f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 0.7765499f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.7765499 > Game.Music.Time) yield return Wait(launchTime + 0.7765499f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 0.7765499f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.582535 > Game.Music.Time) yield return Wait(launchTime + 1.582535f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 1.582535f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.582535 > Game.Music.Time) yield return Wait(launchTime + 1.582535f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 1.582535f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.874939 > Game.Music.Time) yield return Wait(launchTime + 1.874939f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 1.874939f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.874939 > Game.Music.Time) yield return Wait(launchTime + 1.874939f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 1.874939f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.359924 > Game.Music.Time) yield return Wait(launchTime + 2.359924f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 2.359924f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.359924 > Game.Music.Time) yield return Wait(launchTime + 2.359924f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 2.359924f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.130203 > Game.Music.Time) yield return Wait(launchTime + 3.130203f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 3.130203f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.130203 > Game.Music.Time) yield return Wait(launchTime + 3.130203f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 3.130203f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.917465 > Game.Music.Time) yield return Wait(launchTime + 3.917465f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 3.917465f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.917465 > Game.Music.Time) yield return Wait(launchTime + 3.917465f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 3.917465f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.720093 > Game.Music.Time) yield return Wait(launchTime + 4.720093f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 4.720093f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.720093 > Game.Music.Time) yield return Wait(launchTime + 4.720093f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 4.720093f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.497604 > Game.Music.Time) yield return Wait(launchTime + 5.497604f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 5.497604f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.497604 > Game.Music.Time) yield return Wait(launchTime + 5.497604f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 5.497604f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.27385 > Game.Music.Time) yield return Wait(launchTime + 6.27385f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 6.27385f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.27385 > Game.Music.Time) yield return Wait(launchTime + 6.27385f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 6.27385f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.062729 > Game.Music.Time) yield return Wait(launchTime + 7.062729f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 7.062729f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.062729 > Game.Music.Time) yield return Wait(launchTime + 7.062729f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 7.062729f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.838959 > Game.Music.Time) yield return Wait(launchTime + 7.838959f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 7.838959f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.838959 > Game.Music.Time) yield return Wait(launchTime + 7.838959f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 7.838959f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 8.632065 > Game.Music.Time) yield return Wait(launchTime + 8.632065f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 8.632065f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 8.632065 > Game.Music.Time) yield return Wait(launchTime + 8.632065f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 8.632065f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.429367 > Game.Music.Time) yield return Wait(launchTime + 9.429367f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 9.429367f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.429367 > Game.Music.Time) yield return Wait(launchTime + 9.429367f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 9.429367f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 10.20561 > Game.Music.Time) yield return Wait(launchTime + 10.20561f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 10.20561f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 10.20561 > Game.Music.Time) yield return Wait(launchTime + 10.20561f);
            Player.StartCoroutine(Pattern_47/*Shot*/(launchTime + 10.20561f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_47(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(154f*(float)flipx,129f), (c,i,t,l, castc, casti) => 4f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x9a/255f,0x4b/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.0002441406 > Game.Music.Time) yield return Wait(launchTime + 0.0002441406f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0002441406f, blackboard, (c,i,t,l, castc, casti) => new Vector2(154f*(float)flipx,129f), (c,i,t,l, castc, casti) => 4f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x4b/255f,0x77/255f,0xc1/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_48(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-91f*(float)flipx,85f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => (float)rndf(-20f,20f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xa2/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.001113892 > Game.Music.Time) yield return Wait(launchTime + 0.001113892f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.001113892f, blackboard, (c,i,t,l, castc, casti) => new Color(0x31/255f,0x1d/255f,0x0c/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1681366f));
            yield break;
        }

        private IEnumerator Pattern_49(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_50/*MachineGun*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.7761382 > Game.Music.Time) yield return Wait(launchTime + 0.7761382f);
            Player.StartCoroutine(Pattern_53/*Circle*/(launchTime + 0.7761382f, blackboard/*out*/));
            if (launchTime + 3.311936 > Game.Music.Time) yield return Wait(launchTime + 3.311936f);
            Player.StartCoroutine(Pattern_74/*Lazer*/(launchTime + 3.311936f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.311936 > Game.Music.Time) yield return Wait(launchTime + 3.311936f);
            Player.StartCoroutine(Pattern_74/*Lazer*/(launchTime + 3.311936f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.872071 > Game.Music.Time) yield return Wait(launchTime + 5.872071f);
            Player.StartCoroutine(Pattern_79/*Ships*/(launchTime + 5.872071f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_50(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.555679 > Game.Music.Time) yield return Wait(launchTime + 1.555679f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 1.555679f, blackboard/*out*/));
            if (launchTime + 3.142227 > Game.Music.Time) yield return Wait(launchTime + 3.142227f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 3.142227f, blackboard/*out*/));
            if (launchTime + 4.695144 > Game.Music.Time) yield return Wait(launchTime + 4.695144f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 4.695144f, blackboard/*out*/));
            if (launchTime + 6.279968 > Game.Music.Time) yield return Wait(launchTime + 6.279968f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 6.279968f, blackboard/*out*/));
            if (launchTime + 7.82756 > Game.Music.Time) yield return Wait(launchTime + 7.82756f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 7.82756f, blackboard/*out*/));
            if (launchTime + 9.407059 > Game.Music.Time) yield return Wait(launchTime + 9.407059f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 9.407059f, blackboard/*out*/));
            if (launchTime + 11.06641 > Game.Music.Time) yield return Wait(launchTime + 11.06641f);
            Player.StartCoroutine(Pattern_51/*Shot*/(launchTime + 11.06641f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_51(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.197113 > Game.Music.Time) yield return Wait(launchTime + 0.197113f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.197113f, blackboard/*out*/));
            if (launchTime + 0.2858124 > Game.Music.Time) yield return Wait(launchTime + 0.2858124f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.2858124f, blackboard/*out*/));
            if (launchTime + 0.3875732 > Game.Music.Time) yield return Wait(launchTime + 0.3875732f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.3875732f, blackboard/*out*/));
            if (launchTime + 0.4905395 > Game.Music.Time) yield return Wait(launchTime + 0.4905395f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.4905395f, blackboard/*out*/));
            if (launchTime + 0.5951843 > Game.Music.Time) yield return Wait(launchTime + 0.5951843f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.5951843f, blackboard/*out*/));
            if (launchTime + 0.6947784 > Game.Music.Time) yield return Wait(launchTime + 0.6947784f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.6947784f, blackboard/*out*/));
            if (launchTime + 0.7874298 > Game.Music.Time) yield return Wait(launchTime + 0.7874298f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.7874298f, blackboard/*out*/));
            if (launchTime + 0.9969025 > Game.Music.Time) yield return Wait(launchTime + 0.9969025f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 0.9969025f, blackboard/*out*/));
            if (launchTime + 1.181305 > Game.Music.Time) yield return Wait(launchTime + 1.181305f);
            Player.StartCoroutine(Pattern_52/*Bullet*/(launchTime + 1.181305f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_52(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,96f), (c,i,t,l, castc, casti) => (float)d+(float)toint((float)d,3f), (c,i,t,l, castc, casti) => new Vector2((float)rndf(-340f,340f)*(float)ar,0f), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-162f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x8d/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.003448487 > Game.Music.Time) yield return Wait(launchTime + 0.003448487f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.003448487f, blackboard, (c,i,t,l, castc, casti) => new Color(0x35/255f,0x1f/255f,0x0a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4099731f));
            yield break;
        }

        private IEnumerator Pattern_53(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.563674 > Game.Music.Time) yield return Wait(launchTime + 1.563674f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 1.563674f, blackboard/*out*/));
            if (launchTime + 3.139434 > Game.Music.Time) yield return Wait(launchTime + 3.139434f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 3.139434f, blackboard/*out*/));
            if (launchTime + 4.701171 > Game.Music.Time) yield return Wait(launchTime + 4.701171f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 4.701171f, blackboard/*out*/));
            if (launchTime + 6.283905 > Game.Music.Time) yield return Wait(launchTime + 6.283905f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 6.283905f, blackboard/*out*/));
            if (launchTime + 7.845627 > Game.Music.Time) yield return Wait(launchTime + 7.845627f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 7.845627f, blackboard/*out*/));
            if (launchTime + 9.407331 > Game.Music.Time) yield return Wait(launchTime + 9.407331f);
            Player.StartCoroutine(Pattern_54/*2Shots*/(launchTime + 9.407331f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_54(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_55/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_55/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.3992157 > Game.Music.Time) yield return Wait(launchTime + 0.3992157f);
            Player.StartCoroutine(Pattern_55/*Shot*/(launchTime + 0.3992157f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.3992157 > Game.Music.Time) yield return Wait(launchTime + 0.3992157f);
            Player.StartCoroutine(Pattern_55/*Shot*/(launchTime + 0.3992157f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_55(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 180f));
            blackboard.SetValue("node_2.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x10/255f,0x28/255f,0x40/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2665558f));
            if (launchTime + 0.009994507 > Game.Music.Time) yield return Wait(launchTime + 0.009994507f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.009994507f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-93f*(float)flipx,101f), (c,i,t,l, castc, casti) => 3f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x86/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_56(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_61/*Bass*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.01377869 > Game.Music.Time) yield return Wait(launchTime + 0.01377869f);
            Player.StartCoroutine(Pattern_57/*Wob*/(launchTime + 0.01377869f, blackboard/*out*/));
            if (launchTime + 6.651413 > Game.Music.Time) yield return Wait(launchTime + 6.651413f);
            Player.StartCoroutine(Pattern_63/*Beat*/(launchTime + 6.651413f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_57(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_58/*FirstVar*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.565827 > Game.Music.Time) yield return Wait(launchTime + 1.565827f);
            Player.StartCoroutine(Pattern_58/*FirstVar*/(launchTime + 1.565827f, blackboard/*out*/));
            if (launchTime + 3.142822 > Game.Music.Time) yield return Wait(launchTime + 3.142822f);
            Player.StartCoroutine(Pattern_58/*FirstVar*/(launchTime + 3.142822f, blackboard/*out*/));
            if (launchTime + 4.703094 > Game.Music.Time) yield return Wait(launchTime + 4.703094f);
            Player.StartCoroutine(Pattern_58/*FirstVar*/(launchTime + 4.703094f, blackboard/*out*/));
            if (launchTime + 6.285538 > Game.Music.Time) yield return Wait(launchTime + 6.285538f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 6.285538f, blackboard/*out*/));
            if (launchTime + 7.836075 > Game.Music.Time) yield return Wait(launchTime + 7.836075f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 7.836075f, blackboard/*out*/));
            if (launchTime + 9.414307 > Game.Music.Time) yield return Wait(launchTime + 9.414307f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 9.414307f, blackboard/*out*/));
            if (launchTime + 10.98111 > Game.Music.Time) yield return Wait(launchTime + 10.98111f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 10.98111f, blackboard/*out*/));
            if (launchTime + 12.54509 > Game.Music.Time) yield return Wait(launchTime + 12.54509f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 12.54509f, blackboard/*out*/));
            if (launchTime + 14.12912 > Game.Music.Time) yield return Wait(launchTime + 14.12912f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 14.12912f, blackboard/*out*/));
            if (launchTime + 15.68732 > Game.Music.Time) yield return Wait(launchTime + 15.68732f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 15.68732f, blackboard/*out*/));
            if (launchTime + 16.46637 > Game.Music.Time) yield return Wait(launchTime + 16.46637f);
            Player.StartCoroutine(Pattern_65/*Big*/(launchTime + 16.46637f, blackboard/*out*/));
            if (launchTime + 17.25713 > Game.Music.Time) yield return Wait(launchTime + 17.25713f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 17.25713f, blackboard/*out*/));
            if (launchTime + 18.83769 > Game.Music.Time) yield return Wait(launchTime + 18.83769f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 18.83769f, blackboard/*out*/));
            if (launchTime + 20.41321 > Game.Music.Time) yield return Wait(launchTime + 20.41321f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 20.41321f, blackboard/*out*/));
            if (launchTime + 21.97385 > Game.Music.Time) yield return Wait(launchTime + 21.97385f);
            Player.StartCoroutine(Pattern_60/*SecondVar*/(launchTime + 21.97385f, blackboard/*out*/));
            if (launchTime + 22.75806 > Game.Music.Time) yield return Wait(launchTime + 22.75806f);
            Player.StartCoroutine(Pattern_65/*Big*/(launchTime + 22.75806f, blackboard/*out*/));
            if (launchTime + 23.56656 > Game.Music.Time) yield return Wait(launchTime + 23.56656f);
            Player.StartCoroutine(Pattern_65/*Big*/(launchTime + 23.56656f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_58(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 0.1734619 > Game.Music.Time) yield return Wait(launchTime + 0.1734619f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.1734619f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.3635559 > Game.Music.Time) yield return Wait(launchTime + 0.3635559f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.3635559f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.581665 > Game.Music.Time) yield return Wait(launchTime + 0.581665f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.581665f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.7718048 > Game.Music.Time) yield return Wait(launchTime + 0.7718048f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.7718048f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.9618835 > Game.Music.Time) yield return Wait(launchTime + 0.9618835f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.9618835f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 1.185639 > Game.Music.Time) yield return Wait(launchTime + 1.185639f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 1.185639f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 1.381363 > Game.Music.Time) yield return Wait(launchTime + 1.381363f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 1.381363f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            yield break;
        }

        private IEnumerator Pattern_59(float launchTime, Blackboard outboard, InputFunc count_input, InputFunc color_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, count_input, "node_0.output_patterninput");
            object count = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => count = o);

            SubscribeInput(outboard, blackboard, color_input, "node_1.output_patterninput");
            object color = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => color = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,124f), (c,i,t,l, castc, casti) => (float)count/(3f-(float)d/2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-30f,30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,250f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_60(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 0.1846161 > Game.Music.Time) yield return Wait(launchTime + 0.1846161f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.1846161f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.379715 > Game.Music.Time) yield return Wait(launchTime + 0.379715f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.379715f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.5748444 > Game.Music.Time) yield return Wait(launchTime + 0.5748444f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.5748444f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 0.7814942 > Game.Music.Time) yield return Wait(launchTime + 0.7814942f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 0.7814942f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 1.168853 > Game.Music.Time) yield return Wait(launchTime + 1.168853f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 1.168853f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            if (launchTime + 1.363968 > Game.Music.Time) yield return Wait(launchTime + 1.363968f);
            Player.StartCoroutine(Pattern_59/*Shot*/(launchTime + 1.363968f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6e/255f,0x00/255f)/*out*/));
            yield break;
        }

        private IEnumerator Pattern_61(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.570907 > Game.Music.Time) yield return Wait(launchTime + 1.570907f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 1.570907f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.570907 > Game.Music.Time) yield return Wait(launchTime + 1.570907f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 1.570907f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.149261 > Game.Music.Time) yield return Wait(launchTime + 3.149261f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 3.149261f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.149261 > Game.Music.Time) yield return Wait(launchTime + 3.149261f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 3.149261f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.710845 > Game.Music.Time) yield return Wait(launchTime + 4.710845f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 4.710845f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.710845 > Game.Music.Time) yield return Wait(launchTime + 4.710845f);
            Player.StartCoroutine(Pattern_62/*Pattern*/(launchTime + 4.710845f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_62(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x36/255f,0x12/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2289276f));
            if (launchTime + 0.00239563 > Game.Music.Time) yield return Wait(launchTime + 0.00239563f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.00239563f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-133f*(float)flipx,108f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => -400f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x66/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.301773 > Game.Music.Time) yield return Wait(launchTime + 0.301773f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.301773f, blackboard, (c,i,t,l, castc, casti) => new Color(0x36/255f,0x12/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1904907f));
            if (launchTime + 0.3040009 > Game.Music.Time) yield return Wait(launchTime + 0.3040009f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.3040009f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f-70f+140f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => -400f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x66/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            if (launchTime + 0.3103485 > Game.Music.Time) yield return Wait(launchTime + 0.3103485f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.3103485f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 0.5925751 > Game.Music.Time) yield return Wait(launchTime + 0.5925751f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5925751f, blackboard, (c,i,t,l, castc, casti) => new Color(0x36/255f,0x12/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2179413f));
            if (launchTime + 0.5961608 > Game.Music.Time) yield return Wait(launchTime + 0.5961608f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.5961608f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f-70f+140f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => -400f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x66/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_bullet(s)"));
            if (launchTime + 0.6059113 > Game.Music.Time) yield return Wait(launchTime + 0.6059113f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.6059113f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.189758 > Game.Music.Time) yield return Wait(launchTime + 1.189758f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.189758f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet(s)", (c,i,t,l, castc, casti) => 2.5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_12.output_bullet(s)"));
            if (launchTime + 1.193375 > Game.Music.Time) yield return Wait(launchTime + 1.193375f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.193375f, blackboard, (c,i,t,l, castc, casti) => new Color(0x30/255f,0x36/255f,0x0e/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2645569f));
            if (launchTime + 1.197968 > Game.Music.Time) yield return Wait(launchTime + 1.197968f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.197968f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_63(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.8074337 > Game.Music.Time) yield return Wait(launchTime + 0.8074337f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 0.8074337f, blackboard/*out*/));
            if (launchTime + 1.559418 > Game.Music.Time) yield return Wait(launchTime + 1.559418f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 1.559418f, blackboard/*out*/));
            if (launchTime + 2.358948 > Game.Music.Time) yield return Wait(launchTime + 2.358948f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 2.358948f, blackboard/*out*/));
            if (launchTime + 3.150497 > Game.Music.Time) yield return Wait(launchTime + 3.150497f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 3.150497f, blackboard/*out*/));
            if (launchTime + 3.923355 > Game.Music.Time) yield return Wait(launchTime + 3.923355f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 3.923355f, blackboard/*out*/));
            if (launchTime + 4.70993 > Game.Music.Time) yield return Wait(launchTime + 4.70993f);
            Player.StartCoroutine(Pattern_64/*Shot*/(launchTime + 4.70993f, blackboard/*out*/));
            if (launchTime + 5.891724 > Game.Music.Time) yield return Wait(launchTime + 5.891724f);
            Player.StartCoroutine(Pattern_29/*Beat*/(launchTime + 5.891724f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_64(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x0e/255f,0x0c/255f,0x38/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3485413f));
            if (launchTime + 0.008895874 > Game.Music.Time) yield return Wait(launchTime + 0.008895874f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.008895874f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-200f,200f),180f), (c,i,t,l, castc, casti) => (float)d*2f+2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x61/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_65(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x31/255f,0x0c/255f,0x35/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3295746f));
            if (launchTime + 0.009078979 > Game.Music.Time) yield return Wait(launchTime + 0.009078979f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.009078979f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-1f,124f), (c,i,t,l, castc, casti) => (float)d*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_66(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_68/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4119315 > Game.Music.Time) yield return Wait(launchTime + 0.4119315f);
            Player.StartCoroutine(Pattern_67/*Small*/(launchTime + 0.4119315f, blackboard/*out*/));
            if (launchTime + 5.735654 > Game.Music.Time) yield return Wait(launchTime + 5.735654f);
            Player.StartCoroutine(Pattern_72/*Guitar*/(launchTime + 5.735654f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_67(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.7697561 > Game.Music.Time) yield return Wait(launchTime + 0.7697561f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 0.7697561f, blackboard/*out*/));
            if (launchTime + 1.554658 > Game.Music.Time) yield return Wait(launchTime + 1.554658f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 1.554658f, blackboard/*out*/));
            if (launchTime + 2.331963 > Game.Music.Time) yield return Wait(launchTime + 2.331963f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 2.331963f, blackboard/*out*/));
            if (launchTime + 3.118875 > Game.Music.Time) yield return Wait(launchTime + 3.118875f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 3.118875f, blackboard/*out*/));
            if (launchTime + 3.899978 > Game.Music.Time) yield return Wait(launchTime + 3.899978f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 3.899978f, blackboard/*out*/));
            if (launchTime + 4.68689 > Game.Music.Time) yield return Wait(launchTime + 4.68689f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 4.68689f, blackboard/*out*/));
            if (launchTime + 5.459284 > Game.Music.Time) yield return Wait(launchTime + 5.459284f);
            Player.StartCoroutine(Pattern_69/*3shots*/(launchTime + 5.459284f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_68(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.3067836 > Game.Music.Time) yield return Wait(launchTime + 0.3067836f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 0.3067836f, blackboard/*out*/));
            if (launchTime + 0.9934274 > Game.Music.Time) yield return Wait(launchTime + 0.9934274f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 0.9934274f, blackboard/*out*/));
            if (launchTime + 1.358834 > Game.Music.Time) yield return Wait(launchTime + 1.358834f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 1.358834f, blackboard/*out*/));
            if (launchTime + 1.563623 > Game.Music.Time) yield return Wait(launchTime + 1.563623f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 1.563623f, blackboard/*out*/));
            if (launchTime + 1.868797 > Game.Music.Time) yield return Wait(launchTime + 1.868797f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 1.868797f, blackboard/*out*/));
            if (launchTime + 2.230188 > Game.Music.Time) yield return Wait(launchTime + 2.230188f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 2.230188f, blackboard/*out*/));
            if (launchTime + 2.543394 > Game.Music.Time) yield return Wait(launchTime + 2.543394f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 2.543394f, blackboard/*out*/));
            if (launchTime + 2.744167 > Game.Music.Time) yield return Wait(launchTime + 2.744167f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 2.744167f, blackboard/*out*/));
            if (launchTime + 2.836523 > Game.Music.Time) yield return Wait(launchTime + 2.836523f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 2.836523f, blackboard/*out*/));
            if (launchTime + 2.940925 > Game.Music.Time) yield return Wait(launchTime + 2.940925f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 2.940925f, blackboard/*out*/));
            if (launchTime + 3.041311 > Game.Music.Time) yield return Wait(launchTime + 3.041311f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 3.041311f, blackboard/*out*/));
            if (launchTime + 3.149728 > Game.Music.Time) yield return Wait(launchTime + 3.149728f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 3.149728f, blackboard/*out*/));
            if (launchTime + 3.426796 > Game.Music.Time) yield return Wait(launchTime + 3.426796f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 3.426796f, blackboard/*out*/));
            if (launchTime + 4.109423 > Game.Music.Time) yield return Wait(launchTime + 4.109423f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 4.109423f, blackboard/*out*/));
            if (launchTime + 4.506954 > Game.Music.Time) yield return Wait(launchTime + 4.506954f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 4.506954f, blackboard/*out*/));
            if (launchTime + 4.69568 > Game.Music.Time) yield return Wait(launchTime + 4.69568f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 4.69568f, blackboard/*out*/));
            if (launchTime + 4.992823 > Game.Music.Time) yield return Wait(launchTime + 4.992823f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 4.992823f, blackboard/*out*/));
            if (launchTime + 5.390354 > Game.Music.Time) yield return Wait(launchTime + 5.390354f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 5.390354f, blackboard/*out*/));
            if (launchTime + 5.679469 > Game.Music.Time) yield return Wait(launchTime + 5.679469f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 5.679469f, blackboard/*out*/));
            if (launchTime + 5.872211 > Game.Music.Time) yield return Wait(launchTime + 5.872211f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 5.872211f, blackboard/*out*/));
            if (launchTime + 5.992675 > Game.Music.Time) yield return Wait(launchTime + 5.992675f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 5.992675f, blackboard/*out*/));
            if (launchTime + 6.076998 > Game.Music.Time) yield return Wait(launchTime + 6.076998f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.076998f, blackboard/*out*/));
            if (launchTime + 6.169354 > Game.Music.Time) yield return Wait(launchTime + 6.169354f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.169354f, blackboard/*out*/));
            if (launchTime + 6.277771 > Game.Music.Time) yield return Wait(launchTime + 6.277771f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.277771f, blackboard/*out*/));
            if (launchTime + 6.386189 > Game.Music.Time) yield return Wait(launchTime + 6.386189f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.386189f, blackboard/*out*/));
            if (launchTime + 6.683333 > Game.Music.Time) yield return Wait(launchTime + 6.683333f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.683333f, blackboard/*out*/));
            if (launchTime + 6.87206 > Game.Music.Time) yield return Wait(launchTime + 6.87206f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.87206f, blackboard/*out*/));
            if (launchTime + 6.956382 > Game.Music.Time) yield return Wait(launchTime + 6.956382f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 6.956382f, blackboard/*out*/));
            if (launchTime + 7.141096 > Game.Music.Time) yield return Wait(launchTime + 7.141096f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 7.141096f, blackboard/*out*/));
            if (launchTime + 7.253525 > Game.Music.Time) yield return Wait(launchTime + 7.253525f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 7.253525f, blackboard/*out*/));
            if (launchTime + 7.450286 > Game.Music.Time) yield return Wait(launchTime + 7.450286f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 7.450286f, blackboard/*out*/));
            if (launchTime + 7.550673 > Game.Music.Time) yield return Wait(launchTime + 7.550673f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 7.550673f, blackboard/*out*/));
            if (launchTime + 7.643028 > Game.Music.Time) yield return Wait(launchTime + 7.643028f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 7.643028f, blackboard/*out*/));
            if (launchTime + 7.843796 > Game.Music.Time) yield return Wait(launchTime + 7.843796f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 7.843796f, blackboard/*out*/));
            if (launchTime + 8.04859 > Game.Music.Time) yield return Wait(launchTime + 8.04859f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 8.04859f, blackboard/*out*/));
            if (launchTime + 8.144966 > Game.Music.Time) yield return Wait(launchTime + 8.144966f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 8.144966f, blackboard/*out*/));
            if (launchTime + 8.430059 > Game.Music.Time) yield return Wait(launchTime + 8.430059f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 8.430059f, blackboard/*out*/));
            if (launchTime + 8.542488 > Game.Music.Time) yield return Wait(launchTime + 8.542488f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 8.542488f, blackboard/*out*/));
            if (launchTime + 8.723186 > Game.Music.Time) yield return Wait(launchTime + 8.723186f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 8.723186f, blackboard/*out*/));
            if (launchTime + 8.839638 > Game.Music.Time) yield return Wait(launchTime + 8.839638f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 8.839638f, blackboard/*out*/));
            if (launchTime + 9.028363 > Game.Music.Time) yield return Wait(launchTime + 9.028363f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.028363f, blackboard/*out*/));
            if (launchTime + 9.120716 > Game.Music.Time) yield return Wait(launchTime + 9.120716f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.120716f, blackboard/*out*/));
            if (launchTime + 9.221107 > Game.Music.Time) yield return Wait(launchTime + 9.221107f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.221107f, blackboard/*out*/));
            if (launchTime + 9.321489 > Game.Music.Time) yield return Wait(launchTime + 9.321489f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.321489f, blackboard/*out*/));
            if (launchTime + 9.506201 > Game.Music.Time) yield return Wait(launchTime + 9.506201f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.506201f, blackboard/*out*/));
            if (launchTime + 9.698938 > Game.Music.Time) yield return Wait(launchTime + 9.698938f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.698938f, blackboard/*out*/));
            if (launchTime + 9.807362 > Game.Music.Time) yield return Wait(launchTime + 9.807362f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 9.807362f, blackboard/*out*/));
            if (launchTime + 10.00813 > Game.Music.Time) yield return Wait(launchTime + 10.00813f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.00813f, blackboard/*out*/));
            if (launchTime + 10.10048 > Game.Music.Time) yield return Wait(launchTime + 10.10048f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.10048f, blackboard/*out*/));
            if (launchTime + 10.29725 > Game.Music.Time) yield return Wait(launchTime + 10.29725f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.29725f, blackboard/*out*/));
            if (launchTime + 10.40164 > Game.Music.Time) yield return Wait(launchTime + 10.40164f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.40164f, blackboard/*out*/));
            if (launchTime + 10.58636 > Game.Music.Time) yield return Wait(launchTime + 10.58636f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.58636f, blackboard/*out*/));
            if (launchTime + 10.69477 > Game.Music.Time) yield return Wait(launchTime + 10.69477f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.69477f, blackboard/*out*/));
            if (launchTime + 10.79114 > Game.Music.Time) yield return Wait(launchTime + 10.79114f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.79114f, blackboard/*out*/));
            if (launchTime + 10.87546 > Game.Music.Time) yield return Wait(launchTime + 10.87546f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.87546f, blackboard/*out*/));
            if (launchTime + 10.97184 > Game.Music.Time) yield return Wait(launchTime + 10.97184f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 10.97184f, blackboard/*out*/));
            if (launchTime + 11.1686 > Game.Music.Time) yield return Wait(launchTime + 11.1686f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.1686f, blackboard/*out*/));
            if (launchTime + 11.26898 > Game.Music.Time) yield return Wait(launchTime + 11.26898f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.26898f, blackboard/*out*/));
            if (launchTime + 11.56613 > Game.Music.Time) yield return Wait(launchTime + 11.56613f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.56613f, blackboard/*out*/));
            if (launchTime + 11.66651 > Game.Music.Time) yield return Wait(launchTime + 11.66651f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.66651f, blackboard/*out*/));
            if (launchTime + 11.77092 > Game.Music.Time) yield return Wait(launchTime + 11.77092f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.77092f, blackboard/*out*/));
            if (launchTime + 11.86327 > Game.Music.Time) yield return Wait(launchTime + 11.86327f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.86327f, blackboard/*out*/));
            if (launchTime + 11.96367 > Game.Music.Time) yield return Wait(launchTime + 11.96367f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 11.96367f, blackboard/*out*/));
            if (launchTime + 12.04799 > Game.Music.Time) yield return Wait(launchTime + 12.04799f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 12.04799f, blackboard/*out*/));
            if (launchTime + 12.16041 > Game.Music.Time) yield return Wait(launchTime + 12.16041f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 12.16041f, blackboard/*out*/));
            if (launchTime + 12.26081 > Game.Music.Time) yield return Wait(launchTime + 12.26081f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 12.26081f, blackboard/*out*/));
            if (launchTime + 12.3371 > Game.Music.Time) yield return Wait(launchTime + 12.3371f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 12.3371f, blackboard/*out*/));
            if (launchTime + 12.45356 > Game.Music.Time) yield return Wait(launchTime + 12.45356f);
            Player.StartCoroutine(Pattern_71/*explode*/(launchTime + 12.45356f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_69(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_70/*shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_70/*shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.104352 > Game.Music.Time) yield return Wait(launchTime + 0.104352f);
            Player.StartCoroutine(Pattern_70/*shot*/(launchTime + 0.104352f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.104352 > Game.Music.Time) yield return Wait(launchTime + 0.104352f);
            Player.StartCoroutine(Pattern_70/*shot*/(launchTime + 0.104352f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.3903513 > Game.Music.Time) yield return Wait(launchTime + 0.3903513f);
            Player.StartCoroutine(Pattern_70/*shot*/(launchTime + 0.3903513f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.3903513 > Game.Music.Time) yield return Wait(launchTime + 0.3903513f);
            Player.StartCoroutine(Pattern_70/*shot*/(launchTime + 0.3903513f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_70(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x33/255f,0x13/255f,0x31/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1000444f));
            if (launchTime + 0.0006448329 > Game.Music.Time) yield return Wait(launchTime + 0.0006448329f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0006448329f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-131f*(float)flipx,78f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f+50f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x5f/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_71(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-200f,200f),146f+(float)rndf(-10f,10f)), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)rndf(-90f,90f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_72(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.2064352 > Game.Music.Time) yield return Wait(launchTime + 0.2064352f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 0.2064352f, blackboard/*out*/));
            if (launchTime + 0.3830776 > Game.Music.Time) yield return Wait(launchTime + 0.3830776f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 0.3830776f, blackboard/*out*/));
            if (launchTime + 0.7618971 > Game.Music.Time) yield return Wait(launchTime + 0.7618971f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 0.7618971f, blackboard/*out*/));
            if (launchTime + 1.000255 > Game.Music.Time) yield return Wait(launchTime + 1.000255f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 1.000255f, blackboard/*out*/));
            if (launchTime + 1.155614 > Game.Music.Time) yield return Wait(launchTime + 1.155614f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 1.155614f, blackboard/*out*/));
            if (launchTime + 1.553589 > Game.Music.Time) yield return Wait(launchTime + 1.553589f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 1.553589f, blackboard/*out*/));
            if (launchTime + 1.943051 > Game.Music.Time) yield return Wait(launchTime + 1.943051f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 1.943051f, blackboard/*out*/));
            if (launchTime + 2.532563 > Game.Music.Time) yield return Wait(launchTime + 2.532563f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 2.532563f, blackboard/*out*/));
            if (launchTime + 2.736868 > Game.Music.Time) yield return Wait(launchTime + 2.736868f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 2.736868f, blackboard/*out*/));
            if (launchTime + 3.132715 > Game.Music.Time) yield return Wait(launchTime + 3.132715f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 3.132715f, blackboard/*out*/));
            if (launchTime + 3.328512 > Game.Music.Time) yield return Wait(launchTime + 3.328512f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 3.328512f, blackboard/*out*/));
            if (launchTime + 3.526434 > Game.Music.Time) yield return Wait(launchTime + 3.526434f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 3.526434f, blackboard/*out*/));
            if (launchTime + 3.911639 > Game.Music.Time) yield return Wait(launchTime + 3.911639f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 3.911639f, blackboard/*out*/));
            if (launchTime + 4.094669 > Game.Music.Time) yield return Wait(launchTime + 4.094669f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 4.094669f, blackboard/*out*/));
            if (launchTime + 4.305361 > Game.Music.Time) yield return Wait(launchTime + 4.305361f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 4.305361f, blackboard/*out*/));
            if (launchTime + 4.69482 > Game.Music.Time) yield return Wait(launchTime + 4.69482f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 4.69482f, blackboard/*out*/));
            if (launchTime + 5.086409 > Game.Music.Time) yield return Wait(launchTime + 5.086409f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 5.086409f, blackboard/*out*/));
            if (launchTime + 5.680178 > Game.Music.Time) yield return Wait(launchTime + 5.680178f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 5.680178f, blackboard/*out*/));
            if (launchTime + 5.882355 > Game.Music.Time) yield return Wait(launchTime + 5.882355f);
            Player.StartCoroutine(Pattern_73/*Pattern*/(launchTime + 5.882355f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_73(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),240f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.5466862 > Game.Music.Time) yield return Wait(launchTime + 0.5466862f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.5466862f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d+3f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 15f+5f*(float)d, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x40/255f,0xff/255f,0x65/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.5481544 > Game.Music.Time) yield return Wait(launchTime + 0.5481544f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5481544f, blackboard, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x2d/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1383991f));
            if (launchTime + 0.5617666 > Game.Music.Time) yield return Wait(launchTime + 0.5617666f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.5617666f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_74(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(204f*(float)flipx,133f), (c,i,t,l, castc, casti) => (float)d*5f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 50f/(float)d*((float)i%((float)d+1f))-25f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+15f*(float)toint((float)i,((float)d+1f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xb4/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_75(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 153f/*out*/));
            if (launchTime + 6.277548 > Game.Music.Time) yield return Wait(launchTime + 6.277548f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 6.277548f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 12.57213 > Game.Music.Time) yield return Wait(launchTime + 12.57213f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 12.57213f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 15.50094 > Game.Music.Time) yield return Wait(launchTime + 15.50094f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 15.50094f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 18.85096 > Game.Music.Time) yield return Wait(launchTime + 18.85096f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 18.85096f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 21.99038 > Game.Music.Time) yield return Wait(launchTime + 21.99038f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 21.99038f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 25.13418 > Game.Music.Time) yield return Wait(launchTime + 25.13418f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 25.13418f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 31.37905 > Game.Music.Time) yield return Wait(launchTime + 31.37905f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 31.37905f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 37.65808 > Game.Music.Time) yield return Wait(launchTime + 37.65808f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 37.65808f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 50.23323 > Game.Music.Time) yield return Wait(launchTime + 50.23323f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 50.23323f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 63.16718 > Game.Music.Time) yield return Wait(launchTime + 63.16718f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 63.16718f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 64.32048 > Game.Music.Time) yield return Wait(launchTime + 64.32048f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 64.32048f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 76.88699 > Game.Music.Time) yield return Wait(launchTime + 76.88699f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 76.88699f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 83.16269 > Game.Music.Time) yield return Wait(launchTime + 83.16269f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 83.16269f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 89.43871 > Game.Music.Time) yield return Wait(launchTime + 89.43871f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 89.43871f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 102.6049 > Game.Music.Time) yield return Wait(launchTime + 102.6049f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 102.6049f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 306f/*out*/));
            if (launchTime + 103.5729 > Game.Music.Time) yield return Wait(launchTime + 103.5729f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 103.5729f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 153f/*out*/));
            if (launchTime + 116.1029 > Game.Music.Time) yield return Wait(launchTime + 116.1029f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 116.1029f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 129.3743 > Game.Music.Time) yield return Wait(launchTime + 129.3743f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 129.3743f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 306f/*out*/));
            if (launchTime + 130.2304 > Game.Music.Time) yield return Wait(launchTime + 130.2304f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 130.2304f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 153f/*out*/));
            if (launchTime + 136.5043 > Game.Music.Time) yield return Wait(launchTime + 136.5043f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 136.5043f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 142.7638 > Game.Music.Time) yield return Wait(launchTime + 142.7638f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 142.7638f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 149.0613 > Game.Music.Time) yield return Wait(launchTime + 149.0613f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 149.0613f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 155.3212 > Game.Music.Time) yield return Wait(launchTime + 155.3212f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 155.3212f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 161.5811 > Game.Music.Time) yield return Wait(launchTime + 161.5811f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 161.5811f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 167.876 > Game.Music.Time) yield return Wait(launchTime + 167.876f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 167.876f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 174.1419 > Game.Music.Time) yield return Wait(launchTime + 174.1419f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 174.1419f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 178.851 > Game.Music.Time) yield return Wait(launchTime + 178.851f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 178.851f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 180.4131 > Game.Music.Time) yield return Wait(launchTime + 180.4131f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 180.4131f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 183.5373 > Game.Music.Time) yield return Wait(launchTime + 183.5373f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 183.5373f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 186.6994 > Game.Music.Time) yield return Wait(launchTime + 186.6994f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 186.6994f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 189.8161 > Game.Music.Time) yield return Wait(launchTime + 189.8161f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 189.8161f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 192.971 > Game.Music.Time) yield return Wait(launchTime + 192.971f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 192.971f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_76(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc flipy_input, InputFunc flipvariants_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy7, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(-260f*(float)flipx,186f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.02302361 > Game.Music.Time) yield return Wait(launchTime + 0.02302361f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.02302361f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_4.output_enemy(ies)", 0.2264977f));
            if (launchTime + 0.5821343 > Game.Music.Time) yield return Wait(launchTime + 0.5821343f);
            Player.StartCoroutine(Pattern_77/*Shot*/(launchTime + 0.5821343f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.161743 > Game.Music.Time) yield return Wait(launchTime + 1.161743f);
            Player.StartCoroutine(Pattern_77/*Shot*/(launchTime + 1.161743f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.144848 > Game.Music.Time) yield return Wait(launchTime + 2.144848f);
            Player.StartCoroutine(Pattern_77/*Shot*/(launchTime + 2.144848f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.726871 > Game.Music.Time) yield return Wait(launchTime + 2.726871f);
            Player.StartCoroutine(Pattern_77/*Shot*/(launchTime + 2.726871f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.121923 > Game.Music.Time) yield return Wait(launchTime + 3.121923f);
            Player.StartCoroutine(Pattern_77/*Shot*/(launchTime + 3.121923f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.4169 > Game.Music.Time) yield return Wait(launchTime + 3.4169f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 3.4169f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-334f*(float)flipx,186f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_11.output_enemy(ies)", 0.2864685f));
            if (launchTime + 3.725826 > Game.Music.Time) yield return Wait(launchTime + 3.725826f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 3.725826f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_77(float launchTime, Blackboard outboard, InputFunc enemy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(0f,100f)*(float)flipx,(float)rndf(90f,120f)), (c,i,t,l, castc, casti) => new Vector2(0f,-100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x52/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_78(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc flipy_input, InputFunc flipvariants_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-310f*(float)flipx,74f*(float)flipy-50f), (c,i,t,l, castc, casti) => 5f*(float)d+10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((150f/(float)c*(float)i+250f)*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_79(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_80/*Ships*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_80/*Ships*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_80(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc flipy_input, InputFunc flipvariants_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy12, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(352f*(float)flipx,165f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.3963318 > Game.Music.Time) yield return Wait(launchTime + 0.3963318f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.3963318f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-372f,213f,-197f,55f,64f,-35f,359f,-44f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx*(float)ar,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 6.260483f));
            if (launchTime + 1.572372 > Game.Music.Time) yield return Wait(launchTime + 1.572372f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 1.572372f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 1.778915 > Game.Music.Time) yield return Wait(launchTime + 1.778915f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 1.778915f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 1.973144 > Game.Music.Time) yield return Wait(launchTime + 1.973144f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 1.973144f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 2.14946 > Game.Music.Time) yield return Wait(launchTime + 2.14946f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 2.14946f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 2.361679 > Game.Music.Time) yield return Wait(launchTime + 2.361679f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 2.361679f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 2.548752 > Game.Music.Time) yield return Wait(launchTime + 2.548752f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 2.548752f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 2.7574 > Game.Music.Time) yield return Wait(launchTime + 2.7574f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 2.7574f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 3.15628 > Game.Music.Time) yield return Wait(launchTime + 3.15628f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 3.15628f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 3.354141 > Game.Music.Time) yield return Wait(launchTime + 3.354141f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 3.354141f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 3.540085 > Game.Music.Time) yield return Wait(launchTime + 3.540085f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 3.540085f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 3.726089 > Game.Music.Time) yield return Wait(launchTime + 3.726089f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 3.726089f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 3.935775 > Game.Music.Time) yield return Wait(launchTime + 3.935775f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 3.935775f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 4.125686 > Game.Music.Time) yield return Wait(launchTime + 4.125686f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 4.125686f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 4.315612 > Game.Music.Time) yield return Wait(launchTime + 4.315612f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 4.315612f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 4.714554 > Game.Music.Time) yield return Wait(launchTime + 4.714554f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 4.714554f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 5.098892 > Game.Music.Time) yield return Wait(launchTime + 5.098892f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 5.098892f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 5.296783 > Game.Music.Time) yield return Wait(launchTime + 5.296783f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 5.296783f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 5.498565 > Game.Music.Time) yield return Wait(launchTime + 5.498565f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 5.498565f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 5.680587 > Game.Music.Time) yield return Wait(launchTime + 5.680587f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 5.680587f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 5.88237 > Game.Music.Time) yield return Wait(launchTime + 5.88237f);
            Player.StartCoroutine(Pattern_81/*Shot*/(launchTime + 5.88237f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)"/*out*/));
            if (launchTime + 6.818542 > Game.Music.Time) yield return Wait(launchTime + 6.818542f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 6.818542f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_81(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

    }
}