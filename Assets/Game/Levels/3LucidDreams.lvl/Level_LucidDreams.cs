#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_LucidDreams : LevelScript
    {
        protected override string ClipName {
            get { return "LucidDreams.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0.6190819 > Game.Music.Time) yield return Wait(launchTime + 0.6190819f);
            Player.StartCoroutine(Pattern_3/*Part1_1*/(launchTime + 0.6190819f, blackboard/*out*/));
            if (launchTime + 0.9157528 > Game.Music.Time) yield return Wait(launchTime + 0.9157528f);
            Player.StartCoroutine(Pattern_68/*DanceMan*/(launchTime + 0.9157528f, blackboard/*out*/));
            if (launchTime + 14.89067 > Game.Music.Time) yield return Wait(launchTime + 14.89067f);
            Player.StartCoroutine(Pattern_6/*Part1_2*/(launchTime + 14.89067f, blackboard/*out*/));
            if (launchTime + 29.83679 > Game.Music.Time) yield return Wait(launchTime + 29.83679f);
            Player.StartCoroutine(Pattern_42/*Part1_3*/(launchTime + 29.83679f, blackboard/*out*/));
            if (launchTime + 44.14281 > Game.Music.Time) yield return Wait(launchTime + 44.14281f);
            Player.StartCoroutine(Pattern_52/*Part1_4*/(launchTime + 44.14281f, blackboard/*out*/));
            if (launchTime + 44.53287 > Game.Music.Time) yield return Wait(launchTime + 44.53287f);
            Player.StartCoroutine(Pattern_51/*Bum*/(launchTime + 44.53287f, blackboard/*out*/));
            if (launchTime + 59.08595 > Game.Music.Time) yield return Wait(launchTime + 59.08595f);
            Player.StartCoroutine(Pattern_71/*Part1_5a*/(launchTime + 59.08595f, blackboard/*out*/));
            if (launchTime + 73.61798 > Game.Music.Time) yield return Wait(launchTime + 73.61798f);
            Player.StartCoroutine(Pattern_1/*AppearCircles*/(launchTime + 73.61798f, blackboard/*out*/));
            if (launchTime + 73.62847 > Game.Music.Time) yield return Wait(launchTime + 73.62847f);
            Player.StartCoroutine(Pattern_7/*Part1_5*/(launchTime + 73.62847f, blackboard/*out*/));
            if (launchTime + 86.17161 > Game.Music.Time) yield return Wait(launchTime + 86.17161f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 86.17161f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x77/255f,0xb8/255f)/*out*/, 1.964035f));
            if (launchTime + 88.17638 > Game.Music.Time) yield return Wait(launchTime + 88.17638f);
            Player.StartCoroutine(Pattern_11/*Part2*/(launchTime + 88.17638f, blackboard/*out*/));
            if (launchTime + 88.1785 > Game.Music.Time) yield return Wait(launchTime + 88.1785f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 88.1785f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 1.100243f));
            if (launchTime + 88.18488 > Game.Music.Time) yield return Wait(launchTime + 88.18488f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 88.18488f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.1192322f));
            if (launchTime + 116.2827 > Game.Music.Time) yield return Wait(launchTime + 116.2827f);
            Player.StartCoroutine(Pattern_19/*Part3*/(launchTime + 116.2827f, blackboard/*out*/));
            if (launchTime + 146.3685 > Game.Music.Time) yield return Wait(launchTime + 146.3685f);
            Player.StartCoroutine(Pattern_28/*Part4*/(launchTime + 146.3685f, blackboard/*out*/));
            if (launchTime + 146.3951 > Game.Music.Time) yield return Wait(launchTime + 146.3951f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 146.3951f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4855042f));
            if (launchTime + 204.527 > Game.Music.Time) yield return Wait(launchTime + 204.527f);
            Player.StartCoroutine(Pattern_29/*Part5*/(launchTime + 204.527f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object variable1;
            blackboard.SubscribeForChanges("node_3.output_value", (b, o) => variable1 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_output", (c,i,t,l, castc, casti) => "node_2.output_output", (c,i,t,l, castc, casti) => "node_4.output_output", (c,i,t,l, castc, casti) => "node_5.output_output", (c,i,t,l, castc, casti) => "node_6.output_output", (c,i,t,l, castc, casti) => "node_7.output_output", (c,i,t,l, castc, casti) => "node_8.output_output", (c,i,t,l, castc, casti) => "node_9.output_output"/*out*/, "node_3.output_value");
            object bullets;
            blackboard.SubscribeForChanges("node_10.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_3.output_value", (c,i,t,l, castc, casti) => "node_15.output_value", (c,i,t,l, castc, casti) => "node_25.output_value", (c,i,t,l, castc, casti) => "node_29.output_output", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_value");
            object variable2;
            blackboard.SubscribeForChanges("node_15.output_value", (b, o) => variable2 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_11.output_output", (c,i,t,l, castc, casti) => "node_12.output_output", (c,i,t,l, castc, casti) => "node_13.output_output", (c,i,t,l, castc, casti) => "node_14.output_output", (c,i,t,l, castc, casti) => "node_16.output_output", (c,i,t,l, castc, casti) => "node_17.output_output", (c,i,t,l, castc, casti) => "node_18.output_output", (c,i,t,l, castc, casti) => "node_19.output_output"/*out*/, "node_15.output_value");
            object variable3;
            blackboard.SubscribeForChanges("node_25.output_value", (b, o) => variable3 = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_20.output_output", (c,i,t,l, castc, casti) => "node_21.output_output", (c,i,t,l, castc, casti) => "node_22.output_output", (c,i,t,l, castc, casti) => "node_23.output_output", (c,i,t,l, castc, casti) => "node_24.output_output", (c,i,t,l, castc, casti) => "node_26.output_output", (c,i,t,l, castc, casti) => "node_27.output_output", (c,i,t,l, castc, casti) => "node_28.output_output"/*out*/, "node_25.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 0f, blackboard/*out*/, "node_0.output_output"));
            if (launchTime + 0.9200134 > Game.Music.Time) yield return Wait(launchTime + 0.9200134f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 0.9200134f, blackboard/*out*/, "node_2.output_output"));
            if (launchTime + 1.82151 > Game.Music.Time) yield return Wait(launchTime + 1.82151f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 1.82151f, blackboard/*out*/, "node_4.output_output"));
            if (launchTime + 2.728523 > Game.Music.Time) yield return Wait(launchTime + 2.728523f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 2.728523f, blackboard/*out*/, "node_5.output_output"));
            if (launchTime + 3.657677 > Game.Music.Time) yield return Wait(launchTime + 3.657677f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 3.657677f, blackboard/*out*/, "node_6.output_output"));
            if (launchTime + 4.548111 > Game.Music.Time) yield return Wait(launchTime + 4.548111f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 4.548111f, blackboard/*out*/, "node_7.output_output"));
            if (launchTime + 5.460655 > Game.Music.Time) yield return Wait(launchTime + 5.460655f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 5.460655f, blackboard/*out*/, "node_8.output_output"));
            if (launchTime + 6.356606 > Game.Music.Time) yield return Wait(launchTime + 6.356606f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 6.356606f, blackboard/*out*/, "node_9.output_output"));
            if (launchTime + 7.279381 > Game.Music.Time) yield return Wait(launchTime + 7.279381f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 7.279381f, blackboard/*out*/, "node_11.output_output"));
            if (launchTime + 7.717125 > Game.Music.Time) yield return Wait(launchTime + 7.717125f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 7.717125f, blackboard/*out*/, "node_12.output_output"));
            if (launchTime + 8.181702 > Game.Music.Time) yield return Wait(launchTime + 8.181702f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 8.181702f, blackboard/*out*/, "node_13.output_output"));
            if (launchTime + 8.632889 > Game.Music.Time) yield return Wait(launchTime + 8.632889f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 8.632889f, blackboard/*out*/, "node_14.output_output"));
            if (launchTime + 9.09597 > Game.Music.Time) yield return Wait(launchTime + 9.09597f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 9.09597f, blackboard/*out*/, "node_16.output_output"));
            if (launchTime + 9.546364 > Game.Music.Time) yield return Wait(launchTime + 9.546364f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 9.546364f, blackboard/*out*/, "node_17.output_output"));
            if (launchTime + 9.996201 > Game.Music.Time) yield return Wait(launchTime + 9.996201f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 9.996201f, blackboard/*out*/, "node_18.output_output"));
            if (launchTime + 10.45293 > Game.Music.Time) yield return Wait(launchTime + 10.45293f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 10.45293f, blackboard/*out*/, "node_19.output_output"));
            if (launchTime + 10.68302 > Game.Music.Time) yield return Wait(launchTime + 10.68302f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 10.68302f, blackboard/*out*/, "node_20.output_output"));
            if (launchTime + 10.90967 > Game.Music.Time) yield return Wait(launchTime + 10.90967f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 10.90967f, blackboard/*out*/, "node_21.output_output"));
            if (launchTime + 11.12945 > Game.Music.Time) yield return Wait(launchTime + 11.12945f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 11.12945f, blackboard/*out*/, "node_22.output_output"));
            if (launchTime + 11.35364 > Game.Music.Time) yield return Wait(launchTime + 11.35364f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 11.35364f, blackboard/*out*/, "node_23.output_output"));
            if (launchTime + 11.57931 > Game.Music.Time) yield return Wait(launchTime + 11.57931f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 11.57931f, blackboard/*out*/, "node_24.output_output"));
            if (launchTime + 11.81627 > Game.Music.Time) yield return Wait(launchTime + 11.81627f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 11.81627f, blackboard/*out*/, "node_26.output_output"));
            if (launchTime + 12.04291 > Game.Music.Time) yield return Wait(launchTime + 12.04291f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 12.04291f, blackboard/*out*/, "node_27.output_output"));
            if (launchTime + 12.27643 > Game.Music.Time) yield return Wait(launchTime + 12.27643f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 12.27643f, blackboard/*out*/, "node_28.output_output"));
            if (launchTime + 12.49621 > Game.Music.Time) yield return Wait(launchTime + 12.49621f);
            Player.StartCoroutine(Pattern_2/*CreateBullet*/(launchTime + 12.49621f, blackboard/*out*/, "node_29.output_output"));
            if (launchTime + 14.56804 > Game.Music.Time) yield return Wait(launchTime + 14.56804f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 14.56804f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_30.output_bullet(s)"));
            if (launchTime + 14.57558 > Game.Music.Time) yield return Wait(launchTime + 14.57558f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 14.57558f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => (float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x75/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_31.output_bullet(s)"));
            if (launchTime + 14.58083 > Game.Music.Time) yield return Wait(launchTime + 14.58083f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 14.58083f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard, string output_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, output_output, "node_3.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-270f,270f)*(float)ar,(float)rndf(100f,220f)), (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 0.01f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.01129913 > Game.Music.Time) yield return Wait(launchTime + 0.01129913f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.01129913f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x17/255f,0x17/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1908646f));
            if (launchTime + 0.0116272 > Game.Music.Time) yield return Wait(launchTime + 0.0116272f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0116272f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.02033996 > Game.Music.Time) yield return Wait(launchTime + 0.02033996f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.02033996f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_35/*Melody*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.2817533 > Game.Music.Time) yield return Wait(launchTime + 0.2817533f);
            Player.StartCoroutine(Pattern_37/*Accords*/(launchTime + 0.2817533f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard, InputFunc enemytype_input, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemytype_input, "node_2.output_patterninput");
            object enemytype = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemytype = o);

            SubscribeOutput(outboard, blackboard, enemies_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => 1300f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-1f,303f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.2159424 > Game.Music.Time) yield return Wait(launchTime + 0.2159424f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.2159424f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((70f*((float)i+0.5f)-240f)*(float)ar,170f+((float)i%2f)*30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_1.output_enemy(ies)", 0.7563629f));
            if (launchTime + 15.52806 > Game.Music.Time) yield return Wait(launchTime + 15.52806f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 15.52806f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-1f,326f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_5.output_enemy(ies)", 1.621277f));
            if (launchTime + 17.19397 > Game.Music.Time) yield return Wait(launchTime + 17.19397f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 17.19397f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_2.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.0962677 > Game.Music.Time) yield return Wait(launchTime + 0.0962677f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0962677f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+(float)rndf(-45f,45f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xa7/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*CreateCircle*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 0.2745392 > Game.Music.Time) yield return Wait(launchTime + 0.2745392f);
            Player.StartCoroutine(Pattern_41/*Melody*/(launchTime + 0.2745392f, blackboard/*out*/));
            if (launchTime + 0.5203257 > Game.Music.Time) yield return Wait(launchTime + 0.5203257f);
            Player.StartCoroutine(Pattern_8/*Shoots*/(launchTime + 0.5203257f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_57/*Homing*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bullets"));
            if (launchTime + 0.007164001 > Game.Music.Time) yield return Wait(launchTime + 0.007164001f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.007164001f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.6930695f));
            if (launchTime + 0.01345062 > Game.Music.Time) yield return Wait(launchTime + 0.01345062f);
            Player.StartCoroutine(Pattern_62/*Melody2*/(launchTime + 0.01345062f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullets"/*out*/));
            if (launchTime + 7.296173 > Game.Music.Time) yield return Wait(launchTime + 7.296173f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 7.296173f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.6930695f));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.827513 > Game.Music.Time) yield return Wait(launchTime + 1.827513f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 1.827513f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.643904 > Game.Music.Time) yield return Wait(launchTime + 3.643904f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 3.643904f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.460293 > Game.Music.Time) yield return Wait(launchTime + 5.460293f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 5.460293f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.289784 > Game.Music.Time) yield return Wait(launchTime + 7.289784f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 7.289784f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.102772 > Game.Music.Time) yield return Wait(launchTime + 9.102772f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 9.102772f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.93636 > Game.Music.Time) yield return Wait(launchTime + 10.93636f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 10.93636f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.73907 > Game.Music.Time) yield return Wait(launchTime + 12.73907f);
            Player.StartCoroutine(Pattern_10/*Shot6*/(launchTime + 12.73907f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d/*out*/, "node_1.output_elements"));
            if (launchTime + 0.02124119 > Game.Music.Time) yield return Wait(launchTime + 0.02124119f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.02124119f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => (float)d/2f+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)rndf(-7f,7f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(75f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.03686142 > Game.Music.Time) yield return Wait(launchTime + 0.03686142f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.03686142f, blackboard, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x24/255f,0x15/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1784868f));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3441972 > Game.Music.Time) yield return Wait(launchTime + 0.3441972f);
            Player.StartCoroutine(Pattern_9/*Shoot*/(launchTime + 0.3441972f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6843482 > Game.Music.Time) yield return Wait(launchTime + 0.6843482f);
            Player.StartCoroutine(Pattern_9/*Shoot*/(launchTime + 0.6843482f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.024497 > Game.Music.Time) yield return Wait(launchTime + 1.024497f);
            Player.StartCoroutine(Pattern_9/*Shoot*/(launchTime + 1.024497f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.372736 > Game.Music.Time) yield return Wait(launchTime + 1.372736f);
            Player.StartCoroutine(Pattern_9/*Shoot*/(launchTime + 1.372736f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.60355 > Game.Music.Time) yield return Wait(launchTime + 1.60355f);
            Player.StartCoroutine(Pattern_9/*Shoot*/(launchTime + 1.60355f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_30/*ShotPattern1*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 3.431098 > Game.Music.Time) yield return Wait(launchTime + 3.431098f);
            Player.StartCoroutine(Pattern_31/*ShotPattern2*/(launchTime + 3.431098f, blackboard, (c,i,t,l, castc, casti) => new Color(0x10/255f,0x70/255f,0xff/255f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark/*out*/));
            if (launchTime + 7.287337 > Game.Music.Time) yield return Wait(launchTime + 7.287337f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 7.287337f, blackboard, (c,i,t,l, castc, casti) => new MyPath(280f,180f,-340f,180f)/*out*/));
            if (launchTime + 10.70796 > Game.Music.Time) yield return Wait(launchTime + 10.70796f);
            Player.StartCoroutine(Pattern_31/*ShotPattern2*/(launchTime + 10.70796f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6c/255f,0x19/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Corner/*out*/));
            if (launchTime + 14.54838 > Game.Music.Time) yield return Wait(launchTime + 14.54838f);
            Player.StartCoroutine(Pattern_30/*ShotPattern1*/(launchTime + 14.54838f, blackboard, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0xff/255f)/*out*/));
            if (launchTime + 17.97706 > Game.Music.Time) yield return Wait(launchTime + 17.97706f);
            Player.StartCoroutine(Pattern_31/*ShotPattern2*/(launchTime + 17.97706f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4d/255f,0xff/255f,0x88/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Torus/*out*/));
            if (launchTime + 21.82641 > Game.Music.Time) yield return Wait(launchTime + 21.82641f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 21.82641f, blackboard, (c,i,t,l, castc, casti) => new MyPath(-280f,180f,340f,180f)/*out*/));
            if (launchTime + 25.2581 > Game.Music.Time) yield return Wait(launchTime + 25.2581f);
            Player.StartCoroutine(Pattern_33/*ShotPatternFinal*/(launchTime + 25.2581f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => EnemyImage.Enemy8, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-(float)sin(180f/(float)c*((float)i+0.5f)-90f)*300f*(float)ar,(float)cos(180f/(float)c*((float)i+0.5f)-90f)*220f), (c,i,t,l, castc, casti) => 180f/(float)c*((float)i+0.5f)-90f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.2053146 > Game.Music.Time) yield return Wait(launchTime + 0.2053146f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.2053146f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_2.output_enemy(ies)", 0.8950729f));
            if (launchTime + 0.2114944 > Game.Music.Time) yield return Wait(launchTime + 0.2114944f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.2114944f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x38/255f,0x47/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8759918f));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3630524 > Game.Music.Time) yield return Wait(launchTime + 0.3630524f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 0.3630524f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6947937 > Game.Music.Time) yield return Wait(launchTime + 0.6947937f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 0.6947937f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.913147 > Game.Music.Time) yield return Wait(launchTime + 0.913147f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 0.913147f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.219681 > Game.Music.Time) yield return Wait(launchTime + 1.219681f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 1.219681f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.58075 > Game.Music.Time) yield return Wait(launchTime + 1.58075f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 1.58075f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.687737 > Game.Music.Time) yield return Wait(launchTime + 1.687737f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.687737f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x38/255f,0x1d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2509232f));
            if (launchTime + 0.005722046 > Game.Music.Time) yield return Wait(launchTime + 0.005722046f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0.005722046f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_2.output_elements"));
            if (launchTime + 0.006698609 > Game.Music.Time) yield return Wait(launchTime + 0.006698609f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.006698609f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.007751465 > Game.Music.Time) yield return Wait(launchTime + 0.007751465f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 0.007751465f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard, InputFunc type_input, InputFunc color_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, type_input, "node_0.output_patterninput");
            object type = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => type = o);

            SubscribeInput(outboard, blackboard, color_input, "node_1.output_patterninput");
            object color = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_2.output_patterninput");
            object enemies = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x24/255f,0x24/255f,0x24/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1553955f));
            if (launchTime + 0.009552004 > Game.Music.Time) yield return Wait(launchTime + 0.009552004f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0.009552004f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => (float)d+2f/*out*/, "node_4.output_elements"));
            if (launchTime + 0.0133667 > Game.Music.Time) yield return Wait(launchTime + 0.0133667f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0133667f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_elements", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)rndf(-20f,20f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc enemies_input, InputFunc type_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_1.output_patterninput");
            object enemies = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemies = o);

            SubscribeInput(outboard, blackboard, type_input, "node_2.output_patterninput");
            object type = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => type = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => (float)d+2f/*out*/, "node_3.output_elements"));
            if (launchTime + 0.01277924 > Game.Music.Time) yield return Wait(launchTime + 0.01277924f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.01277924f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_elements", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+(float)rndf(-30f,30f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => -100f, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.01556397 > Game.Music.Time) yield return Wait(launchTime + 0.01556397f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.01556397f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3c/255f,0x38/255f,0x3c/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1321106f));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc type_input, InputFunc color_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, type_input, "node_0.output_patterninput");
            object type = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => type = o);

            SubscribeInput(outboard, blackboard, color_input, "node_1.output_patterninput");
            object color = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_2.output_patterninput");
            object enemies = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.2745513 > Game.Music.Time) yield return Wait(launchTime + 0.2745513f);
            Player.StartCoroutine(Pattern_15/*Shoot*/(launchTime + 0.2745513f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.501114 > Game.Music.Time) yield return Wait(launchTime + 0.501114f);
            Player.StartCoroutine(Pattern_15/*Shoot*/(launchTime + 0.501114f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.7170258 > Game.Music.Time) yield return Wait(launchTime + 0.7170258f);
            Player.StartCoroutine(Pattern_15/*Shoot*/(launchTime + 0.7170258f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc type_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, type_input, "node_1.output_patterninput");
            object type = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => type = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_2.output_patterninput");
            object enemies = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_16/*Shoot2*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 0.2629776 > Game.Music.Time) yield return Wait(launchTime + 0.2629776f);
            Player.StartCoroutine(Pattern_16/*Shoot2*/(launchTime + 0.2629776f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 0.4850388 > Game.Music.Time) yield return Wait(launchTime + 0.4850388f);
            Player.StartCoroutine(Pattern_16/*Shoot2*/(launchTime + 0.4850388f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 0.7158738 > Game.Music.Time) yield return Wait(launchTime + 0.7158738f);
            Player.StartCoroutine(Pattern_16/*Shoot2*/(launchTime + 0.7158738f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_20/*Piano*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.9762878 > Game.Music.Time) yield return Wait(launchTime + 0.9762878f);
            Player.StartCoroutine(Pattern_22/*Explosions*/(launchTime + 0.9762878f, blackboard/*out*/));
            if (launchTime + 15.51703 > Game.Music.Time) yield return Wait(launchTime + 15.51703f);
            Player.StartCoroutine(Pattern_26/*FallingStars*/(launchTime + 15.51703f, blackboard/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_74/*NewPattern*/(launchTime + 3.402823E+38f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_4/*CreatePiano*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_0.output_enemies"));
            if (launchTime + 0.8893356 > Game.Music.Time) yield return Wait(launchTime + 0.8893356f);
            Player.StartCoroutine(Pattern_21/*ShootMelody*/(launchTime + 0.8893356f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2284241 > Game.Music.Time) yield return Wait(launchTime + 0.2284241f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0.2284241f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4694214 > Game.Music.Time) yield return Wait(launchTime + 0.4694214f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0.4694214f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6844788 > Game.Music.Time) yield return Wait(launchTime + 0.6844788f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0.6844788f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.814224 > Game.Music.Time) yield return Wait(launchTime + 1.814224f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 1.814224f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.042252 > Game.Music.Time) yield return Wait(launchTime + 2.042252f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 2.042252f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.267685 > Game.Music.Time) yield return Wait(launchTime + 2.267685f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 2.267685f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.500885 > Game.Music.Time) yield return Wait(launchTime + 2.500885f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 2.500885f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.723725 > Game.Music.Time) yield return Wait(launchTime + 2.723725f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 2.723725f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.197922 > Game.Music.Time) yield return Wait(launchTime + 3.197922f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.197922f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.425949 > Game.Music.Time) yield return Wait(launchTime + 3.425949f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.425949f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.628067 > Game.Music.Time) yield return Wait(launchTime + 3.628067f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.628067f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.876801 > Game.Music.Time) yield return Wait(launchTime + 3.876801f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.876801f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.089279 > Game.Music.Time) yield return Wait(launchTime + 4.089279f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 4.089279f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.325089 > Game.Music.Time) yield return Wait(launchTime + 4.325089f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 4.325089f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.550537 > Game.Music.Time) yield return Wait(launchTime + 4.550537f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 4.550537f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.462646 > Game.Music.Time) yield return Wait(launchTime + 5.462646f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.462646f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.680313 > Game.Music.Time) yield return Wait(launchTime + 5.680313f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.680313f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.910919 > Game.Music.Time) yield return Wait(launchTime + 5.910919f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.910919f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.128571 > Game.Music.Time) yield return Wait(launchTime + 6.128571f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 6.128571f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.361785 > Game.Music.Time) yield return Wait(launchTime + 6.361785f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 6.361785f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.823029 > Game.Music.Time) yield return Wait(launchTime + 6.823029f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 6.823029f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.045853 > Game.Music.Time) yield return Wait(launchTime + 7.045853f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.045853f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.284241 > Game.Music.Time) yield return Wait(launchTime + 7.284241f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.284241f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.504502 > Game.Music.Time) yield return Wait(launchTime + 7.504502f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.504502f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.727341 > Game.Music.Time) yield return Wait(launchTime + 7.727341f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.727341f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.95018 > Game.Music.Time) yield return Wait(launchTime + 7.95018f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.95018f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.090286 > Game.Music.Time) yield return Wait(launchTime + 9.090286f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 9.090286f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.318314 > Game.Music.Time) yield return Wait(launchTime + 9.318314f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 9.318314f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.543747 > Game.Music.Time) yield return Wait(launchTime + 9.543747f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 9.543747f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.766587 > Game.Music.Time) yield return Wait(launchTime + 9.766587f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 9.766587f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.00757 > Game.Music.Time) yield return Wait(launchTime + 10.00757f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.00757f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.46102 > Game.Music.Time) yield return Wait(launchTime + 10.46102f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.46102f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.68647 > Game.Music.Time) yield return Wait(launchTime + 10.68647f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.68647f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.91445 > Game.Music.Time) yield return Wait(launchTime + 10.91445f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.91445f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.1373 > Game.Music.Time) yield return Wait(launchTime + 11.1373f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 11.1373f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.3705 > Game.Music.Time) yield return Wait(launchTime + 11.3705f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 11.3705f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.5778 > Game.Music.Time) yield return Wait(launchTime + 11.5778f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 11.5778f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.27483 > Game.Music.Time) yield return Wait(launchTime + 12.27483f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.27483f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.51063 > Game.Music.Time) yield return Wait(launchTime + 12.51063f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.51063f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.72829 > Game.Music.Time) yield return Wait(launchTime + 12.72829f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.72829f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.95891 > Game.Music.Time) yield return Wait(launchTime + 12.95891f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.95891f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.18174 > Game.Music.Time) yield return Wait(launchTime + 13.18174f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 13.18174f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.63779 > Game.Music.Time) yield return Wait(launchTime + 13.63779f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 13.63779f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.08865 > Game.Music.Time) yield return Wait(launchTime + 14.08865f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 14.08865f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.32704 > Game.Music.Time) yield return Wait(launchTime + 14.32704f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 14.32704f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.798676 > Game.Music.Time) yield return Wait(launchTime + 1.798676f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 1.798676f, blackboard/*out*/));
            if (launchTime + 3.633362 > Game.Music.Time) yield return Wait(launchTime + 3.633362f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 3.633362f, blackboard/*out*/));
            if (launchTime + 5.452164 > Game.Music.Time) yield return Wait(launchTime + 5.452164f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 5.452164f, blackboard/*out*/));
            if (launchTime + 7.262451 > Game.Music.Time) yield return Wait(launchTime + 7.262451f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 7.262451f, blackboard/*out*/));
            if (launchTime + 9.065903 > Game.Music.Time) yield return Wait(launchTime + 9.065903f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 9.065903f, blackboard/*out*/));
            if (launchTime + 10.88141 > Game.Music.Time) yield return Wait(launchTime + 10.88141f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 10.88141f, blackboard/*out*/));
            if (launchTime + 12.73127 > Game.Music.Time) yield return Wait(launchTime + 12.73127f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 12.73127f, blackboard/*out*/));
            if (launchTime + 14.55372 > Game.Music.Time) yield return Wait(launchTime + 14.55372f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 14.55372f, blackboard/*out*/));
            if (launchTime + 16.37616 > Game.Music.Time) yield return Wait(launchTime + 16.37616f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 16.37616f, blackboard/*out*/));
            if (launchTime + 18.18483 > Game.Music.Time) yield return Wait(launchTime + 18.18483f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 18.18483f, blackboard/*out*/));
            if (launchTime + 19.98658 > Game.Music.Time) yield return Wait(launchTime + 19.98658f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 19.98658f, blackboard/*out*/));
            if (launchTime + 21.80208 > Game.Music.Time) yield return Wait(launchTime + 21.80208f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 21.80208f, blackboard/*out*/));
            if (launchTime + 23.6727 > Game.Music.Time) yield return Wait(launchTime + 23.6727f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 23.6727f, blackboard/*out*/));
            if (launchTime + 25.45382 > Game.Music.Time) yield return Wait(launchTime + 25.45382f);
            Player.StartCoroutine(Pattern_23/*Explosion*/(launchTime + 25.45382f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_24/*Appear*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bullet"));
            if (launchTime + 0.9104386 > Game.Music.Time) yield return Wait(launchTime + 0.9104386f);
            Player.StartCoroutine(Pattern_25/*Explode*/(launchTime + 0.9104386f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard, string bullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullet_output, "node_2.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x38/255f,0x1d/255f,0x1b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2206879f));
            if (launchTime + 0.008918761 > Game.Music.Time) yield return Wait(launchTime + 0.008918761f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.008918761f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => new Vector2(0f+(float)rndf(-300f,300f)*(float)ar,100f+(float)rndf(-40f,40f)), (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard, InputFunc bullet_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullet_input, "node_0.output_patterninput");
            object bullet = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullet = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x75/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.01218414 > Game.Music.Time) yield return Wait(launchTime + 0.01218414f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.01218414f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x3a/255f,0x17/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4440613f));
            if (launchTime + 0.02610016 > Game.Music.Time) yield return Wait(launchTime + 0.02610016f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.02610016f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.853012 > Game.Music.Time) yield return Wait(launchTime + 1.853012f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 1.853012f, blackboard/*out*/));
            if (launchTime + 2.746917 > Game.Music.Time) yield return Wait(launchTime + 2.746917f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 2.746917f, blackboard/*out*/));
            if (launchTime + 3.648239 > Game.Music.Time) yield return Wait(launchTime + 3.648239f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 3.648239f, blackboard/*out*/));
            if (launchTime + 5.473206 > Game.Music.Time) yield return Wait(launchTime + 5.473206f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 5.473206f, blackboard/*out*/));
            if (launchTime + 6.411805 > Game.Music.Time) yield return Wait(launchTime + 6.411805f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 6.411805f, blackboard/*out*/));
            if (launchTime + 7.313126 > Game.Music.Time) yield return Wait(launchTime + 7.313126f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 7.313126f, blackboard/*out*/));
            if (launchTime + 9.115784 > Game.Music.Time) yield return Wait(launchTime + 9.115784f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 9.115784f, blackboard/*out*/));
            if (launchTime + 10.00965 > Game.Music.Time) yield return Wait(launchTime + 10.00965f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 10.00965f, blackboard/*out*/));
            if (launchTime + 10.93336 > Game.Music.Time) yield return Wait(launchTime + 10.93336f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 10.93336f, blackboard/*out*/));
            if (launchTime + 12.72112 > Game.Music.Time) yield return Wait(launchTime + 12.72112f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 12.72112f, blackboard/*out*/));
            if (launchTime + 13.68203 > Game.Music.Time) yield return Wait(launchTime + 13.68203f);
            Player.StartCoroutine(Pattern_27/*Star*/(launchTime + 13.68203f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,0f)*(float)ar,230f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(100f,-100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.001953125 > Game.Music.Time) yield return Wait(launchTime + 0.001953125f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.001953125f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => 0.4f/(float)d-0.05f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(50f,100f), (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => new Color(0x00/255f,0x86/255f,0xad/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)", 4.731964f));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_30/*ShotPattern1*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => ParticleBulletType.Triangle, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x77/255f)/*out*/));
            if (launchTime + 3.423523 > Game.Music.Time) yield return Wait(launchTime + 3.423523f);
            Player.StartCoroutine(Pattern_31/*ShotPattern2*/(launchTime + 3.423523f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6c/255f,0x6a/255f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight/*out*/));
            if (launchTime + 7.282425 > Game.Music.Time) yield return Wait(launchTime + 7.282425f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 7.282425f, blackboard, (c,i,t,l, castc, casti) => new MyPath(280f,180f,-340f,180f)/*out*/));
            if (launchTime + 10.90475 > Game.Music.Time) yield return Wait(launchTime + 10.90475f);
            Player.StartCoroutine(Pattern_69/*ShotPattern5*/(launchTime + 10.90475f, blackboard/*out*/));
            if (launchTime + 14.53648 > Game.Music.Time) yield return Wait(launchTime + 14.53648f);
            Player.StartCoroutine(Pattern_30/*ShotPattern1*/(launchTime + 14.53648f, blackboard, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => new Color(0x05/255f,0x0c/255f,0x05/255f)/*out*/));
            if (launchTime + 17.93151 > Game.Music.Time) yield return Wait(launchTime + 17.93151f);
            Player.StartCoroutine(Pattern_75/*MovePattern1*/(launchTime + 17.93151f, blackboard/*out*/));
            if (launchTime + 21.82592 > Game.Music.Time) yield return Wait(launchTime + 21.82592f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 21.82592f, blackboard, (c,i,t,l, castc, casti) => new MyPath(-280f,180f,340f,180f)/*out*/));
            if (launchTime + 25.45452 > Game.Music.Time) yield return Wait(launchTime + 25.45452f);
            Player.StartCoroutine(Pattern_69/*ShotPattern5*/(launchTime + 25.45452f, blackboard/*out*/));
            if (launchTime + 28.86465 > Game.Music.Time) yield return Wait(launchTime + 28.86465f);
            Player.StartCoroutine(Pattern_75/*MovePattern1*/(launchTime + 28.86465f, blackboard/*out*/));
            if (launchTime + 32.51965 > Game.Music.Time) yield return Wait(launchTime + 32.51965f);
            Player.StartCoroutine(Pattern_31/*ShotPattern2*/(launchTime + 32.51965f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x17/255f,0x6c/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 36.36586 > Game.Music.Time) yield return Wait(launchTime + 36.36586f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 36.36586f, blackboard, (c,i,t,l, castc, casti) => new MyPath(-281f,203f,341f,-254f)/*out*/));
            if (launchTime + 40.00705 > Game.Music.Time) yield return Wait(launchTime + 40.00705f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 40.00705f, blackboard, (c,i,t,l, castc, casti) => new MyPath(289f,204f,-342f,-254f)/*out*/));
            if (launchTime + 43.63363 > Game.Music.Time) yield return Wait(launchTime + 43.63363f);
            Player.StartCoroutine(Pattern_30/*ShotPattern1*/(launchTime + 43.63363f, blackboard, (c,i,t,l, castc, casti) => ParticleBulletType.Torus, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 47.02855 > Game.Music.Time) yield return Wait(launchTime + 47.02855f);
            Player.StartCoroutine(Pattern_75/*MovePattern1*/(launchTime + 47.02855f, blackboard/*out*/));
            if (launchTime + 50.9063 > Game.Music.Time) yield return Wait(launchTime + 50.9063f);
            Player.StartCoroutine(Pattern_32/*ShotPattern3*/(launchTime + 50.9063f, blackboard, (c,i,t,l, castc, casti) => new MyPath(-283f,204f,-188f,150f,-6f,110f,187f,149f,335f,248f)/*out*/));
            if (launchTime + 54.54126 > Game.Music.Time) yield return Wait(launchTime + 54.54126f);
            Player.StartCoroutine(Pattern_65/*ShotPattern4*/(launchTime + 54.54126f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*Explosions*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 13.07852 > Game.Music.Time) yield return Wait(launchTime + 13.07852f);
            Player.StartCoroutine(Pattern_24/*Appear*/(launchTime + 13.07852f, blackboard/*out*/, "node_2.output_bullet"));
            if (launchTime + 13.41226 > Game.Music.Time) yield return Wait(launchTime + 13.41226f);
            Player.StartCoroutine(Pattern_24/*Appear*/(launchTime + 13.41226f, blackboard/*out*/, "node_3.output_bullet"));
            if (launchTime + 13.56643 > Game.Music.Time) yield return Wait(launchTime + 13.56643f);
            Player.StartCoroutine(Pattern_4/*CreatePiano*/(launchTime + 13.56643f, blackboard, (c,i,t,l, castc, casti) => EnemyImage.Enemy4/*out*/, "node_4.output_enemies"));
            if (launchTime + 13.65351 > Game.Music.Time) yield return Wait(launchTime + 13.65351f);
            Player.StartCoroutine(Pattern_25/*Explode*/(launchTime + 13.65351f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet"/*out*/));
            if (launchTime + 13.65355 > Game.Music.Time) yield return Wait(launchTime + 13.65355f);
            Player.StartCoroutine(Pattern_25/*Explode*/(launchTime + 13.65355f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet"/*out*/));
            if (launchTime + 13.97964 > Game.Music.Time) yield return Wait(launchTime + 13.97964f);
            Player.StartCoroutine(Pattern_24/*Appear*/(launchTime + 13.97964f, blackboard/*out*/, "node_7.output_bullet"));
            if (launchTime + 14.10304 > Game.Music.Time) yield return Wait(launchTime + 14.10304f);
            Player.StartCoroutine(Pattern_25/*Explode*/(launchTime + 14.10304f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet"/*out*/));
            if (launchTime + 14.45691 > Game.Music.Time) yield return Wait(launchTime + 14.45691f);
            Player.StartCoroutine(Pattern_67/*Melody*/(launchTime + 14.45691f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard, InputFunc type_input, InputFunc color_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, type_input, "node_2.output_patterninput");
            object type = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => type = o);

            SubscribeInput(outboard, blackboard, color_input, "node_3.output_patterninput");
            object color = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => color = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_34/*Appear1*/(launchTime + 0f, blackboard/*out*/, "node_1.output_enemies"));
            if (launchTime + 0.9319 > Game.Music.Time) yield return Wait(launchTime + 0.9319f);
            Player.StartCoroutine(Pattern_17/*Shoot1*/(launchTime + 0.9319f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemies"/*out*/));
            if (launchTime + 1.817993 > Game.Music.Time) yield return Wait(launchTime + 1.817993f);
            Player.StartCoroutine(Pattern_13/*Explode*/(launchTime + 1.817993f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc type_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_2.output_patterninput");
            object color = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, type_input, "node_3.output_patterninput");
            object type = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => type = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_12/*Appear2*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 1.098663 > Game.Music.Time) yield return Wait(launchTime + 1.098663f);
            Player.StartCoroutine(Pattern_18/*Shoot2*/(launchTime + 1.098663f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.003234 > Game.Music.Time) yield return Wait(launchTime + 2.003234f);
            Player.StartCoroutine(Pattern_13/*Explode*/(launchTime + 2.003234f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard, InputFunc path_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, path_input, "node_0.output_patterninput");
            object path = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => path = o);

            object bulletlist;
            blackboard.SubscribeForChanges("node_12.output_value", (b, o) => bulletlist = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet", (c,i,t,l, castc, casti) => "node_8.output_bullet", (c,i,t,l, castc, casti) => "node_9.output_bullet", (c,i,t,l, castc, casti) => "node_10.output_bullet", (c,i,t,l, castc, casti) => "node_11.output_bullet", (c,i,t,l, castc, casti) => "node_13.output_bullet", (c,i,t,l, castc, casti) => "node_14.output_bullet", (c,i,t,l, castc, casti) => "node_15.output_bullet"/*out*/, "node_12.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,430f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0.1f,0.1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.001998901 > Game.Music.Time) yield return Wait(launchTime + 0.001998901f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.001998901f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x38/255f,0x47/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8757629f));
            if (launchTime + 0.005577083 > Game.Music.Time) yield return Wait(launchTime + 0.005577083f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.005577083f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)ar,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.009597779f));
            if (launchTime + 0.02003479 > Game.Music.Time) yield return Wait(launchTime + 0.02003479f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.02003479f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_5.output_enemy(ies)", 0.8692322f));
            if (launchTime + 0.8964081 > Game.Music.Time) yield return Wait(launchTime + 0.8964081f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.8964081f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)ar,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.8957367f));
            if (launchTime + 0.9079056 > Game.Music.Time) yield return Wait(launchTime + 0.9079056f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 0.9079056f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_7.output_bullet"));
            if (launchTime + 1.028176 > Game.Music.Time) yield return Wait(launchTime + 1.028176f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.028176f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_8.output_bullet"));
            if (launchTime + 1.139595 > Game.Music.Time) yield return Wait(launchTime + 1.139595f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.139595f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_9.output_bullet"));
            if (launchTime + 1.260903 > Game.Music.Time) yield return Wait(launchTime + 1.260903f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.260903f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_10.output_bullet"));
            if (launchTime + 1.355965 > Game.Music.Time) yield return Wait(launchTime + 1.355965f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.355965f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_11.output_bullet"));
            if (launchTime + 1.47889 > Game.Music.Time) yield return Wait(launchTime + 1.47889f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.47889f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_13.output_bullet"));
            if (launchTime + 1.59034 > Game.Music.Time) yield return Wait(launchTime + 1.59034f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.59034f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_14.output_bullet"));
            if (launchTime + 1.709999 > Game.Music.Time) yield return Wait(launchTime + 1.709999f);
            Player.StartCoroutine(Pattern_63/*Shot*/(launchTime + 1.709999f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)"/*out*/, "node_15.output_bullet"));
            if (launchTime + 1.794716 > Game.Music.Time) yield return Wait(launchTime + 1.794716f);
            Player.StartCoroutine(Pattern_64/*Explode*/(launchTime + 1.794716f, blackboard, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 1.818711 > Game.Music.Time) yield return Wait(launchTime + 1.818711f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.818711f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.143257 > Game.Music.Time) yield return Wait(launchTime + 2.143257f);
            Player.StartCoroutine(Pattern_64/*Explode*/(launchTime + 2.143257f, blackboard, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 2.480156 > Game.Music.Time) yield return Wait(launchTime + 2.480156f);
            Player.StartCoroutine(Pattern_64/*Explode*/(launchTime + 2.480156f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 2.717659 > Game.Music.Time) yield return Wait(launchTime + 2.717659f);
            Player.StartCoroutine(Pattern_64/*Explode*/(launchTime + 2.717659f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 3.051186 > Game.Music.Time) yield return Wait(launchTime + 3.051186f);
            Player.StartCoroutine(Pattern_64/*Explode*/(launchTime + 3.051186f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            if (launchTime + 3.386391 > Game.Music.Time) yield return Wait(launchTime + 3.386391f);
            Player.StartCoroutine(Pattern_64/*Explode*/(launchTime + 3.386391f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_12.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_12/*Appear*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 0.1970367 > Game.Music.Time) yield return Wait(launchTime + 0.1970367f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.1970367f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x38/255f,0x47/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8911972f));
            if (launchTime + 1.106529 > Game.Music.Time) yield return Wait(launchTime + 1.106529f);
            Player.StartCoroutine(Pattern_18/*Shoot2*/(launchTime + 1.106529f, blackboard, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x5d/255f,0x19/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.006592 > Game.Music.Time) yield return Wait(launchTime + 2.006592f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 2.006592f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.346809 > Game.Music.Time) yield return Wait(launchTime + 2.346809f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 2.346809f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.682945 > Game.Music.Time) yield return Wait(launchTime + 2.682945f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 2.682945f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.927094 > Game.Music.Time) yield return Wait(launchTime + 2.927094f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 2.927094f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.936775 > Game.Music.Time) yield return Wait(launchTime + 2.936775f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 2.936775f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 2.944519 > Game.Music.Time) yield return Wait(launchTime + 2.944519f);
            Player.StartCoroutine(Pattern_14/*ExplodeOne*/(launchTime + 2.944519f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 3.181007 > Game.Music.Time) yield return Wait(launchTime + 3.181007f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 3.181007f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_9.output_value");
            object enemies;
            blackboard.SubscribeForChanges("node_9.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(135f*(float)ar,210f), (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.0001068115 > Game.Music.Time) yield return Wait(launchTime + 0.0001068115f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0001068115f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-251f*(float)ar,118f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.0001678467 > Game.Music.Time) yield return Wait(launchTime + 0.0001678467f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0001678467f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(251f*(float)ar,118f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.0002288819 > Game.Music.Time) yield return Wait(launchTime + 0.0002288819f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0002288819f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(183f*(float)ar,146f), (c,i,t,l, castc, casti) => -45f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.0002441407 > Game.Music.Time) yield return Wait(launchTime + 0.0002441407f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0002441407f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-135f*(float)ar,210f), (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_enemy(ies)"));
            if (launchTime + 0.0002441407 > Game.Music.Time) yield return Wait(launchTime + 0.0002441407f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0002441407f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy2, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-183f*(float)ar,146f), (c,i,t,l, castc, casti) => 45f, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_enemy(ies)"));
            if (launchTime + 0.0005187988 > Game.Music.Time) yield return Wait(launchTime + 0.0005187988f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.0005187988f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x38/255f,0x47/255f), (c,i,t,l, castc, casti) => null/*out*/, 1.038849f));
            if (launchTime + 0.0131836 > Game.Music.Time) yield return Wait(launchTime + 0.0131836f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.0131836f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_8.output_enemy(ies)", 0.8901672f));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4506262 > Game.Music.Time) yield return Wait(launchTime + 0.4506262f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.4506262f, blackboard/*out*/));
            if (launchTime + 0.9057125 > Game.Music.Time) yield return Wait(launchTime + 0.9057125f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.9057125f, blackboard/*out*/));
            if (launchTime + 1.12991 > Game.Music.Time) yield return Wait(launchTime + 1.12991f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.12991f, blackboard/*out*/));
            if (launchTime + 1.365819 > Game.Music.Time) yield return Wait(launchTime + 1.365819f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.365819f, blackboard/*out*/));
            if (launchTime + 1.58667 > Game.Music.Time) yield return Wait(launchTime + 1.58667f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.58667f, blackboard/*out*/));
            if (launchTime + 1.820905 > Game.Music.Time) yield return Wait(launchTime + 1.820905f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.820905f, blackboard/*out*/));
            if (launchTime + 2.72773 > Game.Music.Time) yield return Wait(launchTime + 2.72773f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 2.72773f, blackboard/*out*/));
            if (launchTime + 3.627864 > Game.Music.Time) yield return Wait(launchTime + 3.627864f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 3.627864f, blackboard/*out*/));
            if (launchTime + 3.862099 > Game.Music.Time) yield return Wait(launchTime + 3.862099f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 3.862099f, blackboard/*out*/));
            if (launchTime + 4.087969 > Game.Music.Time) yield return Wait(launchTime + 4.087969f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 4.087969f, blackboard/*out*/));
            if (launchTime + 5.451663 > Game.Music.Time) yield return Wait(launchTime + 5.451663f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 5.451663f, blackboard/*out*/));
            if (launchTime + 6.364962 > Game.Music.Time) yield return Wait(launchTime + 6.364962f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 6.364962f, blackboard/*out*/));
            if (launchTime + 7.285598 > Game.Music.Time) yield return Wait(launchTime + 7.285598f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 7.285598f, blackboard/*out*/));
            if (launchTime + 7.73308 > Game.Music.Time) yield return Wait(launchTime + 7.73308f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 7.73308f, blackboard/*out*/));
            if (launchTime + 8.180561 > Game.Music.Time) yield return Wait(launchTime + 8.180561f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.180561f, blackboard/*out*/));
            if (launchTime + 8.418972 > Game.Music.Time) yield return Wait(launchTime + 8.418972f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.418972f, blackboard/*out*/));
            if (launchTime + 8.642714 > Game.Music.Time) yield return Wait(launchTime + 8.642714f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.642714f, blackboard/*out*/));
            if (launchTime + 8.859118 > Game.Music.Time) yield return Wait(launchTime + 8.859118f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.859118f, blackboard/*out*/));
            if (launchTime + 9.082857 > Game.Music.Time) yield return Wait(launchTime + 9.082857f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 9.082857f, blackboard/*out*/));
            if (launchTime + 10.0035 > Game.Music.Time) yield return Wait(launchTime + 10.0035f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 10.0035f, blackboard/*out*/));
            if (launchTime + 10.89846 > Game.Music.Time) yield return Wait(launchTime + 10.89846f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 10.89846f, blackboard/*out*/));
            if (launchTime + 11.1332 > Game.Music.Time) yield return Wait(launchTime + 11.1332f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 11.1332f, blackboard/*out*/));
            if (launchTime + 11.35694 > Game.Music.Time) yield return Wait(launchTime + 11.35694f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 11.35694f, blackboard/*out*/));
            if (launchTime + 12.72873 > Game.Music.Time) yield return Wait(launchTime + 12.72873f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 12.72873f, blackboard/*out*/));
            if (launchTime + 13.63103 > Game.Music.Time) yield return Wait(launchTime + 13.63103f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 13.63103f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),250f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(400f,600f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x88/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.2882915 > Game.Music.Time) yield return Wait(launchTime + 0.2882915f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.2882915f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 75f+25f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x7d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.2901742 > Game.Music.Time) yield return Wait(launchTime + 0.2901742f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.2901742f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.2925739 > Game.Music.Time) yield return Wait(launchTime + 0.2925739f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.2925739f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 1.824404 > Game.Music.Time) yield return Wait(launchTime + 1.824404f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 1.824404f, blackboard/*out*/));
            if (launchTime + 3.640494 > Game.Music.Time) yield return Wait(launchTime + 3.640494f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 3.640494f, blackboard/*out*/));
            if (launchTime + 5.459619 > Game.Music.Time) yield return Wait(launchTime + 5.459619f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 5.459619f, blackboard/*out*/));
            if (launchTime + 7.286737 > Game.Music.Time) yield return Wait(launchTime + 7.286737f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 7.286737f, blackboard/*out*/));
            if (launchTime + 9.101343 > Game.Music.Time) yield return Wait(launchTime + 9.101343f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 9.101343f, blackboard/*out*/));
            if (launchTime + 10.94098 > Game.Music.Time) yield return Wait(launchTime + 10.94098f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 10.94098f, blackboard/*out*/));
            if (launchTime + 12.75558 > Game.Music.Time) yield return Wait(launchTime + 12.75558f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 12.75558f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-100f,100f),135f), (c,i,t,l, castc, casti) => (float)d*5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)sin(360f/(float)c*(float)i*5f)*50f+50f+25f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x88/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.001522243 > Game.Music.Time) yield return Wait(launchTime + 0.001522243f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.001522243f, blackboard, (c,i,t,l, castc, casti) => new Color(0x30/255f,0x22/255f,0x17/255f), (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x15/255f,0x12/255f)/*out*/, 1.295987f));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_4.output_value");
            object enemies;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy", (c,i,t,l, castc, casti) => "node_2.output_enemy", (c,i,t,l, castc, casti) => "node_3.output_enemy", (c,i,t,l, castc, casti) => "node_5.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_40/*SingleEnemy*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemy"));
            if (launchTime + 0.8425732 > Game.Music.Time) yield return Wait(launchTime + 0.8425732f);
            Player.StartCoroutine(Pattern_40/*SingleEnemy*/(launchTime + 0.8425732f, blackboard/*out*/, "node_2.output_enemy"));
            if (launchTime + 1.797915 > Game.Music.Time) yield return Wait(launchTime + 1.797915f);
            Player.StartCoroutine(Pattern_40/*SingleEnemy*/(launchTime + 1.797915f, blackboard/*out*/, "node_3.output_enemy"));
            if (launchTime + 2.717866 > Game.Music.Time) yield return Wait(launchTime + 2.717866f);
            Player.StartCoroutine(Pattern_40/*SingleEnemy*/(launchTime + 2.717866f, blackboard/*out*/, "node_5.output_enemy"));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemy_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy9, (c,i,t,l, castc, casti) => 1500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(378f*(float)ar,198f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.1203051 > Game.Music.Time) yield return Wait(launchTime + 0.1203051f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.1203051f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(386f*(float)ar,192f,194f*(float)ar,201f,7f*(float)ar,195f,-146f*(float)ar,170f,-210f*(float)ar,114f,-135f*(float)ar,60f,10f*(float)ar,46f,135f*(float)ar,60f,217f*(float)ar,109f,141f*(float)ar,164f,7f*(float)ar,194f,-227f*(float)ar,201f,-380f*(float)ar,200f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)", 14.99066f));
            if (launchTime + 15.16511 > Game.Music.Time) yield return Wait(launchTime + 15.16511f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.16511f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_41(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.4624872 > Game.Music.Time) yield return Wait(launchTime + 0.4624872f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.4624872f, blackboard/*out*/));
            if (launchTime + 0.9092407 > Game.Music.Time) yield return Wait(launchTime + 0.9092407f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.9092407f, blackboard/*out*/));
            if (launchTime + 1.135765 > Game.Music.Time) yield return Wait(launchTime + 1.135765f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.135765f, blackboard/*out*/));
            if (launchTime + 1.35285 > Game.Music.Time) yield return Wait(launchTime + 1.35285f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.35285f, blackboard/*out*/));
            if (launchTime + 1.585663 > Game.Music.Time) yield return Wait(launchTime + 1.585663f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.585663f, blackboard/*out*/));
            if (launchTime + 1.815335 > Game.Music.Time) yield return Wait(launchTime + 1.815335f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.815335f, blackboard/*out*/));
            if (launchTime + 2.718285 > Game.Music.Time) yield return Wait(launchTime + 2.718285f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 2.718285f, blackboard/*out*/));
            if (launchTime + 3.168179 > Game.Music.Time) yield return Wait(launchTime + 3.168179f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 3.168179f, blackboard/*out*/));
            if (launchTime + 3.410435 > Game.Music.Time) yield return Wait(launchTime + 3.410435f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 3.410435f, blackboard/*out*/));
            if (launchTime + 3.640103 > Game.Music.Time) yield return Wait(launchTime + 3.640103f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 3.640103f, blackboard/*out*/));
            if (launchTime + 3.86348 > Game.Music.Time) yield return Wait(launchTime + 3.86348f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 3.86348f, blackboard/*out*/));
            if (launchTime + 4.089997 > Game.Music.Time) yield return Wait(launchTime + 4.089997f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 4.089997f, blackboard/*out*/));
            if (launchTime + 5.008679 > Game.Music.Time) yield return Wait(launchTime + 5.008679f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 5.008679f, blackboard/*out*/));
            if (launchTime + 5.228913 > Game.Music.Time) yield return Wait(launchTime + 5.228913f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 5.228913f, blackboard/*out*/));
            if (launchTime + 5.445996 > Game.Music.Time) yield return Wait(launchTime + 5.445996f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 5.445996f, blackboard/*out*/));
            if (launchTime + 5.678814 > Game.Music.Time) yield return Wait(launchTime + 5.678814f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 5.678814f, blackboard/*out*/));
            if (launchTime + 5.899039 > Game.Music.Time) yield return Wait(launchTime + 5.899039f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 5.899039f, blackboard/*out*/));
            if (launchTime + 6.374113 > Game.Music.Time) yield return Wait(launchTime + 6.374113f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 6.374113f, blackboard/*out*/));
            if (launchTime + 6.817719 > Game.Music.Time) yield return Wait(launchTime + 6.817719f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 6.817719f, blackboard/*out*/));
            if (launchTime + 7.041096 > Game.Music.Time) yield return Wait(launchTime + 7.041096f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 7.041096f, blackboard/*out*/));
            if (launchTime + 7.264469 > Game.Music.Time) yield return Wait(launchTime + 7.264469f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 7.264469f, blackboard/*out*/));
            if (launchTime + 7.723814 > Game.Music.Time) yield return Wait(launchTime + 7.723814f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 7.723814f, blackboard/*out*/));
            if (launchTime + 8.17371 > Game.Music.Time) yield return Wait(launchTime + 8.17371f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.17371f, blackboard/*out*/));
            if (launchTime + 8.406525 > Game.Music.Time) yield return Wait(launchTime + 8.406525f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.406525f, blackboard/*out*/));
            if (launchTime + 8.62676 > Game.Music.Time) yield return Wait(launchTime + 8.62676f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.62676f, blackboard/*out*/));
            if (launchTime + 8.850136 > Game.Music.Time) yield return Wait(launchTime + 8.850136f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 8.850136f, blackboard/*out*/));
            if (launchTime + 9.082952 > Game.Music.Time) yield return Wait(launchTime + 9.082952f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 9.082952f, blackboard/*out*/));
            if (launchTime + 9.998482 > Game.Music.Time) yield return Wait(launchTime + 9.998482f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 9.998482f, blackboard/*out*/));
            if (launchTime + 10.44838 > Game.Music.Time) yield return Wait(launchTime + 10.44838f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 10.44838f, blackboard/*out*/));
            if (launchTime + 10.66861 > Game.Music.Time) yield return Wait(launchTime + 10.66861f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 10.66861f, blackboard/*out*/));
            if (launchTime + 10.89513 > Game.Music.Time) yield return Wait(launchTime + 10.89513f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 10.89513f, blackboard/*out*/));
            if (launchTime + 11.13424 > Game.Music.Time) yield return Wait(launchTime + 11.13424f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 11.13424f, blackboard/*out*/));
            if (launchTime + 11.35762 > Game.Music.Time) yield return Wait(launchTime + 11.35762f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 11.35762f, blackboard/*out*/));
            if (launchTime + 12.27315 > Game.Music.Time) yield return Wait(launchTime + 12.27315f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 12.27315f, blackboard/*out*/));
            if (launchTime + 12.50597 > Game.Music.Time) yield return Wait(launchTime + 12.50597f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 12.50597f, blackboard/*out*/));
            if (launchTime + 12.7199 > Game.Music.Time) yield return Wait(launchTime + 12.7199f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 12.7199f, blackboard/*out*/));
            if (launchTime + 12.9653 > Game.Music.Time) yield return Wait(launchTime + 12.9653f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 12.9653f, blackboard/*out*/));
            if (launchTime + 13.18553 > Game.Music.Time) yield return Wait(launchTime + 13.18553f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 13.18553f, blackboard/*out*/));
            if (launchTime + 13.63858 > Game.Music.Time) yield return Wait(launchTime + 13.63858f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 13.63858f, blackboard/*out*/));
            if (launchTime + 14.09163 > Game.Music.Time) yield return Wait(launchTime + 14.09163f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 14.09163f, blackboard/*out*/));
            if (launchTime + 14.31815 > Game.Music.Time) yield return Wait(launchTime + 14.31815f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 14.31815f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_42(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_49/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_49/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.149273 > Game.Music.Time) yield return Wait(launchTime + 0.149273f);
            Player.StartCoroutine(Pattern_43/*EnemiesAppear*/(launchTime + 0.149273f, blackboard/*out*/, "node_2.output_enemies"));
            if (launchTime + 0.1656685 > Game.Music.Time) yield return Wait(launchTime + 0.1656685f);
            Player.StartCoroutine(Pattern_45/*Move*/(launchTime + 0.1656685f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemies"/*out*/));
            if (launchTime + 0.1869908 > Game.Music.Time) yield return Wait(launchTime + 0.1869908f);
            Player.StartCoroutine(Pattern_44/*Melody*/(launchTime + 0.1869908f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemies"/*out*/));
            if (launchTime + 14.69327 > Game.Music.Time) yield return Wait(launchTime + 14.69327f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 14.69327f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemies", (c,i,t,l, castc, casti) => new Vector2(54f*(float)ar,1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_5.output_enemy(ies)", 0.4645233f));
            if (launchTime + 15.22721 > Game.Music.Time) yield return Wait(launchTime + 15.22721f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.22721f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemies", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_43(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_3.output_value");
            object enemieslist;
            blackboard.SubscribeForChanges("node_3.output_value", (b, o) => enemieslist = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy", (c,i,t,l, castc, casti) => "node_2.output_enemy", (c,i,t,l, castc, casti) => "node_4.output_enemy", (c,i,t,l, castc, casti) => "node_6.output_enemy", (c,i,t,l, castc, casti) => "node_7.output_enemy", (c,i,t,l, castc, casti) => "node_8.output_enemy", (c,i,t,l, castc, casti) => "node_9.output_enemy", (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 0f, blackboard/*out*/, "node_1.output_enemy"));
            if (launchTime + 1.816719 > Game.Music.Time) yield return Wait(launchTime + 1.816719f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 1.816719f, blackboard/*out*/, "node_2.output_enemy"));
            if (launchTime + 3.635725 > Game.Music.Time) yield return Wait(launchTime + 3.635725f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 3.635725f, blackboard/*out*/, "node_4.output_enemy"));
            if (launchTime + 5.452283 > Game.Music.Time) yield return Wait(launchTime + 5.452283f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 5.452283f, blackboard/*out*/, "node_6.output_enemy"));
            if (launchTime + 7.26817 > Game.Music.Time) yield return Wait(launchTime + 7.26817f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 7.26817f, blackboard/*out*/, "node_7.output_enemy"));
            if (launchTime + 9.086928 > Game.Music.Time) yield return Wait(launchTime + 9.086928f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 9.086928f, blackboard/*out*/, "node_8.output_enemy"));
            if (launchTime + 10.90754 > Game.Music.Time) yield return Wait(launchTime + 10.90754f);
            Player.StartCoroutine(Pattern_46/*Enemy*/(launchTime + 10.90754f, blackboard/*out*/, "node_9.output_enemy"));
            yield break;
        }

        private IEnumerator Pattern_44(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4319972 > Game.Music.Time) yield return Wait(launchTime + 0.4319972f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 0.4319972f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8790836 > Game.Music.Time) yield return Wait(launchTime + 0.8790836f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 0.8790836f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.114151 > Game.Music.Time) yield return Wait(launchTime + 1.114151f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 1.114151f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.326966 > Game.Music.Time) yield return Wait(launchTime + 1.326966f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 1.326966f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.567095 > Game.Music.Time) yield return Wait(launchTime + 1.567095f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 1.567095f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.781834 > Game.Music.Time) yield return Wait(launchTime + 1.781834f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 1.781834f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.695194 > Game.Music.Time) yield return Wait(launchTime + 2.695194f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 2.695194f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.149792 > Game.Music.Time) yield return Wait(launchTime + 3.149792f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 3.149792f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.370949 > Game.Music.Time) yield return Wait(launchTime + 3.370949f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 3.370949f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.593479 > Game.Music.Time) yield return Wait(launchTime + 3.593479f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 3.593479f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.824982 > Game.Music.Time) yield return Wait(launchTime + 3.824982f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 3.824982f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.048077 > Game.Music.Time) yield return Wait(launchTime + 4.048077f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 4.048077f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.975476 > Game.Music.Time) yield return Wait(launchTime + 4.975476f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 4.975476f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.192135 > Game.Music.Time) yield return Wait(launchTime + 5.192135f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 5.192135f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.419155 > Game.Music.Time) yield return Wait(launchTime + 5.419155f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 5.419155f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.656437 > Game.Music.Time) yield return Wait(launchTime + 5.656437f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 5.656437f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.870106 > Game.Music.Time) yield return Wait(launchTime + 5.870106f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 5.870106f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.324711 > Game.Music.Time) yield return Wait(launchTime + 6.324711f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 6.324711f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.786579 > Game.Music.Time) yield return Wait(launchTime + 6.786579f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 6.786579f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.000118 > Game.Music.Time) yield return Wait(launchTime + 7.000118f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 7.000118f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.241184 > Game.Music.Time) yield return Wait(launchTime + 7.241184f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 7.241184f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.699406 > Game.Music.Time) yield return Wait(launchTime + 7.699406f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 7.699406f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.153996 > Game.Music.Time) yield return Wait(launchTime + 8.153996f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 8.153996f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.371418 > Game.Music.Time) yield return Wait(launchTime + 8.371418f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 8.371418f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.608593 > Game.Music.Time) yield return Wait(launchTime + 8.608593f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 8.608593f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.816242 > Game.Music.Time) yield return Wait(launchTime + 8.816242f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 8.816242f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.06683 > Game.Music.Time) yield return Wait(launchTime + 9.06683f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 9.06683f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.968755 > Game.Music.Time) yield return Wait(launchTime + 9.968755f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 9.968755f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.41971 > Game.Music.Time) yield return Wait(launchTime + 10.41971f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 10.41971f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.64672 > Game.Music.Time) yield return Wait(launchTime + 10.64672f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 10.64672f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.8743 > Game.Music.Time) yield return Wait(launchTime + 10.8743f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 10.8743f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.1048 > Game.Music.Time) yield return Wait(launchTime + 11.1048f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 11.1048f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.33617 > Game.Music.Time) yield return Wait(launchTime + 11.33617f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 11.33617f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.24537 > Game.Music.Time) yield return Wait(launchTime + 12.24537f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 12.24537f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.47911 > Game.Music.Time) yield return Wait(launchTime + 12.47911f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 12.47911f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.7036 > Game.Music.Time) yield return Wait(launchTime + 12.7036f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 12.7036f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.92706 > Game.Music.Time) yield return Wait(launchTime + 12.92706f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 12.92706f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.15092 > Game.Music.Time) yield return Wait(launchTime + 13.15092f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 13.15092f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.61425 > Game.Music.Time) yield return Wait(launchTime + 13.61425f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 13.61425f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.056 > Game.Music.Time) yield return Wait(launchTime + 14.056f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 14.056f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.28609 > Game.Music.Time) yield return Wait(launchTime + 14.28609f);
            Player.StartCoroutine(Pattern_48/*Shot*/(launchTime + 14.28609f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_45(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.82135 > Game.Music.Time) yield return Wait(launchTime + 1.82135f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 1.82135f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.643894 > Game.Music.Time) yield return Wait(launchTime + 3.643894f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 3.643894f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.458153 > Game.Music.Time) yield return Wait(launchTime + 5.458153f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 5.458153f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.273517 > Game.Music.Time) yield return Wait(launchTime + 7.273517f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 7.273517f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.088047 > Game.Music.Time) yield return Wait(launchTime + 9.088047f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 9.088047f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.90942 > Game.Music.Time) yield return Wait(launchTime + 10.90942f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 10.90942f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.72645 > Game.Music.Time) yield return Wait(launchTime + 12.72645f);
            Player.StartCoroutine(Pattern_47/*6Moves*/(launchTime + 12.72645f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_46(float launchTime, Blackboard outboard, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemy_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy6, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(-346f*(float)ar,171f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            yield break;
        }

        private IEnumerator Pattern_47(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(50f*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_2.output_enemy(ies)", 0.1862411f));
            if (launchTime + 0.3371047 > Game.Music.Time) yield return Wait(launchTime + 0.3371047f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.3371047f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(50f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_3.output_enemy(ies)", 0.1372948f));
            if (launchTime + 0.6845016 > Game.Music.Time) yield return Wait(launchTime + 0.6845016f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.6845016f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(50f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_4.output_enemy(ies)", 0.1372986f));
            if (launchTime + 1.01981 > Game.Music.Time) yield return Wait(launchTime + 1.01981f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.01981f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(50f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_5.output_enemy(ies)", 0.1372948f));
            if (launchTime + 1.364182 > Game.Music.Time) yield return Wait(launchTime + 1.364182f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.364182f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(50f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_6.output_enemy(ies)", 0.1372948f));
            if (launchTime + 1.584698 > Game.Music.Time) yield return Wait(launchTime + 1.584698f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.584698f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(50f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_7.output_enemy(ies)", 0.1372948f));
            yield break;
        }

        private IEnumerator Pattern_48(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xab/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_49(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_3.output_patterninput");
            object flipy = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_4.output_patterninput");
            object flipx = blackboard.GetValue("node_4.output_patterninput");
            blackboard.SubscribeForChanges("node_4.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy9, (c,i,t,l, castc, casti) => 2400f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-382f*(float)flipx*(float)ar,-196f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.00378041 > Game.Music.Time) yield return Wait(launchTime + 0.00378041f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.00378041f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-239f*(float)flipx*(float)ar,-186f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 0.1430073f));
            if (launchTime + 0.1453133 > Game.Music.Time) yield return Wait(launchTime + 0.1453133f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 0.1453133f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 0.616541 > Game.Music.Time) yield return Wait(launchTime + 0.616541f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 0.616541f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 1.068707 > Game.Music.Time) yield return Wait(launchTime + 1.068707f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 1.068707f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 1.515017 > Game.Music.Time) yield return Wait(launchTime + 1.515017f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 1.515017f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 1.981869 > Game.Music.Time) yield return Wait(launchTime + 1.981869f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 1.981869f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 2.450561 > Game.Music.Time) yield return Wait(launchTime + 2.450561f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 2.450561f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 2.871461 > Game.Music.Time) yield return Wait(launchTime + 2.871461f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 2.871461f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 3.328819 > Game.Music.Time) yield return Wait(launchTime + 3.328819f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 3.328819f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 3.787632 > Game.Music.Time) yield return Wait(launchTime + 3.787632f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 3.787632f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 4.257405 > Game.Music.Time) yield return Wait(launchTime + 4.257405f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 4.257405f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 4.690159 > Game.Music.Time) yield return Wait(launchTime + 4.690159f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 4.690159f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 5.158826 > Game.Music.Time) yield return Wait(launchTime + 5.158826f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 5.158826f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 5.613929 > Game.Music.Time) yield return Wait(launchTime + 5.613929f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 5.613929f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 6.074917 > Game.Music.Time) yield return Wait(launchTime + 6.074917f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 6.074917f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 6.518265 > Game.Music.Time) yield return Wait(launchTime + 6.518265f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 6.518265f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 6.985134 > Game.Music.Time) yield return Wait(launchTime + 6.985134f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 6.985134f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 7.431431 > Game.Music.Time) yield return Wait(launchTime + 7.431431f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 7.431431f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 7.880665 > Game.Music.Time) yield return Wait(launchTime + 7.880665f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 7.880665f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 8.341648 > Game.Music.Time) yield return Wait(launchTime + 8.341648f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 8.341648f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 8.796764 > Game.Music.Time) yield return Wait(launchTime + 8.796764f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 8.796764f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 9.231319 > Game.Music.Time) yield return Wait(launchTime + 9.231319f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 9.231319f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 9.717007 > Game.Music.Time) yield return Wait(launchTime + 9.717007f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 9.717007f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 10.15916 > Game.Music.Time) yield return Wait(launchTime + 10.15916f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 10.15916f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 10.61427 > Game.Music.Time) yield return Wait(launchTime + 10.61427f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 10.61427f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 11.06057 > Game.Music.Time) yield return Wait(launchTime + 11.06057f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 11.06057f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 11.51568 > Game.Music.Time) yield return Wait(launchTime + 11.51568f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 11.51568f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 11.96458 > Game.Music.Time) yield return Wait(launchTime + 11.96458f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 11.96458f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 12.4347 > Game.Music.Time) yield return Wait(launchTime + 12.4347f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 12.4347f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 12.88981 > Game.Music.Time) yield return Wait(launchTime + 12.88981f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 12.88981f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 13.33905 > Game.Music.Time) yield return Wait(launchTime + 13.33905f);
            Player.StartCoroutine(Pattern_50/*Shot*/(launchTime + 13.33905f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 14.71207 > Game.Music.Time) yield return Wait(launchTime + 14.71207f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 14.71207f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-340f*(float)flipx,-252f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_36.output_enemy(ies)", 0.2903404f));
            if (launchTime + 15.11475 > Game.Music.Time) yield return Wait(launchTime + 15.11475f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.11475f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_50(float launchTime, Blackboard outboard, InputFunc enemies_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-25f,25f)*(float)d*(float)flipx,(float)rndf(0f,300f)), (c,i,t,l, castc, casti) => new Vector2(0f,-100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.005125045 > Game.Music.Time) yield return Wait(launchTime + 0.005125045f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.005125045f, blackboard, (c,i,t,l, castc, casti) => new Color(0x34/255f,0x19/255f,0x1b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2503223f));
            yield break;
        }

        private IEnumerator Pattern_51(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,130f), (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.002578739 > Game.Music.Time) yield return Wait(launchTime + 0.002578739f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.002578739f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.886776f));
            if (launchTime + 0.004615779 > Game.Music.Time) yield return Wait(launchTime + 0.004615779f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.004615779f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,130f), (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x70/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.004615779 > Game.Music.Time) yield return Wait(launchTime + 0.004615779f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.004615779f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,130f), (c,i,t,l, castc, casti) => (float)d*5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x8a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_52(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_53/*Claps*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 0.3929711 > Game.Music.Time) yield return Wait(launchTime + 0.3929711f);
            Player.StartCoroutine(Pattern_55/*Beat*/(launchTime + 0.3929711f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_53(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => EnemyImage.Enemy13, (c,i,t,l, castc, casti) => 4000f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(0f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 17f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.0394668 > Game.Music.Time) yield return Wait(launchTime + 0.0394668f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.0394668f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2((160f/((float)c-1f)*(float)i-80f)*(float)ar,185f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_1.output_enemy(ies)", 0.3417168f));
            if (launchTime + 0.3652954 > Game.Music.Time) yield return Wait(launchTime + 0.3652954f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 0.3652954f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.26313 > Game.Music.Time) yield return Wait(launchTime + 1.26313f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 1.26313f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 2.171375 > Game.Music.Time) yield return Wait(launchTime + 2.171375f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 2.171375f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 3.079617 > Game.Music.Time) yield return Wait(launchTime + 3.079617f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 3.079617f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 3.987854 > Game.Music.Time) yield return Wait(launchTime + 3.987854f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 3.987854f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 4.903557 > Game.Music.Time) yield return Wait(launchTime + 4.903557f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 4.903557f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 5.809311 > Game.Music.Time) yield return Wait(launchTime + 5.809311f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 5.809311f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 6.717552 > Game.Music.Time) yield return Wait(launchTime + 6.717552f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 6.717552f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 7.628262 > Game.Music.Time) yield return Wait(launchTime + 7.628262f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 7.628262f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 8.536533 > Game.Music.Time) yield return Wait(launchTime + 8.536533f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 8.536533f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 9.442287 > Game.Music.Time) yield return Wait(launchTime + 9.442287f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 9.442287f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 10.34804 > Game.Music.Time) yield return Wait(launchTime + 10.34804f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 10.34804f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 11.26126 > Game.Music.Time) yield return Wait(launchTime + 11.26126f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 11.26126f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 12.17448 > Game.Music.Time) yield return Wait(launchTime + 12.17448f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 12.17448f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 13.08523 > Game.Music.Time) yield return Wait(launchTime + 13.08523f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 13.08523f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 13.99346 > Game.Music.Time) yield return Wait(launchTime + 13.99346f);
            Player.StartCoroutine(Pattern_54/*Clap*/(launchTime + 13.99346f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 14.93394 > Game.Music.Time) yield return Wait(launchTime + 14.93394f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 14.93394f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,335f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_20.output_enemy(ies)", 0.8797073f));
            if (launchTime + 15.93125 > Game.Music.Time) yield return Wait(launchTime + 15.93125f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.93125f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_54(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "Enemy13_clap", (c,i,t,l, castc, casti) => true/*out*/));
            if (launchTime + 0.4948845 > Game.Music.Time) yield return Wait(launchTime + 0.4948845f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.4948845f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 4f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.5017204 > Game.Music.Time) yield return Wait(launchTime + 0.5017204f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5017204f, blackboard, (c,i,t,l, castc, casti) => new Color(0x35/255f,0x34/255f,0x15/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4782639f));
            yield break;
        }

        private IEnumerator Pattern_55(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.837727 > Game.Music.Time) yield return Wait(launchTime + 1.837727f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 1.837727f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.646138 > Game.Music.Time) yield return Wait(launchTime + 3.646138f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 3.646138f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.459004 > Game.Music.Time) yield return Wait(launchTime + 5.459004f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 5.459004f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.28973 > Game.Music.Time) yield return Wait(launchTime + 7.28973f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 7.28973f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.1026 > Game.Music.Time) yield return Wait(launchTime + 9.1026f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 9.1026f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.91994 > Game.Music.Time) yield return Wait(launchTime + 10.91994f);
            Player.StartCoroutine(Pattern_56/*Move*/(launchTime + 10.91994f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_56(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(-80f*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_2.output_enemy(ies)", 0.3921204f));
            if (launchTime + 0.4555435 > Game.Music.Time) yield return Wait(launchTime + 0.4555435f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.4555435f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(80f*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_3.output_enemy(ies)", 0.3772659f));
            if (launchTime + 0.905323 > Game.Music.Time) yield return Wait(launchTime + 0.905323f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.905323f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(80f*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_4.output_enemy(ies)", 0.3858109f));
            if (launchTime + 1.368549 > Game.Music.Time) yield return Wait(launchTime + 1.368549f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.368549f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(-80f*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutQuad/*out*/, "node_5.output_enemy(ies)", 0.392601f));
            yield break;
        }

        private IEnumerator Pattern_57(float launchTime, Blackboard outboard, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullets_output, "node_2.output_value");
            object bulletlist;
            blackboard.SubscribeForChanges("node_2.output_value", (b, o) => bulletlist = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_58/*NewBullet*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bullet"));
            if (launchTime + 14.60524 > Game.Music.Time) yield return Wait(launchTime + 14.60524f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 14.60524f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.402823E+38 > Game.Music.Time) yield return Wait(launchTime + 3.402823E+38f);
            Player.StartCoroutine(Pattern_59/*SetSpeed6Times*/(launchTime + 3.402823E+38f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_58(float launchTime, Blackboard outboard, string bullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bullet_output, "node_1.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2((-260f+520f*(float)i)*(float)ar,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 60f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            yield break;
        }

        private IEnumerator Pattern_59(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_60/*Speed*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_60/*Speed*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_60/*Speed*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_60/*Speed*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_60/*Speed*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_60/*Speed*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_60(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            yield break;
        }

        private IEnumerator Pattern_61(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-50f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x8b/255f,0x36/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_62(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2242737 > Game.Music.Time) yield return Wait(launchTime + 0.2242737f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 0.2242737f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4634094 > Game.Music.Time) yield return Wait(launchTime + 0.4634094f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 0.4634094f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6732712 > Game.Music.Time) yield return Wait(launchTime + 0.6732712f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 0.6732712f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9099731 > Game.Music.Time) yield return Wait(launchTime + 0.9099731f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 0.9099731f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.351654 > Game.Music.Time) yield return Wait(launchTime + 1.351654f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 1.351654f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.593232 > Game.Music.Time) yield return Wait(launchTime + 1.593232f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 1.593232f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.822617 > Game.Music.Time) yield return Wait(launchTime + 1.822617f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 1.822617f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.059318 > Game.Music.Time) yield return Wait(launchTime + 2.059318f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 2.059318f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.278931 > Game.Music.Time) yield return Wait(launchTime + 2.278931f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 2.278931f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.735245 > Game.Music.Time) yield return Wait(launchTime + 2.735245f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 2.735245f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.184249 > Game.Music.Time) yield return Wait(launchTime + 3.184249f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 3.184249f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.411187 > Game.Music.Time) yield return Wait(launchTime + 3.411187f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 3.411187f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.643005 > Game.Music.Time) yield return Wait(launchTime + 3.643005f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 3.643005f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.869949 > Game.Music.Time) yield return Wait(launchTime + 3.869949f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 3.869949f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.099328 > Game.Music.Time) yield return Wait(launchTime + 4.099328f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 4.099328f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.323829 > Game.Music.Time) yield return Wait(launchTime + 4.323829f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 4.323829f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.553207 > Game.Music.Time) yield return Wait(launchTime + 4.553207f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 4.553207f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.002197 > Game.Music.Time) yield return Wait(launchTime + 5.002197f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 5.002197f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.229141 > Game.Music.Time) yield return Wait(launchTime + 5.229141f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 5.229141f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.463401 > Game.Music.Time) yield return Wait(launchTime + 5.463401f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 5.463401f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.914826 > Game.Music.Time) yield return Wait(launchTime + 5.914826f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 5.914826f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.139329 > Game.Music.Time) yield return Wait(launchTime + 6.139329f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 6.139329f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.361382 > Game.Music.Time) yield return Wait(launchTime + 6.361382f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 6.361382f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.588326 > Game.Music.Time) yield return Wait(launchTime + 6.588326f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 6.588326f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.815261 > Game.Music.Time) yield return Wait(launchTime + 6.815261f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 6.815261f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.044647 > Game.Music.Time) yield return Wait(launchTime + 7.044647f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 7.044647f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.271584 > Game.Music.Time) yield return Wait(launchTime + 7.271584f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 7.271584f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.496087 > Game.Music.Time) yield return Wait(launchTime + 7.496087f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 7.496087f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.735221 > Game.Music.Time) yield return Wait(launchTime + 7.735221f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 7.735221f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.954842 > Game.Music.Time) yield return Wait(launchTime + 7.954842f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 7.954842f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.181778 > Game.Music.Time) yield return Wait(launchTime + 8.181778f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 8.181778f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.625893 > Game.Music.Time) yield return Wait(launchTime + 8.625893f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 8.625893f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.85527 > Game.Music.Time) yield return Wait(launchTime + 8.85527f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 8.85527f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.082214 > Game.Music.Time) yield return Wait(launchTime + 9.082214f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 9.082214f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.548279 > Game.Music.Time) yield return Wait(launchTime + 9.548279f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 9.548279f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.777672 > Game.Music.Time) yield return Wait(launchTime + 9.777672f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 9.777672f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.00216 > Game.Music.Time) yield return Wait(launchTime + 10.00216f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 10.00216f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.22665 > Game.Music.Time) yield return Wait(launchTime + 10.22665f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 10.22665f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.44873 > Game.Music.Time) yield return Wait(launchTime + 10.44873f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 10.44873f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.67322 > Game.Music.Time) yield return Wait(launchTime + 10.67322f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 10.67322f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.9148 > Game.Music.Time) yield return Wait(launchTime + 10.9148f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 10.9148f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.1393 > Game.Music.Time) yield return Wait(launchTime + 11.1393f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 11.1393f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.37112 > Game.Music.Time) yield return Wait(launchTime + 11.37112f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 11.37112f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.59074 > Game.Music.Time) yield return Wait(launchTime + 11.59074f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 11.59074f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.81768 > Game.Music.Time) yield return Wait(launchTime + 11.81768f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 11.81768f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.04218 > Game.Music.Time) yield return Wait(launchTime + 12.04218f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 12.04218f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.28132 > Game.Music.Time) yield return Wait(launchTime + 12.28132f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 12.28132f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.4925 > Game.Music.Time) yield return Wait(launchTime + 12.4925f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 12.4925f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.72321 > Game.Music.Time) yield return Wait(launchTime + 12.72321f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 12.72321f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.96058 > Game.Music.Time) yield return Wait(launchTime + 12.96058f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 12.96058f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.18907 > Game.Music.Time) yield return Wait(launchTime + 13.18907f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 13.18907f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.63496 > Game.Music.Time) yield return Wait(launchTime + 13.63496f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 13.63496f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.08972 > Game.Music.Time) yield return Wait(launchTime + 14.08972f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 14.08972f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.33152 > Game.Music.Time) yield return Wait(launchTime + 14.33152f);
            Player.StartCoroutine(Pattern_61/*Shot*/(launchTime + 14.33152f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_63(float launchTime, Blackboard outboard, InputFunc enemy_input, string bullet_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            SubscribeOutput(outboard, blackboard, bullet_output, "node_3.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x31/255f,0x17/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07618713f));
            if (launchTime + 0.006736755 > Game.Music.Time) yield return Wait(launchTime + 0.006736755f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.006736755f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => -200f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_64(float launchTime, Blackboard outboard, InputFunc count_input, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, count_input, "node_0.output_patterninput");
            object count = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => count = o);

            SubscribeInput(outboard, blackboard, bullets_input, "node_1.output_patterninput");
            object bullets = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => bullets = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_2.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_3.output_elements"));
            if (launchTime + 0.007217407 > Game.Music.Time) yield return Wait(launchTime + 0.007217407f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.007217407f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x38/255f,0x1b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1101227f));
            if (launchTime + 0.008499146 > Game.Music.Time) yield return Wait(launchTime + 0.008499146f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.008499146f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_elements", (c,i,t,l, castc, casti) => 4f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x15/255f,0xff/255f,0x84/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.01317596 > Game.Music.Time) yield return Wait(launchTime + 0.01317596f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.01317596f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_elements", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_65(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object path = blackboard.GetValue((c, i, t, l, castc, casti) => new MyPath(-402f*(float)ar,28f,-27f*(float)ar,187f,27f*(float)ar,-30f,-367f*(float)ar,12f,-107f*(float)ar,153f,-8f*(float)ar,4f,-366f*(float)ar,54f,-183f*(float)ar,193f,-57f*(float)ar,45f,-361f*(float)ar,87f,-255f*(float)ar,153f,-111f*(float)ar,79f,-365f*(float)ar,127f));
            blackboard.SetValue("node_0.output_path", path);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_66/*EnemyMove*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => "node_0.output_path", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_66/*EnemyMove*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => "node_0.output_path", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_66(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc path_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, path_input, "node_1.output_patterninput");
            object path = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => path = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_2.output_patterninput");
            object flipy = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);

            object enmieslist;
            blackboard.SubscribeForChanges("node_13.output_value", (b, o) => enmieslist = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_10.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy4, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-27f,400f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_enemy(ies)"));
            if (launchTime + 0.002441406 > Game.Music.Time) yield return Wait(launchTime + 0.002441406f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.002441406f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy4, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-27f,400f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_enemy(ies)"));
            if (launchTime + 0.003799438 > Game.Music.Time) yield return Wait(launchTime + 0.003799438f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.003799438f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_7.output_enemy(ies)", 0.7998352f));
            if (launchTime + 0.003845215 > Game.Music.Time) yield return Wait(launchTime + 0.003845215f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.003845215f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy4, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-27f,400f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_enemy(ies)"));
            if (launchTime + 0.004333496 > Game.Music.Time) yield return Wait(launchTime + 0.004333496f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.004333496f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_9.output_enemy(ies)", 0.8040009f));
            if (launchTime + 0.005355835 > Game.Music.Time) yield return Wait(launchTime + 0.005355835f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.005355835f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy4, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-27f,400f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_enemy(ies)"));
            if (launchTime + 0.00567627 > Game.Music.Time) yield return Wait(launchTime + 0.00567627f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.00567627f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_11.output_enemy(ies)", 0.7988281f));
            if (launchTime + 0.007202148 > Game.Music.Time) yield return Wait(launchTime + 0.007202148f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.007202148f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 9f, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_12.output_enemy(ies)", 0.7960663f));
            if (launchTime + 0.888382 > Game.Music.Time) yield return Wait(launchTime + 0.888382f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.888382f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x72/255f,0x71/255f,0x72/255f)/*out*/, 0.4654388f));
            if (launchTime + 0.8935242 > Game.Music.Time) yield return Wait(launchTime + 0.8935242f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.8935242f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_15.output_enemy(ies)", 0.9233093f));
            if (launchTime + 0.8978423 > Game.Music.Time) yield return Wait(launchTime + 0.8978423f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.8978423f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_16.output_enemy(ies)", 0.9167786f));
            if (launchTime + 0.8988343 > Game.Music.Time) yield return Wait(launchTime + 0.8988343f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.8988343f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_value", (c,i,t,l, castc, casti) => 0.20f/(float)d, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => new Color(0x31/255f,0xff/255f,0x58/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_17.output_bullet(s)", 0.9177551f));
            if (launchTime + 0.9000245 > Game.Music.Time) yield return Wait(launchTime + 0.9000245f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.9000245f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_18.output_enemy(ies)", 0.9190063f));
            if (launchTime + 0.9021912 > Game.Music.Time) yield return Wait(launchTime + 0.9021912f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.9021912f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 7f, (c,i,t,l, castc, casti) => 9f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_19.output_enemy(ies)", 0.9167633f));
            if (launchTime + 1.387146 > Game.Music.Time) yield return Wait(launchTime + 1.387146f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.387146f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.4236603f));
            if (launchTime + 1.876602 > Game.Music.Time) yield return Wait(launchTime + 1.876602f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.876602f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.8983 > Game.Music.Time) yield return Wait(launchTime + 1.8983f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.8983f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.902634 > Game.Music.Time) yield return Wait(launchTime + 1.902634f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.902634f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.919998 > Game.Music.Time) yield return Wait(launchTime + 1.919998f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.919998f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_67(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4630433 > Game.Music.Time) yield return Wait(launchTime + 0.4630433f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0.4630433f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.917923 > Game.Music.Time) yield return Wait(launchTime + 0.917923f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 0.917923f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.160293 > Game.Music.Time) yield return Wait(launchTime + 1.160293f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 1.160293f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.375396 > Game.Music.Time) yield return Wait(launchTime + 1.375396f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 1.375396f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.593353 > Game.Music.Time) yield return Wait(launchTime + 1.593353f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 1.593353f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.816681 > Game.Music.Time) yield return Wait(launchTime + 1.816681f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 1.816681f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.734558 > Game.Music.Time) yield return Wait(launchTime + 2.734558f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 2.734558f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.183914 > Game.Music.Time) yield return Wait(launchTime + 3.183914f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.183914f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.409973 > Game.Music.Time) yield return Wait(launchTime + 3.409973f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.409973f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.655105 > Game.Music.Time) yield return Wait(launchTime + 3.655105f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.655105f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.87027 > Game.Music.Time) yield return Wait(launchTime + 3.87027f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 3.87027f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.09906 > Game.Music.Time) yield return Wait(launchTime + 4.09906f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 4.09906f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.008743 > Game.Music.Time) yield return Wait(launchTime + 5.008743f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.008743f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.232071 > Game.Music.Time) yield return Wait(launchTime + 5.232071f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.232071f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.460861 > Game.Music.Time) yield return Wait(launchTime + 5.460861f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.460861f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.68695 > Game.Music.Time) yield return Wait(launchTime + 5.68695f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.68695f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.921158 > Game.Music.Time) yield return Wait(launchTime + 5.921158f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 5.921158f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.381424 > Game.Music.Time) yield return Wait(launchTime + 6.381424f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 6.381424f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.83081 > Game.Music.Time) yield return Wait(launchTime + 6.83081f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 6.83081f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.040542 > Game.Music.Time) yield return Wait(launchTime + 7.040542f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.040542f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.280212 > Game.Music.Time) yield return Wait(launchTime + 7.280212f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.280212f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.735061 > Game.Music.Time) yield return Wait(launchTime + 7.735061f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 7.735061f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.195327 > Game.Music.Time) yield return Wait(launchTime + 8.195327f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 8.195327f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.418655 > Game.Music.Time) yield return Wait(launchTime + 8.418655f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 8.418655f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.639267 > Game.Music.Time) yield return Wait(launchTime + 8.639267f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 8.639267f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.873505 > Game.Music.Time) yield return Wait(launchTime + 8.873505f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 8.873505f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.094116 > Game.Music.Time) yield return Wait(launchTime + 9.094116f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 9.094116f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.0065 > Game.Music.Time) yield return Wait(launchTime + 10.0065f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.0065f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.46407 > Game.Music.Time) yield return Wait(launchTime + 10.46407f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.46407f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.69559 > Game.Music.Time) yield return Wait(launchTime + 10.69559f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.69559f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.91347 > Game.Music.Time) yield return Wait(launchTime + 10.91347f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 10.91347f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.13953 > Game.Music.Time) yield return Wait(launchTime + 11.13953f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 11.13953f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.36831 > Game.Music.Time) yield return Wait(launchTime + 11.36831f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 11.36831f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.27528 > Game.Music.Time) yield return Wait(launchTime + 12.27528f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.27528f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.50679 > Game.Music.Time) yield return Wait(launchTime + 12.50679f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.50679f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.73557 > Game.Music.Time) yield return Wait(launchTime + 12.73557f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.73557f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.97797 > Game.Music.Time) yield return Wait(launchTime + 12.97797f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 12.97797f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.20131 > Game.Music.Time) yield return Wait(launchTime + 13.20131f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 13.20131f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.41103 > Game.Music.Time) yield return Wait(launchTime + 13.41103f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 13.41103f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.65071 > Game.Music.Time) yield return Wait(launchTime + 13.65071f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 13.65071f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.10283 > Game.Music.Time) yield return Wait(launchTime + 14.10283f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 14.10283f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.33706 > Game.Music.Time) yield return Wait(launchTime + 14.33706f);
            Player.StartCoroutine(Pattern_5/*Shoot*/(launchTime + 14.33706f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_68(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 132f/*out*/));
            if (launchTime + 14.53493 > Game.Music.Time) yield return Wait(launchTime + 14.53493f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 14.53493f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 29.12293 > Game.Music.Time) yield return Wait(launchTime + 29.12293f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 29.12293f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 43.66181 > Game.Music.Time) yield return Wait(launchTime + 43.66181f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 43.66181f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 58.24198 > Game.Music.Time) yield return Wait(launchTime + 58.24198f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 58.24198f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 65.47402 > Game.Music.Time) yield return Wait(launchTime + 65.47402f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 65.47402f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 72.72319 > Game.Music.Time) yield return Wait(launchTime + 72.72319f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 72.72319f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 80.00644 > Game.Music.Time) yield return Wait(launchTime + 80.00644f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 80.00644f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 264f/*out*/));
            if (launchTime + 83.17008 > Game.Music.Time) yield return Wait(launchTime + 83.17008f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 83.17008f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 264f*2f/*out*/));
            if (launchTime + 85.43615 > Game.Music.Time) yield return Wait(launchTime + 85.43615f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 85.43615f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            if (launchTime + 87.3254 > Game.Music.Time) yield return Wait(launchTime + 87.3254f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 87.3254f, blackboard/*out*/));
            if (launchTime + 90.91281 > Game.Music.Time) yield return Wait(launchTime + 90.91281f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 90.91281f, blackboard/*out*/));
            if (launchTime + 94.55457 > Game.Music.Time) yield return Wait(launchTime + 94.55457f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 94.55457f, blackboard/*out*/));
            if (launchTime + 98.15318 > Game.Music.Time) yield return Wait(launchTime + 98.15318f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 98.15318f, blackboard/*out*/));
            if (launchTime + 101.8165 > Game.Music.Time) yield return Wait(launchTime + 101.8165f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 101.8165f, blackboard/*out*/));
            if (launchTime + 105.4584 > Game.Music.Time) yield return Wait(launchTime + 105.4584f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 105.4584f, blackboard/*out*/));
            if (launchTime + 109.0894 > Game.Music.Time) yield return Wait(launchTime + 109.0894f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 109.0894f, blackboard/*out*/));
            if (launchTime + 112.7203 > Game.Music.Time) yield return Wait(launchTime + 112.7203f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 112.7203f, blackboard/*out*/));
            if (launchTime + 116.3865 > Game.Music.Time) yield return Wait(launchTime + 116.3865f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 116.3865f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 132f/2f/*out*/));
            if (launchTime + 130.9221 > Game.Music.Time) yield return Wait(launchTime + 130.9221f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 130.9221f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 132f/2f/*out*/));
            if (launchTime + 145.4655 > Game.Music.Time) yield return Wait(launchTime + 145.4655f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 145.4655f, blackboard/*out*/));
            if (launchTime + 149.1226 > Game.Music.Time) yield return Wait(launchTime + 149.1226f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 149.1226f, blackboard/*out*/));
            if (launchTime + 152.7276 > Game.Music.Time) yield return Wait(launchTime + 152.7276f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 152.7276f, blackboard/*out*/));
            if (launchTime + 156.3666 > Game.Music.Time) yield return Wait(launchTime + 156.3666f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 156.3666f, blackboard/*out*/));
            if (launchTime + 160.0169 > Game.Music.Time) yield return Wait(launchTime + 160.0169f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 160.0169f, blackboard/*out*/));
            if (launchTime + 163.6672 > Game.Music.Time) yield return Wait(launchTime + 163.6672f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 163.6672f, blackboard/*out*/));
            if (launchTime + 167.3061 > Game.Music.Time) yield return Wait(launchTime + 167.3061f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 167.3061f, blackboard/*out*/));
            if (launchTime + 170.9111 > Game.Music.Time) yield return Wait(launchTime + 170.9111f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 170.9111f, blackboard/*out*/));
            if (launchTime + 174.55 > Game.Music.Time) yield return Wait(launchTime + 174.55f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 174.55f, blackboard/*out*/));
            if (launchTime + 178.2003 > Game.Music.Time) yield return Wait(launchTime + 178.2003f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 178.2003f, blackboard/*out*/));
            if (launchTime + 181.896 > Game.Music.Time) yield return Wait(launchTime + 181.896f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 181.896f, blackboard/*out*/));
            if (launchTime + 185.467 > Game.Music.Time) yield return Wait(launchTime + 185.467f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 185.467f, blackboard/*out*/));
            if (launchTime + 189.0947 > Game.Music.Time) yield return Wait(launchTime + 189.0947f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 189.0947f, blackboard/*out*/));
            if (launchTime + 192.7681 > Game.Music.Time) yield return Wait(launchTime + 192.7681f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 192.7681f, blackboard/*out*/));
            if (launchTime + 196.3433 > Game.Music.Time) yield return Wait(launchTime + 196.3433f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 196.3433f, blackboard/*out*/));
            if (launchTime + 200.0076 > Game.Music.Time) yield return Wait(launchTime + 200.0076f);
            Player.StartCoroutine(Pattern_70/*Pattern*/(launchTime + 200.0076f, blackboard/*out*/));
            if (launchTime + 203.6262 > Game.Music.Time) yield return Wait(launchTime + 203.6262f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 203.6262f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 218.1945 > Game.Music.Time) yield return Wait(launchTime + 218.1945f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 218.1945f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 232.9189 > Game.Music.Time) yield return Wait(launchTime + 232.9189f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 232.9189f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_69(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object path = blackboard.GetValue((c, i, t, l, castc, casti) => new MyPath(-402f*(float)ar,28f,-27f,187f,27f,-30f,-362f,-147f,-107f,153f,-8f,4f,-363f,-86f,-183f,193f,-57f,45f,-361f,-40f,-255f,153f,-111f,79f,-362f*(float)ar,8f));
            blackboard.SetValue("node_0.output_path", path);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_66/*EnemyMove*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => "node_0.output_path", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_66/*EnemyMove*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => "node_0.output_path", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_70(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 132f/*out*/));
            if (launchTime + 0.8731614 > Game.Music.Time) yield return Wait(launchTime + 0.8731614f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0.8731614f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 1.7584 > Game.Music.Time) yield return Wait(launchTime + 1.7584f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 1.7584f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_71(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.2325249 > Game.Music.Time) yield return Wait(launchTime + 0.2325249f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 0.2325249f, blackboard/*out*/));
            if (launchTime + 0.4600334 > Game.Music.Time) yield return Wait(launchTime + 0.4600334f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 0.4600334f, blackboard/*out*/));
            if (launchTime + 0.6765403 > Game.Music.Time) yield return Wait(launchTime + 0.6765403f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 0.6765403f, blackboard/*out*/));
            if (launchTime + 0.9177552 > Game.Music.Time) yield return Wait(launchTime + 0.9177552f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 0.9177552f, blackboard/*out*/));
            if (launchTime + 1.364521 > Game.Music.Time) yield return Wait(launchTime + 1.364521f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 1.364521f, blackboard/*out*/));
            if (launchTime + 1.619423 > Game.Music.Time) yield return Wait(launchTime + 1.619423f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 1.619423f, blackboard/*out*/));
            if (launchTime + 1.819481 > Game.Music.Time) yield return Wait(launchTime + 1.819481f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 1.819481f, blackboard/*out*/));
            if (launchTime + 2.055217 > Game.Music.Time) yield return Wait(launchTime + 2.055217f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 2.055217f, blackboard/*out*/));
            if (launchTime + 2.288208 > Game.Music.Time) yield return Wait(launchTime + 2.288208f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 2.288208f, blackboard/*out*/));
            if (launchTime + 2.729473 > Game.Music.Time) yield return Wait(launchTime + 2.729473f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 2.729473f, blackboard/*out*/));
            if (launchTime + 3.195419 > Game.Music.Time) yield return Wait(launchTime + 3.195419f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 3.195419f, blackboard/*out*/));
            if (launchTime + 3.433876 > Game.Music.Time) yield return Wait(launchTime + 3.433876f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 3.433876f, blackboard/*out*/));
            if (launchTime + 3.642185 > Game.Music.Time) yield return Wait(launchTime + 3.642185f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 3.642185f, blackboard/*out*/));
            if (launchTime + 3.877899 > Game.Music.Time) yield return Wait(launchTime + 3.877899f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 3.877899f, blackboard/*out*/));
            if (launchTime + 4.09169 > Game.Music.Time) yield return Wait(launchTime + 4.09169f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 4.09169f, blackboard/*out*/));
            if (launchTime + 4.319183 > Game.Music.Time) yield return Wait(launchTime + 4.319183f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 4.319183f, blackboard/*out*/));
            if (launchTime + 4.535694 > Game.Music.Time) yield return Wait(launchTime + 4.535694f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 4.535694f, blackboard/*out*/));
            if (launchTime + 5.009884 > Game.Music.Time) yield return Wait(launchTime + 5.009884f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 5.009884f, blackboard/*out*/));
            if (launchTime + 5.231884 > Game.Music.Time) yield return Wait(launchTime + 5.231884f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 5.231884f, blackboard/*out*/));
            if (launchTime + 5.45663 > Game.Music.Time) yield return Wait(launchTime + 5.45663f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 5.45663f, blackboard/*out*/));
            if (launchTime + 5.903392 > Game.Music.Time) yield return Wait(launchTime + 5.903392f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 5.903392f, blackboard/*out*/));
            if (launchTime + 6.136379 > Game.Music.Time) yield return Wait(launchTime + 6.136379f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 6.136379f, blackboard/*out*/));
            if (launchTime + 6.369342 > Game.Music.Time) yield return Wait(launchTime + 6.369342f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 6.369342f, blackboard/*out*/));
            if (launchTime + 6.599582 > Game.Music.Time) yield return Wait(launchTime + 6.599582f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 6.599582f, blackboard/*out*/));
            if (launchTime + 6.82159 > Game.Music.Time) yield return Wait(launchTime + 6.82159f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 6.82159f, blackboard/*out*/));
            if (launchTime + 7.049083 > Game.Music.Time) yield return Wait(launchTime + 7.049083f);
            Player.StartCoroutine(Pattern_72/*Shot*/(launchTime + 7.049083f, blackboard/*out*/));
            if (launchTime + 7.279827 > Game.Music.Time) yield return Wait(launchTime + 7.279827f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 7.279827f, blackboard/*out*/));
            if (launchTime + 7.50185 > Game.Music.Time) yield return Wait(launchTime + 7.50185f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 7.50185f, blackboard/*out*/));
            if (launchTime + 7.732067 > Game.Music.Time) yield return Wait(launchTime + 7.732067f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 7.732067f, blackboard/*out*/));
            if (launchTime + 7.951343 > Game.Music.Time) yield return Wait(launchTime + 7.951343f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 7.951343f, blackboard/*out*/));
            if (launchTime + 8.17609 > Game.Music.Time) yield return Wait(launchTime + 8.17609f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 8.17609f, blackboard/*out*/));
            if (launchTime + 8.63657 > Game.Music.Time) yield return Wait(launchTime + 8.63657f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 8.63657f, blackboard/*out*/));
            if (launchTime + 8.864063 > Game.Music.Time) yield return Wait(launchTime + 8.864063f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 8.864063f, blackboard/*out*/));
            if (launchTime + 9.097034 > Game.Music.Time) yield return Wait(launchTime + 9.097034f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 9.097034f, blackboard/*out*/));
            if (launchTime + 9.321796 > Game.Music.Time) yield return Wait(launchTime + 9.321796f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 9.321796f, blackboard/*out*/));
            if (launchTime + 9.552021 > Game.Music.Time) yield return Wait(launchTime + 9.552021f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 9.552021f, blackboard/*out*/));
            if (launchTime + 10.01248 > Game.Music.Time) yield return Wait(launchTime + 10.01248f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 10.01248f, blackboard/*out*/));
            if (launchTime + 10.46472 > Game.Music.Time) yield return Wait(launchTime + 10.46472f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 10.46472f, blackboard/*out*/));
            if (launchTime + 10.68682 > Game.Music.Time) yield return Wait(launchTime + 10.68682f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 10.68682f, blackboard/*out*/));
            if (launchTime + 10.91115 > Game.Music.Time) yield return Wait(launchTime + 10.91115f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 10.91115f, blackboard/*out*/));
            if (launchTime + 11.1355 > Game.Music.Time) yield return Wait(launchTime + 11.1355f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 11.1355f, blackboard/*out*/));
            if (launchTime + 11.36603 > Game.Music.Time) yield return Wait(launchTime + 11.36603f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 11.36603f, blackboard/*out*/));
            if (launchTime + 11.59191 > Game.Music.Time) yield return Wait(launchTime + 11.59191f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 11.59191f, blackboard/*out*/));
            if (launchTime + 11.81161 > Game.Music.Time) yield return Wait(launchTime + 11.81161f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 11.81161f, blackboard/*out*/));
            if (launchTime + 12.27266 > Game.Music.Time) yield return Wait(launchTime + 12.27266f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 12.27266f, blackboard/*out*/));
            if (launchTime + 12.49699 > Game.Music.Time) yield return Wait(launchTime + 12.49699f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 12.49699f, blackboard/*out*/));
            if (launchTime + 12.72751 > Game.Music.Time) yield return Wait(launchTime + 12.72751f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 12.72751f, blackboard/*out*/));
            if (launchTime + 13.1731 > Game.Music.Time) yield return Wait(launchTime + 13.1731f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 13.1731f, blackboard/*out*/));
            if (launchTime + 13.40827 > Game.Music.Time) yield return Wait(launchTime + 13.40827f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 13.40827f, blackboard/*out*/));
            if (launchTime + 13.6388 > Game.Music.Time) yield return Wait(launchTime + 13.6388f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 13.6388f, blackboard/*out*/));
            if (launchTime + 13.85385 > Game.Music.Time) yield return Wait(launchTime + 13.85385f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 13.85385f, blackboard/*out*/));
            if (launchTime + 14.09056 > Game.Music.Time) yield return Wait(launchTime + 14.09056f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 14.09056f, blackboard/*out*/));
            if (launchTime + 14.32109 > Game.Music.Time) yield return Wait(launchTime + 14.32109f);
            Player.StartCoroutine(Pattern_73/*Shot2*/(launchTime + 14.32109f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_72(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f)*(float)ar,230f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-160f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x0a/255f,0x40/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_73(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f)*(float)ar,-230f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,160f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x0a/255f,0xcb/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_74(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            yield break;
        }

        private IEnumerator Pattern_75(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object enemies;
            blackboard.SubscribeForChanges("node_2.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_77/*Mirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 15f/*out*/, "node_0.output_enemy"));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_77/*Mirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 15f/*out*/, "node_0.output_enemy"));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_77/*Mirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 15f/*out*/, "node_0.output_enemy"));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_77/*Mirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 15f/*out*/, "node_0.output_enemy"));
            if (launchTime + 2.056473 > Game.Music.Time) yield return Wait(launchTime + 2.056473f);
            Player.StartCoroutine(Pattern_78/*Explode*/(launchTime + 2.056473f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.399323 > Game.Music.Time) yield return Wait(launchTime + 2.399323f);
            Player.StartCoroutine(Pattern_78/*Explode*/(launchTime + 2.399323f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.751968 > Game.Music.Time) yield return Wait(launchTime + 2.751968f);
            Player.StartCoroutine(Pattern_78/*Explode*/(launchTime + 2.751968f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.970337 > Game.Music.Time) yield return Wait(launchTime + 2.970337f);
            Player.StartCoroutine(Pattern_78/*Explode*/(launchTime + 2.970337f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.294922 > Game.Music.Time) yield return Wait(launchTime + 3.294922f);
            Player.StartCoroutine(Pattern_78/*Explode*/(launchTime + 3.294922f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.644333 > Game.Music.Time) yield return Wait(launchTime + 3.644333f);
            Player.StartCoroutine(Pattern_78/*Explode*/(launchTime + 3.644333f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_76(float launchTime, Blackboard outboard, InputFunc position1_input, InputFunc position2_input, InputFunc position3_input, InputFunc position4_input, InputFunc enemytype_input, InputFunc startposition_input, InputFunc bullettype_input, InputFunc bulletcolor_input, InputFunc bulletdirection1_input, InputFunc bulletdirection2_input, InputFunc bulletdirection3_input, InputFunc bulletdirection4_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, position1_input, "node_0.output_patterninput");
            object position1 = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => position1 = o);

            SubscribeInput(outboard, blackboard, position2_input, "node_1.output_patterninput");
            object position2 = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => position2 = o);

            SubscribeInput(outboard, blackboard, position3_input, "node_2.output_patterninput");
            object position3 = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => position3 = o);

            SubscribeInput(outboard, blackboard, position4_input, "node_3.output_patterninput");
            object position4 = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => position4 = o);

            SubscribeInput(outboard, blackboard, enemytype_input, "node_4.output_patterninput");
            object enemytype = blackboard.GetValue("node_4.output_patterninput");
            blackboard.SubscribeForChanges("node_4.output_patterninput", (b, o) => enemytype = o);

            SubscribeInput(outboard, blackboard, startposition_input, "node_5.output_patterninput");
            object startposition = blackboard.GetValue("node_5.output_patterninput");
            blackboard.SubscribeForChanges("node_5.output_patterninput", (b, o) => startposition = o);

            SubscribeInput(outboard, blackboard, bullettype_input, "node_6.output_patterninput");
            object bullettype = blackboard.GetValue("node_6.output_patterninput");
            blackboard.SubscribeForChanges("node_6.output_patterninput", (b, o) => bullettype = o);

            SubscribeInput(outboard, blackboard, bulletcolor_input, "node_7.output_patterninput");
            object bulletcolor = blackboard.GetValue("node_7.output_patterninput");
            blackboard.SubscribeForChanges("node_7.output_patterninput", (b, o) => bulletcolor = o);

            SubscribeInput(outboard, blackboard, bulletdirection1_input, "node_8.output_patterninput");
            object bulletdirection1 = blackboard.GetValue("node_8.output_patterninput");
            blackboard.SubscribeForChanges("node_8.output_patterninput", (b, o) => bulletdirection1 = o);

            SubscribeInput(outboard, blackboard, bulletdirection2_input, "node_9.output_patterninput");
            object bulletdirection2 = blackboard.GetValue("node_9.output_patterninput");
            blackboard.SubscribeForChanges("node_9.output_patterninput", (b, o) => bulletdirection2 = o);

            SubscribeInput(outboard, blackboard, bulletdirection3_input, "node_10.output_patterninput");
            object bulletdirection3 = blackboard.GetValue("node_10.output_patterninput");
            blackboard.SubscribeForChanges("node_10.output_patterninput", (b, o) => bulletdirection3 = o);

            SubscribeInput(outboard, blackboard, bulletdirection4_input, "node_11.output_patterninput");
            object bulletdirection4 = blackboard.GetValue("node_11.output_patterninput");
            blackboard.SubscribeForChanges("node_11.output_patterninput", (b, o) => bulletdirection4 = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_12.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => "node_5.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_12.output_enemy(ies)"));
            if (launchTime + 0.2480317 > Game.Music.Time) yield return Wait(launchTime + 0.2480317f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.2480317f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_14.output_enemy(ies)", 0.9116821f));
            if (launchTime + 1.185196 > Game.Music.Time) yield return Wait(launchTime + 1.185196f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.185196f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_15.output_enemy(ies)", 0.1493378f));
            if (launchTime + 1.185944 > Game.Music.Time) yield return Wait(launchTime + 1.185944f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.185944f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.01f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_8.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_7.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_16.output_bullet(s)", 0.1506805f));
            if (launchTime + 1.400436 > Game.Music.Time) yield return Wait(launchTime + 1.400436f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.400436f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_17.output_enemy(ies)", 0.1504211f));
            if (launchTime + 1.40123 > Game.Music.Time) yield return Wait(launchTime + 1.40123f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.40123f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.01f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_9.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_7.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_18.output_bullet(s)", 0.1496277f));
            if (launchTime + 1.62677 > Game.Music.Time) yield return Wait(launchTime + 1.62677f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.62677f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_19.output_enemy(ies)", 0.1483765f));
            if (launchTime + 1.627045 > Game.Music.Time) yield return Wait(launchTime + 1.627045f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.627045f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.01f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_10.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_7.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_20.output_bullet(s)", 0.149353f));
            if (launchTime + 1.847763 > Game.Music.Time) yield return Wait(launchTime + 1.847763f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.847763f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_21.output_enemy(ies)", 0.1505737f));
            if (launchTime + 1.848267 > Game.Music.Time) yield return Wait(launchTime + 1.848267f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.848267f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.01f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_11.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_7.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_22.output_bullet(s)", 0.1504822f));
            if (launchTime + 3.882645 > Game.Music.Time) yield return Wait(launchTime + 3.882645f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 3.882645f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_24.output_enemy(ies)", 0.9047854f));
            if (launchTime + 4.836151 > Game.Music.Time) yield return Wait(launchTime + 4.836151f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 4.836151f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_77(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc flipy_input, InputFunc flipvariants_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_3.output_enemy");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_76/*OneEnemyMove*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-50f*(float)flipx,160f*(float)flipy+50f), (c,i,t,l, castc, casti) => new Vector2(-170f*(float)flipx,160f*(float)flipy+50f), (c,i,t,l, castc, casti) => new Vector2(-170f*(float)flipx,50f*(float)flipy+50f), (c,i,t,l, castc, casti) => new Vector2(-50f*(float)flipx,50f*(float)flipy+50f), (c,i,t,l, castc, casti) => EnemyImage.Enemy9, (c,i,t,l, castc, casti) => new Vector2(-50f*(float)flipx,50f*(float)flipy), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => new Color(0x31/255f,0xa0/255f,0xe5/255f), (c,i,t,l, castc, casti) => 180f+180f*((float)flipy-1f)/2f, (c,i,t,l, castc, casti) => 270f+180f*((float)flipx-1f)/2f, (c,i,t,l, castc, casti) => 0f+180f*((float)flipy-1f)/2f, (c,i,t,l, castc, casti) => 90f+180f*((float)flipx-1f)/2f/*out*/, "node_3.output_enemy"));
            yield break;
        }

        private IEnumerator Pattern_78(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.003204346 > Game.Music.Time) yield return Wait(launchTime + 0.003204346f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.003204346f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xb6/255f,0x54/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_79(float launchTime, Blackboard outboard, InputFunc num_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, num_input, "node_0.output_patterninput");
            object num = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => num = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-295f*(float)ar*(float)flipx,480f/19f*(float)num-240f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(1500f*(float)flipx,0f), (c,i,t,l, castc, casti) => new Vector2(-3000f*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0xd2/255f,0x31/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

    }
}