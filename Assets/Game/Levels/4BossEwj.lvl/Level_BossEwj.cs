#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_BossEwj : LevelScript
    {
        protected override string ClipName {
            get { return "BossEwj.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object boss1 = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_3.output_boss", boss1);

            object boss2;
            blackboard.SubscribeForChanges("node_27.output_value", (b, o) => boss2 = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_19.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_27.output_value");

            if (launchTime + 0.02747201 > Game.Music.Time) yield return Wait(launchTime + 0.02747201f);
            Player.StartCoroutine(BossHealth.Act(launchTime + 0.02747201f, blackboard, (c,i,t,l, castc, casti) => "Evil twins: Boom! and Bam!", (c,i,t,l, castc, casti) => 150000f, (c,i,t,l, castc, casti) => true/*out*/));
            if (launchTime + 0.3308339 > Game.Music.Time) yield return Wait(launchTime + 0.3308339f);
            Player.StartCoroutine(Pattern_1/*Part1*/(launchTime + 0.3308339f, blackboard/*out*/));
            if (launchTime + 0.7450624 > Game.Music.Time) yield return Wait(launchTime + 0.7450624f);
            Player.StartCoroutine(Pattern_42/*DanceMan*/(launchTime + 0.7450624f, blackboard/*out*/));
            if (launchTime + 0.7463814 > Game.Music.Time) yield return Wait(launchTime + 0.7463814f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.7463814f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_boss", (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_5.output_enemy(ies)", 2.803409f));
            if (launchTime + 3.574209 > Game.Music.Time) yield return Wait(launchTime + 3.574209f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 3.574209f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_boss", (c,i,t,l, castc, casti) => "Boss_2_dissasembling", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 4.634616 > Game.Music.Time) yield return Wait(launchTime + 4.634616f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 4.634616f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Boss2, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Boss, (c,i,t,l, castc, casti) => new Vector2(-34f,15f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_7.output_enemy(ies)"));
            if (launchTime + 4.634683 > Game.Music.Time) yield return Wait(launchTime + 4.634683f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 4.634683f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Boss2, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Boss, (c,i,t,l, castc, casti) => new Vector2(34f,15f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_8.output_enemy(ies)"));
            if (launchTime + 4.634918 > Game.Music.Time) yield return Wait(launchTime + 4.634918f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 4.634918f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy(ies)", (c,i,t,l, castc, casti) => "Boss2_leftpart", (c,i,t,l, castc, casti) => false/*out*/));
            if (launchTime + 4.634919 > Game.Music.Time) yield return Wait(launchTime + 4.634919f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 4.634919f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => "Boss2_rightpart", (c,i,t,l, castc, casti) => false/*out*/));
            if (launchTime + 4.636715 > Game.Music.Time) yield return Wait(launchTime + 4.636715f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 4.636715f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_boss", (c,i,t,l, castc, casti) => new Vector2(-374f,328f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_11.output_enemy(ies)", 0.002276897f));
            if (launchTime + 4.824179 > Game.Music.Time) yield return Wait(launchTime + 4.824179f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 4.824179f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-379f*(float)ar,310f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_12.output_enemy(ies)", 0.521884f));
            if (launchTime + 4.825742 > Game.Music.Time) yield return Wait(launchTime + 4.825742f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 4.825742f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(379f*(float)ar,310f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_13.output_enemy(ies)", 0.5261168f));
            if (launchTime + 5.357517 > Game.Music.Time) yield return Wait(launchTime + 5.357517f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 5.357517f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 5.36451 > Game.Music.Time) yield return Wait(launchTime + 5.36451f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 5.36451f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 11.69511 > Game.Music.Time) yield return Wait(launchTime + 11.69511f);
            Player.StartCoroutine(Pattern_38/*Flash*/(launchTime + 11.69511f, blackboard/*out*/));
            if (launchTime + 12.01048 > Game.Music.Time) yield return Wait(launchTime + 12.01048f);
            Player.StartCoroutine(Pattern_7/*Part2*/(launchTime + 12.01048f, blackboard/*out*/));
            if (launchTime + 17.66552 > Game.Music.Time) yield return Wait(launchTime + 17.66552f);
            Player.StartCoroutine(Pattern_7/*Part2*/(launchTime + 17.66552f, blackboard/*out*/));
            if (launchTime + 23.30414 > Game.Music.Time) yield return Wait(launchTime + 23.30414f);
            Player.StartCoroutine(Pattern_9/*BossAppear*/(launchTime + 23.30414f, blackboard, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/, "node_19.output_enemy"));
            if (launchTime + 23.30414 > Game.Music.Time) yield return Wait(launchTime + 23.30414f);
            Player.StartCoroutine(Pattern_9/*BossAppear*/(launchTime + 23.30414f, blackboard, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f/*out*/, "node_19.output_enemy"));
            if (launchTime + 23.33549 > Game.Music.Time) yield return Wait(launchTime + 23.33549f);
            Player.StartCoroutine(Pattern_8/*Part3*/(launchTime + 23.33549f, blackboard, (c,i,t,l, castc, casti) => "node_27.output_value"/*out*/));
            if (launchTime + 23.33779 > Game.Music.Time) yield return Wait(launchTime + 23.33779f);
            Player.StartCoroutine(Pattern_39/*Background*/(launchTime + 23.33779f, blackboard/*out*/));
            if (launchTime + 26.16339 > Game.Music.Time) yield return Wait(launchTime + 26.16339f);
            Player.StartCoroutine(Pattern_39/*Background*/(launchTime + 26.16339f, blackboard/*out*/));
            if (launchTime + 28.97438 > Game.Music.Time) yield return Wait(launchTime + 28.97438f);
            Player.StartCoroutine(Pattern_8/*Part3*/(launchTime + 28.97438f, blackboard, (c,i,t,l, castc, casti) => "node_27.output_value"/*out*/));
            if (launchTime + 28.97864 > Game.Music.Time) yield return Wait(launchTime + 28.97864f);
            Player.StartCoroutine(Pattern_39/*Background*/(launchTime + 28.97864f, blackboard/*out*/));
            if (launchTime + 31.81375 > Game.Music.Time) yield return Wait(launchTime + 31.81375f);
            Player.StartCoroutine(Pattern_39/*Background*/(launchTime + 31.81375f, blackboard/*out*/));
            if (launchTime + 34.63437 > Game.Music.Time) yield return Wait(launchTime + 34.63437f);
            Player.StartCoroutine(Pattern_11/*Part4*/(launchTime + 34.63437f, blackboard, (c,i,t,l, castc, casti) => "node_27.output_value"/*out*/));
            if (launchTime + 54.23258 > Game.Music.Time) yield return Wait(launchTime + 54.23258f);
            Player.StartCoroutine(Pattern_14/*ConnectBoss*/(launchTime + 54.23258f, blackboard, (c,i,t,l, castc, casti) => "node_27.output_value"/*out*/, "node_28.output_boss"));
            if (launchTime + 57.21766 > Game.Music.Time) yield return Wait(launchTime + 57.21766f);
            Player.StartCoroutine(Pattern_15/*Part5*/(launchTime + 57.21766f, blackboard, (c,i,t,l, castc, casti) => "node_28.output_boss"/*out*/));
            if (launchTime + 57.22461 > Game.Music.Time) yield return Wait(launchTime + 57.22461f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 57.22461f, blackboard, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4959908f));
            if (launchTime + 79.82143 > Game.Music.Time) yield return Wait(launchTime + 79.82143f);
            Player.StartCoroutine(Pattern_21/*Part6*/(launchTime + 79.82143f, blackboard, (c,i,t,l, castc, casti) => "node_28.output_boss"/*out*/));
            if (launchTime + 99.58326 > Game.Music.Time) yield return Wait(launchTime + 99.58326f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 99.58326f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1880112f));
            if (launchTime + 99.84042 > Game.Music.Time) yield return Wait(launchTime + 99.84042f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 99.84042f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1674194f));
            if (launchTime + 100.0983 > Game.Music.Time) yield return Wait(launchTime + 100.0983f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 100.0983f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1674194f));
            if (launchTime + 100.3709 > Game.Music.Time) yield return Wait(launchTime + 100.3709f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 100.3709f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1674194f));
            if (launchTime + 100.6361 > Game.Music.Time) yield return Wait(launchTime + 100.6361f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 100.6361f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1489944f));
            if (launchTime + 100.8166 > Game.Music.Time) yield return Wait(launchTime + 100.8166f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 100.8166f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1121521f));
            if (launchTime + 100.9823 > Game.Music.Time) yield return Wait(launchTime + 100.9823f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 100.9823f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1526794f));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_2/*CreateEnemies*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => EnemyImage.Enemy6/*out*/, "node_0.output_enemy"));
            if (launchTime + 0.4056107 > Game.Music.Time) yield return Wait(launchTime + 0.4056107f);
            Player.StartCoroutine(Pattern_3/*ShootEnemies*/(launchTime + 0.4056107f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            if (launchTime + 5.645201 > Game.Music.Time) yield return Wait(launchTime + 5.645201f);
            Player.StartCoroutine(Pattern_2/*CreateEnemies*/(launchTime + 5.645201f, blackboard, (c,i,t,l, castc, casti) => EnemyImage.Enemy10/*out*/, "node_3.output_enemy"));
            if (launchTime + 6.057421 > Game.Music.Time) yield return Wait(launchTime + 6.057421f);
            Player.StartCoroutine(Pattern_3/*ShootEnemies*/(launchTime + 6.057421f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard, InputFunc type_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, type_input, "node_0.output_patterninput");
            object type = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => type = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_1.output_enemy(ies)");
            object enemies;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(200f*((float)i-0.5f)*2f*(float)ar,350f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.004089326 > Game.Music.Time) yield return Wait(launchTime + 0.004089326f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.004089326f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(250f*((float)i-0.5f)*2f*(float)ar,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.01413441 > Game.Music.Time) yield return Wait(launchTime + 0.01413441f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.01413441f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_value", (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_3.output_enemy(ies)", 0.397108f));
            if (launchTime + 5.929776 > Game.Music.Time) yield return Wait(launchTime + 5.929776f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 5.929776f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_value", (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_7.output_enemy(ies)", 0.5321903f));
            if (launchTime + 6.502198 > Game.Music.Time) yield return Wait(launchTime + 6.502198f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 6.502198f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_value", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3552563 > Game.Music.Time) yield return Wait(launchTime + 0.3552563f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 0.3552563f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7179231 > Game.Music.Time) yield return Wait(launchTime + 0.7179231f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 0.7179231f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8954268 > Game.Music.Time) yield return Wait(launchTime + 0.8954268f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 0.8954268f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.060624 > Game.Music.Time) yield return Wait(launchTime + 1.060624f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 1.060624f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.246488 > Game.Music.Time) yield return Wait(launchTime + 1.246488f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 1.246488f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.59755 > Game.Music.Time) yield return Wait(launchTime + 1.59755f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 1.59755f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.7601 > Game.Music.Time) yield return Wait(launchTime + 1.7601f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 1.7601f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.956502 > Game.Music.Time) yield return Wait(launchTime + 1.956502f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 1.956502f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.122171 > Game.Music.Time) yield return Wait(launchTime + 2.122171f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 2.122171f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.299675 > Game.Music.Time) yield return Wait(launchTime + 2.299675f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 2.299675f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.473638 > Game.Music.Time) yield return Wait(launchTime + 2.473638f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 2.473638f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.666515 > Game.Music.Time) yield return Wait(launchTime + 2.666515f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 2.666515f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.832184 > Game.Music.Time) yield return Wait(launchTime + 2.832184f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 2.832184f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.173116 > Game.Music.Time) yield return Wait(launchTime + 3.173116f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 3.173116f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.534309 > Game.Music.Time) yield return Wait(launchTime + 3.534309f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 3.534309f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.715756 > Game.Music.Time) yield return Wait(launchTime + 3.715756f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 3.715756f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.890169 > Game.Music.Time) yield return Wait(launchTime + 3.890169f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 3.890169f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.062874 > Game.Music.Time) yield return Wait(launchTime + 4.062874f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 4.062874f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.413935 > Game.Music.Time) yield return Wait(launchTime + 4.413935f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 4.413935f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.593159 > Game.Music.Time) yield return Wait(launchTime + 4.593159f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 4.593159f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.772886 > Game.Music.Time) yield return Wait(launchTime + 4.772886f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 4.772886f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.954334 > Game.Music.Time) yield return Wait(launchTime + 4.954334f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 4.954334f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.131837 > Game.Music.Time) yield return Wait(launchTime + 5.131837f);
            Player.StartCoroutine(Pattern_4/*BassDrum*/(launchTime + 5.131837f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.292635 > Game.Music.Time) yield return Wait(launchTime + 5.292635f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 5.292635f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.477649 > Game.Music.Time) yield return Wait(launchTime + 5.477649f);
            Player.StartCoroutine(Pattern_5/*Drum*/(launchTime + 5.477649f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.01013869 > Game.Music.Time) yield return Wait(launchTime + 0.01013869f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.01013869f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.009042978 > Game.Music.Time) yield return Wait(launchTime + 0.009042978f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.009042978f, blackboard, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x29/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3802592f));
            if (launchTime + 0.01933968 > Game.Music.Time) yield return Wait(launchTime + 0.01933968f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.01933968f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard, InputFunc direction_input, InputFunc partname_input, InputFunc height_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, direction_input, "node_0.output_patterninput");
            object direction = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => direction = o);

            SubscribeInput(outboard, blackboard, partname_input, "node_1.output_patterninput");
            object partname = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => partname = o);

            SubscribeInput(outboard, blackboard, height_input, "node_2.output_patterninput");
            object height = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => height = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Boss2, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Boss, (c,i,t,l, castc, casti) => new Vector2(-350f*(float)direction*(float)ar,(float)height), (c,i,t,l, castc, casti) => 90f*(float)direction, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.00420285 > Game.Music.Time) yield return Wait(launchTime + 0.00420285f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 0.00420285f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => false/*out*/));
            if (launchTime + 0.04435254 > Game.Music.Time) yield return Wait(launchTime + 0.04435254f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.04435254f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(350f*(float)direction*(float)ar,(float)height), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_5.output_enemy(ies)", 2.828794f));
            if (launchTime + 0.04487229 > Game.Music.Time) yield return Wait(launchTime + 0.04487229f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.04487229f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.15f-0.05f*(float)toint((float)d,3f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -(float)t*3600f+360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x4d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)", 2.831259f));
            if (launchTime + 0.04487706 > Game.Music.Time) yield return Wait(launchTime + 0.04487706f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.04487706f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.15f-0.05f*(float)toint((float)d,3f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)t*3600f+360f/(float)c+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x4d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_bullet(s)", 2.829901f));
            if (launchTime + 0.1670694 > Game.Music.Time) yield return Wait(launchTime + 0.1670694f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.1670694f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2c/255f,0x10/255f,0x2e/255f)/*out*/, 2.680452f));
            if (launchTime + 2.852046 > Game.Music.Time) yield return Wait(launchTime + 2.852046f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.852046f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.03583622f));
            if (launchTime + 2.894077 > Game.Music.Time) yield return Wait(launchTime + 2.894077f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.894077f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_6/*OneWayBoss*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "Boss2_rightpart", (c,i,t,l, castc, casti) => 200f/*out*/));
            if (launchTime + 2.825445 > Game.Music.Time) yield return Wait(launchTime + 2.825445f);
            Player.StartCoroutine(Pattern_6/*OneWayBoss*/(launchTime + 2.825445f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => "Boss2_leftpart", (c,i,t,l, castc, casti) => -200f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 0.1712838 > Game.Music.Time) yield return Wait(launchTime + 0.1712838f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 0.1712838f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x40/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 0.3613263 > Game.Music.Time) yield return Wait(launchTime + 0.3613263f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 0.3613263f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xa4/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 0.5379695 > Game.Music.Time) yield return Wait(launchTime + 0.5379695f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 0.5379695f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 0.7107373 > Game.Music.Time) yield return Wait(launchTime + 0.7107373f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 0.7107373f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 0.8865395 > Game.Music.Time) yield return Wait(launchTime + 0.8865395f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 0.8865395f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 1.058337 > Game.Music.Time) yield return Wait(launchTime + 1.058337f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 1.058337f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 1.236533 > Game.Music.Time) yield return Wait(launchTime + 1.236533f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 1.236533f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x95/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 1.425906 > Game.Music.Time) yield return Wait(launchTime + 1.425906f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 1.425906f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x95/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 1.596375 > Game.Music.Time) yield return Wait(launchTime + 1.596375f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 1.596375f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x95/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 1.777497 > Game.Music.Time) yield return Wait(launchTime + 1.777497f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 1.777497f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x95/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 1.9453 > Game.Music.Time) yield return Wait(launchTime + 1.9453f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 1.9453f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 2.143737 > Game.Music.Time) yield return Wait(launchTime + 2.143737f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 2.143737f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 2.30355 > Game.Music.Time) yield return Wait(launchTime + 2.30355f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 2.30355f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 2.470023 > Game.Music.Time) yield return Wait(launchTime + 2.470023f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 2.470023f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 2.643155 > Game.Music.Time) yield return Wait(launchTime + 2.643155f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 2.643155f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 2.831239 > Game.Music.Time) yield return Wait(launchTime + 2.831239f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 2.831239f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 2.999568 > Game.Music.Time) yield return Wait(launchTime + 2.999568f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 2.999568f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x4b/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 3.174546 > Game.Music.Time) yield return Wait(launchTime + 3.174546f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 3.174546f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xa2/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 3.35838 > Game.Music.Time) yield return Wait(launchTime + 3.35838f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 3.35838f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 3.533349 > Game.Music.Time) yield return Wait(launchTime + 3.533349f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 3.533349f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 3.722241 > Game.Music.Time) yield return Wait(launchTime + 3.722241f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 3.722241f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 3.878217 > Game.Music.Time) yield return Wait(launchTime + 3.878217f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 3.878217f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 4.071567 > Game.Music.Time) yield return Wait(launchTime + 4.071567f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 4.071567f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 4.254277 > Game.Music.Time) yield return Wait(launchTime + 4.254277f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 4.254277f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 4.424115 > Game.Music.Time) yield return Wait(launchTime + 4.424115f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 4.424115f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 4.588749 > Game.Music.Time) yield return Wait(launchTime + 4.588749f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 4.588749f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 4.763721 > Game.Music.Time) yield return Wait(launchTime + 4.763721f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 4.763721f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 4.95055 > Game.Music.Time) yield return Wait(launchTime + 4.95055f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 4.95055f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 5.133199 > Game.Music.Time) yield return Wait(launchTime + 5.133199f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 5.133199f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            if (launchTime + 5.30093 > Game.Music.Time) yield return Wait(launchTime + 5.30093f);
            Player.StartCoroutine(Pattern_10/*Shoot*/(launchTime + 5.30093f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => 250f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_3.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Boss2, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Boss, (c,i,t,l, castc, casti) => new Vector2(-386f*(float)flipx,147f*(float)flipy), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.003458031 > Game.Music.Time) yield return Wait(launchTime + 0.003458031f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 0.003458031f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => "Boss2_rightpart", (c,i,t,l, castc, casti) => false/*out*/));
            if (launchTime + 0.03988648 > Game.Music.Time) yield return Wait(launchTime + 0.03988648f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.03988648f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-370f*(float)ar,100f,-150f*(float)ar,100f,150f*(float)ar,-100f,270f*(float)ar,0f,150f*(float)ar,100f,-150f*(float)ar,-100f,-270f*(float)ar,0f,-150f*(float)ar,100f,150f*(float)ar,-100f,270f*(float)ar,0f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 9f, (c,i,t,l, castc, casti) => new Vector2(0f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx*0.9f,(float)flipy), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 11.28036f));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard, InputFunc enemy_input, InputFunc color_input, InputFunc speed_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            SubscribeInput(outboard, blackboard, color_input, "node_1.output_patterninput");
            object color = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, speed_input, "node_2.output_patterninput");
            object speed = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => speed = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*2f-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_12/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.003532409 > Game.Music.Time) yield return Wait(launchTime + 0.003532409f);
            Player.StartCoroutine(Pattern_19/*Beat*/(launchTime + 0.003532409f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.009124756 > Game.Music.Time) yield return Wait(launchTime + 0.009124756f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 0.009124756f, blackboard/*out*/));
            if (launchTime + 5.652073 > Game.Music.Time) yield return Wait(launchTime + 5.652073f);
            Player.StartCoroutine(Pattern_12/*Shoot*/(launchTime + 5.652073f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.655536 > Game.Music.Time) yield return Wait(launchTime + 5.655536f);
            Player.StartCoroutine(Pattern_19/*Beat*/(launchTime + 5.655536f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.21262 > Game.Music.Time) yield return Wait(launchTime + 10.21262f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 10.21262f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(-60f+120f*((float)i%2f),180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_enemy(ies)", 1.066254f));
            if (launchTime + 11.29803 > Game.Music.Time) yield return Wait(launchTime + 11.29803f);
            Player.StartCoroutine(Pattern_43/*Beat2*/(launchTime + 11.29803f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.30251 > Game.Music.Time) yield return Wait(launchTime + 11.30251f);
            Player.StartCoroutine(Pattern_12/*Shoot*/(launchTime + 11.30251f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 16.94062 > Game.Music.Time) yield return Wait(launchTime + 16.94062f);
            Player.StartCoroutine(Pattern_12/*Shoot*/(launchTime + 16.94062f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 16.9443 > Game.Music.Time) yield return Wait(launchTime + 16.9443f);
            Player.StartCoroutine(Pattern_43/*Beat2*/(launchTime + 16.9443f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard, InputFunc angle_input, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, angle_input, "node_0.output_patterninput");
            object angle = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => angle = o);

            SubscribeInput(outboard, blackboard, boss_input, "node_1.output_patterninput");
            object boss = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.1520462 > Game.Music.Time) yield return Wait(launchTime + 0.1520462f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 0.1520462f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3380699 > Game.Music.Time) yield return Wait(launchTime + 0.3380699f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 0.3380699f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5027733 > Game.Music.Time) yield return Wait(launchTime + 0.5027733f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 0.5027733f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6887969 > Game.Music.Time) yield return Wait(launchTime + 0.6887969f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 0.6887969f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8670654 > Game.Music.Time) yield return Wait(launchTime + 0.8670654f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 0.8670654f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.04921 > Game.Music.Time) yield return Wait(launchTime + 1.04921f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 1.04921f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.210041 > Game.Music.Time) yield return Wait(launchTime + 1.210041f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 1.210041f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.397999 > Game.Music.Time) yield return Wait(launchTime + 1.397999f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 1.397999f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.564644 > Game.Music.Time) yield return Wait(launchTime + 1.564644f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 1.564644f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.746788 > Game.Music.Time) yield return Wait(launchTime + 1.746788f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 1.746788f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.921185 > Game.Music.Time) yield return Wait(launchTime + 1.921185f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 1.921185f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.105267 > Game.Music.Time) yield return Wait(launchTime + 2.105267f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 2.105267f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.260285 > Game.Music.Time) yield return Wait(launchTime + 2.260285f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 2.260285f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.457932 > Game.Music.Time) yield return Wait(launchTime + 2.457932f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 2.457932f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.623585 > Game.Music.Time) yield return Wait(launchTime + 2.623585f);
            Player.StartCoroutine(Pattern_13/*SingleShot*/(launchTime + 2.623585f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard, InputFunc boss_input, InputFunc angle_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);

            SubscribeInput(outboard, blackboard, angle_input, "node_1.output_patterninput");
            object angle = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => angle = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_2.output_elements"));
            if (launchTime + 0.009735107 > Game.Music.Time) yield return Wait(launchTime + 0.009735107f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.009735107f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => (float)rndf(-50f-12.5f*(float)d,50f+12.5f*(float)d), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 90f+180f*((float)i%2f)+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(50f,100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 1.3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 2.822948 > Game.Music.Time) yield return Wait(launchTime + 2.822948f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.822948f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 2.826404 > Game.Music.Time) yield return Wait(launchTime + 2.826404f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 2.826404f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => 0.7f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 2.875534 > Game.Music.Time) yield return Wait(launchTime + 2.875534f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.875534f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard, InputFunc bossparts_input, string boss_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bossparts_input, "node_0.output_patterninput");
            object bossparts = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bossparts = o);

            SubscribeOutput(outboard, blackboard, boss_output, "node_3.output_boss");
            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_3.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(30f-60f*((float)i%2f),0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.spring/*out*/, "node_2.output_enemy(ies)", 1.253826f));
            if (launchTime + 1.255756 > Game.Music.Time) yield return Wait(launchTime + 1.255756f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.255756f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_boss", (c,i,t,l, castc, casti) => new Vector2(0f,172f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.002075195f));
            if (launchTime + 1.25843 > Game.Music.Time) yield return Wait(launchTime + 1.25843f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 1.25843f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_boss", (c,i,t,l, castc, casti) => "Boss2_connecting", (c,i,t,l, castc, casti) => true/*out*/));
            if (launchTime + 1.260181 > Game.Music.Time) yield return Wait(launchTime + 1.260181f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 1.260181f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 3.24263 > Game.Music.Time) yield return Wait(launchTime + 3.24263f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 3.24263f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_boss", (c,i,t,l, castc, casti) => "Boss2_comon", (c,i,t,l, castc, casti) => false/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_17/*Melody*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.01234054 > Game.Music.Time) yield return Wait(launchTime + 0.01234054f);
            Player.StartCoroutine(Pattern_16/*Flower*/(launchTime + 0.01234054f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.29296 > Game.Music.Time) yield return Wait(launchTime + 11.29296f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 11.29296f, blackboard, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.3303146f));
            if (launchTime + 11.30999 > Game.Music.Time) yield return Wait(launchTime + 11.30999f);
            Player.StartCoroutine(Pattern_17/*Melody*/(launchTime + 11.30999f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0.17f/(((float)d+1f)/2f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => -(float)t*5200f-360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -(float)t*5200f-360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 130f+70f*(float)sint((float)t*8f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x72/255f,0xcc/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 22.57942f));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0.15f/(((float)d+1f)/2f), (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)t*5100f+360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)t*5100f+360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+70f*(float)sint((float)t*8f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3e/255f,0x5d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)", 22.58228f));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3602143 > Game.Music.Time) yield return Wait(launchTime + 0.3602143f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 0.3602143f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5477104 > Game.Music.Time) yield return Wait(launchTime + 0.5477104f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 0.5477104f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7178269 > Game.Music.Time) yield return Wait(launchTime + 0.7178269f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 0.7178269f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.898346 > Game.Music.Time) yield return Wait(launchTime + 0.898346f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 0.898346f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.235096 > Game.Music.Time) yield return Wait(launchTime + 1.235096f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.235096f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.585739 > Game.Music.Time) yield return Wait(launchTime + 1.585739f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.585739f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.922489 > Game.Music.Time) yield return Wait(launchTime + 1.922489f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 1.922489f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.151623 > Game.Music.Time) yield return Wait(launchTime + 2.151623f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.151623f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.321736 > Game.Music.Time) yield return Wait(launchTime + 2.321736f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.321736f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.49186 > Game.Music.Time) yield return Wait(launchTime + 2.49186f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.49186f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.773041 > Game.Music.Time) yield return Wait(launchTime + 2.773041f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 2.773041f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.186188 > Game.Music.Time) yield return Wait(launchTime + 3.186188f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 3.186188f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.533333 > Game.Music.Time) yield return Wait(launchTime + 3.533333f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 3.533333f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.71735 > Game.Music.Time) yield return Wait(launchTime + 3.71735f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 3.71735f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.887455 > Game.Music.Time) yield return Wait(launchTime + 3.887455f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 3.887455f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.088799 > Game.Music.Time) yield return Wait(launchTime + 4.088799f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 4.088799f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.418629 > Game.Music.Time) yield return Wait(launchTime + 4.418629f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 4.418629f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.772724 > Game.Music.Time) yield return Wait(launchTime + 4.772724f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 4.772724f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.95673 > Game.Music.Time) yield return Wait(launchTime + 4.95673f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 4.95673f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.293473 > Game.Music.Time) yield return Wait(launchTime + 5.293473f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 5.293473f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.657978 > Game.Music.Time) yield return Wait(launchTime + 5.657978f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 5.657978f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.921855 > Game.Music.Time) yield return Wait(launchTime + 5.921855f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 5.921855f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.17181 > Game.Music.Time) yield return Wait(launchTime + 6.17181f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 6.17181f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.439129 > Game.Music.Time) yield return Wait(launchTime + 6.439129f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 6.439129f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.708248 > Game.Music.Time) yield return Wait(launchTime + 6.708248f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 6.708248f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.899891 > Game.Music.Time) yield return Wait(launchTime + 6.899891f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 6.899891f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.066555 > Game.Music.Time) yield return Wait(launchTime + 7.066555f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 7.066555f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.32442 > Game.Music.Time) yield return Wait(launchTime + 7.32442f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 7.32442f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.588276 > Game.Music.Time) yield return Wait(launchTime + 7.588276f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 7.588276f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.127079 > Game.Music.Time) yield return Wait(launchTime + 8.127079f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 8.127079f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.330311 > Game.Music.Time) yield return Wait(launchTime + 8.330311f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 8.330311f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.489986 > Game.Music.Time) yield return Wait(launchTime + 8.489986f);
            Player.StartCoroutine(Pattern_45/*Circle*/(launchTime + 8.489986f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.818821 > Game.Music.Time) yield return Wait(launchTime + 8.818821f);
            Player.StartCoroutine(Pattern_45/*Circle*/(launchTime + 8.818821f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.179478 > Game.Music.Time) yield return Wait(launchTime + 9.179478f);
            Player.StartCoroutine(Pattern_45/*Circle*/(launchTime + 9.179478f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.534855 > Game.Music.Time) yield return Wait(launchTime + 9.534855f);
            Player.StartCoroutine(Pattern_45/*Circle*/(launchTime + 9.534855f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.874851 > Game.Music.Time) yield return Wait(launchTime + 9.874851f);
            Player.StartCoroutine(Pattern_46/*Laser*/(launchTime + 9.874851f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.06048 > Game.Music.Time) yield return Wait(launchTime + 10.06048f);
            Player.StartCoroutine(Pattern_46/*Laser*/(launchTime + 10.06048f, blackboard, (c,i,t,l, castc, casti) => -45f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.23551 > Game.Music.Time) yield return Wait(launchTime + 10.23551f);
            Player.StartCoroutine(Pattern_46/*Laser*/(launchTime + 10.23551f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.42117 > Game.Music.Time) yield return Wait(launchTime + 10.42117f);
            Player.StartCoroutine(Pattern_46/*Laser*/(launchTime + 10.42117f, blackboard, (c,i,t,l, castc, casti) => 45f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.60144 > Game.Music.Time) yield return Wait(launchTime + 10.60144f);
            Player.StartCoroutine(Pattern_46/*Laser*/(launchTime + 10.60144f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.94844 > Game.Music.Time) yield return Wait(launchTime + 10.94844f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 10.94844f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.12409 > Game.Music.Time) yield return Wait(launchTime + 11.12409f);
            Player.StartCoroutine(Pattern_18/*Shot*/(launchTime + 11.12409f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)angle+(float)rndf(-30f,30f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f+50f*((float)d-1f)+50f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x35/255f,0x93/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.0009422303 > Game.Music.Time) yield return Wait(launchTime + 0.0009422303f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.0009422303f, blackboard, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x14/255f,0x24/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537552f));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3502999 > Game.Music.Time) yield return Wait(launchTime + 0.3502999f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 0.3502999f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7152176 > Game.Music.Time) yield return Wait(launchTime + 0.7152176f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 0.7152176f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.060024 > Game.Music.Time) yield return Wait(launchTime + 1.060024f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 1.060024f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.41058 > Game.Music.Time) yield return Wait(launchTime + 1.41058f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 1.41058f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.772632 > Game.Music.Time) yield return Wait(launchTime + 1.772632f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 1.772632f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.120311 > Game.Music.Time) yield return Wait(launchTime + 2.120311f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 2.120311f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.473747 > Game.Music.Time) yield return Wait(launchTime + 2.473747f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 2.473747f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.830044 > Game.Music.Time) yield return Wait(launchTime + 2.830044f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 2.830044f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.192094 > Game.Music.Time) yield return Wait(launchTime + 3.192094f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 3.192094f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.531154 > Game.Music.Time) yield return Wait(launchTime + 3.531154f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 3.531154f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.893203 > Game.Music.Time) yield return Wait(launchTime + 3.893203f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 3.893203f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.24664 > Game.Music.Time) yield return Wait(launchTime + 4.24664f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 4.24664f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.594323 > Game.Music.Time) yield return Wait(launchTime + 4.594323f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 4.594323f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.947746 > Game.Music.Time) yield return Wait(launchTime + 4.947746f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 4.947746f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.301175 > Game.Music.Time) yield return Wait(launchTime + 5.301175f);
            Player.StartCoroutine(Pattern_20/*OneShot*/(launchTime + 5.301175f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 180f+(float)rndf(-45f,45f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x01/255f,0x5d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.0004730229 > Game.Music.Time) yield return Wait(launchTime + 0.0004730229f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0004730229f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f+(float)rndf(-45f,45f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x01/255f,0x5d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_3.output_patterninput");
            object boss = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*FirstShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 1.223618 > Game.Music.Time) yield return Wait(launchTime + 1.223618f);
            Player.StartCoroutine(Pattern_23/*SecondShot*/(launchTime + 1.223618f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 2.856194 > Game.Music.Time) yield return Wait(launchTime + 2.856194f);
            Player.StartCoroutine(Pattern_22/*FirstShot*/(launchTime + 2.856194f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 4.042236 > Game.Music.Time) yield return Wait(launchTime + 4.042236f);
            Player.StartCoroutine(Pattern_30/*tratata*/(launchTime + 4.042236f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 5.643425 > Game.Music.Time) yield return Wait(launchTime + 5.643425f);
            Player.StartCoroutine(Pattern_24/*Shower*/(launchTime + 5.643425f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 8.446548 > Game.Music.Time) yield return Wait(launchTime + 8.446548f);
            Player.StartCoroutine(Pattern_25/*Zigzag*/(launchTime + 8.446548f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 11.28446 > Game.Music.Time) yield return Wait(launchTime + 11.28446f);
            Player.StartCoroutine(Pattern_26/*TwoLines*/(launchTime + 11.28446f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 16.94085 > Game.Music.Time) yield return Wait(launchTime + 16.94085f);
            Player.StartCoroutine(Pattern_24/*Shower*/(launchTime + 16.94085f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 19.7619 > Game.Music.Time) yield return Wait(launchTime + 19.7619f);
            Player.StartCoroutine(Pattern_29/*Disappear*/(launchTime + 19.7619f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1697159f));
            if (launchTime + 0.006866459 > Game.Music.Time) yield return Wait(launchTime + 0.006866459f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.006866459f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => -45f+90f*((float)i%2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => -400f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.3426361 > Game.Music.Time) yield return Wait(launchTime + 0.3426361f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 0.3426361f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)"/*out*/));
            if (launchTime + 0.5359421 > Game.Music.Time) yield return Wait(launchTime + 0.5359421f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 0.5359421f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)"/*out*/));
            if (launchTime + 0.7058105 > Game.Music.Time) yield return Wait(launchTime + 0.7058105f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 0.7058105f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)"/*out*/));
            if (launchTime + 0.887413 > Game.Music.Time) yield return Wait(launchTime + 0.887413f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 0.887413f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)"/*out*/));
            if (launchTime + 1.056671 > Game.Music.Time) yield return Wait(launchTime + 1.056671f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.056671f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => -400f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.007583619 > Game.Music.Time) yield return Wait(launchTime + 0.007583619f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.007583619f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2220078f));
            if (launchTime + 0.3529435 > Game.Music.Time) yield return Wait(launchTime + 0.3529435f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.3529435f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)i*180f+90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.3531265 > Game.Music.Time) yield return Wait(launchTime + 0.3531265f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3531265f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2045593f));
            if (launchTime + 0.7133483 > Game.Music.Time) yield return Wait(launchTime + 0.7133483f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 0.7133483f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)"/*out*/));
            if (launchTime + 0.89048 > Game.Music.Time) yield return Wait(launchTime + 0.89048f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 0.89048f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)"/*out*/));
            if (launchTime + 1.080734 > Game.Music.Time) yield return Wait(launchTime + 1.080734f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.080734f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)"/*out*/));
            if (launchTime + 1.244736 > Game.Music.Time) yield return Wait(launchTime + 1.244736f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.244736f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)"/*out*/));
            if (launchTime + 1.466843 > Game.Music.Time) yield return Wait(launchTime + 1.466843f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.466843f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2509308 > Game.Music.Time) yield return Wait(launchTime + 0.2509308f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.2509308f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5211945 > Game.Music.Time) yield return Wait(launchTime + 0.5211945f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.5211945f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.776413 > Game.Music.Time) yield return Wait(launchTime + 0.776413f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 0.776413f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.042915 > Game.Music.Time) yield return Wait(launchTime + 1.042915f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.042915f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.226836 > Game.Music.Time) yield return Wait(launchTime + 1.226836f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.226836f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.395728 > Game.Music.Time) yield return Wait(launchTime + 1.395728f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.395728f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.665985 > Game.Music.Time) yield return Wait(launchTime + 1.665985f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.665985f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.924987 > Game.Music.Time) yield return Wait(launchTime + 1.924987f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 1.924987f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.457984 > Game.Music.Time) yield return Wait(launchTime + 2.457984f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.457984f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.653167 > Game.Music.Time) yield return Wait(launchTime + 2.653167f);
            Player.StartCoroutine(Pattern_28/*Shot*/(launchTime + 2.653167f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);

            object bullets;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullets", (c,i,t,l, castc, casti) => "node_3.output_bullets", (c,i,t,l, castc, casti) => "node_5.output_bullets", (c,i,t,l, castc, casti) => "node_6.output_bullets", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_31/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 300f*(((float)ar-1f)/2f + 1f)/*out*/, "node_1.output_bullets"));
            if (launchTime + 0.3607407 > Game.Music.Time) yield return Wait(launchTime + 0.3607407f);
            Player.StartCoroutine(Pattern_31/*Shot*/(launchTime + 0.3607407f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 250f*(((float)ar-1f)/2f + 1f)/*out*/, "node_3.output_bullets"));
            if (launchTime + 0.6944961 > Game.Music.Time) yield return Wait(launchTime + 0.6944961f);
            Player.StartCoroutine(Pattern_31/*Shot*/(launchTime + 0.6944961f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 200f*(((float)ar-1f)/2f + 1f)/*out*/, "node_5.output_bullets"));
            if (launchTime + 1.062142 > Game.Music.Time) yield return Wait(launchTime + 1.062142f);
            Player.StartCoroutine(Pattern_31/*Shot*/(launchTime + 1.062142f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 150f*(((float)ar-1f)/2f + 1f)/*out*/, "node_6.output_bullets"));
            if (launchTime + 1.409569 > Game.Music.Time) yield return Wait(launchTime + 1.409569f);
            Player.StartCoroutine(Pattern_32/*FallDown*/(launchTime + 1.409569f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_33/*Bullets*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_33/*Bullets*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard, InputFunc parent_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, parent_input, "node_0.output_patterninput");
            object parent = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => parent = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x15/255f,0x24/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1319733f));
            if (launchTime + 7.629395E-05 > Game.Music.Time) yield return Wait(launchTime + 7.629395E-05f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 7.629395E-05f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)rndf(0f,100f), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x97/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-45f,45f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.002960205 > Game.Music.Time) yield return Wait(launchTime + 0.002960205f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.002960205f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x10/255f,0x2a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1468964f));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2556381 > Game.Music.Time) yield return Wait(launchTime + 0.2556381f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.2556381f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.51931 > Game.Music.Time) yield return Wait(launchTime + 0.51931f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.51931f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7829665 > Game.Music.Time) yield return Wait(launchTime + 0.7829665f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.7829665f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.046623 > Game.Music.Time) yield return Wait(launchTime + 1.046623f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.046623f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.238334 > Game.Music.Time) yield return Wait(launchTime + 1.238334f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.238334f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.406105 > Game.Music.Time) yield return Wait(launchTime + 1.406105f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.406105f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.426048 > Game.Music.Time) yield return Wait(launchTime + 1.426048f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.426048f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(0f,209f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_9.output_enemy(ies)", 0.6568756f));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3464812 > Game.Music.Time) yield return Wait(launchTime + 0.3464812f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.3464812f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5171967 > Game.Music.Time) yield return Wait(launchTime + 0.5171967f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.5171967f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7267761 > Game.Music.Time) yield return Wait(launchTime + 0.7267761f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.7267761f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8703917 > Game.Music.Time) yield return Wait(launchTime + 0.8703917f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.8703917f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.06443 > Game.Music.Time) yield return Wait(launchTime + 1.06443f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 1.06443f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.246788 > Game.Music.Time) yield return Wait(launchTime + 1.246788f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 1.246788f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.425308 > Game.Music.Time) yield return Wait(launchTime + 1.425308f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 1.425308f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard, InputFunc boss_input, InputFunc speed_input, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);

            SubscribeInput(outboard, blackboard, speed_input, "node_1.output_patterninput");
            object speed = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => speed = o);

            SubscribeOutput(outboard, blackboard, bullets_output, "node_4.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x13/255f,0x24/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3021698f));
            if (launchTime + 0.02072144 > Game.Music.Time) yield return Wait(launchTime + 0.02072144f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.02072144f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 90f+180f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => -200f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x88/255f,0x42/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x10/255f,0x21/255f)/*out*/, 0.03340149f));
            if (launchTime + 0.0215683 > Game.Music.Time) yield return Wait(launchTime + 0.0215683f);
            Player.StartCoroutine(Pattern_35/*Shot*/(launchTime + 0.0215683f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.203598 > Game.Music.Time) yield return Wait(launchTime + 0.203598f);
            Player.StartCoroutine(Pattern_35/*Shot*/(launchTime + 0.203598f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3820572 > Game.Music.Time) yield return Wait(launchTime + 0.3820572f);
            Player.StartCoroutine(Pattern_35/*Shot*/(launchTime + 0.3820572f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5568542 > Game.Music.Time) yield return Wait(launchTime + 0.5568542f);
            Player.StartCoroutine(Pattern_35/*Shot*/(launchTime + 0.5568542f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7047806 > Game.Music.Time) yield return Wait(launchTime + 0.7047806f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.7047806f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x31/255f,0x10/255f,0x30/255f)/*out*/, 0.07032776f));
            if (launchTime + 0.7151947 > Game.Music.Time) yield return Wait(launchTime + 0.7151947f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.7151947f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0.2f/(float)d, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 123f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x2f/255f,0x68/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_bullet(s)", 0.7127075f));
            if (launchTime + 1.460205 > Game.Music.Time) yield return Wait(launchTime + 1.460205f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.460205f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc boss_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, boss_input, "node_2.output_patterninput");
            object boss = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => boss = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);

            object bullets;
            blackboard.SubscribeForChanges("node_10.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_6.output_bullet(s)", (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 5f+4f*(float)d, (c,i,t,l, castc, casti) => new Vector2((float)ar*(-320f+640f/((float)c-1f)*(float)i),260f*(float)flipy), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-1f*(float)flipy), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 2.289005E-05 > Game.Music.Time) yield return Wait(launchTime + 2.289005E-05f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 2.289005E-05f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 5f+4f*(float)d, (c,i,t,l, castc, casti) => new Vector2((float)ar*(-320f+640f/((float)c-1f)*(float)i),280f*(float)flipy), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-1f*(float)flipy), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.0003967294 > Game.Music.Time) yield return Wait(launchTime + 0.0003967294f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.0003967294f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 5f+4f*(float)d, (c,i,t,l, castc, casti) => new Vector2((float)ar*(-320f+640f/((float)c-1f)*(float)i),240f*(float)flipy), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-1f*(float)flipy), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.001586909 > Game.Music.Time) yield return Wait(launchTime + 0.001586909f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.001586909f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x13/255f,0x2a/255f,0x3c/255f)/*out*/, 1.428864f));
            if (launchTime + 0.007972715 > Game.Music.Time) yield return Wait(launchTime + 0.007972715f);
            Player.StartCoroutine(Pattern_34/*MoveAndShoot*/(launchTime + 0.007972715f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.432312 > Game.Music.Time) yield return Wait(launchTime + 1.432312f);
            Player.StartCoroutine(Pattern_34/*MoveAndShoot*/(launchTime + 1.432312f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.432411 > Game.Music.Time) yield return Wait(launchTime + 1.432411f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.432411f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.01279449f));
            if (launchTime + 1.44606 > Game.Music.Time) yield return Wait(launchTime + 1.44606f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.44606f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x33/255f,0x65/255f,0x9c/255f)/*out*/, 1.389351f));
            if (launchTime + 2.834068 > Game.Music.Time) yield return Wait(launchTime + 2.834068f);
            Player.StartCoroutine(Pattern_34/*MoveAndShoot*/(launchTime + 2.834068f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 2.83815 > Game.Music.Time) yield return Wait(launchTime + 2.83815f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.83815f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.01110077f));
            if (launchTime + 2.849854 > Game.Music.Time) yield return Wait(launchTime + 2.849854f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.849854f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x9f/255f,0xc7/255f,0xff/255f)/*out*/, 1.389618f));
            if (launchTime + 4.240792 > Game.Music.Time) yield return Wait(launchTime + 4.240792f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.240792f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x28/255f,0x19/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.02003479f));
            if (launchTime + 4.241676 > Game.Music.Time) yield return Wait(launchTime + 4.241676f);
            Player.StartCoroutine(Pattern_34/*MoveAndShoot*/(launchTime + 4.241676f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 4.268769 > Game.Music.Time) yield return Wait(launchTime + 4.268769f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.268769f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 1.390602f));
            if (launchTime + 5.703224 > Game.Music.Time) yield return Wait(launchTime + 5.703224f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 5.703224f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard, InputFunc allbullets_input, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, allbullets_input, "node_0.output_patterninput");
            object allbullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => allbullets = o);

            SubscribeInput(outboard, blackboard, boss_input, "node_1.output_patterninput");
            object boss = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 130f, (c,i,t,l, castc, casti) => -150f, (c,i,t,l, castc, casti) => 0.01f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.0004272459 > Game.Music.Time) yield return Wait(launchTime + 0.0004272459f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.0004272459f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 0.4f/(float)d, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-20f,20f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x5a/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)", 1.392624f));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x2f/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 10f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x2d/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -10f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 10f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x16/255f,0x2c/255f,0x2a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1217499f));
            if (launchTime + 0.003555298 > Game.Music.Time) yield return Wait(launchTime + 0.003555298f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.003555298f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f+10f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x49/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1295033f));
            if (launchTime + 0.1804352 > Game.Music.Time) yield return Wait(launchTime + 0.1804352f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.1804352f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1332626f));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x33/255f,0x26/255f,0x13/255f)/*out*/, 0.04656792f));
            if (launchTime + 0.1725178 > Game.Music.Time) yield return Wait(launchTime + 0.1725178f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.1725178f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2c/255f,0x1f/255f,0x13/255f)/*out*/, 0.05536079f));
            if (launchTime + 0.3626461 > Game.Music.Time) yield return Wait(launchTime + 0.3626461f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3626461f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x25/255f,0x1b/255f,0x13/255f)/*out*/, 0.03952026f));
            if (launchTime + 0.5439663 > Game.Music.Time) yield return Wait(launchTime + 0.5439663f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5439663f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x1c/255f,0x17/255f,0x13/255f)/*out*/, 0.05536461f));
            if (launchTime + 1.237564 > Game.Music.Time) yield return Wait(launchTime + 1.237564f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.237564f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x15/255f,0x13/255f)/*out*/, 0.04656029f));
            if (launchTime + 1.954039 > Game.Music.Time) yield return Wait(launchTime + 1.954039f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.954039f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x11/255f,0x11/255f,0x0c/255f)/*out*/, 0.05184186f));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x23/255f,0x2d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1791649f));
            yield break;
        }

        private IEnumerator Pattern_41(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.3422356 > Game.Music.Time) yield return Wait(launchTime + 0.3422356f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 0.3422356f, blackboard/*out*/));
            if (launchTime + 0.6976318 > Game.Music.Time) yield return Wait(launchTime + 0.6976318f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 0.6976318f, blackboard/*out*/));
            if (launchTime + 1.053035 > Game.Music.Time) yield return Wait(launchTime + 1.053035f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 1.053035f, blackboard/*out*/));
            if (launchTime + 1.408436 > Game.Music.Time) yield return Wait(launchTime + 1.408436f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 1.408436f, blackboard/*out*/));
            if (launchTime + 1.760544 > Game.Music.Time) yield return Wait(launchTime + 1.760544f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 1.760544f, blackboard/*out*/));
            if (launchTime + 2.102783 > Game.Music.Time) yield return Wait(launchTime + 2.102783f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 2.102783f, blackboard/*out*/));
            if (launchTime + 2.468055 > Game.Music.Time) yield return Wait(launchTime + 2.468055f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 2.468055f, blackboard/*out*/));
            if (launchTime + 2.813583 > Game.Music.Time) yield return Wait(launchTime + 2.813583f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 2.813583f, blackboard/*out*/));
            if (launchTime + 3.178856 > Game.Music.Time) yield return Wait(launchTime + 3.178856f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 3.178856f, blackboard/*out*/));
            if (launchTime + 3.524383 > Game.Music.Time) yield return Wait(launchTime + 3.524383f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 3.524383f, blackboard/*out*/));
            if (launchTime + 3.876495 > Game.Music.Time) yield return Wait(launchTime + 3.876495f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 3.876495f, blackboard/*out*/));
            if (launchTime + 4.222023 > Game.Music.Time) yield return Wait(launchTime + 4.222023f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 4.222023f, blackboard/*out*/));
            if (launchTime + 4.590587 > Game.Music.Time) yield return Wait(launchTime + 4.590587f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 4.590587f, blackboard/*out*/));
            if (launchTime + 4.936115 > Game.Music.Time) yield return Wait(launchTime + 4.936115f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 4.936115f, blackboard/*out*/));
            if (launchTime + 5.288227 > Game.Music.Time) yield return Wait(launchTime + 5.288227f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 5.288227f, blackboard/*out*/));
            if (launchTime + 5.6502 > Game.Music.Time) yield return Wait(launchTime + 5.6502f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 5.6502f, blackboard/*out*/));
            if (launchTime + 5.995731 > Game.Music.Time) yield return Wait(launchTime + 5.995731f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 5.995731f, blackboard/*out*/));
            if (launchTime + 6.347839 > Game.Music.Time) yield return Wait(launchTime + 6.347839f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 6.347839f, blackboard/*out*/));
            if (launchTime + 6.696659 > Game.Music.Time) yield return Wait(launchTime + 6.696659f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 6.696659f, blackboard/*out*/));
            if (launchTime + 7.04219 > Game.Music.Time) yield return Wait(launchTime + 7.04219f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 7.04219f, blackboard/*out*/));
            if (launchTime + 7.407463 > Game.Music.Time) yield return Wait(launchTime + 7.407463f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 7.407463f, blackboard/*out*/));
            if (launchTime + 7.759571 > Game.Music.Time) yield return Wait(launchTime + 7.759571f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 7.759571f, blackboard/*out*/));
            if (launchTime + 8.114975 > Game.Music.Time) yield return Wait(launchTime + 8.114975f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 8.114975f, blackboard/*out*/));
            if (launchTime + 8.470371 > Game.Music.Time) yield return Wait(launchTime + 8.470371f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 8.470371f, blackboard/*out*/));
            if (launchTime + 8.819191 > Game.Music.Time) yield return Wait(launchTime + 8.819191f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 8.819191f, blackboard/*out*/));
            if (launchTime + 9.174591 > Game.Music.Time) yield return Wait(launchTime + 9.174591f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 9.174591f, blackboard/*out*/));
            if (launchTime + 9.520119 > Game.Music.Time) yield return Wait(launchTime + 9.520119f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 9.520119f, blackboard/*out*/));
            if (launchTime + 9.868938 > Game.Music.Time) yield return Wait(launchTime + 9.868938f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 9.868938f, blackboard/*out*/));
            if (launchTime + 10.22434 > Game.Music.Time) yield return Wait(launchTime + 10.22434f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 10.22434f, blackboard/*out*/));
            if (launchTime + 10.58303 > Game.Music.Time) yield return Wait(launchTime + 10.58303f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 10.58303f, blackboard/*out*/));
            if (launchTime + 10.93185 > Game.Music.Time) yield return Wait(launchTime + 10.93185f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 10.93185f, blackboard/*out*/));
            if (launchTime + 11.28395 > Game.Music.Time) yield return Wait(launchTime + 11.28395f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 11.28395f, blackboard/*out*/));
            if (launchTime + 11.63935 > Game.Music.Time) yield return Wait(launchTime + 11.63935f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 11.63935f, blackboard/*out*/));
            if (launchTime + 11.98489 > Game.Music.Time) yield return Wait(launchTime + 11.98489f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 11.98489f, blackboard/*out*/));
            if (launchTime + 12.34358 > Game.Music.Time) yield return Wait(launchTime + 12.34358f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 12.34358f, blackboard/*out*/));
            if (launchTime + 12.68911 > Game.Music.Time) yield return Wait(launchTime + 12.68911f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 12.68911f, blackboard/*out*/));
            if (launchTime + 13.03793 > Game.Music.Time) yield return Wait(launchTime + 13.03793f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 13.03793f, blackboard/*out*/));
            if (launchTime + 13.4032 > Game.Music.Time) yield return Wait(launchTime + 13.4032f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 13.4032f, blackboard/*out*/));
            if (launchTime + 13.76518 > Game.Music.Time) yield return Wait(launchTime + 13.76518f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 13.76518f, blackboard/*out*/));
            if (launchTime + 14.10084 > Game.Music.Time) yield return Wait(launchTime + 14.10084f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 14.10084f, blackboard/*out*/));
            if (launchTime + 14.45953 > Game.Music.Time) yield return Wait(launchTime + 14.45953f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 14.45953f, blackboard/*out*/));
            if (launchTime + 14.81493 > Game.Music.Time) yield return Wait(launchTime + 14.81493f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 14.81493f, blackboard/*out*/));
            if (launchTime + 15.17691 > Game.Music.Time) yield return Wait(launchTime + 15.17691f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 15.17691f, blackboard/*out*/));
            if (launchTime + 15.51586 > Game.Music.Time) yield return Wait(launchTime + 15.51586f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 15.51586f, blackboard/*out*/));
            if (launchTime + 15.88442 > Game.Music.Time) yield return Wait(launchTime + 15.88442f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 15.88442f, blackboard/*out*/));
            if (launchTime + 16.23653 > Game.Music.Time) yield return Wait(launchTime + 16.23653f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 16.23653f, blackboard/*out*/));
            if (launchTime + 16.58865 > Game.Music.Time) yield return Wait(launchTime + 16.58865f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 16.58865f, blackboard/*out*/));
            if (launchTime + 16.93089 > Game.Music.Time) yield return Wait(launchTime + 16.93089f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 16.93089f, blackboard/*out*/));
            if (launchTime + 17.28958 > Game.Music.Time) yield return Wait(launchTime + 17.28958f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 17.28958f, blackboard/*out*/));
            if (launchTime + 17.6351 > Game.Music.Time) yield return Wait(launchTime + 17.6351f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 17.6351f, blackboard/*out*/));
            if (launchTime + 17.9905 > Game.Music.Time) yield return Wait(launchTime + 17.9905f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 17.9905f, blackboard/*out*/));
            if (launchTime + 18.3492 > Game.Music.Time) yield return Wait(launchTime + 18.3492f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 18.3492f, blackboard/*out*/));
            if (launchTime + 18.69801 > Game.Music.Time) yield return Wait(launchTime + 18.69801f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 18.69801f, blackboard/*out*/));
            if (launchTime + 19.05013 > Game.Music.Time) yield return Wait(launchTime + 19.05013f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 19.05013f, blackboard/*out*/));
            if (launchTime + 19.40224 > Game.Music.Time) yield return Wait(launchTime + 19.40224f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 19.40224f, blackboard/*out*/));
            if (launchTime + 19.76093 > Game.Music.Time) yield return Wait(launchTime + 19.76093f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 19.76093f, blackboard/*out*/));
            if (launchTime + 20.10646 > Game.Music.Time) yield return Wait(launchTime + 20.10646f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 20.10646f, blackboard/*out*/));
            if (launchTime + 20.47173 > Game.Music.Time) yield return Wait(launchTime + 20.47173f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 20.47173f, blackboard/*out*/));
            if (launchTime + 20.82384 > Game.Music.Time) yield return Wait(launchTime + 20.82384f);
            Player.StartCoroutine(Pattern_40/*Beat*/(launchTime + 20.82384f, blackboard/*out*/));
            if (launchTime + 21.16609 > Game.Music.Time) yield return Wait(launchTime + 21.16609f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.16609f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x14/255f,0x12/255f)/*out*/, 1.413223f));
            yield break;
        }

        private IEnumerator Pattern_42(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 170f/*out*/));
            if (launchTime + 11.30813 > Game.Music.Time) yield return Wait(launchTime + 11.30813f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 11.30813f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 85f/*out*/));
            if (launchTime + 22.62496 > Game.Music.Time) yield return Wait(launchTime + 22.62496f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 22.62496f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 33.93622 > Game.Music.Time) yield return Wait(launchTime + 33.93622f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 33.93622f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 170f/*out*/));
            if (launchTime + 39.55976 > Game.Music.Time) yield return Wait(launchTime + 39.55976f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 39.55976f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 45.19418 > Game.Music.Time) yield return Wait(launchTime + 45.19418f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 45.19418f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 50.82858 > Game.Music.Time) yield return Wait(launchTime + 50.82858f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 50.82858f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 55.09573 > Game.Music.Time) yield return Wait(launchTime + 55.09573f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 55.09573f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 340f/*out*/));
            if (launchTime + 56.47402 > Game.Music.Time) yield return Wait(launchTime + 56.47402f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 56.47402f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 85f/*out*/));
            if (launchTime + 62.12285 > Game.Music.Time) yield return Wait(launchTime + 62.12285f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 62.12285f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 170f/*out*/));
            if (launchTime + 67.81766 > Game.Music.Time) yield return Wait(launchTime + 67.81766f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 67.81766f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 85f/*out*/));
            if (launchTime + 73.42204 > Game.Music.Time) yield return Wait(launchTime + 73.42204f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 73.42204f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 170f/*out*/));
            if (launchTime + 79.07813 > Game.Music.Time) yield return Wait(launchTime + 79.07813f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 79.07813f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 85f/*out*/));
            if (launchTime + 81.86736 > Game.Music.Time) yield return Wait(launchTime + 81.86736f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 81.86736f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 83.15869 > Game.Music.Time) yield return Wait(launchTime + 83.15869f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 83.15869f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 84.73415 > Game.Music.Time) yield return Wait(launchTime + 84.73415f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 84.73415f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 87.57507 > Game.Music.Time) yield return Wait(launchTime + 87.57507f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 87.57507f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 90.3902 > Game.Music.Time) yield return Wait(launchTime + 90.3902f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 90.3902f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 91.83649 > Game.Music.Time) yield return Wait(launchTime + 91.83649f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 91.83649f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 170f/*out*/));
            if (launchTime + 93.23113 > Game.Music.Time) yield return Wait(launchTime + 93.23113f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 93.23113f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 340f/*out*/));
            if (launchTime + 94.63867 > Game.Music.Time) yield return Wait(launchTime + 94.63867f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 94.63867f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 680f/*out*/));
            if (launchTime + 96.04623 > Game.Music.Time) yield return Wait(launchTime + 96.04623f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 96.04623f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 170f/*out*/));
            if (launchTime + 98.73223 > Game.Music.Time) yield return Wait(launchTime + 98.73223f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 98.73223f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 100.2441 > Game.Music.Time) yield return Wait(launchTime + 100.2441f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 100.2441f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_43(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.347023 > Game.Music.Time) yield return Wait(launchTime + 0.347023f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 0.347023f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.707714 > Game.Music.Time) yield return Wait(launchTime + 0.707714f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 0.707714f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.053749 > Game.Music.Time) yield return Wait(launchTime + 1.053749f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 1.053749f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.40894 > Game.Music.Time) yield return Wait(launchTime + 1.40894f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 1.40894f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.76413 > Game.Music.Time) yield return Wait(launchTime + 1.76413f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 1.76413f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.112049 > Game.Music.Time) yield return Wait(launchTime + 2.112049f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 2.112049f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.472088 > Game.Music.Time) yield return Wait(launchTime + 2.472088f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 2.472088f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.815876 > Game.Music.Time) yield return Wait(launchTime + 2.815876f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 2.815876f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.173218 > Game.Music.Time) yield return Wait(launchTime + 3.173218f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 3.173218f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.527844 > Game.Music.Time) yield return Wait(launchTime + 3.527844f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 3.527844f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.868931 > Game.Music.Time) yield return Wait(launchTime + 3.868931f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 3.868931f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.226264 > Game.Music.Time) yield return Wait(launchTime + 4.226264f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 4.226264f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.578197 > Game.Music.Time) yield return Wait(launchTime + 4.578197f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 4.578197f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.932816 > Game.Music.Time) yield return Wait(launchTime + 4.932816f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 4.932816f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.30865 > Game.Music.Time) yield return Wait(launchTime + 5.30865f);
            Player.StartCoroutine(Pattern_44/*Shot*/(launchTime + 5.30865f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_44(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(45f,120f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x01/255f,0x5d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.001831055 > Game.Music.Time) yield return Wait(launchTime + 0.001831055f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.001831055f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(-120f,-45f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x01/255f,0x5d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_45(float launchTime, Blackboard outboard, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, boss_input, "node_0.output_patterninput");
            object boss = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => boss = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 10f*(float)d, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => (float)rndf(20f,30f), (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x1b/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_46(float launchTime, Blackboard outboard, InputFunc angle_input, InputFunc boss_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, angle_input, "node_0.output_patterninput");
            object angle = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => angle = o);

            SubscribeInput(outboard, blackboard, boss_input, "node_1.output_patterninput");
            object boss = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => boss = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => (float)d*4f, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => 30f, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f/(float)c*((float)i+1f)+100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x33/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

    }
}