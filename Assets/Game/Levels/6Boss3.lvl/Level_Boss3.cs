#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_Boss3 : LevelScript
    {
        protected override string ClipName {
            get { return "Boss3.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0.02902813 > Game.Music.Time) yield return Wait(launchTime + 0.02902813f);
            Player.StartCoroutine(BossHealth.Act(launchTime + 0.02902813f, blackboard, (c,i,t,l, castc, casti) => "Defective Builder", (c,i,t,l, castc, casti) => 300000f, (c,i,t,l, castc, casti) => true/*out*/));
            if (launchTime + 0.03202045 > Game.Music.Time) yield return Wait(launchTime + 0.03202045f);
            Player.StartCoroutine(Pattern_38/*DanceMan*/(launchTime + 0.03202045f, blackboard/*out*/));
            if (launchTime + 0.03654655 > Game.Music.Time) yield return Wait(launchTime + 0.03654655f);
            Player.StartCoroutine(Pattern_1/*Part1*/(launchTime + 0.03654655f, blackboard/*out*/));
            if (launchTime + 0.03827659 > Game.Music.Time) yield return Wait(launchTime + 0.03827659f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.03827659f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 1.841391f));
            if (launchTime + 6.401751 > Game.Music.Time) yield return Wait(launchTime + 6.401751f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.401751f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1091423f));
            if (launchTime + 6.418489 > Game.Music.Time) yield return Wait(launchTime + 6.418489f);
            Player.StartCoroutine(Pattern_26/*Transition0*/(launchTime + 6.418489f, blackboard/*out*/));
            if (launchTime + 6.608068 > Game.Music.Time) yield return Wait(launchTime + 6.608068f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.608068f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1050162f));
            if (launchTime + 6.802007 > Game.Music.Time) yield return Wait(launchTime + 6.802007f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.802007f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1091433f));
            if (launchTime + 6.995944 > Game.Music.Time) yield return Wait(launchTime + 6.995944f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.995944f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1050172f));
            if (launchTime + 7.19401 > Game.Music.Time) yield return Wait(launchTime + 7.19401f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.19401f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09676409f));
            if (launchTime + 7.313674 > Game.Music.Time) yield return Wait(launchTime + 7.313674f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.313674f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.08025837f));
            if (launchTime + 7.404453 > Game.Music.Time) yield return Wait(launchTime + 7.404453f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.404453f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.08438492f));
            if (launchTime + 7.507613 > Game.Music.Time) yield return Wait(launchTime + 7.507613f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.507613f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09263754f));
            if (launchTime + 7.613906 > Game.Music.Time) yield return Wait(launchTime + 7.613906f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.613906f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x12/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 1.519594f));
            if (launchTime + 7.619504 > Game.Music.Time) yield return Wait(launchTime + 7.619504f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 7.619504f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1300926f));
            if (launchTime + 8.039243 > Game.Music.Time) yield return Wait(launchTime + 8.039243f);
            Player.StartCoroutine(Pattern_3/*Part2*/(launchTime + 8.039243f, blackboard/*out*/));
            if (launchTime + 20.81366 > Game.Music.Time) yield return Wait(launchTime + 20.81366f);
            Player.StartCoroutine(Pattern_7/*Transition1*/(launchTime + 20.81366f, blackboard/*out*/));
            if (launchTime + 22.41266 > Game.Music.Time) yield return Wait(launchTime + 22.41266f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 22.41266f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1157665f));
            if (launchTime + 22.41306 > Game.Music.Time) yield return Wait(launchTime + 22.41306f);
            Player.StartCoroutine(Pattern_9/*Part3*/(launchTime + 22.41306f, blackboard/*out*/));
            if (launchTime + 25.61087 > Game.Music.Time) yield return Wait(launchTime + 25.61087f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 25.61087f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1788921f));
            if (launchTime + 28.75165 > Game.Music.Time) yield return Wait(launchTime + 28.75165f);
            Player.StartCoroutine(Pattern_12/*Part4_1*/(launchTime + 28.75165f, blackboard/*out*/));
            if (launchTime + 35.24033 > Game.Music.Time) yield return Wait(launchTime + 35.24033f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 35.24033f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2823105f));
            if (launchTime + 35.24064 > Game.Music.Time) yield return Wait(launchTime + 35.24064f);
            Player.StartCoroutine(Pattern_9/*Part3*/(launchTime + 35.24064f, blackboard/*out*/));
            if (launchTime + 38.43303 > Game.Music.Time) yield return Wait(launchTime + 38.43303f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 38.43303f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2823105f));
            if (launchTime + 41.6452 > Game.Music.Time) yield return Wait(launchTime + 41.6452f);
            Player.StartCoroutine(Pattern_16/*Part4_2*/(launchTime + 41.6452f, blackboard/*out*/));
            if (launchTime + 48.01838 > Game.Music.Time) yield return Wait(launchTime + 48.01838f);
            Player.StartCoroutine(Pattern_32/*Part5*/(launchTime + 48.01838f, blackboard/*out*/));
            if (launchTime + 59.19669 > Game.Music.Time) yield return Wait(launchTime + 59.19669f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 59.19669f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2546959f));
            if (launchTime + 59.51686 > Game.Music.Time) yield return Wait(launchTime + 59.51686f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 59.51686f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2000923f));
            if (launchTime + 59.81182 > Game.Music.Time) yield return Wait(launchTime + 59.81182f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 59.81182f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1648788f));
            if (launchTime + 60.01873 > Game.Music.Time) yield return Wait(launchTime + 60.01873f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 60.01873f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2000923f));
            if (launchTime + 60.30929 > Game.Music.Time) yield return Wait(launchTime + 60.30929f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 60.30929f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2000923f));
            if (launchTime + 60.61307 > Game.Music.Time) yield return Wait(launchTime + 60.61307f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 60.61307f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2000923f));
            if (launchTime + 60.83699 > Game.Music.Time) yield return Wait(launchTime + 60.83699f);
            Player.StartCoroutine(Pattern_18/*Part6*/(launchTime + 60.83699f, blackboard/*out*/));
            if (launchTime + 61.74711 > Game.Music.Time) yield return Wait(launchTime + 61.74711f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 61.74711f, blackboard/*out*/));
            if (launchTime + 63.35513 > Game.Music.Time) yield return Wait(launchTime + 63.35513f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 63.35513f, blackboard/*out*/));
            if (launchTime + 64.93077 > Game.Music.Time) yield return Wait(launchTime + 64.93077f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 64.93077f, blackboard/*out*/));
            if (launchTime + 66.53498 > Game.Music.Time) yield return Wait(launchTime + 66.53498f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 66.53498f, blackboard/*out*/));
            if (launchTime + 68.1454 > Game.Music.Time) yield return Wait(launchTime + 68.1454f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 68.1454f, blackboard/*out*/));
            if (launchTime + 69.70629 > Game.Music.Time) yield return Wait(launchTime + 69.70629f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 69.70629f, blackboard/*out*/));
            if (launchTime + 71.31664 > Game.Music.Time) yield return Wait(launchTime + 71.31664f);
            Player.StartCoroutine(Pattern_37/*Wind*/(launchTime + 71.31664f, blackboard/*out*/));
            if (launchTime + 72.81272 > Game.Music.Time) yield return Wait(launchTime + 72.81272f);
            Player.StartCoroutine(Pattern_21/*Transition6*/(launchTime + 72.81272f, blackboard/*out*/));
            if (launchTime + 73.7424 > Game.Music.Time) yield return Wait(launchTime + 73.7424f);
            Player.StartCoroutine(Pattern_9/*Part3*/(launchTime + 73.7424f, blackboard/*out*/));
            if (launchTime + 79.93915 > Game.Music.Time) yield return Wait(launchTime + 79.93915f);
            Player.StartCoroutine(Pattern_16/*Part4_2*/(launchTime + 79.93915f, blackboard/*out*/));
            if (launchTime + 86.5181 > Game.Music.Time) yield return Wait(launchTime + 86.5181f);
            Player.StartCoroutine(Pattern_9/*Part3*/(launchTime + 86.5181f, blackboard/*out*/));
            if (launchTime + 92.83339 > Game.Music.Time) yield return Wait(launchTime + 92.83339f);
            Player.StartCoroutine(Pattern_16/*Part4_2*/(launchTime + 92.83339f, blackboard/*out*/));
            if (launchTime + 98.40782 > Game.Music.Time) yield return Wait(launchTime + 98.40782f);
            Player.StartCoroutine(Pattern_23/*Transition7*/(launchTime + 98.40782f, blackboard/*out*/));
            if (launchTime + 98.4227 > Game.Music.Time) yield return Wait(launchTime + 98.4227f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 98.4227f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 1.799278f));
            if (launchTime + 99.23198 > Game.Music.Time) yield return Wait(launchTime + 99.23198f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 99.23198f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.7109222f));
            if (launchTime + 99.24162 > Game.Music.Time) yield return Wait(launchTime + 99.24162f);
            Player.StartCoroutine(Pattern_28/*Part7*/(launchTime + 99.24162f, blackboard/*out*/));
            if (launchTime + 100.8036 > Game.Music.Time) yield return Wait(launchTime + 100.8036f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 100.8036f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.7109528f));
            if (launchTime + 102.4348 > Game.Music.Time) yield return Wait(launchTime + 102.4348f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 102.4348f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.7333069f));
            if (launchTime + 104.0064 > Game.Music.Time) yield return Wait(launchTime + 104.0064f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 104.0064f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.6513977f));
            if (launchTime + 105.6004 > Game.Music.Time) yield return Wait(launchTime + 105.6004f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 105.6004f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8003159f));
            if (launchTime + 107.2241 > Game.Music.Time) yield return Wait(launchTime + 107.2241f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 107.2241f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.636467f));
            if (launchTime + 108.8032 > Game.Music.Time) yield return Wait(launchTime + 108.8032f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 108.8032f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.7454453f));
            if (launchTime + 110.3971 > Game.Music.Time) yield return Wait(launchTime + 110.3971f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 110.3971f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x44/255f,0x75/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.675621f));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*Wave*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.007456537 > Game.Music.Time) yield return Wait(launchTime + 0.007456537f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 0.007456537f, blackboard/*out*/));
            if (launchTime + 0.1094158 > Game.Music.Time) yield return Wait(launchTime + 0.1094158f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 0.1094158f, blackboard/*out*/));
            if (launchTime + 0.2051111 > Game.Music.Time) yield return Wait(launchTime + 0.2051111f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 0.2051111f, blackboard/*out*/));
            if (launchTime + 0.4094795 > Game.Music.Time) yield return Wait(launchTime + 0.4094795f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 0.4094795f, blackboard/*out*/));
            if (launchTime + 0.8149734 > Game.Music.Time) yield return Wait(launchTime + 0.8149734f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 0.8149734f, blackboard/*out*/));
            if (launchTime + 1.213978 > Game.Music.Time) yield return Wait(launchTime + 1.213978f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 1.213978f, blackboard/*out*/));
            if (launchTime + 1.612984 > Game.Music.Time) yield return Wait(launchTime + 1.612984f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 1.612984f, blackboard/*out*/));
            if (launchTime + 1.814109 > Game.Music.Time) yield return Wait(launchTime + 1.814109f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 1.814109f, blackboard/*out*/));
            if (launchTime + 1.906562 > Game.Music.Time) yield return Wait(launchTime + 1.906562f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 1.906562f, blackboard/*out*/));
            if (launchTime + 2.002258 > Game.Music.Time) yield return Wait(launchTime + 2.002258f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 2.002258f, blackboard/*out*/));
            if (launchTime + 2.112552 > Game.Music.Time) yield return Wait(launchTime + 2.112552f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 2.112552f, blackboard/*out*/));
            if (launchTime + 2.214736 > Game.Music.Time) yield return Wait(launchTime + 2.214736f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 2.214736f, blackboard/*out*/));
            if (launchTime + 2.407752 > Game.Music.Time) yield return Wait(launchTime + 2.407752f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 2.407752f, blackboard/*out*/));
            if (launchTime + 2.808379 > Game.Music.Time) yield return Wait(launchTime + 2.808379f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 2.808379f, blackboard/*out*/));
            if (launchTime + 3.004639 > Game.Music.Time) yield return Wait(launchTime + 3.004639f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 3.004639f, blackboard/*out*/));
            if (launchTime + 3.207385 > Game.Music.Time) yield return Wait(launchTime + 3.207385f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 3.207385f, blackboard/*out*/));
            if (launchTime + 3.411754 > Game.Music.Time) yield return Wait(launchTime + 3.411754f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 3.411754f, blackboard/*out*/));
            if (launchTime + 3.606391 > Game.Music.Time) yield return Wait(launchTime + 3.606391f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 3.606391f, blackboard/*out*/));
            if (launchTime + 3.809137 > Game.Music.Time) yield return Wait(launchTime + 3.809137f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 3.809137f, blackboard/*out*/));
            if (launchTime + 4.007018 > Game.Music.Time) yield return Wait(launchTime + 4.007018f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 4.007018f, blackboard/*out*/));
            if (launchTime + 4.217875 > Game.Music.Time) yield return Wait(launchTime + 4.217875f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 4.217875f, blackboard/*out*/));
            if (launchTime + 4.414135 > Game.Music.Time) yield return Wait(launchTime + 4.414135f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 4.414135f, blackboard/*out*/));
            if (launchTime + 4.608772 > Game.Music.Time) yield return Wait(launchTime + 4.608772f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 4.608772f, blackboard/*out*/));
            if (launchTime + 4.710956 > Game.Music.Time) yield return Wait(launchTime + 4.710956f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 4.710956f, blackboard/*out*/));
            if (launchTime + 4.814023 > Game.Music.Time) yield return Wait(launchTime + 4.814023f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 4.814023f, blackboard/*out*/));
            if (launchTime + 5.009606 > Game.Music.Time) yield return Wait(launchTime + 5.009606f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 5.009606f, blackboard/*out*/));
            if (launchTime + 5.207507 > Game.Music.Time) yield return Wait(launchTime + 5.207507f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 5.207507f, blackboard/*out*/));
            if (launchTime + 5.407948 > Game.Music.Time) yield return Wait(launchTime + 5.407948f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 5.407948f, blackboard/*out*/));
            if (launchTime + 5.61389 > Game.Music.Time) yield return Wait(launchTime + 5.61389f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 5.61389f, blackboard/*out*/));
            if (launchTime + 5.811362 > Game.Music.Time) yield return Wait(launchTime + 5.811362f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 5.811362f, blackboard/*out*/));
            if (launchTime + 6.01307 > Game.Music.Time) yield return Wait(launchTime + 6.01307f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 6.01307f, blackboard/*out*/));
            if (launchTime + 6.210974 > Game.Music.Time) yield return Wait(launchTime + 6.210974f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 6.210974f, blackboard/*out*/));
            if (launchTime + 6.506557 > Game.Music.Time) yield return Wait(launchTime + 6.506557f);
            Player.StartCoroutine(Pattern_2/*Shot*/(launchTime + 6.506557f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object ypos = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -50f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 50f));
            blackboard.SetValue("node_1.output_value", ypos);

            object xpos = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -300f*(float)ar), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 300f*(float)ar));
            blackboard.SetValue("node_2.output_value", xpos);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)xpos,140f+(float)ypos), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x10/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.3034338 > Game.Music.Time) yield return Wait(launchTime + 0.3034338f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.3034338f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)xpos,140f+(float)ypos), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x10/255f), (c,i,t,l, castc, casti) => 0.75f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.6083676 > Game.Music.Time) yield return Wait(launchTime + 0.6083676f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.6083676f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)xpos,140f+(float)ypos), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x10/255f), (c,i,t,l, castc, casti) => 0.5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.9343828 > Game.Music.Time) yield return Wait(launchTime + 0.9343828f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.9343828f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)xpos,140f+(float)ypos), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x72/255f,0x10/255f), (c,i,t,l, castc, casti) => 0.25f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_4/*Melody*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 4.787185 > Game.Music.Time) yield return Wait(launchTime + 4.787185f);
            Player.StartCoroutine(Pattern_6/*Drumroll*/(launchTime + 4.787185f, blackboard/*out*/));
            if (launchTime + 6.394752 > Game.Music.Time) yield return Wait(launchTime + 6.394752f);
            Player.StartCoroutine(Pattern_4/*Melody*/(launchTime + 6.394752f, blackboard/*out*/));
            if (launchTime + 11.18779 > Game.Music.Time) yield return Wait(launchTime + 11.18779f);
            Player.StartCoroutine(Pattern_6/*Drumroll*/(launchTime + 11.18779f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.09744263 > Game.Music.Time) yield return Wait(launchTime + 0.09744263f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0.09744263f, blackboard/*out*/));
            if (launchTime + 0.4094315 > Game.Music.Time) yield return Wait(launchTime + 0.4094315f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0.4094315f, blackboard/*out*/));
            if (launchTime + 0.817421 > Game.Music.Time) yield return Wait(launchTime + 0.817421f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 0.817421f, blackboard/*out*/));
            if (launchTime + 1.207412 > Game.Music.Time) yield return Wait(launchTime + 1.207412f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.207412f, blackboard/*out*/));
            if (launchTime + 1.617401 > Game.Music.Time) yield return Wait(launchTime + 1.617401f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.617401f, blackboard/*out*/));
            if (launchTime + 1.807395 > Game.Music.Time) yield return Wait(launchTime + 1.807395f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.807395f, blackboard/*out*/));
            if (launchTime + 1.905578 > Game.Music.Time) yield return Wait(launchTime + 1.905578f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 1.905578f, blackboard/*out*/));
            if (launchTime + 2.001937 > Game.Music.Time) yield return Wait(launchTime + 2.001937f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.001937f, blackboard/*out*/));
            if (launchTime + 2.114662 > Game.Music.Time) yield return Wait(launchTime + 2.114662f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.114662f, blackboard/*out*/));
            if (launchTime + 2.21466 > Game.Music.Time) yield return Wait(launchTime + 2.21466f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.21466f, blackboard/*out*/));
            if (launchTime + 2.407386 > Game.Music.Time) yield return Wait(launchTime + 2.407386f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.407386f, blackboard/*out*/));
            if (launchTime + 2.801917 > Game.Music.Time) yield return Wait(launchTime + 2.801917f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 2.801917f, blackboard/*out*/));
            if (launchTime + 3.001914 > Game.Music.Time) yield return Wait(launchTime + 3.001914f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.001914f, blackboard/*out*/));
            if (launchTime + 3.209182 > Game.Music.Time) yield return Wait(launchTime + 3.209182f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.209182f, blackboard/*out*/));
            if (launchTime + 3.40554 > Game.Music.Time) yield return Wait(launchTime + 3.40554f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.40554f, blackboard/*out*/));
            if (launchTime + 3.59925 > Game.Music.Time) yield return Wait(launchTime + 3.59925f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.59925f, blackboard/*out*/));
            if (launchTime + 3.80471 > Game.Music.Time) yield return Wait(launchTime + 3.80471f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 3.80471f, blackboard/*out*/));
            if (launchTime + 4.009651 > Game.Music.Time) yield return Wait(launchTime + 4.009651f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 4.009651f, blackboard/*out*/));
            if (launchTime + 4.201385 > Game.Music.Time) yield return Wait(launchTime + 4.201385f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 4.201385f, blackboard/*out*/));
            if (launchTime + 4.409646 > Game.Music.Time) yield return Wait(launchTime + 4.409646f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 4.409646f, blackboard/*out*/));
            if (launchTime + 4.611297 > Game.Music.Time) yield return Wait(launchTime + 4.611297f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 4.611297f, blackboard/*out*/));
            if (launchTime + 5.012939 > Game.Music.Time) yield return Wait(launchTime + 5.012939f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 5.012939f, blackboard/*out*/));
            if (launchTime + 5.409629 > Game.Music.Time) yield return Wait(launchTime + 5.409629f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 5.409629f, blackboard/*out*/));
            if (launchTime + 5.806304 > Game.Music.Time) yield return Wait(launchTime + 5.806304f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 5.806304f, blackboard/*out*/));
            if (launchTime + 6.001342 > Game.Music.Time) yield return Wait(launchTime + 6.001342f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.001342f, blackboard/*out*/));
            if (launchTime + 6.214561 > Game.Music.Time) yield return Wait(launchTime + 6.214561f);
            Player.StartCoroutine(Pattern_5/*Shot*/(launchTime + 6.214561f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-20f-3f*(float)d,20f+3f*(float)d), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x65/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.001664162 > Game.Music.Time) yield return Wait(launchTime + 0.001664162f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.001664162f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2b/255f,0x14/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1949167f));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08143997f));
            if (launchTime + 0.02181631 > Game.Music.Time) yield return Wait(launchTime + 0.02181631f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0.02181631f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.098f, (c,i,t,l, castc, casti) => 2f*(float)d, (c,i,t,l, castc, casti) => new Vector2(134f*((float)i%2f*2f-1f),178f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-70f,70f)*(float)ar,177f), (c,i,t,l, castc, casti) => new Vector2(0f,-219f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x4d/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 1.58292f));
            if (launchTime + 0.08959392 > Game.Music.Time) yield return Wait(launchTime + 0.08959392f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.08959392f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08143997f));
            if (launchTime + 0.1827717 > Game.Music.Time) yield return Wait(launchTime + 0.1827717f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.1827717f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08144188f));
            if (launchTime + 0.2849083 > Game.Music.Time) yield return Wait(launchTime + 0.2849083f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.2849083f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08144188f));
            if (launchTime + 0.3816719 > Game.Music.Time) yield return Wait(launchTime + 0.3816719f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.3816719f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08143997f));
            if (launchTime + 0.4766388 > Game.Music.Time) yield return Wait(launchTime + 0.4766388f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.4766388f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08143997f));
            if (launchTime + 0.583498 > Game.Music.Time) yield return Wait(launchTime + 0.583498f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.583498f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.08144188f));
            if (launchTime + 0.6844963 > Game.Music.Time) yield return Wait(launchTime + 0.6844963f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.6844963f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403564f));
            if (launchTime + 0.7773453 > Game.Music.Time) yield return Wait(launchTime + 0.7773453f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.7773453f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403564f));
            if (launchTime + 0.8864863 > Game.Music.Time) yield return Wait(launchTime + 0.8864863f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.8864863f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403564f));
            if (launchTime + 0.9825953 > Game.Music.Time) yield return Wait(launchTime + 0.9825953f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0.9825953f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403755f));
            if (launchTime + 1.091736 > Game.Music.Time) yield return Wait(launchTime + 1.091736f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.091736f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403755f));
            if (launchTime + 1.184585 > Game.Music.Time) yield return Wait(launchTime + 1.184585f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.184585f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403755f));
            if (launchTime + 1.288843 > Game.Music.Time) yield return Wait(launchTime + 1.288843f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.288843f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403564f));
            if (launchTime + 1.378433 > Game.Music.Time) yield return Wait(launchTime + 1.378433f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.378433f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403564f));
            if (launchTime + 1.482694 > Game.Music.Time) yield return Wait(launchTime + 1.482694f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.482694f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403564f));
            if (launchTime + 1.588573 > Game.Music.Time) yield return Wait(launchTime + 1.588573f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 1.588573f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.07403755f));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_0.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.008367545 > Game.Music.Time) yield return Wait(launchTime + 0.008367545f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.008367545f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1.3f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_2.output_enemy(ies)", 1.551167f));
            if (launchTime + 0.04394723 > Game.Music.Time) yield return Wait(launchTime + 0.04394723f);
            Player.StartCoroutine(DestroyAllBullet.Act(launchTime + 0.04394723f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 0.1167336 > Game.Music.Time) yield return Wait(launchTime + 0.1167336f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.1167336f, blackboard/*out*/));
            if (launchTime + 0.2151929 > Game.Music.Time) yield return Wait(launchTime + 0.2151929f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.2151929f, blackboard/*out*/));
            if (launchTime + 0.3064042 > Game.Music.Time) yield return Wait(launchTime + 0.3064042f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.3064042f, blackboard/*out*/));
            if (launchTime + 0.4092064 > Game.Music.Time) yield return Wait(launchTime + 0.4092064f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.4092064f, blackboard/*out*/));
            if (launchTime + 0.5076599 > Game.Music.Time) yield return Wait(launchTime + 0.5076599f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.5076599f, blackboard/*out*/));
            if (launchTime + 0.6032201 > Game.Music.Time) yield return Wait(launchTime + 0.6032201f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.6032201f, blackboard/*out*/));
            if (launchTime + 0.7074661 > Game.Music.Time) yield return Wait(launchTime + 0.7074661f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.7074661f, blackboard/*out*/));
            if (launchTime + 0.8204022 > Game.Music.Time) yield return Wait(launchTime + 0.8204022f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.8204022f, blackboard/*out*/));
            if (launchTime + 0.9116172 > Game.Music.Time) yield return Wait(launchTime + 0.9116172f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 0.9116172f, blackboard/*out*/));
            if (launchTime + 1.015863 > Game.Music.Time) yield return Wait(launchTime + 1.015863f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 1.015863f, blackboard/*out*/));
            if (launchTime + 1.105631 > Game.Music.Time) yield return Wait(launchTime + 1.105631f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 1.105631f, blackboard/*out*/));
            if (launchTime + 1.205536 > Game.Music.Time) yield return Wait(launchTime + 1.205536f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 1.205536f, blackboard/*out*/));
            if (launchTime + 1.308334 > Game.Music.Time) yield return Wait(launchTime + 1.308334f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 1.308334f, blackboard/*out*/));
            if (launchTime + 1.405346 > Game.Music.Time) yield return Wait(launchTime + 1.405346f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 1.405346f, blackboard/*out*/));
            if (launchTime + 1.507288 > Game.Music.Time) yield return Wait(launchTime + 1.507288f);
            Player.StartCoroutine(Pattern_8/*Shot*/(launchTime + 1.507288f, blackboard/*out*/));
            if (launchTime + 1.59659 > Game.Music.Time) yield return Wait(launchTime + 1.59659f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.59659f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_20.output_enemy(ies)", 0.1280842f));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x31/255f,0x1f/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.07644844f));
            if (launchTime + 0.01680565 > Game.Music.Time) yield return Wait(launchTime + 0.01680565f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.01680565f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => (float)rndf(50f,120f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)angle+180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x40/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.4152699 > Game.Music.Time) yield return Wait(launchTime + 0.4152699f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.4152699f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object bullets;
            blackboard.SubscribeForChanges("node_10.output_value", (b, o) => bullets = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullets", (c,i,t,l, castc, casti) => "node_4.output_bullets", (c,i,t,l, castc, casti) => "node_8.output_bullets", (c,i,t,l, castc, casti) => "node_11.output_bullets", (c,i,t,l, castc, casti) => "node_16.output_bullets", (c,i,t,l, castc, casti) => "node_19.output_bullets", (c,i,t,l, castc, casti) => "node_22.output_bullets", (c,i,t,l, castc, casti) => "node_25.output_bullets"/*out*/, "node_10.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x21/255f,0x28/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537476f));
            if (launchTime + 0.001991272 > Game.Music.Time) yield return Wait(launchTime + 0.001991272f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 0.001991272f, blackboard, (c,i,t,l, castc, casti) => new Color(0xcc/255f,0xff/255f,0x00/255f)/*out*/, "node_2.output_bullets"));
            if (launchTime + 0.003273011 > Game.Music.Time) yield return Wait(launchTime + 0.003273011f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 0.003273011f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 0.5977897 > Game.Music.Time) yield return Wait(launchTime + 0.5977897f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 0.5977897f, blackboard, (c,i,t,l, castc, casti) => new Color(0x4d/255f,0xff/255f,0x00/255f)/*out*/, "node_4.output_bullets"));
            if (launchTime + 0.5987549 > Game.Music.Time) yield return Wait(launchTime + 0.5987549f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5987549f, blackboard, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x24/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537514f));
            if (launchTime + 0.5998687 > Game.Music.Time) yield return Wait(launchTime + 0.5998687f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 0.5998687f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 1.001312 > Game.Music.Time) yield return Wait(launchTime + 1.001312f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.001312f, blackboard, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x23/255f,0x23/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537514f));
            if (launchTime + 1.001884 > Game.Music.Time) yield return Wait(launchTime + 1.001884f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 1.001884f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x63/255f,0x44/255f)/*out*/, "node_8.output_bullets"));
            if (launchTime + 1.007873 > Game.Music.Time) yield return Wait(launchTime + 1.007873f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 1.007873f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 1.401516 > Game.Music.Time) yield return Wait(launchTime + 1.401516f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 1.401516f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xd7/255f,0xaf/255f)/*out*/, "node_11.output_bullets"));
            if (launchTime + 1.407249 > Game.Music.Time) yield return Wait(launchTime + 1.407249f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.407249f, blackboard, (c,i,t,l, castc, casti) => new Color(0x19/255f,0x2a/255f,0x2a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537514f));
            if (launchTime + 1.407486 > Game.Music.Time) yield return Wait(launchTime + 1.407486f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 1.407486f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 2.405922 > Game.Music.Time) yield return Wait(launchTime + 2.405922f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 2.405922f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 2.807014 > Game.Music.Time) yield return Wait(launchTime + 2.807014f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 2.807014f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 3.193546 > Game.Music.Time) yield return Wait(launchTime + 3.193546f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 3.193546f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x63/255f,0x7f/255f)/*out*/, "node_16.output_bullets"));
            if (launchTime + 3.198837 > Game.Music.Time) yield return Wait(launchTime + 3.198837f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 3.198837f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 3.201645 > Game.Music.Time) yield return Wait(launchTime + 3.201645f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.201645f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x15/255f,0x1f/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537514f));
            if (launchTime + 3.798187 > Game.Music.Time) yield return Wait(launchTime + 3.798187f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 3.798187f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x16/255f,0xb4/255f)/*out*/, "node_19.output_bullets"));
            if (launchTime + 3.798279 > Game.Music.Time) yield return Wait(launchTime + 3.798279f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.798279f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x13/255f,0x2a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1537514f));
            if (launchTime + 3.807396 > Game.Music.Time) yield return Wait(launchTime + 3.807396f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 3.807396f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 4.201298 > Game.Music.Time) yield return Wait(launchTime + 4.201298f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 4.201298f, blackboard, (c,i,t,l, castc, casti) => new Color(0x91/255f,0x2e/255f,0xff/255f)/*out*/, "node_22.output_bullets"));
            if (launchTime + 4.203918 > Game.Music.Time) yield return Wait(launchTime + 4.203918f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.203918f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x0c/255f,0x2a/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1397743f));
            if (launchTime + 4.206306 > Game.Music.Time) yield return Wait(launchTime + 4.206306f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 4.206306f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 4.705177 > Game.Music.Time) yield return Wait(launchTime + 4.705177f);
            Player.StartCoroutine(Pattern_10/*Bullets*/(launchTime + 4.705177f, blackboard, (c,i,t,l, castc, casti) => new Color(0x5b/255f,0x00/255f,0xa1/255f)/*out*/, "node_25.output_bullets"));
            if (launchTime + 4.705192 > Game.Music.Time) yield return Wait(launchTime + 4.705192f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.705192f, blackboard, (c,i,t,l, castc, casti) => new Color(0x13/255f,0x00/255f,0x1d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1397743f));
            if (launchTime + 4.710037 > Game.Music.Time) yield return Wait(launchTime + 4.710037f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 4.710037f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 5.6185 > Game.Music.Time) yield return Wait(launchTime + 5.6185f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 5.6185f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 5.999397 > Game.Music.Time) yield return Wait(launchTime + 5.999397f);
            Player.StartCoroutine(Pattern_11/*Move*/(launchTime + 5.999397f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value"/*out*/));
            if (launchTime + 6.416119 > Game.Music.Time) yield return Wait(launchTime + 6.416119f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 6.416119f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard, InputFunc color_input, string bullets_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            SubscribeOutput(outboard, blackboard, bullets_output, "node_1.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => (float)d*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.ThreeDots, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => -500f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard, InputFunc bullets_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullets_input, "node_0.output_patterninput");
            object bullets = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullets = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*Background*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.005832674 > Game.Music.Time) yield return Wait(launchTime + 0.005832674f);
            Player.StartCoroutine(Pattern_14/*Melody*/(launchTime + 0.005832674f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f)*(float)ar,200f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x75/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)", 6.491734f));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.6885643 > Game.Music.Time) yield return Wait(launchTime + 0.6885643f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.6885643f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.6885643 > Game.Music.Time) yield return Wait(launchTime + 0.6885643f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.6885643f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.290451 > Game.Music.Time) yield return Wait(launchTime + 1.290451f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.290451f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.290451 > Game.Music.Time) yield return Wait(launchTime + 1.290451f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.290451f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.694252 > Game.Music.Time) yield return Wait(launchTime + 1.694252f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.694252f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.694252 > Game.Music.Time) yield return Wait(launchTime + 1.694252f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.694252f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.079002 > Game.Music.Time) yield return Wait(launchTime + 2.079002f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.079002f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.079002 > Game.Music.Time) yield return Wait(launchTime + 2.079002f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.079002f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.30376 > Game.Music.Time) yield return Wait(launchTime + 2.30376f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.30376f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.30376 > Game.Music.Time) yield return Wait(launchTime + 2.30376f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.30376f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.495747 > Game.Music.Time) yield return Wait(launchTime + 2.495747f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.495747f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.495747 > Game.Music.Time) yield return Wait(launchTime + 2.495747f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.495747f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.690834 > Game.Music.Time) yield return Wait(launchTime + 2.690834f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.690834f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.690834 > Game.Music.Time) yield return Wait(launchTime + 2.690834f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.690834f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.893249 > Game.Music.Time) yield return Wait(launchTime + 2.893249f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.893249f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.893249 > Game.Music.Time) yield return Wait(launchTime + 2.893249f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.893249f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.08477 > Game.Music.Time) yield return Wait(launchTime + 3.08477f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.08477f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.08477 > Game.Music.Time) yield return Wait(launchTime + 3.08477f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.08477f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.293693 > Game.Music.Time) yield return Wait(launchTime + 3.293693f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.293693f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.293693 > Game.Music.Time) yield return Wait(launchTime + 3.293693f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.293693f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.490189 > Game.Music.Time) yield return Wait(launchTime + 3.490189f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.490189f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.490189 > Game.Music.Time) yield return Wait(launchTime + 3.490189f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.490189f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.689171 > Game.Music.Time) yield return Wait(launchTime + 3.689171f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.689171f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.689171 > Game.Music.Time) yield return Wait(launchTime + 3.689171f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.689171f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.883182 > Game.Music.Time) yield return Wait(launchTime + 3.883182f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.883182f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.883182 > Game.Music.Time) yield return Wait(launchTime + 3.883182f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.883182f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.099575 > Game.Music.Time) yield return Wait(launchTime + 4.099575f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.099575f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.099575 > Game.Music.Time) yield return Wait(launchTime + 4.099575f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.099575f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.286117 > Game.Music.Time) yield return Wait(launchTime + 4.286117f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.286117f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.286117 > Game.Music.Time) yield return Wait(launchTime + 4.286117f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.286117f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.497536 > Game.Music.Time) yield return Wait(launchTime + 4.497536f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.497536f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.497536 > Game.Music.Time) yield return Wait(launchTime + 4.497536f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.497536f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.691543 > Game.Music.Time) yield return Wait(launchTime + 4.691543f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.691543f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.691543 > Game.Music.Time) yield return Wait(launchTime + 4.691543f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.691543f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.895512 > Game.Music.Time) yield return Wait(launchTime + 4.895512f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.895512f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.895512 > Game.Music.Time) yield return Wait(launchTime + 4.895512f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.895512f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.087033 > Game.Music.Time) yield return Wait(launchTime + 5.087033f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.087033f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.087033 > Game.Music.Time) yield return Wait(launchTime + 5.087033f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.087033f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.290974 > Game.Music.Time) yield return Wait(launchTime + 5.290974f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.290974f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.290974 > Game.Music.Time) yield return Wait(launchTime + 5.290974f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.290974f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.490642 > Game.Music.Time) yield return Wait(launchTime + 5.490642f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.490642f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.490642 > Game.Music.Time) yield return Wait(launchTime + 5.490642f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.490642f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.6851 > Game.Music.Time) yield return Wait(launchTime + 5.6851f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.6851f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.6851 > Game.Music.Time) yield return Wait(launchTime + 5.6851f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.6851f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.884075 > Game.Music.Time) yield return Wait(launchTime + 5.884075f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.884075f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.884075 > Game.Music.Time) yield return Wait(launchTime + 5.884075f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.884075f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.092113 > Game.Music.Time) yield return Wait(launchTime + 6.092113f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 6.092113f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.092113 > Game.Music.Time) yield return Wait(launchTime + 6.092113f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 6.092113f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.293346 > Game.Music.Time) yield return Wait(launchTime + 6.293346f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 6.293346f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.293346 > Game.Music.Time) yield return Wait(launchTime + 6.293346f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 6.293346f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(140f*(float)flipx,100f), (c,i,t,l, castc, casti) => (float)d*2f-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(-5f,5f)*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x8a/255f,0x88/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.002029418 > Game.Music.Time) yield return Wait(launchTime + 0.002029418f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.002029418f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2c/255f,0x1a/255f,0x1c/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1509438f));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*Background*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.006494522 > Game.Music.Time) yield return Wait(launchTime + 0.006494522f);
            Player.StartCoroutine(Pattern_17/*Melody*/(launchTime + 0.006494522f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.2001572 > Game.Music.Time) yield return Wait(launchTime + 0.2001572f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.2001572f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.2001572 > Game.Music.Time) yield return Wait(launchTime + 0.2001572f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.2001572f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.4024964 > Game.Music.Time) yield return Wait(launchTime + 0.4024964f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.4024964f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.4024964 > Game.Music.Time) yield return Wait(launchTime + 0.4024964f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.4024964f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.6070252 > Game.Music.Time) yield return Wait(launchTime + 0.6070252f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.6070252f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.6070252 > Game.Music.Time) yield return Wait(launchTime + 0.6070252f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.6070252f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.8049698 > Game.Music.Time) yield return Wait(launchTime + 0.8049698f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.8049698f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.8049698 > Game.Music.Time) yield return Wait(launchTime + 0.8049698f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.8049698f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.9919129 > Game.Music.Time) yield return Wait(launchTime + 0.9919129f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.9919129f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.9919129 > Game.Music.Time) yield return Wait(launchTime + 0.9919129f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 0.9919129f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.194252 > Game.Music.Time) yield return Wait(launchTime + 1.194252f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.194252f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.194252 > Game.Music.Time) yield return Wait(launchTime + 1.194252f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.194252f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.394393 > Game.Music.Time) yield return Wait(launchTime + 1.394393f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.394393f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.394393 > Game.Music.Time) yield return Wait(launchTime + 1.394393f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.394393f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.598938 > Game.Music.Time) yield return Wait(launchTime + 1.598938f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.598938f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.598938 > Game.Music.Time) yield return Wait(launchTime + 1.598938f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.598938f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.697906 > Game.Music.Time) yield return Wait(launchTime + 1.697906f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.697906f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.697906 > Game.Music.Time) yield return Wait(launchTime + 1.697906f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.697906f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.799072 > Game.Music.Time) yield return Wait(launchTime + 1.799072f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.799072f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.799072 > Game.Music.Time) yield return Wait(launchTime + 1.799072f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.799072f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.89804 > Game.Music.Time) yield return Wait(launchTime + 1.89804f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.89804f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.89804 > Game.Music.Time) yield return Wait(launchTime + 1.89804f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.89804f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.994812 > Game.Music.Time) yield return Wait(launchTime + 1.994812f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.994812f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.994812 > Game.Music.Time) yield return Wait(launchTime + 1.994812f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 1.994812f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.098175 > Game.Music.Time) yield return Wait(launchTime + 2.098175f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.098175f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.098175 > Game.Music.Time) yield return Wait(launchTime + 2.098175f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.098175f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.19934 > Game.Music.Time) yield return Wait(launchTime + 2.19934f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.19934f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.19934 > Game.Music.Time) yield return Wait(launchTime + 2.19934f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.19934f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.296112 > Game.Music.Time) yield return Wait(launchTime + 2.296112f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.296112f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.296112 > Game.Music.Time) yield return Wait(launchTime + 2.296112f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.296112f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.392906 > Game.Music.Time) yield return Wait(launchTime + 2.392906f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.392906f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.392906 > Game.Music.Time) yield return Wait(launchTime + 2.392906f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.392906f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.50505 > Game.Music.Time) yield return Wait(launchTime + 2.50505f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.50505f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.50505 > Game.Music.Time) yield return Wait(launchTime + 2.50505f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.50505f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.59523 > Game.Music.Time) yield return Wait(launchTime + 2.59523f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.59523f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.59523 > Game.Music.Time) yield return Wait(launchTime + 2.59523f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.59523f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.696395 > Game.Music.Time) yield return Wait(launchTime + 2.696395f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.696395f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.696395 > Game.Music.Time) yield return Wait(launchTime + 2.696395f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.696395f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.799774 > Game.Music.Time) yield return Wait(launchTime + 2.799774f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.799774f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.799774 > Game.Music.Time) yield return Wait(launchTime + 2.799774f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.799774f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 2.903129 > Game.Music.Time) yield return Wait(launchTime + 2.903129f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.903129f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 2.903129 > Game.Music.Time) yield return Wait(launchTime + 2.903129f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 2.903129f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.202407 > Game.Music.Time) yield return Wait(launchTime + 3.202407f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.202407f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.202407 > Game.Music.Time) yield return Wait(launchTime + 3.202407f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 3.202407f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.782714 > Game.Music.Time) yield return Wait(launchTime + 4.782714f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.782714f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.782714 > Game.Music.Time) yield return Wait(launchTime + 4.782714f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 4.782714f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.591613 > Game.Music.Time) yield return Wait(launchTime + 5.591613f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.591613f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.591613 > Game.Music.Time) yield return Wait(launchTime + 5.591613f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.591613f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.99823 > Game.Music.Time) yield return Wait(launchTime + 5.99823f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.99823f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.99823 > Game.Music.Time) yield return Wait(launchTime + 5.99823f);
            Player.StartCoroutine(Pattern_15/*Shot*/(launchTime + 5.99823f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.008846283 > Game.Music.Time) yield return Wait(launchTime + 0.008846283f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 0.008846283f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f)/*out*/));
            if (launchTime + 1.606281 > Game.Music.Time) yield return Wait(launchTime + 1.606281f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 1.606281f, blackboard/*out*/));
            if (launchTime + 1.611125 > Game.Music.Time) yield return Wait(launchTime + 1.611125f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 1.611125f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 3.18351 > Game.Music.Time) yield return Wait(launchTime + 3.18351f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 3.18351f, blackboard/*out*/));
            if (launchTime + 3.190616 > Game.Music.Time) yield return Wait(launchTime + 3.190616f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 3.190616f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0xff/255f)/*out*/));
            if (launchTime + 4.807969 > Game.Music.Time) yield return Wait(launchTime + 4.807969f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 4.807969f, blackboard/*out*/));
            if (launchTime + 4.812397 > Game.Music.Time) yield return Wait(launchTime + 4.812397f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 4.812397f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f)/*out*/));
            if (launchTime + 6.389955 > Game.Music.Time) yield return Wait(launchTime + 6.389955f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 6.389955f, blackboard/*out*/));
            if (launchTime + 6.392902 > Game.Music.Time) yield return Wait(launchTime + 6.392902f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 6.392902f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x6c/255f,0x00/255f)/*out*/));
            if (launchTime + 8.004967 > Game.Music.Time) yield return Wait(launchTime + 8.004967f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 8.004967f, blackboard/*out*/));
            if (launchTime + 8.010319 > Game.Music.Time) yield return Wait(launchTime + 8.010319f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 8.010319f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f)/*out*/));
            if (launchTime + 9.610535 > Game.Music.Time) yield return Wait(launchTime + 9.610535f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 9.610535f, blackboard/*out*/));
            if (launchTime + 9.614307 > Game.Music.Time) yield return Wait(launchTime + 9.614307f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 9.614307f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/));
            if (launchTime + 11.20194 > Game.Music.Time) yield return Wait(launchTime + 11.20194f);
            Player.StartCoroutine(Pattern_20/*Backgorund*/(launchTime + 11.20194f, blackboard/*out*/));
            if (launchTime + 11.20229 > Game.Music.Time) yield return Wait(launchTime + 11.20229f);
            Player.StartCoroutine(Pattern_19/*Small*/(launchTime + 11.20229f, blackboard, (c,i,t,l, castc, casti) => new Color(0x8a/255f,0xff/255f,0x2a/255f)/*out*/));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard, InputFunc color_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            object posy = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 70f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 200f));
            blackboard.SetValue("node_1.output_value", posy);

            object posx = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -200f*(float)ar), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 200f));
            blackboard.SetValue("node_2.output_value", posx);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.1f+0.05f*(5f-(float)d), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)posx+(float)rndf(-150f,150f),(float)posy+(float)rndf(-50f,50f)), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-190f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)", 1.59407f));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => 0.12f, (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareDark, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)t*360f+360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)sint((float)t)*70f+150f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x35/255f,0xb2/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)", 2.448277f));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.09269714 > Game.Music.Time) yield return Wait(launchTime + 0.09269714f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.09269714f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.09269714 > Game.Music.Time) yield return Wait(launchTime + 0.09269714f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.09269714f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.1957932 > Game.Music.Time) yield return Wait(launchTime + 0.1957932f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.1957932f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.1957932 > Game.Music.Time) yield return Wait(launchTime + 0.1957932f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.1957932f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.2929459 > Game.Music.Time) yield return Wait(launchTime + 0.2929459f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.2929459f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.2929459 > Game.Music.Time) yield return Wait(launchTime + 0.2929459f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.2929459f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.3940506 > Game.Music.Time) yield return Wait(launchTime + 0.3940506f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.3940506f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.3940506 > Game.Music.Time) yield return Wait(launchTime + 0.3940506f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.3940506f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.4852295 > Game.Music.Time) yield return Wait(launchTime + 0.4852295f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.4852295f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.4852295 > Game.Music.Time) yield return Wait(launchTime + 0.4852295f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.4852295f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.5982437 > Game.Music.Time) yield return Wait(launchTime + 0.5982437f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.5982437f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.5982437 > Game.Music.Time) yield return Wait(launchTime + 0.5982437f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.5982437f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.6973724 > Game.Music.Time) yield return Wait(launchTime + 0.6973724f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.6973724f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.6973724 > Game.Music.Time) yield return Wait(launchTime + 0.6973724f);
            Player.StartCoroutine(Pattern_22/*Beat*/(launchTime + 0.6973724f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_3.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(120f*(float)flipx,100f), (c,i,t,l, castc, casti) => 3f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 0.8f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object ray = blackboard.GetValue((c, i, t, l, castc, casti) => 5f*(float)d+10f);
            blackboard.SetValue("node_0.output_value", ray);

            object dens = blackboard.GetValue((c, i, t, l, castc, casti) => 3f*(float)d+5f);
            blackboard.SetValue("node_1.output_value", dens);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => (float)dens*(float)ray, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/((float)toint((float)c,(float)dens))*((float)toint((float)i,(float)dens)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f+100f/((float)toint((float)c,(float)ray))*((float)i%(float)dens), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x49/255f,0xff/255f,0x47/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.7934799 > Game.Music.Time) yield return Wait(launchTime + 0.7934799f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 0.7934799f, blackboard/*out*/));
            if (launchTime + 0.9901657 > Game.Music.Time) yield return Wait(launchTime + 0.9901657f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 0.9901657f, blackboard/*out*/));
            if (launchTime + 1.195747 > Game.Music.Time) yield return Wait(launchTime + 1.195747f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 1.195747f, blackboard/*out*/));
            if (launchTime + 1.392403 > Game.Music.Time) yield return Wait(launchTime + 1.392403f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 1.392403f, blackboard/*out*/));
            if (launchTime + 1.499673 > Game.Music.Time) yield return Wait(launchTime + 1.499673f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 1.499673f, blackboard/*out*/));
            if (launchTime + 1.592018 > Game.Music.Time) yield return Wait(launchTime + 1.592018f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 1.592018f, blackboard/*out*/));
            if (launchTime + 2.390594 > Game.Music.Time) yield return Wait(launchTime + 2.390594f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 2.390594f, blackboard/*out*/));
            if (launchTime + 2.59919 > Game.Music.Time) yield return Wait(launchTime + 2.59919f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 2.59919f, blackboard/*out*/));
            if (launchTime + 2.789856 > Game.Music.Time) yield return Wait(launchTime + 2.789856f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 2.789856f, blackboard/*out*/));
            if (launchTime + 2.998429 > Game.Music.Time) yield return Wait(launchTime + 2.998429f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 2.998429f, blackboard/*out*/));
            if (launchTime + 3.093209 > Game.Music.Time) yield return Wait(launchTime + 3.093209f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 3.093209f, blackboard/*out*/));
            if (launchTime + 3.597076 > Game.Music.Time) yield return Wait(launchTime + 3.597076f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 3.597076f, blackboard/*out*/));
            if (launchTime + 3.987144 > Game.Music.Time) yield return Wait(launchTime + 3.987144f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 3.987144f, blackboard/*out*/));
            if (launchTime + 4.393441 > Game.Music.Time) yield return Wait(launchTime + 4.393441f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 4.393441f, blackboard/*out*/));
            if (launchTime + 4.797067 > Game.Music.Time) yield return Wait(launchTime + 4.797067f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 4.797067f, blackboard/*out*/));
            if (launchTime + 5.189826 > Game.Music.Time) yield return Wait(launchTime + 5.189826f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 5.189826f, blackboard/*out*/));
            if (launchTime + 5.598845 > Game.Music.Time) yield return Wait(launchTime + 5.598845f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 5.598845f, blackboard/*out*/));
            if (launchTime + 5.997033 > Game.Music.Time) yield return Wait(launchTime + 5.997033f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 5.997033f, blackboard/*out*/));
            if (launchTime + 6.398057 > Game.Music.Time) yield return Wait(launchTime + 6.398057f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 6.398057f, blackboard/*out*/));
            if (launchTime + 7.197061 > Game.Music.Time) yield return Wait(launchTime + 7.197061f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 7.197061f, blackboard/*out*/));
            if (launchTime + 7.407784 > Game.Music.Time) yield return Wait(launchTime + 7.407784f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 7.407784f, blackboard/*out*/));
            if (launchTime + 7.580475 > Game.Music.Time) yield return Wait(launchTime + 7.580475f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 7.580475f, blackboard/*out*/));
            if (launchTime + 7.794122 > Game.Music.Time) yield return Wait(launchTime + 7.794122f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 7.794122f, blackboard/*out*/));
            if (launchTime + 7.893661 > Game.Music.Time) yield return Wait(launchTime + 7.893661f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 7.893661f, blackboard/*out*/));
            if (launchTime + 7.998985 > Game.Music.Time) yield return Wait(launchTime + 7.998985f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 7.998985f, blackboard/*out*/));
            if (launchTime + 8.795052 > Game.Music.Time) yield return Wait(launchTime + 8.795052f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 8.795052f, blackboard/*out*/));
            if (launchTime + 8.996726 > Game.Music.Time) yield return Wait(launchTime + 8.996726f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 8.996726f, blackboard/*out*/));
            if (launchTime + 9.201599 > Game.Music.Time) yield return Wait(launchTime + 9.201599f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 9.201599f, blackboard/*out*/));
            if (launchTime + 9.393166 > Game.Music.Time) yield return Wait(launchTime + 9.393166f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 9.393166f, blackboard/*out*/));
            if (launchTime + 9.496651 > Game.Music.Time) yield return Wait(launchTime + 9.496651f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 9.496651f, blackboard/*out*/));
            if (launchTime + 9.595772 > Game.Music.Time) yield return Wait(launchTime + 9.595772f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 9.595772f, blackboard/*out*/));
            if (launchTime + 9.998264 > Game.Music.Time) yield return Wait(launchTime + 9.998264f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 9.998264f, blackboard/*out*/));
            if (launchTime + 10.39775 > Game.Music.Time) yield return Wait(launchTime + 10.39775f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 10.39775f, blackboard/*out*/));
            if (launchTime + 10.79724 > Game.Music.Time) yield return Wait(launchTime + 10.79724f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 10.79724f, blackboard/*out*/));
            if (launchTime + 11.09311 > Game.Music.Time) yield return Wait(launchTime + 11.09311f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 11.09311f, blackboard/*out*/));
            if (launchTime + 11.19524 > Game.Music.Time) yield return Wait(launchTime + 11.19524f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 11.19524f, blackboard/*out*/));
            if (launchTime + 11.59474 > Game.Music.Time) yield return Wait(launchTime + 11.59474f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 11.59474f, blackboard/*out*/));
            if (launchTime + 11.99424 > Game.Music.Time) yield return Wait(launchTime + 11.99424f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 11.99424f, blackboard/*out*/));
            if (launchTime + 12.39674 > Game.Music.Time) yield return Wait(launchTime + 12.39674f);
            Player.StartCoroutine(Pattern_25/*Sparks*/(launchTime + 12.39674f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object posy = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 110f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 170f));
            blackboard.SetValue("node_0.output_value", posy);

            object posx = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -100f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 100f));
            blackboard.SetValue("node_1.output_value", posx);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)posx,(float)posy), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x7d/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.002204895 > Game.Music.Time) yield return Wait(launchTime + 0.002204895f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.002204895f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Vector2((float)posx,(float)posy), (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-100f,100f)*(float)ar,(float)rndf(0f,200f)), (c,i,t,l, castc, casti) => new Vector2(0f,-200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x93/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.23983 > Game.Music.Time) yield return Wait(launchTime + 0.23983f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.23983f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.187525 > Game.Music.Time) yield return Wait(launchTime + 0.187525f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0.187525f, blackboard/*out*/));
            if (launchTime + 0.3840674 > Game.Music.Time) yield return Wait(launchTime + 0.3840674f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0.3840674f, blackboard/*out*/));
            if (launchTime + 0.5786476 > Game.Music.Time) yield return Wait(launchTime + 0.5786476f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0.5786476f, blackboard/*out*/));
            if (launchTime + 0.7987751 > Game.Music.Time) yield return Wait(launchTime + 0.7987751f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0.7987751f, blackboard/*out*/));
            if (launchTime + 0.8911508 > Game.Music.Time) yield return Wait(launchTime + 0.8911508f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0.8911508f, blackboard/*out*/));
            if (launchTime + 0.9992499 > Game.Music.Time) yield return Wait(launchTime + 0.9992499f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 0.9992499f, blackboard/*out*/));
            if (launchTime + 1.097522 > Game.Music.Time) yield return Wait(launchTime + 1.097522f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.097522f, blackboard/*out*/));
            if (launchTime + 1.190262 > Game.Music.Time) yield return Wait(launchTime + 1.190262f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.190262f, blackboard/*out*/));
            if (launchTime + 1.195529 > Game.Music.Time) yield return Wait(launchTime + 1.195529f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.195529f, blackboard/*out*/));
            if (launchTime + 1.22485 > Game.Music.Time) yield return Wait(launchTime + 1.22485f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.22485f, blackboard/*out*/));
            if (launchTime + 1.237968 > Game.Music.Time) yield return Wait(launchTime + 1.237968f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.237968f, blackboard/*out*/));
            if (launchTime + 1.255552 > Game.Music.Time) yield return Wait(launchTime + 1.255552f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.255552f, blackboard/*out*/));
            if (launchTime + 1.284858 > Game.Music.Time) yield return Wait(launchTime + 1.284858f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.284858f, blackboard/*out*/));
            if (launchTime + 1.292331 > Game.Music.Time) yield return Wait(launchTime + 1.292331f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.292331f, blackboard/*out*/));
            if (launchTime + 1.317655 > Game.Music.Time) yield return Wait(launchTime + 1.317655f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.317655f, blackboard/*out*/));
            if (launchTime + 1.340036 > Game.Music.Time) yield return Wait(launchTime + 1.340036f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.340036f, blackboard/*out*/));
            if (launchTime + 1.348356 > Game.Music.Time) yield return Wait(launchTime + 1.348356f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.348356f, blackboard/*out*/));
            if (launchTime + 1.379756 > Game.Music.Time) yield return Wait(launchTime + 1.379756f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.379756f, blackboard/*out*/));
            if (launchTime + 1.387742 > Game.Music.Time) yield return Wait(launchTime + 1.387742f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.387742f, blackboard/*out*/));
            if (launchTime + 1.413947 > Game.Music.Time) yield return Wait(launchTime + 1.413947f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.413947f, blackboard/*out*/));
            if (launchTime + 1.439886 > Game.Music.Time) yield return Wait(launchTime + 1.439886f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.439886f, blackboard/*out*/));
            if (launchTime + 1.442506 > Game.Music.Time) yield return Wait(launchTime + 1.442506f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.442506f, blackboard/*out*/));
            if (launchTime + 1.47182 > Game.Music.Time) yield return Wait(launchTime + 1.47182f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.47182f, blackboard/*out*/));
            if (launchTime + 1.489811 > Game.Music.Time) yield return Wait(launchTime + 1.489811f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.489811f, blackboard/*out*/));
            if (launchTime + 1.501134 > Game.Music.Time) yield return Wait(launchTime + 1.501134f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.501134f, blackboard/*out*/));
            if (launchTime + 1.528046 > Game.Music.Time) yield return Wait(launchTime + 1.528046f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.528046f, blackboard/*out*/));
            if (launchTime + 1.538626 > Game.Music.Time) yield return Wait(launchTime + 1.538626f);
            Player.StartCoroutine(Pattern_31/*Beat*/(launchTime + 1.538626f, blackboard/*out*/));
            if (launchTime + 1.554477 > Game.Music.Time) yield return Wait(launchTime + 1.554477f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.554477f, blackboard/*out*/));
            if (launchTime + 1.582349 > Game.Music.Time) yield return Wait(launchTime + 1.582349f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.582349f, blackboard/*out*/));
            if (launchTime + 1.596424 > Game.Music.Time) yield return Wait(launchTime + 1.596424f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.596424f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => (float)d*15f-5f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => -20f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x6c/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_31.output_bullet(s)"));
            if (launchTime + 1.606857 > Game.Music.Time) yield return Wait(launchTime + 1.606857f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.606857f, blackboard/*out*/));
            if (launchTime + 1.636652 > Game.Music.Time) yield return Wait(launchTime + 1.636652f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.636652f, blackboard/*out*/));
            if (launchTime + 1.659237 > Game.Music.Time) yield return Wait(launchTime + 1.659237f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.659237f, blackboard/*out*/));
            if (launchTime + 1.683267 > Game.Music.Time) yield return Wait(launchTime + 1.683267f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.683267f, blackboard/*out*/));
            if (launchTime + 1.706814 > Game.Music.Time) yield return Wait(launchTime + 1.706814f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.706814f, blackboard/*out*/));
            if (launchTime + 1.736129 > Game.Music.Time) yield return Wait(launchTime + 1.736129f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.736129f, blackboard/*out*/));
            if (launchTime + 1.765923 > Game.Music.Time) yield return Wait(launchTime + 1.765923f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.765923f, blackboard/*out*/));
            if (launchTime + 1.787067 > Game.Music.Time) yield return Wait(launchTime + 1.787067f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.787067f, blackboard/*out*/));
            if (launchTime + 1.815277 > Game.Music.Time) yield return Wait(launchTime + 1.815277f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.815277f, blackboard/*out*/));
            if (launchTime + 1.853514 > Game.Music.Time) yield return Wait(launchTime + 1.853514f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.853514f, blackboard/*out*/));
            if (launchTime + 1.88744 > Game.Music.Time) yield return Wait(launchTime + 1.88744f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.88744f, blackboard/*out*/));
            if (launchTime + 1.921368 > Game.Music.Time) yield return Wait(launchTime + 1.921368f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.921368f, blackboard/*out*/));
            if (launchTime + 1.95045 > Game.Music.Time) yield return Wait(launchTime + 1.95045f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.95045f, blackboard/*out*/));
            if (launchTime + 1.973069 > Game.Music.Time) yield return Wait(launchTime + 1.973069f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 1.973069f, blackboard/*out*/));
            if (launchTime + 2.000535 > Game.Music.Time) yield return Wait(launchTime + 2.000535f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 2.000535f, blackboard/*out*/));
            if (launchTime + 2.026922 > Game.Music.Time) yield return Wait(launchTime + 2.026922f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 2.026922f, blackboard/*out*/));
            if (launchTime + 2.063541 > Game.Music.Time) yield return Wait(launchTime + 2.063541f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 2.063541f, blackboard/*out*/));
            if (launchTime + 2.09747 > Game.Music.Time) yield return Wait(launchTime + 2.09747f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 2.09747f, blackboard/*out*/));
            if (launchTime + 2.128167 > Game.Music.Time) yield return Wait(launchTime + 2.128167f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 2.128167f, blackboard/*out*/));
            if (launchTime + 2.160479 > Game.Music.Time) yield return Wait(launchTime + 2.160479f);
            Player.StartCoroutine(Pattern_27/*Shot*/(launchTime + 2.160479f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_0.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,100f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => (float)angle, (c,i,t,l, castc, casti) => (float)rndf(70f,100f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)angle+180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x40/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.07612371 > Game.Music.Time) yield return Wait(launchTime + 0.07612371f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.07612371f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object boss = GameObject.Find("Boss").GetComponent<Enemy>();
            blackboard.SetValue("node_0.output_boss", boss);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_29/*Smokes*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.008743286 > Game.Music.Time) yield return Wait(launchTime + 0.008743286f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 0.008743286f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => "Boss3_electrified", (c,i,t,l, castc, casti) => false/*out*/));
            if (launchTime + 0.01055908 > Game.Music.Time) yield return Wait(launchTime + 0.01055908f);
            Player.StartCoroutine(Pattern_24/*Sparks*/(launchTime + 0.01055908f, blackboard/*out*/));
            if (launchTime + 12.67924 > Game.Music.Time) yield return Wait(launchTime + 12.67924f);
            Player.StartCoroutine(SetEnemyAnimation.Act(launchTime + 12.67924f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_boss", (c,i,t,l, castc, casti) => "Boss3", (c,i,t,l, castc, casti) => false/*out*/));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.3011551 > Game.Music.Time) yield return Wait(launchTime + 0.3011551f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 0.3011551f, blackboard/*out*/));
            if (launchTime + 0.3958511 > Game.Music.Time) yield return Wait(launchTime + 0.3958511f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 0.3958511f, blackboard/*out*/));
            if (launchTime + 0.5943146 > Game.Music.Time) yield return Wait(launchTime + 0.5943146f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 0.5943146f, blackboard/*out*/));
            if (launchTime + 0.9975509 > Game.Music.Time) yield return Wait(launchTime + 0.9975509f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 0.9975509f, blackboard/*out*/));
            if (launchTime + 1.192963 > Game.Music.Time) yield return Wait(launchTime + 1.192963f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 1.192963f, blackboard/*out*/));
            if (launchTime + 1.354836 > Game.Music.Time) yield return Wait(launchTime + 1.354836f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 1.354836f, blackboard/*out*/));
            if (launchTime + 1.897415 > Game.Music.Time) yield return Wait(launchTime + 1.897415f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 1.897415f, blackboard/*out*/));
            if (launchTime + 1.99083 > Game.Music.Time) yield return Wait(launchTime + 1.99083f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 1.99083f, blackboard/*out*/));
            if (launchTime + 2.1978 > Game.Music.Time) yield return Wait(launchTime + 2.1978f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 2.1978f, blackboard/*out*/));
            if (launchTime + 2.397217 > Game.Music.Time) yield return Wait(launchTime + 2.397217f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 2.397217f, blackboard/*out*/));
            if (launchTime + 2.599144 > Game.Music.Time) yield return Wait(launchTime + 2.599144f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 2.599144f, blackboard/*out*/));
            if (launchTime + 2.793503 > Game.Music.Time) yield return Wait(launchTime + 2.793503f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 2.793503f, blackboard/*out*/));
            if (launchTime + 3.000527 > Game.Music.Time) yield return Wait(launchTime + 3.000527f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 3.000527f, blackboard/*out*/));
            if (launchTime + 3.502838 > Game.Music.Time) yield return Wait(launchTime + 3.502838f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 3.502838f, blackboard/*out*/));
            if (launchTime + 3.603806 > Game.Music.Time) yield return Wait(launchTime + 3.603806f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 3.603806f, blackboard/*out*/));
            if (launchTime + 3.788078 > Game.Music.Time) yield return Wait(launchTime + 3.788078f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 3.788078f, blackboard/*out*/));
            if (launchTime + 4.194481 > Game.Music.Time) yield return Wait(launchTime + 4.194481f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 4.194481f, blackboard/*out*/));
            if (launchTime + 4.401467 > Game.Music.Time) yield return Wait(launchTime + 4.401467f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 4.401467f, blackboard/*out*/));
            if (launchTime + 4.545357 > Game.Music.Time) yield return Wait(launchTime + 4.545357f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 4.545357f, blackboard/*out*/));
            if (launchTime + 5.088082 > Game.Music.Time) yield return Wait(launchTime + 5.088082f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 5.088082f, blackboard/*out*/));
            if (launchTime + 5.191559 > Game.Music.Time) yield return Wait(launchTime + 5.191559f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 5.191559f, blackboard/*out*/));
            if (launchTime + 5.39856 > Game.Music.Time) yield return Wait(launchTime + 5.39856f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 5.39856f, blackboard/*out*/));
            if (launchTime + 5.595436 > Game.Music.Time) yield return Wait(launchTime + 5.595436f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 5.595436f, blackboard/*out*/));
            if (launchTime + 5.797386 > Game.Music.Time) yield return Wait(launchTime + 5.797386f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 5.797386f, blackboard/*out*/));
            if (launchTime + 6.001808 > Game.Music.Time) yield return Wait(launchTime + 6.001808f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 6.001808f, blackboard/*out*/));
            if (launchTime + 6.191155 > Game.Music.Time) yield return Wait(launchTime + 6.191155f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 6.191155f, blackboard/*out*/));
            if (launchTime + 6.703575 > Game.Music.Time) yield return Wait(launchTime + 6.703575f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 6.703575f, blackboard/*out*/));
            if (launchTime + 6.784371 > Game.Music.Time) yield return Wait(launchTime + 6.784371f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 6.784371f, blackboard/*out*/));
            if (launchTime + 7.001465 > Game.Music.Time) yield return Wait(launchTime + 7.001465f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 7.001465f, blackboard/*out*/));
            if (launchTime + 7.385147 > Game.Music.Time) yield return Wait(launchTime + 7.385147f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 7.385147f, blackboard/*out*/));
            if (launchTime + 7.59214 > Game.Music.Time) yield return Wait(launchTime + 7.59214f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 7.59214f, blackboard/*out*/));
            if (launchTime + 7.748642 > Game.Music.Time) yield return Wait(launchTime + 7.748642f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 7.748642f, blackboard/*out*/));
            if (launchTime + 8.291336 > Game.Music.Time) yield return Wait(launchTime + 8.291336f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 8.291336f, blackboard/*out*/));
            if (launchTime + 8.392319 > Game.Music.Time) yield return Wait(launchTime + 8.392319f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 8.392319f, blackboard/*out*/));
            if (launchTime + 8.591713 > Game.Music.Time) yield return Wait(launchTime + 8.591713f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 8.591713f, blackboard/*out*/));
            if (launchTime + 8.796196 > Game.Music.Time) yield return Wait(launchTime + 8.796196f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 8.796196f, blackboard/*out*/));
            if (launchTime + 8.995605 > Game.Music.Time) yield return Wait(launchTime + 8.995605f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 8.995605f, blackboard/*out*/));
            if (launchTime + 9.200066 > Game.Music.Time) yield return Wait(launchTime + 9.200066f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 9.200066f, blackboard/*out*/));
            if (launchTime + 9.894203 > Game.Music.Time) yield return Wait(launchTime + 9.894203f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 9.894203f, blackboard/*out*/));
            if (launchTime + 9.997696 > Game.Music.Time) yield return Wait(launchTime + 9.997696f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 9.997696f, blackboard/*out*/));
            if (launchTime + 10.19458 > Game.Music.Time) yield return Wait(launchTime + 10.19458f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 10.19458f, blackboard/*out*/));
            if (launchTime + 10.59593 > Game.Music.Time) yield return Wait(launchTime + 10.59593f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 10.59593f, blackboard/*out*/));
            if (launchTime + 10.79537 > Game.Music.Time) yield return Wait(launchTime + 10.79537f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 10.79537f, blackboard/*out*/));
            if (launchTime + 10.94679 > Game.Music.Time) yield return Wait(launchTime + 10.94679f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 10.94679f, blackboard/*out*/));
            if (launchTime + 11.49459 > Game.Music.Time) yield return Wait(launchTime + 11.49459f);
            Player.StartCoroutine(Pattern_30/*Smoke*/(launchTime + 11.49459f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Electro, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-150f,150f),(float)rndf(130f,180f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.001335144 > Game.Music.Time) yield return Wait(launchTime + 0.001335144f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.001335144f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Smoke, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-150f,150f),(float)rndf(130f,180f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.5811463 > Game.Music.Time) yield return Wait(launchTime + 0.5811463f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 0.5811463f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 0.58284 > Game.Music.Time) yield return Wait(launchTime + 0.58284f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 0.58284f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,167f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.587348 > Game.Music.Time) yield return Wait(launchTime + 1.587348f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 1.587348f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.587348 > Game.Music.Time) yield return Wait(launchTime + 1.587348f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 1.587348f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.199311 > Game.Music.Time) yield return Wait(launchTime + 3.199311f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 3.199311f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.199311 > Game.Music.Time) yield return Wait(launchTime + 3.199311f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 3.199311f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 4.786816 > Game.Music.Time) yield return Wait(launchTime + 4.786816f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 4.786816f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 4.786816 > Game.Music.Time) yield return Wait(launchTime + 4.786816f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 4.786816f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.397586 > Game.Music.Time) yield return Wait(launchTime + 6.397586f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 6.397586f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.397586 > Game.Music.Time) yield return Wait(launchTime + 6.397586f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 6.397586f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.987317 > Game.Music.Time) yield return Wait(launchTime + 7.987317f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 7.987317f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.987317 > Game.Music.Time) yield return Wait(launchTime + 7.987317f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 7.987317f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.592464 > Game.Music.Time) yield return Wait(launchTime + 9.592464f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 9.592464f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.592464 > Game.Music.Time) yield return Wait(launchTime + 9.592464f);
            Player.StartCoroutine(Pattern_33/*TopDown*/(launchTime + 9.592464f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.18781 > Game.Music.Time) yield return Wait(launchTime + 11.18781f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 11.18781f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.18781 > Game.Music.Time) yield return Wait(launchTime + 11.18781f);
            Player.StartCoroutine(Pattern_35/*LeftRight*/(launchTime + 11.18781f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.3849601 > Game.Music.Time) yield return Wait(launchTime + 0.3849601f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 0.3849601f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.786026 > Game.Music.Time) yield return Wait(launchTime + 0.786026f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 0.786026f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.183678 > Game.Music.Time) yield return Wait(launchTime + 1.183678f);
            Player.StartCoroutine(Pattern_34/*Shot*/(launchTime + 1.183678f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2b/255f,0x10/255f,0x35/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3037529f));
            if (launchTime + 0.006797791 > Game.Music.Time) yield return Wait(launchTime + 0.006797791f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.006797791f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-310f,310f)*(float)ar,240f*(float)flipx), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-(float)rndf(100f,150f)*(float)flipx), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x91/255f,0x22/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.4010734 > Game.Music.Time) yield return Wait(launchTime + 0.4010734f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.4010734f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.7815742 > Game.Music.Time) yield return Wait(launchTime + 0.7815742f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 0.7815742f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.199787 > Game.Music.Time) yield return Wait(launchTime + 1.199787f);
            Player.StartCoroutine(Pattern_36/*Shot*/(launchTime + 1.199787f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x33/255f,0x2c/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.285759f));
            if (launchTime + 0.007518768 > Game.Music.Time) yield return Wait(launchTime + 0.007518768f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.007518768f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => new Vector2(320f*(float)flipx*(float)ar,(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(-(float)rndf(100f,150f)*(float)flipx,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2b/255f,0xff/255f,0xc4/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x0c/255f,0x1b/255f)/*out*/, 0.6580544f));
            if (launchTime + 0.7040367 > Game.Music.Time) yield return Wait(launchTime + 0.7040367f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.7040367f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.6795731f));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 6.377443 > Game.Music.Time) yield return Wait(launchTime + 6.377443f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 6.377443f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 150f/*out*/));
            if (launchTime + 7.17955 > Game.Music.Time) yield return Wait(launchTime + 7.17955f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 7.17955f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 300f/*out*/));
            if (launchTime + 7.590578 > Game.Music.Time) yield return Wait(launchTime + 7.590578f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 7.590578f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 150f/*out*/));
            if (launchTime + 8.782228 > Game.Music.Time) yield return Wait(launchTime + 8.782228f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 8.782228f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 11.18189 > Game.Music.Time) yield return Wait(launchTime + 11.18189f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 11.18189f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 12.77194 > Game.Music.Time) yield return Wait(launchTime + 12.77194f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 12.77194f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 14.38256 > Game.Music.Time) yield return Wait(launchTime + 14.38256f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 14.38256f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 15.97947 > Game.Music.Time) yield return Wait(launchTime + 15.97947f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 15.97947f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 17.59008 > Game.Music.Time) yield return Wait(launchTime + 17.59008f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 17.59008f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 19.19384 > Game.Music.Time) yield return Wait(launchTime + 19.19384f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 19.19384f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 20.68795 > Game.Music.Time) yield return Wait(launchTime + 20.68795f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 20.68795f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 22.3671 > Game.Music.Time) yield return Wait(launchTime + 22.3671f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 22.3671f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 28.76158 > Game.Music.Time) yield return Wait(launchTime + 28.76158f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 28.76158f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 35.08067 > Game.Music.Time) yield return Wait(launchTime + 35.08067f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 35.08067f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 41.47514 > Game.Music.Time) yield return Wait(launchTime + 41.47514f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 41.47514f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 47.98872 > Game.Music.Time) yield return Wait(launchTime + 47.98872f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 47.98872f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 49.58468 > Game.Music.Time) yield return Wait(launchTime + 49.58468f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 49.58468f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 51.19285 > Game.Music.Time) yield return Wait(launchTime + 51.19285f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 51.19285f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 52.78271 > Game.Music.Time) yield return Wait(launchTime + 52.78271f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 52.78271f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 54.37869 > Game.Music.Time) yield return Wait(launchTime + 54.37869f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 54.37869f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 55.98681 > Game.Music.Time) yield return Wait(launchTime + 55.98681f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 55.98681f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 57.60713 > Game.Music.Time) yield return Wait(launchTime + 57.60713f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 57.60713f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 59.1788 > Game.Music.Time) yield return Wait(launchTime + 59.1788f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 59.1788f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 60.78083 > Game.Music.Time) yield return Wait(launchTime + 60.78083f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 60.78083f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 72.77041 > Game.Music.Time) yield return Wait(launchTime + 72.77041f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 72.77041f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 300f/*out*/));
            if (launchTime + 73.58041 > Game.Music.Time) yield return Wait(launchTime + 73.58041f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 73.58041f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 150f/*out*/));
            if (launchTime + 79.99666 > Game.Music.Time) yield return Wait(launchTime + 79.99666f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 79.99666f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 86.37057 > Game.Music.Time) yield return Wait(launchTime + 86.37057f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 86.37057f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 92.78001 > Game.Music.Time) yield return Wait(launchTime + 92.78001f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 92.78001f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 98.37645 > Game.Music.Time) yield return Wait(launchTime + 98.37645f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 98.37645f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 99.19338 > Game.Music.Time) yield return Wait(launchTime + 99.19338f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 99.19338f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 100.7797 > Game.Music.Time) yield return Wait(launchTime + 100.7797f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 100.7797f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 102.3839 > Game.Music.Time) yield return Wait(launchTime + 102.3839f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 102.3839f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 103.988 > Game.Music.Time) yield return Wait(launchTime + 103.988f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 103.988f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 105.6041 > Game.Music.Time) yield return Wait(launchTime + 105.6041f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 105.6041f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 107.1844 > Game.Music.Time) yield return Wait(launchTime + 107.1844f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 107.1844f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 108.7826 > Game.Music.Time) yield return Wait(launchTime + 108.7826f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 108.7826f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 110.3749 > Game.Music.Time) yield return Wait(launchTime + 110.3749f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 110.3749f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 110.7803 > Game.Music.Time) yield return Wait(launchTime + 110.7803f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 110.7803f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 111.1814 > Game.Music.Time) yield return Wait(launchTime + 111.1814f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 111.1814f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 111.5762 > Game.Music.Time) yield return Wait(launchTime + 111.5762f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 111.5762f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,235f), (c,i,t,l, castc, casti) => (float)d*7f, (c,i,t,l, castc, casti) => new Vector2((640f/((float)c-1f)*(float)i-320f+640f/((float)c-1f)/2f)*(float)ar,30f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,(float)rndf(-150f,-200f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x10/255f,0x5a/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.001308184 > Game.Music.Time) yield return Wait(launchTime + 0.001308184f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.001308184f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,235f), (c,i,t,l, castc, casti) => (float)d*7f, (c,i,t,l, castc, casti) => new Vector2((640f/((float)c-1f)*(float)i-320f)*(float)ar,0f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,(float)rndf(-150f,-200f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x3a/255f,0x10/255f,0x5a/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

    }
}