#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_Infiltration : LevelScript
    {
        protected override string ClipName {
            get { return "Infiltration.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0.2376842 > Game.Music.Time) yield return Wait(launchTime + 0.2376842f);
            Player.StartCoroutine(Pattern_1/*Part1*/(launchTime + 0.2376842f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x63/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 1.25809 > Game.Music.Time) yield return Wait(launchTime + 1.25809f);
            Player.StartCoroutine(Pattern_40/*DanceMan*/(launchTime + 1.25809f, blackboard/*out*/));
            if (launchTime + 13.04695 > Game.Music.Time) yield return Wait(launchTime + 13.04695f);
            Player.StartCoroutine(Pattern_1/*Part1*/(launchTime + 13.04695f, blackboard, (c,i,t,l, castc, casti) => new Color(0x82/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallRectangle/*out*/));
            if (launchTime + 14.05412 > Game.Music.Time) yield return Wait(launchTime + 14.05412f);
            Player.StartCoroutine(Pattern_39/*Effects*/(launchTime + 14.05412f, blackboard/*out*/));
            if (launchTime + 14.0702 > Game.Music.Time) yield return Wait(launchTime + 14.0702f);
            Player.StartCoroutine(Pattern_8/*Rhytm*/(launchTime + 14.0702f, blackboard/*out*/));
            if (launchTime + 25.847 > Game.Music.Time) yield return Wait(launchTime + 25.847f);
            Player.StartCoroutine(Pattern_6/*Part2*/(launchTime + 25.847f, blackboard/*out*/));
            if (launchTime + 39.49107 > Game.Music.Time) yield return Wait(launchTime + 39.49107f);
            Player.StartCoroutine(Pattern_10/*Part3*/(launchTime + 39.49107f, blackboard/*out*/));
            if (launchTime + 64.79554 > Game.Music.Time) yield return Wait(launchTime + 64.79554f);
            Player.StartCoroutine(Pattern_13/*Part4*/(launchTime + 64.79554f, blackboard/*out*/));
            if (launchTime + 87.65189 > Game.Music.Time) yield return Wait(launchTime + 87.65189f);
            Player.StartCoroutine(Pattern_37/*Transition*/(launchTime + 87.65189f, blackboard/*out*/));
            if (launchTime + 90.39543 > Game.Music.Time) yield return Wait(launchTime + 90.39543f);
            Player.StartCoroutine(Pattern_20/*Part5*/(launchTime + 90.39543f, blackboard/*out*/));
            if (launchTime + 116.4645 > Game.Music.Time) yield return Wait(launchTime + 116.4645f);
            Player.StartCoroutine(Pattern_34/*Part7*/(launchTime + 116.4645f, blackboard/*out*/));
            if (launchTime + 139.6449 > Game.Music.Time) yield return Wait(launchTime + 139.6449f);
            Player.StartCoroutine(Pattern_43/*Transition2*/(launchTime + 139.6449f, blackboard/*out*/));
            if (launchTime + 141.4656 > Game.Music.Time) yield return Wait(launchTime + 141.4656f);
            Player.StartCoroutine(Pattern_33/*Background*/(launchTime + 141.4656f, blackboard/*out*/));
            if (launchTime + 142.062 > Game.Music.Time) yield return Wait(launchTime + 142.062f);
            Player.StartCoroutine(Pattern_31/*Part8*/(launchTime + 142.062f, blackboard/*out*/));
            if (launchTime + 154.69 > Game.Music.Time) yield return Wait(launchTime + 154.69f);
            Player.StartCoroutine(Pattern_44/*Part9*/(launchTime + 154.69f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 154.69 > Game.Music.Time) yield return Wait(launchTime + 154.69f);
            Player.StartCoroutine(Pattern_44/*Part9*/(launchTime + 154.69f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 3f/*out*/));
            if (launchTime + 154.8603 > Game.Music.Time) yield return Wait(launchTime + 154.8603f);
            Player.StartCoroutine(Pattern_50/*Background*/(launchTime + 154.8603f, blackboard/*out*/));
            if (launchTime + 154.8659 > Game.Music.Time) yield return Wait(launchTime + 154.8659f);
            Player.StartCoroutine(Pattern_47/*BigBullets*/(launchTime + 154.8659f, blackboard/*out*/));
            if (launchTime + 180.4621 > Game.Music.Time) yield return Wait(launchTime + 180.4621f);
            Player.StartCoroutine(Pattern_31/*Part8*/(launchTime + 180.4621f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc bullettype_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_1.output_patterninput");
            object color = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, bullettype_input, "node_2.output_patterninput");
            object bullettype = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => bullettype = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_2/*CreateEnemies*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 1.031948 > Game.Music.Time) yield return Wait(launchTime + 1.031948f);
            Player.StartCoroutine(Pattern_4/*RotateEnemies*/(launchTime + 1.031948f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_4.output_value");
            object enemies;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy", (c,i,t,l, castc, casti) => "node_1.output_enemy", (c,i,t,l, castc, casti) => "node_2.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_3/*SingleEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => (float)toint((float)d,3f), (c,i,t,l, castc, casti) => new Vector2(0f,200f)/*out*/, "node_0.output_enemy"));
            if (launchTime + 2.026558E-06 > Game.Music.Time) yield return Wait(launchTime + 2.026558E-06f);
            Player.StartCoroutine(Pattern_3/*SingleEnemy*/(launchTime + 2.026558E-06f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2(-200f*(float)ar,170f)/*out*/, "node_1.output_enemy"));
            if (launchTime + 1.108646E-05 > Game.Music.Time) yield return Wait(launchTime + 1.108646E-05f);
            Player.StartCoroutine(Pattern_3/*SingleEnemy*/(launchTime + 1.108646E-05f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2(200f*(float)ar,170f)/*out*/, "node_2.output_enemy"));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard, InputFunc count_input, InputFunc position_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, count_input, "node_2.output_patterninput");
            object count = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => count = o);

            SubscribeInput(outboard, blackboard, position_input, "node_3.output_patterninput");
            object position = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => position = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => EnemyImage.Enemy10, (c,i,t,l, castc, casti) => 3000f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(0f,337f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.05211991 > Game.Music.Time) yield return Wait(launchTime + 0.05211991f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.05211991f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_1.output_enemy(ies)", 0.9769115f));
            if (launchTime + 11.51153 > Game.Music.Time) yield return Wait(launchTime + 11.51153f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 11.51153f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_6.output_enemy(ies)", 2.300087f));
            if (launchTime + 13.85358 > Game.Music.Time) yield return Wait(launchTime + 13.85358f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 13.85358f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard, InputFunc bullettype_input, InputFunc color_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bullettype_input, "node_0.output_patterninput");
            object bullettype = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bullettype = o);

            SubscribeInput(outboard, blackboard, color_input, "node_1.output_patterninput");
            object color = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_2.output_patterninput");
            object enemies = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 0.6151896 > Game.Music.Time) yield return Wait(launchTime + 0.6151896f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 0.6151896f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.246103 > Game.Music.Time) yield return Wait(launchTime + 1.246103f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 1.246103f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.815162 > Game.Music.Time) yield return Wait(launchTime + 1.815162f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 1.815162f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 2.41721 > Game.Music.Time) yield return Wait(launchTime + 2.41721f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 2.41721f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 3.023381 > Game.Music.Time) yield return Wait(launchTime + 3.023381f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 3.023381f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 3.229562 > Game.Music.Time) yield return Wait(launchTime + 3.229562f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 3.229562f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 3.815116 > Game.Music.Time) yield return Wait(launchTime + 3.815116f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 3.815116f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 4.437781 > Game.Music.Time) yield return Wait(launchTime + 4.437781f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 4.437781f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 5.010966 > Game.Music.Time) yield return Wait(launchTime + 5.010966f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 5.010966f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 5.600642 > Game.Music.Time) yield return Wait(launchTime + 5.600642f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 5.600642f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 6.227431 > Game.Music.Time) yield return Wait(launchTime + 6.227431f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 6.227431f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 6.417117 > Game.Music.Time) yield return Wait(launchTime + 6.417117f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 6.417117f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 7.035661 > Game.Music.Time) yield return Wait(launchTime + 7.035661f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 7.035661f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 7.633586 > Game.Music.Time) yield return Wait(launchTime + 7.633586f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 7.633586f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 8.219138 > Game.Music.Time) yield return Wait(launchTime + 8.219138f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 8.219138f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 8.81294 > Game.Music.Time) yield return Wait(launchTime + 8.81294f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 8.81294f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 9.414989 > Game.Music.Time) yield return Wait(launchTime + 9.414989f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 9.414989f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 9.625293 > Game.Music.Time) yield return Wait(launchTime + 9.625293f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 9.625293f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 10.20672 > Game.Music.Time) yield return Wait(launchTime + 10.20672f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 10.20672f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 10.80052 > Game.Music.Time) yield return Wait(launchTime + 10.80052f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 10.80052f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 11.39845 > Game.Music.Time) yield return Wait(launchTime + 11.39845f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 11.39845f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 12.01287 > Game.Music.Time) yield return Wait(launchTime + 12.01287f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 12.01287f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 12.61079 > Game.Music.Time) yield return Wait(launchTime + 12.61079f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 12.61079f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc bullettype_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, bullettype_input, "node_1.output_patterninput");
            object bullettype = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => bullettype = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_2.output_patterninput");
            object enemy = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => enemy = o);

            object angle = blackboard.GetValue((c, i, t, l, castc, casti) => (float)rndf(0f,360f));
            blackboard.SetValue("node_3.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_2/*CreateEnemies*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 1.025241 > Game.Music.Time) yield return Wait(launchTime + 1.025241f);
            Player.StartCoroutine(Pattern_7/*RotateEnemy*/(launchTime + 1.025241f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies", (c,i,t,l, castc, casti) => new Color(0xff/255f,0xa7/255f,0x47/255f), (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp/*out*/));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard, InputFunc enemy_input, InputFunc color_input, InputFunc bullettype_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            SubscribeInput(outboard, blackboard, color_input, "node_4.output_patterninput");
            object color = blackboard.GetValue("node_4.output_patterninput");
            blackboard.SubscribeForChanges("node_4.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, bullettype_input, "node_17.output_patterninput");
            object bullettype = blackboard.GetValue("node_17.output_patterninput");
            blackboard.SubscribeForChanges("node_17.output_patterninput", (b, o) => bullettype = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.3890629 > Game.Music.Time) yield return Wait(launchTime + 0.3890629f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 0.3890629f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6099281 > Game.Music.Time) yield return Wait(launchTime + 0.6099281f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 0.6099281f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9994512 > Game.Music.Time) yield return Wait(launchTime + 0.9994512f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 0.9994512f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.188189 > Game.Music.Time) yield return Wait(launchTime + 1.188189f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 1.188189f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.581726 > Game.Music.Time) yield return Wait(launchTime + 1.581726f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 1.581726f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.818653 > Game.Music.Time) yield return Wait(launchTime + 1.818653f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 1.818653f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.200144 > Game.Music.Time) yield return Wait(launchTime + 2.200144f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 2.200144f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.392897 > Game.Music.Time) yield return Wait(launchTime + 2.392897f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 2.392897f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.794466 > Game.Music.Time) yield return Wait(launchTime + 2.794466f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 2.794466f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.995251 > Game.Music.Time) yield return Wait(launchTime + 2.995251f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 2.995251f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.208084 > Game.Music.Time) yield return Wait(launchTime + 3.208084f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 3.208084f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.581545 > Game.Music.Time) yield return Wait(launchTime + 3.581545f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 3.581545f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.790361 > Game.Music.Time) yield return Wait(launchTime + 3.790361f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 3.790361f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.212006 > Game.Music.Time) yield return Wait(launchTime + 4.212006f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 4.212006f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.392712 > Game.Music.Time) yield return Wait(launchTime + 4.392712f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 4.392712f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.798298 > Game.Music.Time) yield return Wait(launchTime + 4.798298f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 4.798298f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.983021 > Game.Music.Time) yield return Wait(launchTime + 4.983021f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 4.983021f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.384588 > Game.Music.Time) yield return Wait(launchTime + 5.384588f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 5.384588f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.605452 > Game.Music.Time) yield return Wait(launchTime + 5.605452f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 5.605452f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.994978 > Game.Music.Time) yield return Wait(launchTime + 5.994978f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 5.994978f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.183714 > Game.Music.Time) yield return Wait(launchTime + 6.183714f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 6.183714f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.39253 > Game.Music.Time) yield return Wait(launchTime + 6.39253f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 6.39253f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.782049 > Game.Music.Time) yield return Wait(launchTime + 6.782049f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 6.782049f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.010946 > Game.Music.Time) yield return Wait(launchTime + 7.010946f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 7.010946f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.376371 > Game.Music.Time) yield return Wait(launchTime + 7.376371f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 7.376371f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.585188 > Game.Music.Time) yield return Wait(launchTime + 7.585188f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 7.585188f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.990774 > Game.Music.Time) yield return Wait(launchTime + 7.990774f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 7.990774f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.191557 > Game.Music.Time) yield return Wait(launchTime + 8.191557f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 8.191557f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.597143 > Game.Music.Time) yield return Wait(launchTime + 8.597143f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 8.597143f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.765806 > Game.Music.Time) yield return Wait(launchTime + 8.765806f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 8.765806f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.187452 > Game.Music.Time) yield return Wait(launchTime + 9.187452f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 9.187452f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.404303 > Game.Music.Time) yield return Wait(launchTime + 9.404303f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 9.404303f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.609106 > Game.Music.Time) yield return Wait(launchTime + 9.609106f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 9.609106f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.990599 > Game.Music.Time) yield return Wait(launchTime + 9.990599f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 9.990599f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.20343 > Game.Music.Time) yield return Wait(launchTime + 10.20343f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 10.20343f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.58492 > Game.Music.Time) yield return Wait(launchTime + 10.58492f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 10.58492f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.78972 > Game.Music.Time) yield return Wait(launchTime + 10.78972f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 10.78972f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.39939 > Game.Music.Time) yield return Wait(launchTime + 11.39939f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 11.39939f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.99444 > Game.Music.Time) yield return Wait(launchTime + 11.99444f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 11.99444f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.40331 > Game.Music.Time) yield return Wait(launchTime + 12.40331f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 12.40331f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.59314 > Game.Music.Time) yield return Wait(launchTime + 12.59314f);
            Player.StartCoroutine(Pattern_5/*Rotate*/(launchTime + 12.59314f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_patterninput", (c,i,t,l, castc, casti) => "node_17.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.8074951 > Game.Music.Time) yield return Wait(launchTime + 0.8074951f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0.8074951f, blackboard/*out*/));
            if (launchTime + 1.187286 > Game.Music.Time) yield return Wait(launchTime + 1.187286f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 1.187286f, blackboard/*out*/));
            if (launchTime + 2.193498 > Game.Music.Time) yield return Wait(launchTime + 2.193498f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.193498f, blackboard/*out*/));
            if (launchTime + 2.395722 > Game.Music.Time) yield return Wait(launchTime + 2.395722f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.395722f, blackboard/*out*/));
            if (launchTime + 3.189836 > Game.Music.Time) yield return Wait(launchTime + 3.189836f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 3.189836f, blackboard/*out*/));
            if (launchTime + 3.988877 > Game.Music.Time) yield return Wait(launchTime + 3.988877f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 3.988877f, blackboard/*out*/));
            if (launchTime + 4.4032 > Game.Music.Time) yield return Wait(launchTime + 4.4032f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 4.4032f, blackboard/*out*/));
            if (launchTime + 5.394608 > Game.Music.Time) yield return Wait(launchTime + 5.394608f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 5.394608f, blackboard/*out*/));
            if (launchTime + 5.586976 > Game.Music.Time) yield return Wait(launchTime + 5.586976f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 5.586976f, blackboard/*out*/));
            if (launchTime + 5.991425 > Game.Music.Time) yield return Wait(launchTime + 5.991425f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 5.991425f, blackboard/*out*/));
            if (launchTime + 6.400819 > Game.Music.Time) yield return Wait(launchTime + 6.400819f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 6.400819f, blackboard/*out*/));
            if (launchTime + 7.20479 > Game.Music.Time) yield return Wait(launchTime + 7.20479f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 7.20479f, blackboard/*out*/));
            if (launchTime + 7.589524 > Game.Music.Time) yield return Wait(launchTime + 7.589524f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 7.589524f, blackboard/*out*/));
            if (launchTime + 8.595719 > Game.Music.Time) yield return Wait(launchTime + 8.595719f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 8.595719f, blackboard/*out*/));
            if (launchTime + 8.797944 > Game.Music.Time) yield return Wait(launchTime + 8.797944f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 8.797944f, blackboard/*out*/));
            if (launchTime + 9.597001 > Game.Music.Time) yield return Wait(launchTime + 9.597001f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 9.597001f, blackboard/*out*/));
            if (launchTime + 10.39111 > Game.Music.Time) yield return Wait(launchTime + 10.39111f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 10.39111f, blackboard/*out*/));
            if (launchTime + 10.79555 > Game.Music.Time) yield return Wait(launchTime + 10.79555f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 10.79555f, blackboard/*out*/));
            if (launchTime + 11.79684 > Game.Music.Time) yield return Wait(launchTime + 11.79684f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 11.79684f, blackboard/*out*/));
            if (launchTime + 11.99905 > Game.Music.Time) yield return Wait(launchTime + 11.99905f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 11.99905f, blackboard/*out*/));
            if (launchTime + 12.38872 > Game.Music.Time) yield return Wait(launchTime + 12.38872f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 12.38872f, blackboard/*out*/));
            if (launchTime + 12.60081 > Game.Music.Time) yield return Wait(launchTime + 12.60081f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 12.60081f, blackboard/*out*/));
            if (launchTime + 12.79811 > Game.Music.Time) yield return Wait(launchTime + 12.79811f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 12.79811f, blackboard/*out*/));
            if (launchTime + 13.59717 > Game.Music.Time) yield return Wait(launchTime + 13.59717f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 13.59717f, blackboard/*out*/));
            if (launchTime + 14.00162 > Game.Music.Time) yield return Wait(launchTime + 14.00162f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 14.00162f, blackboard/*out*/));
            if (launchTime + 14.99796 > Game.Music.Time) yield return Wait(launchTime + 14.99796f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 14.99796f, blackboard/*out*/));
            if (launchTime + 15.19524 > Game.Music.Time) yield return Wait(launchTime + 15.19524f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 15.19524f, blackboard/*out*/));
            if (launchTime + 15.99924 > Game.Music.Time) yield return Wait(launchTime + 15.99924f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 15.99924f, blackboard/*out*/));
            if (launchTime + 16.78841 > Game.Music.Time) yield return Wait(launchTime + 16.78841f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 16.78841f, blackboard/*out*/));
            if (launchTime + 17.18793 > Game.Music.Time) yield return Wait(launchTime + 17.18793f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 17.18793f, blackboard/*out*/));
            if (launchTime + 18.20401 > Game.Music.Time) yield return Wait(launchTime + 18.20401f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 18.20401f, blackboard/*out*/));
            if (launchTime + 18.3865 > Game.Music.Time) yield return Wait(launchTime + 18.3865f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 18.3865f, blackboard/*out*/));
            if (launchTime + 18.79005 > Game.Music.Time) yield return Wait(launchTime + 18.79005f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 18.79005f, blackboard/*out*/));
            if (launchTime + 19.19362 > Game.Music.Time) yield return Wait(launchTime + 19.19362f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 19.19362f, blackboard/*out*/));
            if (launchTime + 19.99625 > Game.Music.Time) yield return Wait(launchTime + 19.99625f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 19.99625f, blackboard/*out*/));
            if (launchTime + 20.4043 > Game.Music.Time) yield return Wait(launchTime + 20.4043f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 20.4043f, blackboard/*out*/));
            if (launchTime + 21.3818 > Game.Music.Time) yield return Wait(launchTime + 21.3818f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 21.3818f, blackboard/*out*/));
            if (launchTime + 21.59254 > Game.Music.Time) yield return Wait(launchTime + 21.59254f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 21.59254f, blackboard/*out*/));
            if (launchTime + 22.39966 > Game.Music.Time) yield return Wait(launchTime + 22.39966f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 22.39966f, blackboard/*out*/));
            if (launchTime + 23.19333 > Game.Music.Time) yield return Wait(launchTime + 23.19333f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 23.19333f, blackboard/*out*/));
            if (launchTime + 23.59688 > Game.Music.Time) yield return Wait(launchTime + 23.59688f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 23.59688f, blackboard/*out*/));
            if (launchTime + 24.19325 > Game.Music.Time) yield return Wait(launchTime + 24.19325f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 24.19325f, blackboard/*out*/));
            if (launchTime + 24.7986 > Game.Music.Time) yield return Wait(launchTime + 24.7986f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 24.7986f, blackboard/*out*/));
            if (launchTime + 25.19766 > Game.Music.Time) yield return Wait(launchTime + 25.19766f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 25.19766f, blackboard/*out*/));
            if (launchTime + 25.59227 > Game.Music.Time) yield return Wait(launchTime + 25.59227f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 25.59227f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => (float)toint((float)d,2f)+1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f)*(float)ar,(float)rndf(100f,220f)), (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.008132935 > Game.Music.Time) yield return Wait(launchTime + 0.008132935f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.008132935f, blackboard, (c,i,t,l, castc, casti) => new Color(0x3c/255f,0x19/255f,0x12/255f), (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f)/*out*/, 0.1674271f));
            if (launchTime + 0.01831818 > Game.Music.Time) yield return Wait(launchTime + 0.01831818f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.01831818f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f+(float)rndf(0f,100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.1900558 > Game.Music.Time) yield return Wait(launchTime + 0.1900558f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.1900558f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object radiusstart = blackboard.GetValue((c, i, t, l, castc, casti) => 0f);
            blackboard.SetValue("node_4.output_value", radiusstart);

            object radius1 = blackboard.GetValue((c, i, t, l, castc, casti) => 60f);
            blackboard.SetValue("node_8.output_value", radius1);

            object radius2 = blackboard.GetValue((c, i, t, l, castc, casti) => 120f);
            blackboard.SetValue("node_16.output_value", radius2);

            object radius3 = blackboard.GetValue((c, i, t, l, castc, casti) => 180f);
            blackboard.SetValue("node_25.output_value", radius3);

            object radiusfinal = blackboard.GetValue((c, i, t, l, castc, casti) => 240f);
            blackboard.SetValue("node_33.output_value", radiusfinal);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => -5f/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.007919297 > Game.Music.Time) yield return Wait(launchTime + 0.007919297f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.007919297f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_1.output_enemy(ies)", 0.1585197f));
            if (launchTime + 0.1792527 > Game.Music.Time) yield return Wait(launchTime + 0.1792527f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.1792527f, blackboard, (c,i,t,l, castc, casti) => (float)radiusstart+20f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 0.3642922 > Game.Music.Time) yield return Wait(launchTime + 0.3642922f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.3642922f, blackboard, (c,i,t,l, castc, casti) => (float)radiusstart+40f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 0.5794602 > Game.Music.Time) yield return Wait(launchTime + 0.5794602f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.5794602f, blackboard, (c,i,t,l, castc, casti) => (float)radiusstart+60f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 0.7729226 > Game.Music.Time) yield return Wait(launchTime + 0.7729226f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.7729226f, blackboard, (c,i,t,l, castc, casti) => (float)radius1, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 0.9729882 > Game.Music.Time) yield return Wait(launchTime + 0.9729882f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 0.9729882f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius1/*out*/));
            if (launchTime + 1.773774 > Game.Music.Time) yield return Wait(launchTime + 1.773774f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 1.773774f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius1/*out*/));
            if (launchTime + 2.573017 > Game.Music.Time) yield return Wait(launchTime + 2.573017f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 2.573017f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius1/*out*/));
            if (launchTime + 3.371536 > Game.Music.Time) yield return Wait(launchTime + 3.371536f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 3.371536f, blackboard, (c,i,t,l, castc, casti) => (float)radius1+20f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 3.579376 > Game.Music.Time) yield return Wait(launchTime + 3.579376f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 3.579376f, blackboard, (c,i,t,l, castc, casti) => (float)radius1+40f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 3.773994 > Game.Music.Time) yield return Wait(launchTime + 3.773994f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 3.773994f, blackboard, (c,i,t,l, castc, casti) => (float)radius2, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 3.970497 > Game.Music.Time) yield return Wait(launchTime + 3.970497f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 3.970497f, blackboard, (c,i,t,l, castc, casti) => (float)radius2, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 4.16586 > Game.Music.Time) yield return Wait(launchTime + 4.16586f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 4.16586f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius2/*out*/));
            if (launchTime + 4.968883 > Game.Music.Time) yield return Wait(launchTime + 4.968883f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 4.968883f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius2/*out*/));
            if (launchTime + 5.770424 > Game.Music.Time) yield return Wait(launchTime + 5.770424f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 5.770424f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius2/*out*/));
            if (launchTime + 5.872002 > Game.Music.Time) yield return Wait(launchTime + 5.872002f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 5.872002f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius2/*out*/));
            if (launchTime + 6.568569 > Game.Music.Time) yield return Wait(launchTime + 6.568569f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 6.568569f, blackboard, (c,i,t,l, castc, casti) => (float)radius2+20f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 6.770367 > Game.Music.Time) yield return Wait(launchTime + 6.770367f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 6.770367f, blackboard, (c,i,t,l, castc, casti) => (float)radius2+40f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 6.970367 > Game.Music.Time) yield return Wait(launchTime + 6.970367f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 6.970367f, blackboard, (c,i,t,l, castc, casti) => (float)radius3, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 7.168587 > Game.Music.Time) yield return Wait(launchTime + 7.168587f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 7.168587f, blackboard, (c,i,t,l, castc, casti) => (float)radius3, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 7.369393 > Game.Music.Time) yield return Wait(launchTime + 7.369393f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 7.369393f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius3/*out*/));
            if (launchTime + 8.165791 > Game.Music.Time) yield return Wait(launchTime + 8.165791f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 8.165791f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius3/*out*/));
            if (launchTime + 8.96579 > Game.Music.Time) yield return Wait(launchTime + 8.96579f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 8.96579f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radius3/*out*/));
            if (launchTime + 9.765499 > Game.Music.Time) yield return Wait(launchTime + 9.765499f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 9.765499f, blackboard, (c,i,t,l, castc, casti) => (float)radius3+20f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 9.958076 > Game.Music.Time) yield return Wait(launchTime + 9.958076f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 9.958076f, blackboard, (c,i,t,l, castc, casti) => (float)radius3+40f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 10.15777 > Game.Music.Time) yield return Wait(launchTime + 10.15777f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 10.15777f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 10.36224 > Game.Music.Time) yield return Wait(launchTime + 10.36224f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 10.36224f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 10.56526 > Game.Music.Time) yield return Wait(launchTime + 10.56526f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 10.56526f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 11.36153 > Game.Music.Time) yield return Wait(launchTime + 11.36153f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 11.36153f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 12.16691 > Game.Music.Time) yield return Wait(launchTime + 12.16691f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 12.16691f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 12.2719 > Game.Music.Time) yield return Wait(launchTime + 12.2719f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 12.2719f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 12.9627 > Game.Music.Time) yield return Wait(launchTime + 12.9627f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 12.9627f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 13.15921 > Game.Music.Time) yield return Wait(launchTime + 13.15921f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 13.15921f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 13.37183 > Game.Music.Time) yield return Wait(launchTime + 13.37183f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 13.37183f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 13.56563 > Game.Music.Time) yield return Wait(launchTime + 13.56563f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 13.56563f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 13.7633 > Game.Music.Time) yield return Wait(launchTime + 13.7633f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 13.7633f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 14.56291 > Game.Music.Time) yield return Wait(launchTime + 14.56291f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 14.56291f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 15.36831 > Game.Music.Time) yield return Wait(launchTime + 15.36831f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 15.36831f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 16.153 > Game.Music.Time) yield return Wait(launchTime + 16.153f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 16.153f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 16.37435 > Game.Music.Time) yield return Wait(launchTime + 16.37435f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 16.37435f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 16.57949 > Game.Music.Time) yield return Wait(launchTime + 16.57949f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 16.57949f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 16.77114 > Game.Music.Time) yield return Wait(launchTime + 16.77114f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 16.77114f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 16.9733 > Game.Music.Time) yield return Wait(launchTime + 16.9733f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 16.9733f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 17.76143 > Game.Music.Time) yield return Wait(launchTime + 17.76143f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 17.76143f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 18.57256 > Game.Music.Time) yield return Wait(launchTime + 18.57256f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 18.57256f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 18.67105 > Game.Music.Time) yield return Wait(launchTime + 18.67105f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 18.67105f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 19.37326 > Game.Music.Time) yield return Wait(launchTime + 19.37326f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 19.37326f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 19.57623 > Game.Music.Time) yield return Wait(launchTime + 19.57623f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 19.57623f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 19.75975 > Game.Music.Time) yield return Wait(launchTime + 19.75975f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 19.75975f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 19.96921 > Game.Music.Time) yield return Wait(launchTime + 19.96921f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 19.96921f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 20.16033 > Game.Music.Time) yield return Wait(launchTime + 20.16033f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 20.16033f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 20.97145 > Game.Music.Time) yield return Wait(launchTime + 20.97145f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 20.97145f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 21.7567 > Game.Music.Time) yield return Wait(launchTime + 21.7567f);
            Player.StartCoroutine(Pattern_12/*4Shots*/(launchTime + 21.7567f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x70/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)radiusfinal/*out*/));
            if (launchTime + 22.55301 > Game.Music.Time) yield return Wait(launchTime + 22.55301f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 22.55301f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 22.80773 > Game.Music.Time) yield return Wait(launchTime + 22.80773f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 22.80773f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 23.02689 > Game.Music.Time) yield return Wait(launchTime + 23.02689f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 23.02689f, blackboard, (c,i,t,l, castc, casti) => (float)radiusfinal, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Medium/*out*/));
            if (launchTime + 23.35896 > Game.Music.Time) yield return Wait(launchTime + 23.35896f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 23.35896f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_63.output_enemy(ies)", 1.596367f));
            if (launchTime + 24.97507 > Game.Music.Time) yield return Wait(launchTime + 24.97507f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 24.97507f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard, InputFunc radius_input, InputFunc bulletcolor_input, InputFunc bullettype_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, radius_input, "node_0.output_patterninput");
            object radius = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => radius = o);

            SubscribeInput(outboard, blackboard, bulletcolor_input, "node_1.output_patterninput");
            object bulletcolor = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => bulletcolor = o);

            SubscribeInput(outboard, blackboard, bullettype_input, "node_2.output_patterninput");
            object bullettype = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => bullettype = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_3.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.0009078979 > Game.Music.Time) yield return Wait(launchTime + 0.0009078979f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0009078979f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => 5f*(float)d-3f, (c,i,t,l, castc, casti) => (float)angle+360f/(float)c*(float)i, (c,i,t,l, castc, casti) => (float)radius, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)angle+360f/(float)c*(float)i+180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 150f+150f/4f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.009220123 > Game.Music.Time) yield return Wait(launchTime + 0.009220123f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.009220123f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard, InputFunc bulletcolor_input, InputFunc radius_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, bulletcolor_input, "node_0.output_patterninput");
            object bulletcolor = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => bulletcolor = o);

            SubscribeInput(outboard, blackboard, radius_input, "node_1.output_patterninput");
            object radius = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => radius = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => (float)radius, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 0.1964073 > Game.Music.Time) yield return Wait(launchTime + 0.1964073f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.1964073f, blackboard, (c,i,t,l, castc, casti) => (float)radius, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 0.3952713 > Game.Music.Time) yield return Wait(launchTime + 0.3952713f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.3952713f, blackboard, (c,i,t,l, castc, casti) => (float)radius, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 0.5939942 > Game.Music.Time) yield return Wait(launchTime + 0.5939942f);
            Player.StartCoroutine(Pattern_11/*SingleShot*/(launchTime + 0.5939942f, blackboard, (c,i,t,l, castc, casti) => (float)radius, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_15/*RightEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0.002952904 > Game.Music.Time) yield return Wait(launchTime + 0.002952904f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 0.002952904f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 0.4634098 > Game.Music.Time) yield return Wait(launchTime + 0.4634098f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 0.4634098f, blackboard/*out*/));
            if (launchTime + 3.205743 > Game.Music.Time) yield return Wait(launchTime + 3.205743f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 3.205743f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 3.662453 > Game.Music.Time) yield return Wait(launchTime + 3.662453f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 3.662453f, blackboard/*out*/));
            if (launchTime + 3.680466 > Game.Music.Time) yield return Wait(launchTime + 3.680466f);
            Player.StartCoroutine(Pattern_18/*ShortWob*/(launchTime + 3.680466f, blackboard/*out*/));
            if (launchTime + 6.396431 > Game.Music.Time) yield return Wait(launchTime + 6.396431f);
            Player.StartCoroutine(Pattern_15/*RightEnemy*/(launchTime + 6.396431f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 6.401978 > Game.Music.Time) yield return Wait(launchTime + 6.401978f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 6.401978f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 6.870789 > Game.Music.Time) yield return Wait(launchTime + 6.870789f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 6.870789f, blackboard/*out*/));
            if (launchTime + 9.61805 > Game.Music.Time) yield return Wait(launchTime + 9.61805f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 9.61805f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 10.06332 > Game.Music.Time) yield return Wait(launchTime + 10.06332f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 10.06332f, blackboard/*out*/));
            if (launchTime + 10.06538 > Game.Music.Time) yield return Wait(launchTime + 10.06538f);
            Player.StartCoroutine(Pattern_18/*ShortWob*/(launchTime + 10.06538f, blackboard/*out*/));
            if (launchTime + 12.7835 > Game.Music.Time) yield return Wait(launchTime + 12.7835f);
            Player.StartCoroutine(Pattern_15/*RightEnemy*/(launchTime + 12.7835f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 12.80765 > Game.Music.Time) yield return Wait(launchTime + 12.80765f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 12.80765f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 13.12032 > Game.Music.Time) yield return Wait(launchTime + 13.12032f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 13.12032f, blackboard/*out*/));
            if (launchTime + 15.9972 > Game.Music.Time) yield return Wait(launchTime + 15.9972f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 15.9972f, blackboard, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 16.46947 > Game.Music.Time) yield return Wait(launchTime + 16.46947f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 16.46947f, blackboard/*out*/));
            if (launchTime + 16.47772 > Game.Music.Time) yield return Wait(launchTime + 16.47772f);
            Player.StartCoroutine(Pattern_18/*ShortWob*/(launchTime + 16.47772f, blackboard/*out*/));
            if (launchTime + 19.19338 > Game.Music.Time) yield return Wait(launchTime + 19.19338f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 19.19338f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x8f/255f,0x10/255f)/*out*/));
            if (launchTime + 19.20647 > Game.Music.Time) yield return Wait(launchTime + 19.20647f);
            Player.StartCoroutine(Pattern_15/*RightEnemy*/(launchTime + 19.20647f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 19.65143 > Game.Music.Time) yield return Wait(launchTime + 19.65143f);
            Player.StartCoroutine(Pattern_41/*Background*/(launchTime + 19.65143f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc speed_input, InputFunc color_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, speed_input, "node_2.output_patterninput");
            object speed = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => speed = o);

            SubscribeInput(outboard, blackboard, color_input, "node_3.output_patterninput");
            object color = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => color = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy12, (c,i,t,l, castc, casti) => 1500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2((-180f*(float)ar-150f*((float)ar-1f))*(float)flipx,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => -5f/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.03374481 > Game.Music.Time) yield return Wait(launchTime + 0.03374481f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.03374481f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_4.output_enemy(ies)", 0.4185638f));
            if (launchTime + 0.4711685 > Game.Music.Time) yield return Wait(launchTime + 0.4711685f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.4711685f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.04f, (c,i,t,l, castc, casti) => 1f+(float)t*(2f*(float)d - 1f), (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (float)t*(50f+50f*(float)d), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed*(float)t+(float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f/*out*/, "node_5.output_bullet(s)", 0.5776978f));
            if (launchTime + 3.500389 > Game.Music.Time) yield return Wait(launchTime + 3.500389f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 3.500389f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_7.output_enemy(ies)", 0.7009506f));
            if (launchTime + 4.228569 > Game.Music.Time) yield return Wait(launchTime + 4.228569f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 4.228569f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            object enemies;
            blackboard.SubscribeForChanges("node_2.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_5.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_12.output_enemy", (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_17/*LaunchEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_1.output_enemy"));
            if (launchTime + 1.069642 > Game.Music.Time) yield return Wait(launchTime + 1.069642f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 1.069642f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 1.185685 > Game.Music.Time) yield return Wait(launchTime + 1.185685f);
            Player.StartCoroutine(Pattern_17/*LaunchEnemy*/(launchTime + 1.185685f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_5.output_enemy"));
            if (launchTime + 1.262314 > Game.Music.Time) yield return Wait(launchTime + 1.262314f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 1.262314f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 1.45945 > Game.Music.Time) yield return Wait(launchTime + 1.45945f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 1.45945f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 1.656815 > Game.Music.Time) yield return Wait(launchTime + 1.656815f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 1.656815f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 1.857094 > Game.Music.Time) yield return Wait(launchTime + 1.857094f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 1.857094f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.074791 > Game.Music.Time) yield return Wait(launchTime + 2.074791f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 2.074791f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.266358 > Game.Music.Time) yield return Wait(launchTime + 2.266358f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 2.266358f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.292023 > Game.Music.Time) yield return Wait(launchTime + 2.292023f);
            Player.StartCoroutine(Pattern_17/*LaunchEnemy*/(launchTime + 2.292023f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_12.output_enemy"));
            if (launchTime + 2.466637 > Game.Music.Time) yield return Wait(launchTime + 2.466637f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 2.466637f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.66687 > Game.Music.Time) yield return Wait(launchTime + 2.66687f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 2.66687f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 2.864273 > Game.Music.Time) yield return Wait(launchTime + 2.864273f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 2.864273f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.050042 > Game.Music.Time) yield return Wait(launchTime + 3.050042f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 3.050042f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.259026 > Game.Music.Time) yield return Wait(launchTime + 3.259026f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 3.259026f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            if (launchTime + 3.458512 > Game.Music.Time) yield return Wait(launchTime + 3.458512f);
            Player.StartCoroutine(Pattern_16/*Shot*/(launchTime + 3.458512f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d/*out*/, "node_1.output_elements"));
            if (launchTime + 0.008033752 > Game.Music.Time) yield return Wait(launchTime + 0.008033752f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.008033752f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 20f/(float)c*((float)i+0.5f)-10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xab/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc flipx_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_1.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy9, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(347f*(float)flipx*(float)ar,19f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.009368896 > Game.Music.Time) yield return Wait(launchTime + 0.009368896f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.009368896f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(347f*(float)ar,79f,135f*(float)ar,93f,-88f*(float)ar,165f,-250f*(float)ar,267f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,1f), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_3.output_enemy(ies)", 3.761948f));
            if (launchTime + 3.824638 > Game.Music.Time) yield return Wait(launchTime + 3.824638f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 3.824638f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy8, (c,i,t,l, castc, casti) => 1500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(1f,290f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.00541693 > Game.Music.Time) yield return Wait(launchTime + 0.00541693f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.00541693f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_1.output_enemy(ies)", 0.5790482f));
            if (launchTime + 0.5824203 > Game.Music.Time) yield return Wait(launchTime + 0.5824203f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.5824203f, blackboard, (c,i,t,l, castc, casti) => -60f, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 0.7817764 > Game.Music.Time) yield return Wait(launchTime + 0.7817764f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.7817764f, blackboard, (c,i,t,l, castc, casti) => -20f, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 0.9891891 > Game.Music.Time) yield return Wait(launchTime + 0.9891891f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.9891891f, blackboard, (c,i,t,l, castc, casti) => 20f, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 1.18737 > Game.Music.Time) yield return Wait(launchTime + 1.18737f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.18737f, blackboard, (c,i,t,l, castc, casti) => 60f, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)"/*out*/));
            if (launchTime + 2.398224 > Game.Music.Time) yield return Wait(launchTime + 2.398224f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.398224f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => 30f*(float)d - 25f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(300f,400f), (c,i,t,l, castc, casti) => -200f, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            if (launchTime + 2.401054 > Game.Music.Time) yield return Wait(launchTime + 2.401054f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.401054f, blackboard, (c,i,t,l, castc, casti) => new Color(0x46/255f,0x22/255f,0x21/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4015961f));
            if (launchTime + 2.402435 > Game.Music.Time) yield return Wait(launchTime + 2.402435f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 2.402435f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,290f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_9.output_enemy(ies)", 0.7778244f));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard, InputFunc angle_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, angle_input, "node_0.output_patterninput");
            object angle = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => angle = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => -900f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 1.020706 > Game.Music.Time) yield return Wait(launchTime + 1.020706f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.020706f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => 2f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => (float)rndf(-30f,30f)+90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(70f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x75/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 1.034279 > Game.Music.Time) yield return Wait(launchTime + 1.034279f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.034279f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_26/*LongWobEnemies*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => 150f/*out*/));
            if (launchTime + 0.4549865 > Game.Music.Time) yield return Wait(launchTime + 0.4549865f);
            Player.StartCoroutine(Pattern_23/*UpperEnemies*/(launchTime + 0.4549865f, blackboard/*out*/, "node_1.output_enemylist"));
            if (launchTime + 1.25795 > Game.Music.Time) yield return Wait(launchTime + 1.25795f);
            Player.StartCoroutine(Pattern_24/*UpperEnemiesShot*/(launchTime + 1.25795f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemylist"/*out*/));
            if (launchTime + 10.44225 > Game.Music.Time) yield return Wait(launchTime + 10.44225f);
            Player.StartCoroutine(Pattern_28/*Insertion1*/(launchTime + 10.44225f, blackboard/*out*/));
            if (launchTime + 24.07521 > Game.Music.Time) yield return Wait(launchTime + 24.07521f);
            Player.StartCoroutine(Pattern_30/*insertion2*/(launchTime + 24.07521f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemylist"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard, string lastenemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, lastenemy_output, "node_3.output_value");
            object varlastenemy;
            blackboard.SubscribeForChanges("node_3.output_value", (b, o) => varlastenemy = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy", (c,i,t,l, castc, casti) => "node_2.output_enemy", (c,i,t,l, castc, casti) => "node_4.output_enemy", (c,i,t,l, castc, casti) => "node_6.output_enemy", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*CreateEnemy*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemy"));
            if (launchTime + 0.7858123 > Game.Music.Time) yield return Wait(launchTime + 0.7858123f);
            Player.StartCoroutine(Pattern_22/*CreateEnemy*/(launchTime + 0.7858123f, blackboard/*out*/, "node_2.output_enemy"));
            if (launchTime + 1.593338 > Game.Music.Time) yield return Wait(launchTime + 1.593338f);
            Player.StartCoroutine(Pattern_22/*CreateEnemy*/(launchTime + 1.593338f, blackboard/*out*/, "node_4.output_enemy"));
            if (launchTime + 2.386521 > Game.Music.Time) yield return Wait(launchTime + 2.386521f);
            Player.StartCoroutine(Pattern_22/*CreateEnemy*/(launchTime + 2.386521f, blackboard/*out*/, "node_6.output_enemy"));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemy_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy3, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-332f*(float)ar,208f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.01181793 > Game.Music.Time) yield return Wait(launchTime + 0.01181793f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.01181793f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(-350f*(float)ar,208f,350f*(float)ar,208f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 2.548706f));
            if (launchTime + 2.582374 > Game.Music.Time) yield return Wait(launchTime + 2.582374f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.582374f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard, string enemylist_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemylist_output, "node_4.output_value");
            object enemieslist;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => enemieslist = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_lastenemy", (c,i,t,l, castc, casti) => "node_2.output_lastenemy", (c,i,t,l, castc, casti) => "node_3.output_lastenemy", (c,i,t,l, castc, casti) => "node_5.output_lastenemy", (c,i,t,l, castc, casti) => "node_7.output_lastenemy", (c,i,t,l, castc, casti) => "node_8.output_lastenemy", (c,i,t,l, castc, casti) => "node_9.output_lastenemy", (c,i,t,l, castc, casti) => "node_10.output_lastenemy"/*out*/, "node_4.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 0f, blackboard/*out*/, "node_0.output_lastenemy"));
            if (launchTime + 3.152031 > Game.Music.Time) yield return Wait(launchTime + 3.152031f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 3.152031f, blackboard/*out*/, "node_2.output_lastenemy"));
            if (launchTime + 6.401527 > Game.Music.Time) yield return Wait(launchTime + 6.401527f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 6.401527f, blackboard/*out*/, "node_3.output_lastenemy"));
            if (launchTime + 9.578667 > Game.Music.Time) yield return Wait(launchTime + 9.578667f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 9.578667f, blackboard/*out*/, "node_5.output_lastenemy"));
            if (launchTime + 12.79058 > Game.Music.Time) yield return Wait(launchTime + 12.79058f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 12.79058f, blackboard/*out*/, "node_7.output_lastenemy"));
            if (launchTime + 15.97935 > Game.Music.Time) yield return Wait(launchTime + 15.97935f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 15.97935f, blackboard/*out*/, "node_8.output_lastenemy"));
            if (launchTime + 19.19132 > Game.Music.Time) yield return Wait(launchTime + 19.19132f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 19.19132f, blackboard/*out*/, "node_9.output_lastenemy"));
            if (launchTime + 22.40326 > Game.Music.Time) yield return Wait(launchTime + 22.40326f);
            Player.StartCoroutine(Pattern_21/*4UpperEnemies*/(launchTime + 22.40326f, blackboard/*out*/, "node_10.output_lastenemy"));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4073877 > Game.Music.Time) yield return Wait(launchTime + 0.4073877f);
            Player.StartCoroutine(Pattern_25/*5Shots*/(launchTime + 0.4073877f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.601326 > Game.Music.Time) yield return Wait(launchTime + 1.601326f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 1.601326f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.785652 > Game.Music.Time) yield return Wait(launchTime + 1.785652f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 1.785652f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.011383 > Game.Music.Time) yield return Wait(launchTime + 2.011383f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 2.011383f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.199486 > Game.Music.Time) yield return Wait(launchTime + 2.199486f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 2.199486f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.034683 > Game.Music.Time) yield return Wait(launchTime + 3.034683f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 3.034683f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.207726 > Game.Music.Time) yield return Wait(launchTime + 3.207726f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 3.207726f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.614532 > Game.Music.Time) yield return Wait(launchTime + 3.614532f);
            Player.StartCoroutine(Pattern_25/*5Shots*/(launchTime + 3.614532f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.802406 > Game.Music.Time) yield return Wait(launchTime + 4.802406f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 4.802406f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.277275 > Game.Music.Time) yield return Wait(launchTime + 5.277275f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 5.277275f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.409317 > Game.Music.Time) yield return Wait(launchTime + 6.409317f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 6.409317f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.800918 > Game.Music.Time) yield return Wait(launchTime + 6.800918f);
            Player.StartCoroutine(Pattern_25/*5Shots*/(launchTime + 6.800918f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.996964 > Game.Music.Time) yield return Wait(launchTime + 7.996964f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 7.996964f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.185059 > Game.Music.Time) yield return Wait(launchTime + 8.185059f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 8.185059f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.395729 > Game.Music.Time) yield return Wait(launchTime + 8.395729f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 8.395729f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.504852 > Game.Music.Time) yield return Wait(launchTime + 8.504852f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 8.504852f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.617706 > Game.Music.Time) yield return Wait(launchTime + 8.617706f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 8.617706f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.419057 > Game.Music.Time) yield return Wait(launchTime + 9.419057f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 9.419057f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.599637 > Game.Music.Time) yield return Wait(launchTime + 9.599637f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 9.599637f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.60861 > Game.Music.Time) yield return Wait(launchTime + 12.60861f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 12.60861f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.80497 > Game.Music.Time) yield return Wait(launchTime + 12.80497f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 12.80497f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.20721 > Game.Music.Time) yield return Wait(launchTime + 13.20721f);
            Player.StartCoroutine(Pattern_25/*5Shots*/(launchTime + 13.20721f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.38882 > Game.Music.Time) yield return Wait(launchTime + 14.38882f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 14.38882f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.59196 > Game.Music.Time) yield return Wait(launchTime + 14.59196f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 14.59196f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.79511 > Game.Music.Time) yield return Wait(launchTime + 14.79511f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 14.79511f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 15.00198 > Game.Music.Time) yield return Wait(launchTime + 15.00198f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 15.00198f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 15.79961 > Game.Music.Time) yield return Wait(launchTime + 15.79961f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 15.79961f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 15.99524 > Game.Music.Time) yield return Wait(launchTime + 15.99524f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 15.99524f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 16.40481 > Game.Music.Time) yield return Wait(launchTime + 16.40481f);
            Player.StartCoroutine(Pattern_25/*5Shots*/(launchTime + 16.40481f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 17.60001 > Game.Music.Time) yield return Wait(launchTime + 17.60001f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 17.60001f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 19.21566 > Game.Music.Time) yield return Wait(launchTime + 19.21566f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 19.21566f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 19.60798 > Game.Music.Time) yield return Wait(launchTime + 19.60798f);
            Player.StartCoroutine(Pattern_25/*5Shots*/(launchTime + 19.60798f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 20.79196 > Game.Music.Time) yield return Wait(launchTime + 20.79196f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 20.79196f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 20.98382 > Game.Music.Time) yield return Wait(launchTime + 20.98382f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 20.98382f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 21.17194 > Game.Music.Time) yield return Wait(launchTime + 21.17194f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 21.17194f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 21.29611 > Game.Music.Time) yield return Wait(launchTime + 21.29611f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 21.29611f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 21.4014 > Game.Music.Time) yield return Wait(launchTime + 21.4014f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 21.4014f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 22.19897 > Game.Music.Time) yield return Wait(launchTime + 22.19897f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 22.19897f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 22.39418 > Game.Music.Time) yield return Wait(launchTime + 22.39418f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 22.39418f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 22.59058 > Game.Music.Time) yield return Wait(launchTime + 22.59058f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 22.59058f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 23.03988 > Game.Music.Time) yield return Wait(launchTime + 23.03988f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 23.03988f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 23.19459 > Game.Music.Time) yield return Wait(launchTime + 23.19459f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 23.19459f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 23.64388 > Game.Music.Time) yield return Wait(launchTime + 23.64388f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 23.64388f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 23.80452 > Game.Music.Time) yield return Wait(launchTime + 23.80452f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 23.80452f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2032623 > Game.Music.Time) yield return Wait(launchTime + 0.2032623f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 0.2032623f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.603775 > Game.Music.Time) yield return Wait(launchTime + 0.603775f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 0.603775f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.8085098 > Game.Music.Time) yield return Wait(launchTime + 0.8085098f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 0.8085098f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.010253 > Game.Music.Time) yield return Wait(launchTime + 1.010253f);
            Player.StartCoroutine(Pattern_27/*StrightShot*/(launchTime + 1.010253f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc speed_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_4.output_patterninput");
            object color = blackboard.GetValue("node_4.output_patterninput");
            blackboard.SubscribeForChanges("node_4.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, speed_input, "node_6.output_patterninput");
            object speed = blackboard.GetValue("node_6.output_patterninput");
            blackboard.SubscribeForChanges("node_6.output_patterninput", (b, o) => speed = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 0.4813766 > Game.Music.Time) yield return Wait(launchTime + 0.4813766f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 0.4813766f, blackboard/*out*/));
            if (launchTime + 3.204948 > Game.Music.Time) yield return Wait(launchTime + 3.204948f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 3.204948f, blackboard, (c,i,t,l, castc, casti) => -0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 3.68502 > Game.Music.Time) yield return Wait(launchTime + 3.68502f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 3.68502f, blackboard/*out*/));
            if (launchTime + 6.396125 > Game.Music.Time) yield return Wait(launchTime + 6.396125f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 6.396125f, blackboard, (c,i,t,l, castc, casti) => 0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 6.872818 > Game.Music.Time) yield return Wait(launchTime + 6.872818f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 6.872818f, blackboard/*out*/));
            if (launchTime + 9.59024 > Game.Music.Time) yield return Wait(launchTime + 9.59024f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 9.59024f, blackboard, (c,i,t,l, castc, casti) => -0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 10.06814 > Game.Music.Time) yield return Wait(launchTime + 10.06814f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 10.06814f, blackboard/*out*/));
            if (launchTime + 12.80274 > Game.Music.Time) yield return Wait(launchTime + 12.80274f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 12.80274f, blackboard, (c,i,t,l, castc, casti) => 0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 13.26344 > Game.Music.Time) yield return Wait(launchTime + 13.26344f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 13.26344f, blackboard/*out*/));
            if (launchTime + 16.0091 > Game.Music.Time) yield return Wait(launchTime + 16.0091f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 16.0091f, blackboard, (c,i,t,l, castc, casti) => -0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 16.48124 > Game.Music.Time) yield return Wait(launchTime + 16.48124f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 16.48124f, blackboard/*out*/));
            if (launchTime + 19.20323 > Game.Music.Time) yield return Wait(launchTime + 19.20323f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 19.20323f, blackboard, (c,i,t,l, castc, casti) => 0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 19.66906 > Game.Music.Time) yield return Wait(launchTime + 19.66906f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 19.66906f, blackboard/*out*/));
            if (launchTime + 22.4161 > Game.Music.Time) yield return Wait(launchTime + 22.4161f);
            Player.StartCoroutine(Pattern_14/*LongWob*/(launchTime + 22.4161f, blackboard, (c,i,t,l, castc, casti) => -0.5f, (c,i,t,l, castc, casti) => "node_6.output_patterninput", (c,i,t,l, castc, casti) => "node_4.output_patterninput"/*out*/));
            if (launchTime + 22.84186 > Game.Music.Time) yield return Wait(launchTime + 22.84186f);
            Player.StartCoroutine(Pattern_42/*Background*/(launchTime + 22.84186f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d/*out*/, "node_1.output_elements"));
            if (launchTime + 0.004203797 > Game.Music.Time) yield return Wait(launchTime + 0.004203797f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.004203797f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => (float)toint((float)d,3f)+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f+(float)rndf(-5f,5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xba/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => 0.8f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object bulletslist;
            blackboard.SubscribeForChanges("node_3.output_value", (b, o) => bulletslist = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullets1", (c,i,t,l, castc, casti) => "node_0.output_bullets2", (c,i,t,l, castc, casti) => "node_2.output_bullets1", (c,i,t,l, castc, casti) => "node_2.output_bullets2", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_29/*Star*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-171f*(float)ar,318f)/*out*/, "node_0.output_bullets1", "node_0.output_bullets2"));
            if (launchTime + 0.6035004 > Game.Music.Time) yield return Wait(launchTime + 0.6035004f);
            Player.StartCoroutine(Pattern_29/*Star*/(launchTime + 0.6035004f, blackboard, (c,i,t,l, castc, casti) => new Vector2(171f*(float)ar,318f)/*out*/, "node_2.output_bullets1", "node_2.output_bullets2"));
            if (launchTime + 2.014237 > Game.Music.Time) yield return Wait(launchTime + 2.014237f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 2.014237f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => 300f+(float)rndf(-50f,50f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 2.02301 > Game.Music.Time) yield return Wait(launchTime + 2.02301f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.02301f, blackboard, (c,i,t,l, castc, casti) => new Color(0x72/255f,0x14/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1066666f));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard, InputFunc position_input, string bullets1_output, string bullets2_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, position_input, "node_0.output_patterninput");
            object position = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => position = o);

            SubscribeOutput(outboard, blackboard, bullets1_output, "node_5.output_bullet(s)");
            SubscribeOutput(outboard, blackboard, bullets2_output, "node_7.output_bullet(s)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.005729556 > Game.Music.Time) yield return Wait(launchTime + 0.005729556f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.005729556f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-163f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_2.output_enemy(ies)", 0.7948837f));
            if (launchTime + 0.820549 > Game.Music.Time) yield return Wait(launchTime + 0.820549f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.820549f, blackboard, (c,i,t,l, castc, casti) => new Color(0x33/255f,0x14/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1591263f));
            if (launchTime + 0.8293838 > Game.Music.Time) yield return Wait(launchTime + 0.8293838f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.8293838f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => 1f+(float)d*3f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 75f, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)rndf(-5f,5f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 1.030609 > Game.Music.Time) yield return Wait(launchTime + 1.030609f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.030609f, blackboard, (c,i,t,l, castc, casti) => new Color(0x33/255f,0x14/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1591263f));
            if (launchTime + 1.049644 > Game.Music.Time) yield return Wait(launchTime + 1.049644f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.049644f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => 3f+(float)d*2f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 150f, (c,i,t,l, castc, casti) => ParticleBulletType.Large, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            if (launchTime + 2.846771 > Game.Music.Time) yield return Wait(launchTime + 2.846771f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 2.846771f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,275f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_10.output_enemy(ies)", 0.803627f));
            if (launchTime + 3.674423 > Game.Music.Time) yield return Wait(launchTime + 3.674423f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 3.674423f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.09021759 > Game.Music.Time) yield return Wait(launchTime + 0.09021759f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.09021759f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.186348 > Game.Music.Time) yield return Wait(launchTime + 0.186348f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.186348f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.5871655 > Game.Music.Time) yield return Wait(launchTime + 0.5871655f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.5871655f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 0.6853865 > Game.Music.Time) yield return Wait(launchTime + 0.6853865f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.6853865f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.7855985 > Game.Music.Time) yield return Wait(launchTime + 0.7855985f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.7855985f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.6119995 > Game.Music.Time) yield return Wait(launchTime + 0.6119995f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 0.6119995f, blackboard/*out*/));
            if (launchTime + 1.211456 > Game.Music.Time) yield return Wait(launchTime + 1.211456f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 1.211456f, blackboard/*out*/));
            if (launchTime + 1.814819 > Game.Music.Time) yield return Wait(launchTime + 1.814819f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 1.814819f, blackboard/*out*/));
            if (launchTime + 2.414276 > Game.Music.Time) yield return Wait(launchTime + 2.414276f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 2.414276f, blackboard/*out*/));
            if (launchTime + 3.003967 > Game.Music.Time) yield return Wait(launchTime + 3.003967f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 3.003967f, blackboard/*out*/));
            if (launchTime + 3.205062 > Game.Music.Time) yield return Wait(launchTime + 3.205062f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 3.205062f, blackboard/*out*/));
            if (launchTime + 3.806457 > Game.Music.Time) yield return Wait(launchTime + 3.806457f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 3.806457f, blackboard/*out*/));
            if (launchTime + 4.403961 > Game.Music.Time) yield return Wait(launchTime + 4.403961f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 4.403961f, blackboard/*out*/));
            if (launchTime + 4.997619 > Game.Music.Time) yield return Wait(launchTime + 4.997619f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 4.997619f, blackboard/*out*/));
            if (launchTime + 5.595108 > Game.Music.Time) yield return Wait(launchTime + 5.595108f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 5.595108f, blackboard/*out*/));
            if (launchTime + 6.20433 > Game.Music.Time) yield return Wait(launchTime + 6.20433f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 6.20433f, blackboard/*out*/));
            if (launchTime + 6.40155 > Game.Music.Time) yield return Wait(launchTime + 6.40155f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 6.40155f, blackboard/*out*/));
            if (launchTime + 7.00882 > Game.Music.Time) yield return Wait(launchTime + 7.00882f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 7.00882f, blackboard/*out*/));
            if (launchTime + 7.612183 > Game.Music.Time) yield return Wait(launchTime + 7.612183f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 7.612183f, blackboard/*out*/));
            if (launchTime + 8.205765 > Game.Music.Time) yield return Wait(launchTime + 8.205765f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 8.205765f, blackboard/*out*/));
            if (launchTime + 8.809114 > Game.Music.Time) yield return Wait(launchTime + 8.809114f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 8.809114f, blackboard/*out*/));
            if (launchTime + 9.410553 > Game.Music.Time) yield return Wait(launchTime + 9.410553f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 9.410553f, blackboard/*out*/));
            if (launchTime + 9.603851 > Game.Music.Time) yield return Wait(launchTime + 9.603851f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 9.603851f, blackboard/*out*/));
            if (launchTime + 10.20721 > Game.Music.Time) yield return Wait(launchTime + 10.20721f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 10.20721f, blackboard/*out*/));
            if (launchTime + 10.81253 > Game.Music.Time) yield return Wait(launchTime + 10.81253f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 10.81253f, blackboard/*out*/));
            if (launchTime + 11.40023 > Game.Music.Time) yield return Wait(launchTime + 11.40023f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 11.40023f, blackboard/*out*/));
            if (launchTime + 12.00555 > Game.Music.Time) yield return Wait(launchTime + 12.00555f);
            Player.StartCoroutine(Pattern_32/*Shoot*/(launchTime + 12.00555f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),(float)rndf(0f,220f)), (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x00/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.008361818 > Game.Music.Time) yield return Wait(launchTime + 0.008361818f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.008361818f, blackboard, (c,i,t,l, castc, casti) => new Color(0x29/255f,0x2a/255f,0x42/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.05410767f));
            if (launchTime + 0.008499146 > Game.Music.Time) yield return Wait(launchTime + 0.008499146f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.008499146f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => 2f+(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 50f, (c,i,t,l, castc, casti) => 150f+(float)rndf(0f,100f), (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.09889221 > Game.Music.Time) yield return Wait(launchTime + 0.09889221f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.09889221f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,149f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 12f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.01127619 > Game.Music.Time) yield return Wait(launchTime + 0.01127619f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.01127619f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_1.output_enemy(ies)", 0.58078f));
            if (launchTime + 0.5967407 > Game.Music.Time) yield return Wait(launchTime + 0.5967407f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.5967407f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.2f, (c,i,t,l, castc, casti) => 1f+(float)t*((float)d*2f - 1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)t*1440f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => (float)t+0.2f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 12.78366f));
            if (launchTime + 13.40319 > Game.Music.Time) yield return Wait(launchTime + 13.40319f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 13.40319f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_4.output_enemy(ies)", 0.7947235f));
            if (launchTime + 14.20065 > Game.Music.Time) yield return Wait(launchTime + 14.20065f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 14.20065f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_35/*FirstElement*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 3.198814 > Game.Music.Time) yield return Wait(launchTime + 3.198814f);
            Player.StartCoroutine(Pattern_18/*ShortWob*/(launchTime + 3.198814f, blackboard/*out*/));
            if (launchTime + 6.396065 > Game.Music.Time) yield return Wait(launchTime + 6.396065f);
            Player.StartCoroutine(Pattern_35/*FirstElement*/(launchTime + 6.396065f, blackboard/*out*/));
            if (launchTime + 9.614367 > Game.Music.Time) yield return Wait(launchTime + 9.614367f);
            Player.StartCoroutine(Pattern_18/*ShortWob*/(launchTime + 9.614367f, blackboard/*out*/));
            if (launchTime + 12.80316 > Game.Music.Time) yield return Wait(launchTime + 12.80316f);
            Player.StartCoroutine(Pattern_35/*FirstElement*/(launchTime + 12.80316f, blackboard/*out*/));
            if (launchTime + 16.00243 > Game.Music.Time) yield return Wait(launchTime + 16.00243f);
            Player.StartCoroutine(Pattern_18/*ShortWob*/(launchTime + 16.00243f, blackboard/*out*/));
            if (launchTime + 19.21701 > Game.Music.Time) yield return Wait(launchTime + 19.21701f);
            Player.StartCoroutine(Pattern_35/*FirstElement*/(launchTime + 19.21701f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.2012787 > Game.Music.Time) yield return Wait(launchTime + 0.2012787f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0.2012787f, blackboard/*out*/));
            if (launchTime + 0.2694549 > Game.Music.Time) yield return Wait(launchTime + 0.2694549f);
            Player.StartCoroutine(Pattern_36/*Music*/(launchTime + 0.2694549f, blackboard/*out*/));
            if (launchTime + 0.5916748 > Game.Music.Time) yield return Wait(launchTime + 0.5916748f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0.5916748f, blackboard/*out*/));
            if (launchTime + 0.680244 > Game.Music.Time) yield return Wait(launchTime + 0.680244f);
            Player.StartCoroutine(Pattern_36/*Music*/(launchTime + 0.680244f, blackboard/*out*/));
            if (launchTime + 0.7868805 > Game.Music.Time) yield return Wait(launchTime + 0.7868805f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 0.7868805f, blackboard/*out*/));
            if (launchTime + 1.277702 > Game.Music.Time) yield return Wait(launchTime + 1.277702f);
            Player.StartCoroutine(Pattern_36/*Music*/(launchTime + 1.277702f, blackboard/*out*/));
            if (launchTime + 1.854927 > Game.Music.Time) yield return Wait(launchTime + 1.854927f);
            Player.StartCoroutine(Pattern_36/*Music*/(launchTime + 1.854927f, blackboard/*out*/));
            if (launchTime + 2.190032 > Game.Music.Time) yield return Wait(launchTime + 2.190032f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.190032f, blackboard/*out*/));
            if (launchTime + 2.390853 > Game.Music.Time) yield return Wait(launchTime + 2.390853f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.390853f, blackboard/*out*/));
            if (launchTime + 2.465904 > Game.Music.Time) yield return Wait(launchTime + 2.465904f);
            Player.StartCoroutine(Pattern_36/*Music*/(launchTime + 2.465904f, blackboard/*out*/));
            if (launchTime + 3.202476 > Game.Music.Time) yield return Wait(launchTime + 3.202476f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 3.202476f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object depth = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 100f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 200f));
            blackboard.SetValue("node_0.output_value", depth);

            object position = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -300f*(float)ar), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 300f));
            blackboard.SetValue("node_1.output_value", position);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy11, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2((float)position,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.01570892 > Game.Music.Time) yield return Wait(launchTime + 0.01570892f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.01570892f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-(float)depth), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutCubic/*out*/, "node_3.output_enemy(ies)", 0.5085831f));
            if (launchTime + 0.5266342 > Game.Music.Time) yield return Wait(launchTime + 0.5266342f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.5266342f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => 2f*(float)d - 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.BulletToPlayer, (c,i,t,l, castc, casti) => 20f/(float)c*((float)i+0.5f)-10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x8d/255f,0x5a/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 2.116241 > Game.Music.Time) yield return Wait(launchTime + 2.116241f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 2.116241f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,(float)depth), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeInCubic/*out*/, "node_6.output_enemy(ies)", 0.6221085f));
            if (launchTime + 2.752823 > Game.Music.Time) yield return Wait(launchTime + 2.752823f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.752823f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,134f), (c,i,t,l, castc, casti) => 0.04f, (c,i,t,l, castc, casti) => 1f+(float)t*2f*(float)d, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => (1f-(float)t)*(50f+35f*(float)d)+30f, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x56/255f,0x00/255f,0xcc/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f/*out*/, "node_0.output_bullet(s)", 0.7375488f));
            if (launchTime + 0.0007553101 > Game.Music.Time) yield return Wait(launchTime + 0.0007553101f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.0007553101f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x27/255f,0x14/255f,0x38/255f)/*out*/, 0.692955f));
            if (launchTime + 0.09522241 > Game.Music.Time) yield return Wait(launchTime + 0.09522241f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.09522241f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,134f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.1337966 > Game.Music.Time) yield return Wait(launchTime + 0.1337966f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.1337966f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_3.output_enemy(ies)", 0.6043625f));
            if (launchTime + 0.7138671 > Game.Music.Time) yield return Wait(launchTime + 0.7138671f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.7138671f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.1934967f));
            if (launchTime + 0.8142471 > Game.Music.Time) yield return Wait(launchTime + 0.8142471f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 0.8142471f, blackboard/*out*/));
            if (launchTime + 1.219612 > Game.Music.Time) yield return Wait(launchTime + 1.219612f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 1.219612f, blackboard/*out*/));
            if (launchTime + 1.423912 > Game.Music.Time) yield return Wait(launchTime + 1.423912f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 1.423912f, blackboard/*out*/));
            if (launchTime + 1.798485 > Game.Music.Time) yield return Wait(launchTime + 1.798485f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 1.798485f, blackboard/*out*/));
            if (launchTime + 2.016433 > Game.Music.Time) yield return Wait(launchTime + 2.016433f);
            Player.StartCoroutine(Pattern_38/*Shot*/(launchTime + 2.016433f, blackboard/*out*/));
            if (launchTime + 2.203239 > Game.Music.Time) yield return Wait(launchTime + 2.203239f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.203239f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            if (launchTime + 2.209877 > Game.Music.Time) yield return Wait(launchTime + 2.209877f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 2.209877f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_12.output_enemy(ies)", 0.6020584f));
            if (launchTime + 2.212081 > Game.Music.Time) yield return Wait(launchTime + 2.212081f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.212081f, blackboard/*out*/));
            if (launchTime + 2.395271 > Game.Music.Time) yield return Wait(launchTime + 2.395271f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.395271f, blackboard/*out*/));
            if (launchTime + 2.813895 > Game.Music.Time) yield return Wait(launchTime + 2.813895f);
            Player.StartCoroutine(Pattern_9/*Beat*/(launchTime + 2.813895f, blackboard/*out*/));
            if (launchTime + 2.831589 > Game.Music.Time) yield return Wait(launchTime + 2.831589f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.831589f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,134f), (c,i,t,l, castc, casti) => 3f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-15f,15f)*(float)d,-200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x47/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4960289f));
            if (launchTime + 23.64434 > Game.Music.Time) yield return Wait(launchTime + 23.64434f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 23.64434f, blackboard, (c,i,t,l, castc, casti) => new Color(0x42/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4745178f));
            if (launchTime + 24.21407 > Game.Music.Time) yield return Wait(launchTime + 24.21407f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 24.21407f, blackboard, (c,i,t,l, castc, casti) => new Color(0x42/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.323204f));
            if (launchTime + 24.81141 > Game.Music.Time) yield return Wait(launchTime + 24.81141f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 24.81141f, blackboard, (c,i,t,l, castc, casti) => new Color(0x42/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.323204f));
            if (launchTime + 25.19579 > Game.Music.Time) yield return Wait(launchTime + 25.19579f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.19579f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f), (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f)/*out*/, 0.4039154f));
            if (launchTime + 25.64741 > Game.Music.Time) yield return Wait(launchTime + 25.64741f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.64741f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f), (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x14/255f,0x12/255f)/*out*/, 12.77914f));
            if (launchTime + 47.28843 > Game.Music.Time) yield return Wait(launchTime + 47.28843f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 47.28843f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc4/255f,0xc3/255f,0xc3/255f)/*out*/, 3.125572f));
            if (launchTime + 50.41776 > Game.Music.Time) yield return Wait(launchTime + 50.41776f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 50.41776f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f), (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f)/*out*/, 0.08473969f));
            if (launchTime + 50.41791 > Game.Music.Time) yield return Wait(launchTime + 50.41791f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 50.41791f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1808472f));
            if (launchTime + 51.21098 > Game.Music.Time) yield return Wait(launchTime + 51.21098f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 51.21098f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1278801f));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 12.82405 > Game.Music.Time) yield return Wait(launchTime + 12.82405f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 12.82405f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 25.61021 > Game.Music.Time) yield return Wait(launchTime + 25.61021f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 25.61021f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 36.40007 > Game.Music.Time) yield return Wait(launchTime + 36.40007f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 36.40007f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 38.4143 > Game.Music.Time) yield return Wait(launchTime + 38.4143f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 38.4143f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 41.59336 > Game.Music.Time) yield return Wait(launchTime + 41.59336f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 41.59336f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*4f/*out*/));
            if (launchTime + 42.26473 > Game.Music.Time) yield return Wait(launchTime + 42.26473f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 42.26473f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 44.83164 > Game.Music.Time) yield return Wait(launchTime + 44.83164f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 44.83164f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*4f/*out*/));
            if (launchTime + 45.41922 > Game.Music.Time) yield return Wait(launchTime + 45.41922f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 45.41922f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 48.02169 > Game.Music.Time) yield return Wait(launchTime + 48.02169f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 48.02169f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*4f/*out*/));
            if (launchTime + 48.59037 > Game.Music.Time) yield return Wait(launchTime + 48.59037f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 48.59037f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 50.38756 > Game.Music.Time) yield return Wait(launchTime + 50.38756f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 50.38756f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*8f/*out*/));
            if (launchTime + 51.26627 > Game.Music.Time) yield return Wait(launchTime + 51.26627f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 51.26627f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 56.79857 > Game.Music.Time) yield return Wait(launchTime + 56.79857f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 56.79857f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*4f/*out*/));
            if (launchTime + 57.592 > Game.Music.Time) yield return Wait(launchTime + 57.592f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 57.592f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 63.20543 > Game.Music.Time) yield return Wait(launchTime + 63.20543f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 63.20543f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 64.0041 > Game.Music.Time) yield return Wait(launchTime + 64.0041f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 64.0041f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 86.42937 > Game.Music.Time) yield return Wait(launchTime + 86.42937f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 86.42937f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 87.21758 > Game.Music.Time) yield return Wait(launchTime + 87.21758f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 87.21758f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 89.62189 > Game.Music.Time) yield return Wait(launchTime + 89.62189f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 89.62189f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 112.7764 > Game.Music.Time) yield return Wait(launchTime + 112.7764f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 112.7764f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*8f/*out*/));
            if (launchTime + 115.2079 > Game.Music.Time) yield return Wait(launchTime + 115.2079f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 115.2079f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 116.0015 > Game.Music.Time) yield return Wait(launchTime + 116.0015f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 116.0015f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 118.4083 > Game.Music.Time) yield return Wait(launchTime + 118.4083f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 118.4083f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 122.3964 > Game.Music.Time) yield return Wait(launchTime + 122.3964f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 122.3964f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 124.8115 > Game.Music.Time) yield return Wait(launchTime + 124.8115f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 124.8115f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 128.7996 > Game.Music.Time) yield return Wait(launchTime + 128.7996f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 128.7996f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 131.2166 > Game.Music.Time) yield return Wait(launchTime + 131.2166f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 131.2166f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 135.2047 > Game.Music.Time) yield return Wait(launchTime + 135.2047f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 135.2047f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 138.3954 > Game.Music.Time) yield return Wait(launchTime + 138.3954f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 138.3954f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 140.8199 > Game.Music.Time) yield return Wait(launchTime + 140.8199f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 140.8199f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 74.859f/*out*/));
            if (launchTime + 153.6088 > Game.Music.Time) yield return Wait(launchTime + 153.6088f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 153.6088f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 74.859f*2f/*out*/));
            if (launchTime + 179.2071 > Game.Music.Time) yield return Wait(launchTime + 179.2071f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 179.2071f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 74.859f/*out*/));
            if (launchTime + 192.0679 > Game.Music.Time) yield return Wait(launchTime + 192.0679f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 192.0679f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_41(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x4e/255f,0x12/255f), (c,i,t,l, castc, casti) => new Color(0x1a/255f,0x14/255f,0x12/255f)/*out*/, 0.8784027f));
            yield break;
        }

        private IEnumerator Pattern_42(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x27/255f,0x2a/255f,0x12/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.9098892f));
            yield break;
        }

        private IEnumerator Pattern_43(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 50f*(float)d, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => 380f, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => -600f, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x2d/255f,0x47/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => true, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)"));
            if (launchTime + 0.6080933 > Game.Music.Time) yield return Wait(launchTime + 0.6080933f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 0.6080933f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 1.220627 > Game.Music.Time) yield return Wait(launchTime + 1.220627f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 1.220627f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 1.847809 > Game.Music.Time) yield return Wait(launchTime + 1.847809f);
            Player.StartCoroutine(SetBulletPolar.Act(launchTime + 1.847809f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 2.418747 > Game.Music.Time) yield return Wait(launchTime + 2.418747f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.418747f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Fade/*out*/));
            yield break;
        }

        private IEnumerator Pattern_44(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc flipy_input, InputFunc flipvariants_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_2.output_patterninput");
            object flipvariants = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipvariants = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_3.output_enemy"));
            if (launchTime + 2.368958 > Game.Music.Time) yield return Wait(launchTime + 2.368958f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 2.368958f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 2.56601 > Game.Music.Time) yield return Wait(launchTime + 2.56601f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 2.56601f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 3.20813 > Game.Music.Time) yield return Wait(launchTime + 3.20813f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 3.20813f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_7.output_enemy"));
            if (launchTime + 5.567521 > Game.Music.Time) yield return Wait(launchTime + 5.567521f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 5.567521f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy"/*out*/));
            if (launchTime + 5.771698 > Game.Music.Time) yield return Wait(launchTime + 5.771698f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 5.771698f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy"/*out*/));
            if (launchTime + 6.164216 > Game.Music.Time) yield return Wait(launchTime + 6.164216f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 6.164216f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy"/*out*/));
            if (launchTime + 6.4189 > Game.Music.Time) yield return Wait(launchTime + 6.4189f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 6.4189f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_11.output_enemy"));
            if (launchTime + 8.75766 > Game.Music.Time) yield return Wait(launchTime + 8.75766f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 8.75766f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_enemy"/*out*/));
            if (launchTime + 8.963501 > Game.Music.Time) yield return Wait(launchTime + 8.963501f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 8.963501f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_enemy"/*out*/));
            if (launchTime + 9.601227 > Game.Music.Time) yield return Wait(launchTime + 9.601227f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 9.601227f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_14.output_enemy"));
            if (launchTime + 11.96961 > Game.Music.Time) yield return Wait(launchTime + 11.96961f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 11.96961f, blackboard, (c,i,t,l, castc, casti) => "node_14.output_enemy"/*out*/));
            if (launchTime + 12.1595 > Game.Music.Time) yield return Wait(launchTime + 12.1595f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 12.1595f, blackboard, (c,i,t,l, castc, casti) => "node_14.output_enemy"/*out*/));
            if (launchTime + 12.56827 > Game.Music.Time) yield return Wait(launchTime + 12.56827f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 12.56827f, blackboard, (c,i,t,l, castc, casti) => "node_14.output_enemy"/*out*/));
            if (launchTime + 12.77427 > Game.Music.Time) yield return Wait(launchTime + 12.77427f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 12.77427f, blackboard, (c,i,t,l, castc, casti) => "node_14.output_enemy"/*out*/));
            if (launchTime + 12.81203 > Game.Music.Time) yield return Wait(launchTime + 12.81203f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 12.81203f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_19.output_enemy"));
            if (launchTime + 15.17574 > Game.Music.Time) yield return Wait(launchTime + 15.17574f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 15.17574f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_enemy"/*out*/));
            if (launchTime + 15.37162 > Game.Music.Time) yield return Wait(launchTime + 15.37162f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 15.37162f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_enemy"/*out*/));
            if (launchTime + 16.02277 > Game.Music.Time) yield return Wait(launchTime + 16.02277f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 16.02277f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_22.output_enemy"));
            if (launchTime + 18.3681 > Game.Music.Time) yield return Wait(launchTime + 18.3681f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 18.3681f, blackboard, (c,i,t,l, castc, casti) => "node_22.output_enemy"/*out*/));
            if (launchTime + 18.55799 > Game.Music.Time) yield return Wait(launchTime + 18.55799f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 18.55799f, blackboard, (c,i,t,l, castc, casti) => "node_22.output_enemy"/*out*/));
            if (launchTime + 18.96999 > Game.Music.Time) yield return Wait(launchTime + 18.96999f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 18.96999f, blackboard, (c,i,t,l, castc, casti) => "node_22.output_enemy"/*out*/));
            if (launchTime + 19.20983 > Game.Music.Time) yield return Wait(launchTime + 19.20983f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 19.20983f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_26.output_enemy"));
            if (launchTime + 21.57444 > Game.Music.Time) yield return Wait(launchTime + 21.57444f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 21.57444f, blackboard, (c,i,t,l, castc, casti) => "node_26.output_enemy"/*out*/));
            if (launchTime + 21.77015 > Game.Music.Time) yield return Wait(launchTime + 21.77015f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 21.77015f, blackboard, (c,i,t,l, castc, casti) => "node_26.output_enemy"/*out*/));
            if (launchTime + 22.41979 > Game.Music.Time) yield return Wait(launchTime + 22.41979f);
            Player.StartCoroutine(Pattern_45/*DefaultEnemy*/(launchTime + 22.41979f, blackboard, (c,i,t,l, castc, casti) => (float)flipx/*out*/, "node_29.output_enemy"));
            if (launchTime + 24.37399 > Game.Music.Time) yield return Wait(launchTime + 24.37399f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 24.37399f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy"/*out*/));
            if (launchTime + 24.98553 > Game.Music.Time) yield return Wait(launchTime + 24.98553f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 24.98553f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy"/*out*/));
            if (launchTime + 25.36533 > Game.Music.Time) yield return Wait(launchTime + 25.36533f);
            Player.StartCoroutine(Pattern_46/*ShotVar1*/(launchTime + 25.36533f, blackboard, (c,i,t,l, castc, casti) => "node_29.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_45(float launchTime, Blackboard outboard, InputFunc flipx_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_1.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-240f*(float)ar*(float)flipx,279f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.03553769 > Game.Music.Time) yield return Wait(launchTime + 0.03553769f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.03553769f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-240f*(float)ar*(float)flipx,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_2.output_enemy(ies)", 0.1594543f));
            if (launchTime + 0.9690399 > Game.Music.Time) yield return Wait(launchTime + 0.9690399f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.9690399f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-170f*(float)ar*(float)flipx,180f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.08946228f));
            if (launchTime + 1.374435 > Game.Music.Time) yield return Wait(launchTime + 1.374435f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 1.374435f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-170f*(float)ar*(float)flipx,130f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_6.output_enemy(ies)", 0.1214905f));
            if (launchTime + 3.379837 > Game.Music.Time) yield return Wait(launchTime + 3.379837f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 3.379837f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-350f*(float)ar*(float)flipx,130f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_7.output_enemy(ies)", 0.2677765f));
            if (launchTime + 3.68103 > Game.Music.Time) yield return Wait(launchTime + 3.68103f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 3.68103f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_46(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d-3f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-200f,200f),(float)rndf(-100f,200f)), (c,i,t,l, castc, casti) => new Vector2(0f,-100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x58/255f,0x0c/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_47(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_48/*BigBullet*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 3.222122 > Game.Music.Time) yield return Wait(launchTime + 3.222122f);
            Player.StartCoroutine(Pattern_48/*BigBullet*/(launchTime + 3.222122f, blackboard/*out*/));
            if (launchTime + 6.424407 > Game.Music.Time) yield return Wait(launchTime + 6.424407f);
            Player.StartCoroutine(Pattern_48/*BigBullet*/(launchTime + 6.424407f, blackboard/*out*/));
            if (launchTime + 9.600646 > Game.Music.Time) yield return Wait(launchTime + 9.600646f);
            Player.StartCoroutine(Pattern_48/*BigBullet*/(launchTime + 9.600646f, blackboard/*out*/));
            if (launchTime + 12.80452 > Game.Music.Time) yield return Wait(launchTime + 12.80452f);
            Player.StartCoroutine(Pattern_49/*Bullet2*/(launchTime + 12.80452f, blackboard/*out*/));
            if (launchTime + 15.99499 > Game.Music.Time) yield return Wait(launchTime + 15.99499f);
            Player.StartCoroutine(Pattern_49/*Bullet2*/(launchTime + 15.99499f, blackboard/*out*/));
            if (launchTime + 19.1933 > Game.Music.Time) yield return Wait(launchTime + 19.1933f);
            Player.StartCoroutine(Pattern_49/*Bullet2*/(launchTime + 19.1933f, blackboard/*out*/));
            if (launchTime + 22.38678 > Game.Music.Time) yield return Wait(launchTime + 22.38678f);
            Player.StartCoroutine(Pattern_49/*Bullet2*/(launchTime + 22.38678f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_48(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object speed = blackboard.GetValue((c, i, t, l, castc, casti) => 100f+30f*(float)d);
            blackboard.SetValue("node_0.output_value", speed);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),200f), (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-(float)speed), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.6030274 > Game.Music.Time) yield return Wait(launchTime + 0.6030274f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.6030274f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            if (launchTime + 0.6127625 > Game.Music.Time) yield return Wait(launchTime + 0.6127625f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.6127625f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.201248 > Game.Music.Time) yield return Wait(launchTime + 1.201248f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.201248f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            if (launchTime + 1.207733 > Game.Music.Time) yield return Wait(launchTime + 1.207733f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.207733f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.772675 > Game.Music.Time) yield return Wait(launchTime + 1.772675f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.772675f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)"));
            if (launchTime + 1.786057 > Game.Music.Time) yield return Wait(launchTime + 1.786057f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.786057f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.175034 > Game.Music.Time) yield return Wait(launchTime + 2.175034f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.175034f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)"));
            if (launchTime + 2.185181 > Game.Music.Time) yield return Wait(launchTime + 2.185181f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.185181f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.365845 > Game.Music.Time) yield return Wait(launchTime + 2.365845f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.365845f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_11.output_bullet(s)"));
            if (launchTime + 2.379105 > Game.Music.Time) yield return Wait(launchTime + 2.379105f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.379105f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.756317 > Game.Music.Time) yield return Wait(launchTime + 2.756317f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.756317f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_13.output_bullet(s)"));
            if (launchTime + 2.783112 > Game.Music.Time) yield return Wait(launchTime + 2.783112f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.783112f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.973923 > Game.Music.Time) yield return Wait(launchTime + 2.973923f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.973923f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xc1/255f,0x2a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_15.output_bullet(s)"));
            if (launchTime + 2.979737 > Game.Music.Time) yield return Wait(launchTime + 2.979737f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.979737f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 3.177185 > Game.Music.Time) yield return Wait(launchTime + 3.177185f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 3.177185f, blackboard, (c,i,t,l, castc, casti) => "node_15.output_bullet(s)", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_17.output_bullet(s)"));
            if (launchTime + 3.184677 > Game.Music.Time) yield return Wait(launchTime + 3.184677f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 3.184677f, blackboard, (c,i,t,l, castc, casti) => "node_15.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_49(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object speed = blackboard.GetValue((c, i, t, l, castc, casti) => 100f+30f*(float)d);
            blackboard.SetValue("node_0.output_value", speed);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),200f), (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-(float)speed), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.0006256111 > Game.Music.Time) yield return Wait(launchTime + 0.0006256111f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.0006256111f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-300f,300f),200f), (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-(float)speed), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.4031067 > Game.Music.Time) yield return Wait(launchTime + 0.4031067f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.4031067f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.4186096 > Game.Music.Time) yield return Wait(launchTime + 0.4186096f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.4186096f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 0.6060028 > Game.Music.Time) yield return Wait(launchTime + 0.6060028f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.6060028f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_bullet(s)"));
            if (launchTime + 0.6137848 > Game.Music.Time) yield return Wait(launchTime + 0.6137848f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.6137848f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 0.9833221 > Game.Music.Time) yield return Wait(launchTime + 0.9833221f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.9833221f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_bullet(s)"));
            if (launchTime + 0.9939117 > Game.Music.Time) yield return Wait(launchTime + 0.9939117f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 0.9939117f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.189407 > Game.Music.Time) yield return Wait(launchTime + 1.189407f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.189407f, blackboard, (c,i,t,l, castc, casti) => "node_6.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_bullet(s)"));
            if (launchTime + 1.196655 > Game.Music.Time) yield return Wait(launchTime + 1.196655f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.196655f, blackboard, (c,i,t,l, castc, casti) => "node_6.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.604797 > Game.Music.Time) yield return Wait(launchTime + 1.604797f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.604797f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_12.output_bullet(s)"));
            if (launchTime + 1.614792 > Game.Music.Time) yield return Wait(launchTime + 1.614792f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.614792f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 1.801346 > Game.Music.Time) yield return Wait(launchTime + 1.801346f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 1.801346f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_14.output_bullet(s)"));
            if (launchTime + 1.809982 > Game.Music.Time) yield return Wait(launchTime + 1.809982f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 1.809982f, blackboard, (c,i,t,l, castc, casti) => "node_10.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.197677 > Game.Music.Time) yield return Wait(launchTime + 2.197677f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.197677f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_16.output_bullet(s)"));
            if (launchTime + 2.205307 > Game.Music.Time) yield return Wait(launchTime + 2.205307f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.205307f, blackboard, (c,i,t,l, castc, casti) => "node_12.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.384796 > Game.Music.Time) yield return Wait(launchTime + 2.384796f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.384796f, blackboard, (c,i,t,l, castc, casti) => "node_14.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_18.output_bullet(s)"));
            if (launchTime + 2.390381 > Game.Music.Time) yield return Wait(launchTime + 2.390381f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.390381f, blackboard, (c,i,t,l, castc, casti) => "node_14.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.784332 > Game.Music.Time) yield return Wait(launchTime + 2.784332f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.784332f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_20.output_bullet(s)"));
            if (launchTime + 2.790817 > Game.Music.Time) yield return Wait(launchTime + 2.790817f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 2.790817f, blackboard, (c,i,t,l, castc, casti) => "node_16.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 2.996704 > Game.Music.Time) yield return Wait(launchTime + 2.996704f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 2.996704f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_bullet(s)", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.LongSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)speed, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x3a/255f,0x00/255f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_22.output_bullet(s)"));
            if (launchTime + 3.001175 > Game.Music.Time) yield return Wait(launchTime + 3.001175f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 3.001175f, blackboard, (c,i,t,l, castc, casti) => "node_18.output_bullet(s)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 3.189956 > Game.Music.Time) yield return Wait(launchTime + 3.189956f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 3.189956f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_bullet(s)", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x88/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_24.output_bullet(s)"));
            if (launchTime + 3.19313 > Game.Music.Time) yield return Wait(launchTime + 3.19313f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 3.19313f, blackboard, (c,i,t,l, castc, casti) => "node_22.output_bullet(s)", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x88/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_25.output_bullet(s)"));
            if (launchTime + 3.201645 > Game.Music.Time) yield return Wait(launchTime + 3.201645f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 3.201645f, blackboard, (c,i,t,l, castc, casti) => "node_20.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 3.205734 > Game.Music.Time) yield return Wait(launchTime + 3.205734f);
            Player.StartCoroutine(DestroyBullet.Act(launchTime + 3.205734f, blackboard, (c,i,t,l, castc, casti) => "node_22.output_bullet(s)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_50(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2757874f));
            if (launchTime + 0.6078949 > Game.Music.Time) yield return Wait(launchTime + 0.6078949f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.6078949f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 1.202072 > Game.Music.Time) yield return Wait(launchTime + 1.202072f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.202072f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 1.813355 > Game.Music.Time) yield return Wait(launchTime + 1.813355f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.813355f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 2.381882 > Game.Music.Time) yield return Wait(launchTime + 2.381882f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.381882f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 2.993149 > Game.Music.Time) yield return Wait(launchTime + 2.993149f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.993149f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 3.202606 > Game.Music.Time) yield return Wait(launchTime + 3.202606f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.202606f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 3.801041 > Game.Music.Time) yield return Wait(launchTime + 3.801041f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.801041f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028353f));
            if (launchTime + 4.395218 > Game.Music.Time) yield return Wait(launchTime + 4.395218f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.395218f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 4.997956 > Game.Music.Time) yield return Wait(launchTime + 4.997956f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.997956f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 5.596405 > Game.Music.Time) yield return Wait(launchTime + 5.596405f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.596405f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 6.194855 > Game.Music.Time) yield return Wait(launchTime + 6.194855f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.194855f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 6.404312 > Game.Music.Time) yield return Wait(launchTime + 6.404312f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.404312f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 7.015549 > Game.Music.Time) yield return Wait(launchTime + 7.015549f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.015549f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 7.601181 > Game.Music.Time) yield return Wait(launchTime + 7.601181f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.601181f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 8.225281 > Game.Music.Time) yield return Wait(launchTime + 8.225281f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.225281f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 8.789536 > Game.Music.Time) yield return Wait(launchTime + 8.789536f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.789536f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 9.400818 > Game.Music.Time) yield return Wait(launchTime + 9.400818f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.400818f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 9.610274 > Game.Music.Time) yield return Wait(launchTime + 9.610274f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.610274f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 10.20871 > Game.Music.Time) yield return Wait(launchTime + 10.20871f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.20871f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 10.7986 > Game.Music.Time) yield return Wait(launchTime + 10.7986f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 10.7986f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 11.39706 > Game.Music.Time) yield return Wait(launchTime + 11.39706f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.39706f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 11.99975 > Game.Music.Time) yield return Wait(launchTime + 11.99975f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.99975f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 12.5854 > Game.Music.Time) yield return Wait(launchTime + 12.5854f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.5854f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 12.8034 > Game.Music.Time) yield return Wait(launchTime + 12.8034f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.8034f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 13.18811 > Game.Music.Time) yield return Wait(launchTime + 13.18811f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.18811f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 13.4104 > Game.Music.Time) yield return Wait(launchTime + 13.4104f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.4104f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 13.80793 > Game.Music.Time) yield return Wait(launchTime + 13.80793f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.80793f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 14.00457 > Game.Music.Time) yield return Wait(launchTime + 14.00457f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.00457f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 14.39784 > Game.Music.Time) yield return Wait(launchTime + 14.39784f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.39784f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 14.6073 > Game.Music.Time) yield return Wait(launchTime + 14.6073f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.6073f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 14.98771 > Game.Music.Time) yield return Wait(launchTime + 14.98771f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.98771f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 15.19288 > Game.Music.Time) yield return Wait(launchTime + 15.19288f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.19288f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 15.58616 > Game.Music.Time) yield return Wait(launchTime + 15.58616f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.58616f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 15.79989 > Game.Music.Time) yield return Wait(launchTime + 15.79989f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.79989f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.202836f));
            if (launchTime + 16.00077 > Game.Music.Time) yield return Wait(launchTime + 16.00077f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 16.00077f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 16.4026 > Game.Music.Time) yield return Wait(launchTime + 16.4026f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 16.4026f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 16.60778 > Game.Music.Time) yield return Wait(launchTime + 16.60778f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 16.60778f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 17.00105 > Game.Music.Time) yield return Wait(launchTime + 17.00105f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.00105f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 17.20623 > Game.Music.Time) yield return Wait(launchTime + 17.20623f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.20623f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 17.59523 > Game.Music.Time) yield return Wait(launchTime + 17.59523f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.59523f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 17.80468 > Game.Music.Time) yield return Wait(launchTime + 17.80468f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.80468f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 18.20652 > Game.Music.Time) yield return Wait(launchTime + 18.20652f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 18.20652f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 18.39032 > Game.Music.Time) yield return Wait(launchTime + 18.39032f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 18.39032f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 18.80497 > Game.Music.Time) yield return Wait(launchTime + 18.80497f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 18.80497f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 18.99305 > Game.Music.Time) yield return Wait(launchTime + 18.99305f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 18.99305f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 19.21107 > Game.Music.Time) yield return Wait(launchTime + 19.21107f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 19.21107f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 19.5915 > Game.Music.Time) yield return Wait(launchTime + 19.5915f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 19.5915f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 19.82234 > Game.Music.Time) yield return Wait(launchTime + 19.82234f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 19.82234f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 20.17714 > Game.Music.Time) yield return Wait(launchTime + 20.17714f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.17714f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 20.4037 > Game.Music.Time) yield return Wait(launchTime + 20.4037f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.4037f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 20.80548 > Game.Music.Time) yield return Wait(launchTime + 20.80548f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.80548f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 21.02349 > Game.Music.Time) yield return Wait(launchTime + 21.02349f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.02349f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 21.39111 > Game.Music.Time) yield return Wait(launchTime + 21.39111f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.39111f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 21.57919 > Game.Music.Time) yield return Wait(launchTime + 21.57919f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.57919f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 21.98529 > Game.Music.Time) yield return Wait(launchTime + 21.98529f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.98529f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 22.20752 > Game.Music.Time) yield return Wait(launchTime + 22.20752f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 22.20752f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 22.40422 > Game.Music.Time) yield return Wait(launchTime + 22.40422f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 22.40422f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 22.80169 > Game.Music.Time) yield return Wait(launchTime + 22.80169f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 22.80169f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 22.99835 > Game.Music.Time) yield return Wait(launchTime + 22.99835f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 22.99835f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 23.40017 > Game.Music.Time) yield return Wait(launchTime + 23.40017f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 23.40017f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 23.60472 > Game.Music.Time) yield return Wait(launchTime + 23.60472f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 23.60472f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.3508911f));
            if (launchTime + 23.60534 > Game.Music.Time) yield return Wait(launchTime + 23.60534f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 23.60534f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0x28/255f,0x44/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2028351f));
            if (launchTime + 24.19323 > Game.Music.Time) yield return Wait(launchTime + 24.19323f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 24.19323f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.3285217f));
            if (launchTime + 24.81495 > Game.Music.Time) yield return Wait(launchTime + 24.81495f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 24.81495f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.2908173f));
            if (launchTime + 25.19844 > Game.Music.Time) yield return Wait(launchTime + 25.19844f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 25.19844f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xcc/255f,0x00/255f,0x00/255f)/*out*/, 0.34758f));
            yield break;
        }

    }
}