#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace LevelParserV3
{
    public class Level_MomentOfTruth : LevelScript
    {
        protected override string ClipName {
            get { return "MomentOfTruth.lvl"; }
        }

        protected override void StartLevel()
        {
            Game.Music.Play(ClipName);

            Player.StartCoroutine(Pattern_0(0, null));
        }

        private IEnumerator Pattern_0(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 1.036644 > Game.Music.Time) yield return Wait(launchTime + 1.036644f);
            Player.StartCoroutine(Pattern_1/*Part1_1*/(launchTime + 1.036644f, blackboard/*out*/));
            if (launchTime + 1.647132 > Game.Music.Time) yield return Wait(launchTime + 1.647132f);
            Player.StartCoroutine(Pattern_70/*DanceMan*/(launchTime + 1.647132f, blackboard/*out*/));
            if (launchTime + 15.43447 > Game.Music.Time) yield return Wait(launchTime + 15.43447f);
            Player.StartCoroutine(Pattern_5/*Part1_2*/(launchTime + 15.43447f, blackboard/*out*/));
            if (launchTime + 30.20094 > Game.Music.Time) yield return Wait(launchTime + 30.20094f);
            Player.StartCoroutine(Pattern_12/*Part1_3*/(launchTime + 30.20094f, blackboard/*out*/));
            if (launchTime + 31.20472 > Game.Music.Time) yield return Wait(launchTime + 31.20472f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 31.20472f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.7790775f));
            if (launchTime + 44.97116 > Game.Music.Time) yield return Wait(launchTime + 44.97116f);
            Player.StartCoroutine(Pattern_21/*Part1_4*/(launchTime + 44.97116f, blackboard/*out*/));
            if (launchTime + 45.97654 > Game.Music.Time) yield return Wait(launchTime + 45.97654f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 45.97654f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4114685f));
            if (launchTime + 56.89721 > Game.Music.Time) yield return Wait(launchTime + 56.89721f);
            Player.StartCoroutine(Pattern_25/*Part1_F*/(launchTime + 56.89721f, blackboard, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 56.89721 > Game.Music.Time) yield return Wait(launchTime + 56.89721f);
            Player.StartCoroutine(Pattern_25/*Part1_F*/(launchTime + 56.89721f, blackboard, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => -1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 60.38359 > Game.Music.Time) yield return Wait(launchTime + 60.38359f);
            Player.StartCoroutine(Pattern_26/*Part2*/(launchTime + 60.38359f, blackboard/*out*/));
            if (launchTime + 60.75151 > Game.Music.Time) yield return Wait(launchTime + 60.75151f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 60.75151f, blackboard, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.4217682f));
            if (launchTime + 90.25888 > Game.Music.Time) yield return Wait(launchTime + 90.25888f);
            Player.StartCoroutine(Pattern_34/*Part3*/(launchTime + 90.25888f, blackboard/*out*/));
            if (launchTime + 90.27001 > Game.Music.Time) yield return Wait(launchTime + 90.27001f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 90.27001f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.1929016f));
            if (launchTime + 104.7103 > Game.Music.Time) yield return Wait(launchTime + 104.7103f);
            Player.StartCoroutine(Pattern_40/*Part4*/(launchTime + 104.7103f, blackboard/*out*/));
            if (launchTime + 160.0441 > Game.Music.Time) yield return Wait(launchTime + 160.0441f);
            Player.StartCoroutine(Pattern_55/*Part4_F*/(launchTime + 160.0441f, blackboard/*out*/));
            if (launchTime + 160.4248 > Game.Music.Time) yield return Wait(launchTime + 160.4248f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 160.4248f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x96/255f,0x00/255f,0x00/255f)/*out*/, 3.714432f));
            if (launchTime + 163.293 > Game.Music.Time) yield return Wait(launchTime + 163.293f);
            Player.StartCoroutine(Pattern_58/*Part5*/(launchTime + 163.293f, blackboard/*out*/));
            if (launchTime + 164.1484 > Game.Music.Time) yield return Wait(launchTime + 164.1484f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 164.1484f, blackboard, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.268631f));
            if (launchTime + 193.5793 > Game.Music.Time) yield return Wait(launchTime + 193.5793f);
            Player.StartCoroutine(StarSpeed.Act(launchTime + 193.5793f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f)/*out*/, 0.7084045f));
            if (launchTime + 193.6448 > Game.Music.Time) yield return Wait(launchTime + 193.6448f);
            Player.StartCoroutine(Pattern_34/*Part3*/(launchTime + 193.6448f, blackboard/*out*/));
            if (launchTime + 208.4442 > Game.Music.Time) yield return Wait(launchTime + 208.4442f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 208.4442f, blackboard/*out*/));
            if (launchTime + 208.4467 > Game.Music.Time) yield return Wait(launchTime + 208.4467f);
            Player.StartCoroutine(CreateBulletCartesianInterval.Act(launchTime + 208.4467f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => 0.2307f, (c,i,t,l, castc, casti) => (2f*(float)d)*(1f-(float)t), (c,i,t,l, castc, casti) => new Vector2((float)rndf(-320f,320f)*(float)ar,(float)rndf(70f,240f)), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,-(float)rndf(200f,300f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_21.output_bullet(s)", 14.73215f));
            if (launchTime + 210.2685 > Game.Music.Time) yield return Wait(launchTime + 210.2685f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 210.2685f, blackboard/*out*/));
            if (launchTime + 212.1199 > Game.Music.Time) yield return Wait(launchTime + 212.1199f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 212.1199f, blackboard/*out*/));
            if (launchTime + 213.9661 > Game.Music.Time) yield return Wait(launchTime + 213.9661f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 213.9661f, blackboard/*out*/));
            if (launchTime + 215.8096 > Game.Music.Time) yield return Wait(launchTime + 215.8096f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 215.8096f, blackboard/*out*/));
            if (launchTime + 217.6585 > Game.Music.Time) yield return Wait(launchTime + 217.6585f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 217.6585f, blackboard/*out*/));
            if (launchTime + 219.5073 > Game.Music.Time) yield return Wait(launchTime + 219.5073f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 219.5073f, blackboard/*out*/));
            if (launchTime + 221.3426 > Game.Music.Time) yield return Wait(launchTime + 221.3426f);
            Player.StartCoroutine(Pattern_68/*flashes*/(launchTime + 221.3426f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_1(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object enemies;
            blackboard.SubscribeForChanges("node_7.output_value", (b, o) => enemies = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 60f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.00801903 > Game.Music.Time) yield return Wait(launchTime + 0.00801903f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.00801903f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(200f*(float)ar,170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 60f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.008020043 > Game.Music.Time) yield return Wait(launchTime + 0.008020043f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.008020043f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(-200f*(float)ar,170f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2(0f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 60f, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.1013077 > Game.Music.Time) yield return Wait(launchTime + 0.1013077f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.1013077f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_3.output_enemy(ies)", 0.4822538f));
            if (launchTime + 0.6216841 > Game.Music.Time) yield return Wait(launchTime + 0.6216841f);
            Player.StartCoroutine(Pattern_2/*SideAttack*/(launchTime + 0.6216841f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x35/255f,0xa2/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 0.6239529 > Game.Music.Time) yield return Wait(launchTime + 0.6239529f);
            Player.StartCoroutine(Pattern_3/*8Accords*/(launchTime + 0.6239529f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)"/*out*/));
            if (launchTime + 8.018525 > Game.Music.Time) yield return Wait(launchTime + 8.018525f);
            Player.StartCoroutine(Pattern_3/*8Accords*/(launchTime + 8.018525f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)"/*out*/));
            if (launchTime + 14.52405 > Game.Music.Time) yield return Wait(launchTime + 14.52405f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 14.52405f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_9.output_enemy(ies)", 0.8456268f));
            if (launchTime + 15.39747 > Game.Music.Time) yield return Wait(launchTime + 15.39747f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.39747f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_value", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_2(float launchTime, Blackboard outboard, InputFunc color_input, InputFunc type_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, color_input, "node_0.output_patterninput");
            object color = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => color = o);

            SubscribeInput(outboard, blackboard, type_input, "node_1.output_patterninput");
            object type = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => type = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-320f*(float)ar,(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => 0.2307f*4f/((float)toint((float)d,3f)+1f), (c,i,t,l, castc, casti) => (float)toint((float)d+1f,3f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)", 14.76564f));
            if (launchTime + 0.2325096 > Game.Music.Time) yield return Wait(launchTime + 0.2325096f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.2325096f, blackboard, (c,i,t,l, castc, casti) => new Vector2(320f*(float)ar,(float)rndf(-240f,240f)), (c,i,t,l, castc, casti) => 0.2307f*4f/((float)toint((float)d,3f)+1f), (c,i,t,l, castc, casti) => (float)toint((float)d+1f,3f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)", 14.32571f));
            yield break;
        }

        private IEnumerator Pattern_3(float launchTime, Blackboard outboard, InputFunc center_input, InputFunc left_input, InputFunc right_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, center_input, "node_0.output_patterninput");
            object center = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => center = o);

            SubscribeInput(outboard, blackboard, left_input, "node_1.output_patterninput");
            object left = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => left = o);

            SubscribeInput(outboard, blackboard, right_input, "node_2.output_patterninput");
            object right = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => right = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9249282 > Game.Music.Time) yield return Wait(launchTime + 0.9249282f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 0.9249282f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.838205 > Game.Music.Time) yield return Wait(launchTime + 1.838205f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 1.838205f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.771096 > Game.Music.Time) yield return Wait(launchTime + 2.771096f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 2.771096f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.695178 > Game.Music.Time) yield return Wait(launchTime + 3.695178f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 3.695178f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 4.614746 > Game.Music.Time) yield return Wait(launchTime + 4.614746f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 4.614746f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/));
            if (launchTime + 5.526948 > Game.Music.Time) yield return Wait(launchTime + 5.526948f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 5.526948f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 6.477709 > Game.Music.Time) yield return Wait(launchTime + 6.477709f);
            Player.StartCoroutine(Pattern_4/*Shot*/(launchTime + 6.477709f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_4(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SquareLight, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x03/255f,0x10/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.01162779 > Game.Music.Time) yield return Wait(launchTime + 0.01162779f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.01162779f, blackboard, (c,i,t,l, castc, casti) => new Color(0x15/255f,0x14/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4123634f));
            yield break;
        }

        private IEnumerator Pattern_5(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*BeatMirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_9/*BeatMirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.7933407 > Game.Music.Time) yield return Wait(launchTime + 0.7933407f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 0.7933407f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_enemy"));
            if (launchTime + 1.453194 > Game.Music.Time) yield return Wait(launchTime + 1.453194f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 1.453194f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy"/*out*/));
            if (launchTime + 1.455448 > Game.Music.Time) yield return Wait(launchTime + 1.455448f);
            Player.StartCoroutine(Pattern_65/*BackgroundFlash*/(launchTime + 1.455448f, blackboard/*out*/));
            if (launchTime + 4.497412 > Game.Music.Time) yield return Wait(launchTime + 4.497412f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 4.497412f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/, "node_5.output_enemy"));
            if (launchTime + 5.145025 > Game.Music.Time) yield return Wait(launchTime + 5.145025f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 5.145025f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy"/*out*/));
            if (launchTime + 5.15078 > Game.Music.Time) yield return Wait(launchTime + 5.15078f);
            Player.StartCoroutine(Pattern_65/*BackgroundFlash*/(launchTime + 5.15078f, blackboard/*out*/));
            if (launchTime + 8.180668 > Game.Music.Time) yield return Wait(launchTime + 8.180668f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 8.180668f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_8.output_enemy"));
            if (launchTime + 8.836084 > Game.Music.Time) yield return Wait(launchTime + 8.836084f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 8.836084f, blackboard, (c,i,t,l, castc, casti) => "node_8.output_enemy"/*out*/));
            if (launchTime + 8.837988 > Game.Music.Time) yield return Wait(launchTime + 8.837988f);
            Player.StartCoroutine(Pattern_65/*BackgroundFlash*/(launchTime + 8.837988f, blackboard/*out*/));
            if (launchTime + 11.88334 > Game.Music.Time) yield return Wait(launchTime + 11.88334f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 11.88334f, blackboard, (c,i,t,l, castc, casti) => -1f/*out*/, "node_11.output_enemy"));
            if (launchTime + 12.53169 > Game.Music.Time) yield return Wait(launchTime + 12.53169f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 12.53169f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_enemy"/*out*/));
            if (launchTime + 12.5417 > Game.Music.Time) yield return Wait(launchTime + 12.5417f);
            Player.StartCoroutine(Pattern_65/*BackgroundFlash*/(launchTime + 12.5417f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_6(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_8/*MoveEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_1.output_enemy"));
            if (launchTime + 0.9737416 > Game.Music.Time) yield return Wait(launchTime + 0.9737416f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.9737416f, blackboard, (c,i,t,l, castc, casti) => new Color(0x22/255f,0x10/255f,0x22/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3363934f));
            if (launchTime + 0.9971314 > Game.Music.Time) yield return Wait(launchTime + 0.9971314f);
            Player.StartCoroutine(Pattern_7/*Shoot*/(launchTime + 0.9971314f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_7(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x6c/255f,0x38/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_8(float launchTime, Blackboard outboard, InputFunc flipx_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy1, (c,i,t,l, castc, casti) => 400f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-282f*(float)flipx*(float)ar,-249f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.07124901 > Game.Music.Time) yield return Wait(launchTime + 0.07124901f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.07124901f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-222f*(float)flipx*(float)ar,259f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 2.318361f));
            if (launchTime + 2.410775 > Game.Music.Time) yield return Wait(launchTime + 2.410775f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 2.410775f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_9(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 1.845671 > Game.Music.Time) yield return Wait(launchTime + 1.845671f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 1.845671f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 3.689529 > Game.Music.Time) yield return Wait(launchTime + 3.689529f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 3.689529f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 5.541635 > Game.Music.Time) yield return Wait(launchTime + 5.541635f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 5.541635f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 7.389218 > Game.Music.Time) yield return Wait(launchTime + 7.389218f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 7.389218f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 9.227364 > Game.Music.Time) yield return Wait(launchTime + 9.227364f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 9.227364f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 11.07576 > Game.Music.Time) yield return Wait(launchTime + 11.07576f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 11.07576f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 12.91962 > Game.Music.Time) yield return Wait(launchTime + 12.91962f);
            Player.StartCoroutine(Pattern_6/*BeatAttackVar1*/(launchTime + 12.91962f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_10(float launchTime, Blackboard outboard, InputFunc flipx_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_1.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy4, (c,i,t,l, castc, casti) => 600f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-350f*(float)flipx*(float)ar,198f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.6564655 > Game.Music.Time) yield return Wait(launchTime + 0.6564655f);
            Player.StartCoroutine(Pattern_11/*MoveEnemy*/(launchTime + 0.6564655f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)"/*out*/));
            if (launchTime + 1.575502 > Game.Music.Time) yield return Wait(launchTime + 1.575502f);
            Player.StartCoroutine(Pattern_11/*MoveEnemy*/(launchTime + 1.575502f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)"/*out*/));
            if (launchTime + 2.529352 > Game.Music.Time) yield return Wait(launchTime + 2.529352f);
            Player.StartCoroutine(Pattern_11/*MoveEnemy*/(launchTime + 2.529352f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)"/*out*/));
            if (launchTime + 3.459367 > Game.Music.Time) yield return Wait(launchTime + 3.459367f);
            Player.StartCoroutine(Pattern_11/*MoveEnemy*/(launchTime + 3.459367f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)"/*out*/));
            if (launchTime + 3.916876 > Game.Music.Time) yield return Wait(launchTime + 3.916876f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 3.916876f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_11(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(175f*(float)flipx*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutCubic/*out*/, "node_3.output_enemy(ies)", 0.4450493f));
            yield break;
        }

        private IEnumerator Pattern_12(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*BeatMirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_13/*BeatMirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.7985554 > Game.Music.Time) yield return Wait(launchTime + 0.7985554f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 0.7985554f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_enemy"));
            if (launchTime + 0.993084 > Game.Music.Time) yield return Wait(launchTime + 0.993084f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 0.993084f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy"/*out*/));
            if (launchTime + 0.9937916 > Game.Music.Time) yield return Wait(launchTime + 0.9937916f);
            Player.StartCoroutine(Pattern_2/*SideAttack*/(launchTime + 0.9937916f, blackboard, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x35/255f,0xa2/255f), (c,i,t,l, castc, casti) => ParticleBulletType.Smallest/*out*/));
            if (launchTime + 4.512693 > Game.Music.Time) yield return Wait(launchTime + 4.512693f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 4.512693f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_5.output_enemy"));
            if (launchTime + 4.674019 > Game.Music.Time) yield return Wait(launchTime + 4.674019f);
            Player.StartCoroutine(Pattern_20/*TopMoverShoot2*/(launchTime + 4.674019f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy"/*out*/));
            if (launchTime + 8.196493 > Game.Music.Time) yield return Wait(launchTime + 8.196493f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 8.196493f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_7.output_enemy"));
            if (launchTime + 8.375294 > Game.Music.Time) yield return Wait(launchTime + 8.375294f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 8.375294f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy"/*out*/));
            if (launchTime + 11.89246 > Game.Music.Time) yield return Wait(launchTime + 11.89246f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 11.89246f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_9.output_enemy"));
            if (launchTime + 12.05102 > Game.Music.Time) yield return Wait(launchTime + 12.05102f);
            Player.StartCoroutine(Pattern_20/*TopMoverShoot2*/(launchTime + 12.05102f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_13(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 1.855268 > Game.Music.Time) yield return Wait(launchTime + 1.855268f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 1.855268f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 3.691122 > Game.Music.Time) yield return Wait(launchTime + 3.691122f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 3.691122f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 5.526967 > Game.Music.Time) yield return Wait(launchTime + 5.526967f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 5.526967f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 7.38529 > Game.Music.Time) yield return Wait(launchTime + 7.38529f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 7.38529f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 9.236143 > Game.Music.Time) yield return Wait(launchTime + 9.236143f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 9.236143f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 11.072 > Game.Music.Time) yield return Wait(launchTime + 11.072f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 11.072f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            if (launchTime + 12.92284 > Game.Music.Time) yield return Wait(launchTime + 12.92284f);
            Player.StartCoroutine(Pattern_14/*BeatAttackVar2*/(launchTime + 12.92284f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_14(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_8/*MoveEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/, "node_1.output_enemy"));
            if (launchTime + 0.9922199 > Game.Music.Time) yield return Wait(launchTime + 0.9922199f);
            Player.StartCoroutine(Pattern_15/*Shoot*/(launchTime + 0.9922199f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_15(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x6c/255f,0x38/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.2106667 > Game.Music.Time) yield return Wait(launchTime + 0.2106667f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.2106667f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x38/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.456131 > Game.Music.Time) yield return Wait(launchTime + 0.456131f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.456131f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x6e/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_16(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_17/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9213715 > Game.Music.Time) yield return Wait(launchTime + 0.9213715f);
            Player.StartCoroutine(Pattern_17/*Shoot*/(launchTime + 0.9213715f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.846138 > Game.Music.Time) yield return Wait(launchTime + 1.846138f);
            Player.StartCoroutine(Pattern_17/*Shoot*/(launchTime + 1.846138f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.774903 > Game.Music.Time) yield return Wait(launchTime + 2.774903f);
            Player.StartCoroutine(Pattern_17/*Shoot*/(launchTime + 2.774903f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_17(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 0.04f*((4f-(float)d)/2f+1f), (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Plasma, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 0.44104f));
            yield break;
        }

        private IEnumerator Pattern_18(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.66008 > Game.Music.Time) yield return Wait(launchTime + 0.66008f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.66008f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9110947 > Game.Music.Time) yield return Wait(launchTime + 0.9110947f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.9110947f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.394981 > Game.Music.Time) yield return Wait(launchTime + 1.394981f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.394981f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.842544 > Game.Music.Time) yield return Wait(launchTime + 1.842544f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.842544f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.532078 > Game.Music.Time) yield return Wait(launchTime + 2.532078f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 2.532078f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.758892 > Game.Music.Time) yield return Wait(launchTime + 2.758892f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 2.758892f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.2337 > Game.Music.Time) yield return Wait(launchTime + 3.2337f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 3.2337f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_19(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+50f/(float)c*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x91/255f,0x1d/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.004375457 > Game.Music.Time) yield return Wait(launchTime + 0.004375457f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.004375457f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2a/255f,0x2a/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2541962f));
            yield break;
        }

        private IEnumerator Pattern_20(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7013245 > Game.Music.Time) yield return Wait(launchTime + 0.7013245f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.7013245f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9257507 > Game.Music.Time) yield return Wait(launchTime + 0.9257507f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 0.9257507f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.200272 > Game.Music.Time) yield return Wait(launchTime + 1.200272f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.200272f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.400665 > Game.Music.Time) yield return Wait(launchTime + 1.400665f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.400665f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.609039 > Game.Music.Time) yield return Wait(launchTime + 1.609039f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.609039f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.853501 > Game.Music.Time) yield return Wait(launchTime + 1.853501f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 1.853501f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.542816 > Game.Music.Time) yield return Wait(launchTime + 2.542816f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 2.542816f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.775238 > Game.Music.Time) yield return Wait(launchTime + 2.775238f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 2.775238f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.013703 > Game.Music.Time) yield return Wait(launchTime + 3.013703f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 3.013703f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.230103 > Game.Music.Time) yield return Wait(launchTime + 3.230103f);
            Player.StartCoroutine(Pattern_19/*Shot*/(launchTime + 3.230103f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_21(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*BeatMirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_22/*BeatMirror*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 0.7969894 > Game.Music.Time) yield return Wait(launchTime + 0.7969894f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 0.7969894f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_enemy"));
            if (launchTime + 0.9933147 > Game.Music.Time) yield return Wait(launchTime + 0.9933147f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 0.9933147f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy"/*out*/));
            if (launchTime + 1.018888 > Game.Music.Time) yield return Wait(launchTime + 1.018888f);
            Player.StartCoroutine(Pattern_2/*SideAttack*/(launchTime + 1.018888f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xb4/255f,0xff/255f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp/*out*/));
            if (launchTime + 4.491825 > Game.Music.Time) yield return Wait(launchTime + 4.491825f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 4.491825f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_5.output_enemy"));
            if (launchTime + 4.679678 > Game.Music.Time) yield return Wait(launchTime + 4.679678f);
            Player.StartCoroutine(Pattern_20/*TopMoverShoot2*/(launchTime + 4.679678f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy"/*out*/));
            if (launchTime + 8.169231 > Game.Music.Time) yield return Wait(launchTime + 8.169231f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 8.169231f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_7.output_enemy"));
            if (launchTime + 8.372625 > Game.Music.Time) yield return Wait(launchTime + 8.372625f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 8.372625f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_22(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_23/*BeatAttackVar3*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 1.855888 > Game.Music.Time) yield return Wait(launchTime + 1.855888f);
            Player.StartCoroutine(Pattern_23/*BeatAttackVar3*/(launchTime + 1.855888f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 3.699142 > Game.Music.Time) yield return Wait(launchTime + 3.699142f);
            Player.StartCoroutine(Pattern_23/*BeatAttackVar3*/(launchTime + 3.699142f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 5.559658 > Game.Music.Time) yield return Wait(launchTime + 5.559658f);
            Player.StartCoroutine(Pattern_23/*BeatAttackVar3*/(launchTime + 5.559658f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 7.398616 > Game.Music.Time) yield return Wait(launchTime + 7.398616f);
            Player.StartCoroutine(Pattern_23/*BeatAttackVar3*/(launchTime + 7.398616f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            if (launchTime + 9.233227 > Game.Music.Time) yield return Wait(launchTime + 9.233227f);
            Player.StartCoroutine(Pattern_23/*BeatAttackVar3*/(launchTime + 9.233227f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_23(float launchTime, Blackboard outboard, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_1.output_patterninput");
            object flipx = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_8/*MoveEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput"/*out*/, "node_0.output_enemy"));
            if (launchTime + 0.9781018 > Game.Music.Time) yield return Wait(launchTime + 0.9781018f);
            Player.StartCoroutine(Pattern_24/*Shot*/(launchTime + 0.9781018f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_24(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 5f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x6c/255f,0x38/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)"));
            if (launchTime + 0.6934471 > Game.Music.Time) yield return Wait(launchTime + 0.6934471f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.6934471f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 3f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x6c/255f,0x38/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            if (launchTime + 0.9105949 > Game.Music.Time) yield return Wait(launchTime + 0.9105949f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.9105949f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 3f*(float)d-1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x6c/255f,0x38/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_25(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(347f*(float)flipx*(float)ar,210f*(float)flipy), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.02688601 > Game.Music.Time) yield return Wait(launchTime + 0.02688601f);
            Player.StartCoroutine(MovePathEnemy.Act(launchTime + 0.02688601f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new MyPath(359f*(float)ar,208f,-43f*(float)ar,208f,-252f*(float)ar,158f,-285f*(float)ar,-98f,-287f*(float)ar,-273f), (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)flipx,(float)flipy), (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 4.047615f));
            if (launchTime + 0.1193466 > Game.Music.Time) yield return Wait(launchTime + 0.1193466f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.1193466f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x96/255f,0x00/255f,0x00/255f)/*out*/, 3.710178f));
            if (launchTime + 0.1348991 > Game.Music.Time) yield return Wait(launchTime + 0.1348991f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0.1348991f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.2307f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_7.output_bullet(s)", 0.927002f));
            if (launchTime + 1.062531 > Game.Music.Time) yield return Wait(launchTime + 1.062531f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.062531f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.2307f/2f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_8.output_bullet(s)", 0.9067039f));
            if (launchTime + 1.995388 > Game.Music.Time) yield return Wait(launchTime + 1.995388f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 1.995388f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.2307f/4f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_9.output_bullet(s)", 0.9266815f));
            if (launchTime + 2.926838 > Game.Music.Time) yield return Wait(launchTime + 2.926838f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 2.926838f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => 0.2307f/8f, (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(200f,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_10.output_bullet(s)", 0.9031906f));
            if (launchTime + 4.107361 > Game.Music.Time) yield return Wait(launchTime + 4.107361f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 4.107361f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_26(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object topenemies;
            blackboard.SubscribeForChanges("node_19.output_value", (b, o) => topenemies = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_topenemies", (c,i,t,l, castc, casti) => "node_13.output_topenemies", (c,i,t,l, castc, casti) => "node_26.output_topenemies", (c,i,t,l, castc, casti) => "node_40.output_topenemies", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_19.output_value");
            object bottomenemies;
            blackboard.SubscribeForChanges("node_21.output_value", (b, o) => bottomenemies = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_0.output_bottomenemies", (c,i,t,l, castc, casti) => "node_13.output_bottomenemies", (c,i,t,l, castc, casti) => "node_26.output_bottomenemies", (c,i,t,l, castc, casti) => "node_40.output_bottomenemies", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_21.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_27/*AppearEnemies*/(launchTime + 0f, blackboard/*out*/, "node_0.output_bottomenemies", "node_0.output_topenemies"));
            if (launchTime + 0.368763 > Game.Music.Time) yield return Wait(launchTime + 0.368763f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 0.368763f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 1.309944 > Game.Music.Time) yield return Wait(launchTime + 1.309944f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 1.309944f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 1.321041 > Game.Music.Time) yield return Wait(launchTime + 1.321041f);
            Player.StartCoroutine(Pattern_66/*LazerColors*/(launchTime + 1.321041f, blackboard/*out*/));
            if (launchTime + 2.190365 > Game.Music.Time) yield return Wait(launchTime + 2.190365f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 2.190365f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 3.118527 > Game.Music.Time) yield return Wait(launchTime + 3.118527f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 3.118527f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 4.042256 > Game.Music.Time) yield return Wait(launchTime + 4.042256f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 4.042256f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 4.953076 > Game.Music.Time) yield return Wait(launchTime + 4.953076f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 4.953076f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 5.868191 > Game.Music.Time) yield return Wait(launchTime + 5.868191f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 5.868191f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 6.847222 > Game.Music.Time) yield return Wait(launchTime + 6.847222f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 6.847222f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 7.059906 > Game.Music.Time) yield return Wait(launchTime + 7.059906f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 7.059906f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 7.287789 > Game.Music.Time) yield return Wait(launchTime + 7.287789f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 7.287789f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 7.376805 > Game.Music.Time) yield return Wait(launchTime + 7.376805f);
            Player.StartCoroutine(Pattern_27/*AppearEnemies*/(launchTime + 7.376805f, blackboard/*out*/, "node_13.output_bottomenemies", "node_13.output_topenemies"));
            if (launchTime + 7.756978 > Game.Music.Time) yield return Wait(launchTime + 7.756978f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 7.756978f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 8.681664 > Game.Music.Time) yield return Wait(launchTime + 8.681664f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 8.681664f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 9.591259 > Game.Music.Time) yield return Wait(launchTime + 9.591259f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 9.591259f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 10.49079 > Game.Music.Time) yield return Wait(launchTime + 10.49079f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 10.49079f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 11.41654 > Game.Music.Time) yield return Wait(launchTime + 11.41654f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 11.41654f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 12.35842 > Game.Music.Time) yield return Wait(launchTime + 12.35842f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 12.35842f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 13.26339 > Game.Music.Time) yield return Wait(launchTime + 13.26339f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 13.26339f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 14.21347 > Game.Music.Time) yield return Wait(launchTime + 14.21347f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 14.21347f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 14.45399 > Game.Music.Time) yield return Wait(launchTime + 14.45399f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 14.45399f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 14.68142 > Game.Music.Time) yield return Wait(launchTime + 14.68142f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 14.68142f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 14.74967 > Game.Music.Time) yield return Wait(launchTime + 14.74967f);
            Player.StartCoroutine(Pattern_27/*AppearEnemies*/(launchTime + 14.74967f, blackboard/*out*/, "node_26.output_bottomenemies", "node_26.output_topenemies"));
            if (launchTime + 15.12157 > Game.Music.Time) yield return Wait(launchTime + 15.12157f);
            Player.StartCoroutine(Pattern_2/*SideAttack*/(launchTime + 15.12157f, blackboard, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xb4/255f,0xff/255f), (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp/*out*/));
            if (launchTime + 15.13441 > Game.Music.Time) yield return Wait(launchTime + 15.13441f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 15.13441f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 16.08674 > Game.Music.Time) yield return Wait(launchTime + 16.08674f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 16.08674f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 16.94825 > Game.Music.Time) yield return Wait(launchTime + 16.94825f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 16.94825f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 17.91346 > Game.Music.Time) yield return Wait(launchTime + 17.91346f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 17.91346f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 18.25544 > Game.Music.Time) yield return Wait(launchTime + 18.25544f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 18.25544f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 18.59742 > Game.Music.Time) yield return Wait(launchTime + 18.59742f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 18.59742f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 18.80326 > Game.Music.Time) yield return Wait(launchTime + 18.80326f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 18.80326f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 19.72421 > Game.Music.Time) yield return Wait(launchTime + 19.72421f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 19.72421f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 20.65197 > Game.Music.Time) yield return Wait(launchTime + 20.65197f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 20.65197f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 21.6175 > Game.Music.Time) yield return Wait(launchTime + 21.6175f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 21.6175f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 21.84901 > Game.Music.Time) yield return Wait(launchTime + 21.84901f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 21.84901f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 22.07234 > Game.Music.Time) yield return Wait(launchTime + 22.07234f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 22.07234f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 22.15606 > Game.Music.Time) yield return Wait(launchTime + 22.15606f);
            Player.StartCoroutine(Pattern_27/*AppearEnemies*/(launchTime + 22.15606f, blackboard/*out*/, "node_40.output_bottomenemies", "node_40.output_topenemies"));
            if (launchTime + 22.5036 > Game.Music.Time) yield return Wait(launchTime + 22.5036f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 22.5036f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 23.46131 > Game.Music.Time) yield return Wait(launchTime + 23.46131f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 23.46131f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 24.35392 > Game.Music.Time) yield return Wait(launchTime + 24.35392f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 24.35392f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 25.30587 > Game.Music.Time) yield return Wait(launchTime + 25.30587f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 25.30587f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 25.6587 > Game.Music.Time) yield return Wait(launchTime + 25.6587f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 25.6587f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 26.00704 > Game.Music.Time) yield return Wait(launchTime + 26.00704f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 26.00704f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 26.18784 > Game.Music.Time) yield return Wait(launchTime + 26.18784f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 26.18784f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 27.11673 > Game.Music.Time) yield return Wait(launchTime + 27.11673f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 27.11673f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 28.04101 > Game.Music.Time) yield return Wait(launchTime + 28.04101f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 28.04101f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_value"/*out*/));
            if (launchTime + 29.01087 > Game.Music.Time) yield return Wait(launchTime + 29.01087f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 29.01087f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            if (launchTime + 29.2387 > Game.Music.Time) yield return Wait(launchTime + 29.2387f);
            Player.StartCoroutine(Pattern_30/*LazerShot*/(launchTime + 29.2387f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_value"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_27(float launchTime, Blackboard outboard, string bottomenemies_output, string topenemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, bottomenemies_output, "node_0.output_enemy(ies)");
            SubscribeOutput(outboard, blackboard, topenemies_output, "node_1.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 2f, (c,i,t,l, castc, casti) => EnemyImage.Enemy6, (c,i,t,l, castc, casti) => 700f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2((-100f+200f*(float)i)*(float)ar,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => -5f/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.0007629991 > Game.Music.Time) yield return Wait(launchTime + 0.0007629991f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0007629991f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => EnemyImage.Enemy7, (c,i,t,l, castc, casti) => 700f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2((-200f+200f*(float)i)*(float)ar,300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 15f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.05472952 > Game.Music.Time) yield return Wait(launchTime + 0.05472952f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.05472952f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-140f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_2.output_enemy(ies)", 0.3003235f));
            if (launchTime + 0.05551922 > Game.Music.Time) yield return Wait(launchTime + 0.05551922f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.05551922f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-110f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_3.output_enemy(ies)", 0.300415f));
            if (launchTime + 6.864543 > Game.Music.Time) yield return Wait(launchTime + 6.864543f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.864543f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_7.output_enemy(ies)", 0.8510437f));
            if (launchTime + 6.870464 > Game.Music.Time) yield return Wait(launchTime + 6.870464f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 6.870464f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_8.output_enemy(ies)", 0.842392f));
            if (launchTime + 7.780858 > Game.Music.Time) yield return Wait(launchTime + 7.780858f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 7.780858f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            if (launchTime + 7.804256 > Game.Music.Time) yield return Wait(launchTime + 7.804256f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 7.804256f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_28(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_29/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.1371308 > Game.Music.Time) yield return Wait(launchTime + 0.1371308f);
            Player.StartCoroutine(Pattern_29/*Shot*/(launchTime + 0.1371308f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2872468 > Game.Music.Time) yield return Wait(launchTime + 0.2872468f);
            Player.StartCoroutine(Pattern_29/*Shot*/(launchTime + 0.2872468f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4373322 > Game.Music.Time) yield return Wait(launchTime + 0.4373322f);
            Player.StartCoroutine(Pattern_29/*Shot*/(launchTime + 0.4373322f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.5965424 > Game.Music.Time) yield return Wait(launchTime + 0.5965424f);
            Player.StartCoroutine(Pattern_29/*Shot*/(launchTime + 0.5965424f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.7511749 > Game.Music.Time) yield return Wait(launchTime + 0.7511749f);
            Player.StartCoroutine(Pattern_29/*Shot*/(launchTime + 0.7511749f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_29(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2d/255f,0x1d/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.0579834f));
            if (launchTime + 0.0001678464 > Game.Music.Time) yield return Wait(launchTime + 0.0001678464f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0001678464f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => 70f, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle+180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x75/255f,0x0e/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_30(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => -20f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 20f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_2.output_elements"));
            if (launchTime + 0.004177094 > Game.Music.Time) yield return Wait(launchTime + 0.004177094f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.004177094f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => (float)d*5f*((float)d+1f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Stretched, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.CasterToPlayer, (c,i,t,l, castc, casti) => 50f/(float)d*((float)i%((float)d+1f))-25f+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f+15f*(float)toint((float)i,((float)d+1f)), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_31(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_32/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2121734 > Game.Music.Time) yield return Wait(launchTime + 0.2121734f);
            Player.StartCoroutine(Pattern_32/*Shot*/(launchTime + 0.2121734f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4531555 > Game.Music.Time) yield return Wait(launchTime + 0.4531555f);
            Player.StartCoroutine(Pattern_32/*Shot*/(launchTime + 0.4531555f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6814269 > Game.Music.Time) yield return Wait(launchTime + 0.6814269f);
            Player.StartCoroutine(Pattern_32/*Shot*/(launchTime + 0.6814269f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_32(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_1.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x26/255f,0x26/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1234283f));
            if (launchTime + 0.00567627 > Game.Music.Time) yield return Wait(launchTime + 0.00567627f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.00567627f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d+1f, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => 70f, (c,i,t,l, castc, casti) => ParticleBulletType.Medium, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle+180f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 250f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_33(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_4.output_value");
            object invaders;
            blackboard.SubscribeForChanges("node_4.output_value", (b, o) => invaders = o);
            SubscribeListVariable(blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_value");
            object stepside = blackboard.GetValue((c, i, t, l, castc, casti) => 50f*(float)ar);
            blackboard.SetValue("node_16.output_value", stepside);

            object stepdown = blackboard.GetValue((c, i, t, l, castc, casti) => -15f);
            blackboard.SetValue("node_20.output_value", stepdown);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => EnemyImage.Enemy6, (c,i,t,l, castc, casti) => 1000f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(300f/((float)c-1f)*((float)i)-150f,100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.00110627 > Game.Music.Time) yield return Wait(launchTime + 0.00110627f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.00110627f, blackboard, (c,i,t,l, castc, casti) => 4f, (c,i,t,l, castc, casti) => EnemyImage.Enemy11, (c,i,t,l, castc, casti) => 1000f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(300f/((float)c-1f)*((float)i)-150f,150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.001304629 > Game.Music.Time) yield return Wait(launchTime + 0.001304629f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.001304629f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => EnemyImage.Enemy12, (c,i,t,l, castc, casti) => 1000f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(300f/((float)c-1f)*((float)i)-150f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.4655151 > Game.Music.Time) yield return Wait(launchTime + 0.4655151f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 0.4655151f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 0.7010041 > Game.Music.Time) yield return Wait(launchTime + 0.7010041f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 0.7010041f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 0.9262543 > Game.Music.Time) yield return Wait(launchTime + 0.9262543f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 0.9262543f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 1.393142 > Game.Music.Time) yield return Wait(launchTime + 1.393142f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 1.393142f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 1.862502 > Game.Music.Time) yield return Wait(launchTime + 1.862502f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 1.862502f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 2.313675 > Game.Music.Time) yield return Wait(launchTime + 2.313675f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 2.313675f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 2.559341 > Game.Music.Time) yield return Wait(launchTime + 2.559341f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 2.559341f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 2.783431 > Game.Music.Time) yield return Wait(launchTime + 2.783431f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 2.783431f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 3.006821 > Game.Music.Time) yield return Wait(launchTime + 3.006821f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 3.006821f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 3.239669 > Game.Music.Time) yield return Wait(launchTime + 3.239669f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 3.239669f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 3.706559 > Game.Music.Time) yield return Wait(launchTime + 3.706559f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 3.706559f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 3.96849 > Game.Music.Time) yield return Wait(launchTime + 3.96849f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 3.96849f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 4.189834 > Game.Music.Time) yield return Wait(launchTime + 4.189834f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 4.189834f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 4.416581 > Game.Music.Time) yield return Wait(launchTime + 4.416581f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 4.416581f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 4.623443 > Game.Music.Time) yield return Wait(launchTime + 4.623443f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 4.623443f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 4.837646 > Game.Music.Time) yield return Wait(launchTime + 4.837646f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 4.837646f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 5.088729 > Game.Music.Time) yield return Wait(launchTime + 5.088729f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 5.088729f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 5.550309 > Game.Music.Time) yield return Wait(launchTime + 5.550309f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 5.550309f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 6.006515 > Game.Music.Time) yield return Wait(launchTime + 6.006515f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 6.006515f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 6.252167 > Game.Music.Time) yield return Wait(launchTime + 6.252167f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 6.252167f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 6.462754 > Game.Music.Time) yield return Wait(launchTime + 6.462754f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 6.462754f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 6.700271 > Game.Music.Time) yield return Wait(launchTime + 6.700271f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 6.700271f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 6.93785 > Game.Music.Time) yield return Wait(launchTime + 6.93785f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 6.93785f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 7.392486 > Game.Music.Time) yield return Wait(launchTime + 7.392486f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 7.392486f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 7.849488 > Game.Music.Time) yield return Wait(launchTime + 7.849488f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 7.849488f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 8.090103 > Game.Music.Time) yield return Wait(launchTime + 8.090103f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 8.090103f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 8.322646 > Game.Music.Time) yield return Wait(launchTime + 8.322646f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 8.322646f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 8.779617 > Game.Music.Time) yield return Wait(launchTime + 8.779617f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 8.779617f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 9.242645 > Game.Music.Time) yield return Wait(launchTime + 9.242645f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 9.242645f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 9.695619 > Game.Music.Time) yield return Wait(launchTime + 9.695619f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 9.695619f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 9.948364 > Game.Music.Time) yield return Wait(launchTime + 9.948364f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 9.948364f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 10.15266 > Game.Music.Time) yield return Wait(launchTime + 10.15266f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 10.15266f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 10.39925 > Game.Music.Time) yield return Wait(launchTime + 10.39925f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 10.39925f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 10.6386 > Game.Music.Time) yield return Wait(launchTime + 10.6386f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 10.6386f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 11.08016 > Game.Music.Time) yield return Wait(launchTime + 11.08016f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 11.08016f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 11.30867 > Game.Music.Time) yield return Wait(launchTime + 11.30867f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 11.30867f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 11.54763 > Game.Music.Time) yield return Wait(launchTime + 11.54763f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 11.54763f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 11.77248 > Game.Music.Time) yield return Wait(launchTime + 11.77248f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 11.77248f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 12.01145 > Game.Music.Time) yield return Wait(launchTime + 12.01145f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 12.01145f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 12.23018 > Game.Music.Time) yield return Wait(launchTime + 12.23018f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 12.23018f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 12.46105 > Game.Music.Time) yield return Wait(launchTime + 12.46105f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 12.46105f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 12.93932 > Game.Music.Time) yield return Wait(launchTime + 12.93932f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 12.93932f, blackboard, (c,i,t,l, castc, casti) => new Vector2(-(float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 13.38456 > Game.Music.Time) yield return Wait(launchTime + 13.38456f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 13.38456f, blackboard, (c,i,t,l, castc, casti) => new Vector2(0f,(float)stepdown), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 13.60741 > Game.Music.Time) yield return Wait(launchTime + 13.60741f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 13.60741f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 13.82811 > Game.Music.Time) yield return Wait(launchTime + 13.82811f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 13.82811f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 14.08533 > Game.Music.Time) yield return Wait(launchTime + 14.08533f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 14.08533f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 14.31011 > Game.Music.Time) yield return Wait(launchTime + 14.31011f);
            Player.StartCoroutine(Pattern_64/*Move*/(launchTime + 14.31011f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)stepside,0f), (c,i,t,l, castc, casti) => "node_4.output_value"/*out*/));
            if (launchTime + 14.79294 > Game.Music.Time) yield return Wait(launchTime + 14.79294f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 14.79294f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_value", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_55.output_enemy(ies)", 0.5226746f));
            if (launchTime + 15.44701 > Game.Music.Time) yield return Wait(launchTime + 15.44701f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 15.44701f, blackboard, (c,i,t,l, castc, casti) => "node_4.output_value", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_34(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_33/*Invaders*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemies"));
            if (launchTime + 0.01697541 > Game.Music.Time) yield return Wait(launchTime + 0.01697541f);
            Player.StartCoroutine(Pattern_35/*ShootSmall*/(launchTime + 0.01697541f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            if (launchTime + 0.7104797 > Game.Music.Time) yield return Wait(launchTime + 0.7104797f);
            Player.StartCoroutine(Pattern_38/*BigShoot*/(launchTime + 0.7104797f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemies"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_35(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.832977 > Game.Music.Time) yield return Wait(launchTime + 1.832977f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 1.832977f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.67955 > Game.Music.Time) yield return Wait(launchTime + 3.67955f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 3.67955f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.52613 > Game.Music.Time) yield return Wait(launchTime + 5.52613f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 5.52613f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.372674 > Game.Music.Time) yield return Wait(launchTime + 7.372674f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 7.372674f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.215684 > Game.Music.Time) yield return Wait(launchTime + 9.215684f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 9.215684f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.06935 > Game.Music.Time) yield return Wait(launchTime + 11.06935f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 11.06935f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.9088 > Game.Music.Time) yield return Wait(launchTime + 12.9088f);
            Player.StartCoroutine(Pattern_36/*8shots*/(launchTime + 12.9088f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_36(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2384567 > Game.Music.Time) yield return Wait(launchTime + 0.2384567f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.2384567f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.4523086 > Game.Music.Time) yield return Wait(launchTime + 0.4523086f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.4523086f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.6875762 > Game.Music.Time) yield return Wait(launchTime + 0.6875762f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.6875762f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.9157714 > Game.Music.Time) yield return Wait(launchTime + 0.9157714f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 0.9157714f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.143898 > Game.Music.Time) yield return Wait(launchTime + 1.143898f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 1.143898f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.379158 > Game.Music.Time) yield return Wait(launchTime + 1.379158f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 1.379158f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.62156 > Game.Music.Time) yield return Wait(launchTime + 1.62156f);
            Player.StartCoroutine(Pattern_37/*Shot*/(launchTime + 1.62156f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_37(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d/*out*/, "node_1.output_elements"));
            if (launchTime + 0.0025177 > Game.Music.Time) yield return Wait(launchTime + 0.0025177f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.0025177f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Caster, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_38(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 0.2199326 > Game.Music.Time) yield return Wait(launchTime + 0.2199326f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 0.2199326f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.151566 > Game.Music.Time) yield return Wait(launchTime + 1.151566f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 1.151566f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.075104 > Game.Music.Time) yield return Wait(launchTime + 2.075104f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 2.075104f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.9852 > Game.Music.Time) yield return Wait(launchTime + 2.9852f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 2.9852f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.684571 > Game.Music.Time) yield return Wait(launchTime + 3.684571f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 3.684571f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.904236 > Game.Music.Time) yield return Wait(launchTime + 3.904236f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 3.904236f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.841248 > Game.Music.Time) yield return Wait(launchTime + 4.841248f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 4.841248f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 5.755814 > Game.Music.Time) yield return Wait(launchTime + 5.755814f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 5.755814f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 6.683839 > Game.Music.Time) yield return Wait(launchTime + 6.683839f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 6.683839f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.369765 > Game.Music.Time) yield return Wait(launchTime + 7.369765f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 7.369765f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.607391 > Game.Music.Time) yield return Wait(launchTime + 7.607391f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 7.607391f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 8.526459 > Game.Music.Time) yield return Wait(launchTime + 8.526459f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 8.526459f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.463457 > Game.Music.Time) yield return Wait(launchTime + 9.463457f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 9.463457f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.36905 > Game.Music.Time) yield return Wait(launchTime + 10.36905f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 10.36905f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.06395 > Game.Music.Time) yield return Wait(launchTime + 11.06395f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 11.06395f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.28365 > Game.Music.Time) yield return Wait(launchTime + 11.28365f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 11.28365f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 12.21616 > Game.Music.Time) yield return Wait(launchTime + 12.21616f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 12.21616f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.13969 > Game.Music.Time) yield return Wait(launchTime + 13.13969f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 13.13969f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.59699 > Game.Music.Time) yield return Wait(launchTime + 13.59699f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 13.59699f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.85252 > Game.Music.Time) yield return Wait(launchTime + 13.85252f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 13.85252f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.97353 > Game.Music.Time) yield return Wait(launchTime + 13.97353f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 13.97353f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 14.08121 > Game.Music.Time) yield return Wait(launchTime + 14.08121f);
            Player.StartCoroutine(Pattern_39/*Shoot*/(launchTime + 14.08121f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_39(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);

            object angle = Random.Range((float)blackboard.GetValue((c, i, t, l, castc, casti) => 0f), (float)blackboard.GetValue((c, i, t, l, castc, casti) => 360f));
            blackboard.SetValue("node_2.output_value", angle);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => 1f/*out*/, "node_1.output_elements"));
            if (launchTime + 0.00957489 > Game.Music.Time) yield return Wait(launchTime + 0.00957489f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.00957489f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_elements", (c,i,t,l, castc, casti) => 3f*(float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallRectangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)angle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 300f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_40(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_41/*BeatManager1*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.3328705 > Game.Music.Time) yield return Wait(launchTime + 0.3328705f);
            Player.StartCoroutine(Pattern_54/*Rain*/(launchTime + 0.3328705f, blackboard/*out*/));
            if (launchTime + 14.76498 > Game.Music.Time) yield return Wait(launchTime + 14.76498f);
            Player.StartCoroutine(Pattern_46/*BeatManager2*/(launchTime + 14.76498f, blackboard/*out*/));
            if (launchTime + 14.9064 > Game.Music.Time) yield return Wait(launchTime + 14.9064f);
            Player.StartCoroutine(Pattern_53/*Top*/(launchTime + 14.9064f, blackboard/*out*/));
            if (launchTime + 29.55475 > Game.Music.Time) yield return Wait(launchTime + 29.55475f);
            Player.StartCoroutine(Pattern_49/*BeatManager3*/(launchTime + 29.55475f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_41(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 1.843384 > Game.Music.Time) yield return Wait(launchTime + 1.843384f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 1.843384f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 1.843384 > Game.Music.Time) yield return Wait(launchTime + 1.843384f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 1.843384f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.700539 > Game.Music.Time) yield return Wait(launchTime + 3.700539f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 3.700539f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.700539 > Game.Music.Time) yield return Wait(launchTime + 3.700539f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 3.700539f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 5.545105 > Game.Music.Time) yield return Wait(launchTime + 5.545105f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 5.545105f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 5.545105 > Game.Music.Time) yield return Wait(launchTime + 5.545105f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 5.545105f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.382836 > Game.Music.Time) yield return Wait(launchTime + 7.382836f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 7.382836f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.382836 > Game.Music.Time) yield return Wait(launchTime + 7.382836f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 7.382836f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 9.234077 > Game.Music.Time) yield return Wait(launchTime + 9.234077f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 9.234077f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 9.234077 > Game.Music.Time) yield return Wait(launchTime + 9.234077f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 9.234077f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.07188 > Game.Music.Time) yield return Wait(launchTime + 11.07188f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 11.07188f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.07188 > Game.Music.Time) yield return Wait(launchTime + 11.07188f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 11.07188f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 12.92316 > Game.Music.Time) yield return Wait(launchTime + 12.92316f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 12.92316f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 12.92316 > Game.Music.Time) yield return Wait(launchTime + 12.92316f);
            Player.StartCoroutine(Pattern_43/*Beat*/(launchTime + 12.92316f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_42(float launchTime, Blackboard outboard, InputFunc flipx_input, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeOutput(outboard, blackboard, enemy_output, "node_1.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy8, (c,i,t,l, castc, casti) => 1500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(-360f*(float)flipx*(float)ar,190f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 6f, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.3362274 > Game.Music.Time) yield return Wait(launchTime + 0.3362274f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.3362274f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-180f*(float)flipx*(float)ar,190f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_3.output_enemy(ies)", 0.1798859f));
            if (launchTime + 0.8381805 > Game.Music.Time) yield return Wait(launchTime + 0.8381805f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.8381805f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-180f*(float)flipx*(float)ar,100f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInOutBack/*out*/, "node_4.output_enemy(ies)", 0.4035339f));
            yield break;
        }

        private IEnumerator Pattern_43(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_3.output_patterninput");
            object flipx = blackboard.GetValue("node_3.output_patterninput");
            blackboard.SubscribeForChanges("node_3.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_42/*AppearEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput"/*out*/, "node_2.output_enemy"));
            if (launchTime + 0.3197326 > Game.Music.Time) yield return Wait(launchTime + 0.3197326f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3197326f, blackboard, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x28/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1494446f));
            if (launchTime + 0.5499115 > Game.Music.Time) yield return Wait(launchTime + 0.5499115f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5499115f, blackboard, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x28/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1494446f));
            if (launchTime + 0.7702026 > Game.Music.Time) yield return Wait(launchTime + 0.7702026f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.7702026f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x28/255f,0x28/255f)/*out*/, 0.2683258f));
            if (launchTime + 1.044563 > Game.Music.Time) yield return Wait(launchTime + 1.044563f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.044563f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.01960754f));
            if (launchTime + 1.25 > Game.Music.Time) yield return Wait(launchTime + 1.25f);
            Player.StartCoroutine(Pattern_44/*Shoot*/(launchTime + 1.25f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy"/*out*/));
            if (launchTime + 1.263412 > Game.Music.Time) yield return Wait(launchTime + 1.263412f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.263412f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2c/255f,0x1f/255f,0x13/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3204269f));
            if (launchTime + 2.166427 > Game.Music.Time) yield return Wait(launchTime + 2.166427f);
            Player.StartCoroutine(Pattern_45/*Disappear*/(launchTime + 2.166427f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_44(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d*3f+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Corner, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x8a/255f,0x3e/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_45(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => new Vector2(-180f*(float)flipx*(float)ar,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_3.output_enemy(ies)", 0.3306656f));
            if (launchTime + 0.3690872 > Game.Music.Time) yield return Wait(launchTime + 0.3690872f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 0.3690872f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_46(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.686829 > Game.Music.Time) yield return Wait(launchTime + 3.686829f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 3.686829f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.686829 > Game.Music.Time) yield return Wait(launchTime + 3.686829f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 3.686829f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.39457 > Game.Music.Time) yield return Wait(launchTime + 7.39457f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 7.39457f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.39457 > Game.Music.Time) yield return Wait(launchTime + 7.39457f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 7.39457f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.07819 > Game.Music.Time) yield return Wait(launchTime + 11.07819f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 11.07819f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.07819 > Game.Music.Time) yield return Wait(launchTime + 11.07819f);
            Player.StartCoroutine(Pattern_47/*Beat*/(launchTime + 11.07819f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_47(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_42/*AppearEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/, "node_3.output_enemy"));
            if (launchTime + 0.3348923 > Game.Music.Time) yield return Wait(launchTime + 0.3348923f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3348923f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1b/255f,0x1b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1481857f));
            if (launchTime + 0.5751724 > Game.Music.Time) yield return Wait(launchTime + 0.5751724f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5751724f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1b/255f,0x1b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.12146f));
            if (launchTime + 0.7574997 > Game.Music.Time) yield return Wait(launchTime + 0.7574997f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.7574997f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1b/255f,0x1b/255f)/*out*/, 0.2847061f));
            if (launchTime + 1.044876 > Game.Music.Time) yield return Wait(launchTime + 1.044876f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.044876f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.03198242f));
            if (launchTime + 1.259667 > Game.Music.Time) yield return Wait(launchTime + 1.259667f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.259667f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2b/255f,0x1f/255f,0x15/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3086852f));
            if (launchTime + 1.261909 > Game.Music.Time) yield return Wait(launchTime + 1.261909f);
            Player.StartCoroutine(Pattern_44/*Shoot1*/(launchTime + 1.261909f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 1.735832 > Game.Music.Time) yield return Wait(launchTime + 1.735832f);
            Player.StartCoroutine(Pattern_48/*Shoot*/(launchTime + 1.735832f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 2.174438 > Game.Music.Time) yield return Wait(launchTime + 2.174438f);
            Player.StartCoroutine(Pattern_48/*Shoot*/(launchTime + 2.174438f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 2.518669 > Game.Music.Time) yield return Wait(launchTime + 2.518669f);
            Player.StartCoroutine(Pattern_48/*Shoot*/(launchTime + 2.518669f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 2.874611 > Game.Music.Time) yield return Wait(launchTime + 2.874611f);
            Player.StartCoroutine(Pattern_48/*Shoot*/(launchTime + 2.874611f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 3.104691 > Game.Music.Time) yield return Wait(launchTime + 3.104691f);
            Player.StartCoroutine(Pattern_44/*Shoot1*/(launchTime + 3.104691f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 3.108177 > Game.Music.Time) yield return Wait(launchTime + 3.108177f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 3.108177f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2b/255f,0x1f/255f,0x15/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.3621063f));
            if (launchTime + 4.016998 > Game.Music.Time) yield return Wait(launchTime + 4.016998f);
            Player.StartCoroutine(Pattern_45/*Disappear*/(launchTime + 4.016998f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_48(float launchTime, Blackboard outboard, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemy_input, "node_0.output_patterninput");
            object enemy = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)d, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Torus, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(0f,360f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => (float)rndf(100f,200f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x40/255f,0xa2/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_bullet(s)"));
            if (launchTime + 0.000526428 > Game.Music.Time) yield return Wait(launchTime + 0.000526428f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.000526428f, blackboard, (c,i,t,l, castc, casti) => new Color(0x13/255f,0x14/255f,0x28/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.256691f));
            yield break;
        }

        private IEnumerator Pattern_49(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 3.665542 > Game.Music.Time) yield return Wait(launchTime + 3.665542f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 3.665542f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 3.665542 > Game.Music.Time) yield return Wait(launchTime + 3.665542f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 3.665542f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 7.374619 > Game.Music.Time) yield return Wait(launchTime + 7.374619f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 7.374619f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 7.374619 > Game.Music.Time) yield return Wait(launchTime + 7.374619f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 7.374619f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 11.07294 > Game.Music.Time) yield return Wait(launchTime + 11.07294f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 11.07294f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 11.07294 > Game.Music.Time) yield return Wait(launchTime + 11.07294f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 11.07294f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 14.77126 > Game.Music.Time) yield return Wait(launchTime + 14.77126f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 14.77126f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 14.77126 > Game.Music.Time) yield return Wait(launchTime + 14.77126f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 14.77126f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 18.44807 > Game.Music.Time) yield return Wait(launchTime + 18.44807f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 18.44807f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 18.44807 > Game.Music.Time) yield return Wait(launchTime + 18.44807f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 18.44807f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            if (launchTime + 22.13559 > Game.Music.Time) yield return Wait(launchTime + 22.13559f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 22.13559f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 22.13559 > Game.Music.Time) yield return Wait(launchTime + 22.13559f);
            Player.StartCoroutine(Pattern_50/*Beat*/(launchTime + 22.13559f, blackboard, (c,i,t,l, castc, casti) => 3f, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => -1f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_50(float launchTime, Blackboard outboard, InputFunc flipvariants_input, InputFunc flipy_input, InputFunc flipx_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipvariants_input, "node_0.output_patterninput");
            object flipvariants = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipvariants = o);

            SubscribeInput(outboard, blackboard, flipy_input, "node_1.output_patterninput");
            object flipy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => flipy = o);

            SubscribeInput(outboard, blackboard, flipx_input, "node_2.output_patterninput");
            object flipx = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => flipx = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_42/*AppearEnemy*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput"/*out*/, "node_3.output_enemy"));
            if (launchTime + 0.3072662 > Game.Music.Time) yield return Wait(launchTime + 0.3072662f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.3072662f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1d/255f,0x1d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1412811f));
            if (launchTime + 0.5600433 > Game.Music.Time) yield return Wait(launchTime + 0.5600433f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.5600433f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1d/255f,0x1d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1413879f));
            if (launchTime + 0.7404633 > Game.Music.Time) yield return Wait(launchTime + 0.7404633f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.7404633f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1d/255f,0x1d/255f)/*out*/, 0.2845764f));
            if (launchTime + 1.039093 > Game.Music.Time) yield return Wait(launchTime + 1.039093f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.039093f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, 0.04187012f));
            if (launchTime + 1.256866 > Game.Music.Time) yield return Wait(launchTime + 1.256866f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.256866f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1d/255f,0x1d/255f,0x1d/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.340454f));
            if (launchTime + 1.70169 > Game.Music.Time) yield return Wait(launchTime + 1.70169f);
            Player.StartCoroutine(Pattern_51/*Shoot*/(launchTime + 1.70169f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 4.017395 > Game.Music.Time) yield return Wait(launchTime + 4.017395f);
            Player.StartCoroutine(Pattern_45/*Disappear*/(launchTime + 4.017395f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_51(float launchTime, Blackboard outboard, InputFunc flipx_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, flipx_input, "node_0.output_patterninput");
            object flipx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => flipx = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f/*out*/));
            if (launchTime + 0.4690856 > Game.Music.Time) yield return Wait(launchTime + 0.4690856f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 0.4690856f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f/*out*/));
            if (launchTime + 0.9271699 > Game.Music.Time) yield return Wait(launchTime + 0.9271699f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 0.9271699f, blackboard, (c,i,t,l, castc, casti) => -50f*(float)flipx, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f/*out*/));
            if (launchTime + 1.021393 > Game.Music.Time) yield return Wait(launchTime + 1.021393f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 1.021393f, blackboard, (c,i,t,l, castc, casti) => -16.6f*(float)flipx, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f/*out*/));
            if (launchTime + 1.140686 > Game.Music.Time) yield return Wait(launchTime + 1.140686f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 1.140686f, blackboard, (c,i,t,l, castc, casti) => 16.6f*(float)flipx, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f/*out*/));
            if (launchTime + 1.256027 > Game.Music.Time) yield return Wait(launchTime + 1.256027f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 1.256027f, blackboard, (c,i,t,l, castc, casti) => 50f*(float)flipx, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 2f*(float)d-1f/*out*/));
            if (launchTime + 1.372726 > Game.Music.Time) yield return Wait(launchTime + 1.372726f);
            Player.StartCoroutine(Pattern_52/*SingleShoot*/(launchTime + 1.372726f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => 4f*(float)d-2f/*out*/));
            yield break;
        }

        private IEnumerator Pattern_52(float launchTime, Blackboard outboard, InputFunc velocityx_input, InputFunc enemy_input, InputFunc count_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, velocityx_input, "node_0.output_patterninput");
            object velocityx = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => velocityx = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);

            SubscribeInput(outboard, blackboard, count_input, "node_2.output_patterninput");
            object count = blackboard.GetValue("node_2.output_patterninput");
            blackboard.SubscribeForChanges("node_2.output_patterninput", (b, o) => count = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x28/255f,0x2a/255f,0x15/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.138443f));
            if (launchTime + 0.004104614 > Game.Music.Time) yield return Wait(launchTime + 0.004104614f);
            Player.StartCoroutine(CreateBulletCartesian.Act(launchTime + 0.004104614f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_2.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Vector2((float)velocityx+(float)rndf(-20f,20f),(float)rndf(200f,300f)), (c,i,t,l, castc, casti) => new Vector2(0f,-300f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_5.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_53(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_0.output_enemy"));
            if (launchTime + 0.661871 > Game.Music.Time) yield return Wait(launchTime + 0.661871f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 0.661871f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            if (launchTime + 3.691887 > Game.Music.Time) yield return Wait(launchTime + 3.691887f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 3.691887f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_3.output_enemy"));
            if (launchTime + 4.337805 > Game.Music.Time) yield return Wait(launchTime + 4.337805f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 4.337805f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy"/*out*/));
            if (launchTime + 7.385193 > Game.Music.Time) yield return Wait(launchTime + 7.385193f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 7.385193f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_5.output_enemy"));
            if (launchTime + 8.040725 > Game.Music.Time) yield return Wait(launchTime + 8.040725f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 8.040725f, blackboard, (c,i,t,l, castc, casti) => "node_5.output_enemy"/*out*/));
            if (launchTime + 11.0794 > Game.Music.Time) yield return Wait(launchTime + 11.0794f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 11.0794f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_7.output_enemy"));
            if (launchTime + 11.73058 > Game.Music.Time) yield return Wait(launchTime + 11.73058f);
            Player.StartCoroutine(Pattern_16/*TopMoverShoot*/(launchTime + 11.73058f, blackboard, (c,i,t,l, castc, casti) => "node_7.output_enemy"/*out*/));
            if (launchTime + 14.76404 > Game.Music.Time) yield return Wait(launchTime + 14.76404f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 14.76404f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_9.output_enemy"));
            if (launchTime + 14.967 > Game.Music.Time) yield return Wait(launchTime + 14.967f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 14.967f, blackboard, (c,i,t,l, castc, casti) => "node_9.output_enemy"/*out*/));
            if (launchTime + 18.46531 > Game.Music.Time) yield return Wait(launchTime + 18.46531f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 18.46531f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_11.output_enemy"));
            if (launchTime + 18.6372 > Game.Music.Time) yield return Wait(launchTime + 18.6372f);
            Player.StartCoroutine(Pattern_20/*TopMoverShoot2*/(launchTime + 18.6372f, blackboard, (c,i,t,l, castc, casti) => "node_11.output_enemy"/*out*/));
            if (launchTime + 22.15864 > Game.Music.Time) yield return Wait(launchTime + 22.15864f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 22.15864f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_13.output_enemy"));
            if (launchTime + 22.34963 > Game.Music.Time) yield return Wait(launchTime + 22.34963f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 22.34963f, blackboard, (c,i,t,l, castc, casti) => "node_13.output_enemy"/*out*/));
            if (launchTime + 25.84405 > Game.Music.Time) yield return Wait(launchTime + 25.84405f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 25.84405f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_15.output_enemy"));
            if (launchTime + 26.02075 > Game.Music.Time) yield return Wait(launchTime + 26.02075f);
            Player.StartCoroutine(Pattern_20/*TopMoverShoot2*/(launchTime + 26.02075f, blackboard, (c,i,t,l, castc, casti) => "node_15.output_enemy"/*out*/));
            if (launchTime + 29.52958 > Game.Music.Time) yield return Wait(launchTime + 29.52958f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 29.52958f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_17.output_enemy"));
            if (launchTime + 29.73698 > Game.Music.Time) yield return Wait(launchTime + 29.73698f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 29.73698f, blackboard, (c,i,t,l, castc, casti) => "node_17.output_enemy"/*out*/));
            if (launchTime + 33.24653 > Game.Music.Time) yield return Wait(launchTime + 33.24653f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 33.24653f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_19.output_enemy"));
            if (launchTime + 33.41432 > Game.Music.Time) yield return Wait(launchTime + 33.41432f);
            Player.StartCoroutine(Pattern_20/*TopMoverShoot2*/(launchTime + 33.41432f, blackboard, (c,i,t,l, castc, casti) => "node_19.output_enemy"/*out*/));
            if (launchTime + 36.91624 > Game.Music.Time) yield return Wait(launchTime + 36.91624f);
            Player.StartCoroutine(Pattern_10/*TopMover*/(launchTime + 36.91624f, blackboard, (c,i,t,l, castc, casti) => 1f/*out*/, "node_21.output_enemy"));
            if (launchTime + 37.11823 > Game.Music.Time) yield return Wait(launchTime + 37.11823f);
            Player.StartCoroutine(Pattern_18/*TopMoverShoot1*/(launchTime + 37.11823f, blackboard, (c,i,t,l, castc, casti) => "node_21.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_54(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-320f,320f)*(float)ar,230f), (c,i,t,l, castc, casti) => 0.2307f*((4f-(float)d)/2f+1f), (c,i,t,l, castc, casti) => (float)toint((float)d+1f,3f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x00/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_bullet(s)", 44.30869f));
            if (launchTime + 44.33789 > Game.Music.Time) yield return Wait(launchTime + 44.33789f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 44.33789f, blackboard, (c,i,t,l, castc, casti) => new Vector2((float)rndf(-320f,320f),230f), (c,i,t,l, castc, casti) => 0.2307f, (c,i,t,l, castc, casti) => (float)toint((float)d+1f,3f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.Smallest, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0xff/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 11.03267f));
            yield break;
        }

        private IEnumerator Pattern_55(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_57/*ShowEnemy*/(launchTime + 0f, blackboard/*out*/, "node_0.output_enemy"));
            if (launchTime + 0.374588 > Game.Music.Time) yield return Wait(launchTime + 0.374588f);
            Player.StartCoroutine(Pattern_56/*Shoot*/(launchTime + 0.374588f, blackboard, (c,i,t,l, castc, casti) => 0.2307f, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            if (launchTime + 1.299225 > Game.Music.Time) yield return Wait(launchTime + 1.299225f);
            Player.StartCoroutine(Pattern_56/*Shoot*/(launchTime + 1.299225f, blackboard, (c,i,t,l, castc, casti) => 0.2307f/2f, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            if (launchTime + 2.229522 > Game.Music.Time) yield return Wait(launchTime + 2.229522f);
            Player.StartCoroutine(Pattern_56/*Shoot*/(launchTime + 2.229522f, blackboard, (c,i,t,l, castc, casti) => 0.2307f/4f, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            if (launchTime + 3.205261 > Game.Music.Time) yield return Wait(launchTime + 3.205261f);
            Player.StartCoroutine(Pattern_56/*Shoot*/(launchTime + 3.205261f, blackboard, (c,i,t,l, castc, casti) => 0.2307f/8f, (c,i,t,l, castc, casti) => "node_0.output_enemy"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_56(float launchTime, Blackboard outboard, InputFunc interval_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, interval_input, "node_0.output_patterninput");
            object interval = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => interval = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)toint((float)d,2f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallRectangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i-(float)t*360f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0xff/255f,0x4d/255f,0xd2/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_bullet(s)", 0.9047699f));
            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateBulletPolarInterval.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => (float)toint((float)d,2f)+1f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallRectangle, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 360f/(float)c*(float)i+(float)t*360f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 200f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x88/255f,0x4d/255f,0xd2/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_4.output_bullet(s)", 0.9074097f));
            yield break;
        }

        private IEnumerator Pattern_57(float launchTime, Blackboard outboard, string enemy_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemy_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 1f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.Invulnerable, (c,i,t,l, castc, casti) => new Vector2(0f,287f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => -5f/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.01051331 > Game.Music.Time) yield return Wait(launchTime + 0.01051331f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.01051331f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,166f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_1.output_enemy(ies)", 0.3591003f));
            if (launchTime + 4.074051 > Game.Music.Time) yield return Wait(launchTime + 4.074051f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 4.074051f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,271f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_4.output_enemy(ies)", 0.3175049f));
            if (launchTime + 4.419067 > Game.Music.Time) yield return Wait(launchTime + 4.419067f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 4.419067f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null/*out*/));
            yield break;
        }

        private IEnumerator Pattern_58(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();

            object corner;
            blackboard.SubscribeForChanges("node_6.output_value", (b, o) => corner = o);
            SubscribeVariable(blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemies", (c,i,t,l, castc, casti) => "node_7.output_enemies", (c,i,t,l, castc, casti) => "node_8.output_enemies", (c,i,t,l, castc, casti) => "node_9.output_enemies", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_6.output_value");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_59/*AppearCage*/(launchTime + 0f, blackboard/*out*/, "node_0.output_right", "node_0.output_left", "node_0.output_up", "node_0.output_down"));
            if (launchTime + 0.1534118 > Game.Music.Time) yield return Wait(launchTime + 0.1534118f);
            Player.StartCoroutine(Pattern_62/*AppearCorners*/(launchTime + 0.1534118f, blackboard/*out*/, "node_1.output_enemies"));
            if (launchTime + 0.8237304 > Game.Music.Time) yield return Wait(launchTime + 0.8237304f);
            Player.StartCoroutine(Pattern_60/*Lazers*/(launchTime + 0.8237304f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_down", (c,i,t,l, castc, casti) => "node_0.output_up", (c,i,t,l, castc, casti) => "node_0.output_right", (c,i,t,l, castc, casti) => "node_0.output_left"/*out*/));
            if (launchTime + 0.8491668 > Game.Music.Time) yield return Wait(launchTime + 0.8491668f);
            Player.StartCoroutine(Pattern_67/*LazerFlash*/(launchTime + 0.8491668f, blackboard/*out*/));
            if (launchTime + 1.734589 > Game.Music.Time) yield return Wait(launchTime + 1.734589f);
            Player.StartCoroutine(Pattern_61/*Corners*/(launchTime + 1.734589f, blackboard, (c,i,t,l, castc, casti) => "node_6.output_value"/*out*/));
            if (launchTime + 7.530869 > Game.Music.Time) yield return Wait(launchTime + 7.530869f);
            Player.StartCoroutine(Pattern_62/*AppearCorners*/(launchTime + 7.530869f, blackboard/*out*/, "node_7.output_enemies"));
            if (launchTime + 14.90004 > Game.Music.Time) yield return Wait(launchTime + 14.90004f);
            Player.StartCoroutine(Pattern_62/*AppearCorners*/(launchTime + 14.90004f, blackboard/*out*/, "node_8.output_enemies"));
            if (launchTime + 22.31682 > Game.Music.Time) yield return Wait(launchTime + 22.31682f);
            Player.StartCoroutine(Pattern_62/*AppearCorners*/(launchTime + 22.31682f, blackboard/*out*/, "node_9.output_enemies"));
            yield break;
        }

        private IEnumerator Pattern_59(float launchTime, Blackboard outboard, string right_output, string left_output, string up_output, string down_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, right_output, "node_0.output_enemy(ies)");
            SubscribeOutput(outboard, blackboard, left_output, "node_1.output_enemy(ies)");
            SubscribeOutput(outboard, blackboard, up_output, "node_3.output_enemy(ies)");
            SubscribeOutput(outboard, blackboard, down_output, "node_2.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(370f*(float)ar,350f/((float)c-1f)*(float)i-175f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.0003814697 > Game.Music.Time) yield return Wait(launchTime + 0.0003814697f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0003814697f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2(-370f*(float)ar,350f/((float)c-1f)*(float)i-175f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_1.output_enemy(ies)"));
            if (launchTime + 0.0004577637 > Game.Music.Time) yield return Wait(launchTime + 0.0004577637f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.0004577637f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2((460f/((float)c-1f)*(float)i-230f)*(float)ar,-320f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_2.output_enemy(ies)"));
            if (launchTime + 0.001022279 > Game.Music.Time) yield return Wait(launchTime + 0.001022279f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0.001022279f, blackboard, (c,i,t,l, castc, casti) => 5f, (c,i,t,l, castc, casti) => EnemyImage.Enemy14, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => HealthType.OnBackground, (c,i,t,l, castc, casti) => new Vector2((460f/((float)c-1f)*(float)i-230f)*(float)ar,320f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_enemy(ies)"));
            if (launchTime + 0.08050537 > Game.Music.Time) yield return Wait(launchTime + 0.08050537f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.08050537f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_4.output_enemy(ies)", 0.7398071f));
            if (launchTime + 0.0826416 > Game.Music.Time) yield return Wait(launchTime + 0.0826416f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.0826416f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(100f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_5.output_enemy(ies)", 0.7332916f));
            if (launchTime + 0.0847168 > Game.Music.Time) yield return Wait(launchTime + 0.0847168f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.0847168f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-80f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_6.output_enemy(ies)", 0.7419891f));
            if (launchTime + 0.08482361 > Game.Music.Time) yield return Wait(launchTime + 0.08482361f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.08482361f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(-100f,0f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_7.output_enemy(ies)", 0.7288055f));
            if (launchTime + 30.36563 > Game.Music.Time) yield return Wait(launchTime + 30.36563f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 30.36563f, blackboard, (c,i,t,l, castc, casti) => "node_3.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_13.output_enemy(ies)", 0.6878967f));
            if (launchTime + 30.36874 > Game.Music.Time) yield return Wait(launchTime + 30.36874f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 30.36874f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_14.output_enemy(ies)", 0.6769867f));
            if (launchTime + 30.36984 > Game.Music.Time) yield return Wait(launchTime + 30.36984f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 30.36984f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_15.output_enemy(ies)", 0.6813965f));
            if (launchTime + 30.37095 > Game.Music.Time) yield return Wait(launchTime + 30.37095f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 30.37095f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_16.output_enemy(ies)", 0.6813507f));
            yield break;
        }

        private IEnumerator Pattern_60(float launchTime, Blackboard outboard, InputFunc down_input, InputFunc up_input, InputFunc right_input, InputFunc left_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, down_input, "node_25.output_patterninput");
            object down = blackboard.GetValue("node_25.output_patterninput");
            blackboard.SubscribeForChanges("node_25.output_patterninput", (b, o) => down = o);

            SubscribeInput(outboard, blackboard, up_input, "node_27.output_patterninput");
            object up = blackboard.GetValue("node_27.output_patterninput");
            blackboard.SubscribeForChanges("node_27.output_patterninput", (b, o) => up = o);

            SubscribeInput(outboard, blackboard, right_input, "node_31.output_patterninput");
            object right = blackboard.GetValue("node_31.output_patterninput");
            blackboard.SubscribeForChanges("node_31.output_patterninput", (b, o) => right = o);

            SubscribeInput(outboard, blackboard, left_input, "node_35.output_patterninput");
            object left = blackboard.GetValue("node_35.output_patterninput");
            blackboard.SubscribeForChanges("node_35.output_patterninput", (b, o) => left = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 0.2597809 > Game.Music.Time) yield return Wait(launchTime + 0.2597809f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 0.2597809f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 0.4954834 > Game.Music.Time) yield return Wait(launchTime + 0.4954834f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 0.4954834f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 0.7312317 > Game.Music.Time) yield return Wait(launchTime + 0.7312317f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 0.7312317f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 1.875046 > Game.Music.Time) yield return Wait(launchTime + 1.875046f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 1.875046f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 2.105438 > Game.Music.Time) yield return Wait(launchTime + 2.105438f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 2.105438f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            if (launchTime + 2.341125 > Game.Music.Time) yield return Wait(launchTime + 2.341125f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 2.341125f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 4.192626 > Game.Music.Time) yield return Wait(launchTime + 4.192626f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 4.192626f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 4.4245 > Game.Music.Time) yield return Wait(launchTime + 4.4245f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 4.4245f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 6.49614 > Game.Music.Time) yield return Wait(launchTime + 6.49614f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 6.49614f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            if (launchTime + 6.717957 > Game.Music.Time) yield return Wait(launchTime + 6.717957f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 6.717957f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 6.964905 > Game.Music.Time) yield return Wait(launchTime + 6.964905f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 6.964905f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            if (launchTime + 7.377823 > Game.Music.Time) yield return Wait(launchTime + 7.377823f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 7.377823f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 7.643829 > Game.Music.Time) yield return Wait(launchTime + 7.643829f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 7.643829f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 7.884827 > Game.Music.Time) yield return Wait(launchTime + 7.884827f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 7.884827f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 8.11644 > Game.Music.Time) yield return Wait(launchTime + 8.11644f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 8.11644f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 9.268097 > Game.Music.Time) yield return Wait(launchTime + 9.268097f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 9.268097f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 9.496643 > Game.Music.Time) yield return Wait(launchTime + 9.496643f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 9.496643f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 9.728271 > Game.Music.Time) yield return Wait(launchTime + 9.728271f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 9.728271f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            if (launchTime + 11.56851 > Game.Music.Time) yield return Wait(launchTime + 11.56851f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 11.56851f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 11.81665 > Game.Music.Time) yield return Wait(launchTime + 11.81665f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 11.81665f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 13.87261 > Game.Music.Time) yield return Wait(launchTime + 13.87261f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 13.87261f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 14.11113 > Game.Music.Time) yield return Wait(launchTime + 14.11113f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 14.11113f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 14.33528 > Game.Music.Time) yield return Wait(launchTime + 14.33528f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 14.33528f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 15.74725 > Game.Music.Time) yield return Wait(launchTime + 15.74725f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 15.74725f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 17.56947 > Game.Music.Time) yield return Wait(launchTime + 17.56947f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 17.56947f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 17.92728 > Game.Music.Time) yield return Wait(launchTime + 17.92728f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 17.92728f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            if (launchTime + 18.25642 > Game.Music.Time) yield return Wait(launchTime + 18.25642f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 18.25642f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 21.27608 > Game.Music.Time) yield return Wait(launchTime + 21.27608f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 21.27608f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 21.50499 > Game.Music.Time) yield return Wait(launchTime + 21.50499f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 21.50499f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 21.73396 > Game.Music.Time) yield return Wait(launchTime + 21.73396f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 21.73396f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 23.1268 > Game.Music.Time) yield return Wait(launchTime + 23.1268f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 23.1268f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            if (launchTime + 24.98239 > Game.Music.Time) yield return Wait(launchTime + 24.98239f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 24.98239f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 25.31636 > Game.Music.Time) yield return Wait(launchTime + 25.31636f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 25.31636f, blackboard, (c,i,t,l, castc, casti) => 180f, (c,i,t,l, castc, casti) => "node_31.output_patterninput"/*out*/));
            if (launchTime + 25.65976 > Game.Music.Time) yield return Wait(launchTime + 25.65976f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 25.65976f, blackboard, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => "node_35.output_patterninput"/*out*/));
            if (launchTime + 28.68259 > Game.Music.Time) yield return Wait(launchTime + 28.68259f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 28.68259f, blackboard, (c,i,t,l, castc, casti) => -90f, (c,i,t,l, castc, casti) => "node_27.output_patterninput"/*out*/));
            if (launchTime + 28.90305 > Game.Music.Time) yield return Wait(launchTime + 28.90305f);
            Player.StartCoroutine(Pattern_63/*Lazer*/(launchTime + 28.90305f, blackboard, (c,i,t,l, castc, casti) => 90f, (c,i,t,l, castc, casti) => "node_25.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_61(float launchTime, Blackboard outboard, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, enemies_input, "node_0.output_patterninput");
            object enemies = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 1.853531 > Game.Music.Time) yield return Wait(launchTime + 1.853531f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 1.853531f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.7668 > Game.Music.Time) yield return Wait(launchTime + 2.7668f);
            Player.StartCoroutine(Pattern_32/*OneShot*/(launchTime + 2.7668f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 2.991562 > Game.Music.Time) yield return Wait(launchTime + 2.991562f);
            Player.StartCoroutine(Pattern_32/*OneShot*/(launchTime + 2.991562f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 3.683075 > Game.Music.Time) yield return Wait(launchTime + 3.683075f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 3.683075f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 4.619812 > Game.Music.Time) yield return Wait(launchTime + 4.619812f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 4.619812f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 7.385437 > Game.Music.Time) yield return Wait(launchTime + 7.385437f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 7.385437f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 9.23347 > Game.Music.Time) yield return Wait(launchTime + 9.23347f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 9.23347f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.15396 > Game.Music.Time) yield return Wait(launchTime + 10.15396f);
            Player.StartCoroutine(Pattern_32/*OneShot*/(launchTime + 10.15396f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 10.35578 > Game.Music.Time) yield return Wait(launchTime + 10.35578f);
            Player.StartCoroutine(Pattern_32/*OneShot*/(launchTime + 10.35578f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.08778 > Game.Music.Time) yield return Wait(launchTime + 11.08778f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 11.08778f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 11.99264 > Game.Music.Time) yield return Wait(launchTime + 11.99264f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 11.99264f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 13.85373 > Game.Music.Time) yield return Wait(launchTime + 13.85373f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 13.85373f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 15.68948 > Game.Music.Time) yield return Wait(launchTime + 15.68948f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 15.68948f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 17.53296 > Game.Music.Time) yield return Wait(launchTime + 17.53296f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 17.53296f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 18.46158 > Game.Music.Time) yield return Wait(launchTime + 18.46158f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 18.46158f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 19.40414 > Game.Music.Time) yield return Wait(launchTime + 19.40414f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 19.40414f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 21.25835 > Game.Music.Time) yield return Wait(launchTime + 21.25835f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 21.25835f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 23.08794 > Game.Music.Time) yield return Wait(launchTime + 23.08794f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 23.08794f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 24.92862 > Game.Music.Time) yield return Wait(launchTime + 24.92862f);
            Player.StartCoroutine(Pattern_31/*4Shots*/(launchTime + 24.92862f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 25.86011 > Game.Music.Time) yield return Wait(launchTime + 25.86011f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 25.86011f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            if (launchTime + 26.77182 > Game.Music.Time) yield return Wait(launchTime + 26.77182f);
            Player.StartCoroutine(Pattern_28/*6Shots*/(launchTime + 26.77182f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_patterninput"/*out*/));
            yield break;
        }

        private IEnumerator Pattern_62(float launchTime, Blackboard outboard, string enemies_output)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeOutput(outboard, blackboard, enemies_output, "node_0.output_enemy(ies)");

            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(CreateEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => (float)toint((float)d,3f)+2f, (c,i,t,l, castc, casti) => EnemyImage.Enemy8, (c,i,t,l, castc, casti) => 1500f, (c,i,t,l, castc, casti) => HealthType.Normal, (c,i,t,l, castc, casti) => new Vector2(350f/((float)c-1f)*(float)i-175f,270f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 10f, (c,i,t,l, castc, casti) => -5f/*out*/, "node_0.output_enemy(ies)"));
            if (launchTime + 0.03791809 > Game.Music.Time) yield return Wait(launchTime + 0.03791809f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0.03791809f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => new Vector2(0f,-150f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.easeOutBack/*out*/, "node_1.output_enemy(ies)", 0.6977386f));
            if (launchTime + 7.184998 > Game.Music.Time) yield return Wait(launchTime + 7.184998f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 7.184998f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f, (c,i,t,l, castc, casti) => MoveType.Absolute, (c,i,t,l, castc, casti) => iTween.EaseType.easeInBack/*out*/, "node_4.output_enemy(ies)", 0.8440704f));
            if (launchTime + 8.04744 > Game.Music.Time) yield return Wait(launchTime + 8.04744f);
            Player.StartCoroutine(DestroyEnemy.Act(launchTime + 8.04744f, blackboard, (c,i,t,l, castc, casti) => "node_0.output_enemy(ies)", (c,i,t,l, castc, casti) => DestroyType.Immediate/*out*/));
            yield break;
        }

        private IEnumerator Pattern_63(float launchTime, Blackboard outboard, InputFunc angle_input, InputFunc enemies_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, angle_input, "node_0.output_patterninput");
            object angle = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => angle = o);

            SubscribeInput(outboard, blackboard, enemies_input, "node_1.output_patterninput");
            object enemies = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemies = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(RandomElement.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => (float)toint((float)d,3f)+1f/*out*/, "node_2.output_elements"));
            if (launchTime + 0.00189209 > Game.Music.Time) yield return Wait(launchTime + 0.00189209f);
            Player.StartCoroutine(CreateBulletPolar.Act(launchTime + 0.00189209f, blackboard, (c,i,t,l, castc, casti) => "node_2.output_elements", (c,i,t,l, castc, casti) => (float)d*10f, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => ParticleBulletType.SmallSharp, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => BulletSpace.Absolute, (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 100f*(6f-(float)d)/2f+10f*(float)i, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => new Color(0x00/255f,0xff/255f,0x00/255f), (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null/*out*/, "node_3.output_bullet(s)"));
            yield break;
        }

        private IEnumerator Pattern_64(float launchTime, Blackboard outboard, InputFunc direction_input, InputFunc enemy_input)
        {
            Blackboard blackboard = new Blackboard();

            SubscribeInput(outboard, blackboard, direction_input, "node_0.output_patterninput");
            object direction = blackboard.GetValue("node_0.output_patterninput");
            blackboard.SubscribeForChanges("node_0.output_patterninput", (b, o) => direction = o);

            SubscribeInput(outboard, blackboard, enemy_input, "node_1.output_patterninput");
            object enemy = blackboard.GetValue("node_1.output_patterninput");
            blackboard.SubscribeForChanges("node_1.output_patterninput", (b, o) => enemy = o);


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(TweenEnemy.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => "node_1.output_patterninput", (c,i,t,l, castc, casti) => "node_0.output_patterninput", (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => MoveType.Relative, (c,i,t,l, castc, casti) => iTween.EaseType.linear/*out*/, "node_2.output_enemy(ies)", 0.1024475f));
            if (launchTime + 0.0004577637 > Game.Music.Time) yield return Wait(launchTime + 0.0004577637f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.0004577637f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2c/255f,0x2a/255f,0x40/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.08898926f));
            yield break;
        }

        private IEnumerator Pattern_65(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2d/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.5668392f));
            if (launchTime + 0.9241047 > Game.Music.Time) yield return Wait(launchTime + 0.9241047f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.9241047f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2d/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.6180038f));
            if (launchTime + 1.860977 > Game.Music.Time) yield return Wait(launchTime + 1.860977f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.860977f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2d/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.5776234f));
            if (launchTime + 2.752512 > Game.Music.Time) yield return Wait(launchTime + 2.752512f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.752512f, blackboard, (c,i,t,l, castc, casti) => new Color(0x2f/255f,0x2d/255f,0x14/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.617466f));
            yield break;
        }

        private IEnumerator Pattern_66(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8566818f));
            if (launchTime + 5.523537 > Game.Music.Time) yield return Wait(launchTime + 5.523537f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.523537f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1602783f));
            if (launchTime + 5.747826 > Game.Music.Time) yield return Wait(launchTime + 5.747826f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.747826f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1568298f));
            if (launchTime + 5.992828 > Game.Music.Time) yield return Wait(launchTime + 5.992828f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 5.992828f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4052811f));
            if (launchTime + 7.369583 > Game.Music.Time) yield return Wait(launchTime + 7.369583f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.369583f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8814545f));
            if (launchTime + 12.89742 > Game.Music.Time) yield return Wait(launchTime + 12.89742f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 12.89742f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1568298f));
            if (launchTime + 13.1286 > Game.Music.Time) yield return Wait(launchTime + 13.1286f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.1286f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1568298f));
            if (launchTime + 13.35632 > Game.Music.Time) yield return Wait(launchTime + 13.35632f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.35632f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4225464f));
            if (launchTime + 14.7804 > Game.Music.Time) yield return Wait(launchTime + 14.7804f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.7804f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8638687f));
            if (launchTime + 16.61113 > Game.Music.Time) yield return Wait(launchTime + 16.61113f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 16.61113f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.192276f));
            if (launchTime + 16.96007 > Game.Music.Time) yield return Wait(launchTime + 16.96007f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 16.96007f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1970291f));
            if (launchTime + 17.30423 > Game.Music.Time) yield return Wait(launchTime + 17.30423f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.30423f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1779633f));
            if (launchTime + 20.28491 > Game.Music.Time) yield return Wait(launchTime + 20.28491f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.28491f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.126976f));
            if (launchTime + 20.51961 > Game.Music.Time) yield return Wait(launchTime + 20.51961f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.51961f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.126976f));
            if (launchTime + 20.75147 > Game.Music.Time) yield return Wait(launchTime + 20.75147f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 20.75147f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4231415f));
            if (launchTime + 22.13163 > Game.Music.Time) yield return Wait(launchTime + 22.13163f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 22.13163f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8897095f));
            if (launchTime + 23.97837 > Game.Music.Time) yield return Wait(launchTime + 23.97837f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 23.97837f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2080231f));
            if (launchTime + 24.336 > Game.Music.Time) yield return Wait(launchTime + 24.336f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 24.336f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1940155f));
            if (launchTime + 24.66565 > Game.Music.Time) yield return Wait(launchTime + 24.66565f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 24.66565f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1884155f));
            if (launchTime + 27.66954 > Game.Music.Time) yield return Wait(launchTime + 27.66954f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 27.66954f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1669922f));
            if (launchTime + 27.91571 > Game.Music.Time) yield return Wait(launchTime + 27.91571f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 27.91571f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.629921f));
            yield break;
        }

        private IEnumerator Pattern_67(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1013336f));
            if (launchTime + 0.2297974 > Game.Music.Time) yield return Wait(launchTime + 0.2297974f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.2297974f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1027832f));
            if (launchTime + 0.4681091 > Game.Music.Time) yield return Wait(launchTime + 0.4681091f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.4681091f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1155243f));
            if (launchTime + 0.6921997 > Game.Music.Time) yield return Wait(launchTime + 0.6921997f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0.6921997f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.09848022f));
            if (launchTime + 1.858155 > Game.Music.Time) yield return Wait(launchTime + 1.858155f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 1.858155f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1324005f));
            if (launchTime + 2.067902 > Game.Music.Time) yield return Wait(launchTime + 2.067902f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.067902f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1324005f));
            if (launchTime + 2.315521 > Game.Music.Time) yield return Wait(launchTime + 2.315521f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 2.315521f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4295807f));
            if (launchTime + 4.167267 > Game.Music.Time) yield return Wait(launchTime + 4.167267f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.167267f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 4.383957 > Game.Music.Time) yield return Wait(launchTime + 4.383957f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 4.383957f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 6.469193 > Game.Music.Time) yield return Wait(launchTime + 6.469193f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.469193f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 6.682801 > Game.Music.Time) yield return Wait(launchTime + 6.682801f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.682801f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 6.918289 > Game.Music.Time) yield return Wait(launchTime + 6.918289f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 6.918289f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.5290222f));
            if (launchTime + 7.60289 > Game.Music.Time) yield return Wait(launchTime + 7.60289f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.60289f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 7.854141 > Game.Music.Time) yield return Wait(launchTime + 7.854141f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 7.854141f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427302f));
            if (launchTime + 8.086533 > Game.Music.Time) yield return Wait(launchTime + 8.086533f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 8.086533f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 9.242249 > Game.Music.Time) yield return Wait(launchTime + 9.242249f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.242249f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 9.45578 > Game.Music.Time) yield return Wait(launchTime + 9.45578f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.45578f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 9.688171 > Game.Music.Time) yield return Wait(launchTime + 9.688171f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 9.688171f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4599609f));
            if (launchTime + 11.51905 > Game.Music.Time) yield return Wait(launchTime + 11.51905f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.51905f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 11.77343 > Game.Music.Time) yield return Wait(launchTime + 11.77343f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 11.77343f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 13.84927 > Game.Music.Time) yield return Wait(launchTime + 13.84927f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 13.84927f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 14.08793 > Game.Music.Time) yield return Wait(launchTime + 14.08793f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.08793f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1427307f));
            if (launchTime + 14.30149 > Game.Music.Time) yield return Wait(launchTime + 14.30149f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 14.30149f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4367676f));
            if (launchTime + 15.70379 > Game.Music.Time) yield return Wait(launchTime + 15.70379f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 15.70379f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8777933f));
            if (launchTime + 17.5424 > Game.Music.Time) yield return Wait(launchTime + 17.5424f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.5424f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2068176f));
            if (launchTime + 17.89073 > Game.Music.Time) yield return Wait(launchTime + 17.89073f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 17.89073f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2039795f));
            if (launchTime + 18.23617 > Game.Music.Time) yield return Wait(launchTime + 18.23617f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 18.23617f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1726532f));
            if (launchTime + 21.25096 > Game.Music.Time) yield return Wait(launchTime + 21.25096f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.25096f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1297607f));
            if (launchTime + 21.45366 > Game.Music.Time) yield return Wait(launchTime + 21.45366f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.45366f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1297607f));
            if (launchTime + 21.69635 > Game.Music.Time) yield return Wait(launchTime + 21.69635f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 21.69635f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.4295197f));
            if (launchTime + 23.08381 > Game.Music.Time) yield return Wait(launchTime + 23.08381f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 23.08381f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.8892059f));
            if (launchTime + 24.92239 > Game.Music.Time) yield return Wait(launchTime + 24.92239f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 24.92239f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2211761f));
            if (launchTime + 25.28779 > Game.Music.Time) yield return Wait(launchTime + 25.28779f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.28779f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.2068329f));
            if (launchTime + 25.62471 > Game.Music.Time) yield return Wait(launchTime + 25.62471f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 25.62471f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1668549f));
            if (launchTime + 28.60958 > Game.Music.Time) yield return Wait(launchTime + 28.60958f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 28.60958f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1535034f));
            if (launchTime + 28.86966 > Game.Music.Time) yield return Wait(launchTime + 28.86966f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 28.86966f, blackboard, (c,i,t,l, castc, casti) => new Color(0x40/255f,0x7b/255f,0x10/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.6297302f));
            yield break;
        }

        private IEnumerator Pattern_68(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 0f, blackboard/*out*/));
            if (launchTime + 0.2045136 > Game.Music.Time) yield return Wait(launchTime + 0.2045136f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 0.2045136f, blackboard/*out*/));
            if (launchTime + 0.4505921 > Game.Music.Time) yield return Wait(launchTime + 0.4505921f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 0.4505921f, blackboard/*out*/));
            if (launchTime + 0.6729279 > Game.Music.Time) yield return Wait(launchTime + 0.6729279f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 0.6729279f, blackboard/*out*/));
            if (launchTime + 0.9131012 > Game.Music.Time) yield return Wait(launchTime + 0.9131012f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 0.9131012f, blackboard/*out*/));
            if (launchTime + 1.129516 > Game.Music.Time) yield return Wait(launchTime + 1.129516f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 1.129516f, blackboard/*out*/));
            if (launchTime + 1.368606 > Game.Music.Time) yield return Wait(launchTime + 1.368606f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 1.368606f, blackboard/*out*/));
            if (launchTime + 1.626693 > Game.Music.Time) yield return Wait(launchTime + 1.626693f);
            Player.StartCoroutine(Pattern_69/*Flash*/(launchTime + 1.626693f, blackboard/*out*/));
            yield break;
        }

        private IEnumerator Pattern_69(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(BackgroundColor.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => new Color(0x1b/255f,0x2b/255f,0x1b/255f), (c,i,t,l, castc, casti) => null/*out*/, 0.1643982f));
            yield break;
        }

        private IEnumerator Pattern_70(float launchTime, Blackboard outboard)
        {
            Blackboard blackboard = new Blackboard();


            if (launchTime + 0 > Game.Music.Time) yield return Wait(launchTime + 0f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 0f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 14.76697 > Game.Music.Time) yield return Wait(launchTime + 14.76697f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 14.76697f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 29.57396 > Game.Music.Time) yield return Wait(launchTime + 29.57396f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 29.57396f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 44.34093 > Game.Music.Time) yield return Wait(launchTime + 44.34093f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 44.34093f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 55.38614 > Game.Music.Time) yield return Wait(launchTime + 55.38614f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 55.38614f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 56.3463 > Game.Music.Time) yield return Wait(launchTime + 56.3463f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 56.3463f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 57.27042 > Game.Music.Time) yield return Wait(launchTime + 57.27042f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 57.27042f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 260f/*out*/));
            if (launchTime + 58.18596 > Game.Music.Time) yield return Wait(launchTime + 58.18596f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 58.18596f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 520f/*out*/));
            if (launchTime + 59.13575 > Game.Music.Time) yield return Wait(launchTime + 59.13575f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 59.13575f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 60.04279 > Game.Music.Time) yield return Wait(launchTime + 60.04279f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 60.04279f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 60.95235 > Game.Music.Time) yield return Wait(launchTime + 60.95235f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 60.95235f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 62.76218 > Game.Music.Time) yield return Wait(launchTime + 62.76218f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 62.76218f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 63.70034 > Game.Music.Time) yield return Wait(launchTime + 63.70034f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 63.70034f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 64.61635 > Game.Music.Time) yield return Wait(launchTime + 64.61635f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 64.61635f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 65.57828 > Game.Music.Time) yield return Wait(launchTime + 65.57828f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 65.57828f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 65.81567 > Game.Music.Time) yield return Wait(launchTime + 65.81567f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 65.81567f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 66.04176 > Game.Music.Time) yield return Wait(launchTime + 66.04176f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 66.04176f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 66.47052 > Game.Music.Time) yield return Wait(launchTime + 66.47052f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 66.47052f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 67.43785 > Game.Music.Time) yield return Wait(launchTime + 67.43785f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 67.43785f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 68.28859 > Game.Music.Time) yield return Wait(launchTime + 68.28859f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 68.28859f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 70.17146 > Game.Music.Time) yield return Wait(launchTime + 70.17146f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 70.17146f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 71.08746 > Game.Music.Time) yield return Wait(launchTime + 71.08746f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 71.08746f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 72.00346 > Game.Music.Time) yield return Wait(launchTime + 72.00346f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 72.00346f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 72.96566 > Game.Music.Time) yield return Wait(launchTime + 72.96566f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 72.96566f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 73.20303 > Game.Music.Time) yield return Wait(launchTime + 73.20303f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 73.20303f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 73.40654 > Game.Music.Time) yield return Wait(launchTime + 73.40654f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 73.40654f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 73.85764 > Game.Music.Time) yield return Wait(launchTime + 73.85764f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 73.85764f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 74.83088 > Game.Music.Time) yield return Wait(launchTime + 74.83088f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 74.83088f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 75.69701 > Game.Music.Time) yield return Wait(launchTime + 75.69701f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 75.69701f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 76.65652 > Game.Music.Time) yield return Wait(launchTime + 76.65652f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 76.65652f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 76.99565 > Game.Music.Time) yield return Wait(launchTime + 76.99565f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 76.99565f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 77.36306 > Game.Music.Time) yield return Wait(launchTime + 77.36306f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 77.36306f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 77.52161 > Game.Music.Time) yield return Wait(launchTime + 77.52161f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 77.52161f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 78.4524 > Game.Music.Time) yield return Wait(launchTime + 78.4524f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 78.4524f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 80.38132 > Game.Music.Time) yield return Wait(launchTime + 80.38132f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 80.38132f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 80.58479 > Game.Music.Time) yield return Wait(launchTime + 80.58479f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 80.58479f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 80.81086 > Game.Music.Time) yield return Wait(launchTime + 80.81086f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 80.81086f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 81.23736 > Game.Music.Time) yield return Wait(launchTime + 81.23736f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 81.23736f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 82.20695 > Game.Music.Time) yield return Wait(launchTime + 82.20695f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 82.20695f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 83.06934 > Game.Music.Time) yield return Wait(launchTime + 83.06934f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 83.06934f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 84.0326 > Game.Music.Time) yield return Wait(launchTime + 84.0326f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 84.0326f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 84.39432 > Game.Music.Time) yield return Wait(launchTime + 84.39432f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 84.39432f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 84.73912 > Game.Music.Time) yield return Wait(launchTime + 84.73912f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 84.73912f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 84.93088 > Game.Music.Time) yield return Wait(launchTime + 84.93088f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 84.93088f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 85.85429 > Game.Music.Time) yield return Wait(launchTime + 85.85429f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 85.85429f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 87.74607 > Game.Music.Time) yield return Wait(launchTime + 87.74607f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 87.74607f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 87.98345 > Game.Music.Time) yield return Wait(launchTime + 87.98345f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 87.98345f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 96.00907 > Game.Music.Time) yield return Wait(launchTime + 96.00907f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 96.00907f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 103.4033 > Game.Music.Time) yield return Wait(launchTime + 103.4033f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 103.4033f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 118.1928 > Game.Music.Time) yield return Wait(launchTime + 118.1928f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 118.1928f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => null/*out*/));
            if (launchTime + 132.9951 > Game.Music.Time) yield return Wait(launchTime + 132.9951f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 132.9951f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 147.7364 > Game.Music.Time) yield return Wait(launchTime + 147.7364f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 147.7364f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 158.7681 > Game.Music.Time) yield return Wait(launchTime + 158.7681f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 158.7681f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 159.6992 > Game.Music.Time) yield return Wait(launchTime + 159.6992f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 159.6992f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 160.6352 > Game.Music.Time) yield return Wait(launchTime + 160.6352f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 160.6352f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 260f/*out*/));
            if (launchTime + 161.5529 > Game.Music.Time) yield return Wait(launchTime + 161.5529f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 161.5529f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 520f/*out*/));
            if (launchTime + 162.4693 > Game.Music.Time) yield return Wait(launchTime + 162.4693f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 162.4693f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 162.7288 > Game.Music.Time) yield return Wait(launchTime + 162.7288f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 162.7288f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 162.9621 > Game.Music.Time) yield return Wait(launchTime + 162.9621f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 162.9621f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 163.1879 > Game.Music.Time) yield return Wait(launchTime + 163.1879f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 163.1879f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 163.3898 > Game.Music.Time) yield return Wait(launchTime + 163.3898f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 163.3898f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 164.3442 > Game.Music.Time) yield return Wait(launchTime + 164.3442f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 164.3442f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 164.5881 > Game.Music.Time) yield return Wait(launchTime + 164.5881f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 164.5881f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 164.8199 > Game.Music.Time) yield return Wait(launchTime + 164.8199f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 164.8199f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 165.2115 > Game.Music.Time) yield return Wait(launchTime + 165.2115f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 165.2115f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 166.1679 > Game.Music.Time) yield return Wait(launchTime + 166.1679f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 166.1679f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 166.6566 > Game.Music.Time) yield return Wait(launchTime + 166.6566f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 166.6566f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 166.9004 > Game.Music.Time) yield return Wait(launchTime + 166.9004f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 166.9004f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 167.0583 > Game.Music.Time) yield return Wait(launchTime + 167.0583f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 167.0583f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 168.0037 > Game.Music.Time) yield return Wait(launchTime + 168.0037f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 168.0037f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 168.975 > Game.Music.Time) yield return Wait(launchTime + 168.975f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 168.975f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 169.1897 > Game.Music.Time) yield return Wait(launchTime + 169.1897f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 169.1897f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 169.4176 > Game.Music.Time) yield return Wait(launchTime + 169.4176f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 169.4176f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 170.1252 > Game.Music.Time) yield return Wait(launchTime + 170.1252f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 170.1252f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 170.3781 > Game.Music.Time) yield return Wait(launchTime + 170.3781f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 170.3781f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 170.5859 > Game.Music.Time) yield return Wait(launchTime + 170.5859f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 170.5859f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 170.7628 > Game.Music.Time) yield return Wait(launchTime + 170.7628f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 170.7628f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 171.7451 > Game.Music.Time) yield return Wait(launchTime + 171.7451f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 171.7451f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 171.9679 > Game.Music.Time) yield return Wait(launchTime + 171.9679f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 171.9679f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 172.2148 > Game.Music.Time) yield return Wait(launchTime + 172.2148f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 172.2148f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 172.6316 > Game.Music.Time) yield return Wait(launchTime + 172.6316f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 172.6316f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 173.5385 > Game.Music.Time) yield return Wait(launchTime + 173.5385f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 173.5385f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 174.0304 > Game.Music.Time) yield return Wait(launchTime + 174.0304f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 174.0304f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 174.2863 > Game.Music.Time) yield return Wait(launchTime + 174.2863f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 174.2863f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 174.4674 > Game.Music.Time) yield return Wait(launchTime + 174.4674f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 174.4674f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 175.3853 > Game.Music.Time) yield return Wait(launchTime + 175.3853f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 175.3853f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 176.3548 > Game.Music.Time) yield return Wait(launchTime + 176.3548f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 176.3548f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 176.5867 > Game.Music.Time) yield return Wait(launchTime + 176.5867f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 176.5867f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 176.8185 > Game.Music.Time) yield return Wait(launchTime + 176.8185f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 176.8185f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Gangnam, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 177.2266 > Game.Music.Time) yield return Wait(launchTime + 177.2266f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 177.2266f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 178.1979 > Game.Music.Time) yield return Wait(launchTime + 178.1979f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 178.1979f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 179.0899 > Game.Music.Time) yield return Wait(launchTime + 179.0899f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 179.0899f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 180.0372 > Game.Music.Time) yield return Wait(launchTime + 180.0372f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 180.0372f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 180.3931 > Game.Music.Time) yield return Wait(launchTime + 180.3931f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 180.3931f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 180.7325 > Game.Music.Time) yield return Wait(launchTime + 180.7325f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 180.7325f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 180.9258 > Game.Music.Time) yield return Wait(launchTime + 180.9258f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 180.9258f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 181.8382 > Game.Music.Time) yield return Wait(launchTime + 181.8382f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 181.8382f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 183.7406 > Game.Music.Time) yield return Wait(launchTime + 183.7406f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 183.7406f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Shoulders, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 183.9678 > Game.Music.Time) yield return Wait(launchTime + 183.9678f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 183.9678f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Head, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 184.2005 > Game.Music.Time) yield return Wait(launchTime + 184.2005f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 184.2005f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 184.6249 > Game.Music.Time) yield return Wait(launchTime + 184.6249f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 184.6249f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 185.5773 > Game.Music.Time) yield return Wait(launchTime + 185.5773f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 185.5773f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Mike, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 186.4607 > Game.Music.Time) yield return Wait(launchTime + 186.4607f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 186.4607f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 187.4195 > Game.Music.Time) yield return Wait(launchTime + 187.4195f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 187.4195f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Pelvis, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 187.7726 > Game.Music.Time) yield return Wait(launchTime + 187.7726f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 187.7726f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.CrissCross, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 188.1257 > Game.Music.Time) yield return Wait(launchTime + 188.1257f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 188.1257f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 188.3186 > Game.Music.Time) yield return Wait(launchTime + 188.3186f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 188.3186f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 189.2254 > Game.Music.Time) yield return Wait(launchTime + 189.2254f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 189.2254f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Guitar, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 191.1394 > Game.Music.Time) yield return Wait(launchTime + 191.1394f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 191.1394f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Wave, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 191.353 > Game.Music.Time) yield return Wait(launchTime + 191.353f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 191.353f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Corners, (c,i,t,l, castc, casti) => 1f/*out*/));
            if (launchTime + 199.3792 > Game.Music.Time) yield return Wait(launchTime + 199.3792f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 199.3792f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.Punk, (c,i,t,l, castc, casti) => 130f/*out*/));
            if (launchTime + 206.7945 > Game.Music.Time) yield return Wait(launchTime + 206.7945f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 206.7945f, blackboard, (c,i,t,l, castc, casti) => DanceManMovement.HandLeftRight, (c,i,t,l, castc, casti) => 65f/*out*/));
            if (launchTime + 221.3013 > Game.Music.Time) yield return Wait(launchTime + 221.3013f);
            Player.StartCoroutine(DanceMan.Act(launchTime + 221.3013f, blackboard, (c,i,t,l, castc, casti) => null, (c,i,t,l, castc, casti) => 0f/*out*/));
            yield break;
        }

    }
}