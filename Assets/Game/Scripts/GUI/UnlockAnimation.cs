﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnlockAnimation : MonoBehaviour
{
    public FlashSprite Animation;
    public GameObject ImageHolder;

    private GameObject _image;

    public static Queue<ShopShip> UnlockList = new Queue<ShopShip>();
    public static string NextScene = "MainMenu";
    private ShopShip _ship;

    void Start()
    {
        if (UnlockList.Count == 0)
        {
            LevelLoader.LoadLevel(NextScene);
        }
        else
        {
            _ship = UnlockList.Dequeue();
            _ship.Unlock();
            SetImage();
            StartCoroutine(ShowProcess());
        }
    }

    public void SetImage()
    {
        if (_image == null)
        {
            _image = Instantiate(_ship.Prefab.Image).gameObject;
            _image.transform.SetParent(ImageHolder.transform);
            _image.gameObject.layer = LayerMask.NameToLayer("Button");
            _image.transform.localPosition = Vector3.zero;
            _image.transform.localScale = Vector3.one;
            foreach (Transform child in _image.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }

    private IEnumerator ShowProcess()
    {
        Game.Music.Play("Unlock");
        Animation.ChangeAnimation(0, true, true);

        while (Animation.CurrentFrame < Animation.CurrentAnimation.FrameCount - 30)
        {
            yield return null;
        }

        if (UnlockList.Count == 0)
        {
            LevelLoader.LoadLevel(NextScene);
            NextScene = "MainMenu";
        }
        else
        {
            LevelLoader.LoadLevel("UnlockShip");
        }
    }
}
