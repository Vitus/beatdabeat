﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class Blocker : MonoBehaviour {
    [SerializeField] private tk2dTextMesh CommentLabel;
    [SerializeField] private GameObject LoadingIndicator;

    private static Blocker Prefab {
        get { return Resources.Load<Blocker>("Blocker"); }
    }

    public static Blocker Instance { get; private set; }

    public static bool IsVisible { get; private set; }

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    public static void Show(LocalizedText text = null) {
        if (Instance != null) {
            Destroy(Instance.gameObject);
        }
        Instance = Instantiate(Prefab);
        Instance.SetText(text);
        IsVisible = true;
    }

    public static void Hide(float lifeTime) {
        if (Instance == null)
            return;
        Instance.StartCoroutine(Instance.HideAfterTime(lifeTime));
    }

    private IEnumerator HideAfterTime(float lifeTime) {
        yield return this.WaitForUnscaledSeconds(lifeTime);
        Hide();
    }

    public static void Hide() {
        if (Instance != null) {
            Destroy(Instance.gameObject);
        }
        IsVisible = false;
    }

    private void SetText(LocalizedText text) {
        if (text == null) {
            CommentLabel.gameObject.SetActive(false);
            LoadingIndicator.SetActive(true);
        } else {
            CommentLabel.gameObject.SetActive(true);
            foreach (var label in CommentLabel.GetComponentsInChildren<tk2dTextMesh>()) {
                label.text = text;
            }
            LoadingIndicator.SetActive(false);
        }
    }
}