﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public enum TutorialCharacterImage
{
    Akward = 0,
    Gold = 1,
    Question = 2,
    Rambo = 3,
    Scared = 4,
    Scientist = 5,
    Strong = 6,
    Usual = 7,
}

public static class TutorialCharacterImageExtension
{
    public static string SpriteName(this TutorialCharacterImage image)
    {
        switch (image)
        {
            case TutorialCharacterImage.Akward: return "bot_akward";
            case TutorialCharacterImage.Gold: return "bot_gold";
            case TutorialCharacterImage.Question: return "bot_question";
            case TutorialCharacterImage.Rambo: return "bot_rambo";
            case TutorialCharacterImage.Scared: return "bot_scared";
            case TutorialCharacterImage.Scientist: return "bot_scientist";
            case TutorialCharacterImage.Strong: return "bot_strong";
            case TutorialCharacterImage.Usual: return "bot_usual";
            default: return "bot_usual";
        }
    }
}

public class TutorialDialog : MonoBehaviour
{
    [SerializeField] private tk2dTextMesh Text;
    [SerializeField] private tk2dSprite Sprite;
    [SerializeField] private float CharTypeTime;
    [SerializeField] private float CharTypeTimeAccelerated;
    [SerializeField] private GameObject Triangle;
    [SerializeField] private float TriangleToggleTime;
    private bool _speedUp;
    private float _timeScale;
    private bool _close;
    private bool _showTriangle;
    private bool _wasActive;
    private TutorialCharacterImage _image;
    public bool Hide { get; private set; }
    public bool Complete { get; private set; }
    
    public Action CompleteCallback { get; set; }

    void OnEnable()
    {
        _timeScale = Time.timeScale;
        Time.timeScale = 0;
    }

    void OnDisable()
    {
        Time.timeScale = _timeScale;
    }

    public IEnumerator WaitForComplete()
    {
        while (!Complete)
            yield return null;
    }

    public void Show(LocalizedText text, TutorialCharacterImage image, bool close, bool showTriangle)
    {
        _wasActive = gameObject.activeSelf;
        _image = image;

        gameObject.SetActive(true);
        StopAllCoroutines();
        CompleteCallback = null;

        _showTriangle = showTriangle;
        _close = close;
        Complete = false;
        Hide = false;
        Triangle.SetActive(false);
        Text.GetComponent<LocalizeTextMesh>().SetLocalization(text);
        Text.maxChars = 0;

        Sprite.gameObject.SetActive(false);

        StartCoroutine(WaitClick());
        StartCoroutine("Type");
    }

    private IEnumerator Type()
    {
        if (!_wasActive)
        {
            yield return StartCoroutine(WaitForSecondsNoTimeScale(0.5f));
        }

        if (Sprite.CurrentSprite.name != _image.SpriteName())
        {
            Sprite.gameObject.SetActive(false);
            Sprite.SetSprite(_image.SpriteName());
        }
        Sprite.gameObject.SetActive(true);

        yield return StartCoroutine(WaitForSecondsNoTimeScale(0.5f));

        while (Text.maxChars < Text.text.Length)
        {
            yield return StartCoroutine(WaitForSecondsNoTimeScale(_speedUp ? CharTypeTimeAccelerated : CharTypeTime));
            if (Text.maxChars%2 == 0) {
                Game.Sound.PlaySound(SoundType.TutorialType);
            }
            Text.maxChars++;
        }
        _speedUp = false;
        while (true)
        {
            yield return StartCoroutine(WaitForSecondsNoTimeScale(TriangleToggleTime));
            Triangle.gameObject.SetActive(!Triangle.gameObject.activeSelf && _showTriangle);
        }
    }

    private IEnumerator WaitClick()
    {
        while (true) {
            yield return null;
            if (Input.GetMouseButtonDown(0))
            {
                if (Text.maxChars >= Text.text.Length)
                {
                    Triangle.gameObject.SetActive(false);
                    if (_close) {
                        Hide = true;
                        gameObject.BroadcastMessage("Hide");
                        yield return StartCoroutine(WaitForSecondsNoTimeScale(0.5f));
                        gameObject.SetActive(false);
                        if (!Tutorial.Main.IsInShop) {
                            Game.Player.Focus.ForceSpeedUp();
                            Game.Player.Focus.FastCurrentToTarget();
                        }
                    }
                    Complete = true;
                    if (CompleteCallback != null)
                    {
                        CompleteCallback();
                        CompleteCallback = null;
                    } 
                    yield break;
                }
                else {
                    Text.maxChars = Text.text.Length;
                }
            }
        }
    }

    private IEnumerator WaitForSecondsNoTimeScale(float time)
    {
        var startTime = Time.unscaledTime;
        while (Time.unscaledTime < startTime + time)
        {
            yield return null;
        }
    }

    public IEnumerator WaitForType()
    {
        while (Text.maxChars < Text.text.Length)
        {
            yield return null;
        }
    }

    public IEnumerator WaitForHide() {
        while (!Hide) {
            yield return null;
        }
    }
}
