﻿using UnityEngine;
using System.Collections;

public class FlashSpriteVariant : MonoBehaviour {
    public int _variantNum;
    
    void Start () {
	    GetComponent<FlashSprite>().SetVariant("watch_icon", _variantNum);
	}
}
