﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private bool ShowOnlyAfterFirstHit;
    [SerializeField] private Health Owner;

    private tk2dUIProgressBar progressBar;

	void Awake()
	{
        if (Owner == null)
        {
            Destroy(gameObject);
            return;
        }
	    progressBar = GetComponent<tk2dUIProgressBar>();
        if (ShowOnlyAfterFirstHit)
            gameObject.SetActive(false);
        NEvent.AddListener<HitEvent>(Owner, OnHit);
    }

    private void OnHit(HitEvent e)
    {
        if (Owner.enabled && Owner.AsBoss == false)
            gameObject.SetActive(true);
    }

    void OnEnable()
    {
        if (Owner == null)
            return;
        if (!NEvent.HasListener<HitEvent>(Owner))
        {
            NEvent.AddListener<HitEvent>(Owner, OnHit);
        }
    }

    void OnDisable()
    {
        if (Owner == null)
            return;
        NEvent.RemoveListener<HitEvent>(Owner, OnHit);
    }

    void Update ()
    {
        if (Owner == null)
            return;
        progressBar.Value = (float)Owner.Current / Owner.Max;
    }
}
