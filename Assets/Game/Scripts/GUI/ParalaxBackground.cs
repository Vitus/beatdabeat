﻿using UnityEngine;
using System.Collections;

public class ParalaxBackground : MonoBehaviour {
    [SerializeField] private float ParalaxMultiplier;
    [SerializeField] private float MouseSensitivity;

    private void LateUpdate() {
        transform.SetXPosition((Camera.main.transform.position.x - MouseOffset())*ParalaxMultiplier);
    }

    private float MouseOffset() {
        return (Input.mousePosition.x - Screen.width/2f)*MouseSensitivity;
    }

}