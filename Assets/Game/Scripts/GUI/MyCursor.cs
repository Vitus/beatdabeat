﻿using UnityEngine;
using System.Collections;

public class MyCursor : MonoBehaviour {
    [SerializeField] private Texture2D DefaultCursor;
    [SerializeField] private Vector2 DefaultHotspot;
    [SerializeField] private Texture2D DefaultInGameCursor;
    [SerializeField] private Vector2 DefaultInGameHotspot;
    [SerializeField] private Texture2D HoverCursor;
    [SerializeField] private Vector2 HoverHotspot;
    [SerializeField] private Texture2D ClickCursor;
    [SerializeField] private Vector2 ClickHotspot;
    [SerializeField] private Texture2D ShopCursor;
    [SerializeField] private Vector2 ShopHotspot;
    [SerializeField] private Texture2D ShopClickCursor;
    [SerializeField] private Vector2 ShopClickHotspot;

    private bool _overShop;

    private static MyCursor main;
    public static bool OverShop {
        get { return main._overShop; }
        set { main._overShop = value; }
    }

    private void Awake() {
        main = this;
    }

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WP8
    private void Update() {
        Camera uiCamera = tk2dUIManager.Instance.GetUICameraForControl(gameObject);
        if (uiCamera == null) {
            uiCamera = Camera.main;
        }
        Ray mouseRay = uiCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;
        bool isOver = Physics.Raycast(mouseRay, out hitInfo, float.MaxValue, 1 << LayerMask.NameToLayer("Button"));
        if (Input.GetMouseButton(0)) {
            if (_overShop) {
                Cursor.SetCursor(ShopClickCursor, ShopClickHotspot, CursorMode.Auto);
            } else {
                if (Application.loadedLevelName == "Game") {
                    Cursor.SetCursor(DefaultInGameCursor, DefaultInGameHotspot, CursorMode.Auto);
                } else {
                    Cursor.SetCursor(ClickCursor, DefaultHotspot, CursorMode.Auto);
                }
            }
        } else {
            if (isOver) {
                if (_overShop) {
                    Cursor.SetCursor(ShopCursor, ShopHotspot, CursorMode.Auto);
                } else {
                    Cursor.SetCursor(HoverCursor, HoverHotspot, CursorMode.Auto);
                }
            } else {
                if (_overShop) {
                    Cursor.SetCursor(ShopCursor, ShopHotspot, CursorMode.Auto);
                } else {
                    if (Application.loadedLevelName == "Game") {
                        Cursor.SetCursor(DefaultInGameCursor, DefaultInGameHotspot, CursorMode.Auto);
                    } else {
                        Cursor.SetCursor(DefaultCursor, DefaultHotspot, CursorMode.Auto);
                    }
                }
            }
        }
    }
#endif
}