﻿using UnityEngine;
using System.Collections;

public class ParticlesAfterTime : MonoBehaviour {

    public MinMaxFloat Delay;
    private ParticleSystem _particeSystem;

    void Awake() {
        _particeSystem = GetComponent<ParticleSystem>();
        _particeSystem.enableEmission = false;
    }

    void OnEnable() {
        StartCoroutine(Emit());
    }

    IEnumerator Emit() {
        float elapsedTime = 0;
        float delay = Delay.Random();
        while (elapsedTime < delay) {
            elapsedTime += Time.unscaledDeltaTime;
            yield return null;
        }
        _particeSystem.enableEmission = true;
        _particeSystem.Stop();
        _particeSystem.Play(true);
    }

    void Update() {
        _particeSystem.Simulate(Time.unscaledDeltaTime, true, false);
    }
}
