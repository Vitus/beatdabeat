﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class FullMenuPanel : MonoBehaviour {
    public tk2dSprite[] Images;
    public tk2dTextMesh[] Texts;

    public tk2dSprite TextPanel;

    public float SwapInterval;
    public float SwapDuration;

    private int _currentImageNum = 0;

    public static FullMenuPanel Show() {
        return Instantiate(Resources.Load<FullMenuPanel>("FullVersionBanner"));
    }

    private void OnEnable() {
        StartCoroutine(SwapImages());
    }

    private IEnumerator SwapImages() {
        _currentImageNum = 0;
        for (int i = 0; i < Images.Length; i++) {
            Images[i].gameObject.SetActive(false);
            Texts[i].color = Texts[i].color.WithAlpha(0);
        }
        yield return StartCoroutine(Wait(1f));
        while (true) {
            int nextImageNum = _currentImageNum + 1;
            if (nextImageNum >= Images.Length) {
                nextImageNum -= Images.Length;
            }
            Images[nextImageNum].gameObject.SetActive(true);
            Images[nextImageNum].transform.SetZLocalPosition(-0.1f);
            Images[_currentImageNum].transform.SetZLocalPosition(0);
            this.Tween(new TweenSettings<float>() {
                StartValue = 0,
                EndValue = 1,
                OnExecute = delegate(float value) {
                    Images[nextImageNum].color = Images[nextImageNum].color.WithAlpha(value);
                    Texts[nextImageNum].color = Texts[nextImageNum].color.WithAlpha(value*TextPanel.color.a);
                    Texts[_currentImageNum].color =
                        Texts[_currentImageNum].color.WithAlpha((1 - value)*TextPanel.color.a);
                },
                Duration = SwapDuration,
                IgnoreTimeScale = true,
            });
            yield return StartCoroutine(Wait(SwapDuration + 0.1f));
            Images[_currentImageNum].gameObject.SetActive(false);
            _currentImageNum = nextImageNum;
            yield return StartCoroutine(Wait(SwapInterval));
        }
    }

    private void OnCloseClick() {
        StartCoroutine(CloseProcess());
    }

    private IEnumerator CloseProcess() {
        BroadcastMessage("Hide");
        foreach (var text in Texts) {
            text.gameObject.SetActive(false);
        }
        yield return StartCoroutine(Wait(1f));
        Destroy(gameObject);
    }

    private void OnGetClick() {
        Application.OpenURL("https://itunes.apple.com/us/app/beat-da-beat/id1022268606");
        OnCloseClick();
    }

    private IEnumerator Wait(float time) {
        float start = Time.unscaledTime;
        while (start + time > Time.unscaledTime) {
            yield return null;
        }
    }
}