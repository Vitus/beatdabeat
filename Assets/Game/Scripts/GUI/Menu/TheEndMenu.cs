﻿using UnityEngine;
using System.Collections;

public class TheEndMenu : MonoBehaviour {
    [SerializeField] private GameObject credits;

    private FlashSprite _sprite;

	IEnumerator Start ()
	{
        _sprite = GetComponent<FlashSprite>();
        StartCoroutine(KickAppear());
	    Time.timeScale = 0f;
        yield return StartCoroutine(PlayVideo("Final1.mp4"));
	    Time.timeScale = 1f;

        GetComponent<AudioSource>().Play();

        while (_sprite.CurrentFrame != _sprite.CurrentAnimation.FrameCount - 1)
        {
            yield return null;
        }
        _sprite.ChangeAnimation(1, false, true);
	    StartCoroutine(KickCommon());
        iTween.MoveTo(credits, new Vector3(0, 0, -1), 5f);
	}

    private IEnumerator KickCommon()
    {
        float previousFrame = _sprite.CurrentFrame;
        while (true)
        {
            if ((previousFrame < 32 && _sprite.CurrentFrame >= 32) || (previousFrame < 182 && _sprite.CurrentFrame >= 182))
            {
                Game.Sound.PlaySound(SoundType.FinalKick);
            }
            previousFrame = _sprite.CurrentFrame;
            yield return null;
        }
    }

    private IEnumerator KickAppear()
    {
        while (_sprite.CurrentFrame < 181)
            yield return null;
        Game.Sound.PlaySound(SoundType.FinalKick);
        while (_sprite.CurrentFrame < 331)
            yield return null;
        Game.Sound.PlaySound(SoundType.FinalKick);
        while (_sprite.CurrentFrame < 481)
            yield return null;
        Game.Sound.PlaySound(SoundType.FinalKick);
    }

    void OnBackClick()
    {
        if (UnlockAnimation.UnlockList.Count > 0)
        {
            LevelLoader.LoadLevel("UnlockShip");
        }
        else
        {
            LevelLoader.LoadLevel("MainMenu");
        }
    }

    IEnumerator PlayVideo(string videoPath)
    {
        Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
    }

}
