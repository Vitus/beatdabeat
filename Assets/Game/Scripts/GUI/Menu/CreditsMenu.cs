﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class CreditsMenu : MonoBehaviour {
    [SerializeField] private tk2dSpriteAnimator YuriyAnimator;

    [SerializeField] private FlashSprite Background;

    [SerializeField] private GameObject DevelopersTab;
    [SerializeField] private GameObject CreditsTab;
    [SerializeField] private GameObject MusiciansTab;

    [SerializeField] private GameObject AllMusicians;
    [SerializeField] private GameObject[] MusiciansInfo;
    [SerializeField] private GameObject MusiciansButtons;

    [SerializeField] private tk2dSprite BigFade;
    [SerializeField] private tk2dSprite SmallFade;

    private GameObject _currentTab;

    private void OnBackClick() {
        MenuGUIPosition.Main.ShowMainMenu();
    }

    private void OnEnable() {
        StartCoroutine(Appear());
    }

    private IEnumerator Appear() {
        Background.ChangeAnimation(0, true, true);
        DevelopersTab.SetActive(false);
        MusiciansTab.SetActive(false);
        CreditsTab.SetActive(false);
        yield return StartCoroutine(WaitForLastFrame(Background));
        StartCoroutine(ShowTab(DevelopersTab));
    }

    private IEnumerator WaitForLastFrame(FlashSprite animation) {
        while (animation.CurrentFrame != animation.CurrentAnimation.FrameCount - 1) {
            Background.GetComponent<MeshFilter>().sharedMesh.RecalculateBounds();
            yield return null;
        }
    }

    private IEnumerator ShowTab(GameObject nextTab) {
        yield return StartCoroutine(FadeIn(BigFade));
        if (_currentTab != null) {
            _currentTab.SetActive(false);
        }
        _currentTab = nextTab;
        if (_currentTab == MusiciansTab) {
            AllMusicians.SetActive(true);
            foreach (GameObject info in MusiciansInfo) {
                info.SetActive(false);
            }
        }
        _currentTab.SetActive(true);
        yield return StartCoroutine(FadeOut(BigFade));
    }

    private IEnumerator ShowMusician(int num) {
        yield return StartCoroutine(FadeIn(SmallFade));
        AllMusicians.SetActive(false);
        foreach (GameObject info in MusiciansInfo) {
            info.SetActive(false);
        }
        MusiciansInfo[num].SetActive(true);
        yield return StartCoroutine(FadeOut(SmallFade));
    }

    private IEnumerator FadeIn(tk2dSprite Fade) {
        Fade.gameObject.SetActive(true);
        iTween.ValueTo(Fade.gameObject, iTween.Hash(I.Time, 0.5, I.From, 0, I.To, 1, I.OnUpdate, "Alpha"));
        yield return new WaitForSeconds(0.5f);
    }

    private IEnumerator FadeOut(tk2dSprite Fade) {
        iTween.ValueTo(Fade.gameObject, iTween.Hash(I.Time, 0.5, I.From, 1, I.To, 0, I.OnUpdate, "Alpha"));
        yield return new WaitForSeconds(0.5f);
        Fade.gameObject.SetActive(false);
    }

    private void OnShowMusicianClick(tk2dUIItem item) {
        int num = Convert.ToInt32(item.name.Substring(0, 1));
        StartCoroutine(ShowMusician(num));
    }

    private void OnMusiciansClick() {
        StartCoroutine(ShowTab(MusiciansTab));
    }

    private void OnDevelopersClick() {
        StartCoroutine(ShowTab(DevelopersTab));
    }

    private void OnSponsorClick() {
        StartCoroutine(ShowTab(CreditsTab));
    }

    private void Update() {
        if (!YuriyAnimator.Playing) {
            YuriyAnimator.Play("CreditsYuriy" + Random.Range(1, 3));
        }
    }

}