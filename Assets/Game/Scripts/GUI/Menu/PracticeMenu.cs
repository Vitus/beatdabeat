﻿using UnityEngine;
using System.Collections;

public class PracticeMenu : MonoBehaviour
{
    [SerializeField] private tk2dUIToggleButtonGroup DifficultyGroup;
    [SerializeField] private tk2dUIToggleButtonGroup LevelGroup;
    [SerializeField] private tk2dTextMesh Score;
    [SerializeField] private tk2dUIToggleButton GodMode;
    [SerializeField] private tk2dUIToggleButton InfiniteBomb;
    [SerializeField] private GameObject[] LockedImages;
    [SerializeField] private GameObject[] UnlockingImages;
    [SerializeField] private tk2dUIItem[] LevelButtons;

    private bool _redraw = false;

    void OnEnable()
    {
        _redraw = true;
    }

    void Update()
    {
        if (_redraw)
            Redraw();
    }

    public int GetPreviousUnlockedLevel(int difficulty) {
        if (Game.Level.IsPracticeUnlocked)
            return Game.Level.LevelScripts.Length - 1;
        else
            return PlayerPrefs.GetInt(string.Format("{0}.UnlockedPractice", difficulty), -1);
    }

    public void SetPreviousUnlockedLevel(int difficulty, int value)
    {
        PlayerPrefs.SetInt(string.Format("{0}.UnlockedPractice", difficulty), value);
    }

    private void Redraw()
    {
        DifficultyGroup.SelectedIndex = Game.Difficulty - 1;
        if (Game.Level.CurrentLevel > Game.Level.GetMaxUnlockedLevel(Game.Difficulty) || Game.Level.CurrentLevel == -1)
        {
            LevelGroup.SelectedIndex = 0;
        }
        else
        {
            LevelGroup.SelectedIndex = Game.Level.CurrentLevel;
        }
        GodMode.IsOn = Game.Cheat.InfiniteHealth;
        InfiniteBomb.IsOn = Game.Cheat.InfiniteBomb;

        Score.text = Game.ScoreManager.GetPracticeScore().ToString();

        for (int i = 0; i < 8; i++)
        {
            LockedImages[i].SetActive(false);
            UnlockingImages[i].SetActive(false);
            LevelButtons[i].enabled = true;

            int maxLevel = Game.Level.GetMaxUnlockedLevel(Game.Difficulty);
            int prevLevel = GetPreviousUnlockedLevel(Game.Difficulty);
            if (i > maxLevel)
            {
                LockedImages[i].SetActive(true);
                LevelButtons[i].enabled = false;
            }
            else
            {
                if (prevLevel < i)
                {
                    UnlockingImages[i].SetActive(true);
                    UnlockingImages[i].GetComponent<FlashSprite>().CurrentFrame = 0;
                    UnlockingImages[i].GetComponent<FlashSprite>().ChangeAnimation(0, true, true);
                    SetPreviousUnlockedLevel(Game.Difficulty, i);
                }
            }
        }
        _redraw = false;
    }

    private void OnBackClick()
    {
        MenuGUIPosition.Main.ShowMainMenu();
    }

    private void OnDifficultyClick(tk2dUIToggleButtonGroup buttonGroup)
    {
        Game.DifficultyManager.Current = buttonGroup.SelectedIndex + 1;
        _redraw = true;
    }

    private void OnLevelClick(tk2dUIToggleButtonGroup buttonGroup)
    {
        Game.Level.StartLevel = buttonGroup.SelectedIndex;
        Game.Level.CurrentLevel = buttonGroup.SelectedIndex;
        _redraw = true;
    }

    private void OnGodModeClick(tk2dUIToggleButton button)
    {
        Game.Cheat.InfiniteHealth = button.IsOn;
    }    
    
    private void OnBombClick(tk2dUIToggleButton button)
    {
        Game.Cheat.InfiniteBomb = button.IsOn;
    }

    private void OnPlayClick()
    {
        iTween.ValueTo(Game.Music.gameObject, iTween.Hash(I.Time, 0.25f, I.To, 0, I.From, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
        Game.Level.IsPractice = true;
        LevelLoader.LoadLevel("Shop");
    }
}