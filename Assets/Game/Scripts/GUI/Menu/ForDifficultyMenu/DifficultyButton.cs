﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

[RequireComponent(typeof (tk2dUIToggleButton))]
public class DifficultyButton : MonoBehaviour {
    [SerializeField] private int DifficultyNum;
    [SerializeField] private tk2dUISpriteAnimator Animation;

    [SerializeField] private FlashSprite LockSprite;
    [SerializeField] private GameObject UnlockButton;
    [SerializeField] private GameObject ConfirmationButton;

    private tk2dUIToggleButton toggleButton;
    private bool _isSelected;

    private void Awake() {
        if (LockSprite != null) {
            LockSprite.SetPlay(false);
        }
        toggleButton = GetComponent<tk2dUIToggleButton>();
    }

    private void OnEnable() {
        toggleButton.OnToggle += OnToggle;
        
        UpdateLockStuff();
    }

    void Start() {
        _isSelected = toggleButton.IsOn;
    }

    private void UpdateLockStuff() {
        if (DifficultyNum == 4) {
            ConfirmationButton.SetActive(false);
            if (Game.DifficultyManager.IsBadassUnlocked) {
                Animation.Sprite.color = Color.white;
                LockSprite.gameObject.SetActive(false);
                UnlockButton.SetActive(false);
            } else {
                Animation.Sprite.color = Color.black;
                LockSprite.gameObject.SetActive(true);
                UnlockButton.SetActive(true);
            }
        }
    }

    private void OnDisable() {
        toggleButton.OnToggle -= OnToggle;
    }

    public void OnUnlockClick() {
        toggleButton.IsOn = true;
        Debug.Log(Currency.Current);
        Debug.Log(Game.DifficultyManager.BadassUnlockCost);
        if (Currency.Current >= Game.DifficultyManager.BadassUnlockCost) {
            ConfirmationButton.SetActive(true);
        }
    }

    public void OnConfirmClick() {
        toggleButton.IsOn = true;
        if (Currency.Current >= Game.DifficultyManager.BadassUnlockCost) {
            Currency.Current -= Game.DifficultyManager.BadassUnlockCost;
            Game.DifficultyManager.IsBadassUnlocked = true;
            StartCoroutine(UnlockProcess());
        }
    }

    private IEnumerator UnlockProcess() {
        UnlockButton.SetActive(false);
        ConfirmationButton.SetActive(false);
        LockSprite.ChangeAnimation(0, true, true);
        LockSprite.SetPlay(true);
        Debug.Log("Unlocking");
        while (LockSprite.CurrentFrame < 80) {
            Debug.Log("Playing");
            yield return null;
        }
        Debug.Log("B&W");
        LockSprite.Tween(new TweenSettings<float>() {
            StartValue = 0,
            EndValue = 1,
            Duration = 0.5f,
            OnExecute = t => Animation.Sprite.color = Color.Lerp(Color.black, Color.white, t),
        });
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Complete");
        OnToggle(null);
    }

    private void OnClick() {
        if (IsUnlocked && toggleButton.IsOn && _isSelected) {
            iTween.ValueTo(MenuGUIPosition.Main.gameObject,
                iTween.Hash(I.Time, 0.25f, I.From, Game.Music.MaxVolume, I.To, 0.0000001f, I.OnUpdate,
                    "DiffSoundVolume"));

            Game.DifficultyManager.Current = DifficultyNum;
            Game.Level.StartLevel = 0;
            Game.Level.CurrentLevel = 0;
            LevelLoader.LoadLevel("Shop");
        }
        _isSelected = true;
    }

    public bool IsUnlocked {
        get { return DifficultyNum != 4 || Game.DifficultyManager.IsBadassUnlocked; }
    }

    public void OnToggle(tk2dUIToggleButton toggle) {
        if (toggleButton.IsOn) {
            if (MenuGUIPosition.Main.CurrentTarget == DifficultyMenu.Main.transform) {
                MenuGUIPosition.Main.EnableDiffSound(Convert.ToInt32(name.Substring(0, 1)));
            }
            if (IsUnlocked) {
                Animation.Play();
            }
        } else {
            _isSelected = false;
            Animation.StopAndResetFrame();

            if (DifficultyNum == 4) {
                ConfirmationButton.SetActive(false);
            }
        }
    }
}