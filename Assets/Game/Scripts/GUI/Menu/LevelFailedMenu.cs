﻿using System;
using System.Collections.Generic;
using LevelParserV3;
using NelliTweenEngine;
using UnityEngine;
using System.Collections;
using System.Reflection;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

public class LevelFailedMenu : MonoBehaviour {
    [SerializeField] private FlashSprite AppearAnimation;
    [SerializeField] private GameObject GameOverLabel;
    [SerializeField] private tk2dSprite GGFly;
    [SerializeField] private tk2dSprite GGSmacked;
    [SerializeField] private tk2dSprite White;
    [SerializeField] private GameObject PlayerExplode;
    [SerializeField] private GameObject FailMenuHolder;
    [SerializeField] private tk2dSprite DarkenBackground;
    [SerializeField] private GameObject Title;

    [SerializeField] private ReviveButton ReviveButton;

    [SerializeField] private float DecisionTime;
    [SerializeField] private GameObject TimerBar;

    [SerializeField] private tk2dUISpriteAnimator LeftAlien;
    [SerializeField] private tk2dUISpriteAnimator RightAlien;
    [SerializeField] private tk2dUISpriteAnimator DanceMan;

    private float _timeScale;
    private GameObject _shipImage;
    private bool _videoShown;
    private Vector3 _titlePosition;

    private Vector3 _leftAlienPosition;
    private Vector3 _rightAlienPosition;

    private void Awake() {
        _titlePosition = Title.transform.position;
        _leftAlienPosition = LeftAlien.transform.localPosition;
        _rightAlienPosition = RightAlien.transform.localPosition;
        ReviveButton.ReviveClicked += Revive;
    }

    private void OnEnable() {
        LeftAlien.transform.localPosition = _leftAlienPosition;
        RightAlien.transform.localPosition = _rightAlienPosition;

        LeftAlien.gameObject.SetActive(true);
        RightAlien.gameObject.SetActive(true);

        Game.Player.Ship.enabled = false;
        Game.Player.Focus.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        _shipImage = Game.Player.Ship.transform.Find("Image").gameObject;

        _timeScale = Time.timeScale;
        StartCoroutine(SlowDown());

        Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
            StartValue = 10f,
            EndValue = 0,
            Duration = 1f,
            IgnoreTimeScale = true,
            AffectAxis = AffectAxis.XY
        });
        Instantiate(PlayerExplode, Game.Player.Ship.transform.position + Vector3.back, Quaternion.identity);

        InvokeRepeating("Explode", 0.15f, 0.2f);
    }

    private void OnDisable() {
        Time.timeScale = _timeScale;
    }

    private void Explode() {
        Game.Sound.PlayRandomSound(SoundType.GameExplosion0, SoundType.GameExplosion1, SoundType.GameExplosion2,
            SoundType.GameExplosion3);
        Instantiate(PlayerExplode,
            Game.Player.Ship.transform.position + Vector3.back +
            new Vector3(Random.Range(-25f, 25f), Random.Range(-25f, 25f)), Quaternion.identity);
    }

    private IEnumerator SlowDown() {
        DanceManAnimator.Main.Die();
        LevelPlayer.Main.LevelLaunched = false;
        GGFly.gameObject.SetActive(false);
        GGSmacked.gameObject.SetActive(false);
        White.GetComponent<MeshRenderer>().enabled = false;

        Game.Sound.PlaySound(SoundType.GameOverFly);
        transform.Tween(new TweenSettings<float> {
            StartValue = _timeScale,
            EndValue = 0,
            OnExecute = value => Time.timeScale = value,
            OnComplete = () => Game.Music.Pause(),
            IgnoreTimeScale = true,
            Duration = 4f,
        });

        DarkenBackground.TweenAlpha(new TweenSettings<float>() {
            StartValue = 0,
            EndValue = 1,
            Easing = Ease.Linear,
            Duration = 1.3f,
            IgnoreTimeScale = true,
        });

        GGFly.gameObject.SetActive(true);
        GGFly.transform.TweenPosition(new TweenSettings<Vector3>() {
            StartValue = _shipImage.transform.position,
            EndValue = Vector3.one*0.1f,
            AffectAxis = AffectAxis.XY,
            Easing = Ease.InCubic,
            Duration = 1.3f,
            IgnoreTimeScale = true,
        });
        GGFly.transform.TweenLocalScale(new TweenSettings<Vector3>() {
            StartValue = Vector3.zero,
            EndValue = new Vector3(_shipImage.transform.position.x < 0 ? -1 : 1, 1, 1),
            AffectAxis = AffectAxis.XY,
            Easing = Ease.InCubic,
            Duration = 1.3f,
            IgnoreTimeScale = true,
            OnComplete = delegate {
                GGFly.gameObject.SetActive(false);
                GGSmacked.gameObject.SetActive(true);
                GGSmacked.transform.localPosition = Vector3.zero;
                Game.Sound.PlaySound(SoundType.GameOverHit);
                GGSmacked.transform.localScale = new Vector3(_shipImage.transform.position.x < 0 ? -1 : 1, 1, 1);
                Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
                    StartValue = 30,
                    EndValue = 30,
                    Duration = 0.25f,
                    IgnoreTimeScale = true,
                });
            }
        });

        White.GetComponent<MeshRenderer>().enabled = true;
        White.TweenAlpha(new TweenSettings<float>() {
            StartValue = 1f,
            EndValue = 0f,
            Delay = 1.3f,
            Duration = 0.2f,
            IgnoreTimeScale = true
        });
        yield return StartCoroutine(Wait(2f));
        White.GetComponent<MeshRenderer>().enabled = false;

        GGSmacked.transform.TweenLocalPosition(new TweenSettings<Vector3>() {
            StartValue = Vector3.zero,
            EndValue = Vector3.down*1000,
            AffectAxis = AffectAxis.XY,
            Easing = Ease.InQuad,
            Duration = 4f,
            IgnoreTimeScale = true
        });

        yield return StartCoroutine(Wait(3f));

        OnSkip();
    }

    private IEnumerator FightAnimation() {
        LeftAlien.Play("AlienStand");
        LeftAlien.AnimationCompleted += (animator, clip) => LeftAlien.Play("AlienStand");
        RightAlien.Play("AlienStand");
        RightAlien.AnimationCompleted += (animator, clip) => RightAlien.Play("AlienStand");
        DanceMan.Play("DanceManStandHurt");
        DanceMan.AnimationCompleted += (animator, clip) => DanceMan.Play("DanceManStandHurt");
        while (true) {
            yield return StartCoroutine(Wait(Random.Range(1f, 2f)));

            var random = Random.Range(0, 2);
            var currentAlien = random == 1 ? LeftAlien : RightAlien;

            currentAlien.Play("AlienPunch");
            yield return StartCoroutine(Wait(0.1f));
            DanceMan.Play("DanceManGotHit");
            if (random == 1) {
                DanceMan.transform.parent.SetXLocalScale(-1);
            } else {
                DanceMan.transform.parent.SetXLocalScale(1);
            }
            yield return null;
        }
    }

    private IEnumerator Timer() {
        float elapsedTime = 0;
        while (elapsedTime < DecisionTime) {
            TimerBar.transform.SetXLocalScale(1 - elapsedTime/DecisionTime);
            if (!Blocker.IsVisible) {
                elapsedTime += Time.unscaledDeltaTime;
            }
            yield return null;
        }
        yield return null;
        yield return null;
        OnSkip();
    }

    private IEnumerator AnimateTitle() {
        Title.GetComponent<tk2dSprite>().color = Title.GetComponent<tk2dSprite>().color.WithAlpha(0);
        yield return StartCoroutine(Wait(0.5f));
        Title.SetActive(true);
        Title.transform.TweenLocalScale(new TweenSettings<Vector3>() {
            StartValue = Vector3.zero,
            EndValue = Vector3.one,
            IgnoreTimeScale = true,
            Duration = 0.5f,
            Easing = Ease.OutBack
        });
        Title.TweenAlpha(new TweenSettings<float>() {
            StartValue = 0f,
            EndValue = 1f,
            IgnoreTimeScale = true,
            Duration = 0.5f,
            Easing = Ease.Linear
        });
        Title.transform.TweenPosition(new TweenSettings<Vector3>() {
            StartValue = _titlePosition,
            EndValue = _titlePosition + Vector3.down*15f,
            IgnoreTimeScale = true,
            Duration = 0.4f,
            Delay = 0.5f,
            Easing = Ease.InQuad,
            LoopType = LoopType.PingPong,
            LoopCount = -1,
        });
    }

    private IEnumerator Wait(float delay) {
        float elapsedTime = 0;
        while (elapsedTime < delay) {
            elapsedTime += Time.unscaledDeltaTime;
            yield return null;
        }
    }

    private void OnSkip() {
        Game.Level.CurrentLevel = Game.Level.StartLevel;
        StopCoroutine("Timer");
        StartCoroutine(ShowGameOver());
    }

    private IEnumerator ShowGameOver() {
        if (FailMenuHolder.activeSelf) {
            FailMenuHolder.transform.TweenPosition(new TweenSettings<Vector3>() {
                StartValue = FailMenuHolder.transform.position,
                EndValue = FailMenuHolder.transform.position + Vector3.down*500,
                Duration = 0.5f,
                Easing = Ease.InQuad,
                IgnoreTimeScale = true
            });
            Title.TweenAlpha(new TweenSettings<float>() {
                StartValue = 1,
                EndValue = 0,
                Duration = 0.5f,
                Easing = Ease.InQuad,
                IgnoreTimeScale = true
            });
            yield return StartCoroutine(Wait(0.5f));
        }
        FailMenuHolder.SetActive(false);
        GameOverLabel.SetActive(true);
        AppearAnimation.transform.localScale = new Vector3(Camera.main.aspect/1.3f, 1f, 1f);
        AppearAnimation.gameObject.SetActive(true);
        yield return this.WaitForUnscaledSeconds(3f);

        GameOverLabel.GetComponentInChildren<tk2dSprite>();

        yield return this.WaitForUnscaledSeconds(0.5f);

        while (!Input.GetMouseButtonDown(0)) {
            yield return null;
        }
        OnClose();
    }

    private void OnClose() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (!Tutorial.IsShopTutorialCompleted) {
            LevelLoader.LoadLevel("MainMenu");
        } else {
            LevelLoader.LoadLevel("Shop");
        }
    }

    private void Revive() {
        StartCoroutine(ReviveProcess());
    }

    private IEnumerator ReviveProcess() {
        StopCoroutine("Timer");
        Game.ScoreManager.ResetScore();
        yield return StartCoroutine(PunchAnimation());
        DanceManAnimator.Main.Revive();
        FailMenuHolder.SetActive(false);
        LevelPlayer.Main.LevelLaunched = true;
        Game.Player.Ship.enabled = true;
        Game.Player.Ship.transform.position = Game.Player.StartShipPosition;
        Game.Player.Focus.enabled = true;
        Game.Player.Focus.ForceSpeedUp();
        _shipImage.SetActive(true);
        _shipImage.transform.TweenLocalPosition(new TweenSettings<Vector3>() {
            StartValue = Vector3.down*200,
            EndValue = Vector3.zero,
            Duration = 1f,
            Easing = Ease.OutBack,
        });

        NEvent.Dispatch(Game.GameObject, new BombEvent());
        Game.Player.Health.Replenish(2);
        Game.Player.Health.Revive = true;

        CancelInvoke("Explode");
        Game.Music.Play();
        gameObject.SetActive(false);
    }

    private IEnumerator PunchAnimation() {
        Game.Sound.PlaySound(SoundType.Story2GgPunch1);
        StopCoroutine("FightAnimation");
        LeftAlien.Play("AlienGotHit");
        LeftAlien.transform.TweenLocalPosition(new TweenSettings<Vector3>() {
            StartValue = _leftAlienPosition,
            EndValue = _leftAlienPosition + new Vector3(-100, 200),
            Duration = 0.5f,
            IgnoreTimeScale = true,
            OnComplete = () => LeftAlien.gameObject.SetActive(false)
        });
        RightAlien.Play("AlienGotHit");
        RightAlien.transform.TweenLocalPosition(new TweenSettings<Vector3>() {
            StartValue = _rightAlienPosition,
            EndValue = _rightAlienPosition + new Vector3(300, 100),
            Duration = 0.5f,
            IgnoreTimeScale = true,
            OnComplete = () => RightAlien.gameObject.SetActive(false)
        });
        DanceMan.Play("DanceManStand");
        yield return StartCoroutine(Wait(0.5f));
        BroadcastMessage("Hide");
        yield return StartCoroutine(Wait(1f));
    }
}