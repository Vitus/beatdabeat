﻿using System.Threading;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class MainMenu : MonoBehaviour {
    [SerializeField] private FlashSprite TitleAnimation;
    [SerializeField] private GameObject PlayButton;
    [SerializeField] private GameObject ButtonsHolder;
    [SerializeField] private GameObject StoryButton;
    [SerializeField] private GameObject ManualButton;
    [SerializeField] private GameObject ReplaysButton;
    [SerializeField] private GameObject Manual;
    [SerializeField] private GameObject Blocker;
    [SerializeField] private GameObject[] TitleExplosions;
    [SerializeField] private GameObject Flash;
    [SerializeField] private GameObject FullVersionButtons;
    [SerializeField] private GameObject FullVersion;
    private bool _menuThemeIsPlaying;
    private bool _skiped;

    public static Vector2 ScreenSize;

    private IEnumerator Start() {
        ReplaysButton.SetActive(false);
        ManualButton.SetActive(true);

        ScreenSize = new Vector2(Screen.width, Screen.height);
        Game.Music.Stop();
        GetComponent<AudioSource>().volume = Game.Music.MaxVolume;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.1f);
        StartCoroutine("TestSkip");
        StartCoroutine("AppearFlash");
        yield return StartCoroutine(WaitForMusic(4.35f));
        TitleAnimation.gameObject.SetActive(true);
        TitleAnimation.ChangeAnimation("btb_appear", true, true);
        StartCoroutine(EndAnimation());
        yield return StartCoroutine(ExplodeButton(0, 59));
        yield return StartCoroutine(ExplodeButton(1, 78));
        yield return StartCoroutine(ExplodeButton(2, 97));
        yield return StartCoroutine(ExplodeButton(3, 115));
        yield return StartCoroutine(ExplodeButton(4, 142));
        yield return StartCoroutine(ExplodeButton(5, 169));
        yield return StartCoroutine(ExplodeButton(6, 186));
        yield return StartCoroutine(ExplodeButton(7, 203));
        yield return StartCoroutine(ExplodeButton(8, 218));
    }

    private void Update() {
        if (Game.Music.Time == 0 && !Game.Music.IsPlaying && _menuThemeIsPlaying) {
            Game.Music.Play();
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }

    private IEnumerator AppearFlash() {
        yield return StartCoroutine(WaitForMusic(7.0f));
        Flash.SetActive(true);
    }

    private IEnumerator WaitForMusic(float time) {
        while (GetComponent<AudioSource>().time < time) {
            yield return null;
        }
    }

    private IEnumerator WaitForFrame(int frameNum) {
        while (TitleAnimation.CurrentFrame < frameNum) {
            yield return null;
        }
    }

    private IEnumerator ExplodeButton(int charNum, int frameNum) {
        yield return StartCoroutine(WaitForFrame(frameNum));
        Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
            StartValue = 20f,
            EndValue = 0,
            Duration = 0.1f,
            IgnoreTimeScale = true,
            AffectAxis = AffectAxis.X
        });
        TitleExplosions[charNum].SetActive(true);
        TitleExplosions[charNum].GetComponent<ParticleSystem>().Play();
        Game.Sound.PlayRandomSound(SoundType.MMLetterBoom1, SoundType.MMLetterBoom2);
    }

    private IEnumerator TestSkip() {
        while (!Input.GetMouseButtonDown(0)) {
            yield return null;
        }
        GetComponent<AudioSource>().Stop();
        foreach (var explosion in TitleExplosions) {
            explosion.SetActive(false);
        }
        _skiped = true;
        StartCoroutine(EndAnimation());
    }

    private IEnumerator EndAnimation() {
        if (!_skiped) {
            yield return StartCoroutine(WaitForMusic(7.95f));
        }
        Game.Music.Play("MenuTheme");
        Game.Music.Loop();
        _menuThemeIsPlaying = true;
        StopCoroutine("Start");
        StopCoroutine("TestSkip");
        StopCoroutine("AppearFlash");
        TitleAnimation.gameObject.SetActive(true);
        TitleAnimation.ChangeAnimation("btb_common");

        if (!Game.Level.FirstTimePlay) {
            StoryButton.SetActive(true);
            ButtonsHolder.SetActive(true);
        }
        PlayButton.SetActive(true);

        yield return new WaitForSeconds(2f);
        FullVersion.SetActive(false);
        yield return new WaitForSeconds(2f);
    }

    void OnExitClick() {
        Application.Quit();
    }

    private void OnPlayClick() {
        Game.Cheat.DisableCheats();
        Game.DifficultyManager.Speed = 1f;
        Game.Level.IsPractice = false;
        if (Game.Level.FirstTimePlay) {
            Game.DifficultyManager.Current = 2;
            Game.Level.StartLevel = 0;
            Game.Level.CurrentLevel = 0;
            Game.Level.IsTutorial = true;
            LevelLoader.LoadLevel("Story");
        } else {
            Game.Level.IsTutorial = false;
            MenuGUIPosition.Main.ShowDifficulty();
        }
    }

    private void OnCreditsClick() {
        MenuGUIPosition.Main.ShowCredits();
    }

    private void OnPracticeClick() {
        Game.Level.IsPractice = true;
        Game.DifficultyManager.Current = 1;
        MenuGUIPosition.Main.ShowPractice();
    }

    private void OnStoryClick() {
        Game.Level.StartLevel = -1;
        Game.Level.CurrentLevel = -1;
        LevelLoader.LoadLevel("Story");
    }

    private void OnOptionClick() {
        MenuGUIPosition.Main.ShowOptions();
    }

    private void OnFullScreenClick() {
        if (!Screen.fullScreen) {
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
        } else {
            Screen.SetResolution((int) ScreenSize.x, (int) ScreenSize.y, false);
        }
    }

    private void OnFullGameClick() {
        FullMenuPanel.Show();
    }

    private void OnManualClick() {
        Blocker.SetActive(true);
        Blocker.transform.SetZPosition(-11);
        StartCoroutine(ShowManual());
    }

    private IEnumerator ShowManual() {
        NelliFader.Instance.FadeIn();
        yield return new WaitForSeconds(NelliFader.Instance.DefaultFadeTime);
        Manual.SetActive(!Manual.activeSelf);
        NelliFader.Instance.FadeOut();
        Blocker.SetActive(Manual.activeSelf);
        Blocker.transform.SetZPosition(-9);
    }

    public void FullVersionClick() {
        FullVersionButtons.SetActive(true);
    }
}