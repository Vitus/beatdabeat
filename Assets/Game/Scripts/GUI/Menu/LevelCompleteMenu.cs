﻿using UnityEngine;
using System.Collections;

public class LevelCompleteMenu : MonoBehaviour 
{
    [SerializeField] private GameObject InfoObject;
    [SerializeField] private FlashSprite AppearAnimation;
    [SerializeField] private tk2dTextMesh DifficultyMesh;
    [SerializeField] private tk2dTextMesh TimeMesh;
    [SerializeField] private tk2dTextMesh CoinsMesh;
    [SerializeField] private tk2dTextMesh EnemiesMesh;
    [SerializeField] private tk2dTextMesh GrazedMesh;
    [SerializeField] private GameObject NoDamageSprite;
    [SerializeField] private GameObject NoBombSprite;
    [SerializeField] private GameObject CheatsSprite;
    [SerializeField] private GameObject Background;
    
    private float _timeScale;

    void OnEnable()
    {
        _timeScale = Time.timeScale;
        Game.Player.Focus.enabled = false;
        iTween.ValueTo(Background, iTween.Hash(I.Time, 0.5f, I.From, 0f, I.To, 0.5f, I.OnUpdate, "Alpha"));
    }

    private IEnumerator SlowDown()
    {
        while (Time.timeScale > 0.05)
        {
            Time.timeScale *= 0.99f;
            yield return null;
        }
        Time.timeScale = 0;
    }

    void OnDisable()
    {
        Time.timeScale = _timeScale;
    }

    IEnumerator Start()
    {
        Game.Sound.PlaySound(SoundType.GameLevelComplete);
        while (!AppearAnimation.Finished())
        {
            yield return null;
        }
        InfoObject.SetActive(true);
        DifficultyMesh.GetComponent<LocalizeTextMesh>().SetLocalization(Game.Language.GetLocalization(Game.DifficultyManager.NamesIDs[Game.Difficulty - 1]));
        float time = Game.ScoreManager.LevelTime;
        TimeMesh.text = ((int)(time / 60)).ToString("00") + ":" + ((int)(time % 60)).ToString("00");
        CoinsMesh.text = Game.ScoreManager.CoinCount.ToString();
        EnemiesMesh.text = Game.ScoreManager.EnemyDestroyCount.ToString();
        GrazedMesh.text = Game.ScoreManager.GrazeCount.ToString();

        NoDamageSprite.SetActive(Game.ScoreManager.DeathCount == 0);
        NoBombSprite.SetActive(Game.ScoreManager.BombCount == 0);
        CheatsSprite.SetActive(Game.Cheat.CheatsUsed());
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Game.Level.IsPractice)
            {
                Game.ScoreManager.SavePracticeScore();
                Game.Level.SetIsBeated(Game.Difficulty, Game.Level.CurrentLevel);
                LevelLoader.LoadLevel("MainMenu");
            }
            else
            {
                LevelLoader.LoadLevel("Final");
            }
        }
    }
}
