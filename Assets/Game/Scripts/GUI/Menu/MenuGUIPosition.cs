﻿using UnityEngine;
using System.Collections;

public class MenuGUIPosition : MonoBehaviour {
    private const float TweenTime = 1.0f;
    public static MenuGUIPosition Main { get; set; }

    [SerializeField] private Transform MainMenu;
    [SerializeField] private Transform Credits;
    [SerializeField] private Transform Options;
    [SerializeField] private Transform Difficulty;
    [SerializeField] private Transform Practice;
    [SerializeField] private AudioSource[] DiffSounds;
    [SerializeField] public GameObject[] Fireworks;
    private GameObject _camera;
    private Transform _currentTarget;
    private Transform _previousTarget;
    private float _currentVolume;
    private float _tweenTimeMultiplier;
    private int _difficultySoundNum = 1;

    public Transform CurrentTarget {
        get { return _currentTarget; }
    }

    private void Start() {
        ActivateFireworks();
        Main = this;
        _camera = Camera.main.gameObject;
        TweenTo(MainMenu, 3f);
        iTween.ValueTo(Game.Music.gameObject,
            iTween.Hash(I.Time, 0.5f, I.From, 0, I.To, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
    }

    private void OnEnable() {
        _currentVolume = 0.00000001f;
    }

    private void Update() {
        if (Application.platform == RuntimePlatform.Android) {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                Application.Quit();
            }
        }
    }

    private void ActivateFireworks() {
        for (int i = 0; i < 4; i++) {
            if (Game.Level.GetMaxUnlockedLevel(i + 1) == Game.Level.LevelScripts.Length) {
                Fireworks[i].SetActive(true);
            }
        }
    }

    public void EnableDiffSound(int diffNum) {
        _difficultySoundNum = diffNum;
        for (int i = 0; i < DiffSounds.Length; i++) {
            if (i == diffNum) {
                DiffSounds[i].volume = _currentVolume;
            } else {
                DiffSounds[i].volume = 0;
            }
        }
    }

    private void DiffSoundVolume(float volume) {
        _currentVolume = volume;
        for (int i = 0; i < DiffSounds.Length; i++) {
            if (i == _difficultySoundNum) {
                DiffSounds[i].volume = volume;
            }
        }
    }

    public void ShowMainMenu() {
        if (_currentTarget == Difficulty) {
            iTween.ValueTo(gameObject,
                iTween.Hash(I.Time, 0.5f, I.From, Game.Music.MaxVolume, I.To, 0.00000001f, I.OnUpdate, "DiffSoundVolume"));
            iTween.ValueTo(Game.Music.gameObject,
                iTween.Hash(I.Time, 0.5f, I.From, 0, I.To, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
        }
        TweenTo(MainMenu);
    }

    public void ShowCredits() {
        TweenTo(Credits);
    }

    public void ShowOptions() {
        TweenTo(Options);
    }

    public void ShowDifficulty() {
        iTween.ValueTo(gameObject,
            iTween.Hash(I.Time, 0.5f, I.From, 0.00000001f, I.To, Game.Music.MaxVolume, I.OnUpdate, "DiffSoundVolume"));
        iTween.ValueTo(Game.Music.gameObject,
            iTween.Hash(I.Time, 0.5f, I.From, Game.Music.MaxVolume, I.To, 0, I.OnUpdate, "ChangeVolume"));
        TweenTo(Difficulty);
    }

    public void ShowPractice() {
        TweenTo(Practice);
    }

    private void TweenTo(Transform targetMenu, float TweenTimeMultiplier = 1f) {
        StopAllCoroutines();

        _tweenTimeMultiplier = TweenTimeMultiplier;
        if (_previousTarget != null && _previousTarget != MainMenu) {
            _previousTarget.gameObject.SetActive(false);
        }
        StartCoroutine("AnimateCamera", targetMenu);
    }

    private IEnumerator AnimateCamera(Transform targetMenu) {
        _previousTarget = _currentTarget;
        _currentTarget = targetMenu;
        _currentTarget.gameObject.SetActive(true);
        iTween.MoveTo(_camera.gameObject,
            iTween.Hash(I.Name, "AnimateCamera", I.Time, TweenTime*_tweenTimeMultiplier, I.EaseType,
                iTween.EaseType.easeOutQuint, I.X,
                _currentTarget.transform.position.x, I.Y, _currentTarget.transform.position.y));
        yield return new WaitForSeconds(TweenTime);
        if (_previousTarget != null) {
            _previousTarget.gameObject.SetActive(false);
        }
    }
}