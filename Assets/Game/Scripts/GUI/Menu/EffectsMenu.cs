﻿using UnityEngine;
using System.Collections;

public class EffectsMenu : MonoBehaviour {

    public tk2dUIToggleButton VignetteToggle;
    public tk2dUIToggleButton MotionBlurToggle;
    public tk2dUIToggleButton DistortionToggle;
    public tk2dUIToggleButton BloomToggle;
    public tk2dUIToggleButton ColorBalanceToggle;
    public tk2dUIToggleButton RetroToggle;

    void Start() {
        VignetteToggle.IsOn = PostEffectDisabler.Instance.VignetteEnabled;
        MotionBlurToggle.IsOn = PostEffectDisabler.Instance.MotionBlurEnabled;
        DistortionToggle.IsOn = PostEffectDisabler.Instance.DistortionEnabled;
        BloomToggle.IsOn = PostEffectDisabler.Instance.BloomEnabled;
        ColorBalanceToggle.IsOn = PostEffectDisabler.Instance.ColorBalanceEnabled;
        RetroToggle.IsOn = PostEffectDisabler.Instance.RetroEnabled;
    }


    public void ToggleVignette() {
        PostEffectDisabler.Instance.VignetteEnabled = !PostEffectDisabler.Instance.VignetteEnabled;
    }
    public void ToggleMotionBlur() {
        PostEffectDisabler.Instance.MotionBlurEnabled = !PostEffectDisabler.Instance.MotionBlurEnabled;
    }
    public void ToggleDistortion() {
        PostEffectDisabler.Instance.DistortionEnabled = !PostEffectDisabler.Instance.DistortionEnabled;
    }
    public void ToggleBloom() {
        PostEffectDisabler.Instance.BloomEnabled = !PostEffectDisabler.Instance.BloomEnabled;
    }
    public void ToggleColorBalance() {
        PostEffectDisabler.Instance.ColorBalanceEnabled = !PostEffectDisabler.Instance.ColorBalanceEnabled;
    }
    public void ToggleRetro() {
        PostEffectDisabler.Instance.RetroEnabled = !PostEffectDisabler.Instance.RetroEnabled;
    }

}
