﻿using UnityEngine;
using System.Collections;

public class ScreenshotSwitcher : MonoBehaviour {

    [SerializeField] private tk2dSprite[] Screenshots;
    [SerializeField] private float UpdateTime;
    private int _currentScreenshotNum;

    IEnumerator Start() {
        _currentScreenshotNum = 0;
        while (true) {
            yield return StartCoroutine(ShowScreenshot());
            yield return this.WaitForUnscaledSeconds(UpdateTime);
        }

    }

    private IEnumerator ShowScreenshot() {
        Screenshots[_currentScreenshotNum].transform.SetZLocalPosition(-1);
        
        _currentScreenshotNum++;
        if (_currentScreenshotNum >= Screenshots.Length)
            _currentScreenshotNum -= Screenshots.Length;

        Screenshots[_currentScreenshotNum].transform.SetZLocalPosition(-2);
        Screenshots[_currentScreenshotNum].gameObject.SetActive(true);
        yield return this.WaitForUnscaledSeconds(0.5f);
        foreach (tk2dSprite screenshot in Screenshots)
        {
            if (screenshot != Screenshots[_currentScreenshotNum])
                screenshot.gameObject.SetActive(false);
        }
    }
}
