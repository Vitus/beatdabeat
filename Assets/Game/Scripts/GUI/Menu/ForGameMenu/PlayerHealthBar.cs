﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PlayerHealthBar : MonoBehaviour
{
    [SerializeField] private bool HorizontalLayout;
    [SerializeField] private tk2dTextMesh CostLabel;
    [SerializeField] private GameObject ChipIcon;
    [SerializeField] private GameObject HealthOnPrefab;
    [SerializeField] private GameObject HealthOffPrefab;
    [SerializeField] private tk2dTextMesh ReplenishTimer;
    [SerializeField] private GameObject ReplenishButton;
    [SerializeField] private float HeartSize = 20;

    private LevelFailedMenu _levelFailed;

    private bool Paused
        => _levelFailed != null && _levelFailed.gameObject.activeInHierarchy;

    private bool _wasPaused;


    private ShipHealth _shipHealth;

    private readonly List<GameObject> _hearts = new();


    private void OnEnable()
    {
        NEvent.AddListener<BPMEvent>(Game.GameObject, OnBpmChange);
    }

    private void OnDisable()
    {
        if (_shipHealth != null)
        {
            _shipHealth.HealthChanged -= OnHealthChanged;
        }

        NEvent.RemoveListener<BPMEvent>(Game.GameObject, OnBpmChange);
    }

    private void OnDestroy()
    {
        OnDisable();
    }


    private void Start()
    {
        ReplenishTimer.gameObject.SetActive(false);
        if (FindObjectOfType<GameMenu>() != null)
        {
            _levelFailed = FindObjectOfType<GameMenu>().LevelFailedMenu;
        }

        if (_shipHealth == null)
        {
            SetShipHealth(HealthManager.Instance.CurrentShipHealth);
        }

        OnHealthChanged();
        StartCoroutine(WatchForMax());
    }

    private IEnumerator WatchForMax()
    {
        var max = _shipHealth.Max;
        while (true)
        {
            if (_shipHealth.Max != max)
            {
                max = _shipHealth.Max;
                OnHealthChanged();
            }

            yield return null;
        }
    }

    private void Update()
    {
        _wasPaused = Paused;
    }

    public void SetShipHealth(ShipHealth shipHealth)
    {
        if (_shipHealth != null)
        {
            _shipHealth.HealthChanged -= OnHealthChanged;
        }

        _shipHealth = shipHealth;
        _shipHealth.HealthChanged += OnHealthChanged;
        OnHealthChanged();
    }


    private void OnHealthChanged()
    {
        if (Paused)
        {
            return;
        }

        ReplenishTimer.gameObject.SetActive(!_shipHealth.FullyRestored && false);
        if (ReplenishButton != null)
        {
            ReplenishButton.SetActive(!_shipHealth.FullyRestored);
            if (!_shipHealth.FullyRestored)
            {
                CostLabel.text = _shipHealth.FullReplenishCost.ToString();
                ChipIcon.transform.SetXLocalPosition(CostLabel.transform.localPosition.x -
                                                     CostLabel.GetEstimatedMeshBoundsForString(CostLabel.text).size.x -
                                                     10.5f);
            }
        }

        ClearHearts();
        GenerateHearts();
    }

    private void OnBpmChange(BPMEvent bpmEvent)
    {
        StopCoroutine(nameof(BeatUpdate));
        StartCoroutine(nameof(BeatUpdate), bpmEvent.Bpm);
    }

    private IEnumerator BeatUpdate(float bpm)
    {
        if (bpm < 1)
        {
            foreach (GameObject heart in _hearts)
            {
                heart.transform.localScale = new Vector3(1f, 1f, heart.transform.localScale.z);
            }

            yield break;
        }

        var currentTime = 0f;
        var timeForBeat = 60f / bpm;
        while (true)
        {
            currentTime += Time.deltaTime;
            while (currentTime > timeForBeat)
            {
                currentTime -= timeForBeat;
            }

            var scale = Mathf.Lerp(1f, 1.5f, 1 - currentTime / timeForBeat);

            foreach (GameObject heart in _hearts)
            {
                heart.transform.localScale = new Vector3(scale, scale, heart.transform.localScale.z);
            }

            yield return null;
        }
    }

    private void GenerateHearts()
    {
        for (var i = 0; i < _shipHealth.Max; i++)
        {
            var x = i % 2 == 0 ? -HeartSize / 2f : HeartSize / 2f;
            var y = -i / 2 * HeartSize;
            if (HorizontalLayout)
            {
                var t = x;
                x = -y;
                y = -t;
            }

            var heartPrefab = i < _shipHealth.Current ? HealthOnPrefab : HealthOffPrefab;
            InstantiateHeart(x, y, heartPrefab);
        }
    }

    private void InstantiateHeart(float x, float y, GameObject heartPrefab)
    {
        var heart = Instantiate(heartPrefab);
        heart.transform.parent = transform;
        heart.transform.localPosition = new Vector3(x, y);
        _hearts.Add(heart);
    }

    private void ClearHearts()
    {
        foreach (var heart in _hearts)
        {
            Destroy(heart);
        }

        _hearts.Clear();
    }

    public void OnReplenishClick()
    {
        if (Currency.Current >= _shipHealth.FullReplenishCost)
        {
            Currency.Current -= _shipHealth.FullReplenishCost;
            _shipHealth.FullReplenish();
        }
    }
}