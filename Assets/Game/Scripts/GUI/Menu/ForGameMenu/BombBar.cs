﻿using UnityEngine;
using System.Collections;

public class BombBar : MonoBehaviour {

    public string OnCellName;
    public string OffCellName;
    public tk2dSprite[] Cells;

    private void Update() {
        for (int i = 0; i < Cells.Length; i++) {
            string requiredSpriteName;
            if (Game.Player.Ship.BombCount >= i + 1) {
                requiredSpriteName = OnCellName;
            }
            else {
                requiredSpriteName = OffCellName;
            }
            if (Cells[i].CurrentSprite.name != requiredSpriteName) {
                Cells[i].SetSprite(requiredSpriteName);
            }
        }
    }
}