﻿using LevelParserV3;
using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {
    [SerializeField] private tk2dUIToggleButton SoundButton;

    private float _timeScale;

    private void OnEnable() {
        Game.Music.Pause();
        if (Time.timeScale < 0.01f) {
            _timeScale = -1;
            gameObject.SetActive(false);
            return;
        }
        _timeScale = Time.timeScale;

        Time.timeScale = 0;
        SoundButton.IsOn = Game.Sound.SoundOn;
    }

    private void OnDisable() {
        if (_timeScale >= 0) {
            if (LevelPlayer.Main.LevelLaunched) {
                Game.Music.Play();
            }
            Time.timeScale = _timeScale;
        }
    }

    private void OnContinueClick() {
        gameObject.SetActive(false);
    }

    private void OnRestartClick() {
        _timeScale = 1;
        LevelLoader.LoadLevel("Game");
        Game.ParticleBullets.DestroyAllBullets(false);
        Game.Music.Stop();
        Game.Level.CurrentLevel = Game.Level.StartLevel;
        Game.ParticleBullets._particleSystem.Clear();
    }

    private void OnFullScreenClick() {
        if (!Screen.fullScreen) {
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
        } else {
            Screen.SetResolution((int) MainMenu.ScreenSize.x, (int) MainMenu.ScreenSize.y, false);
        }
    }

    private void OnExitClick() {
        LevelLoader.LoadLevel("MainMenu");
    }

    private void OnToggleSound() {
        Game.Sound.SoundOn = SoundButton.IsOn;
    }
}