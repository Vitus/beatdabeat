﻿using System;
using UnityEngine;
using System.Collections;

public class DifficultyMenu : MonoBehaviour
{
    public static DifficultyMenu Main { get; private set; }
    public GameObject[] BeatedImages;

    void Awake() {
        Main = this;
        for (int i = 0; i < BeatedImages.Length; i++)
        {
            if (Game.Level.IsDifficultyBeated(i+1))
            {
                BeatedImages[i].SetActive(true);
            }
        }
    }

    void OnBackClick()
    {
        MenuGUIPosition.Main.ShowMainMenu();
    }
}
