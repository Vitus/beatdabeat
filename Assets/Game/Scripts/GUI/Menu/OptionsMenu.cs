﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private GameObject LooksLikeLabel;
    [SerializeField] private tk2dUIToggleButton SoundButton;
    [SerializeField] private MinMaxFloat SparkTime;
    [SerializeField] private ParticleSystem SparkSystem;

    void Awake()
    {
        SoundButton.IsOn = Game.Sound.SoundOn;
    }

    void OnEnable()
    {
        StartCoroutine(CreateSpark());
    }

    private IEnumerator CreateSpark()
    {
        while (true)
        {
            SparkSystem.Emit(10);
            Game.Sound.PlayRandomSound(SoundType.MMSpark1, SoundType.MMSpark2);
            yield return new WaitForSeconds(SparkTime.Random());
        }
    }

    void OnToogleSound()
    {
        Game.Sound.SoundOn = SoundButton.IsOn;
    }

    void OnBackClick()
    {
        MenuGUIPosition.Main.ShowMainMenu();
    }

    void OnMusicClick()
    {
        StopCoroutine("ShowLooksLikeLabel");
        iTween.Stop(LooksLikeLabel.gameObject);
        StartCoroutine("ShowLooksLikeLabel");
    }

    private IEnumerator ShowLooksLikeLabel()
    {
        LooksLikeLabel.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        LooksLikeLabel.gameObject.SetActive(false);
    }
}
