﻿using System;
using LevelParserV3;
using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour
{
    public PauseMenu PauseMenu;
    public tk2dTextMesh MoneyLabel;
    public tk2dTextMesh ScoreLabel;
    public tk2dTextMesh ScoreLabelShadow;
    public tk2dTextMesh ScoreMultiplierLabel;
    public tk2dTextMesh ScoreMultiplierLabelShadow;
    public FlashSprite MoneyIcon;
    public LevelFailedMenu LevelFailedMenu;
    public tk2dSprite ActiveSkillBar;
    public GameObject ActiveSkillLabel;
    public FlashSprite ActiveSkillBarContainer;
    public BombBar[] BombBars;

    private float _timeScale;

    private void OnEnable()
    {
        Game.Music.Volume = Game.Music.MaxVolume;
        NEvent.AddListener<GetCoinEvent>(Game.GameObject, Listener);

        foreach (BombBar bar in BombBars)
        {
            bar.gameObject.SetActive(false);
        }
        BombBars[Game.Upgrade.CurrentShip.BombsLevel].gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        NEvent.RemoveListener<GetCoinEvent>(Game.GameObject, Listener);
    }

    private void Listener(GetCoinEvent getCoinEvent)
    {
        MoneyIcon.ChangeAnimation(1, true, true);
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (LevelPlayer.Main.LevelLaunched && pauseStatus && !PauseMenu.gameObject.activeSelf && !LevelFailedMenu.gameObject.activeSelf)
            OnPauseClick();
    }

    private void OnPauseClick()
    {
        if (!Game.Level.IsTutorial && LevelPlayer.Main.LevelLaunched) {
           PauseMenu.gameObject.SetActive(!PauseMenu.gameObject.activeSelf);
        }
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            OnPauseClick();
        }

        if (HealthManager.Instance != null && HealthManager.Instance.CurrentShipHealth != null) {
            var shipHealth = HealthManager.Instance.CurrentShipHealth;
            if (shipHealth.Current == 0 && LevelFailedMenu.gameObject.activeSelf == false)
            {
                LevelFailedMenu.gameObject.SetActive(true);
            }
        }

        MoneyLabel.text = "$" + Game.Money.Current;
        if (MoneyIcon.Finished())
        {
            MoneyIcon.ChangeAnimation(0);
        }

        ScoreLabel.text = Mathf.RoundToInt(Game.ScoreManager.Score).ToString();
        ScoreLabelShadow.text = Mathf.RoundToInt(Game.ScoreManager.Score).ToString();
        ScoreMultiplierLabel.text = String.Format("x{0}", Game.ScoreManager.ScoreMultiplier);
        ScoreMultiplierLabelShadow.text = ScoreMultiplierLabel.text;

        ActiveSkill activeSkill = Game.Player.Ship.ActiveSkill;
        if (activeSkill.MaxEnergy == 0f)
        {
            ActiveSkillBar.transform.SetXLocalScale(0f);
            ActiveSkillLabel.SetActive(false);
            ActiveSkillBarContainer.SetPlay(false);
            ActiveSkillBarContainer.SetFrame(0);
        }
        else
        {
            ActiveSkillBar.transform.SetXLocalScale(Mathf.Clamp(activeSkill.Energy/activeSkill.MaxEnergy, 0f, 1f));
            ActiveSkillLabel.SetActive(activeSkill.Energy >= activeSkill.MaxEnergy);
            ActiveSkillBarContainer.SetPlay(true);
        }
    }
}