﻿using System;
using UnityEngine;
using System.Collections;

public class PrintText : MonoBehaviour {
    [SerializeField] private float Delay = 0f;
    [SerializeField] private float OneCharPrintTime = 0.025f;
    private tk2dTextMesh _textMesh;

    private void OnEnable() {
        _textMesh = GetComponent<tk2dTextMesh>();
        _textMesh.maxChars = 0;
        StartCoroutine(Print());
    }

    private IEnumerator Print() {
        float wait = Time.unscaledTime + Delay;
        while (Time.unscaledTime < wait) {
            yield return null;
        }

        while (_textMesh.maxChars < _textMesh.text.Length) {
            if (_textMesh.maxChars%2 == 0) {
                Game.Sound.PlaySound(SoundType.TutorialType);
            }
            _textMesh.maxChars++;
            wait = Time.unscaledTime + OneCharPrintTime;
            while (Time.unscaledTime < wait) {
                yield return null;
            }
        }
    }
}