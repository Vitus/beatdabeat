﻿using UnityEngine;
using System.Collections;

public class AttachToText : MonoBehaviour {

    private enum AlignEnum {
        Left = 0,
        Right = 1
    }

    [SerializeField] private tk2dTextMesh TextObject;
    [SerializeField] private AlignEnum Align;
    [SerializeField] private float Padding;
    private string oldText = null;

    private void Update() {
        if (oldText != TextObject.text) {
            float offset = TextObject.GetEstimatedMeshBoundsForString(TextObject.text).size.x / 2f + Padding;
            offset *= Align == AlignEnum.Left ? -1 : 1;
            transform.SetXPosition(TextObject.transform.position.x + offset);
            oldText = TextObject.text;
        }
    }
}