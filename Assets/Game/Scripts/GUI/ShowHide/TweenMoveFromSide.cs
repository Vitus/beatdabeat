﻿using NelliTweenEngine;
using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class TweenMoveFromSide : MonoBehaviour {
    [SerializeField] private float _duration;
    [SerializeField] private Vector2 _relativeStarPosition;
    [SerializeField] private bool _back;
    public float StartTime;

    private bool _started;

    private void OnEnable() {
        if (_started) {
            StartCoroutine(Show());
        }
    }

    private void Start() {
        _started = true;
        StartCoroutine(Show());
    }

    private IEnumerator Show() {
        gameObject.TweenAlpha(new TweenSettings<float>() { StartValue = 0, EndValue = 1, Duration = _duration, Delay = StartTime, Prepare = true});
       
        var rectTransfrom = GetComponent<RectTransform>();
        rectTransfrom.TweenRectPosition(new TweenSettings<Vector2>() {
            StartValue = rectTransfrom.anchoredPosition - _relativeStarPosition,
            EndValue = rectTransfrom.anchoredPosition,
            Duration = _duration,
            Delay = StartTime,
            Easing = _back ? (EasingFunction) Ease.OutBack : Ease.OutQuad,
            Prepare = true
        });
        yield break;
    }

    private IEnumerator Hide() {
        var rectTransfrom = GetComponent<RectTransform>();
        rectTransfrom.TweenRectPosition(new TweenSettings<Vector2>()
        {
            StartValue = rectTransfrom.anchoredPosition,
            EndValue = rectTransfrom.anchoredPosition - _relativeStarPosition,
            Duration = _duration,
            Easing = _back ? (EasingFunction)Ease.OutBack : Ease.OutQuad,
        });

        gameObject.TweenAlpha(new TweenSettings<float>() { StartValue = 0, EndValue = 1, Duration = _duration});
        yield break;
    }
}