﻿using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class TweenShake : MonoBehaviour
{
    [SerializeField] private Transform _target;

    [SerializeField] private float _startAmount;
    [SerializeField] private float _endAmount;
    [SerializeField] private float _duration;
    [SerializeField] private float _delay;
    [SerializeField] private bool _prepare;
    [SerializeField] private bool _ignoreTimeScale;
    [SerializeField] private bool _showOnce;
    [SerializeField] private bool _hide;

    private bool _started;
    private Tweener<float> _tween;

    private void Start()
    {
        if (_target == null)
        {
            _target = transform;
        }

        _tween = _target.TweenShakeLocalPosition(new TweenSettings<float>()
        {
            StartValue = _startAmount,
            EndValue = _endAmount,
            Duration = _duration,
            Delay = _delay,
            AutoPlay = false,
            IgnoreTimeScale = _ignoreTimeScale,
        });
        _started = true;
        Show();
    }

    private void OnEnable()
    {
        if (_started && !_showOnce)
        {
            Show();
        }
    }

    public void Show()
    {
        if (_tween != null)
        {
            _tween.Settings.Backward = false;
            _tween.Play();
        }
    }

    public void Hide()
    {
        if (!_hide)
        {
            return;
        }

        if (_tween != null)
        {
            _tween.Settings.Backward = true;
            _tween.Play();
        }
    }

}
