﻿using NelliTweenEngine;
using UnityEngine;
using System.Collections;

public class TweenAppearFromTop : MonoBehaviour {
    [SerializeField] private float _duration;
    [SerializeField] private Vector3 _starSize;
    [SerializeField] private bool _back;
    public float StartTime;

    private bool _started;

    private void OnEnable() {
        if (_started) {
            StartCoroutine(Show());
        }
    }

    private void Start() {
        _started = true;
        StartCoroutine(Show());
    }

    private IEnumerator Show() {
        gameObject.TweenAlpha(new TweenSettings<float>() { StartValue = 0, EndValue = 1, Duration = _duration, Delay = StartTime, Prepare = true});
        transform.TweenLocalScale(new TweenSettings<Vector3>()
        {
            StartValue = _starSize,
            EndValue = Vector3.one,
            Duration = _duration,
            Easing = _back ? (EasingFunction)Ease.OutBack : Ease.OutQuad,
            Delay = StartTime,
            Prepare = true,
        });
        yield break;
    }

    private IEnumerator Hide() {
        transform.TweenLocalScale(new TweenSettings<Vector3>() {
            StartValue = Vector3.one,
            EndValue = _starSize,
            Duration = _duration,
            Easing = _back ? (EasingFunction) Ease.OutBack : Ease.OutQuad
        });

        gameObject.TweenAlpha(new TweenSettings<float>() {StartValue = 1, EndValue = 0, Duration = _duration});
        yield break;
    }
}