﻿using System;
using NelliTweenEngine;
using UnityEngine;
using System.Collections;

public class TweenDefault : MonoBehaviour {

    [SerializeField] private SoundType _showSound = SoundType.NoSound;
    [SerializeField] private SoundType _hideSound = SoundType.NoSound;
    [SerializeField] private bool _showOnce;
    [SerializeField] private bool _hide = true;
    [SerializeField] private bool _prepare = true;
    [SerializeField] private float _delay;
    [SerializeField] private float _duration;
    [SerializeField] private bool _ignoreTimeScale = false;
    [SerializeField] private LoopType _loopType = LoopType.None;
    [SerializeField] private int _loopCount = 0;
    [SerializeField] private Transform _target;
    [SerializeField] private EaseType _ease = EaseType.OutQuad;
    [SerializeField] private bool _animateScale = true;
    [SerializeField] private Vector3 _startScale = Vector3.one;
    [SerializeField] private bool _animateRotation = true;
    [SerializeField] private Vector3 _startRotation = Vector3.zero;
    [SerializeField] private bool _animatePositon = true;
    [SerializeField] private Vector3 _startPosition = Vector3.zero;
    [SerializeField] private AffectAxis _affectAxis = AffectAxis.XY;
    [SerializeField] private bool _animateAlpha = true;
    [SerializeField] private EaseType _alphaEase = EaseType.Linear;
    [SerializeField] private float _startAlpha = 1;
    [SerializeField] private float _endAlpha = 1;

    private bool _started;

    private Tweener<float> _alphaTween;
    private Tweener<Vector3> _scaleTween;
    private Tweener<Vector3> _positionTween;
    private Tweener<Vector3> _rotationTween;

    private void Start() {
        if (_target == null) {
            _target = transform;
        }

        if (_animateAlpha) {
            _alphaTween = _target.gameObject.TweenAlpha(new TweenSettings<float>() {
                StartValue = _startAlpha,
                EndValue = _endAlpha,
                Easing = Ease.Enum(_alphaEase),
                Duration = _duration,
                Delay = _delay,
                Prepare = _prepare,
                AutoPlay = false,
                IgnoreTimeScale = _ignoreTimeScale,
                LoopType = _loopType,
                LoopCount = _loopCount,
            });
        }
        if (_animateScale) {
            _scaleTween = _target.TweenLocalScale(new TweenSettings<Vector3>() {
                StartValue = _startScale,
                EndValue = _target.localScale,
                Easing = Ease.Enum(_ease),
                Duration = _duration,
                Delay = _delay,
                Prepare = _prepare,
                AutoPlay = false,
                IgnoreTimeScale = _ignoreTimeScale,
                LoopType = _loopType,
                LoopCount = _loopCount,
            });
        }
        if (_animatePositon) {
            if (GetComponent<RectTransform>() == null) {
                _positionTween = _target.TweenLocalPosition(new TweenSettings<Vector3>() {
                    StartValue = _target.localPosition + _startPosition,
                    EndValue = _target.localPosition,
                    Easing = Ease.Enum(_ease),
                    Duration = _duration,
                    Delay = _delay,
                    Prepare = _prepare,
                    AffectAxis = _affectAxis,
                    AutoPlay = false,
                    IgnoreTimeScale = _ignoreTimeScale,
                    LoopType = _loopType,
                    LoopCount = _loopCount,
                });
            }
            else {
                RectTransform rectTransform = GetComponent<RectTransform>();
                _positionTween = _target.Tween(new TweenSettings<Vector3>()
                {
                    StartValue = rectTransform.anchoredPosition + (Vector2)_startPosition,
                    EndValue = rectTransform.anchoredPosition,
                    OnExecute = delegate(Vector3 value) {
                        Vector3 newValue = rectTransform.anchoredPosition;
                        if (_affectAxis.HasX())
                            newValue.x = value.x;
                        if (_affectAxis.HasY())
                            newValue.y = value.y;
                        if (_affectAxis.HasZ())
                            newValue.z = value.z;

                        rectTransform.anchoredPosition = newValue; 
                    },
                    Easing = Ease.Enum(_ease),
                    Duration = _duration,
                    Delay = _delay,
                    Prepare = _prepare,
                    AffectAxis = _affectAxis,
                    AutoPlay = false,
                    IgnoreTimeScale = _ignoreTimeScale,
                    LoopType = _loopType,
                    LoopCount = _loopCount,
                });
            }
        }
        if (_animateRotation) {
            _rotationTween = _target.TweenLocalRotation(new TweenSettings<Vector3>() {
                StartValue = _startRotation,
                EndValue = _target.localRotation.eulerAngles,
                Easing = Ease.Enum(_ease),
                Duration = _duration,
                Delay = _delay,
                Prepare = _prepare,
                AutoPlay = false,
                IgnoreTimeScale = _ignoreTimeScale,
                LoopType = _loopType,
                LoopCount = _loopCount,
            });
        }
//    }
//
//    private void Start() {
        _started = true;
        Show();
    }

    private void OnEnable()
    {
        if (_started && !_showOnce)
        {
            Show();
        }
    }

    public void Show() {
        if (_showSound != SoundType.NoSound) {
            StartCoroutine(PlaySound());
        }
        if (_alphaTween != null) {
            _alphaTween.Settings.Backward = false;
            _alphaTween.Play();
        }
        if (_scaleTween != null) {
            _scaleTween.Settings.Backward = false;
            _scaleTween.Play();
        }
        if (_positionTween != null) {
            _positionTween.Settings.Backward = false;
            _positionTween.Play();
        }
        if (_rotationTween != null) {
            _rotationTween.Settings.Backward = false;
            _rotationTween.Play();
        }
    }

    private IEnumerator PlaySound() {

        if (_ignoreTimeScale) {
            float startTime = Time.unscaledTime;
            while (Time.unscaledTime < startTime + _delay) {
                yield return null;
            }
        } else {
            yield return new WaitForSeconds(_delay);
        }
        Game.Sound.PlaySound(_showSound);
    }

    public void Hide()
    {
        if (!_hide)
        {
            return;
        }
        
        if (_hideSound != SoundType.NoSound)
        {
            Game.Sound.PlaySound(_hideSound);
        }
        if (_alphaTween != null)
        {
            _alphaTween.Settings.Backward = true;
            _alphaTween.Play();
        }
        if (_scaleTween != null)
        {
            _scaleTween.Settings.Backward = true;
            _scaleTween.Play();
        }
        if (_positionTween != null)
        {
            _positionTween.Settings.Backward = true;
            _positionTween.Play();
        }
        if (_rotationTween != null)
        {
            _rotationTween.Settings.Backward = true;
            _rotationTween.Play();
        }
    }

}