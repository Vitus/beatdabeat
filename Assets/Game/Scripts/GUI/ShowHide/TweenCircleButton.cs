﻿using System;
using NelliTweenEngine;
using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class TweenCircleButton : MonoBehaviour {

    [SerializeField] private float _duration;
    [SerializeField] private Vector3 _startScale;
    public float StartTime;

    private bool _started;

    private void OnEnable() {
        if (_started) {
            StartCoroutine(Show());
        }
    }

    private void Start() {
        _started = true;
        StartCoroutine(Show());
    }

    public IEnumerator Show(bool delayed = true) {
        gameObject.TweenAlpha(new TweenSettings<float>() { StartValue = 0, EndValue = 1, Duration = _duration, Delay = delayed ? StartTime : 0, Prepare = true});
        transform.TweenLocalScale(new TweenSettings<Vector3>()
        {
            StartValue = new Vector3(0.5f, 1f, 1f),
            EndValue = Vector3.one,
            Duration = _duration,
            Delay = delayed ? StartTime : 0,
            Easing = Ease.OutQuad,
            AutoPlay = false
        });
        yield break;
    }

    public IEnumerator Hide(Action completeEvent) {
        transform.TweenLocalScale(new TweenSettings<Vector3>() {
            StartValue = Vector3.one,
            EndValue = new Vector3(0.5f, 1f, 1f),
            Duration = _duration,
            Easing = Ease.InQuad,
        });
        gameObject.TweenAlpha(new TweenSettings<float>() {StartValue = 1, EndValue = 0, Duration = _duration});

        yield return new WaitForSeconds(_duration);
        if (completeEvent != null) {
            completeEvent();
        }
    }
}