﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

public class ShopPopup : MonoBehaviour {
    private const float ShowTimer = 5f;
    [SerializeField] private tk2dTextMesh FirstLine;
    [SerializeField] private tk2dTextMesh SecondLine;
    [SerializeField] private tk2dTextMesh ThirdLine;

    private class Lines {
        public string FirstLine;
        public string SecondLine;
        public string ThirdLine;
    }

    private static Queue<Lines> _linesQueue = new Queue<Lines>();

    public static void Show(string firstLine, string secondLine, string thirdLine) {
        Lines newLines = new Lines() {
            FirstLine = firstLine,
            SecondLine = secondLine,
            ThirdLine = thirdLine
        };
        _linesQueue.Enqueue(newLines);
        if (_linesQueue.Count > 3) {
            _linesQueue.Dequeue();
        }
        GoToNext();
    }

    private static void GoToNext() {
        if (_linesQueue.Count > 0 && FindObjectOfType<ShopPopup>() == null) {
            Lines lines = _linesQueue.Dequeue();
            ShopPopup popup = Instantiate(Resources.Load<ShopPopup>("ShopPopup"));
            popup.transform.SetParent(Camera.main.transform, false);
            popup.FirstLine.text = lines.FirstLine;
            popup.SecondLine.text = lines.SecondLine;
            popup.ThirdLine.text = lines.ThirdLine;
        }
    }

    void Start() {
       Destroy(gameObject, ShowTimer);
    }

    void OnDestroy() {
        GoToNext();
    }
}
