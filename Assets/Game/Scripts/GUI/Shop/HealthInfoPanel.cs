﻿using System;
using UnityEngine;
using System.Collections;

public class HealthInfoPanel : MonoBehaviour {

    [SerializeField] private tk2dTextMesh Label;
    [SerializeField] private tk2dTextMesh ReplenishCostLabel;
    [SerializeField] private tk2dUIItem ReplenishCostButton;

    private ShopShip _ship;
    private ShipHealth _health;

    public void SetShip(ShopShip ship) {
        _ship = ship;
        _health = HealthManager.Instance.GetShipHealth(_ship);
    }

    void Update() {
        if (_health.Current == _health.Max) {
            Label.text = String.Format("{0}/{1}", _health.Current, _health.Max);
            ReplenishCostButton.gameObject.SetActive(false);
        }
        else {
            Label.text = String.Format("{0}/{1}  {2}", _health.Current, _health.Max, _health.TimeLeftToReplenish);
            ReplenishCostButton.gameObject.SetActive(true);
            ReplenishCostLabel.text = (_health.FullReplenishCost).ToString();
        }
    }

    public void OnReplenishClick() {
        if (Currency.Current >= _health.FullReplenishCost) {
            Currency.Current -= _health.FullReplenishCost;
            _health.FullReplenish();
        }
    }
}
