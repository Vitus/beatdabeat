﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (tk2dTextMesh))]
public class CurrencyLabel : MonoBehaviour {
    [SerializeField] private float IconPadding;
    [SerializeField] private GameObject _currencyIcon;
    [SerializeField] private GameObject _InappButton;
    private tk2dTextMesh _label;

    private void Awake() {
        _label = GetComponent<tk2dTextMesh>();
        if (_InappButton != null) {
            _InappButton.SetActive(false);
        }
    }

    private void OnEnable() {
        Currency.OnValueChanged += UpdateValue;
        UpdateValue();
    }

    private void OnDisable() {
        Currency.OnValueChanged -= UpdateValue;
    }

    private void UpdateValue() {
        _label.text = Currency.Current.ToString();
        if (_currencyIcon != null) {
            _currencyIcon.transform.SetXLocalPosition(-_label.GetEstimatedMeshBoundsForString(_label.text).size.x -
                                                      IconPadding);
        }
    }
}