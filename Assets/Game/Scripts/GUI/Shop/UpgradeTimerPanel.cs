﻿using System;
using UnityEngine;
using System.Collections;

public class UpgradeTimerPanel : MonoBehaviour {
    [SerializeField] private tk2dTextMesh TimeLabel;
    [SerializeField] private tk2dTextMesh UpgradeTypeLabel;
    [SerializeField] private tk2dTextMesh SpeedUpCostLabel;
    [SerializeField] private GameObject ChipIcon;

    private UpgradeTimer _timer;

    public void SetUpgradeTimer(UpgradeTimer timer) {
        _timer = timer;
        UpgradeTypeLabel.text = _timer.UpgradeID;
    }

    private void Update() {
        TimeSpan timeLeft = (_timer.CompleteTime - DateTime.Now);
        TimeLabel.text = String.Format("{0:D2}:{1:D2}", timeLeft.Minutes, timeLeft.Seconds);
        SpeedUpCostLabel.text = UpgradeLevelManager.Instance.SpeedUpCost(_timer).ToString();

        ChipIcon.transform.SetXLocalPosition(SpeedUpCostLabel.transform.localPosition.x -
                                             SpeedUpCostLabel.GetEstimatedMeshBoundsForString(SpeedUpCostLabel.text)
                                                 .size.x - 10.5f);
    }

    public void OnSpeedUpClick() {
        UpgradeLevelManager.Instance.SpeedUpTimer(_timer);
    }
}