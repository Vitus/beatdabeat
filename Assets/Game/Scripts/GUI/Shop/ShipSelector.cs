﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NelliTweenEngine;

public class ShipSelector : MonoBehaviour {
    [SerializeField] private ShipButton ShipButtonPrefab;
    [SerializeField] private float ScreenWidth;
    [SerializeField] private GameObject NextButton;
    [SerializeField] private GameObject PrevButton;
    [SerializeField] private GameObject ShipHandler;
    [SerializeField] private GameObject DotPosition;
    [SerializeField] private float DotDistance;
    [SerializeField] private tk2dSprite DotPrefab;
    [SerializeField] private int SwipeFrameCount;
    [SerializeField] private int RequiredSwipeDistance;

    public List<ShipButton> Buttons = new List<ShipButton>();

    private int _currentShowShip;
    private tk2dSprite[] _dots;
    private Queue<float> _mouseXHistory = new Queue<float>();
    private bool _swiping;
    private Tweener<Vector3> _tween;

    private void Awake() {
        tk2dUIToggleButton[] buttons = new tk2dUIToggleButton[Game.Upgrade.Ships.Length];
        for (int i = 0; i < Game.Upgrade.Ships.Length; i++) {
            ShopShip shopShip = Game.Upgrade.Ships[i];
            ShipButton button =
                Instantiate(ShipButtonPrefab, transform.position + Vector3.right*ScreenWidth*i, Quaternion.identity);
            button.transform.parent = ShipHandler.transform;
            button.SetShip(shopShip);
            buttons[i] = button.GetComponent<tk2dUIToggleButton>();
            button.OnShopTab += UpdateButtons;
            button.OnDescriptionTab += delegate {
                if (Game.Upgrade.CurrentShip.IsUnlocked && shopShip == Game.Upgrade.CurrentShip) {
                    NextButton.SetActive(false);
                    PrevButton.SetActive(false);
                }
            };
            Buttons.Add(button);
        }

        GenerateDots();

        _currentShowShip = Game.Upgrade.CurrentShipIndex;
        ShowShip(0);
    }

    private void Update() {
        if (!Tutorial.Main.gameObject.activeSelf) {
            TestSwipe();
        }
    }

    private void TestSwipe() {
        if (Input.GetMouseButtonDown(0)) {
            _mouseXHistory.Clear();
            for (int i = 0; i < SwipeFrameCount; i++) {
                _mouseXHistory.Enqueue(Input.mousePosition.x);
            }
            _swiping = true;
        }

        if (Input.GetMouseButtonUp(0)) {
            _swiping = false;
        }

        if (_swiping && Input.GetMouseButton(0)) {
            float currentX = Input.mousePosition.x;
            _mouseXHistory.Enqueue(currentX);
            float prevX = _mouseXHistory.Dequeue();

            float deltaX = currentX - prevX;
            if (Mathf.Abs(deltaX) > RequiredSwipeDistance) {
                _swiping = false;
                tk2dUIManager.Instance.OverrideClearAllChildrenPresses(GetComponent<tk2dUIItem>());
                if (deltaX < 0) {
                    OnNextClick();
                } else {
                    OnPrevClick();
                }
            }
        }
    }

    private void GenerateDots() {
        _dots = new tk2dSprite[Game.Upgrade.Ships.Length];
        for (int i = 0; i < Game.Upgrade.Ships.Length; i++) {
            tk2dSprite dot =
                (tk2dSprite)
                    Instantiate(DotPrefab,
                        DotPosition.transform.position +
                        Vector3.right*(-(Game.Upgrade.Ships.Length - 1)/2f + i)*DotDistance, Quaternion.identity);
            dot.transform.parent = DotPosition.transform;
            dot.SetSprite("not_current_page");
            _dots[i] = dot;
        }
    }

    public void OnNextClick() {
        ShowShip(1);
    }

    public void OnPrevClick() {
        ShowShip(-1);
    }

    private void ShowShip(int selectDirection) {
        if (_tween != null) {
            return;
        }

        float prevPosition = -ScreenWidth*_currentShowShip;
        _dots[_currentShowShip].SetSprite("not_current_page");

        _currentShowShip += selectDirection;
        UpdateButtons();
        Game.Upgrade.CurrentShipIndex = _currentShowShip;

        float nextPosition = -ScreenWidth*_currentShowShip;
        _dots[_currentShowShip].SetSprite("current_page");

        _tween = ShipHandler.transform.TweenLocalPosition(new TweenSettings<Vector3>() {
            StartValue = Vector3.right*prevPosition,
            EndValue = Vector3.right*nextPosition,
            AffectAxis = AffectAxis.X,
            Duration = 0.3f,
            IgnoreTimeScale = true,
            Easing = Ease.OutQuad,
            OnComplete = delegate { _tween = null; }
        });
    }

    private void UpdateButtons() {
        if (_currentShowShip >= Game.Upgrade.Ships.Length - 1) {
            _currentShowShip = Game.Upgrade.Ships.Length - 1;
            NextButton.SetActive(false);
        } else {
            NextButton.SetActive(true);
        }

        if (_currentShowShip <= 0) {
            _currentShowShip = 0;
            PrevButton.SetActive(false);
        } else {
            PrevButton.SetActive(true);
        }
    }
}