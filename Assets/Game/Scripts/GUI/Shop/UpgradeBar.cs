﻿using System;
using UnityEngine;
using System.Collections;

public class UpgradeBar : MonoBehaviour {
    [SerializeField] private tk2dUIItem BuyButton;
    [SerializeField] private tk2dSprite BuyButtonGraphic;
    [SerializeField] private tk2dUIProgressBar ProgressBar;
    [SerializeField] private tk2dTextMesh[] Digits;
    [SerializeField] private Color EnabledColor;
    [SerializeField] private Color DisabledColor;

    public event Action OnLevelUpClick;

    public void OnUpgradeClick() {
        if (OnLevelUpClick != null) {
            OnLevelUpClick();
        }
    }

    public void UpdateBar(int currentLevel, int maxLevel, int upgradeCost) {
        if (currentLevel != maxLevel && maxLevel != 0) {
            ShowCost("$" + upgradeCost, currentLevel);
            BuyButton.gameObject.SetActive(true);
        }
        else {
            ShowCost("", currentLevel);
            BuyButton.gameObject.SetActive(false);
        }

        if (Game.Money.Current < upgradeCost || currentLevel == maxLevel) {
            BuyButtonGraphic.color = Color.gray;
        }
        else {
            BuyButtonGraphic.color = Color.white;
        }
        if (maxLevel != 0)
            ProgressBar.Value = ((float) currentLevel)/(maxLevel) - 0.001f;
        else {
            ProgressBar.Value = 0;
        }
    }

    private void HideCost() {
        foreach (tk2dTextMesh digit in Digits) {
            digit.gameObject.SetActive(false);
        }
    }

    private void ShowCost(string costString, int currentLevel) {
        for (int i = 0; i < costString.Length; i++) {
            Digits[i].text = costString[i].ToString();
            Digits[i].color = i+1 <= currentLevel ? DisabledColor : EnabledColor;
            Digits[i].gameObject.SetActive(true);
        }

        for (int i = costString.Length; i < 5; i++) {
            Digits[i].text = " ";
        }
    }
}