﻿using UnityEngine;
using System.Collections;

public class LockPosition : MonoBehaviour {

    public tk2dTextMesh text;

	void Start () {
	    var bounds = text.GetEstimatedMeshBoundsForString(text.text);
	    transform.localPosition = new Vector3(bounds.min.x - 20, transform.localPosition.y, transform.localPosition.z);
	}
}
