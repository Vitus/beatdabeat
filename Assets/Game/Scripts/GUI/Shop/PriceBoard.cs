﻿using UnityEngine;
using System.Collections;

public class PriceBoard : MonoBehaviour {

    public tk2dTextMesh PriceLabel;
    public tk2dTextMesh CurrencyLabel;
    private const float  MaxWidth = 45;

    void Start() {
        PriceLabel.text = "0";
        float width = PriceLabel.GetEstimatedMeshBoundsForString(PriceLabel.text).size.x;
        CurrencyLabel.text =  "USD";

        float shift = (MaxWidth - width)/2f;
        PriceLabel.transform.Translate(-shift, 0f, 0f);
        CurrencyLabel.transform.Translate(-shift, 0f, 0f);
    }

}
