﻿using UnityEngine;

public class UpgradePanel : MonoBehaviour {
    [SerializeField] public UpgradeBar HealthBar;
    [SerializeField] private UpgradeBar BombsBar;
    [SerializeField] private UpgradeBar WeaponsBar;

    [SerializeField] private GameObject Bars;
    [SerializeField] private UpgradeTimerPanel TimerPanel;

    private ShopShip _ship;

    private void OnEnable() {
        UpgradeLevelManager.Instance.UpgradeComplete += UpgradeComplete;
    }

    private void OnDisable() {
        UpgradeLevelManager.Instance.UpgradeComplete -= UpgradeComplete;
    }

    public void SetShip(ShopShip ship) {
        _ship = ship;
        HealthBar.OnLevelUpClick += delegate {
            SaveAnalytics(ShopShip.HealthUpgradeID);
            _ship.UpgradeHealth();
            Redraw();
        };
        BombsBar.OnLevelUpClick += delegate {
            SaveAnalytics(ShopShip.BombsUpgradeID);
            _ship.UpgradeBombs();
            Redraw();
        };
        WeaponsBar.OnLevelUpClick += delegate {
            SaveAnalytics(ShopShip.WeaponsUpgradeID);
            _ship.UpgradeWeapons();
            Redraw();
        };
        Redraw();
    }

    private void SaveAnalytics(string upgradeId) {
    }

    private void UpgradeComplete(UpgradeTimer timer) {
        Redraw();
    }

    public void Redraw() {
        if (UpgradeLevelManager.Instance.IsShipUpgrading(_ship)) {
            Bars.SetActive(false);
            TimerPanel.gameObject.SetActive(true);
            TimerPanel.SetUpgradeTimer(UpgradeLevelManager.Instance.GetTimer(_ship));
        } else {
            Bars.SetActive(true);
            TimerPanel.gameObject.SetActive(false);
            if (_ship.IsUnlocked) {
                HealthBar.UpdateBar(_ship.HealthLevel, _ship.MaxUpgradeLevel, _ship.UpgradeCost(_ship.HealthLevel + 1));
                BombsBar.UpdateBar(_ship.BombsLevel, _ship.MaxUpgradeLevel, _ship.UpgradeCost(_ship.BombsLevel + 1));
                WeaponsBar.UpdateBar(_ship.WeaponsLevel, _ship.MaxUpgradeLevel,
                    _ship.UpgradeCost(_ship.WeaponsLevel + 1));
            } else {
                HealthBar.UpdateBar(0, 0, 0);
                BombsBar.UpdateBar(0, 0, 0);
                WeaponsBar.UpdateBar(0, 0, 0);
            }

            if (_ship.ID == "Pacifist") {
                WeaponsBar.gameObject.SetActive(false);
            }
        }
    }
}