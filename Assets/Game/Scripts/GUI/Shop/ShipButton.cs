﻿using System;
using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

public class ShipButton : MonoBehaviour {
    [SerializeField] private tk2dTextMesh ShipName;
    [SerializeField] private tk2dTextMesh ShipName2;
    [SerializeField] private tk2dSprite Image;
    [SerializeField] private tk2dSprite Image2;

    [SerializeField] private tk2dTextMesh CostLabel;
    [SerializeField] private GameObject UnlockButton;
    [SerializeField] private GameObject DescriptionTab;
    [SerializeField] private GameObject BackButton;
    [SerializeField] private GameObject ShopTab;

    [SerializeField] private GameObject Locked;
    [SerializeField] private GameObject UnLocked;
    [SerializeField] private GameObject DemoLocked;

    [SerializeField] private LocalizeTextMesh UnlockCondition;
    [SerializeField] private tk2dSprite Lock;

    [SerializeField] public UpgradePanel UpgradePanel;

    [SerializeField] private tk2dTextMesh Description;
    [SerializeField] private GameObject ActiveSkillLabel;
    [SerializeField] private GameObject PassivekillLabel;
    [SerializeField] private tk2dSpriteAnimator SkillAnimation;
    [SerializeField] private tk2dSpriteAnimator ShootAnimation;

    [SerializeField] private PlayerHealthBar HealthPanel;

    [SerializeField] private GameObject DetailsHighlight;

    public event Action OnDescriptionTab;
    public event Action OnShopTab;

    private ShopShip _ship;

    private void OnEnable() {
        Game.Money.MoneyChanged += OnMoneyChanged;
        InvokeRepeating("ToggleDetailsHighlight", 1f, 1f);
    }

    private void OnDisable() {
        if (Game.Money != null) {
            Game.Money.MoneyChanged -= OnMoneyChanged;
        }
    }

    private void ToggleDetailsHighlight() {
        DetailsHighlight.gameObject.SetActive(!DetailsHighlight.gameObject.activeSelf);
    }

    private void OnMoneyChanged(int currentMoney) {
        UpdateImage();
    }

    public void SetShip(ShopShip ship) {
        _ship = ship;
        UpdateImage();
        UpgradePanel.SetShip(ship);
    }

    public void OnDescriptionTabClick() {
        DescriptionTab.SetActive(true);
        ShopTab.SetActive(false);
        if (OnDescriptionTab != null) {
            OnDescriptionTab();
        }
    }

    public void OnShopTabClick() {
        DescriptionTab.SetActive(false);
        ShopTab.SetActive(true);
        if (OnShopTab != null) {
            OnShopTab();
        }
    }

    public void UpdateImage() {
        ShipName.text = _ship.ShipName;
        ShipName2.text = _ship.ShipName;
        Image.SetSprite(_ship.IconName);
        Image2.SetSprite(_ship.IconName);

        HealthPanel.gameObject.SetActive(false);

        var descriptionLocaliztion = Description.GetComponent<LocalizeTextMesh>();
        descriptionLocaliztion.SetLocalization(_ship.Description);

        if (_ship.DifficultyConditionSatisfied)
        {
            Locked.SetActive(false);
            UnLocked.SetActive(true);
        }
        else
        {
            Locked.SetActive(true);
            UnLocked.SetActive(false);
        }

        UnlockCondition.SetLocalization(RequirementText());

        CostLabel.text = "$" + _ship.Cost;
        if (_ship.IsUnlocked) {
            UnlockButton.SetActive(false);
            CostLabel.gameObject.SetActive(false);
            Description.gameObject.SetActive(true);
            BackButton.SetActive(true);
            OnShopTabClick();
        } else {
            OnDescriptionTabClick();
            Description.gameObject.SetActive(false);
            BackButton.SetActive(false);
            UnlockButton.SetActive(true);
            CostLabel.gameObject.SetActive(true);
        }

        if (!string.IsNullOrEmpty(_ship.SkillAnimation)) {
            ActiveSkillLabel.SetActive(_ship.IsActiveSkill);
            PassivekillLabel.SetActive(!_ship.IsActiveSkill);
            SkillAnimation.Play(_ship.SkillAnimation);
        } else {
            SkillAnimation.transform.parent.gameObject.SetActive(false);
        }

        if (!string.IsNullOrEmpty(_ship.ShootAnimation)) {
            ShootAnimation.Play(_ship.ShootAnimation);
        } else {
            ShootAnimation.transform.parent.gameObject.SetActive(false);
        }
    }

    private LocalizedText RequirementText() {
        switch (_ship.DifficultyRequirement) {
            case -1:
                return Game.Language.GetLocalization(Game.Upgrade.AllDifficultiesLocalizationID);
            case 0:
                return null;
            case 1:
                return Game.Language.GetLocalization(Game.Upgrade.CasualDifficultyLocalizationID);
            case 2:
                return Game.Language.GetLocalization(Game.Upgrade.NormalDifficultyLocalizationID);
            case 3:
                return Game.Language.GetLocalization(Game.Upgrade.HardDifficultyLocalizationID);
            case 4:
                return Game.Language.GetLocalization(Game.Upgrade.BadassDifficultyLocalizationID);
        }
        return null;
    }

    public void OnUnlockClick() {
        if (Game.Money.Current >= _ship.Cost) {
            UnlockAnimation.NextScene = "Shop";
            UnlockAnimation.UnlockList.Enqueue(_ship);
            LevelLoader.LoadLevel("UnlockShip");
        }
    }

    public ShopShip GetShip() {
        return _ship;
    }
}