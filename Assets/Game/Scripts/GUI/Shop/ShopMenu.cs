﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class ShopMenu : MonoBehaviour {
    [SerializeField] private tk2dTextMesh MoneyLabel;
    [SerializeField] private FlashSprite NotebookAnimator;
    [SerializeField] private GameObject Screen;
    [SerializeField] private Rect ScreenRect;
    [SerializeField] private GameObject InAppButton;
	[SerializeField] private GameObject NoAdsButton;

    public static bool AdShown { get; set; }

    private void OnEnable() {
        Game.Money.MoneyChanged += OnMoneyChange;
	}

    private void OnDisable() {
		if (Game.Money != null) {
            Game.Money.MoneyChanged -= OnMoneyChange;
        }
    }

    private void Awake() {
        Game.Music.Play("ShopTheme");
		NoAdsButton.SetActive (false);
    }

    void HandleOnAdsDisable ()
    {
		NoAdsButton.SetActive (false);
    }

    private void Start() {
        InAppButton.SetActive(false);
        
        if (Tutorial.IsShopTutorialCompleted || Tutorial.IsTutorialSkiped)
        {
            if (!AdShown)
            {
                AdShown = true;
            }
        }

        OnMoneyChange(Game.Money.Current);
        iTween.ValueTo(gameObject, iTween.Hash(I.Time, 1f, I.From, 280, I.To, 240, I.OnUpdate, "ZoomCamera"));
        iTween.ValueTo(Game.Music.gameObject,
            iTween.Hash(I.Time, 0.5f, I.From, 0, I.To, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
    }

    private void ZoomCamera(float value) {
        Camera.main.orthographicSize = value;
    }

    private void Update() {
        if (!Game.Music.IsPlaying && Application.loadedLevelName == "Shop" && !Tutorial.Main.gameObject.activeSelf) {
            Game.Music.Play();
        }
        if (Screen.activeSelf == false && NotebookAnimator.Finished()) {
            OnNotebookAppeared();
        }
        if (ScreenRect.Contains(Camera.main.ScreenToWorldPoint(Input.mousePosition))) {
            MyCursor.OverShop = true;
        } else {
            MyCursor.OverShop = false;
        }
    }

    private void OnNotebookAppeared() {
        Screen.SetActive(true);
    }

    private void OnPlayClick() {
        ShopShip currentShip = Game.Upgrade.CurrentShip;
        if (!currentShip.IsUnlocked) {
            ShopPopup.Show(currentShip.ShipName, Game.Language.GetLocalization("shop_ship_is_locked"), "");
            return;
        }

        iTween.ValueTo(Game.Music.gameObject,
            iTween.Hash(I.Time, 0.25f, I.To, 0, I.From, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
        LevelLoader.LoadLevel("Game");
    }

    private void OnBackClick() {
        iTween.ValueTo(Game.Music.gameObject,
            iTween.Hash(I.Time, 0.25f, I.To, 0, I.From, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
        LevelLoader.LoadLevel("MainMenu");
    }

    private void OnMoneyChange(int currentMoney) {
        MoneyLabel.text = "$" + currentMoney;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(ScreenRect.center, new Vector3(ScreenRect.width, ScreenRect.height));
    }
}