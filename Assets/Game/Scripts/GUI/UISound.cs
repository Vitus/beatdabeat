using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Plays audioclips based on uiItem events
/// </summary>
public class UISound : tk2dUIBaseItemControl
{
    public SoundType HoverSound;
    public SoundType ClickSound;
    private bool _clicked;

    void OnEnable()
    {
        if (uiItem == null) {
            uiItem = GetComponent<tk2dUIItem>();
        }
        uiItem.OnHoverOver += PlayHoverOver;
        uiItem.OnClick += PlayClickSound;
    }

    void OnDisable()
    {
        uiItem.OnHoverOver -= PlayHoverOver;
        uiItem.OnClick -= PlayClickSound;
    }

    private void PlayHoverOver()
    {
        if (!_clicked)
        {
            Game.Sound.PlaySound(HoverSound);
        }
        _clicked = false;
    }

    private void PlayClickSound()
    {
        _clicked = true;
        Game.Sound.PlaySound(ClickSound);
    }
}
