﻿using System;
using UnityEngine;
using System.Collections;

public class LinkItem : tk2dUIBaseItemControl
{
    [SerializeField] private string Link;

    void OnEnable()
    {
        if (uiItem)
        {
            if (Link != null) { uiItem.OnClick += OpenLink; }
        }
    }

    private void OpenLink()
    {
        Application.OpenURL(Link);
    }
}
