﻿using UnityEngine;
using System.Collections;

public class Story : MonoBehaviour 
{

    [SerializeField] private GameObject Story1;
    [SerializeField] private GameObject Story2_1;
    [SerializeField] private GameObject Story2_2;
    [SerializeField] private GameObject Story3;
    
    void Start() {
        StartCoroutine("Animate");
    }

    IEnumerator Animate()
    {
        Story1.SetActive(true);
        Story1Sounds();
        yield return new WaitForSeconds(22.5f);
        Story1.SetActive(false);
        yield return new WaitForSeconds(0f);
        Story2_1.SetActive(true);
        yield return new WaitForSeconds(3.4f);
        Story2_1.SetActive(false);
        Game.Music.Play("FightTheme");
        Game.Music.Loop();
        iTween.ValueTo(Game.Music.gameObject, iTween.Hash(I.Time, 0.5f, I.From, 0, I.To, Game.Music.MaxVolume, I.OnUpdate, "ChangeVolume"));
        Story2_2.SetActive(true);
        yield return StartCoroutine(Fight());
        Story2_2.SetActive(false);
        Game.Music.Stop();
        CancelInvoke("RestartFightMusic");
        Story3.SetActive(true);
        Story3Sounds();
        yield return new WaitForSeconds(11.2f);
        OnSkipClick();
    }

    private void RestartFightMusic()
    {
        Game.Music.Stop();
        Game.Music.Play();
    }

    private void Story1Sounds()
    {
        FlashSprite Story1Sprite = Story1.GetComponent<FlashSprite>();
        StartCoroutine(ChangeSound(Story1Sprite, 0, "HeartTheme", false));
        StartCoroutine(PlaySound(Story1Sprite, 860, SoundType.Story1Flash));
        StartCoroutine(ChangeSound(Story1Sprite, 890, "EnemiesAppear", false));
    }

    private void Story3Sounds()
    {
        FlashSprite Story3Sprite = Story3.GetComponent<FlashSprite>();
        StartCoroutine(PlaySound(Story3Sprite, 0, SoundType.Story3Fall));
        StartCoroutine(PlaySound(Story3Sprite, 100, SoundType.Story3GgHitTheGround));
        StartCoroutine(PlaySound(Story3Sprite, 241, SoundType.Story3GgWakeup));
        StartCoroutine(PlaySound(Story3Sprite, 301, SoundType.Story3GgJump1));
        StartCoroutine(PlaySound(Story3Sprite, 331, SoundType.Story3GgJump2));
        StartCoroutine(PlaySound(Story3Sprite, 359, SoundType.Story3GgJump1));
        StartCoroutine(PlaySound(Story3Sprite, 389, SoundType.Story3GgJump2));
        StartCoroutine(PlaySound(Story3Sprite, 417, SoundType.Story3GgJump1));
        StartCoroutine(PlaySound(Story3Sprite, 447, SoundType.Story3GgJump2));
        StartCoroutine(PlaySound(Story3Sprite, 477, SoundType.Story3GgFinalJump));
        StartCoroutine(PlaySound(Story3Sprite, 509, SoundType.Story3EnteringTheShip));
        StartCoroutine(PlaySound(Story3Sprite, 556, SoundType.Story3GgEnterShip));
    }

    private IEnumerator ChangeSound(FlashSprite story1Sprite, int frameNum, string clip, bool loop)
    {
        while (story1Sprite.CurrentFrame < frameNum)
        {
            yield return null;
        }
        Game.Music.Play(clip);
        Game.Music.Volume = Game.Music.MaxVolume;
    }

    private IEnumerator PlaySound(FlashSprite story1Sprite, int frameNum, SoundType soundType)
    {
        while (story1Sprite.CurrentFrame < frameNum)
        {
            yield return null;
        }
        Game.Sound.PlaySound(soundType);
    }

    private IEnumerator Fight()
    {
        StoryGame game = Story2_2.GetComponent<StoryGame>();
        while (!game.IsFinished)
        {
            yield return null;
        }
        NelliFader.Instance.FadeOut(0.5f);
        yield return new WaitForSeconds(0.5f);
    }

    void OnSkipClick()
    {
        StopCoroutine("Animate");
        StopCoroutine("Fight");
        if (Game.Level.FirstTimePlay) {
            Game.Level.IsTutorial = true;
            LevelLoader.LoadLevel("Game");
        } else {
            LevelLoader.LoadLevel("MainMenu");
        }
    }
}