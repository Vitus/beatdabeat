﻿using UnityEngine;
using System.Collections;

public class StoryBubble : MonoBehaviour
{
    [SerializeField] private MinMaxFloat TimeBetweenBubbles;
    
    private FlashSprite _animation;

    IEnumerator Start()
    {
        _animation = GetComponent<FlashSprite>();
        while (true)
        {
            if (_animation.Finished())
            {
                yield return new WaitForSeconds(TimeBetweenBubbles.Random());
                _animation.SetVariant("Story_s2_bubble", Random.Range(0, 4));
                _animation.ChangeAnimation(0, true, true);
            }
            yield return null;
        }
    }
}
