﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class StoryGame : MonoBehaviour
{

    [SerializeField] private tk2dSpriteAnimator DanceMan;
    [SerializeField] private tk2dSpriteAnimator AlienBossPrefab;
    [SerializeField] private tk2dSpriteAnimator AlienPrefab;
    [SerializeField] private GameObject Girl;
    [SerializeField] private MinMaxFloat DistanceBetweenAliens;
    [SerializeField] private MinMaxFloat TimeBetweenAlienPunch;
    [SerializeField] private float AlienSpeed;
    [SerializeField] private int AliensCount;
    [SerializeField] private tk2dSpriteAnimator ClickToHit;
    [SerializeField] private tk2dSpriteAnimator TapToHit;
    [SerializeField] private GameObject StoryHitEffect;
    private List<tk2dSpriteAnimator> _aliens = new List<tk2dSpriteAnimator>();
    private float _lengthToNextAlien;
    private float _timeToNextPunch;
    private int _createdAliens;
    tk2dSpriteAnimator _boss;

    IEnumerator Start ()
	{
	    StartCoroutine("DanceManPunch");
	    StartCoroutine("Aliens");
	    yield return null;
	}

    IEnumerator Aliens()
    {
        while (true)
        {
            if (_aliens.Count == 0)
                _lengthToNextAlien = 0;
            else
                if (AliensFarFromPlayer())
                    _lengthToNextAlien -= AlienSpeed * Time.deltaTime;

            if (LengthToCreateAlien())
            {
                CreateAlien();
            }
            if (_aliens.Count > 0)
            {
                if (AliensFarFromPlayer())
                {
                    MoveAliens();
                }
                else
                {
                    PunchByAliens();
                }
            }
            yield return null;
        }
    }

    private void PunchByAliens()
    {
        ShowAttackHint();
        foreach (var alien in _aliens)
        {
            if (alien.CurrentClip.name != "AlienPunch")
            {
                alien.Play("AlienStand");
            }
        }

        _timeToNextPunch -= Time.deltaTime;
        if (_timeToNextPunch <= 0)
        {
            StartCoroutine("PerformAlienPunch");
        }
    }

    private void ShowAttackHint()
    {
        ClickToHit.gameObject.SetActive(true);
    }

    private IEnumerator PerformAlienPunch()
    {
        _timeToNextPunch = TimeBetweenAlienPunch.Random();
        _aliens[0].Play("AlienPunch");
        yield return new WaitForSeconds(0.1f);
        DanceMan.Play("DanceManGotHit");
        Game.Sound.PlaySound(SoundType.Story2GgHurt);
        yield return StartCoroutine(WaitAnimation(DanceMan));
        _aliens[0].Play("AlienStand");
        DanceMan.Play("DanceManStand");
    }

    private void MoveAliens()
    {
        foreach (var alien in _aliens)
        {
            alien.Play("AlienWalk");
            alien.transform.position += Vector3.left * AlienSpeed * Time.deltaTime;
        }
    }

    private bool AliensFarFromPlayer()
    {
        return _aliens[0].transform.position.x > -37;
    }

    private bool LengthToCreateAlien()
    {
        return _lengthToNextAlien <= 0;
    }

    private void CreateAlien()
    {
        if (_createdAliens < AliensCount)
        {
            _createdAliens++;
            _lengthToNextAlien = DistanceBetweenAliens.Random();
            tk2dSpriteAnimator alien = Instantiate(AlienPrefab) as tk2dSpriteAnimator;
            alien.Play("AlienWalk");
            alien.transform.position = new Vector3(340, -170, 0f);
            _aliens.Add(alien);
        }
    }

    IEnumerator DanceManPunch()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                DanceMan.Play("DanceManPunch");
                yield return StartCoroutine(WaitAnimation(DanceMan, -3));
                if (DanceMan.CurrentClip.name == "DanceManPunch")
                {
                    LaunchAlien();
                    yield return StartCoroutine(WaitAnimation(DanceMan));
                    DanceMan.Play("DanceManStand");
                }
            }
            yield return null;
        }
    }

    private void LaunchAlien()
    {
        if (_aliens.Count > 0 && !AliensFarFromPlayer())
        {
            StopCoroutine("PerformAlienPunch");
            _aliens[0].Play("AlienGotHit");
            switch (Random.Range(0, 3))
            {
                case 1:
                    Game.Sound.PlaySound(SoundType.Story2GgPunch1);
                    break;
                case 2:
                    Game.Sound.PlaySound(SoundType.Story2GgPunch2);
                    break;
                default:
                    Game.Sound.PlaySound(SoundType.Story2GgPunch3);
                    break;
            }
            Instantiate(StoryHitEffect);
            Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
                StartValue = 10,
                EndValue = 0,
                Duration = 0.5f,
                IgnoreTimeScale = true,
                AffectAxis = AffectAxis.X
            });
            Game.Sound.PlaySound(SoundType.Story2AlienFlyaway);
            iTween.MoveTo(_aliens[0].gameObject, new Vector3(400, Random.Range(0, 500), 0), 2f);
            Destroy(_aliens[0].gameObject, 0.5f);
            _aliens.RemoveAt(0);
            if (_createdAliens == AliensCount && _aliens.Count == 0)
            {
                StopCoroutine("Aliens");
                StartCoroutine("AlienBoss");
            }
        }
        else
        {
            if (_boss != null && _boss.CurrentClip.name == "AlienBossStand")
            {
                Game.Sound.PlaySound(SoundType.Story2GgPunchBoss);
            }
            else
            {
                Game.Sound.PlaySound(SoundType.Story2GgPunchWithoutHit);
            }
        }
    }

    void BossWalk()
    {
        Game.Sound.PlaySound(SoundType.Story2BossWalk);
    }

    IEnumerator AlienBoss()
    {
        _boss = Instantiate(AlienBossPrefab, new Vector3(370, -170, 0.5f), Quaternion.identity) as tk2dSpriteAnimator;
        _boss.Play("AlienBossWalk");
        iTween.MoveTo(_boss.gameObject, iTween.Hash(I.Position, new Vector3(-33, -170, 0.5f), I.Time, 4f, I.EaseType, iTween.EaseType.linear));
        InvokeRepeating("BossWalk", 0, 0.4f);
        yield return new WaitForSeconds(4f);
        CancelInvoke("BossWalk");
        _boss.Play("AlienBossStand");
        Game.Sound.PlaySound(SoundType.Story2BossLaugh);
        yield return new WaitForSeconds(3f);
        _boss.Play("AlienBossPunch");
        ClickToHit.gameObject.SetActive(false);
        Game.Sound.PlaySound(SoundType.Story2BossPunch);
        Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
            StartValue = 20,
            EndValue = 0,
            Duration = 1.5f,
            IgnoreTimeScale = true,
            AffectAxis = AffectAxis.X
        });
        yield return StartCoroutine(WaitAnimation(_boss, -5));
        StopCoroutine("DanceManPunch");
        iTween.MoveTo(DanceMan.gameObject, iTween.Hash(I.Position, DanceMan.transform.position + Vector3.up * 450, I.Time, 0.5f, I.EaseType, iTween.EaseType.linear));
        DanceMan.Play("DanceManKO");
        yield return StartCoroutine(WaitAnimation(_boss));
        _boss.Play("AlienBossWalk");
        iTween.MoveTo(_boss.gameObject, iTween.Hash(I.Position, new Vector3(-151, -170, 0.5f), I.Time, 1f, I.EaseType, iTween.EaseType.linear));
        yield return new WaitForSeconds(1f);
        _boss.Play("AlienBossKidnap");
        Game.Sound.PlaySound(SoundType.Story2BossGrabGirl);
        _boss.gameObject.transform.SetXLocalScale(-1f);
        Girl.SetActive(false);
        iTween.MoveTo(_boss.gameObject, iTween.Hash(I.Position, new Vector3(390, -170, 0.5f), I.Time, 4f, I.EaseType, iTween.EaseType.linear));
        InvokeRepeating("BossWalk", 0, 0.4f);
        yield return new WaitForSeconds(4f);
        CancelInvoke("BossWalk");
        IsFinished = true;
    }

    public IEnumerator WaitAnimation(tk2dSpriteAnimator animator, int frameOffset = 0)
    {
        while (animator.CurrentFrame != animator.CurrentClip.frames.Length - 1 + frameOffset)
        {
            yield return null;
        }
    }

    public bool IsFinished { get; set; }
}
