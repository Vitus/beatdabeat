﻿using UnityEngine;
using System.Collections;
using LevelParserV3;

public class TutorialReward : MonoBehaviour {
    [SerializeField] private FlashSprite mainAnimation;
    [SerializeField] private FlashSprite moneyAnimation;
    [SerializeField] private ParticleSystem coins;

    [SerializeField] private GameObject RememberImage;
    [SerializeField] private GameObject TakeThisImage;
    [SerializeField] private GameObject CmonImage;

    [SerializeField] private GameObject flash;

    private IEnumerator Start() {
        Game.Sound.PlaySound(SoundType.TutorialRewardBackground);
        mainAnimation.gameObject.SetActive(true);
        mainAnimation.ChangeAnimation(0, true, true);

        while (mainAnimation.CurrentFrame < 90) {
            yield return null;
        }
        RememberImage.SetActive(true);
        while (mainAnimation.CurrentFrame < 200) {
            yield return null;
        }
        RememberImage.SendMessage("Hide");

        Game.Sound.PlaySound(SoundType.TutorialRewardText);
        while (mainAnimation.CurrentFrame < 267) {
            yield return null;
        }
        TakeThisImage.SetActive(true);
        Game.Sound.PlaySound(SoundType.TutorialRewardText);
        while (mainAnimation.CurrentFrame < 340)
        {
            yield return null;
        }
        TakeThisImage.SendMessage("Hide");

        while (mainAnimation.CurrentFrame < 410) {
            yield return null;
        }
        Game.Sound.PlaySound(SoundType.TutorialRewardGiftBackground);

        while (!mainAnimation.Finished()) {
            yield return null;
        }

        mainAnimation.ChangeAnimation(1, false, true);
        int prevFrame = 0;
        Game.Sound.PlaySound(SoundType.TutorialRewardCmon);
        CmonImage.SetActive(true);
        while (!Input.GetMouseButtonDown(0)) {
            if (mainAnimation.CurrentFrame < 10 && prevFrame > 10) {
                Game.Sound.PlaySound(SoundType.TutorialRewardCmon);
            }
            prevFrame = mainAnimation.CurrentFrame;
            yield return null;
        }
        CmonImage.SetActive(false);

        flash.SetActive(true);
        coins.Play();
        Game.Sound.PlaySound(SoundType.TutorialRewardCoins);
        mainAnimation.ChangeAnimation(2, true, true);

        yield return new WaitForSeconds(1.5f);
        moneyAnimation.gameObject.SetActive(true);
        moneyAnimation.ChangeAnimation(0, true, true);
        mainAnimation.ChangeAnimation(2, true, true);
        mainAnimation.FPS = 0;

        Game.Sound.PlaySound(SoundType.TutorialRewardEarn);
        while (moneyAnimation.CurrentFrame < 14) {
            yield return null;
        }
        Game.Sound.PlaySound(SoundType.TutorialRewardEarn);
        while (moneyAnimation.CurrentFrame < 29) {
            yield return null;
        }
        Game.Sound.PlaySound(SoundType.TutorialRewardEarn);
        while (!moneyAnimation.Finished()) {
            yield return null;
        }
        Game.Money.Current += 1500;
        LevelPlayer.Main.TutorialRewardWasCollected = true;
        flash.SetActive(false);
        moneyAnimation.gameObject.SetActive(false);

        mainAnimation.FPS = mainAnimation.CurrentAnimation.FPS;

        while (!mainAnimation.Finished()) {
            yield return null;
        }
        mainAnimation.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}