﻿using UnityEngine;
using System.Collections;

public class ParalaxShipBackground : MonoBehaviour {
    [SerializeField] private float ParalaxMultiplier;
    [SerializeField] private float ShipPositionSensitivity;
    private float _startX;

    private void Start() {
        _startX = transform.position.x;
    }

    private void LateUpdate() {
        transform.SetXPosition((Camera.main.transform.position.x - ShipOffset()) * ParalaxMultiplier + _startX);
    }

    private float ShipOffset() {
        return (Game.Player.Ship.transform.position.x)*ShipPositionSensitivity;
    }

}