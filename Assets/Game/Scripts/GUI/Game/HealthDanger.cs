﻿using System;
using UnityEngine;
using System.Collections;

public class HealthDanger : MonoBehaviour {
    [SerializeField] private float BlinkTime;
    private bool _visible = false;

    private void OnEnable() {
        HealthManager.Instance.CurrentShipHealth.HealthChanged += OnHealthChanged;
    }

    private void OnDisable() {
        HealthManager.Instance.CurrentShipHealth.HealthChanged -= OnHealthChanged;
    }

    private void OnHealthChanged() {
        if (HealthManager.Instance.CurrentShipHealth.Current == 1) {
            ShowDanger();
        } else {
            HideDanger();
        }
    }

    private void Update() {
        if (_visible && Time.timeScale < 0.001f) {
            HideDanger();
        }
    }

    private void ShowDanger() {
        if (Time.timeScale < 0.001f) {
            HideDanger();
            return;
        }
        _visible = true;
        foreach (Transform child in transform) {
            child.gameObject.SetActive(true);
            iTween.Stop(child.gameObject);
            iTween.ValueTo(child.gameObject,
                iTween.Hash(I.Time, BlinkTime, I.LoopType, iTween.LoopType.pingPong, I.From, 1f, I.To, 0f, I.OnUpdate,
                    "Alpha"));
        }
        Invoke("HideDanger", 5f);
    }

    private void HideDanger() {
        _visible = false;
        foreach (Transform child in transform) {
            child.gameObject.SetActive(false);
        }
    }
}