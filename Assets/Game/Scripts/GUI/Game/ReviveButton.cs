﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReviveButton : MonoBehaviour {
    public event Action ReviveClicked = delegate { };

    [SerializeField] private tk2dFontData CoinFont;
    [SerializeField] private tk2dTextMesh CostLabel;

    [SerializeField] private tk2dSprite ChipIcon;
    [SerializeField] private tk2dSprite CoinIcon;

    [SerializeField] private List<int> ReviveCoinCost;

    public int ReviveCount { get; private set; }

    public List<int> ReviveCost 
        => ReviveCoinCost;

    public int CurrentCost 
        => ReviveCost[ReviveCount]*Game.Difficulty;

    public bool IsEnoughResources => CurrentCost <= Game.Money.Current;

    private void OnEnable()
    {
        CostLabel.text = CurrentCost.ToString();
        CostLabel.font = CoinFont;
        CoinIcon.gameObject.SetActive(true);
        ChipIcon.gameObject.SetActive(false);
    }

    private void OnContinue() {
        if (IsEnoughResources) {
            Game.Money.Current -= CurrentCost;
            ReviveCount++;
            ReviveClicked();
        }
    }
}