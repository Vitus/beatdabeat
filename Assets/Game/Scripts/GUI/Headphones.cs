﻿using UnityEngine;
using System.Collections;

public class Headphones : MonoBehaviour {

    public float WaitTime;

	IEnumerator Start () {
        Debug.Log("Init");
        yield return new WaitForSeconds(1f);
        const string projectId = "7b180df6-227d-481a-a2c5-4703ac3ff7d1";

        float endTime = Time.unscaledTime + WaitTime;
        while (endTime > Time.unscaledTime && !Input.GetMouseButtonDown(0)) {
	        yield return null;
	    }
	    LevelLoader.LoadLevel("MainMenu");
	}
}
