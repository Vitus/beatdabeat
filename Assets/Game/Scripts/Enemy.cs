﻿using System;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour, ICaster, ITRSObject
{

    public List<GameObject> Images;
    public GameObject BossMark;

    [SerializeField] private GameObject _image;
    private bool _isInFrontOfBullets;
    public float HitShakeAmount;
    public GameObject Image { get { return _image; } }

    public HealthType HealthType { get; private set; }

    void OnEnable()
    {
        NEvent.AddListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
        NEvent.AddListener<HitEvent>(gameObject, OnHit);
    }

    void OnDisable()
    {
        NEvent.RemoveListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
        NEvent.RemoveListener<HitEvent>(gameObject, OnHit);
    }

    private void OnOutOfHealth(OutOfHealthEvent outOfHealthEvent)
    {
        Game.Sound.PlaySound((SoundType) ((int)SoundType.GameExplosion0 + Random.Range(0,4)));
        Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
            StartValue = HitShakeAmount,
            EndValue = 0,
            Duration = 0.5f,
            IgnoreTimeScale = true,
            AffectAxis = AffectAxis.XY
        });
        NEvent.Dispatch(Game.GameObject, new EnemyDestroyEvent{Enemy = this});
    }

    private void OnHit(HitEvent e)
    {
        NEvent.Dispatch(Game.GameObject, EnemyHitEvent.E);
    }

    void LateUpdate()
    {
        if (_isInFrontOfBullets)
            transform.SetZPosition(Game.ParticleBullets.transform.position.z - 0.05f);
    }

    public Enemy SetImage(EnemyImage newName)
    {
        Destroy(_image);
        GameObject imagePrefab = Images.Find(o => o.name == newName.ToString());
        _image = (GameObject)Instantiate(imagePrefab);
        _image.transform.parent = transform;
        _image.transform.localPosition = Vector3.zero;
        GetComponent<BlinkOnHit>().BlinkablePart = _image;
        return this;
    }

    public Enemy SetImage(string newName)
    {
        Destroy(_image);
        GameObject imagePrefab = Images.Find(o => o.name == newName.ToString());
        _image = (GameObject)Instantiate(imagePrefab);
        _image.transform.parent = transform;
        _image.transform.localPosition = Vector3.zero;
        GetComponent<BlinkOnHit>().BlinkablePart = _image;
        return this;
    }

    public Enemy SetHealth(int health)
    {
        GetComponent<Health>().SetMax(health, true);
        return this;
    }

    public Enemy SetPosition(Vector3 position)
    {
        transform.position = position;
        return this;
    }    

    public Enemy SetPosition(float x, float y, float z = 0)
    {
        transform.position = new Vector3(x, y, z);
        return this;
    }

    public Enemy SetRotation(Quaternion rotation)
    {
        transform.rotation = rotation;
        return this;
    }

    public Enemy SetRotation(float rotation)
    {
        transform.rotation = Quaternion.Euler(0f, 0f, rotation);
        return this;
    }

    public Enemy Clone()
    {
        return (Enemy)Instantiate(this);
    }

    public Enemy SetScale(float scale)
    {
        transform.localScale = (Vector3)Vector2.one * scale + Vector3.forward;
        return this;
    }

    public Enemy SetScale(Vector2 scale)
    {
        transform.localScale = (Vector3)scale + Vector3.forward;
        return this;
    }

    public Enemy SetAnimation(string name, bool playOnce)
    {
        _image.GetComponent<FlashSprite>().ChangeAnimation(name, playOnce, true);
        return this;
    }

    public Enemy InFronOfBullets(bool isInFront)
    {
        _isInFrontOfBullets = isInFront;
        return this;
    }

    public Enemy SetKillAward(int award)
    {
        GetComponent<KillAward>().SetAward(award);
        return this;
    }

    public Enemy SetDamageAward(int award, float lifeTime)
    {
        GetComponent<DamageAward>().SetAward(award, lifeTime);
        return this;
    }

    public Enemy SetHealthType(HealthType healthType)
    {
        HealthType = healthType;
        GetComponent<DamageAward>().enabled = false;
        switch (healthType)
        {
            case HealthType.Normal: 
                GetComponent<Health>().enabled = true;
                GetComponentInChildren<Collider>().enabled = true;
                break;
            case HealthType.Invulnerable:
                GetComponent<Health>().enabled = false;
                transform.Find("HealthBar").gameObject.SetActive(false);
                GetComponentInChildren<Collider>().enabled = true;
                GetComponent<DamageAward>().enabled = true;
                break;
            case HealthType.OnBackground:
                GetComponent<Health>().enabled = false;
                transform.Find("HealthBar").gameObject.SetActive(false);
                GetComponentInChildren<Collider>().enabled = false;
                break; 
            case HealthType.Boss:
                GetComponent<Health>().enabled = true;
                GetComponent<Health>().AsBoss = true;
                gameObject.AddComponent<Boss>();
                GetComponentInChildren<Collider>().enabled = true;
                if (BossMark != null)
                {
                    GameObject bossMark = Instantiate(BossMark) as GameObject;
                    bossMark.transform.parent = transform;
                }
                break;
        }
        return this;
    }

    public Vector2 TransformPosition { get { return transform.position; } }

    public float TransformAngle { get { return transform.rotation.eulerAngles.z; } }

    public Vector2 TransformScale { get { return transform.localScale; } }

    public Vector2 TRSPosition {
        get { return transform.position; } 
        set { transform.position = value; } 
    }

    public float TRSAngle
    {
        get { return transform.rotation.eulerAngles.z; }
    }

    public Vector2 TRSScale
    {
        get { return transform.localScale; }
    }

    public bool IsAlive()
    {
        return this != null;
    }
}
