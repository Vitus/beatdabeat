﻿using LevelParserV2;
using UnityEngine;
using System.Collections;

public interface ICaster
{
    Vector2 TransformPosition { get; }
    float TransformAngle { get; }
    Vector2 TransformScale { get; }
}
