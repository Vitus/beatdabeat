﻿using UnityEngine;
using System.Collections;

public class BossMark : MonoBehaviour
{
    [SerializeField] private float YPosition;

    private Transform _parent;

	void LateUpdate ()
	{
        if (transform.parent != null)
        {
            _parent = transform.parent;
            transform.localScale = Vector3.one;
        }
	    transform.position = _parent.position;
	    transform.SetYPosition(YPosition);
	}
}
