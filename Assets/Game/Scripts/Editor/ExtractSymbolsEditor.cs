﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ExtractSymbols))]
public class ExtractSymbolsEditor : Editor {

    [MenuItem("My/CreateAsset")]
    public static void CreateAsset() {
        ExtractSymbols asset = new ExtractSymbols();
        AssetDatabase.CreateAsset(asset, "Assets/Extract.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if (GUILayout.Button("Extract")) {
            (target as ExtractSymbols).Extract();
        }
    }
}
