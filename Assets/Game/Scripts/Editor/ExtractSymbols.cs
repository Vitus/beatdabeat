﻿using System;
using UnityEngine;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

public class ExtractSymbols : ScriptableObject {

    [TextArea(5, 20)]
    public string InputTextUTF;

    public string cultureName;

    [TextArea(5, 20)]
    public string SymbolsInText;

    [TextArea(5, 20)]
    public string SymbolsUpper;

    [TextArea(5, 20)]
    public string SymbolsLower;

    [TextArea(5, 20)]
    public string SymbolsAll;

    public void Extract() {
        CultureInfo culture = new CultureInfo(cultureName);
        SymbolsInText = Regex.Replace(new string(InputTextUTF.OrderBy(c => c.ToString(), StringComparer.Create(culture, false)).Distinct().ToArray()), @"\s+", "");
        SymbolsLower = SymbolsInText.ToLower(culture);
        SymbolsUpper = SymbolsInText.ToUpper(culture);
//        SymbolsAll = new string((SymbolsLower + SymbolsUpper).OrderBy(c => c.ToString(), StringComparer.Create(culture, false)).Distinct().ToArray());
        SymbolsAll = new string((SymbolsUpper + SymbolsLower).Distinct().ToArray());

    }
}
