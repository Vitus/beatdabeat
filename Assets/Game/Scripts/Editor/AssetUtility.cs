﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

public static class AssetUtility {

    [MenuItem("Assets/Create/ShopShip")]
    public static void ShopShip() {
        CreateAsset<ShopShip>();
    }

    public static T CreateAsset<T>(string folderPath = null, string assetName = null, bool selectNewAsset = true) where T : ScriptableObject {
        T asset = ScriptableObject.CreateInstance<T>();

        var path = folderPath ?? GetActiveObjectFolder();

        string fileName = String.IsNullOrEmpty(assetName) ? "New " + typeof (T).ToString() : assetName;

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + fileName + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        if (selectNewAsset) {
            Selection.activeObject = asset;
        }
        return asset;
    }

    public static void CreatePath(string folder) {
        Directory.CreateDirectory(folder);
    }

    public static string GetActiveObjectFolder() {
        return GetAssetFolder(Selection.activeObject);
    }

    public static string GetAssetFolder(Object target) {
        string path = AssetDatabase.GetAssetPath(target);
        while (!String.IsNullOrEmpty(path) && !Directory.Exists(path)) {
            path = Path.GetDirectoryName(path);
        }
        if (String.IsNullOrEmpty(path)) {
            path = "Assets";
        }
        return path;
    }

    public static string AssetGUID(Texture2D texture)
    {
        return AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(texture));
    }

    public static void CreateTextureAsset(Texture2D texture, string path)
    {
        byte[] bytes = texture.EncodeToPNG();
        FileStream fs = File.Create(path);
        fs.Write(bytes, 0, bytes.Length);
        fs.Close();
    }
}