﻿using UnityEngine;
using System.Collections;

public class LevelBoundDrawer : MonoBehaviour {

	void OnDrawGizmos()
	{
	    Gizmos.color = Color.red;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(Camera.main.orthographicSize * Camera.main.aspect * 2, Camera.main.orthographicSize * 2));
	}
}
