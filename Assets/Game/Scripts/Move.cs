﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    [SerializeField] private Vector3 Direction;
    [SerializeField] private Vector3 Rotation;

	void Update () 
    {
	    transform.Translate(Direction * Time.deltaTime);
	    transform.Rotate(Rotation * Time.deltaTime);
	}
}
