﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AttachToBorder : MonoBehaviour {

    public TextAlignment Border;
    public Vector2 Offset;

    private void Awake() {
        float aspect = Game.AspectDelta;
        switch (Border) {
            case TextAlignment.Left:
                transform.SetXPosition(-320f*aspect + Offset.x);
                break;
            case TextAlignment.Center:
                transform.SetXPosition(Offset.x);
                break;
            case TextAlignment.Right:
                transform.SetXPosition(320f*aspect + Offset.x);
                break;
        }
    }

    private void Update() {
        if (!Application.isPlaying) {
            Awake();
        }
    }

}