﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public delegate void BulletUpdateHandler(ParticleBullet bullet);

public class ParticleBulletManager : MonoBehaviour
{
    private const float DeadBulletDisappearTime = 0.3f;
    private int maxBulletCount;
    [SerializeField] private int MaxBullets;
    
    protected ParticleBullet[] _bullets;

    [SerializeField] private ParticleSystem[] _emitters;
    private int _lastUsedParticle;
    [SerializeField] private Texture2D _bulletsTexture;
    private bool[,] _bulletPixels;

    public ParticleSystem _particleSystem;

    void Awake()
    {
        _bullets = new ParticleBullet[MaxBullets];
        PrepareHitPixelArray();
    }

    private void PrepareHitPixelArray()
    {
        _bulletPixels = new bool[_bulletsTexture.width, _bulletsTexture.height];

        for (int x = 0; x < _bulletsTexture.width; x++)
        {
            for (int y = 0; y < _bulletsTexture.height; y++)
            {
                _bulletPixels[x, y] = _bulletsTexture.GetPixel(x, y).a > 0.95;
            }
        }
    }

    void Update()
    {
        if (Camera.main == null)
        {
            return;
        }
        float yBorder = Camera.main.orthographicSize + 30;
        float xBorder = yBorder * Camera.main.aspect + 30;
        float screenSqrRadius = xBorder*xBorder + yBorder*yBorder;
        int currentBulletCount = 0;
        foreach (var emitter in _emitters) {
            emitter.Clear();
            emitter.gameObject.SetActive(false);
        }
        for (int i = 0; i < _bullets.Length; i++)
        {
            ParticleBullet bullet = _bullets[i];
            if (bullet != null && !bullet.IsNull)
            {
                bullet.UpdatePosition();
                float destroySqrRadius = bullet.DestroyRadius == 0f
                                            ? screenSqrRadius
                                            : bullet.DestroyRadius * bullet.DestroyRadius;
                if ((bullet.TRSPosition.sqrMagnitude) > destroySqrRadius)
                {
                    bullet.Kill();
                }
                if (!bullet.IsDead)
                {
                    Game.Player.Health.OnBulletUpdate(bullet);
                    BombFlash.Main.OnBulletUpdate(bullet);
                }
                if (bullet.IsDead && bullet.LifeTime > bullet.DeadTime + DeadBulletDisappearTime)
                {
                    bullet.IsNull = true;
                }
                if (!bullet.IsNull)
                {
                    currentBulletCount++;
                    ShowParticle(bullet);
                }
            }
        }
        if (maxBulletCount < currentBulletCount)
        {
            maxBulletCount = currentBulletCount;
        }
    }

    private void ShowParticle(ParticleBullet bullet)
    {
        ParticleSystem.Particle particle = new ParticleSystem.Particle();
        if (bullet.IsDead)
        {
            float deadState = Mathf.Clamp((bullet.LifeTime - bullet.DeadTime) / DeadBulletDisappearTime, 0f, 1f);
            particle.size = bullet.Size * TextureBulletSize(bullet.Type) * (1 - deadState);
            particle.color = Color.Lerp(bullet.Color, Color.white.WithAlpha(0f), deadState);
        }
        else
        {
            float appearState = Mathf.Clamp((bullet.AppearTime - bullet.LifeTime) / bullet.AppearTime, 0f, 1f);
            particle.size = bullet.Size * TextureBulletSize(bullet.Type) * (1 + appearState * (bullet.AppearSize - 1f));
            particle.color = Color.Lerp(bullet.Color, Color.white, appearState);
        }
        particle.position = new Vector3(bullet.TRSPosition.x, bullet.TRSPosition.y, transform.position.z);
        particle.rotation = -bullet.Angle;
        particle.startLifetime = 1;
        particle.remainingLifetime = 1;

        if (_emitters[(int) bullet.Type].gameObject.activeSelf == false) {
            _emitters[(int) bullet.Type].gameObject.SetActive(true);
        }
        _emitters[(int)bullet.Type].Emit(particle);
    }

    public float TextureBulletSize(ParticleBulletType type) {
        return _emitters[(int) type].startSize;
    }

    public bool TestHit(ParticleBullet bullet, Vector2 position) {
        Vector2 texturePosition = (Vector2) (bullet.GetMatrix().MultiplyPoint3x4(position)) +
                                    new Vector2(32f, 32f);
        texturePosition.x = Mathf.Clamp(texturePosition.x, 0f, 63f);
        texturePosition.y = Mathf.Clamp(texturePosition.y, 0f, 63f);
        int typeNum = (int) bullet.Type;
        int xTile = typeNum%8;
        int yTile = 1 - typeNum/8;
        texturePosition.x += xTile*64f;
        texturePosition.y += yTile*64f;
        try
        {
            return _bulletPixels[(int)texturePosition.x, (int)texturePosition.y];
        }
        catch (Exception)
        {
            Debug.Log(bullet.Position);
            Debug.Log(bullet.Angle);
            Debug.Log(position);
            Debug.Log((int)texturePosition.x + "," + (int)texturePosition.y);
            throw;
        }
    }

    public ParticleBullet SpawnUnusedParticle(string name = "")
    {
        for (int i = _lastUsedParticle; i < MaxBullets; i++)
        {
            if (_bullets[i] == null)
            {
                _bullets[i] = new ParticleBullet();
            }
            if (_bullets[i].IsNull)
            {
                _lastUsedParticle = i;
                return _bullets[i].Spawn(name);
            }
        }
        for (int i = 0; i < _lastUsedParticle; i++)
        {
            if (_bullets[i] == null)
            {
                _bullets[i] = new ParticleBullet();
            }
            if (_bullets[i].IsNull)
            {
                _lastUsedParticle = i;
                return _bullets[i].Spawn(name);
            }
        }
        return _bullets[0].Spawn(name);
    }

    public ParticleBullet[] GetBullets()
    {
        return _bullets;
    }

    void OnLevelWasLoaded()
    {
        DestroyAllBullets(false);
        _particleSystem.Clear();
    }

    public void DestroyAllBullets(bool kill = true)
    {
        foreach (ParticleBullet bullet in _bullets)
        {
            if (bullet != null)
            {
                if (kill && !bullet.IsDead && !bullet.IsNull)
                    bullet.Kill();
                if (!kill)
                    bullet.IsNull = true;
            }
        }
    }
}
