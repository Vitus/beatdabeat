﻿using UnityEngine;
using System.Collections;

public class SaveManager : MonoBehaviour
{
    public float SaveInterval = 60f;

    void Awake()
    {
        Invoke("Save", SaveInterval);
    }

    void OnLevelWasLoaded()
    {
        Save();
    }

    public void Save()
    {
        PlayerPrefs.Save();
    }
}
