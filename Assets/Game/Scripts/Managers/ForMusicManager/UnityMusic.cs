﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class UnityMusic : IMusic
{
    private AudioSource _source;
    private float[] _samples;
    private float[] _data;

    public UnityMusic(AudioSource source)
    {
        _samples = new float[Game.Music.Count * 4];
        _data = new float[Game.Music.Count * 2];
        _source = source;
    }

    public string Clip
    {
        get
        {
            if (_source.clip != null)
                return _source.clip.name;
            return "";
        }
        set {
            Game.Music.StartCoroutine(SetClip(value));
        }
    }

    private IEnumerator SetClip(string Name)
    {
        _source.clip = null;
        var request = Resources.LoadAsync<AudioClip>("LevelMusic/" + Name);
        yield return request;
        _source.clip = request.asset as AudioClip;
    }

    public float Time
    {
        get
        {
            return _source.time;
        }
        set
        {
            _source.time = value;
        }
    }

    public float ClipLength
    {
        get
        {
            return _source.clip.length;
        }
    }

    public float Volume
    {
        get
        {
            return _source.volume;
        }
        set
        {
            _source.volume = value;
        }
    }

    public float Pitch
    {
        get
        {
            return _source.pitch;
        }
        set
        {
            _source.pitch = value;
        }
    }

    public bool IsPlaying
    {
        get
        {
            return _source.isPlaying;
        }
    }

    public void Play()
    {
        _source.Play();
    }

    public void Pause()
    {
        _source.Pause();
    }

    public void Stop()
    {
        _source.Stop();
    }

    public IList<float> GetData(float soundPosition)
    {
#if UNITY_EDITOR || !UNITY_FLASH
        if (_source.clip == null)
        {
            _data.SetToZero();
        }
        else
        {
            int samplesOffset = (int)(soundPosition * _source.clip.frequency);
            _source.clip.GetData(_samples, samplesOffset);
            for (int i = 0; i < _data.Length; i++)
            {
                _data[i] = _samples[i*2];
            }
        }
        return _data;
#else
        return new float[Game.Music.Count];
#endif
    }
}
