﻿using UnityEngine;

public class FFT
{
    // Element for linked list in which we store the
    // input/output data. We use a linked list because
    // for sequential access it's faster than array index.
    class FFTElement
    {
        public float Re = 0.0f;     // Real component
        public float Im = 0.0f;     // Imaginary component
        public FFTElement NextElement;     // Next element in linked list
        public int ReversalTarget;         // Target position post bit-reversal
    }

    private int _power = 0;        // log2 of FFT size
    private int _count = 0;           // FFT size
    private FFTElement[] _elements;       // Vector of linked list elements

    public FFT(int count)
    {
        _power = Mathf.RoundToInt(Mathf.Log(count, 2));
        _count = count;

        // Allocate elements for linked list of complex numbers.
        _elements = new FFTElement[_count];
        for (int i = 0; i < _count; i++)
            _elements[i] = new FFTElement();

        // Set up "next" pointers.
        for (int i = 0; i < _count - 1; i++)
            _elements[i].NextElement = _elements[i + 1];

        // Specify target for bit reversal re-ordering.
        for (int i = 0; i < _count; i++)
            _elements[i].ReversalTarget = BitReverse(i, _power);
    }

    public void Run(ref float[] reRef, ref float[] imRef, bool inverse = false)
    {
        int numFlies = _count >> 1; // Number of butterflies per sub-FFT
        int span = _count >> 1;     // Width of the butterfly
        int spacing = _count;         // Distance between start of sub-FFTs
        int wIndexStep = 1;        // Increment for twiddle table index

        // Copy data into linked complex number objects
        // If it's an IFFT, we divide by N while we're at it
        FFTElement x = _elements[0];
        int k = 0;
        float scale = inverse ? 1.0f / _count : 1.0f;
        while (x != null)
        {
            x.Re = scale * reRef[k];
            x.Im = scale * imRef[k];
            x = x.NextElement;
            k++;
        }

        // For each stage of the FFT
        for (int stage = 0; stage < _power; stage++)
        {
            // Compute a multiplier factor for the "twiddle factors".
            // The twiddle factors are complex unit vectors spaced at
            // regular angular intervals. The angle by which the twiddle
            // factor advances depends on the FFT stage. In many FFT
            // implementations the twiddle factors are cached, but because
            // array lookup is relatively slow in C#, it's just
            // as fast to compute them on the fly.
            float wAngleInc = wIndexStep * 2.0f * Mathf.PI / _count;
            if (inverse == false)
                wAngleInc *= -1;
            float wMulRe = Mathf.Cos(wAngleInc);
            float wMulIm = Mathf.Sin(wAngleInc);

            for (int start = 0; start < _count; start += spacing)
            {
                FFTElement xTop = _elements[start];
                FFTElement xBot = _elements[start + span];

                float wRe = 1.0f;
                float wIm = 0.0f;

                // For each butterfly in this stage
                for (int flyCount = 0; flyCount < numFlies; ++flyCount)
                {
                    // Get the top & bottom values
                    float xTopRe = xTop.Re;
                    float xTopIm = xTop.Im;
                    float xBotRe = xBot.Re;
                    float xBotIm = xBot.Im;

                    // Top branch of butterfly has addition
                    xTop.Re = xTopRe + xBotRe;
                    xTop.Im = xTopIm + xBotIm;

                    // Bottom branch of butterly has subtraction,
                    // followed by multiplication by twiddle factor
                    xBotRe = xTopRe - xBotRe;
                    xBotIm = xTopIm - xBotIm;
                    xBot.Re = xBotRe * wRe - xBotIm * wIm;
                    xBot.Im = xBotRe * wIm + xBotIm * wRe;

                    // Advance butterfly to next top & bottom positions
                    xTop = xTop.NextElement;
                    xBot = xBot.NextElement;

                    // Update the twiddle factor, via complex multiply
                    // by unit vector with the appropriate angle
                    // (wRe + j wIm) = (wRe + j wIm) x (wMulRe + j wMulIm)
                    float tRe = wRe;
                    wRe = wRe * wMulRe - wIm * wMulIm;
                    wIm = tRe * wMulIm + wIm * wMulRe;
                }
            }

            numFlies >>= 1;   // Divide by 2 by right shift
            span >>= 1;
            spacing >>= 1;
            wIndexStep <<= 1;     // Multiply by 2 by left shift
        }

        // The algorithm leaves the result in a scrambled order.
        // Unscramble while copying values from the complex
        // linked list elements back to the input/output vectors.
        x = _elements[0];
        while (x != null)
        {
            int target = x.ReversalTarget;
            reRef[target] = x.Re;
            imRef[target] = x.Im;
            x = x.NextElement;
        }
    }

    /**
     * Do bit reversal of specified number of places of an int
     * For example, 1101 bit-reversed is 1011
     *
     * @param   x       Number to be bit-reverse.
     * @param   numBits Number of bits in the number.
     */
    private int BitReverse(int x, int numBits)
    {
        int y = 0;
        for (int i = 0; i < numBits; i++)
        {
            y <<= 1;
            y |= x & 0x0001;
            x >>= 1;
        }
        return y;
    }
}