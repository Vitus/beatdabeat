﻿using UnityEngine;
using System.Collections;

public class UnityMusicList : MonoBehaviour
{
    public AudioClip LoadedMusic { get; private set; }

    public IEnumerator GetByName(string Name) {
        LoadedMusic = null;
        var request = Resources.LoadAsync<AudioClip>("LevelMusic/" + Name);
        yield return request;
        LoadedMusic = request.asset as AudioClip;
    }
}
