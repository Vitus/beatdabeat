﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public interface IMusic
{
    string Clip { get; set; }
    float Time { get; set; }
    float Pitch { get; set; }
    bool IsPlaying { get; }
    float ClipLength { get; }
    float Volume { get; set; }

    void Play();
    void Pause();
    void Stop();

    IList<float> GetData(float soundPosition);
}
