﻿using NelliTweenEngine;
using UnityEngine;
using System.Collections;

public class NextLevelBanner : MonoBehaviour
{
    private FlashSprite sprite;

    [SerializeField] private int[] Authors;
    [SerializeField] private int[] Songs;
    [SerializeField] private tk2dTextMesh StageNumLabel;
    [SerializeField] private tk2dTextMesh StageLabel;

	IEnumerator Start ()
	{
	    sprite = GetComponent<FlashSprite>();

        sprite.SetVariant("sma_avatar", Authors[Game.Level.CurrentLevel]);
        sprite.SetVariant("sma_name", Authors[Game.Level.CurrentLevel]);
        sprite.SetVariant("sma_song", Songs[Game.Level.CurrentLevel]);

	    StageNumLabel.text = (Game.Level.CurrentLevel + 1).ToString();

	    ShowStage();

        sprite.FPS = 60;
	    sprite.CurrentFrame = 0;
	    yield return new WaitForSeconds(3f);
	    sprite.FPS = -60;

	    HideStage();

        yield return new WaitForSeconds(1.7f);
        Destroy(gameObject);
	}

    private void HideStage() {
        StageNumLabel.transform.TweenLocalPosition(new TweenSettings<Vector3>()
        {
            StartValue = StageNumLabel.transform.localPosition,
            EndValue = StageNumLabel.transform.localPosition + Vector3.left * 50f,
            Duration = 0.5f,
            Easing = Ease.OutQuad,
            Prepare = true,
        });
        StageNumLabel.TweenAlpha(new TweenSettings<float>()
        {
            StartValue = 1f,
            EndValue = 0f,
            Duration = 0.5f,
            Prepare = true,
        });
        StageLabel.TweenAlpha(new TweenSettings<float>()
        {
            StartValue = 1f,
            EndValue = 0f,
            Duration = 0.5f,
            Prepare = true,
        }); 
    }

    private void ShowStage() {
        StageNumLabel.transform.TweenLocalPosition(new TweenSettings<Vector3>()
        {
            StartValue = StageNumLabel.transform.localPosition + Vector3.left * 50f,
            EndValue = StageNumLabel.transform.localPosition + Vector3.zero,
            Duration = 0.5f,
            Delay = 0.5f,
            Easing = Ease.OutQuad,
            Prepare = true,
        });
        StageNumLabel.TweenAlpha(new TweenSettings<float>()
        {
            StartValue = 0f,
            EndValue = 1f,
            Duration = 0.5f,
            Delay = 0.5f,
            Prepare = true,
        });
        StageLabel.TweenAlpha(new TweenSettings<float>()
        {
            StartValue = 0f,
            EndValue = 1f,
            Duration = 0.5f,
            Delay = 0.5f,
            Prepare = true,
        });  
    }
}
