﻿using UnityEngine;
using System.Collections;

public class OMG : MonoBehaviour
{

    private FlashSprite omg;
    [SerializeField] private Vector3 Offset;

    void Start()
    {
        omg = GetComponent<FlashSprite>();
    }

    void Awake()
    {
        Game.Sound.PlaySound(SoundType.GameOMG);
    }

    void Update()
    {
        transform.position = Game.Player.Ship.transform.position + Offset;
	    if (omg.Finished())
	    {
            gameObject.SetActive(false);
	        Destroy(gameObject, 2.5f);
	    }
	}
	
}
