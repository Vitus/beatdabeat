﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum SoundType
{
    NoSound = -1,
    GameExplosion0 = 100,
    GameExplosion1 = 101,
    GameExplosion2 = 102,
    GameExplosion3 = 103,
    GameBossExplosion = 104,
    GameCollectCoin = 105,
    GameVSEmergency = 106,
    GameEmergency = 107,
    GameGetHit = 108,
    GamePowerupHealth = 109,
    GamePowerupBomb = 110,
    GamePowerupShift = 111,
    GameShoot = 112,
    GameBomb = 113,
    GameWarpDrive = 114,
    GameLevelComplete = 115,
    GameOMG = 116,
    GameSlowDown = 117,
    GameSpeedUp = 118,
    GameLaserShoot = 119,
    GameOverFly = 120,
    GameOverHit = 121,
    GameDmgUp = 122,
    GameGraze = 123,
    GameSkillMenuHit = 124,
    GameShieldSkill = 125,
    GameMetamorph = 126,
    GameVampire = 127,
    GameRocketShot = 128,
    GameRocketShotExplode = 129,
    GameRateSound = 130,
    GameMenuAppear = 131,
    MmButtonSelect = 200,
    MmButtonClick = 201,
    MmSwitchControl = 202,
    MmSoundOn = 203,
    MMSpark1 = 204,
    MMSpark2 = 205,
    MMIntro = 206,
    MMLetterBoom1 = 207,
    MMLetterBoom2 = 208,
    MMSharePhoto = 209,
    MMCoinsReward = 210,
    ShopSelectButton = 300,
    ShopClickButton = 301,
    FinalKick = 400,
    Story1EnemiesAppear = 500,
    Story1Flash = 501,
    Story1HeartTheme = 502,
    Story2EngineLoop = 503,
    Story2AlienFlyaway = 504,
    Story2BossGrabGirl = 505,
    Story2BossPunch = 506,
    Story2BossLaugh = 507,
    Story2BossWalk = 508,
    Story2GgHurt = 509,
    Story2GgPunch1 = 510,
    Story2GgPunch2 = 511,
    Story2GgPunch3 = 512,
    Story2GgPunchBoss = 513,
    Story2GgPunchWithoutHit = 514,
    Story3EnteringTheShip = 515,
    Story3Fall = 516,
    Story3GgEnterShip = 517,
    Story3GgFinalJump = 518,
    Story3GgHitTheGround = 519,
    Story3GgJump1 = 520,
    Story3GgJump2 = 521,
    Story3GgLand = 522,
    Story3GgWakeup = 523,
    Story2FightTheme = 524,
    TutorialType = 600,
    TutorialBotAppear = 601,
    TutorialBotHide = 602,
    TutorialStar = 603,
    TurorialApplause = 604,
    TutorialBulletAppear = 605,
    TutorialHead = 606,
    TutorialBelt = 607,
    TutorialWeapon = 608,
    TutorialBossHide = 609,
    TutorialNoMoney = 610,
    TutorialUpgrade = 611,
    TutorialCoinVariants = 612,
    TutorialHeartBeat = 613,
    TutorialRewardBackground = 700,
    TutorialRewardText = 701,
    TutorialRewardGiftBackground = 702,
    TutorialRewardCmon = 703,
    TutorialRewardCoins = 704,
    TutorialRewardEarn = 705,
    StarterPackAppear = 800,
    StarterPackDonutFall = 801,
    StarterPackRainbow = 802,
    StarterPackTimer = 803,
}


public class SoundManager : MonoBehaviour
{
    [Serializable]
    public class SoundElement
    {
        public SoundType Type;
        public AudioClip Clip;
        public float Volume = 0.5f;
        public bool IgnoreTimeScale = false;
    }

    [Serializable]
    public class SoundPack
    {
        public string Name;
        public List<SoundElement> Sounds;
    }

    [SerializeField] public float GlobalVolume;
    [SerializeField] private AudioSource _soundSource;
    [SerializeField] private AudioSource _ignoredTimeScaleSoundSource;
    [SerializeField] private List<SoundPack> _sounds;
    private Dictionary<SoundType, SoundElement> _soundDictionary = new Dictionary<SoundType, SoundElement>();

    private bool _gameLoaded;
    private bool _firstFrame;

    public bool SoundOn
    {
        get { return PlayerPrefs.GetInt("SoundState", 1) == 1; }
        set
        {
            PlayerPrefs.SetInt("SoundState", value ? 1 : 0);
            _soundSource.enabled = value;
            _ignoredTimeScaleSoundSource.enabled = value;
        }
    }

    public static SoundManager Main { get; private set; }

    void Awake()
    {
//        DontDestroyOnLoad(gameObject);
        Main = this;
        foreach (SoundPack pack in _sounds)
        {
            foreach (SoundElement soundElement in pack.Sounds)
            {
                _soundDictionary.Add(soundElement.Type, soundElement);
            }
        }
        OnLevelWasLoaded();
    }

    void Update() {
        _soundSource.pitch = Time.timeScale;
        if (_firstFrame)
        {
            _firstFrame = false;
            InitMusic();
        }
    }

    public float Pitch {
        get { return _soundSource.pitch; }
    }

    void OnLevelWasLoaded()
    {
        _firstFrame = true;
        InitMusic();
    }

    void InitMusic()
    {
        SoundOn = SoundOn;
    }

    public void PlaySound(SoundType type)
    {
        if (_soundSource.enabled && _soundDictionary.ContainsKey(type)) {
            SoundElement soundElement = _soundDictionary[type];
            if (soundElement.IgnoreTimeScale) {
                _ignoredTimeScaleSoundSource.PlayOneShot(soundElement.Clip, soundElement.Volume*GlobalVolume);
            }
            else {
                _soundSource.PlayOneShot(soundElement.Clip, soundElement.Volume * GlobalVolume);
            }
        }
    }

    public void PlayRandomSound(params SoundType[] type)
    {
        SoundType sound = type[Random.Range(0, type.Length)];
        PlaySound(sound);
    }
}