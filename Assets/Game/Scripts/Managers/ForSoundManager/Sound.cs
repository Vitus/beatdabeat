﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Sound : MonoBehaviour
{
    [SerializeField] private float _volume;
    [SerializeField] private SoundCollectionType _collectionType;

    public float Volume
    {
        get { return _volume; }
        set
        {
            _volume = value;
            UpdateVolume();
        }
    }

    public SoundCollectionType CollectionType { get { return _collectionType; } }
    public AudioSource Source { get; private set; }
    public SoundCollection SoundCollection { private get; set; }

    void Awake ()
    {
        Source = GetComponent<AudioSource>();
//	    Game.Sound.RegisterSource(this);
	}

    public void UpdateVolume()
    {
//        Source.volume = SoundCollection != null ? Volume*SoundCollection.Volume : Volume;
    }
}
