﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class SoundCollection
{
    public SoundCollectionType Type;
    public float Volume;
}