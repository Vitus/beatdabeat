﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class LoopedSound : MonoBehaviour {
    public float Volume;
    private AudioSource _source;

    void Awake() {
        _source = GetComponent<AudioSource>();
    }

    void Update() {
        _source.pitch = Game.Sound.Pitch;
        _source.volume = Game.Sound.GlobalVolume * Volume;
        _source.enabled = Game.Sound.SoundOn;
    }
}
