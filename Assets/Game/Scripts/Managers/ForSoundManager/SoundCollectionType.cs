﻿using UnityEngine;
using System.Collections;

public enum SoundCollectionType
{
    Sound = 0,
    Music = 1
}
