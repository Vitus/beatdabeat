﻿using System;
using UnityEngine;
using System.Collections;
using System.Diagnostics;

[Serializable]
public class LocalizedText
{
    [TextArea(1, 10)]
    public string _eng;
    [TextArea(1, 10)]
    public string _rus;
    [TextArea(1, 10)]
    public string _ger;
    [TextArea(1, 10)]
    public string _fra;
    [TextArea(1, 10)]
    public string _chns;
    [TextArea(1, 10)]
    public string _chnt;
    [TextArea(1, 10)]
    public string _kor;
    [TextArea(1, 10)]
    public string _jap;
    [TextArea(1, 10)]
    public string _tur;
    [TextArea(1, 10)]
    public string _spa;
    [TextArea(1, 10)]
    public string _ita;
    [TextArea(1, 10)]
    public string _por;

    public string InCurrentLanguage
    {
        get
        {
            switch (Game.Language.CurrentLanguage)
            {
                case Language.eng:
                    return _eng;
                case Language.rus:
                    return _rus;
                case Language.ger:
                    return _ger;
                case Language.fra:
                    return _fra; 
                case Language.chns:
                    return _chns;
                case Language.chnt:
                    return _chnt; 
                case Language.kor:
                    return _kor; 
                case Language.jap:
                    return _jap; 
                case Language.tur:
                    return _tur; 
                case Language.spa:
                    return _spa; 
                case Language.ita:
                    return _ita; 
                case Language.por:
                    return _por; 
                default:
                    return _eng;
            }
        }
    }

    public override string ToString()
    {
        return InCurrentLanguage;
    }

    public static implicit operator String(LocalizedText d)
    {
        if (d != null)
        {
            return d.ToString();
        }
        else
        {
            return "";
        }
    }
}