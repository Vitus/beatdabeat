﻿using System;
using UnityEngine;
using System.Collections;

public class LocalizeTextMesh : MonoBehaviour {
    public float Scale = 1;
    public string LocalizeID;
    public LocalizedText _text;

    private float _startSpacing;
    private tk2dTextMesh _textMesh;

    private void Awake() {
        _textMesh = GetComponent<tk2dTextMesh>();

        _startSpacing = _textMesh.LineSpacing;
    }

    private void OnEnable() {
        Game.Language.OnLanguageChanged += OnLanguageChanged;
        SetLocalization(LocalizeID);
    }

    private void OnDisable() {
        if (Game.Language != null) {
            Game.Language.OnLanguageChanged -= OnLanguageChanged;
        }
    }

    public void SetLocalization(string localizeID) {
        if (LocalizeID != "") {
            _text = Game.Language.GetLocalization(localizeID);
        }
        SetLocalization(_text);
    }

    public void SetLocalization(LocalizedText localization) {
        _text = localization;

        _textMesh = GetComponent<tk2dTextMesh>();
        UpdateFont();
        UpdateText();
    }

    private void UpdateFont() {
        string fontName = _textMesh.font.name.Split('_')[0];
        string localizedFontName = fontName + Game.Language.FontSuffix;
        tk2dFontData font = Game.Language.GetFont(localizedFontName);
        if (font == null) {
            return;
        }
        _textMesh.LineSpacing = Game.Language.FontDoubleSize ? _startSpacing : _startSpacing + 8;
        _textMesh.font = font;
        _textMesh.scale = (Game.Language.FontDoubleSize ? Vector3.one*2 : Vector3.one) * Scale;
    }

    private void OnLanguageChanged() {
        SetLocalization(_text);
    }

    private void UpdateText() {
        string text = GetText();
        if (Game.Language.RequiredUpperCase) {
            text = text.ToUpper();
        }
        _textMesh.maxChars = text.Length;
        _textMesh.text = text;
    }

    private string GetText() {
        return _text != null ? _text.InCurrentLanguage : "";
    }
}