﻿using System.Linq;
using UnityEngine;
using System.Collections;

public class LocalizeAnimation : MonoBehaviour {

    private tk2dSpriteAnimator _animator;
    private string _animationName;

    void Start()
    {
        _animator = GetComponent<tk2dSpriteAnimator>();
        _animationName = _animator.CurrentClip.name;
        Localize();
    }

    void OnEnable()
    {
        Game.Language.OnLanguageChanged += OnLanguageChanged;
        Localize();
    }

    void OnDisable()
    {
        if (Game.Language != null)
        {
            Game.Language.OnLanguageChanged -= OnLanguageChanged;
        }
    }

    private void OnLanguageChanged()
    {
        Localize();
    }

    public void Localize()
    {
        _animator = GetComponent<tk2dSpriteAnimator>();
        string localizedAnimationName = _animationName + Game.Language.Suffix;
        if (_animator.GetClipByName(localizedAnimationName) != null)
        {
            _animator.Play(localizedAnimationName);
        }
    }
}
