﻿using System.Linq;
using UnityEngine;
using System.Collections;

public class LocalizeFlashAnimation : MonoBehaviour {

    void Awake() {
        var flashSprite = GetComponent<FlashSprite>();
        
        string animationName = flashSprite.CurrentAnimation.name;
        string localizedSpriteName = animationName + Game.Language.Suffix;
        if (flashSprite.AnimationList.Find(flashAnimation => flashAnimation.name == localizedSpriteName) != null)
        {
            flashSprite.ChangeAnimation(localizedSpriteName);
        }
    }
}