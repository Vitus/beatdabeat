﻿using UnityEngine;

public class LocalizeSprite : MonoBehaviour {
    public bool ReduceSizeForAsian;
    private tk2dSprite _sprite;
    private string _spriteName;
    private Vector3 _scale;

    private void Start() {
        Init();
        Localize();
    }

    private void Init() {
        _sprite = GetComponent<tk2dSprite>();
        if (_scale == Vector3.zero) {
            _scale = _sprite.scale;
        }
        if (string.IsNullOrEmpty(_spriteName)) {
            _spriteName = _sprite.CurrentSprite.name;
        }
    }

    private void OnEnable() {
        Game.Language.OnLanguageChanged += OnLanguageChanged;
        Localize();
    }

    private void OnDisable() {
        if (Game.Language != null) {
            Game.Language.OnLanguageChanged -= OnLanguageChanged;
        }
    }

    private void OnLanguageChanged() {
        Localize();
    }

    public void Localize() {
        if (_sprite == null) {
            Init();
        }
        string localizedSpriteName = _spriteName + Game.Language.Suffix;
        if (_sprite.Collection.GetSpriteIdByName(localizedSpriteName, -1) != -1) {
            _sprite.SetSprite(localizedSpriteName);
            _sprite.scale = _scale*(!Game.Language.FontDoubleSize && ReduceSizeForAsian ? 0.5f : 1);
        } else if (_sprite.Collection.GetSpriteIdByName(_spriteName, -1) != -1) {
            _sprite.SetSprite(_spriteName);
            _sprite.scale = _scale * (!Game.Language.FontDoubleSize && ReduceSizeForAsian ? 0.5f : 1);
        }
    }

    public void SetSprite(string imageName) {
        _spriteName = imageName;
        Localize();
    }
}