﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine.Assertions;

public class HealthManager : Singleton<HealthManager> {
    private const float ReplenishTime = 300f;

    public int HealthReplenishCost = 2;

    private Dictionary<ShopShip, ShipHealth> _health = new Dictionary<ShopShip, ShipHealth>();

    private void Awake() {
        Assert.IsTrue(Game.Upgrade.Ships.Length > 0);
        foreach (ShopShip ship in Game.Upgrade.Ships) {
            _health.Add(ship, new ShipHealth(ship, ReplenishTime));
        }
    }

    public ShipHealth CurrentShipHealth {
        get {
            return _health[Game.Upgrade.CurrentShip];
        }
    }

    public ShipHealth GetShipHealth(ShopShip ship) {
        Assert.IsTrue(_health.ContainsKey(ship), "Wrong ship. Seems like this ship is not added to UpgradeManager.");
        return _health[ship];
    }

    protected HealthManager() {
    }

    [RuntimeInitializeOnLoadMethod]
    private static void Init() {
        CreateInstance();
    }
}