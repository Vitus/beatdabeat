﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

[Serializable]
public class ShipHealth {
    public event Action RunOutOfHealth = delegate { };
    public event Action HealthChanged = delegate { };

    private Timer _healthTimer;
    public ShopShip Ship { get; private set; }
    private int _current;

    public int Max {
        get { return MaxFormula(Ship.HealthLevel); }
    }

    public int AtStart {
        get {
            if (Ship.HealthLevel == 0) {
                return 2;
            }
            return Mathf.CeilToInt(MaxFormula(Ship.HealthLevel) / 2f); 
        }
    }

    private int MaxFormula(int healthLevel) {
        return (healthLevel + 1)*2;
    }

    public int Current {
        get { return _current; }
        set {
            Assert.IsTrue(_current >= 0 && _current <= Max);

            if (_current == value) {
                return;
            }
            _current = value;
            if (false) {
                PlayerPrefs.SetInt("Health." + Ship.IconName + ".Current", _current);
            }
            HealthChanged();
            if (false) {
                SetLocalNotification();
            }

            if (_current == 0) {
                RunOutOfHealth();
            }
        }
    }

    public bool FullyRestored {
        get { return Current == Max; }
    }


    public TimeSpan TimeLeftToReplenish {
        get {
            return _healthTimer.IntervalTimeLeft;
        }
    }

    public int FullReplenishCost {
        get { return HealthManager.Instance.HealthReplenishCost*(Max - Current); }
    }

    public ShipHealth(ShopShip ship, float replenishTime) {
        Ship = ship;
        Ship.ShipUpgraded += OnShipUpgraded;
        if (false) {
            _healthTimer = TimerManager.CreateTimer(Ship.IconName);
            _healthTimer.IntervalLengthInSeconds = replenishTime;
            _healthTimer.IntervalComplete += Replenish;

            _current = PlayerPrefs.GetInt("Health." + Ship.IconName + ".Current", Max);
        }
    }

    private void OnShipUpgraded(string upgradeId) {
        if (false && upgradeId == ShopShip.HealthUpgradeID) {
            if (Current == MaxFormula(Ship.HealthLevel - 1)) {
                _healthTimer.ResetStartTime();
            }
        }
    }

    public void Replenish(int amount) {
        if (Current >= Max) {
            return;
        }
        Current = Mathf.Min(Current + amount, Max);
    }

    public void Deplete() {
        if (Current <= 0) {
            return;
        }
        if (Current == Max && false) {
            _healthTimer.ResetStartTime();
        }
        Current--;
    }

    public void FullReplenish() {
        Current = Max;
    }

    private void SetLocalNotification() {
        if (Current >= Max) {
            return;
        }
        _healthTimer.SetLocalNotification(Max - Current, Game.Language.GetLocalization("shop_health_restored"),
            String.Format(Game.Language.GetLocalization("shop_ship_recovered"), Ship.ShipName));
    }
}