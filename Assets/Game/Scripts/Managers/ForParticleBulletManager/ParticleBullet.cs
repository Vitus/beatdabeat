﻿using System;
using LevelParserV2;
using UnityEngine;

[Serializable]
public class ParticleBullet : ICaster, ITRSObject
{
    public ParticleBulletType Type;
    public ParticleBulletBehaviour Behaviour { get; private set; }

    public string Name;

    public Vector2 Position;

    public bool Indestructable;

    public float Angle;
    public float AngularVelocity;
    public float Speed;
    public Vector2 Velocity;

    public float AccelerationLinear;
    public float MinMaxSpeedLinear;

    public Vector2 Acceleration;
    public Vector2 MinMaxSpeed;

    public Color Color;
    public float Size;
    public float AppearSize;
    public float AppearTime;

    public float LifeTime;
    public bool IsNull = true;
    public bool IsDead;
    public float DeadTime { get; private set; }
    public bool IsGrazed;
    public float DestroyRadius;

    private Matrix4x4 _matrix = Matrix4x4.identity;

    public Matrix4x4 GetMatrix()
    {
        float radRotation = -Angle*Mathf.Deg2Rad;
        float sin = Mathf.Sin(radRotation);
        float cos = Mathf.Cos(radRotation);
        _matrix.m00 = cos / Size;
        _matrix.m01 = -sin / Size;
        _matrix.m10 = sin / Size;
        _matrix.m11 = cos / Size;
        _matrix.m03 = -_matrix.m00 * Position.x - _matrix.m01 * Position.y;
        _matrix.m13 = -_matrix.m10 * Position.x - _matrix.m11 * Position.y;
        return _matrix;
    }

    public Vector2 TransformPosition { get { return Position; } }
    public float TransformAngle { get { return Angle; } }
    public Vector2 TransformScale { get { return new Vector2(Size, Size); } }

    public ParticleBullet Spawn(string name = "")
    {
        Type = ParticleBulletType.Medium;
        Behaviour = ParticleBulletBehaviour.Polar;
        Indestructable = false;
        Name = name;
        Position.x = 0;
        Position.y = 0;

        Angle = 0;
        AngularVelocity = 0;
        Speed = 0;
        Velocity.x = 0;
        Velocity.y = 0;

        AccelerationLinear = 0;
        MinMaxSpeedLinear = float.NaN;

        Acceleration.x = 0;
        Acceleration.y = 0;
        MinMaxSpeed.x = float.NaN;
        MinMaxSpeed.y = float.NaN;

        Color = Color.white;
        Size = 1;

        AppearSize = 3f;
        AppearTime = 0.3f;

        IsNull = false;
        IsDead = false;
        DeadTime = 0;
        LifeTime = 0;
        DestroyRadius = 0;
        IsGrazed = false;

        return this;
    }

    public void Kill()
    {
        if (!IsDead)
        {
            IsDead = true;
            DeadTime = LifeTime;
        }
    }

    private float _prevAngle = float.MaxValue;
    private Vector2 _direction;

    public void UpdatePosition()
    {
        const float slowDown = 1f;
        LifeTime += Time.deltaTime;

        if (Behaviour == ParticleBulletBehaviour.Polar)
        {
//            Angle += AngularVelocity * Time.deltaTime;
            Speed += AccelerationLinear * Time.deltaTime * slowDown;
            if (!float.IsNaN(MinMaxSpeedLinear))
                Speed = Mathf.Clamp(Speed, AccelerationLinear > 0 ? float.MinValue : MinMaxSpeedLinear * slowDown, AccelerationLinear > 0 ? MinMaxSpeedLinear * slowDown : float.MaxValue);
            
            if (Angle != _prevAngle)
                _direction = H.PolarVector2(1f, Angle);

            Vector2 velocity = _direction * Speed;
            Position.x += velocity.x * Time.deltaTime * slowDown;
            Position.y += velocity.y * Time.deltaTime * slowDown;
        }
        else if (Behaviour == ParticleBulletBehaviour.Cartesian)
        {
//            Velocity = H.RotateVector(Velocity, AngularVelocity * Time.deltaTime);
            Velocity.x += Acceleration.x * Time.deltaTime * slowDown;
            Velocity.y += Acceleration.y * Time.deltaTime * slowDown;
            if (!float.IsNaN(MinMaxSpeed.x))
                Velocity.x = Mathf.Clamp(Velocity.x, Acceleration.x > 0 ? float.MinValue : MinMaxSpeed.x * slowDown, Acceleration.x > 0 ? MinMaxSpeed.x * slowDown : float.MaxValue);
            if (!float.IsNaN(MinMaxSpeed.y))
                Velocity.y = Mathf.Clamp(Velocity.y, Acceleration.y > 0 ? float.MinValue : MinMaxSpeed.y * slowDown, Acceleration.y > 0 ? MinMaxSpeed.y * slowDown : float.MaxValue);
            Position.x += Velocity.x * Time.deltaTime * slowDown;
            Position.y += Velocity.y * Time.deltaTime * slowDown;

            Angle = H.VectorAngle(Velocity);
        }
    }

    public ParticleBullet SetBehaviour(ParticleBulletBehaviour newBehaviour)
    {
        if (newBehaviour != Behaviour)
        {
            Behaviour = newBehaviour;
            switch (newBehaviour)
            {
                case ParticleBulletBehaviour.Polar:
                    Angle = H.VectorAngle(Velocity);
                    Speed = Velocity.magnitude;
                    AccelerationLinear = Acceleration.magnitude;
                    MinMaxSpeedLinear = MinMaxSpeed.magnitude;
                    break;
                case ParticleBulletBehaviour.Cartesian:
                    Velocity = H.PolarVector2(Speed, Angle);
                    Acceleration = H.PolarVector2(AccelerationLinear, Angle);
                    MinMaxSpeed = H.PolarVector2(MinMaxSpeedLinear, Angle);
                    break;
            }
        }
        return this;
    }

    public ParticleBullet SetType(ParticleBulletType type)
    {
        Type = type;
        return this;
    }

    public ParticleBullet SetIndestructable(bool indestructable)
    {
        Indestructable = indestructable;
        return this;
    }

    public ParticleBullet SetPosition(Vector2 position)
    {
        Position.x = position.x;
        Position.y = position.y;
        return this;
    }

    public ParticleBullet SetPosition(float x, float y)
    {
        Position.x = x;
        Position.y = y;
        return this;
    }

    public ParticleBullet SetRotation(float rotation)
    {
        Angle = rotation;
        return this;
    }

    public ParticleBullet SetSize(float size)
    {
        Size = size;
        return this;
    }

    public ParticleBullet SetVelocity(Vector2 velocity)
    {
        Velocity.x = velocity.x;
        Velocity.y = velocity.y;
        return this;
    }
    
    public ParticleBullet SetMaxSpeed(float minMaxSpeedLinear)
    {
        MinMaxSpeedLinear = minMaxSpeedLinear;
        return this;
    }

    public ParticleBullet SetVelocity(float x, float y)
    {
        Velocity.x = x;
        Velocity.y = y;
        return this;
    }

    public ParticleBullet SetAcceleration(Vector2 acceleration)
    {
        Acceleration.x = acceleration.x;
        Acceleration.y = acceleration.y;
        return this;
    }

    public ParticleBullet SetAcceleration(float x, float y)
    {
        Acceleration.x = x;
        Acceleration.y = y;
        return this;
    }

    public ParticleBullet SetAngularVelocity(float angularVelocity)
    {
        AngularVelocity = angularVelocity;
        return this;
    }

    public ParticleBullet SetColor(Color color)
    {
        Color = color;
        return this;
    }

    public ParticleBullet SetLifeTime(float lifeTime)
    {
        LifeTime = lifeTime;
        return this;
    }

    public ParticleBullet SetIsNull(bool isNull)
    {
        IsNull = isNull;
        return this;
    }

    public ParticleBullet SetIsCollected(bool isCollected)
    {
        IsGrazed = isCollected;
        return this;
    }

    public ParticleBullet SetDestroyRadius(float destroyRadius)
    {
        DestroyRadius = destroyRadius;
        return this;
    }

    public ParticleBullet SetAppearTime(float appearTime)
    {
        AppearTime = appearTime;
        return this;
    }

    public ParticleBullet SetAppearSize(float appearSize)
    {
        AppearSize = appearSize;
        return this;
    }

    public ParticleBullet LookToVelocity()
    {
        Angle = H.VectorAngle(Velocity);
        return this;
    }

    public ParticleBullet SetAngle(float angle)
    {
        return this;
    }

    public Vector2 TRSPosition { get { return Position; } }
    public float TRSAngle { get { return Angle; } }
    public Vector2 TRSScale { get { return new Vector2(Size, Size); } }

    public bool IsAlive()
    {
        return !IsDead;
    }
}
