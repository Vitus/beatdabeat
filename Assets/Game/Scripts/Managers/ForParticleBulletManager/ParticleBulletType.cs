﻿using UnityEngine;
using System.Collections;

public enum ParticleBulletType
{
    LongSharp = 0,
    SixLines = 1,
    Torus = 2,
    Plasma = 3,
    Corner = 4,
    Smallest = 5,
    Stretched = 6,
    SquareDark = 7,
    ThreeDots = 8,
    Medium = 9,
    Large = 10,
    Triangle = 11,
    SmallRectangle = 12,
    SquareLight = 13,
    SmallSharp = 14,
    Arc = 15
}