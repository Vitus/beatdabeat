﻿using System;
using UnityEngine;
using System.Collections;
using System.ComponentModel;

public class Currency : MonoBehaviour
{
    public static event Action OnValueChanged;

    public static int Current
    {
        get { return PlayerPrefs.GetInt("Currency.Current", 0); }
        set
        {
            if (Current == value)
                return;

            PlayerPrefs.SetInt("Currency.Current", value);
            if (OnValueChanged != null)
                OnValueChanged();
        }
    }
}