﻿using System;
using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public float Score { get; private set; }
    public int ScoreMultiplier { get; private set; }
    public int ShootCount { get; private set; }
    public int GrazeCount { get; private set; }
    public int BombCount { get; private set; }
    public int DeathCount { get; private set; }
    public int EnemyHitCount { get; private set; }
    public int CoinCount { get; private set; }
    public int EnemyDestroyCount { get; private set; }
    public int BonusCollectedCount { get; private set; }
    private float StartTime { get; set; }
    public float LevelTime { get { return Time.timeSinceLevelLoad - StartTime; } }
    public float FocusLength { get; set; }

    void OnEnable()
    {
        NEvent.AddListener<GrazeEvent>(Game.GameObject, OnGraze);
        NEvent.AddListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);
        NEvent.AddListener<EnemyDestroyEvent>(Game.GameObject, OnEnemyDestroy);
        NEvent.AddListener<EnemyHitEvent>(Game.GameObject, OnEnemyHit);
        NEvent.AddListener<GetCoinEvent>(Game.GameObject, OnGetCoin);
        NEvent.AddListener<GetBonusEvent>(Game.GameObject, OnGetBonus);
        NEvent.AddListener<PlayerShootEvent>(Game.GameObject, OnPlayerShoot);
        NEvent.AddListener<StartLevelEvent>(Game.GameObject, OnStartLevel);
        NEvent.AddListener<BombEvent>(Game.GameObject, OnBomb);
    }

    private void OnBomb(BombEvent bombEvent)
    {
        BombCount++;
    }

    void OnDisable()
    {
        NEvent.RemoveListener<GrazeEvent>(Game.GameObject, OnGraze);
        NEvent.RemoveListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);
        NEvent.RemoveListener<EnemyDestroyEvent>(Game.GameObject, OnEnemyDestroy);
        NEvent.RemoveListener<EnemyHitEvent>(Game.GameObject, OnEnemyHit);
        NEvent.RemoveListener<GetCoinEvent>(Game.GameObject, OnGetCoin);
        NEvent.RemoveListener<GetBonusEvent>(Game.GameObject, OnGetBonus);
        NEvent.RemoveListener<PlayerShootEvent>(Game.GameObject, OnPlayerShoot);
        NEvent.RemoveListener<StartLevelEvent>(Game.GameObject, OnStartLevel);
        NEvent.RemoveListener<BombEvent>(Game.GameObject, OnBomb);
    }
    
    private void OnStartLevel(StartLevelEvent startLevelEvent)
    {
        ResetScore();
        ShootCount = 0;
        GrazeCount = 0;
        BombCount = 0;
        DeathCount = 0;
        EnemyHitCount = 0;
        CoinCount = 0;
        EnemyDestroyCount = 0;
        BonusCollectedCount = 0;
        FocusLength = 0;
        StartTime = Time.timeSinceLevelLoad;
    }

    public void ResetScore() {
        Score = 0;
        ScoreMultiplier = 1;
    }

    void Awake()
    {
        ScoreMultiplier = 1;
    }

    private void OnGetCoin(GetCoinEvent e)
    {
        CoinCount++;
        Score += 70*ScoreMultiplier;
    }

    private void OnEnemyHit(EnemyHitEvent e)
    {
        EnemyHitCount++;
        Score += 5*ScoreMultiplier;
    }

    private void OnEnemyDestroy(EnemyDestroyEvent e)
    {
        EnemyDestroyCount++;
        ScoreMultiplier += 1;
        Score += 1000*ScoreMultiplier;
    }

    private void OnPlayerHit(PlayerHitEvent e)
    {
        DeathCount++;
        ScoreMultiplier = 1;
    }

    private void OnGetBonus(GetBonusEvent e)
    {
        BonusCollectedCount++;
        Score += 300*ScoreMultiplier;
    }

    private void OnPlayerShoot(PlayerShootEvent e)
    {
        ShootCount++;
    }

    private void OnGraze(GrazeEvent e)
    {
        GrazeCount++;
        Score += 10*ScoreMultiplier;
    }

    public void SavePracticeScore()
    {
        if (!Game.Cheat.CheatsUsed())
        {
            float previousBest = GetPracticeScore();
            if (previousBest < Score)
            {
                PlayerPrefs.SetFloat(String.Format("{0}.Score.{1}", Game.Difficulty, Game.Level.CurrentLevel), Score);
            }
        }
    }

    public float GetPracticeScore()
    {
        return PlayerPrefs.GetFloat(String.Format("{0}.Score.{1}", Game.Difficulty, Game.Level.CurrentLevel), 0);
    }
}
