﻿using System.Collections.Generic;
using LevelParserV3;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour {
    public bool IsPracticeUnlocked;

    public int StartLevel;
    public int CurrentLevel;
    public GameObject EmergencyPrefab;
    public GameObject BigEmeregencyPrefab;
    public GameObject OMG;
    public GameObject NextLevelBanner;
    public GameObject Explosion;
    public GameObject Coin;

    [SerializeField] private Enemy EnemyPrefab;
    [SerializeField] private Laser LaserPrefab;
    public bool IsPractice = true;

    public LevelScript[] LevelScripts = {
        new Level_Infiltration(),
        new Level_Porridge(),
        new Level_LucidDreams(),
        new Level_BossEwj(),
        new Level_MomentOfTruth(),
        new Level_Boss3(),
        new Level_StreetWise(),
        new Level_Dash()
    };

    public Enemy[] Bosses;

    public bool FirstTimePlay {
        get { return PlayerPrefs.GetInt("FirstTimePlay", 1) == 1; }
        set { PlayerPrefs.SetInt("FirstTimePlay", value ? 1 : 0); }
    }

    public LevelScript CurrentLevelScript {
        get { return LevelScripts[CurrentLevel]; }
    }

    public bool IsTutorial;

    public Enemy SpawnEnemy() {
        return EnemyPrefab.Clone();
    }

    public Laser CreateLaser() {
        return Instantiate(LaserPrefab);
    }

    public bool IsDifficultyBeated(int difficulty) {
        return GetMaxUnlockedLevel(difficulty) >= LevelScripts.Length;
    }

    public bool IsAllDifficultiesBeated {
        get {
            return Game.Level.IsDifficultyBeated(1) && Game.Level.IsDifficultyBeated(2) &&
                   Game.Level.IsDifficultyBeated(3) && Game.Level.IsDifficultyBeated(4);
        }
    }

    public void SetIsBeated(int difficulty, int stageNum, bool value = true) {
        PlayerPrefs.SetInt(string.Format("{0}.Beated.{1}", difficulty, stageNum), value ? 1 : 0);
    }

    public int GetMaxUnlockedLevel(int difficulty) {
        if (IsPracticeUnlocked) {
            return LevelScripts.Length - 1;
        } else {
            return PlayerPrefs.GetInt(string.Format("{0}.MaxLevel", difficulty), 0);
        }
    }

    public void SetMaxUnlockedLevel(int difficulty, int value) {
        int prevValue = GetMaxUnlockedLevel(difficulty);
        PlayerPrefs.SetInt(string.Format("{0}.MaxLevel", difficulty), value);
        if (prevValue < value && IsDifficultyBeated(difficulty)) {
            foreach (ShopShip ship in Game.Upgrade.Ships) {
                if (ship.DifficultyRequirement == difficulty ||
                    (IsAllDifficultiesBeated && ship.DifficultyRequirement == -1)) {
//                    Debug.Log("ship unlocked " + ship.name);
                    UnlockAnimation.UnlockList.Enqueue(ship);
                }
            }
        }
    }

    public void Win() {
        LevelPlayer.Main.LevelLaunched = false;
        StartCoroutine(WinProcess());
    }

    private IEnumerator WinProcess() {
        RemoveAllBullets();
        if (Bosses[CurrentLevel] != null) {
            yield return StartCoroutine(BossExplosion());
        }

        if (!IsPractice && CurrentLevel != LevelScripts.Length)
        {
            StartCoroutine(GoToNextLevel());
        }
    }

    private IEnumerator BossExplosion() {
        Game.Music.Stop();
        DanceManAnimator.Main.BPM = 0;
        Object[] enemies = FindObjectsOfType(typeof (Boss));
        Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
            StartValue = 10,
            EndValue = 0,
            Duration = 5f,
            IgnoreTimeScale = true,
            AffectAxis = AffectAxis.XY
        });
        foreach (Object enemy in enemies) {
            (enemy as Boss).GetComponent<Enemy>().SetHealthType(HealthType.OnBackground);
        }
        for (int i = 0; i < 20; i++) {
            Enemy enemy = (H.GetRandomElement<Object>(enemies) as Boss).GetComponent<Enemy>();
            BoxCollider boxCollider = enemy.Image.GetComponent<BoxCollider>();
            Vector3 size = boxCollider.size;
            Vector3 bossPosition = boxCollider.center + enemy.transform.position;
            Vector3 explosionPosition = bossPosition +
                                        new Vector3(Random.Range(-size.x, size.x)/2f, Random.Range(-size.y, size.y)/2f);
            Instantiate(Explosion, explosionPosition, Quaternion.identity);

            if (!IsPractice) {
                int coinCount = (Game.Level.CurrentLevel + 1);

                for (int coinNum = 0; coinNum < coinCount; coinNum++) {
                    var coin =
                        Instantiate(Coin, explosionPosition + (Vector3) H.RandomVector2(20, false), Quaternion.identity)
                            as GameObject;
                    coin.GetComponent<Coin>().SetValue(5);
                }
            }

            if (i%3 == 0) {
                Game.Sound.PlayRandomSound(SoundType.GameExplosion0, SoundType.GameExplosion1, SoundType.GameExplosion2,
                    SoundType.GameExplosion3);
            }

            yield return new WaitForSeconds(0.1f);
        }
        Game.Sound.PlaySound(SoundType.GameBossExplosion);
        NEvent.Dispatch(Game.GameObject, new StarSpeedEvent(1f, Color.white, 1f));
        foreach (Object enemy in enemies) {
            Destroy((enemy as Boss).gameObject);
        }
        yield return new WaitForSeconds(3f);
    }

    private IEnumerator GoToNextLevel() {
        CurrentLevel++;

        int maxLevel = GetMaxUnlockedLevel(Game.Difficulty);
        if (maxLevel < CurrentLevel) {
            SetMaxUnlockedLevel(Game.Difficulty, CurrentLevel);
        }

        if (CurrentLevel != LevelScripts.Length)
        {
            BossHealthBar.Main.SetMax(1000000);
            BossHealthBar.Main.Show(false);
            LevelPlayer.Main.StartCoroutine(LevelPlayer.Main.LoadCurrentLevel());
        }

        yield break;
    }

    private void RemoveAllBullets() {
        foreach (ParticleBullet bullet in Game.ParticleBullets.GetBullets()) {
            if (bullet != null) {
                bullet.Kill();
            }
        }

        foreach (Enemy enemy in FindObjectsOfType(typeof (Enemy))) {
            if (enemy.HealthType != HealthType.Boss) {
                Destroy(enemy.gameObject);
            }
        }
    }
}