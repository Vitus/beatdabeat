﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public class CheatManager : MonoBehaviour
{
    public bool Observer { get; set; }
    public bool InfiniteHealth { get; set; }
    public bool InfiniteBomb { get; set; }

    public void DisableCheats()
    {
        InfiniteBomb = false;
        InfiniteHealth = false;
    }

    public bool CheatsUsed()
    {
        return InfiniteBomb || InfiniteHealth;
    }
}