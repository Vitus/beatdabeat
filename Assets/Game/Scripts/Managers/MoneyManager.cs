﻿using System;
using UnityEngine;
using System.Collections;

public delegate void MoneyChangedHandler(int currentMoney);

public class MoneyManager : MonoBehaviour {
    public event MoneyChangedHandler MoneyChanged;

    public int Current {
        get { return PlayerPrefs.GetInt("Money", 0); }
        set {
            int newValue = value;
            if (newValue < 0) {
                newValue = 0;
            }
            if (Current != newValue) {
                PlayerPrefs.SetInt("Money", newValue);
                NotifyMoneyChanged(newValue);
            }
        }
    }

    private void NotifyMoneyChanged(int currentMoney) {
        if (MoneyChanged != null) {
            MoneyChanged(currentMoney);
        }
    }

    private void OnEnable() {
        NEvent.AddListener<DifficultyChangeEvent>(Game.GameObject, OnDifficultyChange);
    }

    private void OnDisable() {
        NEvent.RemoveListener<DifficultyChangeEvent>(Game.GameObject, OnDifficultyChange);
    }

    private void OnDifficultyChange(DifficultyChangeEvent difficultyChangeEvent) {
        NotifyMoneyChanged(Current);
    }
}