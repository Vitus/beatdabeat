﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{

    public bool IsPractice;

    void Awake()
    {
        Resources.LoadAll("EnemyImages/", typeof(Enemy));
    }

    void OnLevelWasLoaded()
    {
        Game.Music.Stop();
        Time.timeScale = 1f;
        Cursor.visible = true;
    }
}
