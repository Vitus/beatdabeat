﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour
{
    [HideInInspector] 
    public PlayerShip Ship;
    [HideInInspector]
    public FocusMenu Focus;
    [HideInInspector]
    public PlayerHealth Health;

    public Vector3 StartShipPosition = new Vector3(0f, -125f);

    void Awake()
    {
        OnLevelWasLoaded();
    }

    void OnLevelWasLoaded()
    {
        if (Application.loadedLevelName == "Game")
        {
            var shipPrefab = Game.Level.IsTutorial ? Game.Upgrade.Ships[0].Prefab : Game.Upgrade.CurrentShip.Prefab;
            Ship = (PlayerShip)Instantiate(shipPrefab, StartShipPosition, Quaternion.identity);
        }
    }
}
