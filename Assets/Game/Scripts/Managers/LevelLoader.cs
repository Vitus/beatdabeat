﻿using UnityEngine;
using System.Collections;
using System;

public class LevelLoader : MonoBehaviour
{
    private static LevelLoader _main;

    void Awake()
    {
        _main = this;
    }

    public static void LoadLevel(string levelName)
    {
        _main.StartCoroutine(ChangeLevel(levelName));
    }

    private static IEnumerator ChangeLevel(string levelName)
    {
        NelliFader.Instance.FadeIn(useRealTime:true);
        yield return _main.StartCoroutine(_main.WaitForRealtime(NelliFader.Instance.DefaultFadeTime));
        if (Game.Music != null)
            Game.Music.Stop();
        Application.LoadLevel("FreeMem");
		yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
		GC.Collect();
        Application.LoadLevel(levelName);
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
		NelliFader.Instance.FadeOut(useRealTime:true);
    }

    public static void LoadLevel(int levelNum)
    {
        _main.StartCoroutine(ChangeLevel(levelNum));
    }

    private static IEnumerator ChangeLevel(int levelNum)
    {
        NelliFader.Instance.FadeIn(useRealTime: true);
        yield return _main.StartCoroutine(_main.WaitForRealtime(NelliFader.Instance.DefaultFadeTime));
        if (Game.Music != null)
            Game.Music.Stop();
        Application.LoadLevel(levelNum);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
//        yield return _main.StartCoroutine(_main.WaitForRealtime(0.2f));
        NelliFader.Instance.FadeOut(useRealTime:true);
    }

    private IEnumerator WaitForRealtime(float seconds)
    {
        float elapsedTime = 0;
        while (elapsedTime < seconds) {
            elapsedTime += Time.unscaledDeltaTime;
            yield return null;
        }
    }
}
