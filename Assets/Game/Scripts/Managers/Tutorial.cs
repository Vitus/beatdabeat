﻿using System;
using LevelParserV3;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class Tutorial : MonoBehaviour {
    public static Tutorial Main { get; private set; }

    [SerializeField] private GameObject HitBoxHighlight;
    [SerializeField] private GameObject BossImage;
    [SerializeField] private TutorialDialog _dialog;
    [SerializeField] private TutorialStageBase[] Stages;
    [SerializeField] private AudioSource Music;
    [SerializeField] private int firstTestStage;

    private GameObject currentPart;
    private int _currentStage = -1;
    private bool _infiniteHealth;

    public static bool IsControlTutorialCompleted {
        get { return PlayerPrefs.GetInt("IsTutorialCompleted", 0) == 1; }
        set { PlayerPrefs.SetInt("IsTutorialCompleted", value ? 1 : 0); }
    }

    public static bool IsShopTutorialCompleted {
        get { return PlayerPrefs.GetInt("IsShopTutorialCompleted", 0) == 1; }
        set { PlayerPrefs.SetInt("IsShopTutorialCompleted", value ? 1 : 0); }
    }

    public static bool IsHealthTutorialCompleted {
        get { return PlayerPrefs.GetInt("IsHealthTutorialCompleted", 0) == 1; }
        set { PlayerPrefs.SetInt("IsHealthTutorialCompleted", value ? 1 : 0); }
    }

    public static bool IsAllTutorialCompleted {
        get {
            if (false) {
                return IsHealthTutorialCompleted;
            } else {
                return IsShopTutorialCompleted;
            }
        }
    }

    public static bool IsTutorialSkiped {
        get { return PlayerPrefs.GetInt("IsTutorialSkiped", 0) == 1; }
        set { PlayerPrefs.SetInt("IsTutorialSkiped", value ? 1 : 0); }
    }

    public bool IsInShop {
        get { return Application.loadedLevelName == "Shop"; }
    }

    private void Awake() {
        _infiniteHealth = Game.Cheat.InfiniteHealth;

        Game.Level.FirstTimePlay = false;

        Music.Stop();
        Main = this;
        foreach (var stage in Stages) {
            stage.gameObject.SetActive(false);
        }
        if (((!IsControlTutorialCompleted && !IsInShop)
             || (!IsShopTutorialCompleted && IsInShop)
             || (!IsHealthTutorialCompleted && IsInShop && false))
            && !IsTutorialSkiped) {
            if (!IsInShop) {
                LevelPlayer.Main.gameObject.SetActive(false);
            }
            Game.Upgrade.CurrentShipIndex = 0;
        } else {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable() {
        Game.Level.IsTutorial = true;
        Game.Tutorial = this;
        Game.Cheat.InfiniteHealth = true;
    }

    private void OnDisable() {
        Game.Level.IsTutorial = false;
        Game.Tutorial = null;
        Game.Cheat.InfiniteHealth = _infiniteHealth;
        Music.Stop();
        Game.Music.Play();
        if (!IsInShop) {
            if (Game.Player.Focus != null) {
                Game.Player.Focus.gameObject.SetActive(true);
            }
        }
        IsControlTutorialCompleted = true;
        if (IsInShop) {
            if (IsShopTutorialCompleted) {
                IsHealthTutorialCompleted = true;
            }
            IsShopTutorialCompleted = true;
        }
    }

    private IEnumerator Start() {
        Game.Music.Stop();
        Music.Play();
        Debug.Log("Play tutorial");

        yield return new WaitForSeconds(0.5f);
        if (!IsInShop) {
            Game.Player.Ship.BombCount = 0;
        }

        int startStage = 0;
        if (IsShopTutorialCompleted && IsInShop && !IsHealthTutorialCompleted) {
            startStage = 2;
        }

        startStage = (Application.isEditor && firstTestStage != 0) ? firstTestStage : startStage;

        if (!IsInShop) {
            Game.Player.Focus.gameObject.SetActive(false);
        }
        ShowStage(startStage);
    }

    private void ShowStage(int stageNum) {
        if (_currentStage >= 0) {
            Stages[_currentStage].gameObject.SetActive(false);
        }
        _currentStage = stageNum;
        Stages[_currentStage].gameObject.SetActive(true);
    }

    public bool IsShooting { get; set; }

    public static TutorialDialog Dialog {
        get { return Main._dialog; }
    }

    public static void ShowDialog(string textId, TutorialCharacterImage image, bool close = true,
        bool showTriangle = true) {
        Dialog.Show(Game.Language.GetLocalization(textId), image, close, showTriangle);
    }

    public void Complete() {
        gameObject.SetActive(false);
        if (!IsInShop) {
            LevelPlayer.Main.gameObject.SetActive(true);
        }
    }

    public void GoToNextStage() {
        if (_currentStage + 1 == Stages.Length) {
            Complete();
        } else {
            ShowStage(_currentStage + 1);
        }
    }
}