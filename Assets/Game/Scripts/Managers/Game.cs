﻿using UnityEngine;

public class Game : MonoBehaviour {
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Init() {
        Debug.Log("InitGame");
        _instance = Instantiate(Resources.Load<Game>("Game"));
    }

    private static Game _instance;

    [SerializeField] private DifficultyManager _difficultyManager;
    [SerializeField] private MoneyManager _moneyManager;
    [SerializeField] private PlayerManager _playerManager;
    [SerializeField] private SoundManager _soundManager;
    [SerializeField] private UpgradeManager _upgradeManager;
    [SerializeField] private MusicManager _musicManager;
    [SerializeField] private ParticleBulletManager _particleBulletManager;
    [SerializeField] private ScoreManager _scoreManager;
    [SerializeField] private LevelManager _levelManager;
    [SerializeField] private CheatManager _cheatManager;
    [SerializeField] private LanguageManager _languageManager;
    [SerializeField] private bool _developementBuild;

    public static int Difficulty;

    public static DifficultyManager DifficultyManager {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._difficultyManager;
        }
    }

    public static bool DevelopementBuild {
        get { return _instance._developementBuild; }
    }

    public static MoneyManager Money {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._moneyManager;
        }
    }

    public static PlayerManager Player {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._playerManager;
        }
    }

    public static SoundManager Sound {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._soundManager;
        }
    }

    public static UpgradeManager Upgrade {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._upgradeManager;
        }
    }

    public static MusicManager Music {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._musicManager;
        }
    }

    public static ParticleBulletManager ParticleBullets {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._particleBulletManager;
        }
    }

    public static ScoreManager ScoreManager {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._scoreManager;
        }
    }

    public static LevelManager Level {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._levelManager;
        }
    }

    public static LanguageManager Language {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._languageManager;
        }
    }

    public static CheatManager Cheat {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance._cheatManager;
        }
    }

    public static GameObject GameObject {
        get {
            if (_instance == null) {
                return null;
            }
            return _instance.gameObject;
        }
    }

    public static Tutorial Tutorial { get; set; }

    private void Awake() {
        _instance = this;
        if (Game.DevelopementBuild) {
            SRDebug.Init();
        }
        Application.runInBackground = false;
        DontDestroyOnLoad(gameObject);
        if (AspectDelta == 0) {
            AspectDelta = false ? 1f : (float)Screen.width / Screen.height / 1.33333f;
        }
    }

    public static float AspectDelta { get; private set; }
}