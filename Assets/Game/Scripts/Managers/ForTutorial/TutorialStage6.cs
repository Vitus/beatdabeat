﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;
using Random = UnityEngine.Random;

public class TutorialStage6 : TutorialStageBase
{
    [SerializeField] private string OneMoreTrick_Text;
    [SerializeField] private string Activate_Text;
    [SerializeField] private string ActivateMouse_Text;
    [SerializeField] private string Ready_Text;
    [SerializeField] private string ThankYou_Text;

    [SerializeField] private GameObject SkillBarHint;
    [SerializeField] private GameObject Heart;

    public IEnumerator Start()
    {
        Game.Player.Ship.ActiveSkill.Energy = Game.Player.Ship.ActiveSkill.MaxEnergy;
        Tutorial.ShowDialog(OneMoreTrick_Text, TutorialCharacterImage.Scientist, close: false);
        yield return StartCoroutine(WaitForUnscaledSeconds(2.5f));
        SkillBarHint.SetActive(true);
        yield return StartCoroutine(WaitDialog());
        SkillBarHint.SetActive(false);

        Tutorial.ShowDialog(ActivateMouse_Text, TutorialCharacterImage.Usual);

        yield return StartCoroutine(WaitDialog());

        GenerateBullets();
        yield return new WaitForSeconds(1f);
        Game.Player.Focus.gameObject.SetActive(true);

        Tutorial.ShowDialog(Ready_Text, TutorialCharacterImage.Rambo);
        yield return StartCoroutine(WaitDialog());

        yield return new WaitForSeconds(7f);

        Game.ParticleBullets.DestroyAllBullets();
        Game.Player.Focus.ForceSpeedUp();
        yield return new WaitForSeconds(1f);
        Game.Player.Focus.gameObject.SetActive(false);

        Tutorial.ShowDialog(ThankYou_Text, TutorialCharacterImage.Usual);
        yield return StartCoroutine(Tutorial.Dialog.WaitForType());
        Heart.SetActive(true);
        StartCoroutine("HeartBeat");
        yield return StartCoroutine(Tutorial.Dialog.WaitForHide());
        Heart.BroadcastMessage("Hide");
        StopCoroutine("HeartBeat");
        yield return StartCoroutine(Tutorial.Dialog.WaitForComplete());
        Tutorial.Main.GoToNextStage();
    }

    IEnumerator HeartBeat() {
        yield return StartCoroutine(WaitForUnscaledSeconds(0.4f));
        while (true) {
            Game.Sound.PlaySound(SoundType.TutorialHeartBeat);
            yield return StartCoroutine(WaitForUnscaledSeconds(0.8f));
        }
    }

    private void GenerateBullets()
    {
        Array bulletTypes = Enum.GetValues(typeof(ParticleBulletType));
        for (int i = 0; i < 90; i++)
        {
            ParticleBullet bullet = Game.ParticleBullets.SpawnUnusedParticle();
            bullet
                .SetPosition(Random.Range(-440f, 440f), Random.Range(150f, 270f))
                .SetBehaviour(ParticleBulletBehaviour.Cartesian)
                .SetVelocity(0, Random.Range(-40f, -70f))
                .SetType((ParticleBulletType)bulletTypes.GetValue(Random.Range(0, bulletTypes.Length)))
                .SetColor(Random.Range(0, 2) == 1 ? Color.red : Color.magenta);
        }

    }
}
