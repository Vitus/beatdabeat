﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class TutorialStage7 : TutorialStageBase
{
    [SerializeField] private GameObject SpeedupUpgradeButton;

    [SerializeField] private string WTF_Text;
    [SerializeField] private string NoJoke_Text;
    [SerializeField] private string GiveMeAMinute_Text;
    [SerializeField] private string ItsAVirus_Text;
    [SerializeField] private string YouCanSpeedUp_Text;
    [SerializeField] private string TakeChips_Text;
    [SerializeField] private string YouCanBuy_Text;
    [SerializeField] private string FeelThePower_Text;
    private UpgradeTimer _timer;
    private bool _upgradeComplete = false;

    IEnumerator Start () {
        _timer = UpgradeLevelManager.Instance.GetTimer(Game.Upgrade.CurrentShip);
        UpgradeLevelManager.Instance.PauseTimer(_timer);

        Tutorial.ShowDialog(WTF_Text, TutorialCharacterImage.Scared);
	    yield return StartCoroutine(WaitDialog());

        UpgradeLevelManager.Instance.UnpauseTimer(_timer);

        this.Tween(new TweenSettings<float>() {
            StartValue = 240,
            EndValue = 110,
            Duration = 0.5f,
            Easing = Ease.InQuad,
            IgnoreTimeScale = true,
            OnExecute = delegate(float value) {
                Camera.main.orthographicSize = value;
                Camera.main.transform.GetChild(0).GetComponent<Camera>().orthographicSize = value;
            }
            
        });
        Camera.main.transform.TweenPosition(new TweenSettings<Vector3>() {
            StartValue = Vector3.zero,
            EndValue = new Vector3(73,50,0),
            AffectAxis = AffectAxis.XY,
            Duration = 0.5f,
            Easing = Ease.InQuad,
            IgnoreTimeScale = true,
        });

        yield return StartCoroutine(WaitForUnscaledSeconds(5.5f));

        UpgradeLevelManager.Instance.PauseTimer(_timer);

        Camera.main.transform.position = Vector3.forward * Camera.main.transform.position.z;
        Camera.main.orthographicSize = 240;
        Camera.main.transform.GetChild(0).GetComponent<Camera>().orthographicSize = 240;


        Tutorial.ShowDialog(NoJoke_Text, TutorialCharacterImage.Akward, false);
	    yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(GiveMeAMinute_Text, TutorialCharacterImage.Scientist, false);
	    yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(ItsAVirus_Text, TutorialCharacterImage.Scientist, false);
	    yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(YouCanSpeedUp_Text, TutorialCharacterImage.Usual, false);
	    yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(TakeChips_Text, TutorialCharacterImage.Usual);
	    yield return StartCoroutine(WaitDialog());

        UpgradeLevelManager.Instance.UnpauseTimer(_timer);
        SpeedupUpgradeButton.SetActive(true);
    }

    void OnEnable() {
        UpgradeLevelManager.Instance.UpgradeComplete += InstanceOnUpgradeComplete;
    }

    void OnDisable() {
        UpgradeLevelManager.Instance.UpgradeComplete -= InstanceOnUpgradeComplete;
    }

    private void InstanceOnUpgradeComplete(UpgradeTimer timer) {
        Debug.Log("Auto Complete");
        StartCoroutine(OnSpeedupUpgradeButtonClick());
    }

    IEnumerator OnSpeedupUpgradeButtonClick()
    {
        if (_upgradeComplete) {
            yield break;
        }

        _upgradeComplete = true;

        Debug.Log("Click");
        SpeedupUpgradeButton.SetActive(false);
        UpgradeTimer timer = UpgradeLevelManager.Instance.GetTimer(Game.Upgrade.CurrentShip);
        if (timer != null) {
            UpgradeLevelManager.Instance.SpeedUpTimer(timer);
        }
        Debug.Log("Next Tutorial");
       
        
        Tutorial.ShowDialog(FeelThePower_Text, TutorialCharacterImage.Strong, close: false);
        yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(YouCanBuy_Text, TutorialCharacterImage.Gold);
        yield return StartCoroutine(WaitDialog());
    }

    void OnInappButtonClick() {
        Tutorial.Main.Complete();
    }
}
