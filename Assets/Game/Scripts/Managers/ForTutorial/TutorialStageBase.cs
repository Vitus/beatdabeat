﻿using UnityEngine;
using System.Collections;

public class TutorialStageBase : MonoBehaviour {

    protected IEnumerator WaitForUnscaledSeconds(float time)
    {
        var endTime = time + Time.unscaledTime;
        while (Time.unscaledTime < endTime)
        {
            yield return null;
        }
    }

    protected static IEnumerator WaitDialog()
    {
        return Tutorial.Dialog.WaitForComplete();
    } 
    protected static IEnumerator WaitType()
    {
        return Tutorial.Dialog.WaitForType();
    }
}
