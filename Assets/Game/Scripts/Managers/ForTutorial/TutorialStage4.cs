﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class TutorialStage4 : TutorialStageBase
{

    [SerializeField] private tk2dSprite BossImage;
    
    [SerializeField] private string FirstEnemy_Text;
    [SerializeField] private string NONONO_Text;
    [SerializeField] private string FinishHim_Text;
    [SerializeField] private string OhCoin_Text;
    [SerializeField] private string LetMeCollect_Text;

    [SerializeField] private PrepareShine Prepare;

    private IEnumerator Start()
    {
        Tutorial.ShowDialog(FirstEnemy_Text, TutorialCharacterImage.Rambo);
        yield return StartCoroutine(WaitDialog());
        
        Prepare.gameObject.SetActive(true);
        Prepare.OnComplete += delegate { StartCoroutine(AfterPrepare()); };
    }

    private IEnumerator AfterPrepare()
    {
        yield return StartCoroutine(AppearBoss());

        Tutorial.ShowDialog(NONONO_Text, TutorialCharacterImage.Scared);
        yield return StartCoroutine(WaitDialog());

        yield return StartCoroutine(HideBoss());

        Enemy enemy = EnemyAppear();
        yield return new WaitForSeconds(3.5f);

        Tutorial.ShowDialog(FinishHim_Text, TutorialCharacterImage.Strong);
        yield return StartCoroutine(WaitDialog());

        Tutorial.Main.IsShooting = true;
        yield return StartCoroutine(WaitEnemyKill(enemy));
        SlowDownCoins();
        yield return StartCoroutine(WaitForUnscaledSeconds(2f));

        Tutorial.Main.IsShooting = false;

        Tutorial.ShowDialog(OhCoin_Text, TutorialCharacterImage.Scared);
        yield return StartCoroutine(WaitDialog());

        yield return StartCoroutine(WaitForCoins());

        var coins = FindObjectsOfType<Coin>();
        if (coins.Length != 0)
        {
            Tutorial.ShowDialog(LetMeCollect_Text, TutorialCharacterImage.Akward, close: false);
            yield return StartCoroutine(WaitDialog());
        }

        Tutorial.Main.GoToNextStage();
    }

    private IEnumerator AppearBoss()
    {
        yield return new WaitForSeconds(1f);
        BossImage.transform.parent.gameObject.SetActive(true);
        BossImage.SetSprite("super_enemy_evil");
        BossImage.transform.parent.position = new Vector3(0, 350);
        BossImage.transform.parent.TweenPosition(new TweenSettings<Vector3>()
        {
            StartValue = BossImage.transform.position,
            EndValue = new Vector3(0, 130),
            Duration = 3.5f,
            Easing = Ease.Linear
        });
        yield return new WaitForSeconds(1.5f);
        Instantiate(Game.Level.OMG);
        yield return new WaitForSeconds(4f);
    }

    private IEnumerator HideBoss()
    {
        Game.Sound.PlaySound(SoundType.TutorialBossHide);
        BossImage.SetSprite("super_enemy_sad");
        BossImage.transform.parent.TweenPosition(new TweenSettings<Vector3>()
        {
            StartValue = BossImage.transform.position,
            EndValue = new Vector3(0, 350),
            Duration = 3.5f,
            Easing = Ease.Linear
        });

        yield return new WaitForSeconds(3.5f);
        BossImage.transform.parent.gameObject.SetActive(false);
    }

    private IEnumerator WaitForCoins()
    {
        float startTime = Time.time;
        var coins = FindObjectsOfType<Coin>();
        var coinCount = coins.Length;
        while (Time.time - startTime < 7f && coinCount != 0)
        {
            yield return null;
            coinCount = 0;
            foreach (Coin coin in coins)
            {
                if (coin != null)
                    coinCount++;
            }
        }
    }

    private void SlowDownCoins()
    {
        var coins = FindObjectsOfType<Coin>();
        foreach (var coin in coins)
        {
            coin.GetComponent<Rigidbody>().useGravity = false;
            coin.GetComponent<Rigidbody>().GetComponent<ConstantForce>().force = new Vector3(0, -30);
        }
    }

    private IEnumerator WaitEnemyKill(Enemy enemy)
    {
        while (enemy.IsAlive())
        {
            yield return null;
        }
    }

    private Enemy EnemyAppear()
    {
        Enemy enemy = Game.Level.SpawnEnemy();
        enemy
            .SetPosition(0, 350, 0)
            .SetImage(EnemyImage.Enemy2)
            .SetHealth(900)
            .SetKillAward(19);
        enemy.GetComponent<KillAward>().DisableBonuses();
        Destroy(enemy.GetComponent<DamageAward>());
        iTween.MoveTo(enemy.gameObject,
                      iTween.Hash(I.Position, new Vector3(0, 170), I.Time, 3.5f, I.EaseType, iTween.EaseType.linear));
        return enemy;
    }

}
