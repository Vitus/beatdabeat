﻿using System;
using UnityEngine;
using System.Collections;
using LevelParserV3;
using Random = UnityEngine.Random;

public class TutorialStage3 : TutorialStageBase
{
    [SerializeField] private GameObject BombCountLabel;
    [SerializeField] private LocalizeTextMesh BombUseHint;
    [SerializeField] private LocalizeTextMesh BombUseHintShadow;

    [SerializeField] private string Ready_Text;
    [SerializeField] private string Quick_Text;
    [SerializeField] private string QuickMouse_Text;
    [SerializeField] private string UseHint_Text;
    [SerializeField] private string UseHintMouse_Text;
    [SerializeField] private string HelpsALot_Text;

    private IEnumerator Start()
    {
        Tutorial.ShowDialog(Ready_Text, TutorialCharacterImage.Rambo);
        yield return StartCoroutine(WaitDialog());

        StartCoroutine("GenerateBulletSwarm");
        yield return new WaitForSeconds(2f);

        Tutorial.ShowDialog(QuickMouse_Text, TutorialCharacterImage.Scared);

        yield return StartCoroutine(WaitDialog());
        Game.Player.Ship.BombCount++;
        Game.Player.Focus.gameObject.SetActive(true);

        NEvent.AddListener<BombEvent>(Game.GameObject, UseBombEvent);
        NEvent.AddListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);

        yield return StartCoroutine(WaitForUnscaledSeconds(3));
        BombUseHint.LocalizeID = UseHintMouse_Text;
        BombUseHintShadow.LocalizeID = UseHintMouse_Text;
        BombUseHint.SetLocalization(UseHintMouse_Text);
        BombUseHintShadow.SetLocalization(UseHintMouse_Text);

        BombUseHint.gameObject.SetActive(true);
    }

    private void OnPlayerHit(PlayerHitEvent playerHitEvent)
    {
        Game.Player.Health.Replenish(1);
    }

    private void UseBombEvent(BombEvent bombEvent){
        StartCoroutine("AfterBomb");
    }

    private IEnumerator AfterBomb()
    {
        StopCoroutine("Start");
        BombUseHint.gameObject.SetActive(false);
        NEvent.RemoveListener<BombEvent>(Game.GameObject, UseBombEvent);
        NEvent.RemoveListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);
        StopCoroutine("GenerateBulletSwarm");
        yield return new WaitForSeconds(2f);
        Game.Player.Focus.gameObject.SetActive(false);

        BombCountLabel.SetActive(true);
        Tutorial.ShowDialog(HelpsALot_Text, TutorialCharacterImage.Akward, close: false);
        yield return StartCoroutine(WaitDialog());
        BombCountLabel.SetActive(false);
        Tutorial.Main.GoToNextStage();
    }

    private IEnumerator GenerateBulletSwarm()
    {
        Array bulletTypes = Enum.GetValues(typeof(ParticleBulletType));
        while (true)
        {
            Game.Sound.PlaySound(SoundType.TutorialBulletAppear);
            ParticleBullet bullet = Game.ParticleBullets.SpawnUnusedParticle();
            bullet
                .SetPosition(Random.Range(-350f, 350f) * LevelScript.ar, Random.Range(150f, 270f))
                .SetBehaviour(ParticleBulletBehaviour.Cartesian)
                .SetVelocity(0, Random.Range(-25f, -50f))
                .SetType((ParticleBulletType)bulletTypes.GetValue(Random.Range(0, bulletTypes.Length)))
                .SetColor(Random.Range(0, 2) == 1 ? Color.red : Color.magenta);
            yield return new WaitForSeconds(0.03f);
        }
    }
}
