﻿using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class TutorialStage5 : TutorialStageBase
{
    [SerializeField] private ShipSelector Ships;

    [SerializeField] private GameObject NextButton;
    [SerializeField] private GameObject DetailsButton;
    [SerializeField] private GameObject BackButton;
    [SerializeField] private GameObject BuyButton;
    [SerializeField] private GameObject UpgradeButton;

    [SerializeField] private string Intro_Text;
    [SerializeField] private string LetsFindShip_Text;
    [SerializeField] private string LooksCool_Text;
    [SerializeField] private string Description_Text;
    [SerializeField] private string LetsTry_Text;
    [SerializeField] private string NoMoney_Text;
    [SerializeField] private string MoneyVariants_Text;
    [SerializeField] private string Upgrade_Text;
    [SerializeField] private string RisingPower_Text;

    private ShipButton _firstShip;
    private ShipButton _secondShip;

    IEnumerator Start ()
	{
        Game.Money.Current += 100 * 1;
        HealthManager.Instance.CurrentShipHealth.FullReplenish();
        yield return new WaitForSeconds(2f);
        _firstShip = Ships.Buttons[0];
        _secondShip = Ships.Buttons[1];
        Tutorial.ShowDialog(LetsFindShip_Text, TutorialCharacterImage.Usual);
	    yield return StartCoroutine(WaitDialog());
        NextButton.SetActive(true);
	}

    IEnumerator OnNextButtonClick()
    {
        NextButton.SetActive(false);

        Ships.OnNextClick();
        yield return StartCoroutine(WaitForUnscaledSeconds(1f));
        Tutorial.ShowDialog(LooksCool_Text, TutorialCharacterImage.Scared, false);
        yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(Description_Text, TutorialCharacterImage.Scientist, false);
        yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(LetsTry_Text, TutorialCharacterImage.Usual);
        yield return StartCoroutine(WaitDialog());
        BuyButton.SetActive(true);
    }

    IEnumerator OnBuyButtonClick()
    {
        BuyButton.SetActive(false);

        Game.Sound.PlaySound(SoundType.TutorialNoMoney);
        Tutorial.ShowDialog(NoMoney_Text, TutorialCharacterImage.Akward, close: false);
        yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(MoneyVariants_Text, TutorialCharacterImage.Akward);
        yield return StartCoroutine(WaitDialog());

        Ships.OnPrevClick();
        yield return StartCoroutine(WaitForUnscaledSeconds(1f));

        Tutorial.ShowDialog(Upgrade_Text, TutorialCharacterImage.Scared);
        yield return StartCoroutine(WaitDialog());

        UpgradeButton.SetActive(true);
    }

    IEnumerator OnUpgradeButtonClick()
    {
        UpgradeButton.SetActive(false);
        Game.Sound.PlaySound(SoundType.TutorialUpgrade);
        _firstShip.UpgradePanel.HealthBar.OnUpgradeClick();

        if (false) {
            Tutorial.Main.GoToNextStage();
        } else {
            Tutorial.ShowDialog(RisingPower_Text, TutorialCharacterImage.Strong);
            yield return StartCoroutine(WaitDialog());
            Tutorial.Main.Complete();
        }
    }
}
