﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class TutorialStage1 : TutorialStageBase {
    [SerializeField] private GameObject Fade;
    [SerializeField] private GameObject Manual;
    [SerializeField] private GameObject[] Popups;
    [SerializeField] private float[] PopupsTime;
    [SerializeField] private GameObject Yes;
    [SerializeField] private GameObject No;

    [SerializeField] private string Greeting_Text;
    [SerializeField] private string GetReady_Text;
    [SerializeField] private string WasThatClear_Text;
    [SerializeField] private string OkByeBye_Text;

    private IEnumerator Start() {
        yield return StartCoroutine(WaitForUnscaledSeconds(2f));

        Tutorial.ShowDialog(Greeting_Text, TutorialCharacterImage.Usual, close: false);
        yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(GetReady_Text, TutorialCharacterImage.Scared, close: true);
        yield return StartCoroutine(WaitDialog());

        StartCoroutine(ShowManual());
    }

    private IEnumerator ShowManual() {
        Fade.SetActive(true);
        Fade.TweenAlpha(new TweenSettings<float>() {StartValue = 0, EndValue = 1, Duration = 0.3f});
        yield return StartCoroutine(WaitForUnscaledSeconds(0.3f));
        Fade.TweenAlpha(new TweenSettings<float>() {StartValue = 1, EndValue = 0, Duration = 0.3f});
        Manual.SetActive(true);
        yield return StartCoroutine(WaitForUnscaledSeconds(0.3f));
        Fade.SetActive(false);

        for (int i = 0; i < PopupsTime.Length; i++) {
            yield return StartCoroutine(WaitForUnscaledSeconds(PopupsTime[i]));
            Popups[i].SetActive(true);
        }
        yield return StartCoroutine(WaitForUnscaledSeconds(1f));
        
        Fade.SetActive(true);
        Fade.TweenAlpha(new TweenSettings<float>() { StartValue = 0, EndValue = 1, Duration = 0.3f });
        yield return StartCoroutine(WaitForUnscaledSeconds(0.3f));
        Fade.TweenAlpha(new TweenSettings<float>() { StartValue = 1, EndValue = 0, Duration = 0.3f });
        Manual.SetActive(false);
        yield return StartCoroutine(WaitForUnscaledSeconds(0.3f));
        Fade.SetActive(false);

        StartCoroutine(AskIfAllClear());
    }

    private IEnumerator AskIfAllClear() {
        Tutorial.ShowDialog(WasThatClear_Text, TutorialCharacterImage.Question, close: false, showTriangle: false);
        yield return StartCoroutine(WaitType());
        Yes.SetActive(true);
        No.SetActive(true);
    }

    public void SkipClick() {
        Yes.SetActive(false);
        No.SetActive(false);
        StartCoroutine(ByeBye());
    }

    private IEnumerator ByeBye() {
        Tutorial.ShowDialog(OkByeBye_Text, TutorialCharacterImage.Scientist);
        yield return StartCoroutine(WaitDialog());
        Tutorial.IsTutorialSkiped = true;
        Tutorial.Main.Complete();
    }

    public void ContinueClick() {
        Yes.SetActive(false);
        No.SetActive(false);
        Tutorial.Main.GoToNextStage();
    }
}