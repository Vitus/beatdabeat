﻿using System;
using UnityEngine;
using System.Collections;

public class PrepareShine : MonoBehaviour
{

    public event Action OnComplete;

    public tk2dSpriteAnimator Animator;
    public tk2dSpriteAnimator Shine1;
    public tk2dSpriteAnimator Shine2;
    public tk2dSpriteAnimator Shine3;

    IEnumerator Start()
    {
        while (Animator.CurrentFrame < 9)
        {
            yield return null;
        }
        Game.Sound.PlaySound(SoundType.TutorialHead);
        Shine1.gameObject.SetActive(true);
        Shine1.AnimationCompleted += (animator, clip) => Shine1.gameObject.SetActive(false);


        while (Animator.CurrentFrame < 29)
        {
            yield return null;
        }
        Game.Sound.PlaySound(SoundType.TutorialBelt);
        Shine2.gameObject.SetActive(true);
        Shine2.AnimationCompleted += (animator, clip) => Shine2.gameObject.SetActive(false);
    
        while (Animator.CurrentFrame < 55)
        {
            yield return null;
        }
        Game.Sound.PlaySound(SoundType.TutorialWeapon);
        Shine3.gameObject.SetActive(true);
        Shine3.AnimationCompleted += (animator, clip) => Shine3.gameObject.SetActive(false);

        while (Animator.CurrentFrame < Animator.CurrentClip.frames.Length - 1)
        {
            yield return null;
        }

        if (OnComplete != null)
            OnComplete();
        gameObject.SetActive(false);
    }
}
