﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class TutorialStage8 : TutorialStageBase
{
    [SerializeField] private GameObject ReplenishHealthButton;

    [SerializeField] private string NeedWait_Text;
    [SerializeField] private string WaitRisk_Text;
    [SerializeField] private string YouCanSpeedUp_Text;
    [SerializeField] private string BestClick_Text;
    [SerializeField] private string BuyShipRecomendation_Text;

    IEnumerator Start () {
        HealthManager.Instance.CurrentShipHealth.Current = 0;

        yield return new WaitForSeconds(2f);

        Tutorial.ShowDialog(NeedWait_Text, TutorialCharacterImage.Akward, false);
	    yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(WaitRisk_Text, TutorialCharacterImage.Rambo, false);
	    yield return StartCoroutine(WaitDialog());
        Tutorial.ShowDialog(YouCanSpeedUp_Text, TutorialCharacterImage.Gold);
	    yield return StartCoroutine(WaitDialog());

        Currency.Current += 10;
        
        ReplenishHealthButton.SetActive(true);
    }

    IEnumerator OnReplenishUpgradeButtonClick()
    {
        ReplenishHealthButton.SetActive(false);
        HealthManager.Instance.CurrentShipHealth.FullReplenish();
        Currency.Current -= HealthManager.Instance.CurrentShipHealth.FullReplenishCost;

        Tutorial.ShowDialog(BestClick_Text, TutorialCharacterImage.Strong);
        yield return StartCoroutine(WaitDialog());

        Currency.Current += 10;

        yield return StartCoroutine(WaitForUnscaledSeconds(2.5f));

        Tutorial.ShowDialog(BuyShipRecomendation_Text, TutorialCharacterImage.Scientist);
        yield return StartCoroutine(WaitDialog());

        Game.Tutorial.GoToNextStage();
    }
}
