﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;
using Random = UnityEngine.Random;

public class TutorialStage2 : TutorialStageBase
{
    public float RequiredMoveLength = 200f;
    public GameObject BulletHighlight;
    public GameObject HitboxHighlight;
    public GameObject Stars;

    [SerializeField] private string TapToMove_Text;
    [SerializeField] private string UseMouseToMove_Text;
    [SerializeField] private string StarAward_Text;
    [SerializeField] private string ItsABullet_Text;
    [SerializeField] private string Ups_Text;
    [SerializeField] private string ThatWasClose_Text;
    [SerializeField] private string Hitbox_Text;
    
    IEnumerator Start()
    {
        Tutorial.ShowDialog(UseMouseToMove_Text, TutorialCharacterImage.Usual);
        yield return StartCoroutine(WaitDialog());
        StartCoroutine(TryingToMove());
    }

    private IEnumerator TryingToMove()
    {
        float moveLength = 0f;
        Vector3 shipPrevPosition = Game.Player.Ship.transform.position;
        while (moveLength < RequiredMoveLength) {
            float delta = (Game.Player.Ship.transform.position - shipPrevPosition).magnitude;
            moveLength += delta;
            shipPrevPosition = Game.Player.Ship.transform.position;
            yield return null;
        }
        Stars.SetActive(true);
        yield return StartCoroutine(WaitForUnscaledSeconds(4));

        Tutorial.ShowDialog(StarAward_Text, TutorialCharacterImage.Gold);
        yield return StartCoroutine(WaitDialog());

        Stars.transform.TweenPosition(new TweenSettings<Vector3>()
        {
            StartValue = Stars.transform.position,
            EndValue = Stars.transform.position + Vector3.up * 30,
            Duration = 0.5f,
            IgnoreTimeScale = true,
        });

        foreach (Transform child in Stars.transform)
        {
            var sprite = child.GetComponent<tk2dSprite>();
            if (sprite != null && sprite.name != "Flash" && sprite.name != "Back")
            {
                sprite.TweenAlpha(new TweenSettings<float>()
                {
                    StartValue = 1f,
                    EndValue = 0f,
                    Duration = 0.5f,
                    IgnoreTimeScale = true,
                });
            }
            if (sprite.name == "Back")
            {
                sprite.SendMessage("Hide");
            }
        }
        yield return StartCoroutine(WaitForUnscaledSeconds(1));

        StartCoroutine("AvoidBullet");
    }

    private IEnumerator AvoidBullet()
    {
        var bullet = CreateBullet();
        Game.Player.Health.HitTestEnabled = false;
        yield return new WaitForSeconds(2.5f);

        BulletHighlight.SetActive(true);
        BulletHighlight.transform.position = bullet.TransformPosition - (Vector2) Camera.main.transform.parent.localPosition;
        BulletHighlight.transform.SetZLocalPosition(-10);

        Tutorial.ShowDialog(ItsABullet_Text, TutorialCharacterImage.Scared);
        yield return StartCoroutine(WaitDialog());

        BulletHighlight.SetActive(false);

        Game.Player.Health.HitTestEnabled = true;
        NEvent.AddListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);
        while (bullet.TransformPosition.y > -250)
        {
            yield return null;
        }
        NEvent.RemoveListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);
        StartCoroutine(ThatWasClose());
    }

    private ParticleBullet CreateBullet() {
        Game.Sound.PlaySound(SoundType.TutorialBulletAppear);
        var bullet = Game.ParticleBullets.SpawnUnusedParticle();
        bullet
            .SetPosition(Random.Range(-185f, 185f), 230)
            .SetBehaviour(ParticleBulletBehaviour.Cartesian)
            .SetVelocity(0, -100)
            .SetType(ParticleBulletType.Smallest)
            .SetColor(Color.red);
        return bullet;
    }

    private void OnPlayerHit(PlayerHitEvent playerHitEvent)
    {
        Game.Player.Health.Replenish(1);
        StopCoroutine("AvoidBullet");
        StopCoroutine("AfterHit");
        StartCoroutine("AfterHit");
    }

    private IEnumerator AfterHit()
    {
        yield return new WaitForSeconds(Game.Player.Health.ShieldTime + 0.5f);
        Tutorial.ShowDialog(Ups_Text, TutorialCharacterImage.Akward);
        yield return StartCoroutine(WaitDialog());

        var bullet = CreateBullet();
        while (bullet.TransformPosition.y > -250)
        {
            yield return null;
        }
        NEvent.RemoveListener<PlayerHitEvent>(Game.GameObject, OnPlayerHit);
        StartCoroutine(ThatWasClose());
    }

    private IEnumerator ThatWasClose()
    {
        Tutorial.ShowDialog(ThatWasClose_Text, TutorialCharacterImage.Usual, close: false);
        yield return StartCoroutine(WaitDialog());

        HitboxHighlight.SetActive(true);
        HitboxHighlight.transform.position = Game.Player.Ship.transform.position - Camera.main.transform.parent.localPosition + new Vector3(-1, 1);
        HitboxHighlight.transform.SetZLocalPosition(-10);
        Tutorial.ShowDialog(Hitbox_Text, TutorialCharacterImage.Usual, close: false);
        yield return StartCoroutine(WaitDialog());
        HitboxHighlight.SetActive(false);

        Tutorial.Main.GoToNextStage();
    }
}
