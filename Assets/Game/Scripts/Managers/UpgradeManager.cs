﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.ComponentModel;

public class UpgradeManager : MonoBehaviour {
    public string AllDifficultiesLocalizationID;
    public string CasualDifficultyLocalizationID;
    public string NormalDifficultyLocalizationID;
    public string HardDifficultyLocalizationID;
    public string BadassDifficultyLocalizationID;

    [SerializeField] private ShopShip[] _ships;
    public ShopShip ShipForStarterPack;

    public ShopShip[] Ships {
        get { return _ships; }
    }

    public List<ShopShip> UnlockedShips {
        get { return _ships.Where(shopShip => shopShip.IsUnlocked).ToList(); }
    }

    public int CurrentShipIndex {
        get {
            string currentShipName = PlayerPrefs.GetString("CurrentShip", "");
            for (int i = 0; i < _ships.Length; i++) {
                ShopShip shopShip = _ships[i];
                if (shopShip.ID == currentShipName) {
                    return i;
                }
            }
            return 0;
        }
        set { PlayerPrefs.SetString("CurrentShip", _ships[value].ID); }
    }

    public ShopShip CurrentShip {
        get {
            string currentShipName = PlayerPrefs.GetString("CurrentShip", "");
            foreach (ShopShip shopShip in _ships) {
                if (shopShip.ID == currentShipName) {
                    return shopShip;
                }
            }
            return _ships[0];
        }
        set { PlayerPrefs.SetString("CurrentShip", value.ID); }
    }

    public void TestUpgradeAllAchievement() {
        if (UnlockedShips.Count != Ships.Length) {
            return;
        }

        foreach (ShopShip shopShip in UnlockedShips) {
            if (!shopShip.FullyUpgraded())
                return;
        }
    }
}