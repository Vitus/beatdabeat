﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections;

public delegate void ShipUpgradeHandler(string upgradeID);

public class ShopShip : ScriptableObject {
    public const string HealthUpgradeID = "Health";
    public const string WeaponsUpgradeID = "Weapons";
    public const string BombsUpgradeID = "Bombs";

    public event ShipUpgradeHandler ShipUpgraded;


    [SerializeField] private string _ID;
    [SerializeField] private string _shipName;
    [SerializeField] private string _iconName;
    public bool LockedInDemo;
    public string ShootAnimation;
    public string SkillAnimation;
    public bool IsActiveSkill;
    [SerializeField] private LocalizedText _descriptions;
    [SerializeField] private string _descriptionLocalizationID;
    [SerializeField] private int _cost;
    [SerializeField] private int _difficultyRequirement;
    [SerializeField] private PlayerShip _prefab;
    [SerializeField] private int[] _upgradeCost;
    [SerializeField] private int[] _upgradeTime;
    private bool _freeUnlock;

    public string ID {
        get { return _ID; }
    }

    public string IconName {
        get { return _iconName; }
    }

    public string ShipName {
        get { return _shipName; }
    }

    public LocalizedText Description {
        get { return Game.Language.GetLocalization(_descriptionLocalizationID); }
    }

    public int Cost {
        get
        {
            if (_freeUnlock) {
                return 0;
            }

            return _cost;
        }
    }

    public int DifficultyRequirement 
        => _difficultyRequirement;

    public bool DifficultyConditionSatisfied {
        get {
            if (DifficultyRequirement == -1) {
                return Game.Level.IsAllDifficultiesBeated;
            }
            if (DifficultyRequirement == 0) {
                return true;
            }
            return Game.Level.IsDifficultyBeated(DifficultyRequirement);
        }
    }

    public PlayerShip Prefab 
        => _prefab;

    public bool IsUnlocked {
        get {
            if (_cost == 0 && DifficultyConditionSatisfied) {
                return true;
            }
            return (PlayerPrefs.GetInt("Ship." + _iconName, 0) > 0) && DifficultyConditionSatisfied;
        }
    }

    public int MaxUpgradeLevel 
        => _upgradeCost.Length - 1;

    public void CheatUnlock() {
        PlayerPrefs.SetInt("Ship." + _iconName, 1);
    }

    public void Unlock() {
        if (!IsUnlocked && Game.Money.Current >= Cost) {
            PlayerPrefs.SetInt("Ship." + _iconName, 1);
            Game.Money.Current -= Cost;
            Game.Upgrade.CurrentShip = this;
        }
        HealthManager.Instance.GetShipHealth(this).Current = HealthManager.Instance.GetShipHealth(this).Max;
        TestUnlockAchievement();
    }

    public void MakeUnlockFree() {
        _freeUnlock = true;
    }

    private void TestUnlockAchievement() {
    }

    private void TestUpgradeAchievement() {
        Game.Upgrade.TestUpgradeAllAchievement();
        if (FullyUpgraded()) {
        }
    }

    public bool FullyUpgraded() {
        return HealthLevel == MaxUpgradeLevel && BombsLevel == MaxUpgradeLevel && WeaponsLevel == MaxUpgradeLevel;
    }

    public int UpgradeCost(int level) {
        if (level < 0) {
            return -1;
        }
        if (level >= _upgradeCost.Length) {
            return -1;
        }
        return _upgradeCost[level]*1;
    }

    public int GetUpgradeLevel(string upgradeName) {
        return PlayerPrefs.GetInt("Ship." + _iconName + "." + upgradeName, 0);
    }

    public bool MakeUpgrade(string upgradeName) {
        int currentLevel = GetUpgradeLevel(upgradeName);
        if (currentLevel >= _upgradeCost.Length - 1) {
            return false;
        }

        int cost = _upgradeCost[currentLevel + 1]*1;
        if (Game.Money.Current >= cost) {
            Game.Money.Current -= cost;
            {
                CompleteUpgrade(upgradeName);
            }

            return true;
        }
        return false;
    }

    private void StartUpgradeTimer(string upgradeName) {
        UpgradeLevelManager.Instance.StartUpgrade(this, upgradeName);
    }

    public void CompleteUpgrade(string upgradeName) {
        PlayerPrefs.SetInt("Ship." + _iconName + "." + upgradeName, GetUpgradeLevel(upgradeName) + 1);
        TestUpgradeAchievement();
        NotifyShipUpgrade(upgradeName);
    }

    private void NotifyShipUpgrade(string upgradeName) {
        if (ShipUpgraded != null) {
            ShipUpgraded(upgradeName);
        }
    }


    public int HealthLevel {
        get { return GetUpgradeLevel(HealthUpgradeID); }
    }

    public bool UpgradeHealth() {
        return MakeUpgrade(HealthUpgradeID);
    }

    public int BombsLevel {
        get { return GetUpgradeLevel(BombsUpgradeID); }
    }

    public bool UpgradeBombs() {
        return MakeUpgrade(BombsUpgradeID);
    }

    public int WeaponsLevel {
        get { return GetUpgradeLevel(WeaponsUpgradeID); }
    }

    public bool UpgradeWeapons() {
        return MakeUpgrade(WeaponsUpgradeID);
    }

    public float UpgradeTime(string upgradeId) {
        int level = GetUpgradeLevel(upgradeId);
        return _upgradeTime[level + 1];
    }
}