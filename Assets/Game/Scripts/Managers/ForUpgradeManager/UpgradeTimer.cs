﻿//using System;
//using UnityEngine;
//using System.Collections;
//
//public class UpgradeTimer : MonoBehaviour {
//
//    public string UpgradeID { get; private set; }
//    public DateTime StartTime { get; private set; }
//    public TimeSpan Length { get; private set; }
//
//    public static UpgradeTimer LoadTimer(ShopShip ship) {
//        string serializedTimer = PlayerPrefs.GetString("UpgradeTimer." + ship.IconName, null);
//        if (serializedTimer != null) {
//            UpgradeTimer timer = new UpgradeTimer();
//            timer.Deserialize(serializedTimer);
//            return timer;
//        } else {
//            return null;
//        }
//    }
//
//    public static UpgradeTimer StartTimer(ShopShip ship, string upgradeID, TimeSpan length) {
//        UpgradeTimer timer = new UpgradeTimer();
//        timer.UpgradeID = upgradeID;
//        timer.StartTime = DateTime.Now;
//        timer.Length = length;
//        PlayerPrefs.SetString("UpgradeTimer." + ship.IconName, timer.Serialize());
//        string message = String.Format("Upgrade complete. {0} - {1}", ship.ShipName, upgradeID);
//        UM_NotificationController.Instance.ScheduleLocalNotification("Beat Da Beat", message, (int) (timer.Length).TotalSeconds);
//        return timer;
//    }
//
//    public bool IsCompleted() {
//        return (StartTime + Length <= DateTime.Now);
//    }
//
//    private string Serialize() {
//        return String.Format("{0}|{1}|{2}", UpgradeID, StartTime, Length);
//    }
//
//    private void Deserialize(string text) {
//        string[] values = text.Split('|');
//        UpgradeID = values[0];
//        StartTime = DateTime.Parse(values[1]);
//        Length = TimeSpan.Parse(values[2]);
//    }
//
//
//}
