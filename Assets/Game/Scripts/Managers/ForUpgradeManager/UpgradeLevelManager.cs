﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class UpgradeTimer {
    public string UpgradeID;
    public ShopShip Ship;
    public DateTime CompleteTime;
    public int LocalNotificationID;
    public bool Paused;

    public string LocalizationID {
        get {
            switch (UpgradeID) {
                case ShopShip.WeaponsUpgradeID:
                    return "shop_weapons";
                case ShopShip.HealthUpgradeID:
                    return "shop_health";
                case ShopShip.BombsUpgradeID:
                    return "shop_bombs";
            }
            return "";
        }
    }
}

public delegate void UpgradeCompleteDelegate(UpgradeTimer timer);

public class UpgradeLevelManager : Singleton<UpgradeLevelManager> {
    public const float SpeedUpMinuteCost = 0.5f;

    public event UpgradeCompleteDelegate UpgradeComplete;

    private Dictionary<ShopShip, UpgradeTimer> _timers = new Dictionary<ShopShip, UpgradeTimer>();
    private List<UpgradeTimer> _completedTimers = new List<UpgradeTimer>();

    private void Awake() {
        LoadAllTimers();
    }

    public bool IsShipUpgrading(ShopShip ship) {
        return _timers.ContainsKey(ship);
    }

    public void StartUpgrade(ShopShip ship, string upgradeId) {
        Assert.IsFalse(IsShipUpgrading(ship));
        UpgradeTimer timer = CreateTimer(ship, upgradeId, ship.UpgradeTime(upgradeId));
        _timers.Add(ship, timer);
    }

    public UpgradeTimer GetTimer(ShopShip ship) {
        UpgradeTimer timer;
        if (_timers.TryGetValue(ship, out timer)) {
            return timer;
        } else {
            return null;
        }
    }

    public int SpeedUpCost(UpgradeTimer timer) {
        return Mathf.CeilToInt((float) ((timer.CompleteTime - DateTime.Now).TotalMinutes*SpeedUpMinuteCost));
    }

    public void SpeedUpTimer(UpgradeTimer timer) {
        int speedUpCost = SpeedUpCost(timer);
        if (Currency.Current >= speedUpCost) {
            Currency.Current -= speedUpCost;
            int timeLeft = (int) (timer.CompleteTime - DateTime.Now).TotalMinutes;
            timer.CompleteTime = DateTime.Now;
        }
    }


    private void LoadAllTimers() {
        foreach (ShopShip ship in Game.Upgrade.Ships) {
            UpgradeTimer timer = LoadTimer(ship);
            if (timer != null) {
                UnpauseTimer(timer);
                _timers.Add(ship, timer);
            }
        }
    }

    private void Update() {
        if (Application.loadedLevelName == "MainMenu" || Application.loadedLevelName == "Shop") {
            _completedTimers.Clear();
            foreach (UpgradeTimer timer in _timers.Values) {
                if (timer.Paused) {
                    timer.CompleteTime = timer.CompleteTime.AddSeconds(Time.unscaledDeltaTime);
                    SaveTimer(timer);
                }

                if (timer.CompleteTime < DateTime.Now) {
                    _completedTimers.Add(timer);
                }
            }

            foreach (UpgradeTimer timer in _completedTimers) {
                _timers.Remove(timer.Ship);
                ClearTimer(timer.Ship);
                timer.Ship.CompleteUpgrade(timer.UpgradeID);
                NotifyUpgradeComplete(timer);
                ShopPopup.Show(timer.Ship.ShipName, Game.Language.GetLocalization(timer.LocalizationID), Game.Language.GetLocalization("shop_upgrade_complete"));
            }
        }
    }

    private void NotifyUpgradeComplete(UpgradeTimer timer) {
        if (UpgradeComplete != null) {
            UpgradeComplete(timer);
        }
    }

    private static UpgradeTimer LoadTimer(ShopShip ship) {
        string serializedTimer = PlayerPrefs.GetString("UpgradeTimer." + ship.IconName, "");
        if (!String.IsNullOrEmpty(serializedTimer)) {
            UpgradeTimer timer = new UpgradeTimer();
            string[] values = serializedTimer.Split('|');
            timer.Ship = ship;
            timer.UpgradeID = values[0];
            timer.CompleteTime = DateTime.Parse(values[1]);
            timer.LocalNotificationID = Convert.ToInt32(values[2]);
            return timer;
        } else {
            return null;
        }
    }

    private UpgradeTimer CreateTimer(ShopShip ship, string upgradeId, float upgradeTime) {
        UpgradeTimer timer = new UpgradeTimer();
        timer.Ship = ship;
        timer.UpgradeID = upgradeId;
        timer.CompleteTime = DateTime.Now + TimeSpan.FromSeconds(upgradeTime);
        SetNotification(timer);
        SaveTimer(timer);
        return timer;
    }

    private static void SaveTimer(UpgradeTimer timer) {
        string serializedTimer = String.Format("{0}|{1}|{2}", timer.UpgradeID, timer.CompleteTime, timer.LocalNotificationID);
        PlayerPrefs.SetString("UpgradeTimer." + timer.Ship.IconName, serializedTimer);
    }

    private static void SetNotification(UpgradeTimer timer) {
        SaveTimer(timer);
    }

    public void PauseTimer(UpgradeTimer timer) {
        timer.Paused = true;
    }

    public void UnpauseTimer(UpgradeTimer timer) {
        SetNotification(timer);
        timer.Paused = false;
    }


    private void ClearTimer(ShopShip ship) {
        PlayerPrefs.SetString("UpgradeTimer." + ship.IconName, "");
    }

    protected UpgradeLevelManager() {
    }

    [RuntimeInitializeOnLoadMethod]
    private static void Init() {
        CreateInstance();
    }
}