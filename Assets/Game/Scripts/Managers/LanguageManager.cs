﻿using System;
using UnityEngine;
using System.Collections.Generic;

public enum Language {
    auto = -1,
    eng = 0,
    rus = 1,
    ger = 2,
    fra = 3,
    chns = 4,
    chnt = 5,
    kor = 6,
    jap = 7,
    tur = 8,
    spa = 9,
    ita = 10,
    por = 11
}

public class LanguageManager : MonoBehaviour {
    [SerializeField] private Language _currentLanguage;
    [SerializeField] private tk2dFontData[] _fonts;

    private Language _previousLanguage;
    public event Action OnLanguageChanged;

    public TextAsset LocalizationFile;

    public List<string> LocalizationID;
    public List<LocalizedText> LocalizationText;
    private Dictionary<string, LocalizedText> Localizations = new Dictionary<string, LocalizedText>();

    public Language CurrentLanguage {
        get { return _currentLanguage; }
    }

    private void Awake() {
        SetCurrentLanguage();
        ParseFile();
        _previousLanguage = CurrentLanguage;
    }

    private void ParseFile() {
        string[] lines = LocalizationFile.text.Split('\n');
        foreach (string line in lines) {
            if (line[0] == '\t') {
                continue;
            }
            var localizedText = new LocalizedText();
            string[] languages = line.Replace("\\n", "\n").Split('\t');
            LocalizationID.Add(languages[0]);
            localizedText._eng = languages[1];
            localizedText._rus = GetText(languages, 2, localizedText._eng);
            localizedText._ger = GetText(languages, 3, localizedText._eng);
            localizedText._fra = GetText(languages, 4, localizedText._eng);
            localizedText._chns = GetText(languages, 5, localizedText._eng);
            localizedText._chnt = GetText(languages, 6, localizedText._eng);
            localizedText._kor = GetText(languages, 7, localizedText._eng);
            localizedText._jap = GetText(languages, 8, localizedText._eng);
            localizedText._tur = GetText(languages, 9, localizedText._eng);
            localizedText._spa = GetText(languages, 10, localizedText._eng);
            localizedText._ita = GetText(languages, 11, localizedText._eng);
            localizedText._por = GetText(languages, 12, localizedText._eng);
            LocalizationText.Add(localizedText);
            Localizations.Add(languages[0], localizedText);
        }
    }

    private string GetText(string[] languages, int num, string defaultText) {
        if (num < languages.Length) {
            return languages[num];
        }
        return defaultText;
    }


    private void Update() {
        if ((_previousLanguage != _currentLanguage) && OnLanguageChanged != null) {
            _previousLanguage = CurrentLanguage;
            OnLanguageChanged();
        }
    }

    public LocalizedText GetLocalization(string localizeID) {
        LocalizedText text;
        if (Localizations.TryGetValue(localizeID, out text)) {
            return text;
        }
        return null;
    }

    private void SetCurrentLanguage() {
        if (_currentLanguage != Language.auto) {
            return;
        }
        if (false) {
            _currentLanguage = Language.eng;
            return;
        }
        try
        {
            SetMobileLanguage();
        } catch (Exception e) {
            Debug.Log(e.Message);
            _currentLanguage = Language.eng;
        }
    }

    private void SetMobileLanguage() {
        switch (Application.systemLanguage) {
            case SystemLanguage.Russian:
            case SystemLanguage.Ukrainian:
            case SystemLanguage.Belarusian:
                _currentLanguage = Language.rus;
                return;
            case SystemLanguage.French:
                _currentLanguage = Language.fra;
                return;
            case SystemLanguage.German:
                _currentLanguage = Language.ger;
                return;
            case SystemLanguage.ChineseSimplified:
                _currentLanguage = Language.chns;
                return;
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseTraditional:
                _currentLanguage = Language.chnt;
                return;
            case SystemLanguage.Korean:
                _currentLanguage = Language.kor;
                return;
            case SystemLanguage.Japanese:
                _currentLanguage = Language.jap;
                return;
            case SystemLanguage.Turkish:
                _currentLanguage = Language.tur;
                return;
            case SystemLanguage.Spanish:
                _currentLanguage = Language.spa;
                return;
            case SystemLanguage.Italian:
                _currentLanguage = Language.ita;
                return;
            case SystemLanguage.Portuguese:
                _currentLanguage = Language.por;
                return;
            default:
                _currentLanguage = Language.eng;
                return;
        }
    }

    public string Suffix {
        get {
            switch (_currentLanguage) {
                case Language.rus:
                    return "_rus";
                case Language.ger:
                    return "_ger";
                case Language.fra:
                    return "_fra";
                case Language.chns:
                    return "_chn(sim)";
                case Language.chnt:
                    return "_chn(tra)";
                case Language.kor:
                    return "_kor";
                case Language.jap:
                    return "_jap";
                case Language.tur:
                    return "_tr";
                case Language.spa:
                    return "_spa";
                case Language.ita:
                    return "_ita";
                case Language.por:
                    return "_por";
                default:
                    return "";
            }
        }
    }

    public string FontSuffix {
        get {
            switch (_currentLanguage) {
                case Language.chns:
                case Language.chnt:
                    return "_chi";
                case Language.jap:
                    return "_jap";
                case Language.kor:
                    return "_kor";
                default:
                    return "";
            }
        }
    }

    public bool FontDoubleSize {
        get {
            return !(
                _currentLanguage == Language.kor ||
                _currentLanguage == Language.chns ||
                _currentLanguage == Language.chnt ||
                _currentLanguage == Language.jap
                );
        }
    }

    public bool RequiredUpperCase {
        get {
            return !(
                _currentLanguage == Language.kor ||
                _currentLanguage == Language.chns ||
                _currentLanguage == Language.chnt ||
                _currentLanguage == Language.jap
                );
        }
    }

    public tk2dFontData GetFont(string fontName) {
        foreach (var font in _fonts) {
            if (font.name == fontName) {
                return font;
            }
        }
        return null;
    }
}