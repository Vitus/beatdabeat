﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{
    [SerializeField] public AudioSource Source;
    [SerializeField] public int Count;
    [SerializeField] public float MaxVolume;
    [SerializeField] private MusicList _musicList;

    private bool _isLoaded;
    private bool _wasPlaying;
    private Dictionary<string, AudioClip> _preparedTracks = new Dictionary<string, AudioClip>();

    void Awake ()
	{
        Volume = MaxVolume;

        foreach (AudioClip clip in _musicList.Music) {
            _preparedTracks.Add(clip.name, clip);
        }
	}

    IEnumerator Start()
    {
        _wasPlaying = IsPlaying;
        while (true)
        {
            Pitch = UnityEngine.Time.timeScale;
            if (Time == 0f && _wasPlaying && !IsPlaying)
            {
                _wasPlaying = false;
                NEvent.Dispatch(Game.GameObject, new MusicCompleteEvent());
            }
            yield return null;
        }
    }

    public void Play(string clipName = "")
    {
        _wasPlaying = true;
        if (clipName == "")
            Source.Play();
        else {
            Source.loop = false;
            SetClip(clipName);
        }
    }

    private void SetClip(string clipName)
    {
        Source.clip = _preparedTracks[clipName];
        Source.Play();
    }
    
    public float Time {
        get { return Source.time; }
    }  
  
    public float Length {
        get { return Source.clip.length; }
    }

    public bool IsPlaying {
        get { return Source.isPlaying; }
    }
    
    public float Pitch {
        set {
            float newPitch = Mathf.Clamp(value, 0f, 100f);
            Source.pitch = newPitch;
        }
    }

    public float Volume {
        get { return Source.volume; }
        set { Source.volume = value; }
    }

    public void ChangeVolume(float volume) {
        Volume = volume;
    }

    public void Pause()
    {
        Source.Pause();
    }

    public void Stop()
    {
        _wasPlaying = false;
        Source.Stop();
    }

    public string GetClip() {
        return Source.clip.name;
    }

    public void Loop() {
        Source.loop = true;
    }
}
