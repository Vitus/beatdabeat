﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public class DifficultyManager : MonoBehaviour {
    public float MinMusicSpeed = 0.5f;
    public float MaxMusicSpeed = 2f;

    public float Speed {
        get { return _speed; }
        set {
            if (value < MinMusicSpeed) {
                _speed = MinMusicSpeed;
            } else if (value > MaxMusicSpeed) {
                _speed = MaxMusicSpeed;
            } else {
                _speed = value;
            }
        }
    }

    private void Awake() {
        Game.Difficulty = Current;
    }

    public List<string> NamesIDs;

    [SerializeField] private int _current;
    [SerializeField] private int _badassUnlockCost;
    private float _speed = 1;

    public int Current {
        get { return _current; }
        set {
            if (value != _current) {
                _current = value;
                Game.Difficulty = Current;
                NEvent.Dispatch(Game.GameObject, new DifficultyChangeEvent(_current));
            }
        }
    }

    public bool IsBadassUnlocked {
        get {
            if (true) {
                return true;
            } else {
                return PlayerPrefs.GetInt("Difficulty.Badass.IsUnlocked", 0) == 1;
            }
        }
        set {
            Assert.IsTrue(false, "Badass always unlocked in paid version");
            PlayerPrefs.SetInt("Difficulty.Badass.IsUnlocked", value ? 1 : 0);
        }
    }

    public int BadassUnlockCost {
        get { return _badassUnlockCost; }
    }
}