using System;
using UnityEngine;
using UnityEngine.Assertions;

public delegate void IntervalCompleteHandler(int intervalCount);

public class Timer {
    private const int LocalNotificationNotRegistered = -1;

    public event IntervalCompleteHandler IntervalComplete;

    private string _ID;
    private DateTime _intervalStartTime;
    private float _intervalLengthInSeconds;
    private int _localNotificationID = LocalNotificationNotRegistered;

    public float IntervalLengthInSeconds {
        get { return _intervalLengthInSeconds; }
        set {
            _intervalLengthInSeconds = value;
            PlayerPrefs.SetFloat("Timer." + _ID + ".IntervalLengthInSeconds", _intervalLengthInSeconds);
        }
    }

    public DateTime IntervalStartTime {
        get { return _intervalStartTime; }
        private set {
            _intervalStartTime = value;
            PlayerPrefs.SetString("Timer." + _ID + ".StartTime", value.ToString());
        }
    }

    public int LocalNotificationId {
        get { return _localNotificationID; }
        set {
            _localNotificationID = value;
            PlayerPrefs.SetInt("Timer." + _ID + ".IntervalCount", _localNotificationID);
        }
    }

    public TimeSpan IntervalTimeLeft {
        get { return TimeSpan.FromSeconds(IntervalLengthInSeconds) - (DateTime.Now - IntervalStartTime); }
    }

    public Timer(string ID) {
        _ID = ID;
        Load();
    }

    private void Load() {
        _localNotificationID = PlayerPrefs.GetInt("Timer." + _ID + ".NotificationID", LocalNotificationNotRegistered);
        _intervalLengthInSeconds = PlayerPrefs.GetFloat("Timer." + _ID + ".IntervalLengthInSeconds", 1f);
        _intervalStartTime =
            Convert.ToDateTime(PlayerPrefs.GetString("Timer." + _ID + ".StartTime", DateTime.Now.ToString()));
    }

    public void Update() {
        if (IntervalTimeLeft.TotalSeconds < 0) {
            TimeSpan elapsedTime = DateTime.Now - _intervalStartTime;
            int intervalsCount = (int) (elapsedTime.TotalSeconds / _intervalLengthInSeconds);
            IntervalStartTime = _intervalStartTime + TimeSpan.FromSeconds(_intervalLengthInSeconds * intervalsCount);
            NotifyIntervalComplete(intervalsCount);
        }
        if (LocalNotificationId != LocalNotificationNotRegistered) {
            ClearLocalNotification();
        }
    }

    private void NotifyIntervalComplete(int intervalsCount) {
        if (IntervalComplete != null) {
            IntervalComplete(intervalsCount);
        }
    }

    public void SetLocalNotification(int intervalCount, string title, string message) {
        if (LocalNotificationId != LocalNotificationNotRegistered) {
            ClearLocalNotification();
        }
    }

    private TimeSpan GetElapsedTimeSinceStart() {
        return (DateTime.Now - IntervalStartTime);
    }

    public void ClearLocalNotification() {
        if (LocalNotificationId != LocalNotificationNotRegistered) {
            LocalNotificationId = LocalNotificationNotRegistered;
        }
    }

    public void ResetStartTime() {
        IntervalStartTime = DateTime.Now;
    }
}