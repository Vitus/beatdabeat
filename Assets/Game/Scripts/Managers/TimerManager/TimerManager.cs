﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class TimerManager : Singleton<TimerManager> {
    protected TimerManager() {
    }

    [RuntimeInitializeOnLoadMethod]
    public static void Init() {
        CreateInstance();
    }

    private Dictionary<string, Timer> _timers = new Dictionary<string, Timer>();

    public static Timer GetTimer(string timerID) {
        Timer timer;
        if (Instance._timers.TryGetValue(timerID, out timer)) {
            return timer;
        }
        return null;
    }

    public static Timer CreateTimer(string timerID) {
        Assert.IsFalse(Instance._timers.ContainsKey(timerID));

        Timer timer = new Timer(timerID);
        Instance._timers.Add(timerID, timer);
        return timer;
    }

    private void Update() {
        foreach (Timer timer in _timers.Values) {
            timer.Update();
        }
    }
}