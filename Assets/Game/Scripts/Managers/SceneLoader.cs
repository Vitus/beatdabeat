﻿using MusicHell;

public class SceneLoader : ISceneLoader
{
    public void LoadScene(string levelName)
    {
        LevelLoader.LoadLevel(levelName);
    }
}