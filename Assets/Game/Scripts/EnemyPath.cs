﻿using UnityEngine;
using System.Collections;

public class EnemyPath : MonoBehaviour
{
    [SerializeField] private Transform[] Points;
    [SerializeField] private bool ShowGizmosAlways;
    
    public Vector3[] GetPath(Vector3 offset, bool flip)
    {
        Vector3[] resultPath = new Vector3[Points.Length];
        for (int i = 0; i < Points.Length; i++)
        {
            
            resultPath[i] = Points[i].position;
            resultPath[i].x = (flip ? -1 : 1) * resultPath[i].x;
            resultPath[i] += offset;
        }
        return resultPath;
    }

    void OnDrawGizmosSelected()
    {
        if (!ShowGizmosAlways)
        if (Points.Length > 1)
            iTween.DrawPathGizmos(Points);
    }

    void OnDrawGizmos()
    {
        if (ShowGizmosAlways)
            if (Points.Length > 1)
                iTween.DrawPathGizmos(Points);
    }

}
