﻿using UnityEngine;
using System.Collections;

public class BossHealthBar : MonoBehaviour 
{
    [SerializeField] private tk2dUIProgressBar Bar;
    [SerializeField] private tk2dTextMesh BossName;
    [SerializeField] private tk2dTextMesh BossNameShadow;

    public static BossHealthBar Main { get; private set; }

    public float Current
    {
        get { return _current; }
    }

    public float Max
    {
        get { return _max; }
    }

    private float _max = 1;
    private float _current = 1;
    private bool _visible;

    void Awake()
    {
        Main = this;
    }

    void Update()
    {
        Bar.Value = _current/_max;
    }

    public void SetName(string name)
    {
        BossName.text = name;
        BossNameShadow.text = name;
    }

    public void Hit(float power)
    {
        if (_current > 0)
        {
            _current -= power;
            if (_current <= 0)
            {
                _current = 0;
                Show(false);
                Game.Level.Win();
            }
        }
    }

    public void SetMax(float value)
    {
        _max = value < 1 ? 1 : value;
        _current = _max;
    }

    public void Show(bool show)
    {
        _visible = show;
        Bar.gameObject.SetActive(show);
        BossName.gameObject.SetActive(show);
        BossNameShadow.gameObject.SetActive(show);
    }

    public bool Visible
    {
        get { return _visible; }
    }
}