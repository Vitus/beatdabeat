﻿using UnityEngine;

[ExecuteInEditMode, RequireComponent(typeof (Camera))]
public class Perspective2DSortMode : MonoBehaviour {
    private void Awake() {
        GetComponent<Camera>().transparencySortMode = TransparencySortMode.Orthographic;
    }
}