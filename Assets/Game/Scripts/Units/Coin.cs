﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class Coin : MonoBehaviour
{
    [SerializeField] private MinMaxFloat ShineInterval;
    [SerializeField] private GameObject ShineEffect;
    [SerializeField] private float PositionOffset;

    private int _value;

    private void Awake()
    {
//        Invoke("Shine", ShineInterval.Random());
    }

    private void Shine()
    {
        var effect = Instantiate(ShineEffect) as GameObject;
        effect.transform.SetParent(transform, false);
        effect.transform.localPosition = (Vector3) H.RandomVector2(PositionOffset, false) - Vector3.forward;
        Destroy(effect, 0.5f);
        Invoke("Shine", ShineInterval.Random());
    }

    private void OnEnable()
    {
        NEvent.AddListener<GetCoinEvent>(gameObject, OnGetCoin);
    }

    private void OnDisable()
    {
        NEvent.RemoveListener<GetCoinEvent>(gameObject, OnGetCoin);
    }

    public void SetValue(int value)
    {
        _value = value;
        if (_value == 5)
        {
            GetComponent<FlashSprite>().ChangeAnimation(0);
        }
        else
        {
            GetComponent<FlashSprite>().ChangeAnimation(1);
        }
    }

    private void OnGetCoin(GetCoinEvent getCoinEvent)
    {
        Game.Sound.PlaySound(SoundType.GameCollectCoin);
        if (!Game.Level.IsPractice)
        {
            Game.Money.Current += _value * Game.Difficulty;
        }
    }
}