﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class DamageAward : MonoBehaviour {
    [SerializeField] private Coin CoinPrefab;
    [SerializeField] private GameObject[] BonusPrefabs;
    [SerializeField] private float BonusPrefabChance;

    private int _awardLeft;
    private int _damageForCoin;

    private float _currentDamage;
    private int _nextDamageForCoin;

    private void OnEnable() {
        NEvent.AddListener<HitEvent>(gameObject, OnHit);
    }

    private void OnDisable() {
        NEvent.RemoveListener<HitEvent>(gameObject, OnHit);
    }

    private void OnHit(HitEvent e) {
        if (_awardLeft <= 0)
            return;

        if (Game.Level.IsPractice)
            return;

        _currentDamage += e.Power;
        if (_nextDamageForCoin > 0) {
            while (_currentDamage > _nextDamageForCoin && _awardLeft > 0) {
                Coin coin = Instantiate(CoinPrefab, transform.position + (Vector3) H.RandomVector2(50, false), Quaternion.identity) as Coin;
                coin.SetValue(1);
                _awardLeft -= 1;

                _currentDamage -= _nextDamageForCoin;
                _nextDamageForCoin = _damageForCoin;
//                if (Random.Range(0f, 1f) < BonusPrefabChance) {
//                    Instantiate(H.GetRandomElement<GameObject>(BonusPrefabs), transform.position + Vector3.back*2, Quaternion.identity);
//                }
            }
        }
    }

    public void SetAward(int award, float lifeTime)
    {
        _awardLeft = award;
        float totalDamage = lifeTime*1000;
        _damageForCoin = (int) (totalDamage/award);
        _nextDamageForCoin = _damageForCoin;
    }
}