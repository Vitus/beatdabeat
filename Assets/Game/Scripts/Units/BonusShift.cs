﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class BonusShift : MonoBehaviour
{
    void OnEnable()
    {
        NEvent.AddListener<GetBonusEvent>(gameObject, OnGetBonus);
    }
    void OnDisable()
    {
        NEvent.RemoveListener<GetBonusEvent>(gameObject, OnGetBonus);
    }

    private void OnGetBonus(GetBonusEvent e)
    {
        Game.Sound.PlaySound(SoundType.GamePowerupShift);
    }
}
