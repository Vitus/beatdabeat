﻿using UnityEngine;
using System.Collections;

public class KillAward : MonoBehaviour {
    [SerializeField]
    private Coin CoinPrefab;
    private int Award;
    [SerializeField]
    private GameObject[] BonusPrefabs;
    [SerializeField]
    private float BonusPrefabChance;

    void OnEnable()
    {
        NEvent.AddListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
        if (DonutEffects.DoubleUltraCoin) {
            BonusPrefabs = new[] {BonusPrefabs[0], BonusPrefabs[1], BonusPrefabs[2], BonusPrefabs[2]};
        }
        BonusPrefabChance += BonusPrefabChance/3f;
    }

    void OnDisable()
    {
        NEvent.RemoveListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
    }

    private void OnOutOfHealth(OutOfHealthEvent outOfHealthEvent)
    {
        if (!Game.Level.IsPractice)
        {
            int awardLeft = Award;
            while (awardLeft >= 5)
            {
                Coin coin =
                    Instantiate(CoinPrefab, transform.position + (Vector3) H.RandomVector2(50, false),
                        Quaternion.identity) as Coin;
                coin.SetValue(5);
                awardLeft -= 5;
            }
            while (awardLeft >= 1)
            {
                Coin coin =
                    Instantiate(CoinPrefab, transform.position + (Vector3) H.RandomVector2(50, false),
                        Quaternion.identity) as Coin;
                coin.SetValue(1);
                awardLeft -= 1;
            }
        }

        if (Random.Range(0f, 1f) < BonusPrefabChance)
        {
            var randomBonus = H.GetRandomElement<GameObject>(BonusPrefabs);
            if (!Game.Level.IsPractice || randomBonus.name != "BonusCoin")
            {
                Instantiate(randomBonus, transform.position + Vector3.back*2, Quaternion.identity);
            }
        }
    }

    public void DisableBonuses() {
        BonusPrefabChance = -1f;
    }

    public void SetAward(int award)
    {
        Award = award;
    }
}
