using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int Max;

    private int _current = -1;
    public int Current
    {
        get { return _current; }
        set
        {
            if (_current != 0)
            {
                _current = Mathf.Clamp(value, 0, Max);
                if (_current == 0)
                {
                    NEvent.Dispatch(gameObject, new OutOfHealthEvent());
                }
            }
        }
    }

    public bool AsBoss { get; set; }

    public void SetMax(int max, bool setCurrent = false)
    {
        Max = max;
        if (setCurrent)
            _current = Max;
    }

    void OnEnable()
    {
        NEvent.AddListener<HitEvent>(gameObject, OnHit);
        NEvent.AddListener<HealEvent>(gameObject, OnHeal);
    }

    void OnDestroy()
    {
        NEvent.RemoveListener<HitEvent>(gameObject, OnHit);
        NEvent.RemoveListener<HealEvent>(gameObject, OnHeal);
    }

    void Start()
    {
        if (_current == -1)
        {
            SetMax(Max, true);
        }
    }

    void OnHit(HitEvent e)
    {
        if (!enabled) 
            return;
        if (AsBoss)
            BossHealthBar.Main.Hit(e.Power);
        else
            Current -= e.Power;
    }

    void OnHeal(HealEvent e)
    {
        if (enabled)
            Current += (e).Power;
    }
}