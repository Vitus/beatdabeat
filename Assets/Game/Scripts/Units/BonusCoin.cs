﻿using System;
using UnityEngine;
using System.Collections;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class BonusCoin : MonoBehaviour {
    public int Value;
    public GameObject CollectEffect;

    void OnEnable()
    {
        NEvent.AddListener<GetBonusEvent>(gameObject, OnGetBonus);
    }
    void OnDisable()
    {
        NEvent.RemoveListener<GetBonusEvent>(gameObject, OnGetBonus);
    }

    private void OnGetBonus(GetBonusEvent e)
    {
        Game.Money.Current += Value*Game.Difficulty;
        Game.Sound.PlaySound(SoundType.GameCollectCoin);
        Instantiate(CollectEffect, transform.position + Vector3.back * 10, Quaternion.identity);

    }
}
