﻿using UnityEngine;
using System.Collections;

public class DestroyIfOutOfScreen : MonoBehaviour
{
	void Update ()
	{
	    float halfHeight = Camera.main.orthographicSize;
	    float halfWidth = halfHeight*Camera.main.aspect;
	    if (transform.position.x > halfWidth || transform.position.x < -halfWidth || transform.position.y > halfHeight || transform.position.y < -halfHeight)
	    {
	        Destroy(gameObject);
	    }
	}
}
