﻿using UnityEngine;
using System.Collections;

public class BackgroundObjectMover : MonoBehaviour {

    [SerializeField] private MinMaxFloat ObjectFlyTime;
    
    private static float _speedMultiplier = 1;

    private IEnumerator MoveObject(GameObject o, float flyTime) {
        float elapsedTime = 0;
        Vector3 startPositon = o.transform.position;
        while (elapsedTime < flyTime && o != null) {
            elapsedTime += Time.deltaTime*_speedMultiplier;
            o.transform.SetYPosition(Mathf.Lerp(startPositon.y, startPositon.y - 1000, elapsedTime/flyTime));
            yield return null;
        }
        Destroy(o);
    }

    private void Start() {
        float flyTime = ObjectFlyTime.Random();
        StartCoroutine(MoveObject(gameObject, flyTime));
    }

    private void OnEnable() {
        NEvent.AddListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    }

    private void OnDisable() {
        NEvent.RemoveListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    }

    private void OnChangeSpeed(StarSpeedEvent speedEvent) {
        _speedMultiplier = speedEvent.Speed;
    }
}