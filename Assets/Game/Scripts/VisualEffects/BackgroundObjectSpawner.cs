﻿using UnityEngine;
using System.Collections;

public class BackgroundObjectSpawner : MonoBehaviour {
    [SerializeField] private tk2dBaseSprite[] ObjectPrefabs;
    [SerializeField] private MinMaxFloat DistanceBetweenObjects;
    [SerializeField] private MinMaxFloat XPosition;
    private tk2dBaseSprite _currentBackgroundObject;
    private float _speedMultiplier = 1;

    private IEnumerator Start() {
        XPosition.Min += (Camera.main.aspect/1.33f - 1)*334 * Mathf.Sign(XPosition.Min);
        XPosition.Max += (Camera.main.aspect/1.33f - 1)*334 * Mathf.Sign(XPosition.Max);
        if (ObjectPrefabs.Length == 0)
            yield break;
        float nextSpawnTime = Time.time;
        while (true) {
            if (nextSpawnTime < Time.time) {
                SpawnObject();
                nextSpawnTime += DistanceBetweenObjects.Random() / _speedMultiplier;
            }
            yield return null;
        }
    }

    private void SpawnObject() {
        Spawn();
        RandomFlip();
    }

    private void RandomFlip() {
        bool flip = Random.Range(0, 2) == 1;
        _currentBackgroundObject.transform.SetXPosition(_currentBackgroundObject.transform.position.x*(flip ? 1 : -1));
        _currentBackgroundObject.transform.SetXLocalScale(flip ? 1 : -1);
    }

    private void Spawn() {
        tk2dBaseSprite randomPrefab = ObjectPrefabs[Random.Range(0, ObjectPrefabs.Length)];
        _currentBackgroundObject = (tk2dBaseSprite)Instantiate(randomPrefab, RandomPosition() + Vector3.forward * randomPrefab.transform.position.z, Quaternion.identity);
        
        _currentBackgroundObject.transform.parent = this.transform;
    }

    private Vector3 RandomPosition() {
        Vector3 result = new Vector3(XPosition.Random(), 500, transform.position.z);
        return result;
    }

    private void OnEnable()
    {
        NEvent.AddListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    }

    private void OnDisable()
    {
        NEvent.RemoveListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    }

    private void OnChangeSpeed(StarSpeedEvent speedEvent)
    {
        _speedMultiplier = speedEvent.Speed;
    }

}
