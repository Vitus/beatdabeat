﻿using UnityEngine;
using System.Collections;

public class BlinkOnHit : MonoBehaviour
{
    public GameObject BlinkablePart;
    private float _currentWhite;

    void OnEnable()
    {
        NEvent.AddListener<HitEvent>(gameObject, OnHit);
    }

    void OnDisable()
    {
        NEvent.RemoveListener<HitEvent>(gameObject, OnHit);
    }

    void Start()
    {
        if (BlinkablePart == null)
            BlinkablePart = gameObject;
        if (BlinkablePart.gameObject.GetComponent<tk2dSpriteAnimator>() != null)
        {
            this.enabled = false;
            return;
        }
        BlinkablePart.GetComponent<Renderer>().material.shader = Shader.Find("My/ColorMultiplier");
        BlinkablePart.SendMessage("White", value: 0f);
    }

    void Update()
    {
        if (_currentWhite != 0f)
        {
            BlinkablePart.SendMessage("White", value: _currentWhite);
            _currentWhite -= Time.deltaTime / 0.3f;
            _currentWhite = Mathf.Clamp(_currentWhite, 0f, 1f);
        }
    }

    private void OnHit(HitEvent e)
    {
        _currentWhite = 0.6f;
//        iTween.StopByName(BlinkablePart, "Blink");
//        iTween.ValueTo(BlinkablePart, iTween.Hash(I.Name, "Blink", I.From, 1f, I.To, 0f, I.Time, 0.3f, I.OnUpdate, "White"));
    }
}
