﻿using UnityEngine;
using System.Collections;

public class ShakeScaleOnHit : MonoBehaviour
{
    private Vector3 _originalScale;
    private float _currentShake;
    
    void OnEnable()
    {
        NEvent.AddListener<HitEvent>(gameObject, OnHit);
    }

    void OnDisable()
    {
        NEvent.RemoveListener<HitEvent>(gameObject, OnHit);
    }
    void Update()
    {
        if (_currentShake != 0f)
        {
            transform.localScale = new Vector3(_originalScale.x * (1 + Random.Range(-0.3f, 0.3f) * _currentShake),
                                               _originalScale.x * (1 + Random.Range(-0.3f, 0.3f) * _currentShake),
                                               _originalScale.x * (1 + Random.Range(-0.3f, 0.3f) * _currentShake));
            _currentShake = Mathf.MoveTowards(_currentShake, 0f, Time.deltaTime / 0.3f);
            if (_currentShake == 0f)
                transform.localScale = _originalScale;
        }
    }

    private void OnHit(HitEvent e)
    {
        if (_currentShake == 0f)
            _originalScale = transform.localScale;
        _currentShake = 1f;
    }

}
