﻿using UnityEngine;
using System.Collections;

public class RippleEffect : MonoBehaviour {

    [SerializeField] public GameObject Ripple;

    void Start() {
        Ripple.SetActive(PostEffectDisabler.Instance.DistortionEnabled);
    }


}
