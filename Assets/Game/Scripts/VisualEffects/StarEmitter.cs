﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class StarEmitter : MonoBehaviour
{
    [SerializeField] private float ParalaxFactor;
    // private ParticleEmitter _emitter;
    // private float _defaultEmission;
    // private float _defaultSpeed;
    // private float _defaultEnergy;
    //
    // void Awake()
    // {
    //     _emitter = GetComponent<ParticleEmitter>();
    //     _defaultEmission = _emitter.minEmission;
    //     _defaultSpeed = _emitter.worldVelocity.y;
    //     _defaultEnergy = _emitter.minEnergy;
    //     OnChangeSpeed(new StarSpeedEvent(1, null, 0));
    //     for (int i = 0; i < _defaultEnergy; i++)
    //     {
    //         _emitter.Simulate(1f);
    //     }
    // }
    //
    // void OnEnable()
    // {
    //     NEvent.AddListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    // }
    // void OnDisable()
    // {
    //     NEvent.RemoveListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    // }
    //
    // void OnChangeSpeed(StarSpeedEvent starSpeedEvent)
    // {
    //     float newSpeed = _defaultSpeed * starSpeedEvent.Speed;
    //     float newEnergy = (_defaultSpeed / newSpeed) * _defaultEnergy;
    //     float newEmission = (newSpeed / _defaultSpeed) * _defaultEmission;
    //
    //     Particle[] particles = _emitter.particles;
    //     for (int i = 0; i < particles.Length; i++)
    //     {
    //         float lengthRemaining = particles[i].velocity.y * particles[i].energy;
    //         particles[i].velocity = new Vector3(particles[i].velocity.x, newSpeed, particles[i].velocity.y);
    //         particles[i].startEnergy = newEnergy;
    //         particles[i].energy = lengthRemaining / particles[i].velocity.y;
    //     }
    //     _emitter.particles = particles;
    //     
    //     _emitter.minEmission = newEmission;
    //     _emitter.maxEmission = newEmission;
    //     _emitter.worldVelocity = new Vector3(_emitter.worldVelocity.x, newSpeed, _emitter.worldVelocity.z);
    //     _emitter.minEnergy = newEnergy;
    //     _emitter.maxEnergy = newEnergy;
    // }
    //
    // void Update()
    // {
    //     transform.SetXPosition(-Game.Player.Ship.transform.position.x * ParalaxFactor);
    // }
}