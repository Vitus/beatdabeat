﻿using UnityEngine;
using System.Collections;

public class RandomAnimationAtStartup : MonoBehaviour
{
    [SerializeField] private string[] Animations;
    [SerializeField] private bool DestroyOnFinish;
    
    private tk2dSpriteAnimator _animation;

    void Start()
    {
        _animation = GetComponent<tk2dSpriteAnimator>();
        if (_animation != null)
            _animation.Play(H.GetRandomElement<string>(Animations));
    }

    void Update()
    {
        if (DestroyOnFinish)
            if (!_animation.Playing)
                Destroy(gameObject);
    }
}
