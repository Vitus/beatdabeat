﻿using UnityEngine;
using System.Collections;

public class RippleExplode : MonoBehaviour {

    public float AnimationTime;
    public float MaxSize;
    public AnimationCurve SizeCurve;
    public AnimationCurve DistortionCurve;
    public Texture2D[] NormalMaps;

    private float _elapsedTime;
    private Material _glassMaterial;

    void Start() {
        _elapsedTime = 0;
        _glassMaterial = GetComponent<Renderer>().material;
        _glassMaterial.SetTexture("_BumpMap", NormalMaps[Random.Range(0, NormalMaps.Length)]);
    }

    void Update() {
        _elapsedTime += Time.deltaTime;
        _glassMaterial.SetFloat("_BumpAmt", DistortionCurve.Evaluate(_elapsedTime / AnimationTime) * 128);
        transform.localScale = Vector3.one * SizeCurve.Evaluate(_elapsedTime / AnimationTime) * MaxSize;
        if (_elapsedTime > AnimationTime)
            Destroy(gameObject);
    }

}
