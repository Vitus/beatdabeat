﻿using UnityEngine;
using System.Collections;

public class MainMenuBackground : MonoBehaviour
{
    [SerializeField] private tk2dSpriteAnimator Animator;
    [SerializeField] private MinMaxFloat TimeBetweenAnimations;
    
    [SerializeField] private GameObject[] ShipPrefabs;
    [SerializeField] private MinMaxFloat TimeBetweenShips;
    [SerializeField] private MinMaxFloat ShipFlyTime;
    
    [SerializeField] private GameObject FallingStarPrefab;
    [SerializeField] private MinMaxFloat TimeBetweenFallingStar;
    [SerializeField] private MinMaxFloat FallingStarFlyTime;

	IEnumerator Start ()
	{
	    StartCoroutine(Ships());
	    StartCoroutine(FallingStars());
	    while (true)
	    {
	        if (!Animator.Playing)
	        {
	            yield return new WaitForSeconds(TimeBetweenAnimations.Random());
	            Animator.Play("MainMenuAnim" + Random.Range(1, 3));
	        }
	        yield return null;
	    }
    }

    private IEnumerator Ships()
    {
        while (true)
        {
            yield return new WaitForSeconds(TimeBetweenShips.Random());
            GameObject ship = Instantiate(H.GetRandomElement<GameObject>(ShipPrefabs)) as GameObject;
            float flipX = Random.Range(0, 2) == 0 ? -1 : 1;
            ship.transform.localScale = new Vector3(flipX, 1, 1);
            ship.transform.parent = transform;
            ship.transform.localPosition = new Vector3(1000 * flipX, Random.Range(-185, -100), -6.5f);
            float flyTime = ShipFlyTime.Random();
            iTween.MoveTo(ship, iTween.Hash(I.Position, Vector3.left * 1000 * flipX + Vector3.up * ship.transform.position.y, I.Time, flyTime, I.EaseType, iTween.EaseType.linear));
            Destroy(ship, flyTime);
        }
    }

    private IEnumerator FallingStars()
    {
        while (true)
        {
            yield return new WaitForSeconds(TimeBetweenFallingStar.Random());
            GameObject star = Instantiate(FallingStarPrefab) as GameObject;
            star.transform.parent = transform;
            star.transform.localPosition = new Vector3(Random.Range(-1700f, 170f), 750, -6.5f);
            float flyTime = FallingStarFlyTime.Random();
            iTween.MoveTo(star, iTween.Hash(I.X, star.transform.position.x + 960, I.Y, star.transform.position.y - 960, I.Time, flyTime, I.EaseType, iTween.EaseType.linear));
            Destroy(star, flyTime);
        }
    }
}
