﻿using System.Linq;
using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class PostEffectDisabler : MonoBehaviour {
    public static PostEffectDisabler Instance { get; private set; }

    [SerializeField] private bool _vignetteEnabled;
    public bool VignetteEnabled {
        get { return _vignetteEnabled; }
        set {
            _vignetteEnabled = value; 
            PlayerPrefs.SetInt("Effects.Vignette", value ? 1 : 0);
            UpdateEffects();
        }
    }

    [SerializeField] private bool _motionBlurEnabled;
    public bool MotionBlurEnabled {
        get { return _motionBlurEnabled; }
        set {
            _motionBlurEnabled = value;
            PlayerPrefs.SetInt("Effects.MotionBlur", value ? 1 : 0);
            UpdateEffects();
        }
    }

    [SerializeField] private bool _distortionEnabled;
    public bool DistortionEnabled {
        get { return _distortionEnabled; }
        set {
            _distortionEnabled = value;
            PlayerPrefs.SetInt("Effects.Distortion", value ? 1 : 0);
            UpdateEffects();
        }
    }

    [SerializeField] private bool _bloomEnabled;
    public bool BloomEnabled {
        get { return _bloomEnabled; }
        set {
            _bloomEnabled = value;
            PlayerPrefs.SetInt("Effects.Bloom", value ? 1 : 0);
            UpdateEffects();
        }
    }

    [SerializeField] private bool _colorBalanceEnabled;
    public bool ColorBalanceEnabled {
        get { return _colorBalanceEnabled; }
        set {
            _colorBalanceEnabled = value;
            PlayerPrefs.SetInt("Effects.ColorBalance", value ? 1 : 0);
            UpdateEffects();
        }
    }

    [SerializeField] private bool _retroEnabled;
    public bool RetroEnabled {
        get { return _retroEnabled; }
        set {
            _retroEnabled = value;
            PlayerPrefs.SetInt("Effects.Retro", value ? 1 : 0);
            UpdateEffects();
        }
    }

    private void Awake() {
        Instance = this;
        _vignetteEnabled = PlayerPrefs.GetInt("Effects.Vignette", _vignetteEnabled ? 1 : 0) == 1;
        _motionBlurEnabled = PlayerPrefs.GetInt("Effects.MotionBlur", _motionBlurEnabled ? 1 : 0) == 1;
        _distortionEnabled = PlayerPrefs.GetInt("Effects.Distortion", _distortionEnabled ? 1 : 0) == 1;
        _bloomEnabled = PlayerPrefs.GetInt("Effects.Bloom", _bloomEnabled ? 1 : 0) == 1;
        _colorBalanceEnabled = PlayerPrefs.GetInt("Effects.ColorBalance", _colorBalanceEnabled ? 1 : 0) == 1;
        _retroEnabled = PlayerPrefs.GetInt("Effects.Retro", _retroEnabled ? 1 : 0) == 1;
    }

    private void Start() {
        OnLevelWasLoaded();
    }

    private void OnLevelWasLoaded() {
        UpdateEffects();
    }

    private void UpdateEffects() {
        ColorCorrectionCurves colorCorrectionCurves = FindObjectOfType<ColorCorrectionCurves>();
        if (colorCorrectionCurves != null) {
            colorCorrectionCurves.enabled = _colorBalanceEnabled;
        }

        BloomOptimized bloomOptimized = FindObjectOfType<BloomOptimized>();
        if (bloomOptimized != null) {
            bloomOptimized.enabled = _bloomEnabled;
        }

        MotionBlur motionBlur = FindObjectOfType<MotionBlur>();
        if (motionBlur != null) {
            motionBlur.enabled = _motionBlurEnabled;
        }

        VignetteAndChromaticAberration vignetteAndChromaticAberration = FindObjectOfType<VignetteAndChromaticAberration>();
        if (vignetteAndChromaticAberration != null) {
            vignetteAndChromaticAberration.enabled = _vignetteEnabled;
        }
    }
}