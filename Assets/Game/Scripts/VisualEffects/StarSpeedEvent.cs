﻿using UnityEngine;

public class StarSpeedEvent : NelliEvent
{
    public float Speed { get; private set; }
    public Color? FlashColor { get; private set; }
    public float Length { get; set; }

    public StarSpeedEvent(float speed, Color? flashColor, float length)
    {
        Speed = speed;
        FlashColor = flashColor;
        Length = length;
    }
}