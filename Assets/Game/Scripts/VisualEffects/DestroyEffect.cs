﻿using UnityEngine;
using System.Collections;

public class DestroyEffect : MonoBehaviour
{
    [SerializeField] private GameObject EffectPrefab;

    void OnEnable()
    {
        NEvent.AddListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
    }

    void OnDisable()
    {
        NEvent.RemoveListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
    }

    void OnOutOfHealth(OutOfHealthEvent outOfHealthEvent)
    {
        Instantiate(EffectPrefab, transform.position, Quaternion.identity);//Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)));
//        iTween.ShakePosition(Camera.main.gameObject, new Vector3(5f, 5f, 0f), 0.5f);
    }

}
