﻿using System;
using UnityEngine;
using System.Collections;

public class StarFlash : MonoBehaviour 
{
    void OnEnable()
	{
	    NEvent.AddListener<StarSpeedEvent>(Game.GameObject, OnStarSpeedEvent);
	}

    void OnDisable()
    {
        NEvent.RemoveListener<StarSpeedEvent>(Game.GameObject, OnStarSpeedEvent);
    }

    private void OnStarSpeedEvent(StarSpeedEvent starSpeedEvent)
    {
        if (starSpeedEvent.FlashColor != null) {
            GetComponent<Renderer>().enabled = true;
            SendMessage("Color", starSpeedEvent.FlashColor.Value);
            iTween.ValueTo(gameObject, iTween.Hash(I.From, 1f, I.To, 0f, I.Time, starSpeedEvent.Length, I.OnUpdate, "Alpha", I.OnComplete, "DisableRenderer"));
        }
    }

    void DisableRenderer() {
        GetComponent<Renderer>().enabled = false;
    }
}
