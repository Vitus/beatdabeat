﻿using JetBrains.Annotations;
using UnityEngine;
using System.Collections;

public class SideTrashSpawner : MonoBehaviour {

    public tk2dSprite[] TrashSprites;
    public MinMaxFloat Position;
    public MinMaxFloat SpawnInterval;
    private float _speedMultiplier = 1f;

    private float SpawnXPosition(tk2dSprite sprite) {
        float spriteWidth = sprite.GetBounds().size.x;
        float positionWidth = Position.Max - Position.Min;
        if (spriteWidth > positionWidth) {
            return Position.Min + positionWidth/2f;
        }
        else {
            float randomRange = positionWidth - spriteWidth;
            return Position.Min + Random.Range(0f, randomRange);
        }
    }

    private IEnumerator Start() {
        if (TrashSprites.Length == 0) {
            yield break;
        }
        float elapsedTime = 0;
        while (true) {
            float nextSpawnTime = SpawnInterval.Random()/_speedMultiplier;
            while (elapsedTime < nextSpawnTime) {
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            elapsedTime -= nextSpawnTime;
            SpawnObject();
        }
    }

    private void SpawnObject() {
        tk2dSprite trashPrefab = TrashSprites[Random.Range(0, TrashSprites.Length)];
        float spawnX = SpawnXPosition(trashPrefab)*(Random.Range(0, 2) == 0 ? -1 : 1);
        tk2dSprite trash = Instantiate(trashPrefab);
        trash.transform.position = new Vector3(spawnX, 300, trashPrefab.transform.position.z);
    }

    private void OnEnable()
    {
        NEvent.AddListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    }

    private void OnDisable()
    {
        NEvent.RemoveListener<StarSpeedEvent>(Game.GameObject, OnChangeSpeed);
    }

    private void OnChangeSpeed(StarSpeedEvent speedEvent)
    {
        _speedMultiplier = speedEvent.Speed;
    }

}