﻿using System;
using UnityEngine;
using System.Collections;
using NelliTweenEngine;

public class BombFlash : MonoBehaviour {
    [SerializeField] private ParticleSystem ParticleSystem;
    [SerializeField] private float Duration;
    private bool _active;

    public static BombFlash Main { get; private set; }

    public bool IsActive {
        get { return _active; }
    }

    private void OnEnable() {
        NEvent.AddListener<BombEvent>(Game.GameObject, OnBombEvent);
    }

    private void OnDisable() {
        NEvent.RemoveListener<BombEvent>(Game.GameObject, OnBombEvent);
    }

    private void Awake() {
        Main = this;
    }

    private void OnBombEvent(BombEvent bombEvent) {
        StopCoroutine("Activate");
        StopCoroutine("DestroyEnemies");
        _active = false;

        Game.Sound.PlaySound(SoundType.GameBomb);

        StartCoroutine("Activate");
        iTween.ValueTo(gameObject, iTween.Hash(I.From, 1f, I.To, 0f, I.Time, Duration, I.OnUpdate, "Alpha"));
        Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
            StartValue = 10,
            EndValue = 0,
            Duration = 1f,
            IgnoreTimeScale = true,
            AffectAxis = AffectAxis.XY
        });
    }

    private IEnumerator DestroyEnemies() {
        while (true) {
            foreach (Enemy enemy in FindObjectsOfType(typeof (Enemy))) {
                if (enemy.HealthType == HealthType.Normal) {
                    NEvent.Dispatch(enemy, new OutOfHealthEvent());
                }
            }
            yield return null;
        }
    }

    private IEnumerator Activate() {
        _active = true;
        StartCoroutine("DestroyEnemies");
        GetComponent<Renderer>().enabled = true;

        yield return new WaitForSeconds(Duration);

        GetComponent<Renderer>().enabled = false;
        StopCoroutine("DestroyEnemies");
        _active = false;
    }

    public void OnBulletUpdate(ParticleBullet bullet) {
        if (_active) {
            if (!bullet.Indestructable) {
                bullet.Kill();
                ParticleSystem.startColor = bullet.Color;
                ParticleSystem.transform.position = bullet.Position;
                ParticleSystem.Emit(10);
            }
        }
    }
}