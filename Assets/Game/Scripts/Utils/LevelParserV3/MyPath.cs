﻿using UnityEngine;
using System.Collections;

namespace LevelParserV3
{
    public class MyPath
    {
        public Vector3[] Points;

        public MyPath(params float[] coords)
        {
            Points = new Vector3[coords.Length / 2];
            for (int i = 0; i < coords.Length / 2; i++)
            {
                Points[i].x = coords[2*i];
                Points[i].y = coords[2*i + 1];
            }
        }
    }
}