﻿using UnityEngine;
using System.Collections;

public enum DestroyType
{
    Immediate = 0,
    Fade = 1
}

public enum NodeWidgetType
{
    Object = 0,
    String = 1,
    Integer = 2,
    Enemy = 3,
    Color = 4,
    //    Bool = 5, 
    Float = 6,
    Path = 7,
    Enum = 8,
    Bullet = 9,
    Laser = 10,
    Vector = 11,
    Time = 12
}

public enum DanceManMovement
{
    Shoulders = 1,
    Guitar = 2,
    Punk = 5,
    Wave = 6,
    Head = 7,
    Gangnam = 8,
    Corners = 9,
    Mike = 10,
    Pelvis = 11,
    CrissCross = 12,
    HandLeftRight = 13
}

public enum DestroyScope
{
    All = 0,
    Pattern = 1
}

public enum DestroyCategory
{
    All = 0,
    Laser = 1,
    Shot = 2,
    AllAndResistable = 3
}

public enum BulletSpace
{
    Absolute = 0,
    Caster = 1,
    CasterToPlayer = 2,
    BulletToPlayer = 3
}

public enum MoveType
{
    Absolute = 0,
    Relative = 1
}

public enum HealthType
{
    Normal = 0,
    Invulnerable = 1,
    OnBackground = 2,
    Boss = 3
}

public enum EnemyImage
{
    EnemyTeleport = 100,
    Enemy1 = 101,
    Enemy2 = 102,
    Enemy3 = 103,
    Enemy4 = 104,
    Enemy5 = 105,
    Enemy6 = 106,
    Enemy7 = 107,
    Enemy8 = 108,
    Enemy9 = 109,
    Enemy10 = 110,
    Enemy11 = 111,
    Enemy12 = 112,
    Enemy13 = 113,
    Enemy14 = 114,
    Boss1 = 201,
    Boss2 = 202,
    Boss3 = 203,
    Boss4 = 204,
    Rocket = 301,
    Bomb0 = 400,
    Bomb1 = 401,
    Bomb2 = 402,
    Bomb3 = 403,
    Bomb4 = 404,
    Bomb5 = 405,
    Electro = 501,
    Smoke = 502,
    Fbss1 = 503,
    Fbss2 = 504,
    Fbss3 = 505,
    Fbss4 = 506,

}

public enum ParticleBulletBehaviour
{
    Polar = 0,
    Cartesian = 1
}