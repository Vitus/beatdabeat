﻿using System;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public class Blackboard
    {
        private readonly Dictionary<string, object> _values = new Dictionary<string, object>();
        private readonly Dictionary<string, Action<Blackboard, object>> _updateActions = new Dictionary<string, Action<Blackboard, object>>();

        public void SubscribeForChanges(string subscribedValue, Action<Blackboard, object> updateAction)
        {
            if (subscribedValue != null)
            {
                if (_updateActions.ContainsKey(subscribedValue))
                    _updateActions[subscribedValue] += updateAction;
                else
                    _updateActions.Add(subscribedValue, updateAction);
            }
        }

        public void SetValue(string valueName, object value)
        {
            _values[valueName] = value;
            Action<Blackboard, object> updateAction;
            if (_updateActions.TryGetValue(valueName, out updateAction))
            {
                updateAction(this, value);
            }
        }

        public object GetValue(string valueName)
        {
            object result;
            if (_values.TryGetValue(valueName, out result))
                return result;
            return null;
        }

        public object GetValue(InputFunc input)
        {
            if (input == null)
                return null;

            object inputValue = input();
            if (inputValue is string && (inputValue as string).Contains(".output_"))
            {
                return GetValue(inputValue as string);
            }
            return inputValue;
        }

        public bool HasValue(string valueName)
        {
            return _values.ContainsKey(valueName);
        }

        public override string ToString()
        {
            string result = "";
            foreach (var value in _values)
            {
                result += string.Format("{0} = {1}\n", value.Key, value.Value);
            }
            result += "\nSubscribes\n";
            foreach (var value in _updateActions)
            {
                result += string.Format("{0} = {1}\n", value.Key, value.Value);
            }
            return result;
        }

        public object GetOrDefault(object value, object defaultValue)
        {
            if (value == null)
                return defaultValue;
            if (value is InputFunc)
                value = (value as InputFunc)();
            if (value is string && (value as string).Contains(".output_"))
                return GetValue(value as string);
            return value;
        }

        public float? GetFloat(object value, float? defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                return null;
            if (blackboardValue is float)
                return (float)blackboardValue;
            if (blackboardValue is float?)
                return (float?) blackboardValue;
            throw new Exception("Wrong format: expected float or float?, but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        }

        public Vector2? GetVector(object value, Vector2? defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                return null;
            if (blackboardValue is Vector2)
                return (Vector2)blackboardValue;
            if (blackboardValue is Vector2?)
                return (Vector2?)blackboardValue;
            throw new Exception("Wrong format: expected Vector2 or Vector2?, but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        }
        
        public Color? GetColor(object value, Color? defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                return null;
            if (blackboardValue is Color)
                return (Color)blackboardValue;
            if (blackboardValue is Color?)
                return (Color?)blackboardValue;
            throw new Exception("Wrong format: expected Color or Color?, but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        } 
        
        public bool? GetBool(object value, bool? defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                return null;
            if (blackboardValue is bool)
                return (bool)blackboardValue;
            if (blackboardValue is bool?)
                return (bool?)blackboardValue;
            throw new Exception("Wrong format: expected bool or bool?, but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        }    
        
        public string GetString(object value, string defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                return null;
            if (blackboardValue is string)
                return (string)blackboardValue;
            throw new Exception("Wrong format: expected string, but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        }

        public MyPath GetPath(object value, MyPath defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                return null;
            if (blackboardValue is MyPath)
                return (MyPath)blackboardValue;
            throw new Exception("Wrong format: expected string, but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        }

        public T GetEnum<T>(object value, T defaultValue)
        {
            object blackboardValue = GetOrDefault(value, defaultValue);
            if (blackboardValue == null)
                throw new Exception("Wrong format: expected Enum " + typeof(T) + ", but get null");
            if (blackboardValue is T)
                return (T)blackboardValue;
            throw new Exception("Wrong format: expected Enum " + typeof(T) + ", but get " + blackboardValue.GetType() + " with value " + blackboardValue);
        }
        
        public List<object> GetList(object value, List<object> defaultValue)
        {
            object result = GetOrDefault(value, defaultValue);
            if (result is List<object>)
                return result as List<object>;
            else
            {
                List<object> list = new List<object>();
                list.Add(result);
                return list;
            }

        }

    }
}