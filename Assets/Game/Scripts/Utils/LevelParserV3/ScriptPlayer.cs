﻿using System.CodeDom;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace LevelParserV3
{
    public class ScriptPlayer : MonoBehaviour
    {
        public List<IEnumerator> Coroutines = new List<IEnumerator>();

        public new void StartCoroutine(IEnumerator coroutine)
        {
            if (MoveNext(coroutine))
            {
                Coroutines.Add(coroutine);
            }
        }

        private bool MoveNext(IEnumerator coroutine)
        {
            if (coroutine.Current != null && coroutine.Current is IEnumerator)
            {
                if (MoveNext(coroutine.Current as IEnumerator))
                {
                    return true;
                }
            }
            return(coroutine.MoveNext());
        }

        void Update()
        {
            for (int i = Coroutines.Count - 1; i >= 0; i--)
            {
                if (!MoveNext(Coroutines[i]))
                {
                    Coroutines.RemoveAt(i);
                }
            }            
        }
    }
}