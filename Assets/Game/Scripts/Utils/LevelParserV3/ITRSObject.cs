﻿using UnityEngine;

namespace LevelParserV2
{
    public interface ITRSObject : ICaster
    {
        Vector2 TRSPosition { get;  }
        float TRSAngle { get;  }
        Vector2 TRSScale { get;  }
        bool IsAlive();
    }
}