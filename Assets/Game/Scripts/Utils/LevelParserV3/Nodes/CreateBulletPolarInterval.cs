﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class CreateBulletPolarInterval
    {
        public class ParametersMore : CreateBulletPolar.Parameters
        {
            public InputFunc interval;
            public InputFunc perIntervalCount;
            public float length;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc casters, InputFunc interval, InputFunc perIntervalCount, 
            InputFunc positionAngle, InputFunc positionLength, InputFunc type, InputFunc positionSpace,
            InputFunc angleSpace, InputFunc angle, InputFunc angularVelocity, InputFunc speed,
            InputFunc acceleration, InputFunc minMaxSpeed, InputFunc color, InputFunc size,
            InputFunc indestructable, InputFunc appearSize, InputFunc appearTime, string bullets_out, float length)
        {
            ParametersMore _p = new ParametersMore
            {
                launchTime = launchTime,
                length = length,
                casters = casters,
                interval = interval,
                perIntervalCount = perIntervalCount,
                positionAngle = positionAngle,
                positionLength = positionLength,
                type = type,
                positionSpace = positionSpace,
                angleSpace = angleSpace,
                angle = angle,
                angularVelocity = angularVelocity,
                speed = speed,
                acceleration = acceleration,
                minMaxSpeed = minMaxSpeed,
                color = color,
                size = size,
                indestructable = indestructable,
                appearSize = appearSize,
                appearTime = appearTime
            };

            _p.l = length;

            float currentTime = _p.launchTime;
            float startTime = currentTime;
            float interval_v = blackboard.GetFloat(_p.interval(), 1f).Value;

            var bullets = new List<object>();

            while (launchTime + _p.l> Game.Music.Time)
            {
                while (currentTime + interval_v < Game.Music.Time)
                {
                    _p.t = ((currentTime - startTime)/_p.l);
                    CreateBulletPolar.PerformIntervalShot(blackboard, Mathf.RoundToInt(blackboard.GetFloat(_p.perIntervalCount(t: _p.t, l: _p.l), 0).Value), bullets, _p);
                    currentTime += interval_v;
                }
                yield return null;
            }

            blackboard.SetValue(bullets_out, bullets);
        }
    }
}