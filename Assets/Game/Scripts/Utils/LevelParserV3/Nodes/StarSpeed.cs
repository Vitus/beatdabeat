﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class StarSpeed
    {
        public class Parameters
        {
            public InputFunc speed;
            public InputFunc flashColor;
            public float length;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc speed, InputFunc flashColor, float length)
        {
            Parameters _p = new Parameters
            {
                speed = speed,
                flashColor = flashColor,
                length = length
            };

            float speed_v = blackboard.GetFloat(_p.speed(), 1).Value;
            Color? flashColor_v = blackboard.GetColor(_p.flashColor(), null);
            NEvent.Dispatch(Game.GameObject, new StarSpeedEvent(speed_v, flashColor_v, _p.length));

            yield break;
        }
        
    }
}