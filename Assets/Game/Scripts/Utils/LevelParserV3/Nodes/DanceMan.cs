﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class DanceMan
    {
        public class Parameters
        {
            public InputFunc movement;
            public InputFunc bpm;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc movement, InputFunc bpm)
        {
            Parameters _p = new Parameters
            {
                movement = movement,
                bpm = bpm
            };

            float? bpm_value = blackboard.GetFloat(_p.bpm(), null);
            if (bpm_value != null)
            {
                NEvent.Dispatch(Game.GameObject, new BPMEvent(bpm_value.Value));
                DanceManAnimator.Main.BPM = bpm_value.Value;
            }

            DanceManAnimator.Main.CurrentMovement = blackboard.GetEnum(_p.movement(), DanceManAnimator.Main.CurrentMovement);
            yield break;
        }
        
    }
}