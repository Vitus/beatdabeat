﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class MovePathEnemy
    {
        public class Parameters
        {
            public InputFunc enemies;
            public InputFunc path;
            public InputFunc startPoint;
            public InputFunc endPoint;
            public InputFunc pathTranslate;
            public InputFunc pathRotate;
            public InputFunc pathScale;
            public InputFunc easeType;
            public float launchTime;
            public float length;
            public float c;
            public float i;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc enemies, InputFunc path,
            InputFunc startPoint, InputFunc endPoint, InputFunc pathTranslate, InputFunc pathRotate, InputFunc pathScale, InputFunc easeType, string enemy_ies, float length)
        {
            Parameters _p = new Parameters
            {
                launchTime = launchTime,
                length = length,
                enemies = enemies,
                path = path,
                startPoint = startPoint,
                endPoint = endPoint,
                pathTranslate = pathTranslate,
                pathRotate = pathRotate,
                pathScale = pathScale,
                easeType = easeType
            };

            MyPath path_v = blackboard.GetPath(_p.path(), null);
            if (path_v == null)
                yield break;

            List<IEnumerator> enemyRoutines = new List<IEnumerator>();

            List<object> enemies_v = blackboard.GetList(_p.enemies(), new List<object>());
            _p.c = enemies_v.Count;

            for (int i = 0; i < _p.c; i++)
            {
                enemyRoutines.Add(MoveEnemy(blackboard, enemies_v[i] as Enemy, path_v, i, _p));
            }
            blackboard.SetValue(enemy_ies, enemies_v);

            bool end = false;
            while (!end)
            {
                end = true;
                foreach (IEnumerator enemyRoutine in enemyRoutines)
                {
                    var succsessNext = enemyRoutine.MoveNext();
                    if (succsessNext)
                    {
                        end = false;
                    }
                }
                yield return null;
            }
        }

        private static IEnumerator MoveEnemy(Blackboard blackboard, Enemy enemy, MyPath path_v, int i, Parameters _p)
        {
            if (enemy == null)
                yield break;

            _p.i = i;

            int startPoint = Mathf.RoundToInt(blackboard.GetFloat(_p.startPoint(), 0).Value);
            int endPoint = Mathf.RoundToInt(blackboard.GetFloat(_p.endPoint(),  path_v.Points.Length - 1).Value);

            int count = endPoint - startPoint + 1;

            object translate = blackboard.GetOrDefault(_p.pathTranslate(_p.c, _p.i), (object) Vector2.zero);
            Vector2 translateVector = Vector2.zero;

            if (translate is Vector2)
                translateVector = (Vector2) translate;
            if (translate is IList)
                translateVector = ((translate as IList)[i] as ITRSObject).TRSPosition;

            Matrix4x4 TRS = Matrix4x4.TRS(translateVector,
                                          Quaternion.Euler(0f, 0f, blackboard.GetFloat(_p.pathRotate(_p.c, _p.i), 0).Value),
                                          blackboard.GetVector(_p.pathScale(_p.c, _p.i), Vector2.one).Value);
            Vector3[] originalPath = path_v.Points;
            Vector3[] path = new Vector3[count];
            for (int j = startPoint; j <= endPoint; j++)
            {
                path[j - startPoint] = TRS.MultiplyPoint(originalPath[j]) + Vector3.forward * enemy.transform.position.z;
            }
            int random = Random.Range(int.MinValue, int.MaxValue);
            if (path.Length == 1)
            {
                enemy.gameObject.transform.position = path[0];
            }
            else
            {
                enemy.gameObject.transform.position = path[0];
                iTween.MoveTo(enemy.gameObject,
                              iTween.Hash(I.Name, "MovePathNode" + random, I.Path, path, I.Time, _p.length, I.EaseType,
                                          blackboard.GetEnum(_p.easeType(), iTween.EaseType.linear)));
            }
            while (Game.Music.Time < _p.launchTime + _p.length)
                yield return null;

            if (enemy != null)
            {
                iTween.StopByName(enemy.gameObject, "MovePathNode" + random);
                enemy.transform.position = path[path.Length - 1];
            }
        }

    }
}