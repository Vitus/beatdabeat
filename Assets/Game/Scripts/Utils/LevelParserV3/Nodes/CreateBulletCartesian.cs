﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class CreateBulletCartesian
    {
        public class Parameters
        {
            public InputFunc casters;
            public InputFunc count;
            public InputFunc position;
            public InputFunc type;
            public InputFunc positionSpace;
            public InputFunc velocitySpace;
            public InputFunc accelerationSpace;
            public InputFunc angularVelocity;
            public InputFunc velocityXY;
            public InputFunc accelerationXY;
            public InputFunc minMaxSpeedXY;
            public InputFunc color;
            public InputFunc size;
            public InputFunc indestructable;
            public InputFunc appearSize;
            public InputFunc appearTime;
            public float launchTime;
            public float c;
            public float i;         
            public float t;
            public float l;         
            public float castc;
            public float casti;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc casters, InputFunc count,
            InputFunc position, InputFunc type, InputFunc positionSpace,
            InputFunc velocitySpace, InputFunc accelerationSpace, InputFunc angularVelocity, InputFunc velocityXY,
            InputFunc accelerationXY, InputFunc minMaxSpeedXY, InputFunc color, InputFunc size,
            InputFunc indestructable, InputFunc appearSize, InputFunc appearTime, string bullets_out)
        {
            Parameters _p = new Parameters
            {
                launchTime = launchTime,
                casters = casters,
                count = count,
                position = position,
                type = type,
                positionSpace = positionSpace,
                velocitySpace = velocitySpace,
                accelerationSpace = accelerationSpace,
                angularVelocity = angularVelocity,
                velocityXY = velocityXY,
                accelerationXY = accelerationXY,
                minMaxSpeedXY = minMaxSpeedXY,
                color = color,
                size = size,
                indestructable = indestructable,
                appearSize = appearSize,
                appearTime = appearTime
            };

            List<object> bullets = new List<object>();
            PerformIntervalShot(blackboard, Mathf.RoundToInt(blackboard.GetFloat(_p.count(),0).Value), bullets, _p, bullets_out);
            yield break;
        }

        public static void PerformIntervalShot(Blackboard blackboard, int bulletCount, List<object> bullets, Parameters _p, string bullets_out)
        {
            var casters = blackboard.GetList(_p.casters(), null);
            if (casters != null)
            {
                _p.castc = casters.Count;
                if (casters.Count > 0)
                {
                    for (int i = 0; i < casters.Count; i++)
                    {
                        if (casters[i] is Vector2)
                        {
                            MyVector casterVector = (Vector2)casters[i];
                            casters[i] = casterVector;
                        }
                        CreateBulletsForCaster(blackboard, bullets, casters[i] as ITRSObject, i, bulletCount,_p);
                        blackboard.SetValue(bullets_out, bullets);
                    }
                }
            }
        }

        private static void CreateBulletsForCaster(Blackboard blackboard, List<object> bullets, ITRSObject caster, int casterI, int bulletCount, Parameters _p)
        {
            if (caster == null || !caster.IsAlive())
                return;

            _p.casti = casterI;

            CreateBullets(blackboard, bullets, caster, bulletCount, _p);
        }

        private static void CreateBullets(Blackboard blackboard, List<object> bullets, ITRSObject caster, int bulletCount, Parameters _p)
        {
            _p.c = bulletCount;

            for (int i = 0; i < bulletCount; i++)
            {
                _p.i = i;
                ParticleBullet bullet = Game.ParticleBullets.SpawnUnusedParticle();

                ApplyValues(blackboard, caster, bullet, _p);

                bullets.Add(bullet);
            }
        }

        private static void ApplyValues(Blackboard blackboard, ITRSObject caster, ParticleBullet bullet, Parameters _p)
        {
            Vector2 position = blackboard.GetVector(_p.position(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti),  bullet.Position).Value;
            Vector2 velocity = blackboard.GetVector(_p.velocityXY(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.Velocity).Value;
            Vector2 acceleration = blackboard.GetVector(_p.accelerationXY(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.Acceleration).Value;

            Matrix4x4 TRS;

            TRS = GetTRS(caster, blackboard.GetEnum(_p.positionSpace(), BulletSpace.Caster), null);
            position = TRS.MultiplyPoint(position);
            TRS = GetTRS(caster, blackboard.GetEnum(_p.velocitySpace(), BulletSpace.Caster), position);
            velocity = TRS.MultiplyPoint(velocity);
            TRS = GetTRS(caster, blackboard.GetEnum(_p.accelerationSpace(), BulletSpace.Caster), position);
            acceleration = TRS.MultiplyPoint(acceleration);

            bullet.SetBehaviour(ParticleBulletBehaviour.Cartesian);

            bullet.Type = blackboard.GetEnum(_p.type(), bullet.Type);
            bullet.Position = position;
            bullet.AngularVelocity = blackboard.GetFloat(_p.angularVelocity(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AngularVelocity).Value;
            bullet.Velocity = velocity;
            bullet.Acceleration = acceleration;
            bullet.MinMaxSpeed = blackboard.GetVector(_p.minMaxSpeedXY(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.MinMaxSpeed).Value;
            bullet.Color = blackboard.GetColor(_p.color(), bullet.Color).Value;
            bullet.Size = blackboard.GetFloat(_p.size(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.Size).Value;
            bullet.AppearSize = blackboard.GetFloat(_p.appearSize(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AppearSize).Value;
            bullet.AppearTime = blackboard.GetFloat(_p.appearTime(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AppearTime).Value;
            bullet.Indestructable = blackboard.GetBool(_p.indestructable(), bullet.Indestructable).Value;
        }

        private static Matrix4x4 GetTRS(ITRSObject caster, BulletSpace space, Vector2? position)
        {
            return Matrix4x4.TRS(position == null ? GetTranslate(caster) : Vector2.zero,
                                 GetRotation(caster, space, position), GetScale(caster, space, position));
        }

        private static Vector2 GetTranslate(ITRSObject caster)
        {
            return caster == null ? Vector2.zero : caster.TRSPosition;
        }

        private static Quaternion GetRotation(ITRSObject caster, BulletSpace space, Vector2? bulletPosition)
        {
            switch (space)
            {
                case BulletSpace.Absolute:
                    return Quaternion.identity;
                case BulletSpace.Caster:
                    return caster == null ? Quaternion.identity : Quaternion.Euler(0f, 0f, caster.TRSAngle);
                case BulletSpace.CasterToPlayer:
                    return Quaternion.Euler(0f, 0f,
                                            H.VectorAngle((Vector2)Game.Player.Ship.transform.position -
                                                          (caster == null ? Vector2.zero : caster.TRSPosition)) +
                                            90);
                case BulletSpace.BulletToPlayer:
                    Vector2 position;
                    if (bulletPosition != null)
                        position = bulletPosition.Value;
                    else if (caster != null)
                        position = caster.TRSPosition;
                    else
                        position = Vector2.zero;

                    return Quaternion.Euler(0f, 0f,
                                            H.VectorAngle((Vector2)Game.Player.Ship.transform.position -
                                                          position) + 90);

            }
            return Quaternion.identity;
        }

        private static Vector2 GetScale(ITRSObject caster, BulletSpace space, Vector2? bulletPosition)
        {
            switch (space)
            {
                case BulletSpace.Absolute:
                    return Vector2.one;
                case BulletSpace.Caster:
                    return caster == null ? Vector2.one : caster.TRSScale;
                case BulletSpace.CasterToPlayer:
                    return Vector2.one;
                case BulletSpace.BulletToPlayer:
                    return Vector2.one;
            }
            return Vector2.one;
        }

    }
}