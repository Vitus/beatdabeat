﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class TweenEnemy
    {
        public class Parameters
        {
            public InputFunc enemies;
            public InputFunc vector;
            public InputFunc angle;
            public InputFunc scale;
            public InputFunc moveType;
            public InputFunc easeType;
            public float launchTime;
            public float length;
            public float c;
            public float i;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc enemies, InputFunc vector,
            InputFunc angle, InputFunc scale, InputFunc moveType, InputFunc easeType, string enemy_ies, float length)
        {
            Parameters _p = new Parameters
            {
                launchTime = launchTime,
                length = length,
                enemies = enemies,
                vector = vector,
                angle = angle,
                scale = scale,
                moveType = moveType,
                easeType = easeType
            };

            List<object> enemies_out = blackboard.GetList(_p.enemies(), new List<object>());
            _p.c = enemies_out.Count;

            List<IEnumerator> enemyRoutines = new List<IEnumerator>();

            for (int i = 0; i < _p.c; i++)
            {
                _p.i = i;
                enemyRoutines.Add(MoveEnemy(blackboard, enemies_out[i] as Enemy, i, _p));
            }

            blackboard.SetValue(enemy_ies, enemies_out);

            bool end = false;
            while (!end)
            {
                end = true;
                foreach (IEnumerator enemyRoutine in enemyRoutines)
                {
                    var succsessNext = enemyRoutine.MoveNext();
                    if (succsessNext)
                    {
                        end = false;
                    }
                }
                yield return null;
            }
        }

        private static IEnumerator MoveEnemy(Blackboard blackboard, Enemy enemy, int i, Parameters _p)
        {
            if (enemy == null)
                yield break;

            _p.i = i;

            MoveType moveType = blackboard.GetEnum(_p.moveType(_p.c, _p.i), MoveType.Absolute);
            Vector2? vector = blackboard.GetVector(_p.vector(_p.c, _p.i), null);
            float? angle = blackboard.GetFloat(_p.angle(_p.c, _p.i), null);
            float? scale = blackboard.GetFloat(_p.scale(_p.c, _p.i), null);

            Vector2? toVector = Translate(blackboard, enemy, vector, moveType, _p);
            float? toAngle = Rotate(blackboard, enemy, angle, moveType, _p);
            float? toScale = Scale(blackboard, enemy, scale, moveType, _p);

            while (Game.Music.Time < _p.launchTime + _p.length)
            {
                yield return null;
            }

            if (enemy != null)
            {
                if (toVector != null)
                {
                    iTween.StopByName(enemy.gameObject, "TweenEnemyPosition");
                    enemy.transform.position = toVector.Value;
                }
                if (toAngle != null)
                {
                    iTween.StopByName(enemy.gameObject, "TweenEnemyScale");
                    enemy.transform.rotation = Quaternion.Euler(0f, 0f, toAngle.Value);
                }
                if (toScale != null)
                {
                    iTween.StopByName(enemy.gameObject, "TweenEnemyRotation");
                    enemy.transform.localScale = (Vector3)Vector2.one*toScale.Value + Vector3.forward;
                }
            }
        }

        private static Vector2? Translate(Blackboard blackboard, Enemy enemy, Vector2? vector, MoveType moveType,
            Parameters _p)
        {
            if (vector != null)
            {
                Vector2 toPosition = Vector2.zero;

                switch (moveType)
                {
                    case MoveType.Absolute:
                        toPosition = vector.Value;
                        break;
                    case MoveType.Relative:
                        toPosition = vector.Value + (Vector2) (enemy.gameObject.transform.position);
                        break;
                }
                iTween.MoveTo(enemy.gameObject,
                    iTween.Hash(I.Name, "TweenEnemyPosition", I.X, toPosition.x, I.Y, toPosition.y, I.Time, _p.length,
                        I.EaseType,
                        blackboard.GetEnum(_p.easeType(), iTween.EaseType.linear)));
                return toPosition;
            }
            return null;
        }

        private static float? Rotate(Blackboard blackboard, Enemy enemy, float? angle, MoveType moveType, Parameters _p)
        {
            if (angle != null)
            {
                float toAngle = 0f;

                switch (moveType)
                {
                    case MoveType.Absolute:
                        toAngle = angle.Value;
                        break;
                    case MoveType.Relative:
                        toAngle = angle.Value + enemy.gameObject.transform.rotation.eulerAngles.z;
                        break;
                }
                iTween.RotateTo(enemy.gameObject,
                    iTween.Hash(I.Name, "TweenEnemyRotation", I.Z, toAngle, I.Time, _p.length, I.EaseType,
                        blackboard.GetEnum(_p.easeType(), iTween.EaseType.linear)));
                return toAngle;
            }
            return null;
        }

        private static float? Scale(Blackboard blackboard, Enemy enemy, float? scale, MoveType moveType, Parameters _p)
        {
            if (scale != null)
            {
                float toScale = 0f;

                switch (moveType)
                {
                    case MoveType.Absolute:
                        toScale = scale.Value;
                        break;
                    case MoveType.Relative:
                        toScale = scale.Value + enemy.gameObject.transform.localScale.x;
                        break;
                }
                iTween.ScaleTo(enemy.gameObject,
                    iTween.Hash(I.Name, "TweenEnemyScale", I.X, toScale, I.Y, toScale, I.Z, 1, I.Time, _p.length, I.EaseType,
                        blackboard.GetEnum(_p.easeType(), iTween.EaseType.linear)));
                return toScale;
            }
            return null;
        }

    }
}