﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class SetBulletPolar
    {
        public class ParametersMore : CreateBulletPolar.Parameters
        {
            public InputFunc bullets;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc bullets, InputFunc casters,  
            InputFunc positionAngle, InputFunc positionLength, InputFunc type, InputFunc positionSpace,
            InputFunc angleSpace, InputFunc angle, InputFunc angularVelocity, InputFunc speed,
            InputFunc acceleration, InputFunc minMaxSpeed, InputFunc color, InputFunc size,
            InputFunc indestructable, InputFunc appearSize, InputFunc appearTime)
        {
            ParametersMore _p = new ParametersMore
            {
                launchTime = launchTime,
                bullets = bullets,
                casters = casters,
                positionAngle = positionAngle,
                positionLength = positionLength,
                type = type,
                positionSpace = positionSpace,
                angleSpace = angleSpace,
                angle = angle,
                angularVelocity = angularVelocity,
                speed = speed,
                acceleration = acceleration,
                minMaxSpeed = minMaxSpeed,
                color = color,
                size = size,
                indestructable = indestructable,
                appearSize = appearSize,
                appearTime = appearTime
            };

            var bullets_v = blackboard.GetList(_p.bullets(), null);
            PerformIntervalSet(blackboard, 0, bullets_v.Count, bullets_v, _p);
            yield break;
        }

        private static void PerformIntervalSet(Blackboard blackboard, int bulletStartIndex, int bulletCount, List<object> bullets, ParametersMore _p)
        {
            if (bullets == null)
                return;

            var casters = blackboard.GetList(_p.casters(), null);
            _p.c = bulletCount;

            if (casters != null)
            {
                _p.castc = casters.Count;

                int bulletsPerCaster = bulletCount / casters.Count;

                for (int casterNum = 0; casterNum < casters.Count; casterNum++)
                {
                    _p.casti = casterNum;
                    SetBullets(blackboard, bulletsPerCaster, casterNum, casters[casterNum] as ITRSObject, bullets, bulletStartIndex, _p);
                }
            }
            else
            {
                SetBullets(blackboard, bulletCount, 0, null, bullets, bulletStartIndex, _p);
            }
        }

        private static void SetBullets(Blackboard blackboard, int bulletsPerCaster, int casterNum, ITRSObject caster, List<object> bullets, int bulletStartIndex, ParametersMore _p)
        {
            for (int bulletNum = 0; bulletNum < bulletsPerCaster; bulletNum++)
            {
                _p.i = bulletNum;

                int bulletIndex = bulletStartIndex + bulletNum + casterNum * bulletsPerCaster;

                ParticleBullet bullet = bullets[bulletIndex] as ParticleBullet;
                CreateBulletPolar.ApplyValues(blackboard, caster, bullet, true, _p);

            }
        }
    }
}