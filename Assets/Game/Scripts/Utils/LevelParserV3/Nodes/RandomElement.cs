﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class RandomElement
    {
        public class Parameters
        {
            public InputFunc list;
            public InputFunc count;
            public float c;
            public float i;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc list, InputFunc count, string elements_out)
        {
            Parameters _p = new Parameters{list = list, count = count};

            int c = (int)blackboard.GetFloat(_p.count(), 0).Value;
            List<object> l = blackboard.GetList(_p.list(), null);

            List<object> result = new List<object>(l);
            result.RemoveAll(o => !(o is ITRSObject) || !(o as ITRSObject).IsAlive());

            int removeCount = result.Count - c;
            for (int i = 0; i < removeCount; i++)
            {
                result.RemoveAt(Random.Range(0, result.Count));
            }

            blackboard.SetValue(elements_out, result);

            yield break;
            
        }
    }
}