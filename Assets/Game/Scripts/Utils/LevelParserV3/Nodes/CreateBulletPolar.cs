﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class CreateBulletPolar
    {
        public class Parameters
        {
            public InputFunc casters;
            public InputFunc count;
            public InputFunc positionAngle;
            public InputFunc positionLength;
            public InputFunc type;
            public InputFunc positionSpace;
            public InputFunc angleSpace;
            public InputFunc angle;
            public InputFunc angularVelocity;
            public InputFunc speed;
            public InputFunc acceleration;
            public InputFunc minMaxSpeed;
            public InputFunc color;
            public InputFunc size;
            public InputFunc indestructable;
            public InputFunc appearSize;
            public InputFunc appearTime;
            public float launchTime;
            public float c;
            public float i;         
            public float t;
            public float l;         
            public float castc;
            public float casti;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc casters, InputFunc count,
            InputFunc positionAngle, InputFunc positionLength, InputFunc type, InputFunc positionSpace,
            InputFunc angleSpace, InputFunc angle, InputFunc angularVelocity, InputFunc speed,
            InputFunc acceleration, InputFunc minMaxSpeed, InputFunc color, InputFunc size,
            InputFunc indestructable, InputFunc appearSize, InputFunc appearTime, string bullets_out)
        {
            Parameters _p = new Parameters
            {
                launchTime = launchTime,
                casters = casters,
                count = count,
                positionAngle = positionAngle,
                positionLength = positionLength,
                type = type,
                positionSpace = positionSpace,
                angleSpace = angleSpace,
                angle = angle,
                angularVelocity = angularVelocity,
                speed = speed,
                acceleration = acceleration,
                minMaxSpeed = minMaxSpeed,
                color = color,
                size = size,
                indestructable = indestructable,
                appearSize = appearSize,
                appearTime = appearTime
            };

            List<object> bullets = new List<object>();
            PerformIntervalShot(blackboard, Mathf.RoundToInt(blackboard.GetFloat(_p.count(),0f).Value), bullets, _p);
            blackboard.SetValue(bullets_out, bullets);
            yield break;
        }

        public static void PerformIntervalShot(Blackboard blackboard, int bulletCount, List<object> bullets, Parameters _p)
        {
            List<object> casters = blackboard.GetList(_p.casters(), null);
            if (casters != null)
            {
                _p.castc = casters.Count;
                if (casters.Count > 0)
                {
                    for (int i = 0; i < _p.castc; i++)
                    {
                        if (casters[i] is Vector2)
                        {
                            MyVector casterVector = (Vector2)casters[i];
                            casters[i] = casterVector;
                        }
                        CreateBulletsForCaster(blackboard, bullets, casters[i] as ITRSObject, i, bulletCount, _p);
                    }
                }
            }
        }

        private static void CreateBulletsForCaster(Blackboard blackboard, List<object> bullets, ITRSObject caster, int casterNum, int bulletCount, Parameters _p)
        {
            if (caster == null || !caster.IsAlive())
                return;

            _p.casti = casterNum;

            CreateBullets(blackboard, bullets, caster, bulletCount, _p);
        }

        private static void CreateBullets(Blackboard blackboard, List<object> bullets, ITRSObject caster, int bulletCount, Parameters _p)
        {
            _p.c = bulletCount;
            
            for (int i = 0; i < bulletCount; i++)
            {
                _p.i = i;
                ParticleBullet bullet = Game.ParticleBullets.SpawnUnusedParticle();

                ApplyValues(blackboard, caster, bullet, false, _p);

                bullets.Add(bullet);
            }
        }

        public static void ApplyValues(Blackboard blackboard, ITRSObject caster, ParticleBullet bullet, bool set, Parameters _p)
        {
            bullet.SetBehaviour(ParticleBulletBehaviour.Polar);

            Vector2? position = GetPosition(blackboard, _p);
            if (position == null && set)
            {
                position = bullet.Position;
            }
            else
            {
                if (position == null)
                    position = Vector2.zero;
                Matrix4x4 TRS = GetTRSMatrix(caster, blackboard.GetEnum(_p.positionSpace(), BulletSpace.Caster), null);
                position = TRS.MultiplyPoint(position.Value);
            }

            var angle = blackboard.GetFloat(_p.angle(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), null);
            if (angle == null && set)
            {
                angle = bullet.Angle;
            }
            else
            {
                if (angle == null)
                    angle = 0;
                angle += GetRotation(caster, blackboard.GetEnum(_p.angleSpace(), BulletSpace.Caster), position).eulerAngles.z;
            }

            bullet.Type = blackboard.GetEnum(_p.type(), bullet.Type);
            bullet.Position = position.Value;
            bullet.Angle = angle.Value;
            bullet.AngularVelocity = blackboard.GetFloat(_p.angularVelocity(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AngularVelocity).Value;
            bullet.Speed = blackboard.GetFloat(_p.speed(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.Speed).Value;
            bullet.AccelerationLinear = blackboard.GetFloat(_p.acceleration(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AccelerationLinear).Value;
            bullet.MinMaxSpeedLinear = blackboard.GetFloat(_p.minMaxSpeed(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.MinMaxSpeedLinear).Value;
            bullet.Color = blackboard.GetColor(_p.color(), bullet.Color).Value;
            bullet.Size = blackboard.GetFloat(_p.size(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.Size).Value;
            bullet.AppearSize = blackboard.GetFloat(_p.appearSize(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AppearSize).Value;
            bullet.AppearTime = blackboard.GetFloat(_p.appearTime(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), bullet.AppearTime).Value;
            bullet.Indestructable = blackboard.GetBool(_p.indestructable(),  bullet.Indestructable).Value;
        }

        private static Vector2? GetPosition(Blackboard blackboard, Parameters _p)
        {
            var length = blackboard.GetFloat(_p.positionLength(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), null);
            var angle = blackboard.GetFloat(_p.positionAngle(_p.c, _p.i, _p.t, _p.l, _p.castc, _p.casti), null);
            if (length == null || angle == null)
                return null;

            Vector2 position = H.PolarVector2(length.Value, angle.Value);
            return position;
        }

        private static Matrix4x4 GetTRSMatrix(ITRSObject caster, BulletSpace space, Vector2? position)
        {
            return Matrix4x4.TRS(position == null ? GetTranslate(caster) : Vector2.zero,
                                 GetRotation(caster, space, position), GetScale(caster, space, position));
        }

        private static Vector2 GetTranslate(ITRSObject caster)
        {
            return caster == null ? Vector2.zero : caster.TRSPosition;
        }

        private static Quaternion GetRotation(ITRSObject caster, BulletSpace space, Vector2? bulletPosition)
        {
            switch (space)
            {
                case BulletSpace.Absolute:
                    return Quaternion.identity;
                case BulletSpace.Caster:
                    return caster == null ? Quaternion.identity : Quaternion.Euler(0f, 0f, caster.TRSAngle - 90);
                case BulletSpace.CasterToPlayer:
                    return Quaternion.Euler(0f, 0f,
                                            H.VectorAngle((Vector2)Game.Player.Ship.transform.position -
                                                          (caster == null ? Vector2.zero : caster.TRSPosition)));
                case BulletSpace.BulletToPlayer:
                    Vector2 position;
                    if (bulletPosition != null)
                        position = bulletPosition.Value;
                    else if (caster != null)
                        position = caster.TRSPosition;
                    else
                        position = Vector2.zero;

                    return Quaternion.Euler(0f, 0f,
                                            H.VectorAngle((Vector2)Game.Player.Ship.transform.position -
                                                          position));

            }
            return Quaternion.identity;
        }

        private static Vector2 GetScale(ITRSObject caster, BulletSpace space, Vector2? bulletPosition)
        {
            switch (space)
            {
                case BulletSpace.Absolute:
                    return Vector2.one;
                case BulletSpace.Caster:
                    return caster == null ? Vector2.one : caster.TRSScale;
                case BulletSpace.CasterToPlayer:
                    return Vector2.one;
                case BulletSpace.BulletToPlayer:
                    return Vector2.one;
            }
            return Vector2.one;
        }


    }
}