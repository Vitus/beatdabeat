﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class SetEnemyAnimation
    {
        public class Parameters
        {
            public InputFunc enemies;
            public InputFunc animation;
            public InputFunc animationPlayOnce;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc enemies, InputFunc animation, InputFunc animationPlayOnce)
        {
            Parameters _p = new Parameters
            {
                enemies = enemies,
                animation = animation,
                animationPlayOnce = animationPlayOnce
            };

            var enemies_v = blackboard.GetList(_p.enemies(), new List<object>());
            foreach (Enemy enemy in enemies_v)
            {
                if (enemy.IsAlive())
                {
                    enemy.SetAnimation(blackboard.GetString(_p.animation(), null),
                                       blackboard.GetBool(_p.animationPlayOnce(), false).Value);
                }
            }
            yield break;
        }
        
    }
}