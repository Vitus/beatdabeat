﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class DestroyEnemy
    {
        public class Parameters
        {
            public InputFunc enemy;
            public InputFunc destroyType;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc enemy, InputFunc destroyType)
        {
            Parameters _p = new Parameters
            {
                enemy = enemy,
                destroyType = destroyType
            };

            List<object> enemies = blackboard.GetList(_p.enemy(), new List<object>());

            foreach (Enemy oneEnemy in enemies)
            {
                if (oneEnemy == null)
                    continue;

                switch (blackboard.GetEnum(_p.destroyType(), DestroyType.Immediate))
                {
                    case DestroyType.Immediate:
                        Object.Destroy(oneEnemy.gameObject);
                        break;
                    case DestroyType.Fade:
                        NEvent.Dispatch(oneEnemy, new OutOfHealthEvent());
                        break;
                }
            }
            yield break;
        }
        
    }
}