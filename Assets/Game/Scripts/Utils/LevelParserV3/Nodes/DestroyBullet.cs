﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class DestroyBullet
    {
        public class Parameters
        {
            public InputFunc bullets;
            public InputFunc type;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc bullets, InputFunc type)
        {
            Parameters _p = new Parameters
            {
                bullets = bullets,
                type = type
            };

            List<object> bullets_v = blackboard.GetList(_p.bullets(), null);
            if (bullets != null)
            {
                foreach (ParticleBullet bullet in bullets_v)
                {
                    if (bullet == null)
                        continue;

                    switch (blackboard.GetEnum(_p.type(), DestroyType.Immediate))
                    {
                        case DestroyType.Immediate:
                            bullet.IsDead = true;
                            break;
                        case DestroyType.Fade:
                            bullet.Kill();
                            break;
                    }
                }
            }
            yield break;
        }
        
    }
}