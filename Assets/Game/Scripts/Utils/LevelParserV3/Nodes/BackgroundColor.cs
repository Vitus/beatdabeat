﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3 {
    public static class BackgroundColor {
        public class Parameters {
            public InputFunc fromColor;
            public InputFunc toColor;
            public float length;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc fromColor, InputFunc toColor, float length) {
            Parameters _p = new Parameters {
                fromColor = fromColor,
                toColor = toColor,
                length = length
            };

            Color defaultColor = new Color(26/255f, 20/255f, 18/255f);

            Color startColor = blackboard.GetColor(_p.fromColor(), LevelPlayer.Main.BackgroundColorSprite.color).Value;
            Color targetColor = blackboard.GetColor(_p.toColor(), defaultColor).Value;

            while (Game.Music.Time < launchTime + length) {
                float startTime = launchTime;
                float endTime = startTime + length;
                float currentTime = Game.Music.Time;
                float t = (currentTime - startTime)/(endTime - startTime);
                Color currentColor = Color.Lerp(startColor, targetColor, t);
                LevelPlayer.Main.BackgroundColorSprite.color = currentColor;
                yield return null;
            }
            LevelPlayer.Main.BackgroundColorSprite.color = targetColor;
        }

    }
}