﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class DestroyAllBullet
    {
        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc scope, InputFunc type)
        {
            Game.ParticleBullets.DestroyAllBullets();
            yield break;
        }
        
    }
}