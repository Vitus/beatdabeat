﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class CreateLaser
    {
        public class Parameters
        {
            public InputFunc casters;
            public InputFunc attachToCaster;
            public InputFunc count;
            public InputFunc angle;
            public InputFunc endPoint;
            public InputFunc appearDistance;
            public InputFunc vanishDistance;
            public InputFunc startDistance;
            public InputFunc width;
            public InputFunc length;
            public InputFunc reachVanishPointTime;
            public InputFunc angularVelocity;
            public InputFunc enabled;
            public InputFunc enableSpeed;
            public InputFunc autoDestroy;
            public InputFunc color;
            public float launchTime;
            public float c;
            public float i;         
            public float t;
            public float l;         
            public float castc;
            public float casti;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc casters, InputFunc attachToCaster,
            InputFunc count, InputFunc angle, InputFunc endPoint, InputFunc appearDistance,
            InputFunc vanishDistance, InputFunc startDistance, InputFunc width, InputFunc length,
            InputFunc reachVanishPointTime, InputFunc angularVelocity, InputFunc enabled, InputFunc enableSpeed,
            InputFunc autoDestroy, InputFunc color, string lasers_out)
        {
            Parameters _p = new Parameters
            {
                launchTime = launchTime,
                casters = casters,
                attachToCaster = attachToCaster,
                count = count,
                angle = angle,
                endPoint = endPoint,
                appearDistance = appearDistance,
                vanishDistance = vanishDistance,
                startDistance = startDistance,
                width = width,
                length = length,
                reachVanishPointTime = reachVanishPointTime,
                angularVelocity = angularVelocity,
                enabled = enabled,
                enableSpeed = enableSpeed,
                autoDestroy = autoDestroy,
                color = color
            };

            List<object> lasers = new List<object>();

            List<object> casters_v = blackboard.GetList(_p.casters(), new List<object>());
            _p.castc = casters_v.Count;
            for (int i = 0; i < _p.castc; i++)
            {
                CreateBulletsForCaster(blackboard, lasers, casters_v[i] as ITRSObject, i, _p);
            }
            blackboard.SetValue(lasers_out, lasers);
            yield break;
        }

        private static void CreateBulletsForCaster(Blackboard blackboard, List<object> lasers, ITRSObject caster, int casterI, Parameters _p)
        {
            if (!caster.IsAlive())
            {
                return;
            }

            _p.casti = casterI;

            CreateBullets(blackboard, lasers, caster, _p);
        }

        private static void CreateBullets(Blackboard blackboard, List<object> lasers, ITRSObject caster, Parameters _p)
        {
            int bulletCount = Mathf.RoundToInt(blackboard.GetFloat(_p.count(castc: _p.castc, casti: _p.casti), 0).Value);

            _p.c = bulletCount;

            for (int i = 0; i < bulletCount; i++)
            {
                _p.i = i;

                Laser laser = Game.Level.CreateLaser();

                if (blackboard.GetBool(_p.attachToCaster(), true).Value)
                {
                    laser.Caster = caster;
                    laser.Angle = blackboard.GetFloat(_p.angle(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 0f).Value;
                }
                else
                {
                    if (caster != null)
                    {
                        laser.transform.position = caster.TransformPosition;
                        laser.Angle = blackboard.GetFloat(_p.angle(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 0f).Value + caster.TransformAngle;
                    }
                    else
                    {
                        laser.Caster = null;
                        laser.Angle = blackboard.GetFloat(_p.angle(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 0f).Value;
                    }
                }

                laser.AngularVelocity = blackboard.GetFloat(_p.angularVelocity(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 0f).Value;
                laser.AppearDistance = blackboard.GetFloat(_p.appearDistance(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 0f).Value;
                laser.AutoDestroy = blackboard.GetBool(_p.autoDestroy(), true).Value;
                laser.EnableSpeed = blackboard.GetFloat(_p.enableSpeed(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 50f).Value;
                laser.Enabled = blackboard.GetBool(_p.enabled(), true).Value;
                laser.Length = blackboard.GetFloat(_p.length(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 5f).Value;
                laser.StartDistance = blackboard.GetFloat(_p.startDistance(_p.c, _p.i, castc: _p.castc, casti: _p.casti), -laser.Length).Value;
                laser.VanishDistance = blackboard.GetFloat(_p.vanishDistance(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 0f).Value;
                laser.Width = blackboard.GetFloat(_p.width(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 5f).Value;

                laser.Speed = (laser.VanishDistance - laser.StartDistance - laser.Length) /
                              (blackboard.GetFloat(_p.reachVanishPointTime(_p.c, _p.i, castc: _p.castc, casti: _p.casti), 1f).Value);
                laser.SetColor(blackboard.GetColor(_p.color(), Color.white).Value);

                lasers.Add(laser);
            }
        }


    }
}