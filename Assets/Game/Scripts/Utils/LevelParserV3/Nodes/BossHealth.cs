﻿using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class BossHealth
    {
        public class Parameters
        {
            public InputFunc name;
            public InputFunc amount;
            public InputFunc show;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc name, InputFunc amount, InputFunc show)
        {
            Parameters _p = new Parameters
            {
                name = name,
                amount = amount,
                show = show
            };

            string name_v = blackboard.GetString(_p.name(), "");
            if (BossHealthBar.Main.Visible)
                yield break;

            GameObject.Find("Boss").GetComponent<Enemy>().SetHealthType(HealthType.Boss);

            BossHealthBar.Main.SetName(name_v);

            float? amount_v = blackboard.GetFloat(_p.amount(), null);
            if (amount != null)
                BossHealthBar.Main.SetMax(amount_v.Value);

            BossHealthBar.Main.Show(true);
        }
        
    }
}