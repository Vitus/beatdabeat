﻿using System;
using System.Collections;
using System.Collections.Generic;
using LevelParserV2;
using UnityEngine;

namespace LevelParserV3
{
    public static class CreateEnemy
    {
        public class Parameters
        {
            public InputFunc count;
            public InputFunc image;
            public InputFunc health;
            public InputFunc healthType;
            public InputFunc position;
            public InputFunc rotation;
            public InputFunc scale;
            public InputFunc damageAward;
            public InputFunc killAward;
            public InputFunc layer;
            public float c;
            public float i;
        }

        public static IEnumerator Act(float launchTime, Blackboard blackboard, InputFunc count, InputFunc image, InputFunc health, InputFunc healthType, InputFunc position, InputFunc rotation, InputFunc scale, InputFunc damageAward, InputFunc killAward, InputFunc layer, string enemy_ies)
        {
            Parameters _p = new Parameters{count = count, image = image, health = health, healthType = healthType, position = position, rotation = rotation, scale = scale, damageAward = damageAward, killAward = killAward, layer = layer};
            _p.c = blackboard.GetFloat(_p.count(), 1f).Value;

            List<object> enemies = new List<object>();

            for (_p.i = 0; _p.i < _p.c; _p.i++)
            {
                foreach (object enemyPosition in blackboard.GetList(_p.position(_p.c, _p.i), new List<object>()))
                {
                    if (enemyPosition is Vector2)
                        enemies.Add(NewEnemy(blackboard, (Vector2)enemyPosition, _p));
                    if (enemyPosition is Enemy)
                        enemies.Add(NewEnemy(blackboard, (enemyPosition as Enemy).TRSPosition, _p));
                }
            }
            blackboard.SetValue(enemy_ies, enemies);

            yield break;
            
        }

        private static Enemy NewEnemy(Blackboard blackboard, Vector2 position, Parameters _p)
        {
            Vector3 layeredPosition = position;
            layeredPosition.z = blackboard.GetFloat(_p.layer(_p.c, _p.i), 1f).Value;

            var enemyImage = blackboard.GetEnum(_p.image(), EnemyImage.Enemy2);
            var rotation = blackboard.GetFloat(_p.rotation(_p.c, _p.i), 0f).Value;
            var health = blackboard.GetFloat(_p.health(_p.c, _p.i), 0f).Value;
            var healthType = blackboard.GetEnum(_p.healthType(), HealthType.Normal);
            var scale = blackboard.GetVector(_p.scale(_p.c, _p.i), Vector2.one).Value;
            var killAward = blackboard.GetFloat(_p.killAward(_p.c, _p.i), 1f).Value;

            Enemy enemy = Game.Level.SpawnEnemy()
                .SetImage(enemyImage)
                .SetPosition(layeredPosition)
                .SetRotation(rotation)
                .SetHealth(Mathf.RoundToInt(health))
                .SetHealthType(healthType)
                .SetScale(scale);
            if (healthType == HealthType.Normal)
            {
                enemy.SetKillAward(Mathf.RoundToInt(killAward));
            }
            if (healthType == HealthType.Invulnerable)
            {
                enemy.SetDamageAward(Mathf.RoundToInt(killAward), health);
            }
            return enemy;
        }
    }
}