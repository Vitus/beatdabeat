﻿using UnityEngine;
using System.Collections;

namespace LevelParserV3 {
    public class LevelPlayer : MonoBehaviour {
        public static LevelPlayer Main { get; private set; }
        public bool IsBoss { get; set; }
        public LevelScript Level { get; private set; }

        [SerializeField] private float StartTime;
        [SerializeField] private float SkipSpeed;

        [SerializeField] private float FlyTime;
        [SerializeField] private float EmergencyTime;
        [SerializeField] private float OMGTime;

        public GameObject HoldToPlay;
        public TutorialReward TutorialReward;

        public int AutoSkipTime;

        public tk2dSprite BackgroundColorSprite;

        public bool SkipIntroInEditor;

        public bool FirstMusicLap;

        public bool TutorialRewardWasCollected {
            get { return PlayerPrefs.GetInt("TutorialRewardWasCollected", 0) == 1; }
            set { PlayerPrefs.SetInt("TutorialRewardWasCollected", value ? 1 : 0); }
        }

        public int LaunchCount {
            get { return PlayerPrefs.GetInt("LaunchCount", 0); }
            private set { PlayerPrefs.SetInt("LaunchCount", value); }
        }

        public bool LevelLaunched { get; set; }

        public bool SkipIntro {
            get {
                return SkipIntroInEditor && Application.platform == RuntimePlatform.WindowsEditor &&
                       Game.DevelopementBuild;
            }
        }

        private void OnEnable() {
            NEvent.AddListener<MusicCompleteEvent>(Game.GameObject, OnMusicComplete);
        }

        private void OnDisable() {
            NEvent.RemoveListener<MusicCompleteEvent>(Game.GameObject, OnMusicComplete);
        }

        private void Awake() {
            if (!false) {
                HealthManager.Instance.CurrentShipHealth.Current = HealthManager.Instance.CurrentShipHealth.AtStart;
            }
            Main = this;
            NEvent.Dispatch(Game.GameObject, new StartLevelEvent());
        }

        private IEnumerator Start() {
            ShopMenu.AdShown = false;
            DanceManAnimator.Main.BPM = 0;
            yield return new WaitForEndOfFrame();
            BossHealthBar.Main.SetMax(1000000);
            BossHealthBar.Main.Show(false);
            StartCoroutine(LoadCurrentLevel());

            yield return new WaitForSeconds(AutoSkipTime);
            OnMusicComplete(null);
        }

        private IEnumerator MakeScreenshots() {
            while (true) {
                while (!LevelLaunched) {
                    yield return null;
                }
                yield return this.WaitForUnscaledSeconds(5f);
            }
        }

        public IEnumerator LoadCurrentLevel() {
            LevelLaunched = false;
            StopAll();

            if (Game.Level.CurrentLevel == 0 && Game.Level.IsPractice == false) {
                LaunchCount++;
            }
            if (!Tutorial.IsTutorialSkiped &&
                !TutorialRewardWasCollected &&
                Tutorial.IsAllTutorialCompleted &&
                Game.Level.CurrentLevel == 1 &&
                LaunchCount > 3) {
                ShowTutorialReward();
                while (TutorialReward.gameObject.activeSelf) {
                    yield return null;
                }
            }

            if (!SkipIntro) {
                Game.Music.Play("WarpDrive");
                yield return new WaitForSeconds(2.5f);
                NEvent.Dispatch(Game.GameObject, new StarSpeedEvent(10f, Color.white, 1f));
                ChangeTrailSize(3f);
                yield return new WaitForEndOfFrame();
            }
            Level = LoadLevel();
            IsBoss = Game.Level.Bosses[Game.Level.CurrentLevel] != null;
            if (!SkipIntro) {
                Instantiate(Game.Level.NextLevelBanner);
                float loadStart = Time.time;

                yield return null;
                Game.Music.Volume = Game.Music.MaxVolume;
                float timeLeft = 6.2f - 2.5f - (Time.time - loadStart);
                yield return new WaitForSeconds(timeLeft);

                NEvent.Dispatch(Game.GameObject, new StarSpeedEvent(1f, Color.white, 1f));
                ChangeTrailSize(1/3f);
                yield return new WaitForSeconds(3f);
            }
            if (IsBoss) {
                if (!SkipIntro) {
                    GameObject emergency;
                    if ((Game.Level.CurrentLevel != Game.Level.LevelScripts.Length - 1) || false) {
                        Game.Sound.PlaySound(SoundType.GameEmergency);
                        emergency = Instantiate(Game.Level.EmergencyPrefab);
                        yield return new WaitForSeconds(EmergencyTime);
                        Destroy(emergency);
                        AppearBoss();
                        Instantiate(Game.Level.OMG);
                        yield return new WaitForSeconds(OMGTime);
                    } else {
                        emergency = Instantiate(Game.Level.BigEmeregencyPrefab);
                        yield return new WaitForSeconds(1f);
                        Game.Sound.PlaySound(SoundType.GameVSEmergency);
                        AppearBoss();
                        yield return new WaitForSeconds(10f);
                        Destroy(emergency);
                    }
                } else {
                    AppearBoss();
                    yield return new WaitForSeconds(OMGTime);
                }
            }
            FirstMusicLap = true;
            Initialize();
        }

        private static LevelScript LoadLevel() {
            var levelScript = Game.Level.CurrentLevelScript;
            return levelScript;
        }

        private void ChangeTrailSize(float multiplier) {
            foreach (ParticleSystem trail in FindObjectsOfType(typeof (ParticleSystem))) {
                if (trail.name == "Trail") {
                    trail.startLifetime *= multiplier;
                }
            }
        }

        private void AppearBoss() {
            Enemy boss = Instantiate(Game.Level.Bosses[Game.Level.CurrentLevel]) as Enemy;
            boss.SetHealthType(HealthType.OnBackground);
            boss.name = "Boss";
            boss.transform.position = new Vector3(0, 350);
            iTween.MoveTo(boss.gameObject,
                iTween.Hash(I.Position, new Vector3(0, 170), I.Time, OMGTime, I.EaseType, iTween.EaseType.linear));
        }

        private void Initialize() {
            StopAll();

            if (LevelScript.Player != null) {
                Destroy(LevelScript.Player.gameObject);
            }

            Level.Play();
            if (StartTime != 0 && Game.DevelopementBuild) {
                this.PerformCoroutine(SkipTime());
            }
            LevelLaunched = true;
        }

        public void ShowTutorialReward() {
            TutorialReward.gameObject.SetActive(true);
        }

        private IEnumerator SkipTime() {
            float initTimeScale = Game.DifficultyManager.Speed;
//            Game.Music.Volume = 0f;
            Game.DifficultyManager.Speed = SkipSpeed;
            Game.Player.Focus.FastCurrentToTarget();
            bool observerBuild = Game.Cheat.Observer;
            Game.Cheat.Observer = true;
//            NelliFader.Instance.FadeIn(0.01f);
            while (Game.Music.Time < StartTime) {
                yield return null;
            }
//            NelliFader.Instance.FadeOut(0.01f);
            Game.Cheat.Observer = observerBuild;
//            Game.Music.Volume = Game.Music.MaxVolume;
            Game.DifficultyManager.Speed = initTimeScale;
            Game.Player.Focus.FastCurrentToTarget();
        }

        private void OnMusicComplete(MusicCompleteEvent e) {
            if (Game.Music.GetClip() == "WarpDrive" || !LevelLaunched) {
                return;
            }

            if (IsBoss) {
                RestartMusic();
            } else {
                Game.Level.Win();
            }
        }

        private void RestartMusic() {
            FirstMusicLap = false;
            Initialize();
        }

        private void StopAll() {
            Game.Music.Stop();
        }
    }
}