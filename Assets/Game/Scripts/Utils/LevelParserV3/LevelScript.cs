﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public delegate object InputFunc(float c = 0, float i = 0, float t = 0, float l = 0, float castc = 0, float casti = 0);

namespace LevelParserV3 {
    public abstract class LevelScript {
        public static ScriptPlayer Player;

        public void Play() {
            if (Player != null) {
                GameObject.Destroy(Player.gameObject);
            }
            Player = new GameObject("ScriptPlayer").AddComponent<ScriptPlayer>();
            d = Game.Difficulty;
            StartLevel();
        }

        protected abstract void StartLevel();

        protected static IEnumerator Wait(float time) {
            while (Game.Music.Time < time) {
                yield return null;
            }
        }

        protected static void SubscribeInput(Blackboard outboard, Blackboard blackboard, InputFunc input, string nodeOutput) {
            object inputValue = input();
            if (inputValue is string && (inputValue as string).Contains(".output_")) {
                outboard.SubscribeForChanges(inputValue as string,
                    (b, o) => blackboard.SetValue(nodeOutput, o));
                blackboard.SetValue(nodeOutput, outboard.GetValue(inputValue as string));
            }
            else {
                blackboard.SetValue(nodeOutput, inputValue);
            }
        }

        protected static void SubscribeOutput(Blackboard outboard, Blackboard blackboard, string output, string nodeOutput) {
            blackboard.SubscribeForChanges(nodeOutput, (b, o) => outboard.SetValue(output, o));
            outboard.SetValue(output, blackboard.GetValue(nodeOutput));
        }

        public static float ar {
            get { return Game.AspectDelta; }}

        public static float shw {
            get { return (float) 320*ar; }}

        protected static float d { get; private set; }

        protected static float toint(float first, float second) {
            return (int) (first/second);
        }

        protected static float rndf(float min, float max) {
            return Random.Range(min, max);
        }

        protected static float rndi(float min, float max) {
            return Random.Range((int) min, (int) max);
        }

        protected static float sint(float t) {
            return Mathf.Sin(t*2*Mathf.PI);
        }

        protected static float sin(float t) {
            return Mathf.Sin(t*Mathf.Deg2Rad);
        }

        protected static float cos(float t) {
            return Mathf.Cos(t*Mathf.Deg2Rad);
        }

        protected static void SubscribeVariable(Blackboard blackboard, InputFunc i1, InputFunc i2, InputFunc i3, InputFunc i4, InputFunc i5, InputFunc i6, InputFunc i7, InputFunc i8,
            string nodeOutputValue) {
            InputFunc[] inputs = {i1, i2, i3, i4, i5, i6, i7, i8};
            foreach (InputFunc input in inputs) {
                object inputValue = input();
                if (inputValue is string && (inputValue as String).Contains(".output_")) {
                    blackboard.SubscribeForChanges(inputValue as string,
                        delegate(Blackboard b, object o) {
                            if (o != null) {
                                blackboard.SetValue(nodeOutputValue, o);
                            }
                        });
                }
                else {
                    blackboard.SetValue(nodeOutputValue, inputValue);
                }
            }
        }

        protected static void SubscribeListVariable(Blackboard blackboard, InputFunc i1, InputFunc i2, InputFunc i3, InputFunc i4, InputFunc i5, InputFunc i6, InputFunc i7, InputFunc i8,
            string nodeOutputValue) {
            InputFunc[] inputs = {i1, i2, i3, i4, i5, i6, i7, i8};
            foreach (InputFunc input in inputs) {
                Action<Blackboard, object> addAction = delegate(Blackboard b, object o) {
                    if (o != null) {
                        List<object> currentValue;
                        if (blackboard.HasValue(nodeOutputValue)) {
                            currentValue = (List<object>) blackboard.GetValue(nodeOutputValue);
                        }
                        else {
                            currentValue = new List<object>();
                        }

                        if (o is IList) {
                            foreach (object item in (o as IList)) {
                                currentValue.Add(item);
                            }
                        }
                        else {
                            currentValue.Add(o);
                        }

                        blackboard.SetValue(nodeOutputValue, currentValue);
                    }
                };
                object inputValue = input == null ? null : input();

                if (inputValue is string && (inputValue as String).Contains(".output_")) {
                    blackboard.SubscribeForChanges(inputValue as string, addAction);
                }
                else {
                    addAction(blackboard, inputValue);
                }
            }
        }

        protected abstract string ClipName { get; }
    }
}