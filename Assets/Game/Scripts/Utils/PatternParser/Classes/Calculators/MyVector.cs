﻿using UnityEngine;

namespace LevelParserV2
{
    public class MyVector : ITRSObject
    {
        public static MyVector Zero
        {
            get 
            { 
                if (_zero == null)
                {
                    _zero = new MyVector{X = 0, Y = 0};
                }
                return _zero; 
            }
        }

        public static MyVector One
        {
            get 
            { 
                if (_one == null)
                {
                    _one = new MyVector { X = 1, Y = 1 };
                }
                return _one; 
            }
        }

        public float X;
        public float Y;
        private static MyVector _zero;
        private static MyVector _one;

        public MyVector(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public MyVector()
        {
        }

        public static MyVector FromVector(Vector2 vector2)
        {
            return new MyVector() {X = vector2.x, Y = vector2.y};
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }

        public static implicit operator Vector2(MyVector value)
        {
            return value.ToVector2();
        }

        public static implicit operator MyVector(Vector2 value)
        {
            return FromVector(value);
        }

        public Vector2 TransformPosition
        {
            get { return ToVector2(); }
        }

        public float TransformAngle
        {
            get { return 0; }
        }

        public Vector2 TransformScale 
        {
            get { return Vector2.one; }
        }

        public Vector2 TRSPosition
        {
            get { return ToVector2(); }
        }
        public float TRSAngle
        {
            get { return 0; }
        }
        public Vector2 TRSScale
        {
            get { return Vector2.one; }
        }
        public bool IsAlive()
        {
            return true;
        }
    }
}