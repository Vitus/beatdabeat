﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
    private static T _instance;

    private static object _lock = new object();
    private static bool applicationIsQuitting = false;
    private static GameObject _holder;

    public static T Instance {
        get {
            if (applicationIsQuitting) {
                Debug.LogWarning("[Singleton] Instance '" + typeof (T) +
                                 "' already destroyed on application quit." +
                                 " Won't create again - returning null.");
                return null;
            }

            if (_instance == null) {
                CreateInstance();
            }
            return _instance;
        }
    }

    protected static void CreateInstance() {
        lock (_lock) {
            _instance = (T) FindObjectOfType(typeof (T));

            if (FindObjectsOfType(typeof (T)).Length > 1) {
                Debug.LogError("[Singleton] Something went really wrong " +
                               " - there should never be more than 1 singleton!" +
                               " Reopening the scene might fix it.");
            }

            if (_instance == null) {
//                GameObject singletonHolder = FindHolderGameObject();
                GameObject singletonGameObject = new GameObject();
                _instance = singletonGameObject.AddComponent<T>();
                singletonGameObject.name = typeof (T).ToString();
//                singletonGameObject.transform.parent = singletonHolder.transform;

                DontDestroyOnLoad(singletonGameObject);
            }
        }
    }

    private static GameObject FindHolderGameObject() {
        _holder = GameObject.Find("Singletons");
        if (_holder == null) {
            _holder = new GameObject("Singletons");
            DontDestroyOnLoad(_holder);
        }
        return _holder;
    }

    protected virtual void OnDestroy() {
        applicationIsQuitting = true;
    }
}