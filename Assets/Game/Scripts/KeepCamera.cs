﻿using UnityEngine;
using System.Collections;

public class KeepCamera : MonoBehaviour {

    IEnumerator Start()
	{
        while (true) {
            Camera.main.transform.localPosition = Vector3.zero;
            float elapsed = 0f;
            float timeScale = Time.timeScale == 0 ? 0.001f : Time.timeScale;
            while (elapsed < 0.05f*timeScale) {
                elapsed += Time.deltaTime;
                yield return null;
            }
        }
    }

    void LateUpdate()
    {
        var newPosition = Game.Player.Ship.transform.position;
        transform.localPosition = new Vector3(newPosition.x*0.1f, newPosition.y*0.1f, 0);
    }
}
