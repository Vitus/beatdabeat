﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
    [SerializeField] private float RotateSpeed;
    [SerializeField] private float RotateSpeedX;
    [SerializeField] private float RotateSpeedY;
    [SerializeField] private bool IgnoreTimeScale = false;

    private void Update() {
        if (IgnoreTimeScale) {
            transform.Rotate(RotateSpeedX*Time.unscaledDeltaTime, RotateSpeedY*Time.unscaledDeltaTime, RotateSpeed*Time.unscaledDeltaTime);
        }
        else {
            transform.Rotate(RotateSpeedX*Time.deltaTime, RotateSpeedY*Time.deltaTime, RotateSpeed*Time.deltaTime);
        }
    }
}