﻿using UnityEngine;
using System.Collections;

public class DestroyOnAnimationFinish : MonoBehaviour
{
    private tk2dSpriteAnimator _animator;
    public bool rotate = true;

    void Start()
    {
        if (rotate)
            transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
        _animator = GetComponent<tk2dSpriteAnimator>();
    }

	void Update () 
    {
	    if (!_animator.Playing)
	    {
	        Destroy(gameObject);
	    }
	}
}
