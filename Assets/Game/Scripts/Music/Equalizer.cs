﻿using System.Collections.Generic;
using UnityEngine;

public class Equalizer : MonoBehaviour {

    public GameObject EqualizerBarPrefab;

    public int BarCount;
    [Range(0.01f, 3f)]
    public float AudioTime;
    [Range(6,13)]
    public int FFTLevel;
    public float MinFrequency;
    public float MaxFrequency;
    public float Width;
    public float SampleScale;
    
    private AudioSource _source;
    private Queue<float[]> _samplesQueue = new Queue<float[]>();
    private float[] _samplesSum;
    private int _currentFFTLevel = -1;
    private GameObject[] _equalizerBars = new GameObject[0];
    private int _FFTSize;

    public float QueueSize {
        get {
            return _source.clip.frequency * AudioTime /_FFTSize;
        }
    }

    public float BarWidth {
        get { return Width/BarCount; }
    }

    public float FrequencyStep {
        get { return (MaxFrequency - MinFrequency)/BarCount; }
    }

    private void Awake() {
        Width *= Camera.main.aspect/1.333333333f;
        _source = Game.Music.Source;
        RecreateBars();
    }

    private void Update() {
        if (_source == null || _source.clip == null)
            return;
        if (FFTLevel != _currentFFTLevel) {
            UpdateFFTLevel();
        }
        ClampFrequencyIndexes();
        RecreateBars();
        UpdateQueue();
        CalculateFrequencies();
        Draw();
    }

    private void UpdateFFTLevel() {
        if (FFTLevel < 6)
            FFTLevel = 6;
        if (FFTLevel > 13)
            FFTLevel = 13;
        _currentFFTLevel = FFTLevel;
        _FFTSize = Mathf.RoundToInt(Mathf.Pow(2, _currentFFTLevel));
        _samplesSum = new float[_FFTSize];
        _samplesQueue.Clear();
        UpdateQueue();
    }

    private void ClampFrequencyIndexes() {
        MaxFrequency = Mathf.Clamp(MaxFrequency, 0, Index2Freq(_FFTSize - 1, _source.clip.frequency, _FFTSize));
        MinFrequency = Mathf.Clamp(MinFrequency, 0, MaxFrequency);
    }

    private void RecreateBars() {
        if (BarCount < 1) {
            BarCount = 1;
        }
        if (_equalizerBars.Length != BarCount) {
            foreach (GameObject bar in _equalizerBars) {
                Destroy(bar);
            }
            _equalizerBars = new GameObject[BarCount];
            for (int i = 0; i < BarCount; i++) {
                _equalizerBars[i] = Instantiate(EqualizerBarPrefab) as GameObject;
                _equalizerBars[i].transform.SetParent(transform);
                _equalizerBars[i].transform.localPosition = new Vector3((i + 0.5f) * BarWidth - Width / 2f, 0);
                _equalizerBars[i].transform.localScale = new Vector3(BarWidth, 0);
            }
        }
    }

    private void UpdateQueue() {
        if (AudioTime < 0.001f) {
            AudioTime = 0.001f;
        }
        while (QueueSize + 1 < _samplesQueue.Count) {
            Dequeue();
        }
        while (QueueSize + 1 > _samplesQueue.Count) {
            Enqueue(new float[_FFTSize]);
        }
    }

    private void CalculateFrequencies() {
        var samples = Dequeue();
        _source.GetSpectrumData(samples, 0, FFTWindow.BlackmanHarris);
        Enqueue(samples);
    }

    private void Draw() {
        for (int i = 0; i < BarCount; i++) {
            _equalizerBars[i].transform.localScale = new Vector3(BarWidth, GetFrequencyValue(i*FrequencyStep + MinFrequency)*SampleScale);
//            _equalizerBars[i].transform.localPosition = new Vector3((i + 0.5f)*BarWidth - Width/2f, 0);
        }
    }

    private void Enqueue(float[] samples) {
        for (int i = 0; i < _samplesSum.Length; i++) {
            _samplesSum[i] += samples[i];
        }
        _samplesQueue.Enqueue(samples);
    }

    private float[] Dequeue() {
        float[] samples = _samplesQueue.Dequeue();

        for (int i = 0; i < _samplesSum.Length; i++) {
            _samplesSum[i] -= samples[i];
        }
        return samples;
    }

    private float GetFrequencyValue(float frequency) {
        float indexFloat = Freq2IndexFloat(frequency, _source.clip.frequency, _FFTSize);
        int i1 = Mathf.FloorToInt(indexFloat);
        int i2 = i1 + 1;
        float t = indexFloat - i1;
        return (Interpolate(_samplesSum[i1] - _samplesQueue.Peek()[i1] * (QueueSize - (int)QueueSize), _samplesSum[i2] - _samplesQueue.Peek()[i2] * (QueueSize - (int)QueueSize), t)) / (int)(QueueSize+1);
    }

    public static float Index2Freq(int i, float sampleRate, int nFFT) {
        return (float) i*(sampleRate/nFFT/2.0f);
    }

    public static int Freq2Index(float freq, float sampleRate, int nFFT) {
        return (int) (freq/(sampleRate/nFFT/2.0f));
    }

    public static float Freq2IndexFloat(float freq, float sampleRate, int nFFT) {
        return (freq/(sampleRate/nFFT/2.0f));
    }

    private float Interpolate(float y1, float y2, float t) {
        float t2 = (1 - Mathf.Cos(t*Mathf.PI))/2;
        return (y1*(1 - t2) + y2*t2);
    }
}