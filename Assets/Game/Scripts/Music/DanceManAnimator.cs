﻿using UnityEngine;

[RequireComponent(typeof (tk2dSpriteAnimator))]
public class DanceManAnimator : MonoBehaviour
{
    private tk2dSpriteAnimator _animator;

    public static DanceManAnimator Main;
    private DanceManMovement _currentMovement;
    private float _bpm;
    private bool _died;

    public DanceManMovement CurrentMovement
    {
        get { return _currentMovement; }
        set
        {
            if (_currentMovement != value)
            {
                _currentMovement = value;
                Play((int) _currentMovement);
            }
        }
    }

    public float BPM
    {
        get { return _bpm; }
        set
        {
            _bpm = value;
            if (_bpm == 0)
                NoAnimation();
            if (_animator.ClipFps > 0.1f)
                _animator.ClipFps = _animator.CurrentClip.fps*_bpm/60;
        }
    }

    private void NoAnimation()
    {
        _animator.Play("DanceStand");
    }

    private void Awake()
    {
        Main = this;
        _animator = GetComponent<tk2dSpriteAnimator>();
    }

    public void Die()
    {
        _died = true;
        _animator.Play("Dance_died");
    }

    public void Revive()
    {
        _died = false;
        Play((int)CurrentMovement);
    }

    public void Play(int clipNum)
    {
        if (_died)
            return;

        if (clipNum == 0)
        {
            _animator.Play(_animator.GetClipByName("Dance7"), 0f, 0.001f);
        }
        else
        {
            _animator.Play("Dance" + clipNum);
        }
        if (_animator.ClipFps > 0.1f)
            _animator.ClipFps = _animator.CurrentClip.fps*_bpm/60;
    }
}