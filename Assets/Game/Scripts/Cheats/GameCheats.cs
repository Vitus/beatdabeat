﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions {
    [Category("Game")]
    [DisplayName("Spectator")]
    public bool Cheat_Observer {
        get { return Game.Cheat.Observer; }
        set { Game.Cheat.Observer = value; }
    }

    [Category("Game")]
    [DisplayName("Game Speed")]
    [NumberRange(0.5f, 10f)]
    [Increment(0.05)]
    [AutoUpdate]
    public float Cheat_GameSpeed {
        get { return Game.DifficultyManager.Speed; }
        set { Game.DifficultyManager.Speed = value; }
    }

    [Category("Game")]
    [DisplayName("Normal Speed")]
    public void ResetGameSpeed() {
        Game.DifficultyManager.Speed = 1f;
    }

    [Category("Game")]
    [DisplayName("AddBomb")]
    public void AddBomb() {
        if (Game.Player.Ship != null) {
            Game.Player.Ship.BombCount++;
        }
    }

    [Category("Game")]
    [DisplayName("Infinite Bombs")]
    [AutoUpdate]
    public bool Cheat_InfiniteBomb {
        get { return Game.Cheat.InfiniteBomb; }
        set { Game.Cheat.InfiniteBomb = value; }
    }

    [Category("Game")]
    [DisplayName("Infinite Health")]
    [AutoUpdate]
    public bool Cheat_InfiniteHealth {
        get { return Game.Cheat.InfiniteHealth; }
        set { Game.Cheat.InfiniteHealth = value; }
    }
}