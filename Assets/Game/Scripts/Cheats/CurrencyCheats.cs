﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions {
    [Category("Currency")]
    [DisplayName("Chips")]
    [NumberRange(0, int.MaxValue)]
    [Increment(10)]
    [AutoUpdate]
    public int Cheat_Currency {
        get { return Currency.Current; }
        set { Currency.Current = value; }
    }

    [Category("Currency")]
    [DisplayName("Coins")]
    [NumberRange(0, int.MaxValue)]
    [Increment(100)]
    [AutoUpdate]
    public int Cheat_Money {
        get { return Game.Money.Current; }
        set { Game.Money.Current = value; }
    }
}