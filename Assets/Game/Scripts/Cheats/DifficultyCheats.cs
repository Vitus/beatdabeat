﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions {
    [Category("Beat Difficulty")]
    [DisplayName("Casual")]
    public void BeatCasual() {
        Game.Level.SetMaxUnlockedLevel(1, 8);
    }

    [Category("Beat Difficulty")]
    [DisplayName("Normal")]
    public void BeatNormal() {
        Game.Level.SetMaxUnlockedLevel(2, 8);
    }

    [Category("Beat Difficulty")]
    [DisplayName("Hard")]
    public void BeatHard() {
        Game.Level.SetMaxUnlockedLevel(3, 8);
    }

    [Category("Beat Difficulty")]
    [DisplayName("Badass")]
    public void BeatBadass() {
        Game.Level.SetMaxUnlockedLevel(4, 8);
    }

    [Category("Beat Difficulty")]
    [DisplayName("Все")]
    public void BeatAll() {
        Game.Level.SetMaxUnlockedLevel(1, 8);
        Game.Level.SetMaxUnlockedLevel(2, 8);
        Game.Level.SetMaxUnlockedLevel(3, 8);
        Game.Level.SetMaxUnlockedLevel(4, 8);
    }
}