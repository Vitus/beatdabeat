﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions {
    [Category("Reset")]
    [DisplayName("Reset")]
    public void Cheat_Reset() {
        PlayerPrefs.DeleteAll();
    }

    [Category("Reset")]
    [DisplayName("Reset, skip Tutorial")]
    public void Cheat_ResetAndSkipTutorial() {
        PlayerPrefs.DeleteAll();
        Tutorial.IsTutorialSkiped = true;
        Game.Level.FirstTimePlay = false;
    }

    [Category("Reset")]
    [DisplayName("Reset, skip Control Tutorial")]
    public void Cheat_ResetAndSkipControlTutorial() {
        PlayerPrefs.DeleteAll();
        Tutorial.IsControlTutorialCompleted = true;
        Game.Level.FirstTimePlay = false;
    }

    [Category("Reset")]
    [DisplayName("Buy all ships")]
    public void UnlockAllShips() {
        foreach (ShopShip ship in Game.Upgrade.Ships) {
            ship.CheatUnlock();
        }
    }
}