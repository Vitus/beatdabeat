﻿using System;
using System.CodeDom;
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Reflection;

[AttributeUsage(AttributeTargets.Property)]
public class AutoUpdateAttribute : Attribute {
}

public class CheatPropertyUpdate : Singleton<CheatPropertyUpdate> {
    private void Start() {
        Assembly assembly = Assembly.GetAssembly(typeof(CheatPropertyUpdate));
        PropertyInfo[] autoUpdateProperties = assembly.GetTypes()
                      .SelectMany(t => t.GetProperties())
                      .Where(m => m.GetCustomAttributes(typeof(AutoUpdateAttribute), false).Length > 0)
                      .ToArray();

        foreach (PropertyInfo property in autoUpdateProperties) {
            CheckProperty(() => property.GetValue(SROptions.Current, null), property.Name);
        }
    }

    private void CheckProperty(Func<object> propertyGetter, string propertyName) {
        StartCoroutine(CheckPropertyCoroutine(propertyGetter, propertyName));
    }


    private IEnumerator CheckPropertyCoroutine(Func<object> propertyGetter, string propertyName) {
        if (Game.DevelopementBuild) {
            object difficultySpeed = propertyGetter();
            while (true) {
                if (difficultySpeed != propertyGetter()) {
                    difficultySpeed = propertyGetter();
                    SROptions.Current.OnPropertyChanged(propertyName);
                }
                yield return null;
            }
        }
    }

    protected CheatPropertyUpdate() {
    }

    [RuntimeInitializeOnLoadMethod]
    public static void Init() {
        CreateInstance();
    }
}