﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions {
    [Category("Health")]
    [DisplayName("Regain 1")]
    public void Health_ReplenishHealth() {
        HealthManager.Instance.CurrentShipHealth.Replenish(1);
    }

    [Category("Health")]
    [DisplayName("Regain 3")]
    public void Health_Replenish3Health() {
        HealthManager.Instance.CurrentShipHealth.Replenish(3);
    }

    [Category("Health")]
    [DisplayName("Decrease")]
    public void Health_DecreaseHealth() {
        HealthManager.Instance.CurrentShipHealth.Deplete();
    }
}