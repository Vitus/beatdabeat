﻿using UnityEngine;
using System.Collections;

public class DonutEffects : MonoBehaviour {
    private bool _doubleUltraCoin;
    private static DonutEffects _instance;

    private void Awake() {
        _instance = this;
    }

    public static bool DoubleUltraCoin {
        get {
            return _instance != null && _instance._doubleUltraCoin;
        }
        set {
            if (_instance != null) {
                _instance._doubleUltraCoin = value;
            }
        }
    }
}