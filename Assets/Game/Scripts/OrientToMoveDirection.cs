﻿using UnityEngine;
using System.Collections;

public class OrientToMoveDirection : MonoBehaviour
{
    private Vector2 _previousPosition;

    void Start()
    {
        _previousPosition = transform.position;
    }

    void Update ()
    {
        float angle = H.VectorAngle(_previousPosition - (Vector2)transform.position);
        transform.rotation = Quaternion.Euler(0f, 0f, angle);
        _previousPosition = transform.position;
    }
}
