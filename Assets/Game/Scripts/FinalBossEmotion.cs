﻿using UnityEngine;
using System.Collections;

public class FinalBossEmotion : MonoBehaviour
{
    public int ButtonStartFrame;
    public int ButtonEndFrame;

    IEnumerator Start()
    {
        FlashSprite sprite = GetComponent<FlashSprite>();
        while (sprite.CurrentFrame < 109)
        {
            yield return null;
        }
        sprite.CurrentFrame = ButtonStartFrame;
        while (sprite.CurrentFrame < ButtonEndFrame)
        {
            yield return null;
        }
        sprite.CurrentFrame = 218;
        while (!sprite.Finished())
        {
            yield return null;
        }
        Destroy(transform.parent.gameObject);
    }
}
