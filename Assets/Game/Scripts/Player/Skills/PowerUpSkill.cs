﻿using UnityEngine;
using System.Collections;

public class PowerUpSkill : ActiveSkill {

    public GameObject[] DefaultWeapons;
    public GameObject[] PoweredWeapons;
    public float SkillTime;

    public override void OnActivate() {
        StartCoroutine(ChangeWeapon());
    }

    private IEnumerator ChangeWeapon() {
        IsActivated = true;
        Game.Sound.PlaySound(SoundType.GameDmgUp);
        foreach (var defaultWeapon in DefaultWeapons) {
            defaultWeapon.SetActive(false);
        }
        foreach (var poweredWeapon in PoweredWeapons) {
            poweredWeapon.SetActive(true);
        }

        yield return StartCoroutine(DepleteEnergy(SkillTime));

        foreach (var defaultWeapon in DefaultWeapons)
        {
            defaultWeapon.SetActive(true);
        }
        foreach (var poweredWeapon in PoweredWeapons)
        {
            poweredWeapon.SetActive(false);
        }
        IsActivated = false;
    }


}
