﻿using System.Linq;
using JetBrains.Annotations;
using LevelParserV3;
using UnityEngine;
using System.Collections;

public class MetamorphSkill : MonoBehaviour {
    private const string MetamorphID = "Metamorph";

    public float MorphTime;

    public float AnimationDuration;
    public tk2dSprite Flash;
    private bool _isMorphing;

    public string CurrentShipID { get; set; }

    private void Awake() {
        CurrentShipID = MetamorphID;
    }

    private void Start() {
        Invoke("Morph", MorphTime);
    }

    private void Morph() {
        if (!_isMorphing) {
            StartCoroutine(MorphProcess(GetRandomShip()));
        }
    }

    private void Update() {
        if (Flash != null) {
            Flash.transform.position = Vector3.zero + Flash.transform.position.z*Vector3.forward;
        }
        if (!_isMorphing && !LevelPlayer.Main.LevelLaunched && CurrentShipID != MetamorphID) {
            StartCoroutine(MorphProcess(GetMetamorphShip()));
        }
        else if (!_isMorphing && LevelPlayer.Main.LevelLaunched && CurrentShipID == MetamorphID) {
            StartCoroutine(MorphProcess(GetRandomShip()));
        }
    }

    private IEnumerator MorphProcess(ShopShip shopShip) {
        _isMorphing = true;
        var newShipPrefab = shopShip.Prefab;
        var curHealth = GetComponent<PlayerHealth>();
        var curActiveSkill = GetComponent<ActiveSkill>();

        while (curHealth.IsShielded || curActiveSkill.IsActivated || curHealth.Current <= 0) {
            yield return null;
        }

        FlashSprite[] sprites = GetComponentsInChildren<FlashSprite>();

        Game.Sound.PlaySound(SoundType.GameMetamorph);

        Flash.gameObject.SetActive(true);
        yield return StartCoroutine(BecomeWhite(sprites, false));

        curHealth.HitTestEnabled = false;
        curActiveSkill.CanBeActivated = false;

        int currentHealth = Game.Player.Health.Current;
        int currentBombs = Game.Player.Ship.BombCount;
        float currentEnergy = Game.Player.Ship.GetComponent<ActiveSkill>().Energy;

        PlayerShip newShip = Instantiate(newShipPrefab, transform.position, transform.rotation) as PlayerShip;

        Game.Player.Ship = newShip;
        Game.Player.Ship.BombCount = currentBombs;
        Game.Player.Ship.GetComponent<ActiveSkill>().Energy = currentEnergy;
        Game.Player.Health = newShip.GetComponent<PlayerHealth>();
        Game.Player.Health.Replenish(currentHealth - Game.Player.Health.Current);

        MetamorphSkill metamorphSkill;
        if (newShip.gameObject.GetComponent<MetamorphSkill>() != null) {
            metamorphSkill = newShip.gameObject.GetComponent<MetamorphSkill>();
        }
        else {
            metamorphSkill = newShip.gameObject.AddComponent<MetamorphSkill>();
        }
        metamorphSkill.MorphTime = MorphTime;
        metamorphSkill.AnimationDuration = AnimationDuration;
        metamorphSkill.CurrentShipID = shopShip.ID;

        if (shopShip.ID == MetamorphID) {
            if (metamorphSkill.Flash != null) {
                Destroy(metamorphSkill.Flash.gameObject);
            }
        }
        metamorphSkill.Flash = Flash;
        Flash.transform.SetParent(Game.Player.Ship.transform, true);

        curHealth = metamorphSkill.GetComponent<PlayerHealth>();
        curActiveSkill = metamorphSkill.GetComponent<ActiveSkill>();

        curHealth.HitTestEnabled = false;
        curActiveSkill.CanBeActivated = false;

        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }

        foreach (var component in GetComponents<MonoBehaviour>()) {
            if (component != this) {
                Destroy(component);
            }
        }

        Flash.gameObject.SetActive(true);
        yield return StartCoroutine(BecomeWhite(sprites, true));
        Flash.gameObject.SetActive(false);

        curHealth.HitTestEnabled = true;
        curActiveSkill.CanBeActivated = true;

        Destroy(gameObject);
    }

    private IEnumerator BecomeWhite(FlashSprite[] sprites, bool backward) {
        Flash.color = Color.white.WithAlpha(backward ? 1 : 0);

        foreach (var flashSprite in sprites) {
            flashSprite.Color = backward ? Color.white : Color.black;
        }

        float elapsedTime = 0f;
        while (elapsedTime < AnimationDuration/2f) {
            elapsedTime += Time.deltaTime;
            float t = elapsedTime/(AnimationDuration/2f);
            if (backward) {
                t = 1 - t;
            }
            foreach (var flashSprite in sprites) {
                flashSprite.Color = Color.Lerp(Color.black, Color.white, t);
            }
            Flash.color = Color.white.WithAlpha(t);
            yield return null;
        }
    }

    private ShopShip GetMetamorphShip() {
        return Game.Upgrade.Ships.FirstOrDefault(ship => ship.ID == MetamorphID);
    }

    private ShopShip GetRandomShip() {
        ShopShip ship;
        do {
            ship = Game.Upgrade.UnlockedShips[Random.Range(0, Game.Upgrade.UnlockedShips.Count)];
        } while (ship.ID == MetamorphID || (ship.ID == CurrentShipID && Game.Upgrade.UnlockedShips.Count > 2));
        return ship;
    }

}