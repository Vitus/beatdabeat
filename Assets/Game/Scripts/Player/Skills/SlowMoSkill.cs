﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class SlowMoSkill : ActiveSkill {
    [SerializeField] private float SpeedMultiplier;
    [SerializeField] private float SkillTime;
    private bool _slowDown;


    public override void OnActivate() {
        StartCoroutine("SlowMo");
    }

    void OnDisable()
    {
        StopCoroutine("SlowMo");
        if (_slowDown)
        {
            Game.DifficultyManager.Speed /= SpeedMultiplier;
        }
    }

    private IEnumerator SlowMo() {
        IsActivated = true;
        Game.Sound.PlaySound(SoundType.GameSlowDown);
        Game.DifficultyManager.Speed *= SpeedMultiplier;
        _slowDown = true;
        yield return StartCoroutine(DepleteEnergy(SkillTime*SpeedMultiplier));
        Game.Sound.PlaySound(SoundType.GameSpeedUp);
        yield return new WaitForSeconds(0.1f);
        Game.DifficultyManager.Speed /= SpeedMultiplier;
        _slowDown = false;
        IsActivated = false;
    }
}
