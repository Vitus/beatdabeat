﻿using LevelParserV3;
using UnityEngine;
using System.Collections;

public class SpeedUpSkill : MonoBehaviour {

    public float SpeedMultiplier;
    private bool _levelWasLaunched;

    public GameObject HealthBonus;
    public GameObject BombBonus;

    void Awake() {
        _levelWasLaunched = false;
    }

    void OnEnable() {
        Game.DifficultyManager.Speed *= SpeedMultiplier;
    }

    void OnDisable() {
        if (Game.DifficultyManager != null) {
            Game.DifficultyManager.Speed /= SpeedMultiplier;
        }
    }

    void Update() {

        if (_levelWasLaunched && !LevelPlayer.Main.LevelLaunched) {
            SpawnHealth();
        }
        _levelWasLaunched = LevelPlayer.Main.LevelLaunched;
    }

    private void SpawnHealth() {
        if (Game.Upgrade.CurrentShip.ID != "Metamorph" && !Game.Level.IsPractice) {
            Game.Sound.PlaySound(SoundType.GamePowerupHealth);
            Instantiate(HealthBonus, new Vector3(-200, 200), Quaternion.identity);
            Instantiate(HealthBonus, new Vector3(200 ,200), Quaternion.identity);
            Instantiate(BombBonus, new Vector3(0, 200), Quaternion.identity);
        }
    }

}
