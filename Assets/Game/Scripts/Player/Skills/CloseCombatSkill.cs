﻿using LevelParserV3;
using NelliTweenEngine;
using UnityEngine;
using System.Collections;

public class CloseCombatSkill : MonoBehaviour {

    public LayerMask Target;
    public float DPS;
    public float BossDPS;
    public GameObject HealthBonusPrefabs;
    public int KillsBetweenBonus;
    public GameObject HitShine;
    public GameObject Shine;
    public tk2dSpriteAnimator VampireAnimation;

    private bool _isHitShineVisible;

    private static int _enemiesDestroyed;

    private void OnEnable() {
        transform.Tween(new TweenSettings<float>() {
            StartValue = 0.7f,
            EndValue = 1f,
            Easing = Ease.Linear,
            Duration = 0.1f,
            OnExecute = value => HitShine.transform.localScale = Vector3.one*value,
            LoopCount = -1,
            LoopType = LoopType.PingPong,
        });
    }

    private void OnEnemyDestroy() {
        _enemiesDestroyed++;
        if (_enemiesDestroyed >= KillsBetweenBonus) {
            _enemiesDestroyed -= KillsBetweenBonus;
            VampireAnimation.Play();
            Game.Sound.PlaySound(SoundType.GameVampire);
            Game.Player.Health.Replenish(1);
        }
    }

    private void OnTriggerStay(Collider other) {
        if ((Target & 1 << other.gameObject.layer) != 0) {
            _isHitShineVisible = true;
            bool isBoss = other.attachedRigidbody.gameObject.GetComponent<Boss>() != null;
            float upgradedDps = (isBoss ? BossDPS : DPS) * Mathf.Lerp(1f, 2f, (float) Game.Upgrade.CurrentShip.WeaponsLevel/Game.Upgrade.CurrentShip.MaxUpgradeLevel);
            NEvent.Dispatch(other.attachedRigidbody.gameObject, new HitEvent((int) (upgradedDps*Time.deltaTime), transform.position));
            Health enemyHealth = other.attachedRigidbody.GetComponent<Health>();
            if (enemyHealth.enabled && !enemyHealth.AsBoss && enemyHealth.Current == 0) {
                OnEnemyDestroy();
            }
        }
    }

    void Update() {
        Shine.SetActive(LevelPlayer.Main.LevelLaunched);
        HitShine.SetActive(_isHitShineVisible);
    }

    void FixedUpdate() {
        _isHitShineVisible = false;
    }

}