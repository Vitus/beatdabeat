﻿using UnityEngine;
using System.Collections;

public class DroneSkill : MonoBehaviour {

    public Transform Parent;
    public float DistanceToParent;

    private Vector3 ParentPrevPosition;
    private Vector3 SelfPrevPosition;

    void Awake() {
        ParentPrevPosition = Parent.transform.position;
        SelfPrevPosition = transform.position;
    }

    void Update() {
        Vector2 direction = ParentPrevPosition - SelfPrevPosition;
        float distance = direction.magnitude;
        direction.Normalize();
        transform.position = SelfPrevPosition + (Vector3)direction * Mathf.Clamp(distance - DistanceToParent, 0, float.MaxValue);
        ParentPrevPosition = Parent.transform.position;
        SelfPrevPosition = transform.position;
    }

}
