﻿using System.Collections.Generic;
using LevelParserV3;
using UnityEngine;
using System.Collections;

public class ThunderSkill : MonoBehaviour {

    public LayerMask Target;
    public float LightningRadius;
    public float DPS;
    public float BossDPS;

    public Material LineMaterial;
    public int vertexCount;
    public float Randomization;

    public LightningPosition LightningPrefab;

    private List<LightningPosition> _lines = new List<LightningPosition>();
    private int _nextLineIndex;
    public GameObject Shine;

    private void Update() {
        _nextLineIndex = 0;
        if (LevelPlayer.Main.LevelLaunched) {
            var colliders = Physics.OverlapSphere(transform.position, LightningRadius, Target);
            if (colliders.Length > 0) {
                Shine.SetActive(true);
            }
            else {
                Shine.SetActive(false);
            }
            foreach (var enemyCollider in colliders) {
                bool isBoss = enemyCollider.attachedRigidbody.gameObject.GetComponent<Boss>() != null;
                float upgradedDps = (isBoss ? BossDPS : DPS)*Mathf.Lerp(1f, 2f, (float) Game.Upgrade.CurrentShip.WeaponsLevel/Game.Upgrade.CurrentShip.MaxUpgradeLevel);
 
                NEvent.Dispatch(enemyCollider.attachedRigidbody.gameObject, new HitEvent((int) (upgradedDps*Time.deltaTime), transform.position));
                DrawLightning(enemyCollider.attachedRigidbody.transform);
            }
            for (int i = _nextLineIndex; i < _lines.Count; i++) {
                _lines[i].gameObject.SetActive(false);
            }
        }
        else {
            Shine.SetActive(false);
            for (int i = 0; i < _lines.Count; i++) {
                _lines[i].gameObject.SetActive(false);
            }
        }
    }

    private void DrawLightning(Transform other) {
        if (_lines.Count <= _nextLineIndex) {
            LightningPosition newLine = Instantiate(LightningPrefab);
            _lines.Add(newLine);
            newLine.transform.SetParent(transform, false);
            newLine.transform.localPosition = Vector3.zero;
        }
        var line = _lines[_nextLineIndex];

        line.gameObject.SetActive(true);
//        line.SetVertexCount(vertexCount);
//        line.SetColors(Color.white, Color.cyan);
//        line.SetWidth(5, 2);
//        line.material = LineMaterial;

        Vector3 startPosition = transform.position + Vector3.forward;
        Vector3 endPosition = other.position + Vector3.forward;

        line.StartPosition = startPosition;
        line.EndPosition = endPosition;

//        line.SetPosition(0, startPosition);
//        for (int i = 1; i < vertexCount - 1; i++) {
//            line.SetPosition(i, Vector3.Lerp(startPosition, endPosition, (float) i/(vertexCount - 1)) + (Vector3) H.RandomVector2(Randomization, false));
//        }
//        line.SetPosition(vertexCount - 1, endPosition);

        _nextLineIndex++;
    }
}