﻿using LevelParserV3;
using UnityEngine;
using System.Collections;

public class KillBossSkill : MonoBehaviour {

	void Update () {
	    if (LevelPlayer.Main.IsBoss && LevelPlayer.Main.LevelLaunched) {
            Debug.Log(BossHealthBar.Main.Max);
            Debug.Log(BossHealthBar.Main.Current);
            Debug.Log(Time.deltaTime);
            Debug.Log(Game.Music.Length);
            float power = BossHealthBar.Main.Max * (Time.deltaTime / Game.Music.Length);
            Debug.Log(power);
            BossHealthBar.Main.Hit(power);
	    }
	}
}
