﻿using UnityEngine;
using System.Collections;
using LevelParserV3;

public class ActiveSkill : MonoBehaviour {

    public float MaxEnergy;
    public string ButtonImageName;

    public float Energy { get; set; }
    public bool IsActivated { get; set; }
    public bool CanBeActivated { get; set; }

    public bool RegenerationDisabled { get; set; }

    void Awake() {
        CanBeActivated = true;
    }

    public void Activate() {
        if (CanBeActivated && !IsActivated && Energy >= MaxEnergy) {
            Energy = MaxEnergy;
            OnActivate();
        }
    }

    public virtual void OnActivate() {
    }

    private void Update() {
        if (!IsActivated && !RegenerationDisabled && LevelPlayer.Main.LevelLaunched) {
            Energy += Time.deltaTime;
        }
    }

    protected IEnumerator DepleteEnergy(float depleteTime) {
        while (Energy > 0) {
            Energy -= Time.deltaTime/depleteTime*MaxEnergy;
            yield return null;
        }
    }

}