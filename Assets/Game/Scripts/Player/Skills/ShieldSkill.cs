﻿using UnityEngine;
using System.Collections;

public class ShieldSkill : ActiveSkill {

    public override void OnActivate() {
        if (Game.Player.Health.IsShielded) {
            return;
        }

        StartCoroutine(Shield());
    }

    private IEnumerator Shield() {
        IsActivated = true;
        Game.Sound.PlaySound(SoundType.GameShieldSkill);
        Game.Player.Health.UseShield(true);
        yield return StartCoroutine(DepleteEnergy(Game.Player.Health.ShieldTime));

        IsActivated = false;
    }
}
