using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    [SerializeField] private float Speed;
    [SerializeField] public int Power;
    [SerializeField] public int BossPower;
    [SerializeField] private LayerMask Target;
    [SerializeField] private GameObject HitEffectPrefab;
    [SerializeField] private GameObject DamageEffectPrefab;
    [SerializeField] private float SplashRadius;
    [SerializeField] private float LifeTime;
    [SerializeField] private bool MoveThroughTarget;
    [SerializeField] private float MaxHoming;

    private bool _isDead;
    private List<GameObject> _hitedEnemies = new List<GameObject>();

    private IEnumerator Start() {
        GetComponent<Rigidbody>().velocity = H.PolarVector2(Speed, transform.rotation.eulerAngles.z + 90f);
        float elapsedLifeTime = 0;
        while (elapsedLifeTime < LifeTime) {
            if (MaxHoming > 0.01f) {
                TurnToNearestTarget();
            }
            elapsedLifeTime += Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }

    private void TurnToNearestTarget() {
        Vector3 nearest = Vector3.up*20000;
        float nearestSqrDistance = float.MaxValue;
        foreach (Collider other in Physics.OverlapSphere(Vector3.zero, 20000, Target)) {
            float sqrDistance = (transform.position - other.attachedRigidbody.transform.position).sqrMagnitude;
            if (sqrDistance < nearestSqrDistance) {
                nearestSqrDistance = sqrDistance;
                nearest = other.attachedRigidbody.transform.position;
            }
        }
        transform.rotation = Quaternion.RotateTowards(transform.rotation,
            Quaternion.Euler(0f, 0f, H.VectorAngle(nearest - transform.position) - 90), MaxHoming*Time.deltaTime);
        GetComponent<Rigidbody>().velocity = transform.up*Speed;
    }

    private void OnTriggerEnter(Collider other) {
        if (_isDead) {
            return;
        }
        if (other.attachedRigidbody == null) {
            return;
        }

        if ((Target & 1 << other.gameObject.layer) != 0) {
            if (SplashRadius <= 0 && !_hitedEnemies.Contains(other.gameObject)) {
                _hitedEnemies.Add(other.attachedRigidbody.gameObject);
                ApplyDamageOnTarget(other.attachedRigidbody.gameObject);
            }
            if (!MoveThroughTarget) {
                Die();
            }
        }
    }

    private void ApplyDamageOnTarget(GameObject target) {
        float power  = (target.GetComponent<Boss>() != null)&&(BossPower != 0) ? BossPower : Power;
        float upgradedPower = power*Mathf.Lerp(1f, 2f, (float) Game.Upgrade.CurrentShip.WeaponsLevel/Game.Upgrade.CurrentShip.MaxUpgradeLevel);
        NEvent.Dispatch(target, new HitEvent((int) upgradedPower, transform.position));
        ShowEffect(target.transform.position, DamageEffectPrefab);
    }

    private void ShowEffect(Vector3 position, GameObject EffectPrefab) {
        if (EffectPrefab != null) {
            (Instantiate(EffectPrefab) as GameObject).transform.position = position + Vector3.back;
        }
    }

    private void Die() {
        _isDead = true;
        if (SplashRadius != 0) {
            Collider[] colliders = Physics.OverlapSphere(transform.position, SplashRadius, Target);
            foreach (Collider coll in colliders) {
                ApplyDamageOnTarget(coll.attachedRigidbody.gameObject);
            }
        }
        ShowEffect(transform.position, HitEffectPrefab);
        if (GetComponent<BurstBullet>() != null) {
            GetComponent<BurstBullet>().ShowEffect();
        }
        Destroy(gameObject);
    }

}