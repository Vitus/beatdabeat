﻿using LevelParserV3;
using NelliTweenEngine;
using UnityEngine;
using System.Collections;

public class FocusMenu : MonoBehaviour {
    public float TargetSpeed;
    public float Acceleration;
    public GameObject MenuHolder;

    public GameObject BombPanel;
    public GameObject SkillPanel;

    public tk2dSpriteAnimator BombPanelIcon;

    private float _forcedSpeedUp = 1.0f;
    private bool _isAnimating;
    private bool _menuHolderVisible;

    private void Start() {
        TargetSpeed = 1f;
        Game.Player.Focus = this;
    }

    public void FastCurrentToTarget() {
        Time.timeScale = TargetSpeed*Game.DifficultyManager.Speed;
    }

    private void Update() {
        if (Time.timeScale != 0) {
            Time.timeScale = Mathf.MoveTowards(Time.timeScale, TargetSpeed*Game.DifficultyManager.Speed,
                Acceleration*Time.unscaledDeltaTime);
        }
        if(Game.Cheat.Observer) {
            return;
        }

        Game.Player.Ship.IsShooting = TargetSpeed == 1f;

        if ((LevelPlayer.Main.LevelLaunched || (Tutorial.Main != null && Tutorial.Main.isActiveAndEnabled)) && Time.timeScale >= 0.01f) {
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetMouseButtonDown(0)) {
                OnBombClick();
            }
            if (Input.GetKeyDown(KeyCode.X) || Input.GetMouseButtonDown(1)) {
                OnSkillClick();
            }
        }
    }

    private void SpeedUp() {
        if (TargetSpeed != 1f) {
            TargetSpeed = 1f;
            StartCoroutine(HideMenu());
        }
    }

    private IEnumerator HideMenu() {
        _isAnimating = true;
        if (SkillPanel.gameObject.activeInHierarchy) {
            SkillPanel.transform.TweenPosition(new TweenSettings<Vector3>() {
                StartValue = SkillPanel.transform.position,
                EndValue = SkillPanel.transform.position + Vector3.right*300,
                Easing = Ease.OutCubic,
                Duration = 0.25f,
            });
            BombPanel.transform.TweenPosition(new TweenSettings<Vector3>() {
                StartValue = BombPanel.transform.position,
                EndValue = BombPanel.transform.position + Vector3.left*300,
                Easing = Ease.OutCubic,
                Duration = 0.25f,
            });
        }
        yield return StartCoroutine(WaitIgnoreTImeScale(0.25f));
        _isAnimating = false;
        _menuHolderVisible = false;
        MenuHolder.SetActive(false);
    }


    private void OnBombClick() {
        if (Game.Player.Ship.BombCount > 0 || Game.Cheat.InfiniteBomb) {
            if (!Game.Cheat.InfiniteBomb) {
                Game.Player.Ship.BombCount--;
            }
            ForceSpeedUp();
            BombPanelIcon.Play("BombExplodeAnim");
            BombPanelIcon.AnimationCompleted += delegate { BombPanelIcon.Play("BombAnim"); };
            NEvent.Dispatch(Game.GameObject, new BombEvent());
        }
    }

    private IEnumerator WaitIgnoreTImeScale(float duration) {
        float elapsedTime = 0;
        while (elapsedTime < duration) {
            elapsedTime += Time.unscaledDeltaTime;
            yield return null;
        }
    }

    public void OnSkillClick() {
        ForceSpeedUp();
        Game.Player.Ship.UseSkill();
    }

    public void ForceSpeedUp() {
        SpeedUp();
        _forcedSpeedUp = 1.0f;
    }
}