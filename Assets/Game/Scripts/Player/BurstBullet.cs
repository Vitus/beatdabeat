﻿using UnityEngine;
using System.Collections;

public class BurstBullet : MonoBehaviour {

    public int BulletCount;
    public GameObject BulletPrefab;
    public float BurstTime;
    public GameObject ExplodeEffect;

    void Awake() {
        Invoke("Burst", BurstTime);
    }

    void Burst() {
        float randomAngle = Random.Range(0f, 360f);
        for (int i = 0; i < BulletCount; i++) {
            float angle = (float)i/BulletCount*360f;
            Instantiate(BulletPrefab, transform.position, Quaternion.Euler(0f, 0f, angle + randomAngle));
        }
        ShowEffect();
        Destroy(gameObject);
    }

    public void ShowEffect()
    {
        Game.Sound.PlaySound(SoundType.GameRocketShotExplode);
        Instantiate(ExplodeEffect, transform.position, Quaternion.identity);
    }

}
