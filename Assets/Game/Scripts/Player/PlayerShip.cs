﻿using System;
using System.Configuration;
using LevelParserV3;
using UnityEngine;
using System.Collections;

public class PlayerShip : MonoBehaviour {
    [SerializeField] public FlashSprite Image;
    private float _accumulatedDeltaX;
    private float _accumulatedDeltaY;
    private int ScaleSize = 70;
    public ActiveSkill ActiveSkill;
    public int BombCount { get; set; }

    public int MaxBombs {
        get { return Game.Upgrade.CurrentShip.BombsLevel == 0 ? 1 : Game.Upgrade.CurrentShip.BombsLevel*2; }
    }

    public bool IsShooting { get; set; }

    private const float AccumulationFrames = 3f;

    private void Start() {
        Game.Player.Ship = this;
        ActiveSkill = GetComponent<ActiveSkill>();
        BombCount = Game.Upgrade.CurrentShip.BombsLevel/2 + 1;
        this.PerformCoroutine(Animate());
    }

    private IEnumerator Animate() {
        if (Image != null) {
            while (true) {
                float scaleX = Mathf.Abs(_accumulatedDeltaX/ScaleSize);
                float scaleY = Mathf.Abs(_accumulatedDeltaY/ScaleSize);
                scaleX = Mathf.Clamp(scaleX, 0f, 0.25f);
                scaleY = Mathf.Clamp(scaleY, 0f, 0.25f);
                Image.transform.localScale = new Vector3(0.5f + scaleX - scaleY, 0.5f - scaleX + scaleY, 1);
                yield return null;
            }
        }
    }

    private void Update() {
        Vector3 newPosition = MoveWithMouse();

        _accumulatedDeltaX = (_accumulatedDeltaX*AccumulationFrames + (newPosition.x - transform.position.x))/
                             (AccumulationFrames + 1f);
        _accumulatedDeltaY = (_accumulatedDeltaY*AccumulationFrames + (newPosition.y - transform.position.y))/
                             (AccumulationFrames + 1f);

        transform.position = newPosition;

        KeepOnField();
    }

    private void KeepOnField() {
        Vector3 newPosition = transform.position;

        Vector2 fieldHalfSize = GetFieldHalfSize();

        if (newPosition.x > fieldHalfSize.x) {
            newPosition.x = fieldHalfSize.x;
        }
        if (newPosition.x < -fieldHalfSize.x) {
            newPosition.x = -fieldHalfSize.x;
        }
        if (newPosition.y > fieldHalfSize.y) {
            newPosition.y = fieldHalfSize.y;
        }
        if (newPosition.y < -fieldHalfSize.y) {
            newPosition.y = -fieldHalfSize.y;
        }
        transform.position = newPosition;
    }

    private Vector2 GetFieldHalfSize() {
        return new Vector2(LevelScript.shw*1.05f - 55, 230*1.05f);
    }

    private Vector3 MoveWithMouse() {

        if(Time.timeScale == 0) {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            return transform.position;
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Vector3 delta = new Vector3(Input.GetAxis("Mouse X") * 20, Input.GetAxis("Mouse Y") * 20);
        delta.z = 0;
        if(Time.timeScale > 0) {
            return transform.position + delta;
        }

        return transform.position;
    }

    public void UseSkill() {
        ActiveSkill skill = GetComponent<ActiveSkill>();
        if (skill != null) {
            skill.Activate();
        }
    }
}