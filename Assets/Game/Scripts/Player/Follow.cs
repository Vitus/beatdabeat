﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

    public Transform Target;
    public int FrameDelay;
    public Vector3 Offset;

    private Queue<Vector3> _targetPositionHistory = new Queue<Vector3>();

    void Start() {
        for (int i = 0; i < FrameDelay; i++) {
            _targetPositionHistory.Enqueue(Target.position);
        }
    }

    void LateUpdate() {
        _targetPositionHistory.Enqueue(Target.position);
        transform.position = _targetPositionHistory.Dequeue() + Offset;
    }

}
