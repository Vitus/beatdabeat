﻿using UnityEngine;
using System.Collections;

public enum WeaponType
{
    MachineGun = 0,
    SpreadGun = 1,
    Laser = 2,
    RocketLauncher = 3,
    PulseGun = 4,

    Count = 5
}
