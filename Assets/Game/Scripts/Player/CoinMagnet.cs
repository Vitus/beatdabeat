﻿using UnityEngine;
using System.Collections;

public class CoinMagnet : MonoBehaviour {
    [SerializeField] private LayerMask CoinLayer;
    [SerializeField] public float CollectRadius;
    [SerializeField] private float CollectSpeed;
    [SerializeField] private GameObject CoinCollectEffect;
    [SerializeField] private GameObject PowerUpCollectEffect;
    [SerializeField] private float DisappearRadius;

    private void Update() {
        foreach (Collider coin in Physics.OverlapSphere(transform.position, CollectRadius, CoinLayer)) {
            Game.Money.PerformCoroutine(Collect(coin));
        }
    }

    private IEnumerator Collect(Collider coin) {
        NEvent.Dispatch(Game.GameObject, GetCoinEvent.E);
        coin.GetComponent<Rigidbody>().isKinematic = true;
        coin.GetComponent<Collider>().enabled = false;
        do {
            coin.transform.position = Vector3.MoveTowards(coin.transform.position, Game.Player.Ship.transform.position,
                CollectSpeed*Time.deltaTime);
            yield return null;
        } while ((coin.transform.position - Game.Player.Ship.transform.position).sqrMagnitude >
                 DisappearRadius*DisappearRadius);
        if (coin.GetComponent<Coin>() == null) {
            GameObject collectEffect = coin.name.Contains("BonusCoin") ? CoinCollectEffect : PowerUpCollectEffect;
            Instantiate(collectEffect, coin.transform.position + Vector3.back*10, Quaternion.identity);
            NEvent.Dispatch(coin, GetBonusEvent.E);
            NEvent.Dispatch(Game.GameObject, GetBonusEvent.E);
        } else {
            Instantiate(CoinCollectEffect, coin.transform.position + Vector3.back*10, Quaternion.identity);
            NEvent.Dispatch(coin, GetCoinEvent.E);
            NEvent.Dispatch(Game.GameObject, GetCoinEvent.E);
        }
        Destroy(coin.gameObject);
    }
}