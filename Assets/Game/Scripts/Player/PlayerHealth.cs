﻿using UnityEngine;
using System.Collections;
using LevelParserV3;
using NelliTweenEngine;

public class PlayerHealth : MonoBehaviour {
    public float EvadeChance;
    [SerializeField] private float HitShakeAmount;
    [SerializeField] private float ShakeTime;
    [SerializeField] private float MaxShieldSize;
    public float ShieldTime;
    [SerializeField] private float ShieldGrowTime;
    public tk2dSprite ShieldImage;
    public ParticleSystem HitEffect;
    public AnimationCurve ShieldSizeCurve;
    public float GrazeDistance;
    public GameObject GrazeEffectPrefab;

    private float _shieldSize;
    public bool IsShielded { get; private set; }

    public bool HitTestEnabled { get; set; }

    public int Current {
        get { return HealthManager.Instance.CurrentShipHealth.Current; }
    }

    public bool Revive { get; set; }

    private void Awake() {
        ResetToStartValues();
        Game.Player.Health = this;
    }

    public void ResetToStartValues() {
        HitTestEnabled = true;
    }

    public void Replenish(int amount) {
        HealthManager.Instance.CurrentShipHealth.Replenish(amount);
    }

    private void Update() {
        if (!IsShielded && HitTestEnabled) {
            if (Physics.Raycast(transform.position - Vector3.forward*100, Vector3.forward, 1000,
                1 << LayerMask.NameToLayer("Laser"))) {
                this.PerformCoroutine(OnHit());
            }
        }
    }

    private void LateUpdate() {
        if (Revive) {
            Revive = false;
        }
    }


    public void OnBulletUpdate(ParticleBullet bullet) {
        float sqrDistanceToBullet = (bullet.TRSPosition - (Vector2) transform.position).sqrMagnitude;

        if (IsShielded) {
            if (Current > 0 && sqrDistanceToBullet < _shieldSize*_shieldSize/4) {
                if (!bullet.Indestructable) {
                    bullet.Kill();
                }
            }
        } else if (HitTestEnabled && Game.ParticleBullets.TestHit(bullet, transform.position)) {
            if (!bullet.Indestructable) {
                bullet.Kill();
            }
            this.PerformCoroutine(OnHit());
        } else if (!bullet.IsGrazed && sqrDistanceToBullet < GrazeDistance*GrazeDistance && LevelPlayer.Main.LevelLaunched) {
            bullet.IsGrazed = true;
            NEvent.Dispatch(Game.GameObject, GrazeEvent.E);
            if (GrazeEffectPrefab != null) {
                Game.Sound.PlaySound(SoundType.GameGraze);
                Instantiate(GrazeEffectPrefab, bullet.TransformPosition, Quaternion.identity);
            }
        }
    }

    private IEnumerator OnHit() {
        if (Game.Cheat.Observer || Revive) {
            yield break;
        }

        if (IsShielded) {
            yield break;
        }

        if (Current == 0) {
            yield break;
        }

        if (Random.Range(0f, 1f) > EvadeChance) {
            Game.Sound.PlaySound(SoundType.GameGetHit);

            if (!Game.Cheat.InfiniteHealth) {
                HealthManager.Instance.CurrentShipHealth.Deplete();
            }
            NEvent.Dispatch(Game.GameObject, PlayerHitEvent.E);

            UseShield();
        } else {
            Debug.Log("Evade");
        }
    }

    public void UseShield(bool isSkill = false) {
        if (IsShielded) {
            return;
        }

        IsShielded = true;
        if (!isSkill) {
            if (Current != 0) {
                Camera.main.transform.TweenShakeLocalPosition(new TweenSettings<float>() {
                    StartValue = HitShakeAmount,
                    EndValue = 0,
                    Duration = ShakeTime,
                    IgnoreTimeScale = true,
                    AffectAxis = AffectAxis.XY
                });
            }
        }

        StopCoroutine("Shield");

        if (Current == 0) {
            ShieldImage.GetComponent<Renderer>().enabled = false;
            ResetShield();
        } else {
            ShieldImage.GetComponent<Renderer>().enabled = true;
            if (!isSkill) {
                HitEffect.Emit(50);
            }
            StartCoroutine("Shield");
        }
    }

    private IEnumerator Shield() {
        float _currentTime = Time.time;
        float _startTime = Time.time;
        float _previousTime = Time.time;
        while (_currentTime < _startTime + ShieldTime) {
            if (Time.timeScale != 0) {
                _currentTime += Time.time - _previousTime;
                float t = (_currentTime - _startTime)/ShieldTime;
                ChangeShieldSize(ShieldSizeCurve.Evaluate(t)*MaxShieldSize);
                ChangeShieldAlpha(easeInQuad(1, 0, t));
            }
            _previousTime = Time.time;
            yield return null;
        }
        ResetShield();
    }

    private float easeOutQuart(float start, float end, float value) {
        value--;
        end -= start;
        return -end*(value*value*value*value*value*value - 1) + start;
    }

    private float easeInQuad(float start, float end, float value) {
        end -= start;
        return end*value*value + start;
    }

    private void ResetShield() {
        ChangeShieldSize(0);
        IsShielded = false;
    }

    private void ChangeShieldAlpha(float alpha) {
        ShieldImage.color = ShieldImage.color.WithAlpha(alpha);
    }

    private void ChangeShieldSize(float size) {
        _shieldSize = size;
        ShieldImage.transform.localScale = new Vector3(_shieldSize, _shieldSize, _shieldSize);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, GrazeDistance);
    }
}