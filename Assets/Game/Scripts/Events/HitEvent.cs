﻿using UnityEngine;
using System.Collections;

public class HitEvent : NelliEvent
{
    public int Power { get; private set; }
    public Vector3 Position { get; private set; }

    public HitEvent(int power, Vector3 position)
    {
        Power = power;
        Position = position;
    }
}
