﻿
public enum  PlayerShootEventState
{
    StopShoot = 0,
    StartShoot = 1,
    Shoot = 2
}
public class PlayerShootEvent : NelliEvent
{
    public JSONNode Parameters;
    public JSONNode GroupParameters;
    public PlayerShootEventState State;
    public WeaponType Type;

    public PlayerShootEvent(WeaponType type, PlayerShootEventState state, JSONNode parameters, JSONNode groupParameters)
    {
        Type = type;
        State = state;
        Parameters = parameters;
        GroupParameters = groupParameters;
    }
}
