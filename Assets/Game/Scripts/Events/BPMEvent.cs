﻿    public class BPMEvent : NelliEvent
    {
        public readonly float Bpm;

        public BPMEvent(float bpm)
        {
            Bpm = bpm;
        }
    }
