﻿using UnityEngine;
using System.Collections;

public class BeatEvent : NelliEvent
{
    public int TrackNum;

    public BeatEvent(int trackNum)
    {
        TrackNum = trackNum;
    }

}
