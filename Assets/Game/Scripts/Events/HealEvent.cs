﻿using UnityEngine;
using System.Collections;

public class HealEvent : NelliEvent
{
    public int Power { get; private set; }

    public HealEvent(int power)
    {
        Power = power;
    }
}
