﻿using UnityEngine;
using System.Collections;

public class DifficultyChangeEvent : NelliEvent 
{
    public int NewDifficulty;

    public DifficultyChangeEvent(int newDifficulty)
    {
        NewDifficulty = newDifficulty;
    }
}
