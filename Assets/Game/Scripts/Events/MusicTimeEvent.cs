﻿using UnityEngine;
using System.Collections;

public class MusicTimeEvent : NelliEvent
{
    public float Time { get; set; }

    public MusicTimeEvent(float newTime)
    {
        Time = newTime;
    }
}
