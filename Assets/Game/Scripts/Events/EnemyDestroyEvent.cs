﻿using UnityEngine;
using System.Collections;

public class EnemyDestroyEvent : NelliEvent
{
    public Enemy Enemy { get; set; }
}
