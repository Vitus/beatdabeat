﻿using System;
using LevelParserV3;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class SimpleWeaponNoMusic : MonoBehaviour {
    [SerializeField] private Bullet BulletPrefab;
    [SerializeField] private float Interval;
    [SerializeField] private int BulletCount;

    [SerializeField] private float Spread;
    [SerializeField] private bool RandomSpread;
    [SerializeField] private float PositionRange;
    [SerializeField] private bool RandomPosition;
    [SerializeField] private FlashSprite Splash;
    [SerializeField] private SoundType ShootSound = SoundType.GameShoot;



    private CustomCoroutine _shootCoroutine;
    private bool _isShooting;

    private void OnEnable() {
        StartCoroutine(StartShoot());
    }

    private IEnumerator StartShoot() {
        Splash.gameObject.SetActive(false);
        _isShooting = true;
        float nextShootTime = Time.time;
        while (true) {
            while (nextShootTime < Time.time) {
                if (_isShooting) {
                    Shoot();
                }
                nextShootTime += Interval;
            }
            yield return null;
        }
    }

    private void Update() {
        if (Game.Tutorial == null) {
            _isShooting = LevelPlayer.Main.LevelLaunched && Game.Player.Ship.IsShooting;
        }
        else {
            _isShooting = Game.Tutorial.IsShooting;
        }

        Splash.gameObject.SetActive(_isShooting);
    }

    private void Shoot() {
        if (Game.Cheat.Observer) {
            return;
        }

        for (int i = 0; i < BulletCount; i++) {
            var angle = GetAngle(i);
            var position = GetPosition(i);

            Instantiate(BulletPrefab, position, Quaternion.Euler(0f, 0f, angle));
        }
        Game.Sound.PlaySound(ShootSound);
        Splash.gameObject.SetActive(true);
        Splash.ChangeAnimation(0, true, true);
    }

    private Vector3 GetPosition(int bulletNum) {
        Vector3 position = transform.position;
        if (RandomPosition) {
            position += new Vector3(Random.Range(-PositionRange/2f, PositionRange/2f), 0f, 0f);
        }
        else {
            position += new Vector3(PositionRange/BulletCount*(bulletNum + 0.5f) - PositionRange/2f, 0f, 0f);
        }
        return position;
    }

    private float GetAngle(int bulletNum) {
        float angle = transform.rotation.eulerAngles.z;
        if (RandomSpread) {
            angle += Random.Range(-Spread/2f, Spread/2f);
        }
        else {
            angle += -Spread/BulletCount*(bulletNum + 0.5f) + Spread/2f;
        }
        return angle;
    }
}