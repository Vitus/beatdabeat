﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using LevelParserV3;
using NelliTweenEngine;
using UnityEngine;
using System.Collections;

public class LaserWeapon : MonoBehaviour {

    public ActiveSkill ActiveSkill;
    public float EnergyForGraze;
    public LayerMask Target;
    public float EnergyDepleteTime;
    public int MinDPS;
    public int MaxDPS;
    public float MinWidth;
    public float MaxWidth;
    public Gradient PowerGradient;
    public Material ColorOverlayMaterial;
    public GameObject Holder;
    public tk2dSprite Top;
    public tk2dSprite Bottom;
    public tk2dSprite Shine;
    public tk2dSprite Laser;

    void SetColor(Color color) {
        Top.color = color;
        Laser.color = color;
        Bottom.color = color;
        Shine.color = color;
    }

    void OnEnable() {
        ActiveSkill.RegenerationDisabled = true;
        NEvent.AddListener<GrazeEvent>(Game.GameObject, OnGraze);
    }

    private void Animate() {
        Top.GetComponent<Renderer>().material = ColorOverlayMaterial;
        Laser.GetComponent<Renderer>().material = ColorOverlayMaterial;
        Bottom.GetComponent<Renderer>().material = ColorOverlayMaterial;
        Shine.GetComponent<Renderer>().material = ColorOverlayMaterial;
        Bottom.TweenAlpha(new TweenSettings<float>()
        {
            StartValue = 0,
            EndValue = 1,
            LoopType = LoopType.PingPong,
            LoopCount = -1,
            Duration = 0.4f,
            Easing = Ease.Linear
        });
        Shine.TweenAlpha(new TweenSettings<float>() {
            StartValue = 0,
            EndValue = 1,
            LoopType = LoopType.PingPong,
            LoopCount = -1,
            Duration = 0.4f,
            Easing = Ease.Linear
        });
    }

    void OnDisable() {
        ActiveSkill.RegenerationDisabled = false;
        NEvent.RemoveListener<GrazeEvent>(Game.GameObject, OnGraze);
    }

	void Update () {
	    if (!LevelPlayer.Main.LevelLaunched) {
	        if (Holder.activeSelf) {
	            Holder.SetActive(false);
	        }
	        return;
	    }

	    if (Holder.activeSelf == false) {
	        Holder.SetActive(true);
            Animate();
        }

	    ActiveSkill.Energy -= Time.deltaTime*EnergyDepleteTime;
        ActiveSkill.Energy = Mathf.Clamp(ActiveSkill.Energy, 0, ActiveSkill.MaxEnergy);

	    float dps = Mathf.Lerp(MinDPS, MaxDPS, Power);
        float upgradedDps = dps * Mathf.Lerp(1f, 2f, (float)Game.Upgrade.CurrentShip.WeaponsLevel / Game.Upgrade.CurrentShip.MaxUpgradeLevel);
        RaycastHit hitInfo;
        Debug.DrawRay(transform.position, Vector3.up, Color.red);
        if(Physics.SphereCast(transform.position, 2, Vector3.up, out hitInfo, float.MaxValue, Target.value))
	    {
            NEvent.Dispatch(hitInfo.collider.attachedRigidbody.gameObject, new HitEvent((int) (upgradedDps * Time.deltaTime), transform.position));
	        Laser.transform.SetYLocalScale(hitInfo.distance);
	        Top.transform.position = hitInfo.point;
	    }
        else {
            Laser.transform.SetYLocalScale(1000);
            Top.transform.SetYLocalPosition(1000);
        }
        SetColor(PowerGradient.Evaluate(Power));
        Laser.transform.SetXLocalScale(Mathf.Lerp(MinWidth, MaxWidth, Power) + Random.Range(0f, 1f));

	}

    private float Power {
        get { return Mathf.Clamp(ActiveSkill.Energy/ActiveSkill.MaxEnergy, 0f, 1f); }
    }

    private void OnGraze(GrazeEvent e) {
        ActiveSkill.Energy += EnergyForGraze;
        ActiveSkill.Energy = Mathf.Clamp(ActiveSkill.Energy, 0, ActiveSkill.MaxEnergy);
    }
}
