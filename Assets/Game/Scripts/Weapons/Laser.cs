﻿using System;
using LevelParserV2;
using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour, ICaster, ITRSObject
{
    public ICaster Caster
    {
        get { return _caster; }
        set
        {
            if (_caster != null)
            {
                DestroyWithoutCaster = true;
            }
            else
            {
                DestroyWithoutCaster = false;
            }
            _caster = value;
        }
    }

    public float Angle;
    public float AppearDistance;
    public float VanishDistance;
    public float StartDistance;
    public float Width;
    public float Length;
    public float Speed;
    public float AngularVelocity;
    public float EnableSpeed;
    public bool AutoDestroy;
    private bool DestroyWithoutCaster;

    private CustomCoroutine _appearRoutine;
    [SerializeField] private bool _enabled;
    public bool Enabled
    {
        get { return _enabled; }
        set
        {
            if (!_enabled && value)
                _appearRoutine = this.PerformCoroutine(Appear());
            if (!value)
                _currentWidth = 2;
            _enabled = value;
        }
    }
    [SerializeField] private Color _color;
    public Color Color
    {
        get { return _color; }
        set
        {
            SetColor(value);
        }
    }

    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;

    [SerializeField] private ParticleSystem _particlesStart;
    [SerializeField] private ParticleSystem _particlesEnd;

    private Mesh _rendererMesh;
    private Mesh _colliderMesh;

    private Vector3[] _vertices = new Vector3[4];

    private static Mesh _quadMesh;
    private float _currentWidth;

    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private ICaster _caster;

    private static Mesh QuadMesh
    {
        get
        {
            if (_quadMesh == null)
            {
                GameObject tempObj = GameObject.CreatePrimitive(PrimitiveType.Quad);
                _quadMesh = tempObj.GetComponent<MeshFilter>().mesh;
                Destroy(tempObj);
            }
            return _quadMesh;
        }
    }

//    void OnEnable()
//    {
//        NEvent.AddListener<MusicUpdateEvent>(Game.GameObject, MusicUpdate);
//    }

//    void OnDisable()
//    {
//        NEvent.RemoveListener<MusicUpdateEvent>(Game.GameObject, MusicUpdate);
//    }

    private float SqrDistanceToLaser(Vector2 start, Vector2 end, Vector2 point)
    {
        float sqrLength = (start - end).sqrMagnitude;
        
        if (sqrLength == 0.0) 
            return (point - start).sqrMagnitude;
        
        float t = Vector2.Dot(point - start, end - start)/sqrLength;
        
        if (t < 0.0) 
            return (point - start).sqrMagnitude;
        
        if (t > 1.0) 
            return (point - end).sqrMagnitude;
        
        Vector2 projection = start + t*(end - start);
        return (point - projection).sqrMagnitude;
    }

//    private void MusicUpdate(MusicUpdateEvent e)
    void Update()
    {
        if (Caster != null)
            transform.position = Caster.TransformPosition;
        else if (DestroyWithoutCaster)
        {
            Destroy(gameObject);
            return;
        }

        if (_appearRoutine != null)
        {
            _appearRoutine.Pump();
        }

        StartDistance += Speed*Time.deltaTime;
        Angle += AngularVelocity*Time.deltaTime;

        float baseAngle = Caster == null ? Angle : (Caster.TransformAngle + Angle);

        float start = Mathf.Max(AppearDistance, StartDistance);
        float end = Mathf.Min(VanishDistance, StartDistance + Length);

        if (AutoDestroy && end < StartDistance)
        {
            Destroy(gameObject);
        }

        _particlesEnd.enableEmission = false;
        _particlesStart.enableEmission = false;

        if (end - start > 0)
        {
            SetColor(_color);

            _startPosition = H.PolarVector2(start, baseAngle);
            _endPosition = H.PolarVector2(end, baseAngle);

            if (start == AppearDistance)
            {
                _particlesStart.enableEmission = true;
                _particlesStart.transform.position = transform.position + _startPosition;
            }
            if (end == VanishDistance)
            {
                _particlesEnd.enableEmission = true;
                _particlesEnd.transform.position = transform.position + (Vector3)H.PolarVector2(end, baseAngle);
            }

            _rendererMesh.vertices = SetVertices(_vertices,
                                                 Matrix4x4.TRS(_startPosition,
                                                               Quaternion.Euler(0f, 0f, baseAngle),
                                                               new Vector3(end - start, Enabled ? _currentWidth*2 : 2)));
            _rendererMesh.RecalculateBounds();
            _meshFilter.mesh = _rendererMesh;
        }
        else
        {
            _startPosition = new Vector3(float.NaN, float.NaN);
            _meshFilter.mesh = null;
        }

        TestHitPlayer();
    }

    private void TestHitPlayer()
    {
        if (SqrDistanceToLaser(_startPosition + transform.position, _endPosition + transform.position, Game.Player.Ship.transform.position) < Width*Width/4)
        {
            Game.Player.Health.SendMessage("OnHit");
        }
    }

    private IEnumerator Appear()
    {
        while (_currentWidth != Width)
        {
            _currentWidth = Mathf.MoveTowards(_currentWidth, Width, EnableSpeed * Time.deltaTime);
            yield return null;
        }
        _appearRoutine = null;
    }

    public void Awake()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _rendererMesh = CloneMesh(QuadMesh);
        _rendererMesh.MarkDynamic();
        _meshFilter.mesh = _rendererMesh;

        SetColor(_color);
    }

    void Start()
    {
        _currentWidth = Enabled ? Width : 2;
    }

    private Mesh CloneMesh(Mesh mesh)
    {
        Mesh newmesh = new Mesh();
        newmesh.vertices = mesh.vertices;
        newmesh.triangles = mesh.triangles;
        newmesh.uv = mesh.uv;
        newmesh.normals = mesh.normals;
        newmesh.colors = mesh.colors;
        newmesh.tangents = mesh.tangents;
        return newmesh;
    }

    Vector3[] SetVertices(Vector3[] vertices, Matrix4x4 trsMatrix)
    {
        vertices[0] = trsMatrix.MultiplyPoint(new Vector3(0, -0.5f));
        vertices[1] = trsMatrix.MultiplyPoint(new Vector3(1, 0.5f));
        vertices[2] = trsMatrix.MultiplyPoint(new Vector3(1, -0.5f));
        vertices[3] = trsMatrix.MultiplyPoint(new Vector3(0, 0.5f));
        return vertices;
    }

    public void SetColor(Color color)
    {
        _color = color;

        Color[] colors = _rendererMesh.colors;
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = color;
        }
        _rendererMesh.colors = colors;

        _particlesStart.startColor = color;
        _particlesEnd.startColor = color;
    }

    public Vector2 TransformPosition
    {
        get 
        {
            if (this != null)
                return _endPosition + transform.position;
            else
            {
                return _endPosition;
            }
        }
    }

    public float TransformAngle { get { return Angle + (Caster == null ? 0f : Caster.TransformAngle); } }
    public Vector2 TransformScale { get { return new Vector2(Length / 100f, Width / 100f); } }

    public Vector2 TRSPosition { get { return TransformPosition; } }
    public float TRSAngle { get { return TransformAngle; } }
    public Vector2 TRSScale { get { return TransformScale; } }
    public bool IsAlive()
    {
        return this != null;
    }
}
