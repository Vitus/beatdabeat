﻿using System;
using UnityEngine;
using System.Collections;

public class DestroyOnOutOfHealth : MonoBehaviour 
{
    void OnEnable()
    {
        NEvent.AddListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
    }

    void OnDisable()
    {
        NEvent.RemoveListener<OutOfHealthEvent>(gameObject, OnOutOfHealth);
    }

    private void OnOutOfHealth(OutOfHealthEvent outOfHealthEvent)
    {
        Destroy(gameObject);
    }
}
