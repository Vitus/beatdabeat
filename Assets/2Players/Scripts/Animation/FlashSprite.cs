using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class FlashSprite : MonoBehaviour
{
    public List<FlashAnimation> AnimationList;
    public int StartAnimationId;
    public bool IgnoreTimeScale;
    public bool PlayOnceAtStartup;
    public Color Color = Color.white;

    [NonSerialized]
    public float FPS;

    private float _currentFrame;
    private float _previousFrame;
    private FlashAnimation _currentAnimation;
    private float _prevFrameTime;
    private bool _playOnce;
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;

    public int CurrentFrame
    {
        get { return (int) _currentFrame; }
        set
        {
            _currentFrame = value;
            _previousFrame = _currentFrame - 1;
        }
    }    
    
    public int PreviousFrame
    {
        get { return (int) _previousFrame; }
        private set { _previousFrame = value; }
    }

    public FlashAnimation CurrentAnimation { get { return _currentAnimation; } }

    private FlashAnimation.Variant[] _combineVariants;
    private Vector3[] _vertexList;
    private int[] _trianglesList;
    private Vector2[] _uvList;
    private Color[] _colorList;

    private Dictionary<string, int> _selectedVariants = new Dictionary<string, int>();
    private int[] _selectedVariantsArray;

    private Mesh mesh;

    [SerializeField]
    private int thumbnailId;
    [SerializeField]
    private int thumbnailFrame;

    private bool[] _frameEventFired;
    private bool _isPlaying = true;

    public Mesh GetMesh
    {
        get {return mesh;}
    }

    void OnEnable()
    {
        _prevFrameTime = Time.realtimeSinceStartup;
    }

	void Awake()
    {
	    _prevFrameTime = Time.realtimeSinceStartup;
	    PrepareComponents();
        if (Application.isPlaying)
            ChangeAnimation(StartAnimationId, PlayOnceAtStartup);
        else
            ChangeEditorAnimation();
        _currentFrame = 0f;
	    _previousFrame = 0f;
    }

    public void SetFrame(int frame) {
        _currentFrame = frame;
    }

    public void SetPlay(bool play) {
        _isPlaying = play;
    }

    void Update()
    {
        if (Application.isPlaying && _isPlaying)
            SetNextFrame();

        ShowFrame((int)_currentFrame);

        _prevFrameTime = Time.realtimeSinceStartup;
    }

    public void SetVariant(string layerName, int variantNum)
    {
        _selectedVariants[layerName] = variantNum;
        foreach (FlashAnimation.VariantAlias variantAlias in _currentAnimation.VariantAliases)
        {
            if (variantAlias.Name == layerName)
            {
                foreach (string dependentVariant in variantAlias.DependentVariants)
                {
                    _selectedVariants[dependentVariant] = variantNum;
                }
                break;
            }
        }

        UpdateSelectedVariants();
    }

    private void UpdateSelectedVariants()
    {
        for (int i = 0; i < _selectedVariantsArray.Length; i++)
        {
            _selectedVariantsArray[i] = 0;
        }
        foreach (KeyValuePair<string, int> selectedVariant in _selectedVariants)
        {
            int layerNum = _currentAnimation.GetLayerNum(selectedVariant.Key);
            if (layerNum >= 0)
                _selectedVariantsArray[layerNum] = selectedVariant.Value;        
        }
    }

    public void ChangeEditorAnimation()
    {
        if (!Application.isPlaying)
        {
            ChangeAnimation(thumbnailId);
            _currentFrame = thumbnailFrame;
        }
    }

    public void ChangeAnimation(int animationNum, bool playOnce = false, bool reset = false)
    {
        _playOnce = playOnce;
        if (reset || _currentAnimation != AnimationList[animationNum])
        {
            _currentFrame = 0;
            _previousFrame = -1;
            _prevFrameTime = Time.realtimeSinceStartup;
            _currentAnimation = AnimationList[animationNum];
            FPS = _currentAnimation.FPS;
            PrepareAnimation();
            UpdateSelectedVariants();
            ShowFrame((int)_currentFrame);
        }
    }

    public void ChangeAnimation(string animationName, bool playOnce = false, bool reset = false)
    {
        for (int i = 0; i < AnimationList.Count; i++)
        {
            if (AnimationList[i] != null && animationName == AnimationList[i].name)
            {
                ChangeAnimation(i, playOnce, reset);
                return;
            }
        }

        Debug.LogError("There is no animation with name " + animationName);
    }

    public bool Finished()
    {
        return _playOnce && _currentFrame == CurrentAnimation.FrameCount - 1;
    }

    public void ShowFrame(int frameNum)
    {
        if (_meshRenderer == null)
            return;

        FlashAnimation.Frame frame = _currentAnimation.GetFrame((int) _currentFrame);
        for (int i = frame.Layers.Length; i < _combineVariants.Length - frame.Layers.Length; i++)
        {
            _combineVariants[i] = null;
        }
        for (int i = 0; i < frame.Layers.Length; i++)
        {
            if (frame.Layers[i].Variants.Count == 0)
                _combineVariants[i] = FlashAnimation.BlankVariantInstance;
            else {
                _combineVariants[i] = frame.Layers[i].Variants[_selectedVariantsArray[i]];
            }
        }
        CombineMeshes(_combineVariants);
    }
    
    private void PrepareComponents()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _meshRenderer = GetComponent<MeshRenderer>();

        mesh = new Mesh();
        mesh.hideFlags = HideFlags.DontSave;
        _meshFilter.mesh = mesh;
    }

    private void PrepareAnimation()
    {
        _meshRenderer.materials = _currentAnimation.SpriteCollection.materials;

        _combineVariants = new FlashAnimation.Variant[_currentAnimation.LayerCount];
        _vertexList = new Vector3[_currentAnimation.MaxVertices];
        _uvList = new Vector2[_currentAnimation.MaxVertices];
        _colorList = new Color[_currentAnimation.MaxVertices];
        _trianglesList = new int[_currentAnimation.MaxTriangles];
        _selectedVariantsArray = new int[_currentAnimation.LayerCount];
        if (mesh != null)
            mesh.Clear();
    }

    private void SetNextFrame()
    {
        _previousFrame = _currentFrame;
        _currentFrame += (Time.realtimeSinceStartup - _prevFrameTime)*(IgnoreTimeScale ? 1 : Time.timeScale)* FPS;
        if (_currentFrame < 0f)
            _currentFrame = 0f;
        if (_currentFrame >= _currentAnimation.FrameCount - 1)
        {
            if (_playOnce)
                _currentFrame = _currentAnimation.FrameCount - 1;
            else
                _currentFrame -= _currentAnimation.FrameCount;
        }
    }

    private void CombineMeshes(FlashAnimation.Variant[] variants)
    {
        if (mesh == null)
            return;

        int vertCount = 0;
        int trisCount = 0;

        foreach (FlashAnimation.Variant variant in variants)
        {
            vertCount += variant.vertices.Length;
            trisCount += variant.triangles.Length;
        }

        for (int i = vertCount; i < _vertexList.Length/* - vertCount*/; i++)
        {
            _vertexList[i] = Vector3.zero;
            _uvList[i] = Vector2.zero;
            _colorList[i] = Color.black;
        }
        for (int i = trisCount; i < _trianglesList.Length/* - trisCount*/; i++ )
        {
            _trianglesList[i] = 0;
        }

        int currentVertexPosition = 0;
        int currentTrianglePosition = 0;

        foreach (FlashAnimation.Variant variant in variants)
        {
            for (int i = 0; i < variant.vertices.Length; i++)
            {
                _vertexList[currentVertexPosition + i] = variant.vertices[i];
                _uvList[currentVertexPosition + i] = variant.uv[i];
                _colorList[currentVertexPosition + i] = variant.colors[i] * Color;
            }
            for (int i = 0; i < variant.triangles.Length; i++)
            {
                _trianglesList[currentTrianglePosition + i] = variant.triangles[i] + currentVertexPosition;
            }
            currentVertexPosition += variant.vertices.Length;
            currentTrianglePosition += variant.triangles.Length;
        }

        mesh.vertices = _vertexList;
        mesh.triangles = _trianglesList;
        mesh.uv = _uvList;
        mesh.colors = _colorList;
    }

    void OnDestroy()
    {
        if (mesh)
        {
            if (Application.isPlaying)
                Destroy(mesh);
            else
                DestroyImmediate(mesh);
        }
    }

}
