﻿fl.outputPanel.clear();

var selectedItem = fl.getDocumentDOM().selection[0];
var itemFilters = selectedItem.filters;
var scaleX = selectedItem.scaleX;
var scaleY = selectedItem.scaleY;
var selectedLibraryItem = selectedItem.libraryItem;

var library = fl.getDocumentDOM().library;
library.editItem(selectedLibraryItem.name);
var layerCount = fl.getDocumentDOM().getTimeline().layerCount;
var symbolName = fl.getDocumentDOM().getTimeline().name;

var resultXml = <animation />;
resultXml.@name = symbolName;
var tempItems = [];

for (var layerIndex = layerCount - 1; layerIndex >= 0; layerIndex--)
{
	var hasExported = false;
	library.editItem(selectedLibraryItem.name);
	var timeline = fl.getDocumentDOM().getTimeline();
	
	timeline.convertToKeyframes();

	var currentLayer = timeline.layers[layerIndex];
	var layerName =  currentLayer.name;
	var frameCount = currentLayer.frames.length;
	//fl.trace("layer " + layerName);
	
	if (layerName.indexOf("Layer") != -1 || layerName == "" || frameCount == 0)
		continue;

 	var layerXml = <layer />;
	
	for (var frameIndex = 0; frameIndex < frameCount; frameIndex++)
	{
		var currentFrame = currentLayer.frames[frameIndex];
		var frameXml = <frame />;
		
		frameXml.@num = frameIndex;

		if (currentFrame.elements.length <= 0)
			frameXml.@visible = false;
		else
		{
			var clip = currentFrame.elements[0];
			var limit = 10;
			var digitLimit = 2;
			frameXml.@alpha = clip.colorAlphaPercent.toFixed(digitLimit);
			frameXml.@visible = true;
			frameXml.@a = clip.matrix.a;
			frameXml.@b = clip.matrix.b;
			frameXml.@c = clip.matrix.c;
			frameXml.@d = clip.matrix.d;
			frameXml.@tx = clip.matrix.tx;
			frameXml.@ty = clip.matrix.ty;

			if (!hasExported)
			{
				var libraryItem = clip.libraryItem;
				var clipAmountOfFrames = 0;
				for each (var someLayer in libraryItem.timeline.layers)
					clipAmountOfFrames = max(someLayer.frames.length, clipAmountOfFrames);
				
				for (var insideFrameCount = 0; insideFrameCount < clipAmountOfFrames; insideFrameCount++)
				{
					var clipDataXml = <data />;
					var tempLibraryItemName = libraryItem.name + insideFrameCount + "_temp";
					
					var isNew = tempItems.indexOf(tempLibraryItemName) == -1;
					
					if (isNew)
					{
						tempItems.push(tempLibraryItemName);
						library.addNewItem("movie clip", tempLibraryItemName);
					}
						var tempItem = library.items[library.findItemIndex(tempLibraryItemName)];
					
					if (isNew)
					{
						library.editItem(tempLibraryItemName);
						library.selectItem(libraryItem.name);
						library.addItemToDocument({x:0, y:0});
						fl.getDocumentDOM().transformSelection(scaleX, 0, 0, scaleY);
					}
					fl.trace(tempLibraryItemName);
					var currentElement = tempItem.timeline.layers[0].frames[0].elements[0];
					currentElement.symbolType = "graphic";
					currentElement.loop = "single frame";
					currentElement.firstFrame = insideFrameCount;
					currentElement.setTransformationPoint({x:0, y:0});
					currentElement.x = 0;
					currentElement.y = 0;
						
					if (isNew)
						joinFilters(currentElement, itemFilters, clip.filters);
						
						//fl.trace(tempLibraryItemName + " " + currentElement.left + " " + currentElement.top + " " + currentElement.firstFrame);
						clipDataXml.@anchorX = currentElement.left;
						clipDataXml.@anchorY = currentElement.top;
					
					//}
					clipDataXml.@name = "v" + insideFrameCount;
					layerXml.appendChild(clipDataXml);
				}
				
				hasExported = true;
			}
		}
		
		layerXml.appendChild(frameXml);
	}
	
	if (hasExported)
	{
		layerXml.@name = libraryItem.name;
		layerXml.@zOrder = layerCount - layerIndex - 1;
		if (layerXml.@name == "s/anchor")
		{
			resultXml.@anchorX = layerXml.frame[0].@tx;
			resultXml.@anchorY = layerXml.frame[0].@ty;
		}else
		{
			resultXml.appendChild(layerXml);
		}
	}
}

fl.trace(resultXml);
var resFolder = "file:///c|/test/";
//fl.outputPanel.save(resFolder + symbolName + ".xml");
FLfile.write(resFolder + symbolName + ".xml", resultXml);

for each (tempLibraryItemName in tempItems)
	library.deleteItem(tempLibraryItemName);
fl.getDocumentDOM().exitEditMode();

function abs(x)
{
	return x > 0 ? x : -x;
}

function max(a, b)
{
	return a > b ? a : b;
}

function joinFilters(currentElement, itermFilters, clipFilters)
{
	var resFilters = [];
	if (itemFilters && itemFilters != undefined && itemFilters.length > 0)
		for each (var f in itemFilters)
			resFilters.push(f);
	if (clipFilters && clipFilters != undefined && clipFilters.length > 0)
		for each (f in clipFilters)
			resFilters.push(f);
	if (resFilters.length > 0)
		currentElement.filters = resFilters;
}