using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashAnimation : MonoBehaviour 
{
    [Serializable]
    public class VariantAlias
    {
        public string Name;
        public string[] DependentVariants;
    }

    [Serializable]
    public class Variant
    {
        public Vector3[] vertices;
        public int[] triangles;
        public Vector2[] uv;
        public Color[] colors;
    }
	
    [Serializable]
    public class Layer
    {
        public List<Variant> Variants = new List<Variant>();
    }

    [Serializable]
    public class Frame
    {
        public Layer[] Layers;
    }

    public TextAsset AnimationXML;
    public float FPS;
    public float TargetHeight;
    public float AnimationHeight;
    public tk2dSpriteCollectionData SpriteCollection;
    public VariantAlias[] VariantAliases;
    
    [SerializeField]
    [HideInInspector]
    private int _frameCount;

    public int FrameCount
    {
        get { return _frameCount; }
        private set { _frameCount = value; }
    }

    public int LayerCount
    {
        get { return _layerNames.Count; }
    }

    public int MaxTriangles
    {
        get { return _maxTriangles; }
    }

    public int MaxVertices
    {
        get { return _maxVertices; }
    }

    [SerializeField] 
    private List<Frame> _frameList;
    [SerializeField]
    private List<string> _layerNames;

    private Vector2 _anchor;
    private tk2dSprite _tempSprite;

    [SerializeField]
    [HideInInspector]
    private int _maxTriangles;
    [SerializeField]
    [HideInInspector]
    private int _maxVertices;

    public Frame GetFrame(int frameNum)
    {
        if (_frameList == null)
            ParseAnimation();
        
        if (frameNum >= FrameCount)
            return _frameList[FrameCount - 1];
        
        if (frameNum < 0)
            return _frameList[0];
        
        return _frameList[frameNum];
    }

    public int GetLayerNum(string layerName)
    {
        return _layerNames.IndexOf(layerName);
    }

    public string GetLayerName(int layerNum)
    {
        return _layerNames[layerNum];
    }

    public void ParseAnimation()
    {
        _frameList = new List<Frame>();
        _layerNames = new List<string>();
        FrameCount = 0;

        _tempSprite = new GameObject().AddComponent<tk2dSprite>();
        
        XMLParser parser = new XMLParser();
        XMLNode root = parser.Parse(AnimationXML.text);

        XMLNode animation = root.GetNode("animation>0");
        _anchor.x = Convert.ToSingle(animation.GetValue("@anchorX"));
        _anchor.y = -Convert.ToSingle(animation.GetValue("@anchorY"));

        XMLNodeList layersXml = root.GetNodeList("animation>0>layer");
        foreach (XMLNode layerXml in layersXml)
        {
            string layerName = layerXml.GetValue("@name").Substring(2);

            int layerNum = Convert.ToInt32(layerXml.GetValue("@zOrder"));
            
            _layerNames.Insert(layerNum, layerName);

            XMLNodeList variants = layerXml.GetNodeList("data");

            foreach (XMLNode frameXml in layerXml.GetNodeList("frame"))
            {
                int frameNum = Convert.ToInt32(frameXml.GetValue("@num"));

                if (frameNum > FrameCount)
                    FrameCount = frameNum;
                
                if (_frameList.Count < frameNum + 1 || _frameList[frameNum] == null)
                    _frameList.Insert(frameNum, new Frame());

                if (_frameList[frameNum].Layers == null)
                    _frameList[frameNum].Layers = new Layer[layersXml.Count];

                _frameList[frameNum].Layers[layerNum] = new Layer();

                foreach (XMLNode variantXml in variants)
                {
                    int variantNum = Convert.ToInt32(variantXml.GetValue("@name").Substring(1));

                    bool isSingleVariant = variants.Count == 1;
                    _frameList[frameNum].Layers[layerNum].Variants.Insert(variantNum, GetVariantTransformations(variantXml, frameXml, layerXml, isSingleVariant, layerNum));
                }
            }
        }
        FrameCount++;
        DestroyImmediate(_tempSprite.gameObject);
        CalculateMaxVerticesAndTriangles();
    }

    private void CalculateMaxVerticesAndTriangles()
    {
        _maxTriangles = 0;
        _maxVertices = 0;
        foreach (Frame frame in _frameList)
        {
            int frameTriangles = 0;
            int frameVertices = 0;
            for (int i = 0; i < frame.Layers.Length; i++)
            {
                Layer layer = frame.Layers[i];

                int layerTriangles = 0;
                int layerVertices = 0;
                if (layer == null)
                {
                    layerVertices = 4;
                    layerTriangles = 6;
                }
                else
                {
                    foreach (Variant variant in layer.Variants)
                    {
                        if (variant.triangles.Length > layerTriangles)
                            layerTriangles = variant.triangles.Length;
                        if (variant.vertices.Length > layerVertices)
                            layerVertices = variant.vertices.Length;
                    }
                }
                frameTriangles += layerTriangles;
                frameVertices += layerVertices;
            }
            if (frameTriangles > _maxTriangles)
                _maxTriangles = frameTriangles;
            if (frameVertices > _maxVertices)
                _maxVertices = frameVertices;
        }
    }

    private Variant GetVariantTransformations(XMLNode variantXml, XMLNode frameXml, XMLNode layerXml, bool isSingleVariant, int layerNum)
    {
        bool visible = !frameXml.ContainsKey("@visible") ? false : Convert.ToBoolean(frameXml.GetValue("@visible"));

        if (visible == false)
            return BlankVariant();

        float a = !frameXml.ContainsKey("@a") ? 1f : Convert.ToSingle(frameXml.GetValue("@a"));
        float b = !frameXml.ContainsKey("@b") ? 0f : Convert.ToSingle(frameXml.GetValue("@b"));
        float c = !frameXml.ContainsKey("@c") ? 0f : Convert.ToSingle(frameXml.GetValue("@c"));
        float d = !frameXml.ContainsKey("@d") ? 1f : Convert.ToSingle(frameXml.GetValue("@d"));
        float tx = !frameXml.ContainsKey("@tx") ? 0f : Convert.ToSingle(frameXml.GetValue("@tx"));
        float ty = !frameXml.ContainsKey("@ty") ? 0f : Convert.ToSingle(frameXml.GetValue("@ty"));
        float alphaMultiplier = !frameXml.ContainsKey("@alpha")
                                     ? 100f
                                     : Convert.ToSingle(frameXml.GetValue("@alpha"));

        float anchorX = !variantXml.ContainsKey("@anchorX") ? 0f : Convert.ToSingle(variantXml.GetValue("@anchorX"));
        float anchorY = !variantXml.ContainsKey("@anchorY") ? 0f : Convert.ToSingle(variantXml.GetValue("@anchorY"));
        string variantName = !variantXml.ContainsKey("@name") ? "v0" : variantXml.GetValue("@name");

        string layerName = layerXml.GetValue("@name").Substring(2);

        Variant result = new Variant();

        _tempSprite.SetSprite(SpriteCollection, layerName + (isSingleVariant ? "" : "_" + variantName));
        Mesh spriteMesh = _tempSprite.GetComponent<MeshFilter>().sharedMesh;

        result.triangles = spriteMesh.triangles;
        result.uv = spriteMesh.uv;

        Color newColor = new Color(1f, 1f, 1f, alphaMultiplier / 100f);

        result.colors = new Color[result.uv.Length];
        for (int i = 0; i < result.colors.Length; i++)
        {
            result.colors[i] = newColor;
        }

        float scaleCoefficient = TargetHeight / AnimationHeight;

        Matrix4x4 variantAnchorMatrix = Matrix4x4.identity;
        variantAnchorMatrix.SetRow(0, new Vector4(1, 0, 0, anchorX * scaleCoefficient));
        variantAnchorMatrix.SetRow(1, new Vector4(0, 1, 0, -anchorY * scaleCoefficient));  
        
        Matrix4x4 transformMatrix = Matrix4x4.identity;
        transformMatrix.SetRow(0, new Vector4(a, -c, 0, tx * scaleCoefficient));
        transformMatrix.SetRow(1, new Vector4(-b, d, 0, -ty * scaleCoefficient));  

        Matrix4x4 resultMatrix = transformMatrix * variantAnchorMatrix;

        result.vertices = spriteMesh.vertices;
        for (int i = 0; i < result.vertices.Length; i++)
        {
            result.vertices[i] = resultMatrix.MultiplyPoint3x4(result.vertices[i]) - (Vector3)_anchor * scaleCoefficient;
            result.vertices[i].z = -layerNum*0.001f;
        }

        return result;
    }
	
    private static Variant BlankVariant()
    {
        Variant result = new Variant();
        result.vertices = new Vector3[3];
        result.uv = new Vector2[3];
        result.colors = new Color[3];
        result.triangles = new int[]{0, 1, 2};
        return result;
    }

    private static Variant _blankVariantInstance;

    public static Variant BlankVariantInstance
    {
        get
        {
            if (_blankVariantInstance == null)
                _blankVariantInstance = BlankVariant();
            return _blankVariantInstance;
        }
    }
}
