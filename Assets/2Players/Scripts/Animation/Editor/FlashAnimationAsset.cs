using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof (FlashAnimation))]
[CanEditMultipleObjects]
public class FlashAnimationAsset : Editor {
    public override void OnInspectorGUI() {
        if (targets.Count() == 1) {
            if (DrawDefaultInspector()) {
                EditorUtility.SetDirty(target);
            }
            if (GUILayout.Button("Parse")) {
                (target as FlashAnimation).ParseAnimation();
                EditorUtility.SetDirty(target);
            }
        }
        else {
            if (GUILayout.Button("Parse")) {
                foreach (var targ in targets) {
                    (targ as FlashAnimation).ParseAnimation();
                    EditorUtility.SetDirty(targ);
                }
            }
        }
    }
}