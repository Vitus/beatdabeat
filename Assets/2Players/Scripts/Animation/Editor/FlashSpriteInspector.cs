using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(FlashSprite))]
public class FlashSpriteInspector : Editor
{
    public override void OnInspectorGUI()
    {
        if (DrawDefaultInspector())
        {
            FlashSprite sprite = target as FlashSprite;
            sprite.ChangeEditorAnimation();
            EditorUtility.SetDirty(target);
        }
    }
}
