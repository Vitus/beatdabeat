﻿using System;
using UnityEngine;
using System.Collections;

namespace NelliTweenEngine {

    public delegate object LerpFunction(object startValue, object endValue, float t);

    public static class LerpFunctions {

        public static object GetDefault<T>() {
            if (typeof (T) == typeof (Vector3)) {
                return new LerpFunction(Vector3Unclamped);
            }
            if (typeof (T) == typeof (Vector2)) {
                return new LerpFunction(Vector2Unclamped);
            }
            if (typeof (T) == typeof (float)) {
                return new LerpFunction(FloatUnclamped);
            }
            throw new Exception("Unknown Lerp type");
        }

        public static object Vector3Unclamped(object startValue, object endValue, float t) {
            return MathN.Vector3LerpUnclamped((Vector3) startValue, (Vector3) endValue, t);
        }

        public static object Vector2Unclamped(object startValue, object endValue, float t) {
            return MathN.Vector2LerpUnclamped((Vector2) startValue, (Vector2) endValue, t);
        }

        public static object FloatUnclamped(object startValue, object endValue, float t) {
            return MathN.LerpUnclamped((float) startValue, (float) endValue, t);
        }
    }
}