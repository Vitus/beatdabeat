﻿using System;
using System.Collections;
using UnityEngine;

namespace NelliTweenEngine {
    public class Tweener<T> {
        public readonly TweenSettings<T> Settings;

        internal Tweener(TweenSettings<T> settings) {
            Settings = settings;
        }

        public bool IsPlaying { get; private set; }

        public void Prepare() {
            if (Settings.Target != null) {
                Settings.OnExecute(Settings.StartValue);
            }
        }

        public void Play() {
            if (Settings.Target is MonoBehaviour)
            {
                Settings.CoroutineHolder = Settings.Target as MonoBehaviour;
            }
            else
            {
                Settings.CoroutineHolder = Settings.Target.gameObject.AddComponent<TweenCoroutineHolder>();
                Settings.IsCustomCoroutineHolder = true;
//                Settings.CoroutineHolder.hideFlags = HideFlags.HideInInspector;
            }

            if (Settings.CoroutineHolder != null) {
                Settings.Coroutine = Settings.CoroutineHolder.StartCoroutine(TweenProcess());
            }
        }

        public void Reverse() {
            if (Settings.Target != null) {
                Settings.Backward = !Settings.Backward;
            }
        }

        public void Stop(float finishPosition = -1) {
            if (Settings.Target != null) {
                if (Settings.IsCustomCoroutineHolder) {
                    GameObject.Destroy(Settings.CoroutineHolder);
                }
                else {
                    Settings.CoroutineHolder.StopCoroutine(Settings.Coroutine);
                }
            }
            if (finishPosition != -1) {
                GoToPosition(finishPosition);
            }
            IsPlaying = false;
        }

        private IEnumerator TweenProcess() {
            IsPlaying = true;
            if (Settings.Prepare) {
                Prepare();
            }
            if (Settings.Delay > 0 && !Settings.Backward) {
                float waitTime = 0f;
                while (waitTime < Settings.Delay)
                {
                    yield return null;
                    waitTime += DeltaTime();
                }
            }

            if (Settings.OnExecute == null) {
                throw new Exception("Execute function must be specified." + Settings.Target.name);
            }

            if (Settings.LerpFunction == null) {
                Settings.LerpFunction = LerpFunctions.GetDefault<T>() as LerpFunction;
            }

            int loopCount = Settings.LoopCount;
            bool backward = Settings.Backward;

            float elapsedTime = Settings.FastForward;
            do {
                while (elapsedTime < Settings.Duration) {
                    float t = backward ? 1 - elapsedTime/Settings.Duration : elapsedTime/Settings.Duration;

                    GoToPosition(t);

                    elapsedTime += DeltaTime()*Settings.TimeScale;
                    yield return null;
                }
                elapsedTime -= Settings.Duration;
                Settings.OnExecute(backward ? Settings.StartValue : Settings.EndValue);

                if (loopCount > 0) {
                    loopCount--;
                }
                if (Settings.LoopType == LoopType.PingPong) {
                    backward = !backward;
                }
            } while (loopCount != 0 && Settings.LoopType != LoopType.None);

            if (Settings.OnComplete != null) {
                Settings.OnComplete();
            }
            if (Settings.IsCustomCoroutineHolder) {
                GameObject.Destroy(Settings.CoroutineHolder);
            }
            IsPlaying = false;
        }

        private void GoToPosition(float t) {
            T value = (T) Settings.LerpFunction(Settings.StartValue, Settings.EndValue, Settings.Easing(t));
            Settings.OnExecute(value);
        }

        private float DeltaTime() {
            return Settings.IgnoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
        }
    }
}