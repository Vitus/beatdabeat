﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace NelliTweenEngine {

    public static class NelliTween {

        public static Tweener<TValue> Tween<TTarget, TValue>(this TTarget target, TweenSettings<TValue> settings) where TTarget : Component {
            settings.Target = target;
            var tweener = new Tweener<TValue>(settings);
            if (settings.AutoPlay) {
                tweener.Play();
            }
            return tweener;
        }

        public static Tweener<float> TweenAlpha(this Component target, TweenSettings<float> settings) {
            settings.LerpFunction = LerpFunctions.FloatUnclamped;
            if (target is tk2dSprite) {
                settings.OnExecute = delegate(float value) {
                    tk2dSprite sprite = (target as tk2dSprite);
                    sprite.color = sprite.color.WithAlpha(value);
                };
            }else if (target is FlashSprite) {
                settings.OnExecute = delegate(float value) {
                    FlashSprite sprite = (target as FlashSprite);
                    sprite.Color = sprite.Color.WithAlpha(value);
                };
            }else if (target is tk2dTextMesh) {
                settings.OnExecute = delegate(float value) {
                    tk2dTextMesh text = (target as tk2dTextMesh);
                    text.color = text.color.WithAlpha(value);
                };
            }else if (target is SpriteRenderer) {
                settings.OnExecute = delegate(float value) {
                    SpriteRenderer spriteRenderer = (target as SpriteRenderer);
                    spriteRenderer.color = spriteRenderer.color.WithAlpha(value);
                };
            }
            else {
                throw new Exception("This is not transparent component, try use gameObject.TweenAlpha, to auto find transparent component.");
            }
            return Tween(target, settings);
        }

        public static Tweener<float> TweenAlpha(this GameObject target, TweenSettings<float> settings) {
            Component componentWithAlpha;
            if (target.GetComponent<CanvasGroup>() != null) {
                componentWithAlpha = target.GetComponent<CanvasGroup>();
            }
            else if (target.GetComponent<tk2dSprite>() != null) {
                componentWithAlpha = target.GetComponent<tk2dSprite>();
            }
            else if (target.GetComponent<FlashSprite>() != null) {
                componentWithAlpha = target.GetComponent<FlashSprite>();
            }
            else if (target.GetComponent<tk2dTextMesh>() != null) {
                componentWithAlpha = target.GetComponent<tk2dTextMesh>();
            }
            else if (target.GetComponent<SpriteRenderer>() != null) {
                componentWithAlpha = target.GetComponent<SpriteRenderer>();
            }
            else {
                return null;
            }
            return componentWithAlpha.TweenAlpha(settings);
        }

        public static Tweener<Vector3> TweenLocalScale(this Transform target, TweenSettings<Vector3> settings) {
            settings.LerpFunction = LerpFunctions.Vector3Unclamped;

            settings.OnExecute =
                delegate(Vector3 value) {
                    target.localScale = new Vector3(
                        settings.AffectAxis.HasX() ? value.x : target.localScale.x,
                        settings.AffectAxis.HasY() ? value.y : target.localScale.y,
                        settings.AffectAxis.HasZ() ? value.z : target.localScale.z
                        );
                };

            return Tween(target, settings);
        }

        public static Tweener<Vector2> TweenRectPosition(this RectTransform target, TweenSettings<Vector2> settings) {
            settings.LerpFunction = LerpFunctions.Vector2Unclamped;

            settings.OnExecute =
                delegate(Vector2 value) {
                    target.anchoredPosition = new Vector2(
                        settings.AffectAxis.HasX() ? value.x : target.anchoredPosition.x,
                        settings.AffectAxis.HasY() ? value.y : target.anchoredPosition.y
                        );
                };

            return Tween(target, settings);
        }

        public static Tweener<Vector3> TweenLocalPosition(this Transform target, TweenSettings<Vector3> settings) {
            settings.LerpFunction = LerpFunctions.Vector3Unclamped;

            settings.OnExecute =
                delegate(Vector3 value) {
                    target.localPosition = new Vector3(
                        settings.AffectAxis.HasX() ? value.x : target.localPosition.x,
                        settings.AffectAxis.HasY() ? value.y : target.localPosition.y,
                        settings.AffectAxis.HasZ() ? value.z : target.localPosition.z
                        );
                };

            return Tween(target, settings);
        }

        public static Tweener<Vector3> TweenPosition(this Transform target, TweenSettings<Vector3> settings) {
            settings.LerpFunction = LerpFunctions.Vector3Unclamped;

            settings.OnExecute =
                delegate(Vector3 value) {
                    target.position = new Vector3(
                        settings.AffectAxis.HasX() ? value.x : target.position.x,
                        settings.AffectAxis.HasY() ? value.y : target.position.y,
                        settings.AffectAxis.HasZ() ? value.z : target.position.z
                        );
                };

            return Tween(target, settings);
        }

        public static Tweener<Vector3> TweenLocalRotation(this Transform target, TweenSettings<Vector3> settings) {
            settings.LerpFunction = LerpFunctions.Vector3Unclamped;

            settings.OnExecute =
                delegate(Vector3 value) {
                    Vector3 euler = target.localRotation.eulerAngles;
                    target.localRotation = Quaternion.Euler(
                        settings.AffectAxis.HasX() ? value.x : euler.x,
                        settings.AffectAxis.HasY() ? value.y : euler.y,
                        settings.AffectAxis.HasZ() ? value.z : euler.z
                        );
                };

            return Tween(target, settings);
        }

        public static Tweener<float> TweenShakeLocalPosition(this Transform target, TweenSettings<float> settings) {
            settings.LerpFunction = LerpFunctions.FloatUnclamped;
            settings.Easing = Ease.Linear;

            settings.Storage = target.localPosition;

            settings.OnComplete = delegate { target.localPosition = (Vector3) settings.Storage; };
            settings.OnExecute =
                delegate(float value) {
                    Vector3 random = new Vector3(Random.Range(-value, value), Random.Range(-value, value), Random.Range(-value, value));
                    target.localPosition = (Vector3)settings.Storage + new Vector3(
                        settings.AffectAxis.HasX() ? random.x : 0,
                        settings.AffectAxis.HasY() ? random.y : 0,
                        settings.AffectAxis.HasZ() ? random.z : 0
                        );
                };

            return Tween(target, settings);
        }

        public static Tweener<Vector3> TweenRotation(this Transform target, TweenSettings<Vector3> settings) {
            settings.LerpFunction = LerpFunctions.Vector3Unclamped;

            settings.OnExecute =
                delegate(Vector3 value) {
                    Vector3 euler = target.rotation.eulerAngles;
                    target.rotation = Quaternion.Euler(
                        settings.AffectAxis.HasX() ? value.x : euler.x,
                        settings.AffectAxis.HasY() ? value.y : euler.y,
                        settings.AffectAxis.HasZ() ? value.z : euler.z
                        );
                };

            return Tween(target, settings);
        }

    }

}