﻿using UnityEngine;
using System.Collections;

namespace NelliTweenEngine {

    public enum EaseType {
        Linear = 0,
        InQuad = 1,
        OutQuad = 2,
        InOutQuad = 3,
        InCubic = 4,
        OutCubic = 5,
        InOutCubic = 6,
        InQuart = 7,
        OutQuart = 8,
        InOutQuart = 9,
        InQuint = 10,
        OutQuint = 11,
        InOutQuint = 12,
        Spring = 13,
        InBack = 14,
        OutBack = 15,
        InOutBack = 16,
        InSine = 17,
        OutSine = 18,
        InOutSine = 19,
        Shake = 20,
    }

    public delegate float EasingFunction(float t);

    public static class Ease {
        public static EasingFunction Enum(EaseType ease) {
            switch (ease) {
                case EaseType.Linear:
                    return Linear;
                case EaseType.InQuad:
                    return InQuad;
                case EaseType.OutQuad:
                    return OutQuad;
                case EaseType.InOutQuad:
                    return InOutQuad;
                case EaseType.InCubic:
                    return InCubic;
                case EaseType.OutCubic:
                    return OutCubic;
                case EaseType.InOutCubic:
                    return InOutCubic;
                case EaseType.InQuart:
                    return InQuart;
                case EaseType.OutQuart:
                    return OutQuart;
                case EaseType.InOutQuart:
                    return InOutQuart;
                case EaseType.InQuint:
                    return InQuint;
                case EaseType.OutQuint:
                    return OutQuint;
                case EaseType.InOutQuint:
                    return InOutQuint;
                case EaseType.Spring:
                    return Spring;
                case EaseType.InBack:
                    return InBack;
                case EaseType.OutBack:
                    return OutBack;
                case EaseType.InOutBack:
                    return InOutBack;
                case EaseType.InSine:
                    return InSine;
                case EaseType.OutSine:
                    return OutSine;
                case EaseType.InOutSine:
                    return InOutSine;
                case EaseType.Shake:
                    return Shake;
                default:
                    return Linear;
            }
        }

        public static float Linear(float t) {
            return t;
        }

        public static float InQuad(float t) {
            return t*t;
        }

        public static float OutQuad(float t) {
            return t*(2 - t);
        }

        public static float InOutQuad(float t) {
            return t < .5 ? 2*t*t : -1 + (4 - 2*t)*t;
        }

        public static float InCubic(float t) {
            return t*t*t;
        }

        public static float OutCubic(float t) {
            return (--t)*t*t + 1;
        }

        public static float InOutCubic(float t) {
            return t < .5 ? 4*t*t*t : (t - 1)*(2*t - 2)*(2*t - 2) + 1;
        }

        public static float InQuart(float t) {
            return t*t*t*t;
        }

        public static float OutQuart(float t) {
            return 1 - (--t)*t*t*t;
        }

        public static float InOutQuart(float t) {
            return t < .5 ? 8*t*t*t*t : 1 - 8*(--t)*t*t*t;
        }

        public static float InQuint(float t) {
            return t*t*t*t*t;
        }

        public static float OutQuint(float t) {
            return 1 + (--t)*t*t*t*t;
        }

        public static float InOutQuint(float t) {
            return t < .5 ? 16*t*t*t*t*t : 1 + 16*(--t)*t*t*t*t;
        }

        public static float Spring(float t) {
            return (Mathf.Sin(t*Mathf.PI*(0.2f + 2.5f*t*t*t))*Mathf.Pow(1f - t, 2.2f) + t)*(1f + (1.2f*(1f - t)));
        }

        public static float InBack(float t) {
            t /= 1;
            float power = 1.70158f;
            return t*t*((power + 1)*t - power);
        }

        public static float OutBack(float t) {
            float power = 1.70158f;
            t = (t) - 1;
            return ((t)*t*((power + 1)*t + power) + 1);
        }

        public static float InOutBack(float t) {
            float power = 1.70158f;
            t /= .5f;
            if ((t) < 1) {
                power *= (1.525f);
                return 0.5f*(t*t*(((power) + 1)*t - power));
            }
            t -= 2;
            power *= (1.525f);
            return 0.5f*((t)*t*(((power) + 1)*t + power) + 2);
        }

        public static float InSine(float t) {
            return 1 - Mathf.Cos(t*(Mathf.PI*0.5f));
        }

        public static float OutSine(float t) {
            return Mathf.Sin(t*(Mathf.PI*0.5f));
        }

        public static float InOutSine(float t) {
            return -0.5f*(Mathf.Cos(Mathf.PI*t) - 1);
        }

        public static float SmoothStep(float value)
        {
            return value * value * (3.0f - 2.0f * value);
        }

        public static float Shake(float value)
        {
            return Random.Range(0f, 2f);
        }

        public static EasingFunction Curve(AnimationCurve curve) {
            return curve.Evaluate;

        }
    }
}