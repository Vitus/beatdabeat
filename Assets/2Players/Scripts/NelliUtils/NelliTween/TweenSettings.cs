﻿using System;
using UnityEngine;

namespace NelliTweenEngine {
    public delegate void TweenExecuteAction<T>(T value);

    public delegate void CompleteAction();

    public class TweenSettings<T>
    {
        internal Component Target;
        internal MonoBehaviour CoroutineHolder;
        internal bool IsCustomCoroutineHolder;
        internal Coroutine Coroutine;
        public bool AutoPlay = true;
        public T StartValue;
        public T EndValue;
        public float Duration = 1f;
        public float Delay = 0f;
        public EasingFunction Easing = Ease.Linear;
        public int LoopCount = 0;
        public LoopType LoopType = LoopType.None;
        public bool Backward = false;
        public float FastForward = 0;
        public float TimeScale = 1f;
        public AffectAxis AffectAxis = AffectAxis.XYZ;
        public float Magnitude;
        public CompleteAction OnComplete;
        public TweenExecuteAction<T> OnExecute;
        public LerpFunction LerpFunction;
        public bool Prepare;
        public object Storage;
        public bool IgnoreTimeScale;
    }
}
