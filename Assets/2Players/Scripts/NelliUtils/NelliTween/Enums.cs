﻿namespace NelliTweenEngine {

    public enum AffectAxis {
        None,
        X,
        Y,
        Z,
        XY,
        XZ,
        YZ,
        XYZ
    }

    internal static class AffectAxisExtension {
        public static bool HasX(this AffectAxis axis) {
            return axis == AffectAxis.X || axis == AffectAxis.XY || axis == AffectAxis.XZ || axis == AffectAxis.XYZ;
        }

        public static bool HasY(this AffectAxis axis) {
            return axis == AffectAxis.Y || axis == AffectAxis.XY || axis == AffectAxis.YZ || axis == AffectAxis.XYZ;
        }

        public static bool HasZ(this AffectAxis axis) {
            return axis == AffectAxis.Z || axis == AffectAxis.XZ || axis == AffectAxis.YZ || axis == AffectAxis.XYZ;
        }
    }

    public enum LoopType {
        None,
        Loop,
        PingPong
    }
}