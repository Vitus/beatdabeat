﻿using UnityEngine;
using System.Collections;

public class MathN {

    public static float AngleBetweenVectors(Vector2 from, Vector2 to)
    {
        float ang = Vector2.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);

        if (cross.z > 0)
            ang = 360 - ang;
        return ang;
    }

    public static Vector2 RotateVector(Vector2 vector, float angle)
    {
        return PolarToCartesian(vector.magnitude, VectorAngle(vector) + angle);
    }
    
    public static float VectorAngle(Vector2 vector)
    {
        return AngleBetweenVectors(vector, Vector2.right);
    }

    public static Vector2 RandomVector2(float length = 1f, bool fixedLength = true) {
        return PolarToCartesian(length*(fixedLength ? 1 : Random.Range(0f, 1f)), Random.Range(0f, 360f));
    }

    public static Vector2 PolarToCartesian(float angle, float length) {
        return new Vector2(Mathf.Cos(angle*Mathf.Deg2Rad), Mathf.Sin(angle*Mathf.Deg2Rad))*length;
    }

    public static Vector2 PolarToCartesian(Vector2 polarVector) {
        return PolarToCartesian(polarVector.x, polarVector.y);
    }

    public static Vector2 CartesianToPolar(float x, float y) {
        return CartesianToPolar(new Vector2(x, y));
    }

    public static Vector2 CartesianToPolar(Vector2 vector) {
        return new Vector2(Mathf.Atan2(vector.y, vector.x)*Mathf.Rad2Deg, vector.magnitude);
    }

    private static Gradient _rainbowGradien;

    public static Gradient RainbowGradient() {
        if (_rainbowGradien == null) {
            _rainbowGradien = new Gradient();
            GradientColorKey[] colors = new GradientColorKey[7];
            colors[0].color = new Color(1, 0, 0, 1);
            colors[1].color = new Color(1, 0, 1, 1);
            colors[2].color = new Color(0, 0, 1, 1);
            colors[3].color = new Color(0, 1, 1, 1);
            colors[4].color = new Color(0, 1, 0, 1);
            colors[5].color = new Color(1, 1, 0, 1);
            colors[6].color = new Color(1, 0, 0, 1);
            colors[0].time = 0/6f;
            colors[1].time = 1/6f;
            colors[2].time = 2/6f;
            colors[3].time = 3/6f;
            colors[4].time = 4/6f;
            colors[5].time = 5/6f;
            colors[6].time = 6/6f;
            _rainbowGradien.colorKeys = colors;
        }
        return _rainbowGradien;
    }

    public static Color RandomRainbowColor() {
        return RainbowGradient().Evaluate(Random.Range(0f, 1f));
    }

    public static float Lerp(float from, float to, float t) {
        if (t < 0.0f) {
            return from;
        }
        else if (t > 1.0f) {
            return to;
        }
        return LerpUnclamped(from, to, t);
    }

    public static float LerpUnclamped(float from, float to, float t) {
        return (1.0f - t)*from + t*to;
    }

    public static float Percent(float min, float max, float value) {
        if (min < max) {
            if (value < min) {
                return 0.0f;
            }
            if (value > max) {
                return 1.0f;
            }
        }
        else {
            if (value < max) {
                return 1.0f;
            }
            if (value > min) {
                return 0.0f;
            }
        }
        return PercentUnclamped(min, max, value);
    }

    public static float PercentUnclamped(float min, float max, float value) {
        return (value - min)/(max - min);
    }

    public static Color ColorLerp(Color c1, Color c2, float t) {
        if (t > 1.0f) {
            return c2;
        }
        else if (t < 0.0f) {
            return c1;
        }
        return ColorLerpUnclamped(c1, c2, t);
    }

    public static Color ColorLerpUnclamped(Color c1, Color c2, float t) {
        return new Color(c1.r + (c2.r - c1.r)*t,
            c1.g + (c2.g - c1.g)*t,
            c1.b + (c2.b - c1.b)*t,
            c1.a + (c2.a - c1.a)*t);
    }

    public static Vector2 Vector2Lerp(Vector2 v1, Vector2 v2, float t) {
        if (t > 1.0f) {
            return v2;
        }
        else if (t < 0.0f) {
            return v1;
        }
        return Vector2LerpUnclamped(v1, v2, t);
    }

    public static Vector2 Vector2LerpUnclamped(Vector2 v1, Vector2 v2, float t) {
        return new Vector2(v1.x + (v2.x - v1.x)*t,
            v1.y + (v2.y - v1.y)*t);
    }

    public static Vector3 Vector3Lerp(Vector3 v1, Vector3 v2, float t) {
        if (t > 1.0f) {
            return v2;
        }
        else if (t < 0.0f) {
            return v1;
        }
        return Vector3LerpUnclamped(v1, v2, t);
    }

    public static Vector3 Vector3LerpUnclamped(Vector3 v1, Vector3 v2, float t) {
        return new Vector3(v1.x + (v2.x - v1.x)*t,
            v1.y + (v2.y - v1.y)*t,
            v1.z + (v2.z - v1.z)*t);
    }

    public static Vector4 Vector4Lerp(Vector4 v1, Vector4 v2, float t) {
        if (t > 1.0f) {
            return v2;
        }
        else if (t < 0.0f) {
            return v1;
        }
        return Vector4LerpUnclamped(v1, v2, t);
    }

    private static Vector4 Vector4LerpUnclamped(Vector4 v1, Vector4 v2, float t) {
        return new Vector4(v1.x + (v2.x - v1.x)*t,
            v1.y + (v2.y - v1.y)*t,
            v1.z + (v2.z - v1.z)*t,
            v1.w + (v2.w - v1.w)*t);
    }

}