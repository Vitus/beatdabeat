﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(tk2dBaseSprite))]
public class NelliFader : MonoBehaviour
{
    public static NelliFader Instance { get; private set; }

    public float DefaultFadeTime;

    private float _currentFadeTime;
    private float _currentFade;
    private int _fadeDirection;
    private tk2dBaseSprite _sprite;
    private bool _useRealTime;

    void Awake()
    {
        Instance = this;
        _sprite = GetComponent<tk2dBaseSprite>();
    }

    void Update()
    {
        if (_fadeDirection != 0) {
            GetComponent<Renderer>().enabled = true;
            float target = 0;
            if (_fadeDirection == -1)
                target = 0;
            if (_fadeDirection == 1)
                target = 1;

            float deltaTime = _useRealTime ? Time.unscaledDeltaTime : Time.deltaTime;

            _currentFade = Mathf.MoveTowards(_currentFade, target, deltaTime/_currentFadeTime);
            if (_currentFade == target)
                _fadeDirection = 0;
            _sprite.color = _sprite.color.WithAlpha(_currentFade);
        }
        else {
            GetComponent<Renderer>().enabled = _currentFade > 0;
        }
        
    }

    public void FadeIn(float fadeTime = 0, bool useRealTime = false)
    {
        _useRealTime = useRealTime;
        _currentFadeTime = fadeTime > 0 ? fadeTime : DefaultFadeTime;
        _fadeDirection = +1;
    }

    public void FadeOut(float fadeTime = 0, bool useRealTime = false)
    {
        _useRealTime = useRealTime;
        _currentFadeTime = fadeTime > 0 ? fadeTime : DefaultFadeTime;
        _fadeDirection = -1;
    }
}
