﻿using UnityEngine;
using System.Collections;

public static class NEvent
{
//    private static GameObject _eventManager;
//    private static bool _wasCreated = false;
//    private static GameObject EventManager
//    {
//        get
//        {
//            if (_eventManager == null && !_wasCreated)
//            {
//                _wasCreated = true;
//                _eventManager = new GameObject("EventManager");
//                GameObject.DontDestroyOnLoad(_eventManager);
//            }
//            return _eventManager;
//        }
//    }

    public static void AddListener<T>(Component component, NelliEventDispatcher.EventDelegate<T> listener) where T : NelliEvent
    {
        if (component != null)
            GetDispatcher(component).AddListener<T>(listener);
    }

    public static void AddListener<T>(GameObject gameObject, NelliEventDispatcher.EventDelegate<T> listener) where T : NelliEvent
    {
        if (gameObject != null)
            GetDispatcher(gameObject).AddListener<T>(listener);
    }

//    public static void AddListener<T>(NelliEventDispatcher.EventDelegate<T> listener) where T : NelliEvent
//    {
//        if (EventManager != null)
//            GetDispatcher(EventManager).AddListener<T>(listener);
//    }
//
    public static void RemoveListener<T>(Component component, NelliEventDispatcher.EventDelegate<T> listener) where T : NelliEvent
    {
        if (component != null)
            GetDispatcher(component).RemoveListener<T>(listener);
    }

    public static void RemoveListener<T>(GameObject gameObject, NelliEventDispatcher.EventDelegate<T> listener) where T : NelliEvent
    {
        if (gameObject != null)
            GetDispatcher(gameObject).RemoveListener<T>(listener);
    }

//    public static void RemoveListener<T>(NelliEventDispatcher.EventDelegate<T> listener) where T : NelliEvent
//    {
//        if (EventManager != null)
//            GetDispatcher(EventManager).RemoveListener<T>(listener);
//    }
//
    public static void Dispatch<T>(Component component, T e) where T : NelliEvent
    {
        if (component != null)
            GetDispatcher(component).Dispatch<T>(e);
    }

    public static void Dispatch<T>(GameObject gameObject, T e) where T : NelliEvent
    {
        if (gameObject != null)
            GetDispatcher(gameObject).Dispatch<T>(e);
    }

//    public static void Dispatch<T>(T e) where T : NelliEvent
//    {
//        if (EventManager != null)
//            GetDispatcher(EventManager).Dispatch<T>(e);
//    }

    public static bool HasListener<T>(Component component) where T : NelliEvent
    {       
        if (component != null)
            return GetDispatcher(component).HasListener<T>();
        else
            return false;
    }

    public static bool HasListener<T>(GameObject gameObject) where T : NelliEvent
    {
        if (gameObject != null)
            return GetDispatcher(gameObject).HasListener<T>();
        else
            return false;
    }

//    public static bool HasListener<T>() where T : NelliEvent
//    {
//        if (EventManager != null)
//            return GetDispatcher(EventManager).HasListener<T>();
//        else
//            return false;
//    }

    public static NelliEventDispatcher GetDispatcher(Component component)
    {
        NelliEventDispatcher dispatcher = component.GetComponent<NelliEventDispatcher>();
        if (dispatcher == null)
            dispatcher = component.gameObject.AddComponent<NelliEventDispatcher>();
        return dispatcher;
    }

    public static NelliEventDispatcher GetDispatcher(GameObject gameObject)
    {
        NelliEventDispatcher dispatcher = gameObject.GetComponent<NelliEventDispatcher>();
        if (dispatcher == null)
            dispatcher = gameObject.AddComponent<NelliEventDispatcher>();
        return dispatcher;
    }

//    public static NelliEventDispatcher GetDispatcher()
//    {
//        if (EventManager != null)
//        {
//            NelliEventDispatcher dispatcher = EventManager.GetComponent<NelliEventDispatcher>();
//            if (dispatcher == null)
//                dispatcher = EventManager.AddComponent<NelliEventDispatcher>();
//            return dispatcher;
//        }
//        else
//        {
//            return null;
//        }
//    }

}
