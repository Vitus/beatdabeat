﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class NelliEventDispatcher : MonoBehaviour
{
    public delegate void EventDelegate<T>(T e) where T : NelliEvent;

    private readonly Dictionary<Type, Delegate> _delegates = new Dictionary<Type, Delegate>();

    public void AddListener<T>(EventDelegate<T> listener) where T : NelliEvent
    {
        Delegate newDelegate;
        Type eventType = typeof (T);
        if (_delegates.TryGetValue(eventType, out newDelegate))
        {
            _delegates[eventType] = Delegate.Combine(newDelegate, listener);
        }
        else
        {
            _delegates[eventType] = listener;
        }
    }

    public void RemoveListener<T>(EventDelegate<T> listener) where T : NelliEvent
    {
        Delegate currentDelegate;
        Type eventType = typeof(T);
        if (_delegates.TryGetValue(eventType, out currentDelegate))
        {
            currentDelegate = Delegate.Remove(currentDelegate, listener);
            if (currentDelegate == null)
            {
                _delegates.Remove(eventType);
            }
            else
            {
                _delegates[eventType] = currentDelegate;
            }
        }
    }

    public bool HasListener<T>() where T : NelliEvent
    {
        Type eventType = typeof(T);
        return _delegates.ContainsKey(eventType);
    }

    public void Dispatch<T>(T e) where T : NelliEvent
    {
        if (e == null)
        {
            throw new ArgumentNullException("e");
        }

        Delegate delegateToCall;
        Type eventType = typeof(T);
        if (_delegates.TryGetValue(eventType, out delegateToCall))
        {
            EventDelegate<T> callback = delegateToCall as EventDelegate<T>;
            if (callback != null)
            {
                callback(e);
            }
        }
    }

}
