﻿using UnityEngine;
using System.Collections;

public class NbtParallel : NbtComposite {

    public NbtParallel(params NbtNode[] childs)
        : base(childs)
    {
    }

    protected override NbtStatus OnUpdate()
    {
        NbtStatus resultStatus = NbtStatus.Success;
        foreach (NbtNode child in Childs)
        {
            NbtStatus childStatus = child.Tick();
            if (childStatus == NbtStatus.Running && resultStatus == NbtStatus.Success)
                resultStatus = NbtStatus.Running;
            if (childStatus == NbtStatus.Failure && (resultStatus == NbtStatus.Success || resultStatus == NbtStatus.Running))
                resultStatus = NbtStatus.Failure;
        }
        return resultStatus;
    }
}
