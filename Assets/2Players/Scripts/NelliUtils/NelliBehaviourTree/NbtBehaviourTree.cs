﻿using UnityEngine;
using System.Collections;

public abstract class NbtBehaviourTree : MonoBehaviour 
{
    protected NbtNode RootNode;

    public virtual void AfterUpdate(){}
    public virtual void BeforeUpdate(){}

    public abstract NbtNode SetupTree();

    void Awake()
    {
        RootNode = SetupTree();
        RootNode.SetOwner(this);
    }

    void Update()
    {
        BeforeUpdate();
        RootNode.Tick();
        AfterUpdate();
    }

}
