﻿using UnityEngine;
using System.Collections;

public class NbtRepeatedSequence : NbtComposite {

    public NbtRepeatedSequence(params NbtNode[] childs) : base (childs)
    {
    }

    protected override NbtStatus OnUpdate()
    {
        foreach (NbtNode child in Childs)
        {
            NbtStatus childStatus = child.Tick();
            if (childStatus != NbtStatus.Success)
                return childStatus;
        }
        return NbtStatus.Success;
    }

}
