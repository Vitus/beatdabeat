﻿using UnityEngine;
using System.Collections;

public abstract class NbtCondition : NbtNode
{
    protected abstract bool Test();

    protected override sealed NbtStatus OnUpdate()
    {
        if (Test())
            return NbtStatus.Success;
        else
            return NbtStatus.Failure;
    }

}
