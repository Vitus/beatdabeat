﻿using System;
using UnityEngine;
using System.Collections;

public class NbtInput<T>
{
    private Func<T> _function;
    private NbtProperty<T> _property;
    private T _value;

    public NbtInput(T value)
    {
        _value = value;
    }

    public NbtInput(NbtProperty<T> property)
    {
        _property = property;
    }

    public NbtInput(Func<T> function)
    {
        _function = function;
    }

    public T Value
    {
        get
        {
            if (_property != null)
                return _property.Value;
            if (_function != null)
                return _function();
            return _value;
        }
    }

    public NbtProperty<T> GetPropertyIfAny()
    {
        return _property;
    }

    public Func<T> GetFunctionIfAny()
    {
        return _function;
    }

    public static implicit operator T(NbtInput<T> input)
    {
        return input.Value;
    }

    public static implicit operator NbtInput<T>(T value)
    {
        return new NbtInput<T>(value);
    }

    public static implicit operator NbtProperty<T>(NbtInput<T> input)
    {
        return input.GetPropertyIfAny();
    }

    public static implicit operator NbtInput<T>(NbtProperty<T> property)
    {
        return new NbtInput<T>(property);
    }
    public static implicit operator Func<T>(NbtInput<T> input)
    {
        return input.GetFunctionIfAny();
    }

    public static implicit operator NbtInput<T>(Func<T> function)
    {
        return new NbtInput<T>(function);
    }
}
