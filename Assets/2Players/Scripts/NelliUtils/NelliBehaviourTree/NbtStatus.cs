﻿public enum NbtStatus
{
    None = 0,
    Failure = 1,
    Running = 2,
    Success = 3
}
