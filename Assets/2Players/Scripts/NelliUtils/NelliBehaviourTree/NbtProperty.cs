﻿using System;

public class NbtProperty<T>
{
    public T Value { get; set; }

    private Func<T> _createDefaultValueFunc;

    public NbtProperty() : this (default(T)) { }
    public NbtProperty(T defaultValue) : this (() => defaultValue) { }
    public NbtProperty(Func<T> createDefaultValueFunc)
    {
        _createDefaultValueFunc = createDefaultValueFunc;
        Value = createDefaultValueFunc();
    }

    public T GetDefault()
    {
        return _createDefaultValueFunc();
    }
    
}
