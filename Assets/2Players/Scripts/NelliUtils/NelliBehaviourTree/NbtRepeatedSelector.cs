﻿using UnityEngine;
using System.Collections;

public class NbtRepeatedSelector : NbtComposite 
{

    public NbtRepeatedSelector(params NbtNode[] childs) : base (childs)
    {
    }

    protected override NbtStatus OnUpdate()
    {
        foreach (NbtNode child in Childs)
        {
            NbtStatus childStatus = child.Tick();
            if (childStatus != NbtStatus.Failure)
                return childStatus;
        }
        return NbtStatus.Failure;
    }
}
