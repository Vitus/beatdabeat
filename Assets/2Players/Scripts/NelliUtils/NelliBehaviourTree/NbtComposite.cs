﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class NbtComposite : NbtNode
{
    protected NbtNode[] Childs;

    public NbtComposite(params NbtNode[] childs)
    {
        Childs = childs;
    }

    public override void SetOwner(MonoBehaviour owner)
    {
        base.SetOwner(owner);
        foreach (NbtNode child in Childs)
        {
            child.SetOwner(owner);
        }
    }
}
