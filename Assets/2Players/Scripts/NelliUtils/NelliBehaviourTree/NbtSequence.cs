﻿using UnityEngine;

public class NbtSequence : NbtComposite
{
    private int _currentChildNum;

    public NbtSequence(params NbtNode[] childs) : base(childs)
    {
    }

    protected override void OnStart()
    {
        _currentChildNum = 0;
    }

    protected override NbtStatus OnUpdate()
    {
        while (true)
        {
            NbtStatus s = Childs[_currentChildNum].Tick();

            if (s != NbtStatus.Success)
            {
                return s;
            }

            _currentChildNum++;
            if (_currentChildNum == Childs.Length)
            {
                return NbtStatus.Success;
            }
        }
    }


}
