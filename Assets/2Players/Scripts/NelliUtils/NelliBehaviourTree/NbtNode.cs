﻿using UnityEngine;

public class NbtNode
{
    private NbtStatus _status = NbtStatus.None;

    protected virtual NbtStatus OnUpdate() { return NbtStatus.Success; }

    protected MonoBehaviour Owner;

    protected virtual void OnStart() { }
    protected virtual void OnEnd(NbtStatus status) { }

    public virtual void SetOwner(MonoBehaviour owner)
    {
        Owner = owner;
    }

    public NbtStatus Tick()
    {
        if (_status == NbtStatus.None)
        {
            OnStart();
        }

        _status = OnUpdate();

        if (_status != NbtStatus.Running)
        {
            OnEnd(_status);
            NbtStatus result = _status;
            _status = NbtStatus.None;
            return result;
        }

        return _status;
    }

}
