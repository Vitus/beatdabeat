﻿using UnityEngine;
using System.Collections;

public class NbtDecorator : NbtNode
{

    protected NbtNode Child;

    public NbtDecorator(NbtNode child)
    {
        Child = child;
    }

    public override void SetOwner(MonoBehaviour owner)
    {
        base.SetOwner(owner);
        Child.SetOwner(owner);
    }

}
