﻿using System;
using Random = UnityEngine.Random;

[Serializable]
public class MinMaxInt
{
    public int Min;
    public int Max;

    public int GetRandom()
    {
        return Random.Range(Min, Max + 1);
    }
}

[Serializable]
public class MinMaxFloat
{
    public float Min;
    public float Max;

    public float Random()
    {
        return UnityEngine.Random.Range(Min, Max);
    }
}
