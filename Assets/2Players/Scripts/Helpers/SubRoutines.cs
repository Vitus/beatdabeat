﻿using UnityEngine;
using System.Collections;

public class SubRoutines : MonoBehaviour
{
    public static IEnumerator WaitForTrackTime(float soundPosition)
    {
        while (Game.Music.Time < soundPosition)
        {
            yield return null;
        }
    }

    public static IEnumerator SoundTimeSleep(float seconds)
    {
        float startTime = Game.Music.Time;
        while (Game.Music.Time < startTime + seconds)
        {
            yield return null;
        }
    }   
    
    public static IEnumerator Sleep(float seconds)
    {
        float startTime = Time.time;
        while (Time.time < startTime + seconds)
        {
            yield return null;
        }
    }
 }
