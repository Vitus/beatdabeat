using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using Object = System.Object;
using Random = UnityEngine.Random;

public static class H
{
    public static Coroutine WaitForUnscaledSeconds(this MonoBehaviour behaviour, float time) {
        return behaviour.StartCoroutine(WaitForUnscaledSecondsInternal(time));
    }

    private static IEnumerator WaitForUnscaledSecondsInternal(float time) {
        float endTime = Time.unscaledTime + time;
        while (Time.unscaledTime < endTime) {
            yield return null;
        }
    }

    public static void PlayerPrefsFormerlyKnownAs(string oldName, string newName) {
        if (PlayerPrefs.HasKey(oldName)) {
            if (!PlayerPrefs.HasKey(newName)) {
                PlayerPrefs.SetString(newName, PlayerPrefs.GetString(oldName));
            }
            PlayerPrefs.DeleteKey(oldName);
        }
    }


    public static Vector3 RandomVector3(float length, bool fixedLength = true)
    {
        return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * Random.Range(fixedLength ? length : 0, length);
    }

    public static Vector2 RandomVector2(float length, bool fixedLength = true)
    {
        float resultLength = length;
        if (!fixedLength)
            resultLength *= Random.Range(0f, 1f);
        return PolarVector2(resultLength, Random.Range(0f, 360f));
    }

    public static Vector2 PolarVector2(float length, float angle)
    {
        float radAngle = angle*Mathf.Deg2Rad;
        return new Vector2(length * Mathf.Cos(radAngle), length * Mathf.Sin(radAngle));
    }

    public static T GetRandomElement<T>(List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    public static T GetRandomElement<T>(Array array) where T : class
    {
        return array.GetValue(Random.Range(0, array.Length)) as T;
    }

    public static float RandomizeValue(float value, float randomFactor)
    {
        return value * (1 + Random.Range(-randomFactor, randomFactor));
    }

    public static float AngleBetweenVectors(Vector2 from, Vector2 to)
    {
        float ang = Vector2.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);

        if (cross.z > 0)
            ang = 360 - ang;
        return ang;
    }

    public static float VectorAngle(Vector2 vector)
    {
        return AngleBetweenVectors(vector, Vector2.right);
    }

    public static bool ApproximatlyEquals(float num1, float num2, float epsilon = 0.0000001f)
    {
        return Mathf.Abs(num1 - num2) < epsilon;
    }

    private static Gradient _rainbowGradien;
    public static Gradient RainbowGradient()
    {
        if (_rainbowGradien == null)
        {
            _rainbowGradien = new Gradient();
            GradientColorKey[] colors = new GradientColorKey[7];
            colors[0].color = new Color(1, 0, 0, 1);
            colors[1].color = new Color(1, 0, 1, 1);
            colors[2].color = new Color(0, 0, 1, 1);
            colors[3].color = new Color(0, 1, 1, 1);
            colors[4].color = new Color(0, 1, 0, 1);
            colors[5].color = new Color(1, 1, 0, 1);
            colors[6].color = new Color(1, 0, 0, 1);
            colors[0].time = 0 / 6f;
            colors[1].time = 1 / 6f;
            colors[2].time = 2 / 6f;
            colors[3].time = 3 / 6f;
            colors[4].time = 4 / 6f;
            colors[5].time = 5 / 6f;
            colors[6].time = 6 / 6f;
            _rainbowGradien.colorKeys = colors;
        }
        return _rainbowGradien;
    }
    public static Color RandomRainbowColor()
    {
       return RainbowGradient().Evaluate(Random.Range(0f, 1f));
    }
    
    private const int SinCosTableSize = 360;
    private static float[] _sinTable;
    private static float[] _cosTable;

    public static float SinRough(float angle)
    {
        if (_sinTable == null)
        {
            _sinTable = new float[SinCosTableSize];
            for (int i = 0; i < SinCosTableSize; i++)
            {
                _sinTable[i] = Mathf.Sin((i / SinCosTableSize) * 2 * Mathf.PI);
            }
        }
        return _sinTable[(int)(angle / 360f * SinCosTableSize)];
    }

    public static float CosRough(float angle)
    {
        if (_cosTable == null)
        {
            _cosTable = new float[SinCosTableSize];
            for (int i = 0; i < SinCosTableSize; i++)
            {
                _cosTable[i] = Mathf.Cos((i / SinCosTableSize) * 2 * Mathf.PI);
            }
        }
        return _cosTable[(int)(angle / 360f * SinCosTableSize)];
    }

    public static object GetRandomElement(params object[] items)
    {
        return items[Random.Range(0, items.Length)];
    }

    private static string GetHex(int num)
    {
        const string alpha = "0123456789ABCDEF";
        string ret = "" + alpha[num];
        return ret;
    }

    private static int HexToInt(char hexChar)
    {
        switch (hexChar)
        {
            case '0': return 0;
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            case 'A': case 'a': return 10;
            case 'B': case 'b': return 11;
            case 'C': case 'c': return 12;
            case 'D': case 'd': return 13;
            case 'E': case 'e': return 14;
            case 'F': case 'f': return 15;
        }
        return -1;
    }

    public static string ColorToHex(Color color)
    {
        float red = color.r * 255;
        float green = color.g * 255;
        float blue = color.b * 255;

        string a = GetHex(Mathf.FloorToInt(red / 16));
        string b = GetHex(Mathf.RoundToInt(red) % 16);
        string c = GetHex(Mathf.FloorToInt(green / 16));
        string d = GetHex(Mathf.RoundToInt(green) % 16);
        string e = GetHex(Mathf.FloorToInt(blue / 16));
        string f = GetHex(Mathf.RoundToInt(blue) % 16);

        return a + b + c + d + e + f;
    }

    public static Color HexToColor(string color)
    {
        float red = (HexToInt(color[1]) + HexToInt(color[0]) * 16f) / 255f;
        float green = (HexToInt(color[3]) + HexToInt(color[2]) * 16f) / 255f;
        float blue = (HexToInt(color[5]) + HexToInt(color[4]) * 16f) / 255f;
        Color finalColor = new Color { r = red, g = green, b = blue, a = 1 };
        return finalColor;
    }

    public static float GetMax(float[] floats)
    {
        float max = floats[0];
        foreach (float f in floats)
        {
            if (max < f)
                max = f;
        }
        return max;
    }

//    public static List<string> reverseStringFormat(string template, string str)
//    {
//        string pattern = "^" + Regex.Replace(template, @"\{[0-9]+\}", "(.*?)") + "$";
//
//        Regex r = new Regex(pattern);
//        Match m = r.Match(str);
//
//        List<string> ret = new List<string>();
//
//        for (int i = 1; i < m.Groups.Count; i++)
//        {
//            ret.Add(m.Groups[i].Value);
//        }
//
//        return ret;
//    }

    public static Vector2 RotateVector(Vector2 vector, float angle)
    {
        return H.PolarVector2(vector.magnitude, H.VectorAngle(vector) + angle);

    }
}
