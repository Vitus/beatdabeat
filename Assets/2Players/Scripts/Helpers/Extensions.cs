﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public static class Extensions 
{
    public static void SetToZero(this IList<float> array)
    {
        for (int i = 0; i < array.Count; i++)
        {
            array[i] = 0;
        }
    }

    public static void GetValuesFrom(this IList<float> array, IList<float> fromArray)
    {
        for (int i = 0; i < array.Count; i++)
        {
            if (fromArray.Count == i)
                return;
            array[i] = fromArray[i];
        }
    }

    public static int GetIntX(this Vector2 vector)
    {
        return (int) vector.x;
    }

    public static void SetIntX(this Vector2 vector, int value)
    {
        vector.x = value;
    }

    public static int GetIntY(this Vector2 vector)
    {
        return (int) vector.y;
    }

    public static void SetIntY(this Vector2 vector, int value)
    {
        vector.y = value;
    }

    public static void SetXPosition(this Transform transform, float x)
    {
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public static void SetYPosition(this Transform transform, float y)
    {
        transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }

    public static void SetZPosition(this Transform transform, float z)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, z);
    }

    public static void SetXLocalPosition(this Transform transform, float x) {
        transform.localPosition = new Vector3(x, transform.localPosition.y, transform.localPosition.z);
    }

    public static void SetYLocalPosition(this Transform transform, float y)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
    }

    public static void SetZLocalPosition(this Transform transform, float z)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, z);
    }

    public static void SetXLocalScale(this Transform transform, float x)
    {
        transform.localScale = new Vector3(x, transform.localScale.y, transform.localScale.z);
    }

    public static void SetYLocalScale(this Transform transform, float y)
    {
        transform.localScale = new Vector3(transform.localScale.x, y, transform.localScale.z);
    }

    public static void SetZLocalScale(this Transform transform, float z)
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, z);
    }

    public static Color WithAlpha(this Color color, float alpha)
    {
        color.a = alpha;
        return color;
    }

    public static Color MultipliedBy(this Color color, float a)
    {
        color.r *= a;
        color.g *= a;
        color.b *= a;
        return color;
    }

    public static bool ConsistsOf(this string token, bool digits, bool letters, string characters)
    {
        if (string.IsNullOrEmpty(token))
            return false;

        bool suitable = true;
        for (int i = 0; i < token.Length; i++)
        {
            bool symbolSuitable = false;
            
            if (digits && char.IsDigit(token[i]))
                symbolSuitable = true;
            else if (letters && char.IsLetter(token[i]))
                symbolSuitable = true;
            else if (token[i].ToString() == characters)
                symbolSuitable = true;
            
            suitable &= symbolSuitable;
        }
        return suitable;
    }

}
