using UnityEngine;
using System.Collections;

public class SpriteTweener : MonoBehaviour
{
    private tk2dSprite _sprite;
    private FlashSprite _flashSprite;

    private tk2dSprite Sprite
    {
        get
        {
            if (_sprite == null)
                _sprite = GetComponent<tk2dSprite>();
            return _sprite;
        }
    }

    private FlashSprite FlashSprite
    {
        get
        {
            if (_flashSprite == null)
                _flashSprite = GetComponent<FlashSprite>();
            return _flashSprite;
        }
    }

	void Color(Color value)
	{
        if (Sprite != null)
        {
            ColorTk2d(value);
        }
        if (FlashSprite != null)
        {
            ColorFlash(value);
        }
	}

	void Alpha(float value)
	{
        if (Sprite != null)
        {
            AlphaTk2d(value);
        }
        if (FlashSprite != null)
        {
            AlphaFlash(value);
        }
	}

    void Red(float value)
    {
        if (Sprite != null)
        {
            RedTk2d(value);
        }
        if (FlashSprite != null)
        {
            RedFlash(value);
        }
    }

    void Green(float value)
    {
        if (Sprite != null)
        {
            GreenTk2d(value);
        }
        if (FlashSprite != null)
        {
            GreenFlash(value);
        }
    }

    void Blue(float value)
    {
        if (Sprite != null)
        {
            BlueTk2d(value);
        }
        if (FlashSprite != null)
        {
            BlueFlash(value);
        }
    }

    void White(float value)
    {
        if (Sprite != null)
        {
            WhiteTk2d(value);
        }
        if (FlashSprite != null)
        {
            WhiteFlash(value);
        }
    }

    private void AlphaTk2d(float value)
    {
        Color color = Sprite.color;
        color.a = value;
        Sprite.color = color;
    }

    private void ColorTk2d(Color value)
    {
        Sprite.color = value;
    }

    private void RedTk2d(float value)
    {
        Color color = Sprite.color;
        color.r = value;
        Sprite.color = color;
    }

    private void GreenTk2d(float value)
    {
        Color color = Sprite.color;
        color.g = value;
        Sprite.color = color;
    }

    private void BlueTk2d(float value)
    {
        Color color = Sprite.color;
        color.b = value;
        Sprite.color = color;
    }

    private void WhiteTk2d(float value)
    {
        Color color = Sprite.color;
        color.r = value;
        color.g = value;
        color.b = value;
        Sprite.color = color;
    }

    void AlphaFlash(float value)
    {
        FlashSprite.Color.a = value;
    }

    void RedFlash(float value)
    {
        FlashSprite.Color.r = value;
    }

    void GreenFlash(float value)
    {
        FlashSprite.Color.g = value;
    }

    void BlueFlash(float value)
    {
        FlashSprite.Color.b = value;
    }

    void WhiteFlash(float value)
    {
        FlashSprite.Color.r = value;
        FlashSprite.Color.g = value;
        FlashSprite.Color.b = value;
    }

    private void ColorFlash(Color value)
    {
        FlashSprite.Color = value;
    }
}
