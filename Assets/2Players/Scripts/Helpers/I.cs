using UnityEngine;
using System.Collections;

public static class I
{
    public const string Axis = "axis";
    public const string OrientToPath = "orienttopath";
    public const string Position = "position";
    public const string Rotation = "rotation";
    public const string X = "x";
    public const string Y = "y";
    public const string Z = "z";
    public const string Amount = "amount";
    public const string Color = "color";
    public const string R = "r";
    public const string G = "g";
    public const string B = "b";
    public const string A = "a";
    public const string Time = "time";
    public const string EaseType = "easetype";
    public const string LoopType = "looptype";
    public const string Space = "space";
    public const string IsLocal = "islocal";
    public const string OnComplete = "oncomplete";
    public const string OnCompleteTarget = "oncompletetarget";
    public const string OnCompleteParams = "oncompleteparams";
    public const string Path = "path";
    public const string MoveToPath = "movetopath";
    public const string Name = "name";
    public const string Delay = "delay";
    public const string IgnoreTimeScale = "ignoretimescale";
    public const string Scale = "scale";
    public const string From = "from";
    public const string To = "to";
    public const string OnUpdate = "onupdate";
}
