using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MyTime : MonoBehaviour
{
    [SerializeField]
    public class Timer
    {
        public float Time = 0f;
        public float DeltaTime = 0f;
        public float TimeScale = 1f;
    }

    private static MyTime instance;

    [SerializeField]
    private List<string> CustomTimers;
    private Dictionary<string, Timer> _customTimersDictionary = new Dictionary<string, Timer>();

    private float _realDeltaTime = 0f;
    private float _previousFrameTime;

    void Awake()
    {
        instance = this;
        _previousFrameTime = UnityEngine.Time.realtimeSinceStartup;

        foreach (string customTimer in CustomTimers)
        {
            _customTimersDictionary.Add(customTimer, new Timer());
        }
	}

    void Update()
    {
        _realDeltaTime = UnityEngine.Time.realtimeSinceStartup - _previousFrameTime;

        foreach (KeyValuePair<string, Timer> timer in _customTimersDictionary)
        {
            timer.Value.DeltaTime = _realDeltaTime * timer.Value.TimeScale;
            timer.Value.Time += timer.Value.DeltaTime;
        }

        _previousFrameTime = UnityEngine.Time.realtimeSinceStartup;
    }

    public static void AddTimer(string timerName)
    {
        if (!instance._customTimersDictionary.ContainsKey(timerName))
        {
            instance._customTimersDictionary.Add(timerName, new Timer());
        }
        else
        {
            Debug.LogWarning("Trying to add second timer with same name: " + timerName);
        }
    }

    public static void RemoveTimer(string timerName)
    {
        if (instance._customTimersDictionary.ContainsKey(timerName))
        {
            instance._customTimersDictionary.Remove(timerName);
        }
        else
        {
            Debug.LogWarning("Trying to remove nonexistent timer: " + timerName);
        }
    }

    public static float Time
    {
        get { return UnityEngine.Time.time; }
    }
    public static float DeltaTime
    {
        get { return UnityEngine.Time.deltaTime; }
    }
    public static float TimeScale
    {
        get { return UnityEngine.Time.timeScale; }
        set { UnityEngine.Time.timeScale = value; }
    }
    
    public static float RealTime
    {
        get { return UnityEngine.Time.realtimeSinceStartup; }
    }
    public static float RealDeltaTime
    {
        get { return instance._realDeltaTime; }
    }

    public static float GetTime(string timer)
    {
        return instance._customTimersDictionary[timer].Time;
    }

    public static float GetDeltaTime(string timer)
    {
        return instance._customTimersDictionary[timer].DeltaTime;
    }

    public static float GetTimeScale(string timer)
    {
        return instance._customTimersDictionary[timer].TimeScale;
    }

    public static void SetTimeScale(string timer, float newTimeScale)
    {
        instance._customTimersDictionary[timer].TimeScale = newTimeScale;
    }



}
