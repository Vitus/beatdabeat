using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class PolygonEditor : MonoBehaviour
{
    [HideInInspector]
    public List<Vector2> SplinePoints = new List<Vector2>();
    public float TextureSize = 1;
    public int SplineApproximation = 1;
    public bool Editable = false;
    public bool Textured = true;
    public bool ClosedLine = true;
    public bool Batch = true;
    public int PointSize = 10;


    public List<GameObject> ConturPrefab;
    public int ConturApproximate = 4;
    public float MinConturScale = 0.8f;
    public float MaxConturScale = 1.2f;

    public List<GameObject> BackConturPrefab;
    public float MinBackConturScale = 0.8f;
    public float MaxBackConturScale = 1.2f;
    public float FrontConurZ;
    public float BackConurZ;



}
