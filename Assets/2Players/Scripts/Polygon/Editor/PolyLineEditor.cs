using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PolyLine))]
public class PolyLineEditor : Editor
{
    private int _selectedSplinePointIndex = -1;
    private PolyLine _polyline;

    private Vector3 _currentMousePlanePosition;

    void OnSceneGUI()
    {
        _polyline = target as PolyLine;
        if (_polyline.SplinePoints.Count < 3)
            GenerateBasicSpline();

        DrawLines();

        if (!_polyline.Editable)
            return;

        int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);

        switch (Event.current.type)
        {
            case EventType.MouseDown:
                if (Event.current.button == 0)
                {
                    _selectedSplinePointIndex = FindNearestSplinePoint(_currentMousePlanePosition - _polyline.transform.position);
                    if (_selectedSplinePointIndex != -1)
                        Event.current.Use();
                }
                if (Event.current.clickCount == 2 && Event.current.button == 0)
                {
                    InsertPointNearSelected();
                    Event.current.Use();
                }
                break;
            case EventType.MouseDrag:
                if (Event.current.button == 0 && _selectedSplinePointIndex != -1)
                {
                    _polyline.SplinePoints[_selectedSplinePointIndex] =  (Vector2)(_currentMousePlanePosition - _polyline.transform.position);
                    Event.current.Use();
                }
                break;
            case EventType.KeyDown:
                if (Event.current.keyCode == KeyCode.Delete)
                {
                    DeleteSelectedPoint();
                    Event.current.Use();
                }
                break;
            case EventType.MouseUp:
                if (Event.current.button == 0 && _selectedSplinePointIndex != -1)
                {
                    EditorUtility.SetDirty(_polyline);
                    _selectedSplinePointIndex = -1;
                    _polyline.SendMessageUpwards("RepaintLine",SendMessageOptions.DontRequireReceiver);
                    _polyline.SendMessage("RepaintLine", SendMessageOptions.DontRequireReceiver);
                    Event.current.Use();
                }
                break;
            case EventType.Layout:
                HandleUtility.AddDefaultControl(controlID);
                break;
        }
        CalcMouseDelta();

        DrawSpline(0);
        if (GUI.changed)
            EditorUtility.SetDirty(_polyline);
    }

    
    private void GenerateBasicSpline()
    {
        _polyline.SplinePoints = new List<Vector3>();
        _polyline.SplinePoints.Add(new Vector2(100, 100));
        _polyline.SplinePoints.Add(new Vector2(-100, 100));
        _polyline.SplinePoints.Add(new Vector2(-100, -100));
        _polyline.SplinePoints.Add(new Vector2(100, -100));
    }

    private void InsertPointNearSelected()
    {
        if (_selectedSplinePointIndex != -1)
        {
            Vector2 newPoint = (GetPoint(_selectedSplinePointIndex) + GetPoint(_selectedSplinePointIndex + 1))/2;
            _polyline.SplinePoints.Insert(_selectedSplinePointIndex + 1, newPoint);
        }
    }

    private void DeleteSelectedPoint()
    {
        if (_selectedSplinePointIndex == -1 || _polyline.SplinePoints.Count <= 3) 
            return;
        _polyline.SplinePoints.RemoveAt(_selectedSplinePointIndex);
        _selectedSplinePointIndex = -1;
    }

    private void CalcMouseDelta()
    {
        Ray mouseRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        Plane plane = new Plane(Vector3.forward, 0f);
        float enter;
        if (plane.Raycast(mouseRay, out enter))
        {
            _currentMousePlanePosition = mouseRay.GetPoint(enter);
        }
    }

    private int FindNearestSplinePoint(Vector3 worldPoint)
    {
        for (int i = 0; i < _polyline.SplinePoints.Count; i++)
        {
            float curentDistance = Vector2.Distance(_polyline.SplinePoints[i], worldPoint);
            if (curentDistance < 30f)
            {
                return i;
            }
        }
        return -1;
    }

    private void DrawSpline(int controlID)
    {
        Vector3[] points = _polyline.SplinePoints.ToArray();
        if (points.Length == 0)
            return;

        DrawPoints(controlID, points);
        DrawLines();
    }

    private void DrawPoints(int controlID, Vector3[] points)
    {
        for (int i = 0; i < _polyline.SplinePoints.Count; i++)
        {
            Vector2 splinePoint = _polyline.SplinePoints[i];
            Handles.color = Color.white;
            if (_selectedSplinePointIndex == i)
                Handles.color = Color.blue;
            if (i == 0)
                Handles.color = Color.green;
            if (i == points.Length - 1)
                Handles.color = Color.red;
            Handles.CubeHandleCap(controlID, (Vector3) splinePoint + _polyline.transform.position, Quaternion.identity,
                            _polyline.PointSize, EventType.Layout);
        }
    }

    private void DrawLines()
    {
        float t = 0;
        float offset = 1.0f/_polyline.SplinePoints.Count/4;
        Vector3 p1 = _polyline.SplinePoints[0];
        Vector3 p2;
        while (t < 1)
        {
            t += offset;
            p2 = Spline.InterpConstantSpeed(_polyline.SplinePoints.ToArray(), t);
            Handles.color = _polyline.Color;
            Handles.DrawLine(_polyline.transform.position + p1, _polyline.transform.position + p2);
            p1 = p2;
        }
    }

    private Vector2 GetPoint(int index)
    {
        int newIndex = index % _polyline.SplinePoints.Count;
        return _polyline.SplinePoints[newIndex];
    }
    
}