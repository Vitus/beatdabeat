using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;
using tk2dEditor;

[CustomEditor(typeof(PolygonEditor))]
public class PolygonEditorInspector : Editor
{
    private int _selectedSplinePointIndex = -1;
    private PolygonEditor _polygon;

    private Vector3 _currentMousePlanePosition;

    public override void OnInspectorGUI()
    {
        if (DrawDefaultInspector())
            RebuildPolygon();
    }

    private void RebuildPolygon()
    {
        //RebuildContur();
        if (_polygon.Textured)
        {
            Vector2[] vertices2D = GetSplinePoints(Math.Abs(_polygon.SplineApproximation));
            Triangulator triangulator = new Triangulator(vertices2D);
            int[] indices = triangulator.Triangulate();

            // Create the Vector3 vertices
            Vector3[] vertices = new Vector3[vertices2D.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
            }

            // Create the mesh
            Mesh newMesh = new Mesh();
            newMesh.vertices = vertices;
            newMesh.triangles = indices;
            newMesh.uv = RecalculateUVs(vertices2D);
            newMesh.RecalculateNormals();
            newMesh.RecalculateBounds();

            DestroyImmediate(_polygon.GetComponent<MeshFilter>().sharedMesh);
            _polygon.GetComponent<MeshFilter>().sharedMesh = newMesh;
            DestroyImmediate(_polygon.GetComponent<MeshCollider>().sharedMesh);
            _polygon.GetComponent<MeshCollider>().sharedMesh = newMesh;
        }
        else
        {
            MeshToNull();
        }
    }

    private Vector2[] RecalculateUVs(Vector2[] vertices)
    {
        Vector2[] uvs = new Vector2[vertices.Length];
        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x / _polygon.TextureSize, vertices[i].y / _polygon.TextureSize);
        }

        return uvs;
    }

    void OnSceneGUI()
    {
        _polygon = target as PolygonEditor;
        if (_polygon.SplinePoints.Count < 3 && _polygon.Textured)
            GenerateBasicSpline();

        if (_polygon.Textured && !_polygon.ClosedLine)
        {
            _polygon.Textured = false;
            MeshToNull();
        }

        if (!_polygon.Editable)
            return;

        int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);

        switch (Event.current.type)
        {
            case EventType.MouseDown:
                if (Event.current.button == 0)
                {
                    _selectedSplinePointIndex = FindNearestSplinePoint(_currentMousePlanePosition - _polygon.transform.position);
                    if (_selectedSplinePointIndex != -1)
                        Event.current.Use();
                }
                if (Event.current.clickCount == 2 && Event.current.button == 0)
                {
                    InsertPointNearSelected();
                    Event.current.Use();
                }
                break;
            case EventType.MouseDrag:
                if (Event.current.button == 0 && _selectedSplinePointIndex != -1)
                {
                    _polygon.SplinePoints[_selectedSplinePointIndex] =  _currentMousePlanePosition - _polygon.transform.position;
                    Event.current.Use();
                }
                break;
            case EventType.KeyDown:
                if (Event.current.keyCode == KeyCode.Delete)
                {
                    DeleteSelectedPoint();
                    EditorUtility.SetDirty(_polygon);
                    Event.current.Use();
                }
                break;
            case EventType.MouseUp:
                if (Event.current.button == 0 && _selectedSplinePointIndex != -1)
                {
                    _selectedSplinePointIndex = -1;
                    RebuildPolygon();
                    EditorUtility.SetDirty(_polygon);
                    Event.current.Use();
                }
                break;
            case EventType.Layout:
                HandleUtility.AddDefaultControl(controlID);
                break;
        }
        CalcMouseDelta();

        DrawSpline(0);
        if (GUI.changed)
            EditorUtility.SetDirty(_polygon);
    }

    private void MeshToNull()
    {
        _polygon.GetComponent<MeshFilter>().sharedMesh = null;
        _polygon.GetComponent<MeshCollider>().sharedMesh = null;
    }

    private void DestroyChildrenContur()
    {
        var childs = _polygon.transform.GetChild(0).transform.GetComponentsInChildren<Transform>();
        foreach (var child in childs)
        {
            if (child.gameObject != _polygon.transform.GetChild(0).gameObject)
                DestroyImmediate(child.gameObject);
        }
    }
    
/*    private void RebuildContur()
    {
        tk2dStaticSpriteBatcher batcher = _polygon.transform.GetChild(0).GetComponent<tk2dStaticSpriteBatcher>();

        if (!tk2dStaticSpriteBatcherEditor.NeedToCommit(batcher))
        {
            tk2dStaticSpriteBatcherEditor.Edit(batcher);
        }
        DestroyChildrenContur();
        if (_polygon.ConturPrefab.Count == 0) 
            return;
        Vector2[] points = GetSplinePoints(_polygon.ConturApproximate);
        foreach (Vector2 point in points)
        {
            GameObject randomPrefab = _polygon.ConturPrefab[Random.Range(0, _polygon.ConturPrefab.Count)];
            GameObject randomObject =
                Instantiate(randomPrefab, (Vector3) point + Vector3.forward * _polygon.FrontConurZ+ _polygon.transform.position, Quaternion.identity) as
                GameObject;
            randomObject.GetComponent<tk2dSprite>().scale *= Random.Range(_polygon.MinConturScale, _polygon.MaxConturScale);
            randomObject.transform.localEulerAngles = Vector3.forward*Random.Range(0, 360);
            randomObject.transform.parent = _polygon.transform.GetChild(0).transform;

            if (_polygon.BackConturPrefab.Count == 0) continue;
            randomPrefab = _polygon.BackConturPrefab[Random.Range(0, _polygon.BackConturPrefab.Count)];
            randomObject = Instantiate(randomPrefab, new Vector3(point.x, point.y, _polygon.BackConurZ) + _polygon.transform.position, Quaternion.identity) as GameObject;
            randomObject.GetComponent<tk2dSprite>().scale *= Random.Range(_polygon.MinBackConturScale, _polygon.MaxBackConturScale);
            randomObject.transform.localEulerAngles = Vector3.forward * Random.Range(0, 360);
            randomObject.transform.parent = _polygon.transform.GetChild(0).transform;
        }
        
        if (_polygon.Batch)
            tk2dStaticSpriteBatcherEditor.Commit(batcher);
    }*/

    private void GenerateBasicSpline()
    {
        _polygon.SplinePoints = new List<Vector2>();
        _polygon.SplinePoints.Add(new Vector2(100, 100));
        _polygon.SplinePoints.Add(new Vector2(-100, 100));
        _polygon.SplinePoints.Add(new Vector2(-100, -100));
        _polygon.SplinePoints.Add(new Vector2(100, -100));
    }

    private void InsertPointNearSelected()
    {
        if (_selectedSplinePointIndex != -1)
        {
            Vector2 newPoint = (GetPoint(_selectedSplinePointIndex) + GetPoint(_selectedSplinePointIndex + 1))/2;
            _polygon.SplinePoints.Insert(_selectedSplinePointIndex + 1, newPoint);
            RebuildPolygon();
        }
    }

    private void DeleteSelectedPoint()
    {
        if (_selectedSplinePointIndex == -1 || _polygon.SplinePoints.Count <= 3) 
            return;
        _polygon.SplinePoints.RemoveAt(_selectedSplinePointIndex);
        RebuildPolygon();
        _selectedSplinePointIndex = -1;
    }

    private void CalcMouseDelta()
    {
        Ray mouseRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        Plane plane = new Plane(Vector3.forward, 0f);
        float enter;
        if (plane.Raycast(mouseRay, out enter))
        {
            _currentMousePlanePosition = mouseRay.GetPoint(enter);
        }
    }

    private int FindNearestSplinePoint(Vector3 worldPoint)
    {
        float minDistance = float.MaxValue;
        int minIndex = int.MaxValue;
        for (int i = 0; i < _polygon.SplinePoints.Count; i++)
        {
            float curentDistance = Vector2.Distance(_polygon.SplinePoints[i], worldPoint);
            if (curentDistance < minDistance)
            {
                minIndex = i;
                minDistance = curentDistance;
            }
        }
        return minIndex;
    }

    private void DrawSpline(int controlID)
    {
        Vector2[] points = GetSplinePoints(Math.Abs(_polygon.SplineApproximation));
        if (points.Length == 0)
            return;

        for (int i = 0; i < _polygon.SplinePoints.Count; i++)
        {
            Vector2 splinePoint = _polygon.SplinePoints[i];
            if (_selectedSplinePointIndex == i)
                Handles.color = Color.red;
            Handles.CubeHandleCap(controlID, (Vector3) splinePoint + _polygon.transform.position, Quaternion.identity, _polygon.PointSize, EventType.Layout);
            Handles.color = Color.white;
        }

        for (int index = 0; index < points.Length - 1; index++)
        {
            Handles.DrawLine((Vector3)points[index] + _polygon.transform.position, (Vector3)points[index + 1] + _polygon.transform.position);
        }
        if (_polygon.ClosedLine)
            Handles.DrawLine((Vector3)points[points.Length - 1] + _polygon.transform.position, (Vector3)points[0] + _polygon.transform.position);
    }

    private Vector2[] GetSplinePoints(int approximation)
    {
        if (approximation <= 0)
            return new Vector2[0];
        
        int upLimit = _polygon.SplinePoints.Count - 3;
        Vector2[] points = new Vector2[upLimit * approximation+1];

        if (_polygon.ClosedLine)
        {
            upLimit = _polygon.SplinePoints.Count;
            points = new Vector2[upLimit*approximation];
        }

        int pointNum = 0;
        for (int index = 0; index < upLimit; index++)
        {
            Vector3[] currentPoints = {GetPoint(index), GetPoint(index + 1), GetPoint(index + 2), GetPoint(index + 3)};
            for (int i = 0; i < approximation; i++)
            {
                points[pointNum] = PositionOnSpline(currentPoints, (float)i / approximation);
                pointNum++;
            }
        }

        if (!_polygon.ClosedLine)
            points[points.Length - 1] = GetPoint(upLimit+1);

        return points;
    }

    private Vector3 GetPoint(int index)
    {
        int newIndex = index % _polygon.SplinePoints.Count;
        return _polygon.SplinePoints[newIndex];
    }
    
    static Vector3 PositionOnSpline(Vector3[] points, float t)
    {
        Vector3 position = 0.5f * ((2 * points[1]) +
                                   (-points[0] + points[2]) * t +
                                   (2 * points[0] - 5 * points[1] + 4 * points[2] - points[3]) * t * t +
                                   (-points[0] + 3 * points[1] - 3 * points[2] + points[3]) * t * t * t);
        return position;
    }

    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1.0f - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;
     
        Vector3 p = uuu * p0; //first term
        p += 3 * uu * t * p1; //second term
        p += 3 * u * tt * p2; //third term
        p += ttt * p3; //fourth term
     
        return p;
    }

}