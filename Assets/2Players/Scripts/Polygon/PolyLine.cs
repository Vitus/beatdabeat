using System.Collections.Generic;
using UnityEngine;

public class PolyLine : MonoBehaviour
{
    //[HideInInspector]
    public List<Vector3> SplinePoints = new List<Vector3>();
    public Color Color = Color.white;
    public bool Editable = false;
    public int PointSize = 10;

}
