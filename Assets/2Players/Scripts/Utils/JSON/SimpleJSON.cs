﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


    public enum JSONBinaryTag
    {
        Array = 1,
        Class = 2,
        Value = 3,
        IntValue = 4,
        DoubleValue = 5,
        BoolValue = 6,
        FloatValue = 7,
    }

    public class JSONNode
    {
        #region common interface
        public virtual void Add(string aKey, JSONNode aItem) { }
        public virtual JSONNode this[int aIndex]
        {
            get
            {
                return null;
            }
            set { }
        }
        public virtual JSONNode this[string aKey]
        {
            get
            {
                return null;
            }
            set { }
        }
        public virtual string Value { get { return ""; } set { } }
        public virtual int Count { get { return 0; } }

        public virtual void Add(JSONNode aItem)
        {
            Add("", aItem);
        }

        public virtual JSONNode Remove(string aKey) { return null; }
        public virtual JSONNode Remove(int aIndex) { return null; }
        public virtual JSONNode Remove(JSONNode aNode) { return aNode; }

        public virtual IEnumerable Childs { get { return null; } }

        public override string ToString()
        {
            return "JSONNode";
        }
        public virtual string ToString(string aPrefix)
        {
            return "JSONNode";
        }

        #endregion common interface

        #region typecasting properties
        public virtual int AsInt
        {
            get
            {
                int v = 0;
                if (int.TryParse(Value, out v))
                    return v;
                return 0;
            }
            set
            {
                Value = value.ToString();
            }
        }
        public virtual float AsFloat
        {
            get
            {
                float v = 0.0f;
                if (float.TryParse(Value, out v))
                    return v;
                return 0.0f;
            }
            set
            {
                Value = value.ToString();
            }
        }
        public virtual double AsDouble
        {
            get
            {
                double v = 0.0;
                if (double.TryParse(Value, out v))
                    return v;
                return 0.0;
            }
            set
            {
                Value = value.ToString();
            }
        }
        public virtual bool AsBool
        {
            get
            {
                bool v = false;
                if (bool.TryParse(Value, out v))
                    return v;
                return !string.IsNullOrEmpty(Value);
            }
            set
            {
                Value = (value) ? "true" : "false";
            }
        }
        public virtual JSONArray AsArray
        {
            get
            {
                return this as JSONArray;
            }
        }
        public virtual JSONClass AsObject
        {
            get
            {
                return this as JSONClass;
            }
        }


        #endregion typecasting properties

        #region operators
        public static implicit operator JSONNode(string s)
        {
            return new JSONData(s);
        }
        public static implicit operator string(JSONNode d)
        {
            return (d == null) ? null : d.Value;
        }
        public static bool operator ==(JSONNode a, object b)
        {
            if (b == null && a is JSONLazyCreator)
                return true;
            return System.Object.ReferenceEquals(a, b);
        }

        public static bool operator !=(JSONNode a, object b)
        {
            return !(a == b);
        }
        public override bool Equals(object obj)
        {
            return System.Object.ReferenceEquals(this, obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


        #endregion operators
        internal static string Escape(string aText)
        {
            string result = "";
            foreach (char c in aText)
            {
                if (c == '\\')
                {
                    result += "\\\\";
                }
                else if (c == '\"')
                {
                    result += "\\\"";
                }
                else if (c == '\'')
                {
                    result += "\\\'";
                }
                else if (c == '\n')
                {
                    result += "\\n";
                }
                else if (c == '\r')
                {
                    result += "\\r";
                }
                else if (c == '\t')
                {
                    result += "\\t";
                }
                else if (c == '\b')
                {
                    result += "\\b";
                }
                else if (c == '\f')
                {
                    result += "\\f";
                }
                else
                {
                    result += c;
                }
            }
            return result;
        }

        public static JSONNode Parse(string aJSON)
        {
            Stack<JSONNode> stack = new Stack<JSONNode>();
            JSONNode ctx = null;
            int i = 0;
            string Token = "";
            string TokenName = "";
            bool QuoteMode = false;
            while (i < aJSON.Length)
            {
                if (aJSON[i] == '{')
                {
                    if (QuoteMode)
                    {
                        Token += aJSON[i].ToString();
                    }
                    else
                    {
                        stack.Push(new JSONClass());
                        if (ctx != null)
                        {
                            TokenName = TokenName.Trim();
                            if (ctx is JSONArray)
                                ctx.Add(stack.Peek());
                            else if (TokenName != "")
                                ctx.Add(TokenName, stack.Peek());
                        }
                        TokenName = "";
                        Token = "";
                        ctx = stack.Peek();
                    }
                }
                else if (aJSON[i] == '[')
                {
                    if (QuoteMode)
                    {
                        Token += aJSON[i].ToString();
                    }
                    else
                    {
                        stack.Push(new JSONArray());
                        if (ctx != null)
                        {
                            TokenName = TokenName.Trim();
                            if (ctx is JSONArray)
                                ctx.Add(stack.Peek());
                            else if (TokenName != "")
                                ctx.Add(TokenName, stack.Peek());
                        }
                        TokenName = "";
                        Token = "";
                        ctx = stack.Peek();
                    }
                }
                else if (aJSON[i] == '}' || aJSON[i] == ']')
                {
                    if (QuoteMode)
                    {
                        Token += aJSON[i].ToString();
                    }
                    else
                    {
                        if (stack.Count == 0)
                            throw new Exception("JSON Parse: Too many closing brackets");

                        stack.Pop();
                        if (Token != "")
                        {
                            TokenName = TokenName.Trim();
                            if (ctx is JSONArray)
                                ctx.Add(Token);
                            else if (TokenName != "")
                                ctx.Add(TokenName, Token);
                        }
                        TokenName = "";
                        Token = "";
                        if (stack.Count > 0)
                            ctx = stack.Peek();
                    }
                }
                else if (aJSON[i] == ':')
                {
                    if (QuoteMode)
                    {
                        Token += aJSON[i].ToString();
                    }
                    else
                    {
                        TokenName = Token;
                        Token = "";
                    }
                }
                else if (aJSON[i] == '\'')
                {
                    QuoteMode ^= true;
                }
                else if (aJSON[i] == ',')
                {
                    if (QuoteMode)
                    {
                        Token += aJSON[i].ToString();
                    }
                    else
                    {
                        if (Token != "")
                        {
                            if (ctx is JSONArray)
                                ctx.Add(Token);
                            else if (TokenName != "")
                                ctx.Add(TokenName, Token);
                        }
                        TokenName = "";
                        Token = "";
                    }
                }
                else if (aJSON[i] == '\r' || aJSON[i] == '\n')
                {
                }
                else if (aJSON[i] == ' ' || aJSON[i] == '\t')
                {
                    if (QuoteMode)
                        Token += aJSON[i];
                }
                else if (aJSON[i] == '\\')
                {
                    ++i;
                    if (QuoteMode)
                    {
                        char C = aJSON[i];
                        if (C == 't')
                        {
                            Token += '\t';
                        }
                        else if (C == 'r')
                        {
                            Token += '\r';
                        }
                        else if (C == 'n')
                        {
                            Token += '\n';
                        }
                        else if (C == 'b')
                        {
                            Token += '\b';
                        }
                        else if (C == 'f')
                        {
                            Token += '\f';
                        }
                        else if (C == 'u')
                        {
                            string s = aJSON.Substring(i + 1, 4);
                            Token += (char) int.Parse(s, System.Globalization.NumberStyles.AllowHexSpecifier);
                            i += 4;
                        }
                        else
                        {
                            Token += C;
                        }
                    }
                }
                else
                {
                    Token += aJSON[i].ToString();
                }
                ++i;
            }
            if (QuoteMode)
            {
                throw new Exception("JSON Parse: Quotation marks seems to be messed up.");
            }
            return ctx;
        }
        public static KeyValuePair<string, JSONNode> DictionaryPairAtIndex(Dictionary<string, JSONNode> dictionary, int index)
        {
            int i = 0;
            foreach (KeyValuePair<string, JSONNode> pair in dictionary)
            {
                if (i == index)
                    return pair;
                i++;
            }
            throw new IndexOutOfRangeException();
        }
        public static KeyValuePair<string, JSONNode> DictionaryFindByValue(Dictionary<string, JSONNode> dictionary, JSONNode value)
        {
            int i = 0;
            foreach (KeyValuePair<string, JSONNode> pair in dictionary)
            {
                if (pair.Value == value)
                    return pair;
                i++;
            }
            throw new IndexOutOfRangeException();
        }
    } // End of JSONNode

    public class JSONArray : JSONNode, IEnumerable
    {
        private List<JSONNode> m_List = new List<JSONNode>();
        public override JSONNode this[int aIndex]
        {
            get
            {
                if (aIndex < 0 || aIndex >= m_List.Count)
                    return new JSONLazyCreator(this);
                return m_List[aIndex];
            }
            set
            {
                if (aIndex < 0 || aIndex >= m_List.Count)
                    m_List.Add(value);
                else
                    m_List[aIndex] = value;
            }
        }
        public override JSONNode this[string aKey]
        {
            get
            {
                return new JSONLazyCreator(this);
            }
            set { m_List.Add(value); }
        }
        public override int Count
        {
            get { return m_List.Count; }
        }
        public override void Add(string aKey, JSONNode aItem)
        {
            m_List.Add(aItem);
        }
        public override JSONNode Remove(int aIndex)
        {
            if (aIndex < 0 || aIndex >= m_List.Count)
                return null;
            JSONNode tmp = m_List[aIndex];
            m_List.RemoveAt(aIndex);
            return tmp;
        }
        public override JSONNode Remove(JSONNode aNode)
        {
            m_List.Remove(aNode);
            return aNode;
        }
        public override IEnumerable Childs
        {
            get { return m_List; }
        }
        public IEnumerator GetEnumerator()
        {
            foreach (JSONNode N in m_List)
                yield return N;
        }
        public override string ToString()
        {
            string result = "[ ";
            foreach (JSONNode N in m_List)
            {
                if (result.Length > 2)
                    result += ", ";
                result += N.ToString();
            }
            result += " ]\n\r";
            return result;
        }
        public override string ToString(string aPrefix)
        {
            string result = "[ ";
            foreach (JSONNode N in m_List)
            {
                if (result.Length > 3)
                    result += ", ";
                result += "\n" + aPrefix + "   ";
                result += N.ToString(aPrefix + "   ");
            }
            result += "\n" + aPrefix + "]\n\r";
            return result;
        }
    } // End of JSONArray

    [Serializable]
    public class JSONClassElement
    {
        public string Key;
        public JSONNode Value;

        public JSONClassElement(string key, JSONNode value)
        {
            Key = key;
            Value = value;
        }
    }

    public class JSONClass : JSONNode, IEnumerable
    {
        private List<JSONClassElement> m_elements = new List<JSONClassElement>();
        public List<JSONClassElement> Elements { get { return m_elements; } } 

        public JSONClassElement FindElement(string aKey)
        {
            for (int i = 0; i < m_elements.Count; i++)
            {
                JSONClassElement element = m_elements[i];
                if (element.Key == aKey)
                {
                    return element;
                }
            }
            return null;
        }      
        public JSONClassElement FindElement(JSONNode aValue)
        {
            for (int i = 0; i < m_elements.Count; i++)
            {
                JSONClassElement element = m_elements[i];
                if (element.Value == aValue)
                {
                    return element;
                }
            }
            return null;
        }

        public override JSONNode this[string aKey]
        {
            get
            {
                var element = FindElement(aKey);
                if (element != null)
                    return element.Value;
                else
                    return new JSONLazyCreator(this, aKey);
            }
            set
            {
                var element = FindElement(aKey);
                if (element != null)
                    element.Value = value;
                else
                    m_elements.Add(new JSONClassElement(aKey, value));
            }
        }
        public override JSONNode this[int aIndex]
        {
            get
            {
                if (aIndex < 0 || aIndex >= m_elements.Count)
                    return null;
                return m_elements[aIndex].Value;
            }
            set
            {
                if (aIndex < 0 || aIndex >= m_elements.Count)
                    return;
                m_elements[aIndex].Key = value;
            }
        }
        public override int Count
        {
            get { return m_elements.Count; }
        }


        public override void Add(string aKey, JSONNode aItem)
        {
            if (!string.IsNullOrEmpty(aKey))
            {
                JSONClassElement element = FindElement(aKey);
                if (element != null)
                    element.Value = aItem;
                else
                    m_elements.Add(new JSONClassElement(aKey, aItem));
            }
            else
                m_elements.Add(new JSONClassElement(Random.Range(0, int.MaxValue).ToString(), aItem));
        }

        public override JSONNode Remove(string aKey)
        {
            JSONClassElement element = FindElement(aKey);
            if (element == null)
                return null;
            JSONNode tmp = element.Value;
            m_elements.Remove(element);
            return tmp;
        }
        public override JSONNode Remove(int aIndex)
        {
            if (aIndex < 0 || aIndex >= m_elements.Count)
                return null;
            var item = m_elements[aIndex].Value;
            m_elements.RemoveAt(aIndex);
            return item.Value;
        }
        public override JSONNode Remove(JSONNode aNode)
        {
            JSONClassElement element = FindElement(aNode);
            if (element != null)
            {
                m_elements.Remove(element);
                return aNode;
            }
            return null;
        }

        public override IEnumerable Childs
        {
            get { return m_elements; }
        }

        public IEnumerator GetEnumerator()
        {
            foreach (JSONClassElement element in m_elements)
            {
                yield return element;
            }
        }
        public override string ToString()
        {
            string result = "{";
            foreach (JSONClassElement N in m_elements)
            {
                if (result.Length > 2)
                    result += ", ";
                result += "'" + Escape(N.Key) + "\':" + N.Value.ToString();
            }
            result += "}";
            return result;
        }
        public override string ToString(string aPrefix)
        {
            string result = "{ ";
            foreach (JSONClassElement N in m_elements)
            {
                if (result.Length > 3)
                    result += ", ";
                result += "\n" + aPrefix + "   ";
                result += "\'" + Escape(N.Key) + "\' : " + N.Value.ToString(aPrefix + "   ");
            }
            result += "\n" + aPrefix + "}";
            return result;
        }

    } // End of JSONClass

    public class JSONData : JSONNode
    {
        private string m_Data;
        public override string Value
        {
            get { return m_Data; }
            set { m_Data = value; }
        }
        public JSONData(string aData)
        {
            m_Data = aData;
        }
        public JSONData(float aData)
        {
            AsFloat = aData;
        }
        public JSONData(double aData)
        {
            AsDouble = aData;
        }
        public JSONData(bool aData)
        {
            AsBool = aData;
        }
        public JSONData(int aData)
        {
            AsInt = aData;
        }

        public override string ToString()
        {
            return "\'" + Escape(m_Data) + "\'";
        }
        public override string ToString(string aPrefix)
        {
            return "\'" + Escape(m_Data) + "\'";
        }
 } // End of JSONData

internal class JSONLazyCreator : JSONNode
{
    private JSONNode m_Node = null;
    private string m_Key = null;

    public JSONLazyCreator(JSONNode aNode)
    {
        m_Node = aNode;
        m_Key = null;
    }

    public JSONLazyCreator(JSONNode aNode, string aKey)
    {
        m_Node = aNode;
        m_Key = aKey;
    }

    private void Set(JSONNode aVal)
    {
        if (m_Key == null)
        {
            m_Node.Add(aVal);
        }
        else
        {
            m_Node.Add(m_Key, aVal);
        }
        m_Node = null; // Be GC friendly.
    }

    public override JSONNode this[int aIndex]
    {
        get { return new JSONLazyCreator(this); }
        set
        {
            var tmp = new JSONArray();
            tmp.Add(value);
            Set(tmp);
        }
    }

    public override JSONNode this[string aKey]
    {
        get { return new JSONLazyCreator(this, aKey); }
        set
        {
            var tmp = new JSONClass();
            tmp.Add(aKey, value);
            Set(tmp);
        }
    }

    public override void Add(JSONNode aItem)
    {
        var tmp = new JSONArray();
        tmp.Add(aItem);
        Set(tmp);
    }

    public override void Add(string aKey, JSONNode aItem)
    {
        var tmp = new JSONClass();
        tmp.Add(aKey, aItem);
        Set(tmp);
    }

    public static bool operator ==(JSONLazyCreator a, object b)
    {
        if (b == null)
            return true;
        return System.Object.ReferenceEquals(a, b);
    }

    public static bool operator !=(JSONLazyCreator a, object b)
    {
        return !(a == b);
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return true;
        return System.Object.ReferenceEquals(this, obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return "";
    }

    public override string ToString(string aPrefix)
    {
        return "";
    }

    public override int AsInt
    {
        get
        {
            JSONData tmp = new JSONData(0);
            Set(tmp);
            return 0;
        }
        set
        {
            JSONData tmp = new JSONData(value);
            Set(tmp);
        }
    }

    public override float AsFloat
    {
        get
        {
            JSONData tmp = new JSONData(0.0f);
            Set(tmp);
            return 0.0f;
        }
        set
        {
            JSONData tmp = new JSONData(value);
            Set(tmp);
        }
    }

    public override double AsDouble
    {
        get
        {
            JSONData tmp = new JSONData(0.0);
            Set(tmp);
            return 0.0;
        }
        set
        {
            JSONData tmp = new JSONData(value);
            Set(tmp);
        }
    }

    public override bool AsBool
    {
        get
        {
            JSONData tmp = new JSONData(false);
            Set(tmp);
            return false;
        }
        set
        {
            JSONData tmp = new JSONData(value);
            Set(tmp);
        }
    }

    public override JSONArray AsArray
    {
        get
        {
            JSONArray tmp = new JSONArray();
            Set(tmp);
            return tmp;
        }
    }

    public override JSONClass AsObject
    {
        get
        {
            JSONClass tmp = new JSONClass();
            Set(tmp);
            return tmp;
        }
    }
}// End of JSONLazyCreator
