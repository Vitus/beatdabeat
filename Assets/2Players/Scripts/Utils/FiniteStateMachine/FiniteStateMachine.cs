﻿using UnityEngine;
using System.Collections;

abstract public class FSMState<T>
{
    protected T Owner;

    public FSMState(T owner)
    {
        Owner = owner;
    }

    virtual public void Enter()
    {
    }

    virtual public void Update()
    {
    }

    virtual public void FixedUpdate()
    {
    }

    virtual public void Exit()
    {
    }
}

public class FiniteStateMachine<T> : MonoBehaviour
{
    private FSMState<T> _currentState;
    private FSMState<T> _previousState;

    public void Awake()
    {
        _currentState = null;
        _previousState = null;
    }

    public void Configure(FSMState<T> initialState)
    {
        ChangeState(initialState);
    }

    public void Update()
    {
        if (_currentState != null) _currentState.Update();
    }

    public void FixedUpdate()
    {
        if (_currentState != null) _currentState.FixedUpdate();
    }

    public void ChangeState(FSMState<T> newState)
    {
        _previousState = _currentState;
        if (_currentState != null)
            _currentState.Exit();
        _currentState = newState;
        if (_currentState != null)
            _currentState.Enter();
    }

    public void RevertToPreviousState()
    {
        if (_previousState != null)
            ChangeState(_previousState);
    }
};