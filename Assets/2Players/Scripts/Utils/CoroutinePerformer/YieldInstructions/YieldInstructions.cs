using UnityEngine;

namespace abutt1.CustomCoroutines
{

	#region Custom

	public class CustomYieldToggle : ICustomYieldInstruction
	{
		public  float                       GetProgress ()      { return IsDone() ? 1 : 0;                  }
		public  bool                        IsDone ()           { return m_Finished;                        }
		
		private bool    m_Finished;
		
		public  void    Toggle(bool toVal)
		{
			m_Finished = toVal;
		}
		
	}
	
	public class WaitForAnimationLegacy : ICustomYieldInstruction
	{
		public  float                       GetProgress ()      { return IsDone() ? 1 : 0;                  }
		public  bool                        IsDone ()           { return AnimationIsFinished(m_anim);       }
		
		private Animation m_anim;
		
		public WaitForAnimationLegacy(Animation animToWatch)
		{
			m_anim = animToWatch;
		}
		
		private static bool AnimationIsFinished(Animation toCheck)
		{
			if(toCheck == null)     { return false; }
			if(toCheck.isPlaying)   { return false; }
			
			return true;
		}
		
	}

	public class WaitForFrames : ICustomYieldInstruction
	{
		public float GetProgress ()
		{
			if(m_numFrames < 1) { return 1; }
			
			int currentFrameT = Time.frameCount - m_startFrame;
			return (float)currentFrameT / (float)m_numFrames;
		}
		
		public bool IsDone ()
		{
			//Debug.Log("IsDone: " + GetProgress());
			return GetProgress() >= 1;
		}
		
		private int m_numFrames;
		private int m_startFrame;
		
		public WaitForFrames(int framesToWait)
		{
			m_startFrame = Time.frameCount;
			m_numFrames  = framesToWait;
		}
	}

	#endregion

}
