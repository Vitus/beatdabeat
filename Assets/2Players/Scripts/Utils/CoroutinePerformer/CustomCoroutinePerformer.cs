using UnityEngine;
using System.Collections;

public static class CustomCoroutinePerformer
{
    private static CustomCoroutine PerformCoroutineLogic(MonoBehaviour monoBehaviour, IEnumerator routine,
                                                        CoroutineDelegate finishedCallback,
                                                        MessageDelegate exceptionCallback, bool isSubRoutine)
    {
        // don't bother if no data
        if (routine == null)
        {
            return null;
        }

        // create our wrapper coroutine
        CustomCoroutine wrapperCoroutine = new CustomCoroutine(routine);

        // register for our events
        if (finishedCallback != null)
        {
            wrapperCoroutine.FinishedEvent += finishedCallback;
        }
        if (exceptionCallback != null)
        {
            wrapperCoroutine.ExceptionEvent += exceptionCallback;
        }

        // start the coroutine up
        if (!isSubRoutine)
        {
            monoBehaviour.StartCoroutine(wrapperCoroutine);
        }

        return wrapperCoroutine;
    }

    public static CustomCoroutine PerformCoroutine(this MonoBehaviour monoBehaviour, IEnumerator routine)
    {
        return PerformCoroutine(monoBehaviour, routine, null, null);
    }

    public static CustomCoroutine PerformCoroutine(this MonoBehaviour monoBehaviour, IEnumerator routine,
                                                   MessageDelegate exceptionCallback)
    {
        return PerformCoroutine(monoBehaviour, routine, null, exceptionCallback);
    }

    public static CustomCoroutine PerformCoroutine(this MonoBehaviour monoBehaviour, IEnumerator routine,
                                                   CoroutineDelegate finishedCallback)
    {
        return PerformCoroutine(monoBehaviour, routine, finishedCallback, null);
    }

    public static CustomCoroutine PerformCoroutine(this MonoBehaviour monoBehaviour, IEnumerator routine,
                                                   CoroutineDelegate finishedCallback,
                                                   MessageDelegate exceptionCallback)
    {
        return PerformCoroutineLogic(monoBehaviour, routine, finishedCallback, exceptionCallback, false);
    }

    public static CustomCoroutine SubRoutine(this MonoBehaviour monoBehaviour, IEnumerator routine)
    {
        return SubRoutine(monoBehaviour, routine, null, null);
    }

    public static CustomCoroutine SubRoutine(this MonoBehaviour monoBehaviour, IEnumerator routine,
                                                   MessageDelegate exceptionCallback)
    {
        return SubRoutine(monoBehaviour, routine, null, exceptionCallback);
    }

    public static CustomCoroutine SubRoutine(this MonoBehaviour monoBehaviour, IEnumerator routine,
                                                   CoroutineDelegate finishedCallback)
    {
        return SubRoutine(monoBehaviour, routine, finishedCallback, null);
    }

    public static CustomCoroutine SubRoutine(this MonoBehaviour monoBehaviour, IEnumerator routine,
                                                   CoroutineDelegate finishedCallback,
                                                   MessageDelegate exceptionCallback)
    {
        return PerformCoroutineLogic(monoBehaviour, routine, finishedCallback, exceptionCallback, true);
    }
}