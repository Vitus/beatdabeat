using UnityEngine;
using System.Collections;

public  delegate    void    CoroutineDelegate(CustomCoroutine coroutine);
public  delegate    void    MessageDelegate(string message);
		
public class CustomCoroutine : IEnumerator, ICustomYieldInstruction
{
    private IEnumerator currentOp;

    private MessageDelegate exceptionDelegate;
    private CoroutineDelegate finishedDelegate;

    // We define our add/remove handlers in order for dll packaged code to run on aot builds
    public event MessageDelegate ExceptionEvent
    {
        add { exceptionDelegate += value; }
        remove { exceptionDelegate -= value; }
    }

    public event CoroutineDelegate FinishedEvent
    {
        add { finishedDelegate += value; }
        remove { finishedDelegate -= value; }
    }

    private bool paused;
    private bool cancelled;

    #region Constructors

    public CustomCoroutine(IEnumerator routine)
    {
        InitCoroutineWrapper(routine, null);
    }

    public CustomCoroutine(IEnumerator routine, MessageDelegate exceptionCallback)
    {
        InitCoroutineWrapper(routine, exceptionCallback);
    }

    ~CustomCoroutine()
    {
        if (!Finished)
        {
            // throw an exception
            RaiseExceptionEvent("Coroutine object destroyed before completion");
        }
    }

    #endregion


    #region InternalLogic

    private void InitCoroutineWrapper(IEnumerator withRoutine, MessageDelegate withExceptionCB)
    {
        currentOp = CoroutineWrapperLogic(withRoutine);

        if (withExceptionCB != null)
        {
            ExceptionEvent += withExceptionCB;
        }
    }

    private IEnumerator CoroutineWrapperLogic(IEnumerator toWrap)
    {
        while (currentOp != null)
        {
            // continue if paused
            if (paused)
            {
                yield return false;
                continue;
            }

            if (!toWrap.MoveNext())
            {
                break;
            }

            // work out if the last returned obj is something we need to handle or not
            // if it isn't, we can just return it
            // if it is something we need to handle, somehow take care of the logic ourselves

            object tmpCurrent = toWrap.Current;
            ICustomYieldInstruction currentYieldInstruction = GetCustomYieldInstructionFromObject(tmpCurrent);

            if (currentYieldInstruction != null)
            {
                // we need to handle it

                while (currentYieldInstruction != null)
                {
                    // we need to check ourselves whether it's good to go or not
                    if (currentYieldInstruction.IsDone())
                    {
                        // we've finished
                        currentYieldInstruction = null;
                    }
                    else
                    {
                        // we need to wait
                        yield return false;
                    }
                }
            }
            else
            {
                // unity can handle it
                yield return tmpCurrent;
            }

        }

        // we have finished
        currentOp = null;

        RaiseFinishedEvent(this);

    }

    private ICustomYieldInstruction GetCustomYieldInstructionFromObject(object toCheck)
    {
        if (toCheck == null)
        {
            return null;
        }
        if (toCheck is ICustomYieldInstruction)
        {
            return (ICustomYieldInstruction) toCheck;
        }
        if (toCheck is IEnumerator)
        {
            return new CustomCoroutine((IEnumerator) toCheck);
        }

        return null;
    }

    private void RaiseExceptionEvent(string message)
    {
        if (exceptionDelegate != null)
        {
            exceptionDelegate(message);
        }
    }

    private void RaiseFinishedEvent(CustomCoroutine coroutine)
    {
        if (finishedDelegate != null)
        {
            finishedDelegate(coroutine);
        }
    }

    #endregion

    #region ICustomCoroutine

    public bool Finished
    {
        get { return (currentOp == null); }
    }

    public bool Cancelled
    {
        get { return cancelled; }
    }

    public void Cancel()
    {
        cancelled = true;
        currentOp = null;
    }

    public bool Running
    {
        get { return (!Finished && !paused); }
    }

    public void Resume()
    {
        paused = false;
    }

    public void Pause()
    {
        paused = true;
    }

    public bool Pump()
    {
        return ((IEnumerator) this).MoveNext();
    }

    #endregion

    // keeping this private so the user doesn't have to worry about it

    #region IEnumerator

    bool IEnumerator.MoveNext()
    {
        if (currentOp != null)
        {
            return currentOp.MoveNext();
        }
        else
        {
            // throw an exception?
            if (!Cancelled)
            {
                RaiseExceptionEvent("MoveNext() called but internal operation is null -> likely cancelled.");
            }
            else
            {
                // potentially let the user know?
            }
            return false;
        }
    }

    object IEnumerator.Current
    {
        get
        {
            if (currentOp != null)
            {
                return currentOp.Current;
            }
            else
            {
                // throw an exception?
                return null;
            }
        }
    }

    void IEnumerator.Reset()
    {

    }

    #endregion

    // keeping this private so the user doesn't have to worry about it

    #region ICustomYieldInstructions

    float ICustomYieldInstruction.GetProgress()
    {
        return ((ICustomYieldInstruction) this).IsDone() ? 1 : 0;
    }

    bool ICustomYieldInstruction.IsDone()
    {
        return this.Finished;
    }

    #endregion
}
