﻿using NelliTweenEngine;
using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LightningPosition : MonoBehaviour {

    public Vector3 StartPosition;
    public Vector3 EndPosition;

    public Transform LightningHolder;

    public Transform Start;
    public Transform End;

    void LateUpdate () {
        if (LightningHolder != null) {
            LightningHolder.position = (StartPosition + EndPosition)/2;
            LightningHolder.rotation = Quaternion.Euler(0f, 0f, H.VectorAngle(EndPosition - StartPosition));
            LightningHolder.SetXLocalScale((StartPosition - EndPosition).magnitude);
            Start.position = StartPosition;
            End.position = EndPosition;
        }
	}
}
